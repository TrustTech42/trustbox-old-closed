<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsToPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->string('headline');
            $table->string('author');
            $table->string('colour');
            $table->string('snippet');
            $table->string('benefit_snippet');
            $table->boolean('is_guest')->default(false);
            $table->boolean('is_connected')->default(false);
            $table->boolean('notify')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn([
                'author', 'colour', 'snippet', 'benefit_snippet', 'headline', 'is_guest', 'notify', 'is_connected'
            ]);
        });
    }
}
