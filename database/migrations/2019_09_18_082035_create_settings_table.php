<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name');
            $table->string('value')->nullable();

            $table->text('welcome_letter_content');
            $table->text('welcome_email_content');
            $table->text('invitation_letter_content');

            $table->text('code_invite_email_content');
            $table->text('covid_invite_email_content')->nullable();

            $table->text('signup_email_content');
            $table->text('boosted_email_content');
            $table->text('enrolled_email_content');
            $table->text('invite_email_content');
            $table->text('removed_email_content');

            // storage options
            $table->integer('default_storage')->default(5);
            $table->integer('upgraded_storage')->default(10);
            $table->integer('upgraded_storage_price')->default(5);

            // invite limits
            $table->integer('household_limit')->default(4);
            $table->integer('gifted_limit')->default(2);
            $table->integer('trusted_limit')->default(4);

            $table->string('fm_subdomain')->default('filemanager');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
