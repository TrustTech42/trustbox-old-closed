<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveGoCardlessFieldsFromSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->dropColumn(['redirect_id', 'mandate_id', 'customer_id', 'gocardless_sub_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->string('redirect_id')->nullable();
            $table->string('mandate_id')->nullable();
            $table->string('customer_id')->nullable();
            $table->string('gocardless_sub_id')->nullable();
        });
    }
}
