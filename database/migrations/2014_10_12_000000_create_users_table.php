<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Carbon;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->integer('role_id');

            $table->boolean('has_boosted')->default(false);

            // replaced customer table
            $table->string('first_name');
            $table->string('last_name');
            $table->string('address1');
            $table->string('address2')->nullable();
            $table->string('city');
            $table->string('postcode');
            $table->string('telephone');
            $table->string('mobile')->nullable();

            $table->string('customer_number');
            $table->string('date_of_birth')->nullable();

            // Security
            $table->boolean('change_password')->default(false);

            // linked accounts
            $table->integer('referred')->nullable();

            $table->boolean('letter_printed')->default(0);

            # File manager
            $table->integer('storage')->default(0);
            $table->integer('storage_used')->default(0);

            // Probably don't need this bottom one anymore
            $table->boolean('storage_upgraded')->default(false);

            $table->date('file_manager_trial_date')->nullable();

            $table->boolean('deceased')->default(false);
            $table->boolean('marketing_consent')->nullable()->default(false);
            $table->softDeletes();
            // Other
            # $table->boolean('approved')->default(false);
            $table->string('api_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
