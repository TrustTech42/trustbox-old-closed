<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIncludesBenefitFormAndButtonFieldsToBenefitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('benefits', function (Blueprint $table) {
            $table->boolean('includes_benefit_form')->default(false)
                ->after('includes_file_manager');
            $table->boolean('includes_benefit_phone_button')->default(false)
                ->after('includes_file_manager');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('benefits', function (Blueprint $table) {
            $table->dropColumn(['includes_benefit_form', 'includes_benefit_phone_button']);
        });
    }
}
