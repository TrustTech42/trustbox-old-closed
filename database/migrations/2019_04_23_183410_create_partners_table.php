<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('user_id')->nullable();
            $table->string('name');
            $table->string('primary_colour');
            $table->string('secondary_colour')->nullable()->default('#000000');
            $table->string('logo')->nullable()->default('logo.png');
            $table->string('header_mode')->nullable()->default('light');
            $table->string('subdomain');
            $table->string('contact_phone');
            $table->string('contact_email');
            $table->text('description')->nullable();
            $table->boolean('active')->nullable()->default(true);

            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('city')->nullable();
            $table->string('postcode')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners');
    }
}
