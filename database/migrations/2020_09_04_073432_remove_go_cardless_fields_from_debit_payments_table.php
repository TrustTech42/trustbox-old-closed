<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveGoCardlessFieldsFromDebitPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('debit_payments', function (Blueprint $table) {
            $table->dropColumn(['gocardless_payment_id', 'status']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('debit_payments', function (Blueprint $table) {
            $table->string('gocardless_payment_id');
            $table->string('status');
        });
    }
}
