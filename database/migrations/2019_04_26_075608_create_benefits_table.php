<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBenefitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benefits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('page_id')->default(null)->nullable();
            $table->string('name');
            $table->longText('description');
            $table->longText('boosted_description');
            $table->string('thumbnail')->nullable();
            $table->string('banner')->nullable();
            $table->string('price')->nullable();
            $table->string('provider_id');
            $table->string('featured')->default("0");
            $table->longText('redeem_text')->nullable();
            $table->boolean('includes_file_manager')->default(false);

            $table->string('provider_name')->nullable();
            $table->text('provider_description')->nullable();
            $table->text('provider_usp')->nullable();
            $table->string('provider_logo')->nullable();

            // used to distinguish between partner-specific benefits and general benefits
            $table->boolean('unique')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('benefits');
    }
}
