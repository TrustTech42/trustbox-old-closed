<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToPartnerPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partner_post', function (Blueprint $table) {
            $table->string('connection')->nullable();
            $table->boolean('published')->default(0);
            $table->boolean('linked_benefit')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('partner_post', function (Blueprint $table) {
            $table->dropColumn(['connection', 'published', 'linked_benefit']);
        });
    }
}
