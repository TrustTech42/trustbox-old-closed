<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Type::class)->create(["name" => "Personal"]);
        factory(App\Type::class)->create(["name" => "Business"]);
    }
}