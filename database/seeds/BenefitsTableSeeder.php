<?php

use Illuminate\Database\Seeder;

class BenefitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Benefit::class)->create(["name" => "24 Hour Support"]);
        factory(App\Benefit::class)->create(["name" => "File Manager"]);
/*        factory(App\Benefit::class)->create(["name" => "Legacy Assist"]);
        factory(App\Benefit::class)->create(["name" => "Moving Home"]);
        factory(App\Benefit::class)->create(["name" => "Reduced Legal Fees"]);
        factory(App\Benefit::class)->create(["name" => "Gif Memberships"]); // question this one....*/
    }
}
