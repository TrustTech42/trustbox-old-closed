<?php

use Illuminate\Database\Seeder;

use App\Partner;


class UpdateBenefitPartnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(Partner::all() as $partner){
            $x = 0;

            foreach($partner->benefits as $benefit){
                $partner->benefits()->updateExistingPivot($benefit->id, ['sort_order' => $x]);
                $x++;
            }
        }
    }
}
