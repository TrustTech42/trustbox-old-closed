<?php

use Illuminate\Database\Seeder;

class BlocksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Block::class)->create([
            "name"       => "Text",
            "content"    => "<div class=\"container block block-text\"><div class=\"row\"><div class=\"col-8 offset-2\"><h1 class=\"h1 member-title\">Login / Register</h1><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</span><br></div></div></div>",
            "page_id"    => 2,
            "sort_order" => 1,
            "template"   => "block-text"
        ]);

        factory(App\Block::class)->create([
            "name"       => "Split",
            "content"    => "<div data-v-56e51f70=\"\" class=\"container block block-split py-4\"><div data-v-56e51f70=\"\" class=\"row\"><div data-v-56e51f70=\"\" class=\"col block-center\"><div data-v-56e51f70=\"\" class=\"a-center bg-light p-5 rounded\"><h1 data-v-56e51f70=\"\" class=\"h2 member-title\">Already a member? Log In Here</h1> <a data-v-56e51f70=\"\" href=\"login\" class=\"btn btn-primary mt-3\">Click Here</a></div></div> 
                            <div data-v-56e51f70=\"\" class=\"col block-center\"><div data-v-56e51f70=\"\" class=\"a-center bg-member text-white p-5 rounded\"><h1 data-v-56e51f70=\"\" class=\"h2\">New member? Register Here</h1> <a data-v-56e51f70=\"\" href=\"register\" class=\"btn btn-default bg-white text-member mt-3\">Click Here</a></div></div></div></div>",
            "page_id"    => 2,
            "sort_order" => 2,
            "template"   => "block-split"
        ]);
    }
}
