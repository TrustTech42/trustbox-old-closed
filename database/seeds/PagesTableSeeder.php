<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Page::class)->create(['name' => 'Home Page','url'  => '/']);
        factory(App\Page::class)->create(['name' => 'Login / Register','url'  => 'user-login']);
        factory(App\Page::class)->create(['name' => 'Terms & Conditions','url'  => 'terms_and_conditions']);
        factory(App\Page::class)->create(['name' => 'Privacy Policy','url'  => 'privacy_policy']);
    }
}
