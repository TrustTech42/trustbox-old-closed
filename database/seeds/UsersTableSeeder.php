<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'email'   => 'admin@trustbox.com',
            'role_id' => 2
        ]);

        factory(User::class)->create([
            'email'   => 'lewis@visionsharp.co.uk',
            'postcode' => 'm12jn'
        ]);

        factory(User::class)->create([
            'email'   => 'sam.mawhinney@lsgi.co.uk',
            'postcode' => 'm203jl'
        ]);
    }
}
