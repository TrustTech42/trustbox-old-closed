<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        $this->call(UsersTableSeeder::class);
//        $this->call(PagesTableSeeder::class);
//        $this->call(BlocksTableSeeder::class);
//        $this->call(PartnersTableSeeder::class);
//        $this->call(BenefitsTableSeeder::class);
//        $this->call(PostsTableSeeder::class);
//        $this->call(ServicesTableSeeder::class);
//        $this->call(PlansTableSeeder::class);
//        $this->call(MembershipTableSeeder::class);
//        $this->call(RolesTableSeeder::class);
//        $this->call(ProvidersTableSeeder::class);
//        $this->call(SettingsTableSeeder::class);
//        $this->call(BenefitPartnerTableSeeder::class);
//        $this->call(ReasonsTableSeeder::class);

          $this->call(UpdateBenefitPartnerSeeder::class);
    }
}
