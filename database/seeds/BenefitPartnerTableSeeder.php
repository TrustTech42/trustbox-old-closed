<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BenefitPartnerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('benefit_partner')->insert([
            'partner_id' => 2,
            'benefit_id' => 1
        ]);

        DB::table('benefit_partner')->insert([
            'partner_id' => 2,
            'benefit_id' => 2
        ]);
    }
}
