<?php

use Illuminate\Database\Seeder;

class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Plan::class)->create(['name' => 'lifetime']);
        factory(App\Plan::class)->create(['name' => 'gifted']);
        factory(App\Plan::class)->create(['name' => 'household']);
        factory(App\Plan::class)->create(['name' => 'trusted']);
        factory(App\Plan::class)->create(['name' => 'staff']);
    }
}
