<?php

use App\Partner;
use Illuminate\Database\Seeder;

class PartnersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Partner::class)->create([
            'name'      => 'default',
            'subdomain' => 'default'
        ]);

        factory(App\Partner::class)->create([
            'name'      => 'trustbox',
            'subdomain' => 'trustbox'
        ]);
    }
}
