<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Category::class)->create(["name" => "None"]);
        factory(App\Category::class)->create(["name" => "Estate Planning"]);
        factory(App\Category::class)->create(["name" => "Digital Agency"]);
    }
}