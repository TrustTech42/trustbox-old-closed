<?php

use Illuminate\Database\Seeder;

class ReasonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Reason::class)->create(['name' => 'reason 1']);
        factory(App\Reason::class)->create(['name' => 'reason 2']);
        factory(App\Reason::class)->create(['name' => 'reason 3']);
        factory(App\Reason::class)->create(['name' => 'reason 4']);
        factory(App\Reason::class)->create(['name' => 'reason 5']);
    }
}
