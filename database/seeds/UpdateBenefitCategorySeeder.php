<?php

use App\Benefit;
use Illuminate\Database\Seeder;

class UpdateBenefitCategorySeeder extends Seeder
{
    public function run()
    {
        foreach (Benefit::all() as $benefit) {
            $benefit->categories()->attach(1);
        }
    }
}