<?php

use Illuminate\Database\Seeder;

class UpdatePostCategorySeeder extends Seeder
{
    public function run()
    {
        foreach (\App\Post::all() as $post) {
            $post->categories()->attach(1);
        }
    }
}