<?php

use App\Benefit;
use Illuminate\Database\Seeder;

class UpdateBenefitTypeSeeder extends Seeder
{
    public function run()
    {
        foreach (Benefit::all() as $benefit) {
            $benefit->types()->attach(1);
        }
    }
}
