<?php

use Faker\Generator as Faker;

$factory->define(App\Category::class, function (Faker $faker, $attributes) {
    return [
        'name'   => $attributes['name'],
    ];
});
