<?php

use Faker\Generator as Faker;

$factory->define(App\Plan::class, function (Faker $faker, $attributes) {
    return [
        'name'   => $attributes['name'],
        'price'  => 0
    ];
});
