<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Page::class, function (Faker $faker, $attributes = []) {
    return [
        'name'    => $attributes['name'] ?? $faker->words(2, true),
        'url'     => $attributes['url'] ?? $faker->word(),
        'enabled' => false
    ];
});