<?php

use Faker\Generator as Faker;

$factory->define(App\Provider::class, function (Faker $faker, $attributes) {
    return [
        'name'    => $attributes['name'] ?? 'Trustbox',
        'user_id' => $faker->unique(true)->numberBetween(3, 100)
    ];
});
