<?php

use Faker\Generator as Faker;

$factory->define(App\Partner::class, function (Faker $faker, $attributes = []) {
    return [
        'user_id'        => 4,
        'name'           => $attributes['name'] ?? 'Trustbox',
        'primary_colour' => $attributes['primary_colour'] ?? '#21bcbb',
        'subdomain'      => $attributes['subdomain'] ?? 'trustbox',
        'address1'       => $faker->numberBetween(1, 100),
        'address2'       => $faker->streetName,
        'city'           => $faker->city,
        'postcode'       => $faker->postcode,
        'contact_email'  => $attributes['contact_email'] ?? 'admin@trustbox.com',
        'contact_phone'  => $attributes['contact_phone'] ?? '0161 697 3096'
    ];
});
