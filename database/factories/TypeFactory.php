<?php

use Faker\Generator as Faker;

$factory->define(App\Type::class, function (Faker $faker, $attributes) {
    return [
        'name'   => $attributes['name'],
    ];
});
