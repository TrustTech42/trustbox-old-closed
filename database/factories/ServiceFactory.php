<?php

use Faker\Generator as Faker;

$factory->define(App\Service::class, function (Faker $faker) {
    return [
        'name'                => $faker->words(4, true),
        'description'         => $faker->paragraphs(6, true),
        'boosted_description' => $faker->paragraphs(4, true),
        'banner'              => 'storage/banners/banner.jpg',
        'thumbnail'           => '/storage/thumbnails/thumbnail.jpg'
    ];
});
