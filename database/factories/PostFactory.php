<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'title'   => $faker->words(4, true),
        'content' => $faker->paragraphs(10, true),
        'banner'  => 'storage/banners/banner.jpg',
        'enabled' => true
    ];
});
