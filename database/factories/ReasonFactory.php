<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Reason::class, function (Faker $faker, $attributes = []) {
    return [
        'name'   => $attributes['name']
    ];
});
