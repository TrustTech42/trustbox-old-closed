<?php

use Faker\Generator as Faker;

$factory->define(App\Setting::class, function (Faker $faker) {
    return [
        'name' => 'default_partner',
        'value' => '1',
        'welcome_letter_content' => 'This is the welcome content',
        'invitation_letter_content' =>
            '<p style="margin-bottom: 0.28cm; line-height: 108%;">Hi @name@</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">We&rsquo;re writing to you today with some exciting news.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">@partner@ offer their customers an array of additional benefits and services through TrustBox. As a valued customer of @partner@, you&rsquo;re entitled to benefit from the @partner@ TrustBox, a carefully selected collection of benefits and services, designed to lighten the modern life load and help you sleep that little more soundly at night.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">Best of all, you&rsquo;re entitled to Lifetime Membership, absolutely <strong>free</strong>, with no strings attached.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;"><strong>About TrustBox</strong></p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">We know life changes quickly and that the legal, financial and health needs of you and your family are changing every day.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">That&rsquo;s why TrustBox has got your back, is in your corner, and on your side. TrustbBox have tested the market far and wide, to find the best providers of the services we all need in life. TrustBox providers are vetted by some of the best legal minds in the country so we know that the providers we choose are the best in the business. We share our favourite providers with you, like we would with our closest friends.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">Your TrustBox Lifetime Membership introduces you to a specially selected benefits suite from 24/7 Legal Support, to Legacy Planning, to Conveyancing, giving you access to the best of the best in each field.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;"><strong>Create your free TrustBox account</strong></p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">Creating your TrustBox account is really easy. Simply follow the link below and sign up with your email address and password. We&rsquo;ll guide you through the information we need from you.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">You&rsquo;ll then have access to a whole host of benefits for you and your family.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">code: @code@</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">If you need help, or you have any other questions, feel free to email <span style="color: #0563c1;"><u><a href="mailto:help@trustbox.com">help@trustbox.com</a></u></span> or call our customer service team on 0345 548 1230.<!-- Contact details tbc --></p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">Thanks,</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">The TrustBox team</p>',
        'signup_email_content' =>
            '<p style="margin-bottom: 0.28cm; line-height: 108%;">Hi @name@</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">This is just a quick confirmation email to let you know that you are now fully signed up for lifetime membership of TrustBox. This gives you, along with any family member in your household, lifetime access to a range of protective and cost saving benefits.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">Learn how your benefits can help you and your family members throughout some of life&rsquo;s major milestones. Browse your range of benefits by logging in to your TrustBox account here<!-- Insert link -->.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">If you need help, or you have any other questions, feel free to email <span style="color: #0563c1;"><u><a href="mailto:help@trustbox.com">help@trustbox.com</a></u></span> or call our customer service team on 0345 548 1230.<!-- Contact details tbc --></p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">Thanks,</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">The TrustBox team</p>',
        'boosted_email_content' =>
            '<p style="margin-bottom: 0.28cm; line-height: 108%;">Hi @name@</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">You&rsquo;ve just boosted a TrustBox benefit. Congratulations, you&rsquo;re now even more protected!</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">This is confirmation that the following benefit has been boosted</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">@benefit_name@</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">In addition to this benefit&rsquo;s existing features, you now have access to:</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">@benefit_details@</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">Learn how your benefits can help you and your family members throughout some of life&rsquo;s major milestones. Browse your range of benefits by logging in to your TrustBox account here<!-- Insert link -->.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">If you need help, or you have any other questions, feel free to email <span style="color: #0563c1;"><u><a href="mailto:help@trustbox.com">help@trustbox.com</a></u></span> or call our customer service team on 0345 548 1230.<!-- Contact details tbc --></p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">Thanks,</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">The TrustBox team</p>',
        'enrolled_email_content' =>
            '<p style="margin-bottom: 0.28cm; line-height: 108%;">Hi @name@</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">This is a quick email from TrustBox to let you know that you have just successfully enrolled to the following benefit.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">@benefit_name@</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">Learn how your benefits can help you and your family members throughout some of life&rsquo;s major milestones. Browse your range of benefits by logging in to your TrustBox account here<!-- Insert link -->.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">If you need help, or you have any other questions, feel free to email <span style="color: #0563c1;"><u><a href="mailto:help@trustbox.com">help@trustbox.com</a></u></span> or call our customer service team on 0345 548 1230.<!-- Contact details tbc --></p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">Thanks,</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">The TrustBox team</p>',
        'invite_email_content' =>
            '<p style="margin-bottom: 0.28cm; line-height: 108%;">Hi @name@</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">@user@ is a TrustBox member and as such they are entitled to &lsquo;gift&rsquo; membership, free of charge, to family members or friends. They want you to have access to the same protective and cost saving benefits they have access to. So, we&rsquo;re just emailing you some further information about TrustBox and to explain how to set up your account so you can benefit too.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;"><strong>About TrustBox</strong></p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">We know life changes quickly and that the legal, financial and health needs of you and your family are changing every day.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">That&rsquo;s why Trust Box has got your back, is in your corner, and on your side.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">We offer our members access to a carefully chosen collection of benefits and services, lightening the modern life load and helping you sleep that little bit more soundly at night. We share our favourite providers with you, like we would with our closest friends and because we&rsquo;ve tested the market far and wide, we know they&rsquo;re the best in the business.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">Trust Box Lifetime Membership introduces you to a specially selected benefits suite from 24/7 Legal Support, to Legacy Planning, to Conveyancing, giving you access to the best of the best in each field for the most affordable prices you can get (even better than going direct!)</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;"><strong>Create your TrustBox account</strong></p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">Creating your TrustBox account is really easy. Simply follow the link below and sign up with your email address and password. We&rsquo;ll then guide you through the information we need from you.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">If you need help, or you have any other questions, feel free to email <span style="color: #0563c1;"><u><a href="mailto:help@trustbox.com">help@trustbox.com</a></u></span> or call our customer service team on 0345 548 1230.<!-- Contact details tbc --></p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">Thanks,</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">The TrustBox team</p>',
        'removed_email_content' =>
            '<p style="margin-bottom: 0.28cm; line-height: 108%;">Hi @name@</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">@user@ has requested that you be removed from their list of &lsquo;gifted members&rsquo;. We&rsquo;re sorry to see you go!</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">But fear not. You don&rsquo;t need to lose access to all your amazing TrustBox benefits, we can set you up your own personal TrustBox account.<!-- I’m not sure whether this will be the exact placeholder but you can add this in as a placeholder and we can discuss with Claire/Simon. --></p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">Simply contact our customer service team by <span style="color: #0563c1;"><u><a href="mailto:help@trustbox.com">help@trustbox.com</a></u></span> or call us directly on 0345 548 1230.<!-- Contact details tbc --></p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">Thanks,</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">The TrustBox team</p>',
        'welcome_email_content' =>
            '<p style="margin-bottom: 0.28cm; line-height: 17.28px;">Hi @first_name@</p>
            <p style="margin-bottom: 0.28cm; line-height: 17.28px;">Congratulations. You&rsquo;re now a fully-fledged member of @partner@ TrustBox and have access to a collection of benefits and services to lighten the modern life load and help you sleep that little more soundly at night.</p>
            <p style="margin-bottom: 0.28cm; line-height: 17.28px;">Your TrustBox Lifetime Membership gives you, along with any family member in your household, lifetime access to a range of protective and cost saving benefits.</p>
            <p style="margin-bottom: 0.28cm; line-height: 17.28px;">Learn how your benefits can help you and your family members throughout some of life&rsquo;s major milestones. To browse your benefits and learn how you can access them, visit the @partner@ TrustBox website here.</p>
            <p style="margin-bottom: 0.28cm; line-height: 17.28px;"><strong>Your Trusted People</strong></p>
            <p style="margin-bottom: 0.28cm; line-height: 17.28px;">Don&rsquo;t forget to add your &lsquo;Trusted People&rsquo;. Your Trusted People have access to your personal benefit information and digital filing cabinet. If you ever need someone to look after your affairs, for example if you pass away or are incapacitated, they have the support of our team and can access all the information they will need to make the process stress free.</p>
            <p style="margin-bottom: 0.28cm; line-height: 17.28px;">They also benefit from the same protective and cost saving benefits you have access to, which their family can benefit from too!</p>
            <p style="margin-bottom: 0.28cm; line-height: 17.28px;"><a name="_GoBack"></a>Log in to your account here to add your Trusted People</p>
            <p style="margin-bottom: 0.28cm; line-height: 17.28px;">If you need help, or you have any other questions, feel free to email&nbsp;<span style="color: #0563c1;"><u><a href="mailto:help@trustbox.com">help@trustbox.com</a></u></span>&nbsp;or call our customer service team on 0345 548 1230.</p>
            <p style="margin-bottom: 0.28cm; line-height: 17.28px;">Thanks,</p>
            <p style="margin-bottom: 0.28cm; line-height: 17.28px;">The TrustBox team</p>
            <p>&nbsp;</p>',
        'code_invite_email_content' =>
            '<div style="margin-bottom: 0.28cm; line-height: 108%;">Hi @name@</div>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">We&rsquo;re writing to you today with some exciting news.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">@partner@ offer their customers an array of additional benefits and services through TrustBox. As a valued customer of @partner@, you&rsquo;re entitled to benefit from the @partner@ TrustBox, a carefully selected collection of benefits and services, designed to lighten the modern life load and help you sleep that little more soundly at night.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">Best of all, you&rsquo;re entitled to Lifetime Membership, absolutely <strong>free</strong>, with no strings attached.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;"><strong>About TrustBox</strong></p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">We know life changes quickly and that the legal, financial and health needs of you and your family are changing every day.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">That&rsquo;s why TrustBox has got your back, is in your corner, and on your side. TrustbBox have tested the market far and wide, to find the best providers of the services we all need in life. TrustBox providers are vetted by some of the best legal minds in the country so we know that the providers we choose are the best in the business. We share our favourite providers with you, like we would with our closest friends.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">Your TrustBox Lifetime Membership introduces you to a specially selected benefits suite from 24/7 Legal Support, to Legacy Planning, to Conveyancing, giving you access to the best of the best in each field.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;"><strong>Create your free TrustBox account</strong></p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">Creating your TrustBox account is really easy. Simply follow the link below and sign up with your email address and password. We&rsquo;ll guide you through the information we need from you.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">You&rsquo;ll then have access to a whole host of benefits for you and your family.</p>
            <p>Your family code: <code>@code@</code></p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">If you need help, or you have any other questions, feel free to email <span style="color: #0563c1;"><u><a href="mailto:help@trustbox.com">help@trustbox.com</a></u></span> or call our customer service team on 0345 548 1230.<!-- Contact details tbc --></p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">Thanks,</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">The TrustBox team</p>',
        'benefit_notify_content' =>
            '<p style="margin-bottom: 0.28cm; line-height: 108%;">Hi @name@</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">You&rsquo;ve just boosted a TrustBox benefit. Congratulations, you&rsquo;re now even more protected!</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">This is confirmation that the following benefit has been boosted</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">@benefit_name@</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">In addition to this benefit&rsquo;s existing features, you now have access to:</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">@benefit_details@</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">Learn how your benefits can help you and your family members throughout some of life&rsquo;s major milestones. Browse your range of benefits by logging in to your TrustBox account here<!-- Insert link -->.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">If you need help, or you have any other questions, feel free to email <span style="color: #0563c1;"><u><a href="mailto:help@trustbox.com">help@trustbox.com</a></u></span> or call our customer service team on 0345 548 1230.<!-- Contact details tbc --></p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">Thanks,</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">The TrustBox team</p>',
        'benefit_form_content' =>
            '<p style="margin-bottom: 0.28cm; line-height: 108%;">Hi @provider@</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">You&rsquo;ve just boosted a TrustBox benefit. Congratulations, you&rsquo;re now even more protected!</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">This is confirmation that the following benefit has been boosted</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">@benefit_name@</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">In addition to this benefit&rsquo;s existing features, you now have access to:</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">@benefit_details@</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">Learn how your benefits can help you and your family members throughout some of life&rsquo;s major milestones. Browse your range of benefits by logging in to your TrustBox account here<!-- Insert link -->.</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">If you need help, or you have any other questions, feel free to email <span style="color: #0563c1;"><u><a href="mailto:help@trustbox.com">help@trustbox.com</a></u></span> or call our customer service team on 0345 548 1230.<!-- Contact details tbc --></p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">Thanks,</p>
            <p style="margin-bottom: 0.28cm; line-height: 108%;">The TrustBox team</p>'
    ];
});
