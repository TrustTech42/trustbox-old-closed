<?php

use Faker\Generator as Faker;

$factory->define(App\Block::class, function (Faker $faker) {
    return [
        "name"       => "Text Block",
        "page_id"    => 1,
        "template"   => "block-text",
        "content"    => "<div class='container block block-text'>
                        <div class='row'>
                            <div class='col'>
                                <h1 class='h1 partner-title'>The Block Title Goes Here</h1>
                                There be some html here.
                            </div>
                        </div>
                    </div>",
        "sort_order" => 1
    ];
});
