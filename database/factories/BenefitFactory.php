<?php

use Faker\Generator as Faker;

$factory->define(App\Benefit::class, function (Faker $faker, $attributes = []) {
    return [
        'name'                => $attributes['name'] ?? $faker->words(4, true),
        'description'         => $faker->paragraphs(6, true),
        'boosted_description' => $faker->paragraphs(4, true),
        'banner'              => 'storage/banners/banner.jpg',
        'thumbnail'           => '/storage/thumbnails/thumbnail.jpg',
        'price'               => $faker->numberBetween(3, 10),
        'provider_id'         => 2,
        'unique'              => 0
    ];
});
