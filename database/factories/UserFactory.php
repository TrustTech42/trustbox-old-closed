<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker, $attributes = []) {
    return [
        'name' => $faker->name,
        'email' => $attributes['email'] ?? $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
        'remember_token' => Str::random(10),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'address1' => $faker->numberBetween(1, 10),
        'address2' => $faker->streetName,
        'city' => $faker->city,
        'postcode' => $attributes['postcode'] ?? $faker->postcode,
        'telephone' => $faker->phoneNumber,
        'mobile' => '07572398259',
        'role_id' => $attributes['role_id'] ?? 1,
        'customer_number' => "C" . rand(100000000, 999999999),
        'date_of_birth' => $faker->date(),
        'storage' => 5,
        'api_token' => Str::random()
    ];
});
