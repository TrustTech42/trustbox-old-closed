<?php

use Faker\Generator as Faker;

$factory->define(App\Membership::class, function (Faker $faker, $attributes) {
    return [
        'user_id'    => 2,
        'partner_id' => 2,
        'plan_id'    => 1,
        'activated'  => true
    ];
});
