const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .js('resources/js/frontend.js', 'public/js')
    .js('resources/js/admin.js', 'public/js')
    .sass('resources/sass/frontend.scss', 'public/css')
    .sass('resources/sass/admin.scss', 'public/css')
     .browserSync({
         proxy: 'http://openmoney.trustbox.test',
         open : 'true',
         injectChanges: true
     }).version();
