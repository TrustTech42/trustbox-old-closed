-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: trustbox-db2
-- Generation Time: Feb 24, 2021 at 02:46 PM
-- Server version: 5.7.22
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `roottrus_filemanager`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question_id` int(11) NOT NULL,
  `collection_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `collections`
--

CREATE TABLE `collections` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_id` int(11) NOT NULL,
  `notification_period` date DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `collection_folder`
--

CREATE TABLE `collection_folder` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `collection_id` int(11) NOT NULL,
  `folder_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `collection_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `folders`
--

CREATE TABLE `folders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `icon_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default',
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `folders`
--

INSERT INTO `folders` (`id`, `name`, `user_id`, `icon_id`, `image`, `type`, `enabled`, `created_at`, `updated_at`) VALUES
(1, 'End of Life', 0, 11, '', 'default', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(2, 'consequuntur esse', 0, 7, '', 'default', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(3, 'quia iusto', 0, 2, '', 'default', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(4, 'veritatis voluptas', 0, 5, '', 'default', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(5, 'dolorem dicta', 0, 4, '', 'default', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(6, 'expedita earum', 0, 1, '', 'default', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(7, 'numquam porro', 0, 9, '', 'default', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(8, 'reiciendis blanditiis', 0, 12, '', 'default', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(9, 'officia earum', 0, 10, '', 'default', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(10, 'laborum earum', 0, 8, '', 'default', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24');

-- --------------------------------------------------------

--
-- Table structure for table `folder_type`
--

CREATE TABLE `folder_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_id` int(11) NOT NULL,
  `folder_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `folder_type`
--

INSERT INTO `folder_type` (`id`, `type_id`, `folder_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 2, 1, NULL, NULL),
(3, 3, 1, NULL, NULL),
(4, 4, 1, NULL, NULL),
(5, 5, 1, NULL, NULL),
(6, 6, 1, NULL, NULL),
(7, 7, 1, NULL, NULL),
(8, 8, 1, NULL, NULL),
(9, 9, 1, NULL, NULL),
(10, 10, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `icons`
--

CREATE TABLE `icons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `class` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `icons`
--

INSERT INTO `icons` (`id`, `class`, `name`, `enabled`, `created_at`, `updated_at`) VALUES
(1, 'fal fa-abacus', '  Abacus', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(2, 'fal fa-acorn', '  Acorn', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(3, 'fal fa-ad', '  Ad', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(4, 'fal fa-address-book', '  Address Book', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(5, 'fal fa-address-card', '  Address Card', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(6, 'fal fa-adjust', '  Adjust', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(7, 'fal fa-air-freshener', '  Air Freshener', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(8, 'fal fa-alarm-clock', '  Alarm Clock', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(9, 'fal fa-alarm-exclamation', '  Alarm Exclamation', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(10, 'fal fa-alarm-plus', '  Alarm Plus', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(11, 'fal fa-alarm-snooze', '  Alarm Snooze', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(12, 'fal fa-album', '  Album', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(13, 'fal fa-album-collection', '  Album Collection', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(14, 'fal fa-alicorn', '  Alicorn', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(15, 'fal fa-align-center', '  Align Center', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(16, 'fal fa-align-justify', '  Align Justify', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24');

-- --------------------------------------------------------

--
-- Table structure for table `inboxes`
--

CREATE TABLE `inboxes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filesize` int(11) DEFAULT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `resolved` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `first_name`, `last_name`, `mobile`, `email`, `subject`, `message`, `resolved`, `created_at`, `updated_at`) VALUES
(1, 'Rosendo', 'Streich', '415-475-1248 x05348', 'tromp.florida@ruecker.com', 'ducimus iure minima', 'Dolorum animi molestiae nam. Eligendi non ad in qui non. Nulla excepturi rerum est dicta ut voluptate quia. Asperiores magni expedita odio.\n\nUt quasi vel pariatur porro culpa rerum debitis dolorem. Qui accusantium nesciunt reprehenderit.\n\nId fugiat accusantium consequatur aut deserunt. Dolore excepturi impedit porro sunt. Ipsum et nemo odio est animi accusamus. Hic excepturi voluptatum non.\n\nEt dolorem quos necessitatibus nulla magnam. Blanditiis quaerat fugit eveniet fugiat qui rerum. Molestiae autem sed vero architecto. Similique tempore atque ex atque et in nisi.', 0, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(2, 'Nikolas', 'Runte', '1-338-582-8031', 'alexandra57@yahoo.com', 'adipisci et rerum', 'In doloribus quisquam ipsa eligendi iure. Voluptate ipsam natus quam unde. Possimus enim eaque atque aspernatur at et. Quia aperiam non in fugit quam rerum.\n\nNihil eum soluta dolorem. Repellendus voluptas sint repudiandae quia sequi itaque voluptatem aut. Quia ut dolorem ipsa sint earum repudiandae voluptas.\n\nFugiat officiis dolores consequatur nam sunt deserunt tenetur. Ex ut mollitia unde nihil.\n\nRerum pariatur facilis quas qui. Earum saepe modi vero quo repellendus cupiditate. Magni labore in corporis. Tempora velit ad sed dolorem saepe libero architecto.', 0, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(3, 'Vaughn', 'Tillman', '623-219-7225 x99671', 'isac.vonrueden@lehner.com', 'occaecati hic est', 'Dolor quia quis consequuntur eum officia. Nesciunt voluptas similique eum. Eaque in enim aut nihil quia facilis perspiciatis. Quis molestiae similique repudiandae.\n\nTenetur et ullam necessitatibus fugiat sit minus et. Aliquam illo sit porro necessitatibus et et est sed. Odio corporis facilis earum numquam atque dolores temporibus. Quia ratione laudantium tempora aut temporibus voluptatum.\n\nUllam asperiores voluptate dolores. Quis sequi id consequatur perspiciatis repudiandae dolorem dolor. Iste doloribus repellat ut quo. Est aliquam rerum facere sequi tenetur nihil veniam placeat.\n\nUt ducimus exercitationem eos sit. Sunt qui assumenda pariatur quos fuga architecto et. Voluptatum ipsa quisquam dolor. Est id blanditiis saepe rerum est qui.', 0, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(4, 'Corrine', 'Mertz', '260.517.3020 x274', 'tillman.robyn@franecki.com', 'aperiam consequatur dicta', 'Blanditiis est laudantium recusandae placeat sit facere. Reiciendis consectetur est tenetur repellendus officiis libero. Voluptas quo sint aspernatur doloremque.\n\nPerferendis aliquid velit veniam expedita cumque. Odio impedit consequatur molestias. Ut quis animi laboriosam velit.\n\nEius aut ea aut dolore ipsam est. Aut animi maiores provident provident rerum et. Ex voluptas velit velit aspernatur odit corporis adipisci.\n\nModi magnam a facere et sint neque omnis ut. Voluptas magni repudiandae ea veritatis. Est consequatur fuga non ratione quidem id.', 0, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(5, 'Muriel', 'Schmidt', '+1-852-854-3188', 'rod.ward@heidenreich.com', 'ratione autem rerum', 'Earum error doloribus cumque commodi autem reprehenderit voluptatem. Delectus et cupiditate vel ipsa incidunt vel nemo. Distinctio qui dolor architecto molestiae cum et.\n\nUt non et et error consectetur ea. Repudiandae ipsa quo vero nihil dolor praesentium. Et et omnis eos nulla.\n\nBeatae qui non ex fugit repellendus. Quis in quis quod pariatur quisquam dicta dignissimos iste. Qui voluptas ipsam vel sint dolores incidunt tenetur. Nam officia aut cumque voluptatem qui.\n\nInventore modi placeat blanditiis et. Sint molestiae eum voluptatem quidem et labore. Magnam quae dolor debitis est.', 0, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(6, 'Carlo', 'Kling', '+1.440.361.9328', 'arobel@gmail.com', 'nesciunt sapiente est', 'Voluptatem inventore voluptatibus dolorem rerum omnis blanditiis dolorem. Assumenda voluptatum distinctio dolor quia asperiores. Provident dolores assumenda itaque accusantium incidunt.\n\nEt dolor temporibus consequatur et nobis hic et iste. Odit quibusdam aut quam laudantium. Omnis cupiditate culpa vero voluptas omnis corrupti ut dolorem.\n\nQui doloremque qui quis occaecati suscipit atque. Rerum quibusdam iste velit nihil et. Officia voluptatem ullam asperiores deserunt quia. Tempora consectetur sunt illum eos.\n\nConsectetur quas soluta quas iusto. Ullam voluptatem sit sit animi dolor ea eveniet. Facere eos temporibus ipsam.', 0, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(7, 'Marina', 'Bernier', '447.625.3021 x743', 'weissnat.josephine@tromp.com', 'ut repellendus et', 'Consequuntur nostrum aut est et excepturi porro. Sit et explicabo aut dolores et rem voluptatem. Quia voluptatem saepe beatae ullam. Et impedit quibusdam dolor laudantium cupiditate.\n\nPerspiciatis praesentium quia eos consequatur accusantium. Sequi vel vitae totam et et ut. Quia fugiat quia quo veniam debitis blanditiis quia voluptas.\n\nNihil recusandae rerum corporis enim dolorem dicta. Quia qui maiores et non nesciunt rerum. Fugit soluta perferendis esse voluptatibus magni rerum architecto ducimus. Pariatur veritatis quasi et ut sed ut hic.\n\nVoluptatum asperiores dolore pariatur quia repudiandae soluta. Hic eligendi recusandae repellendus. Doloremque libero aut voluptas placeat aut laboriosam sed cum. Reiciendis assumenda reiciendis nihil.', 0, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(8, 'Vita', 'Reilly', '910-319-2886 x6446', 'dcollins@quigley.biz', 'in optio magnam', 'Sed magnam et beatae perspiciatis. Fugiat repellat nobis harum omnis. Fugit soluta accusantium vel.\n\nIncidunt est voluptatem est corporis assumenda. Id sed unde sit laudantium consequatur quisquam saepe.\n\nOptio exercitationem cum voluptates voluptas aut. Laboriosam vitae quisquam esse provident vel ullam voluptates. Sint dolores pariatur consequatur aut quo rerum praesentium. Occaecati ut autem eius eos.\n\nAmet sunt sit dolores aut voluptatem quasi. Rerum sapiente natus vero et voluptatibus. Omnis neque possimus expedita cupiditate. Quod officiis sit in adipisci eos.', 0, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(9, 'Ari', 'Dibbert', '+14798184972', 'hhammes@langosh.com', 'voluptas dolorem repellat', 'Vero vel eius eum aut. Sed hic et totam officia rerum. Quas fugiat voluptatum blanditiis ab expedita molestias.\n\nNihil quasi quia quo ducimus sed. Ipsum autem ipsam natus vel.\n\nSed cumque ut eum rerum fugiat mollitia eligendi. Vero quam aut aliquam pariatur quia explicabo. Sapiente dolorem quia non velit itaque quia. Explicabo dolorum maiores magnam aspernatur consequatur ut. Cupiditate quia odit ut.\n\nProvident dolore dolores et non qui tempore corporis explicabo. Porro optio laborum pariatur nobis quaerat aliquid magni. Quis eveniet sit magnam aut ut maxime veritatis.', 0, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(10, 'Roel', 'Wiza', '1-450-735-7616', 'albina.mann@gmail.com', 'omnis sed dignissimos', 'Et voluptas tenetur sunt sed commodi eveniet. Ex est temporibus iste minus libero odit illum iusto. Est dolorum et voluptas facilis unde non. Veritatis in est id molestiae explicabo praesentium.\n\nConsequatur excepturi recusandae quis qui. Illum minus odio aut est ducimus. Laudantium sequi ducimus sunt architecto.\n\nNostrum nihil consequuntur nobis minus minus. Pariatur facere aperiam totam voluptates aut dolorem. Beatae aut eos sunt veritatis. Porro et perspiciatis non laudantium nulla deserunt quae.\n\nIn aspernatur placeat qui unde enim nesciunt. Sint amet distinctio nam laborum. Et eos nihil porro nostrum et. Explicabo consequatur placeat vel velit.', 0, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(11, 'Karianne', 'Lueilwitz', '946.897.4272 x829', 'adolf13@becker.com', 'deserunt doloremque est', 'Sit velit ipsa est molestiae perspiciatis id. Earum dolorum minima quidem sit sed facilis. Consectetur sed velit harum et reprehenderit aspernatur.\n\nCulpa molestiae aspernatur temporibus. Asperiores consequatur modi non sint culpa ipsum ut. Et qui labore cupiditate.\n\nAsperiores aut incidunt et in numquam. Alias qui debitis maiores. Explicabo ea dolore aspernatur sed rerum. Sunt ut sint quod.\n\nCupiditate velit ut aut. Voluptates ut et minima dolor doloribus error nisi officiis. Ipsa sed culpa nemo nemo.', 0, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(12, 'Robin', 'Mayert', '1-763-651-8988 x715', 'palma.denesik@yahoo.com', 'asperiores possimus incidunt', 'Inventore delectus voluptate odio accusantium odio. Eos nam voluptatem rem inventore quos magnam.\n\nHarum qui et nihil praesentium. In est esse id enim doloribus. Dolore beatae et ut dolorem ut officiis eveniet.\n\nFugiat beatae nisi voluptas voluptatem iusto. Qui sed aperiam qui ab cum voluptatem. Omnis dicta quas voluptas quia et quaerat.\n\nVel dolor fuga beatae corporis. Pariatur nisi est soluta nulla pariatur.', 0, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(13, 'Jensen', 'Lynch', '(931) 477-6021 x65906', 'major62@feest.info', 'ratione amet cum', 'Est dignissimos laborum et vitae architecto eius magnam fugit. Ratione voluptates accusantium molestiae porro corrupti.\n\nDolore sint ducimus ut. Eveniet aut quia molestias illo at dolore voluptates. Magni tempora et voluptatum. Culpa iure enim consequatur aperiam dignissimos.\n\nReprehenderit optio nihil dolor at est vel. Nesciunt excepturi perferendis ab dignissimos. Pariatur neque ullam id maiores ut consequatur et.\n\nVitae itaque dolores officiis animi maiores reprehenderit delectus. Est ut reprehenderit cum qui debitis quia nostrum. Voluptates laboriosam ipsa voluptatem iusto dicta veniam. Aliquam a ut blanditiis eos et est.', 0, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(14, 'Gustave', 'Champlin', '1-881-683-3823 x3159', 'greichel@krajcik.com', 'possimus reiciendis quia', 'Molestiae dolorum vero et perferendis. Optio ipsum numquam et non. Nisi qui tempora totam cumque dolorum nemo hic.\n\nVoluptatibus necessitatibus necessitatibus optio dolor sed. Magnam asperiores doloremque et autem. Sequi qui quidem expedita enim officiis voluptatem consequatur facere.\n\nDeleniti ut ipsam sequi mollitia. Voluptatum ut et deleniti qui. Nisi pariatur nisi facere suscipit harum enim doloremque. Qui omnis dolorem sit eligendi laudantium. Deserunt quo est nemo deserunt.\n\nFugiat reprehenderit tempora quia eligendi molestiae quia. Eligendi quos beatae inventore commodi qui perspiciatis. Id aut debitis molestias aut ipsum similique sed. Hic et nisi et non et. Veritatis nulla provident saepe aliquam est.', 0, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(15, 'Roxane', 'Pacocha', '282.552.7538', 'auer.mariana@green.org', 'atque quibusdam est', 'Quaerat nam quisquam ut perspiciatis quia. Ipsum ex vel maiores a sint totam quaerat. Suscipit distinctio voluptas ex vero ut est temporibus. Quae at sunt eos culpa iure dolorum ut.\n\nItaque et sit possimus ad corrupti dolor. Quia illum voluptas velit illo et labore. Ipsam optio consequatur quae quis accusantium quo. Repellat ut aperiam quae qui nobis vitae nesciunt.\n\nAccusantium officia fuga enim. Dolorem ab ex quaerat fugiat deleniti qui aliquid. Quae enim consequatur perferendis id modi facere dolor.\n\nNemo sed dignissimos porro quae error facilis id voluptas. Qui modi maiores sit tempora rem corrupti aperiam. Voluptatem ut voluptatem aut sunt quisquam. Voluptatem repellendus aspernatur nostrum.', 0, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(16, 'Catalina', 'Bartoletti', '+1-213-362-6185', 'odare@gmail.com', 'delectus voluptatem omnis', 'Nostrum assumenda cumque ullam exercitationem. Reiciendis itaque ut ipsam dicta. Rem recusandae aut eos dolorem est.\n\nEt officiis molestiae in occaecati quibusdam dolorum unde. Omnis magni sit dolores aut excepturi. Delectus beatae aut et sed voluptatem est praesentium.\n\nEum autem iste ut deserunt in natus. Tempore quia repellendus fuga quaerat porro repellendus corrupti perferendis. Unde et eos dolor omnis. Cumque et nisi saepe incidunt qui dolorum occaecati harum.\n\nEt quasi est quasi aut et. Cupiditate sint reprehenderit velit aut. Quam et architecto modi. Hic autem non architecto.', 0, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(17, 'Krystina', 'Schmitt', '+1-265-552-7469', 'flowe@hotmail.com', 'rerum voluptate laborum', 'Id voluptatum et excepturi est repudiandae saepe autem voluptatem. Voluptas sunt quidem tempore saepe. Iste quibusdam et ea nulla recusandae. Ut sit fugiat qui repellat laboriosam a.\n\nDignissimos ea sunt ex pariatur. Hic rem reprehenderit corrupti. Quisquam corporis ipsa voluptatem soluta quia sed occaecati eius.\n\nNesciunt fuga ut facilis qui et ullam eum. Ad et amet soluta animi et reiciendis. Non soluta adipisci commodi dolore vel debitis et. Dolore voluptatem consequatur quis qui praesentium ipsa et voluptatum.\n\nDistinctio odit in officia dolores nemo quis eius quasi. Et maxime sint cupiditate atque architecto et sit cum. Exercitationem saepe accusantium autem cum.', 0, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(18, 'Furman', 'Corkery', '401-832-3627 x335', 'ptromp@mccullough.com', 'in et quisquam', 'Consequatur corrupti laboriosam temporibus qui. Aspernatur illum quis ut tempore sint saepe est. Officia quasi dicta id tempora earum. Libero sapiente reprehenderit atque dignissimos sint.\n\nIpsa repudiandae occaecati fugit quasi sunt. Voluptate accusamus eum repellendus eum sunt voluptatum rerum nesciunt. Commodi fuga deserunt quo.\n\nQuia repellat vero consequuntur quia. Aut et esse illo delectus sed ipsum est. Enim provident id consequatur dolor.\n\nDolore necessitatibus nihil porro labore. Accusantium labore magni minima velit non consectetur quia. Sapiente quo labore autem aperiam.', 0, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(19, 'Alfredo', 'Prohaska', '1-607-364-4902 x205', 'jacobson.devin@dietrich.com', 'quis ducimus pariatur', 'Qui nam alias repellendus quo enim. Est eum odio fugit quas ullam accusamus.\n\nVoluptas beatae odio consequatur dolorem amet. Sequi facere tempore quae nobis officia tempore ipsum. Suscipit est optio sint alias quas id alias. Doloremque eum molestiae mollitia doloribus ut.\n\nPossimus omnis voluptatem ipsum iusto minus dolorem sit. Rerum placeat repellat qui magni quia quo sunt. Voluptas sed ea sapiente perferendis officia possimus ducimus. Quisquam modi et nihil doloribus praesentium porro voluptas amet.\n\nExpedita recusandae ratione ipsa possimus excepturi sunt. Voluptas perspiciatis odit ut fugit et. Architecto optio est nemo ipsa magni odit qui.', 0, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(20, 'Audie', 'Sporer', '(343) 598-7234 x0561', 'mlemke@marvin.com', 'cum autem sit', 'Cumque reprehenderit dolorum et voluptas magni ut. Excepturi impedit fuga odio architecto.\n\nAut consequatur velit assumenda laudantium. Dolorem accusantium architecto totam eos repellat. Sit cumque sed labore qui ea sed magnam. Iusto et debitis dolores iusto.\n\nConsectetur quibusdam hic architecto libero maxime. Magnam ex doloremque perspiciatis doloremque quia.\n\nAt aut sit nihil nam possimus nesciunt. Eligendi quae rerum molestiae error consequatur sed sit. Vel consectetur ipsum necessitatibus et fuga. Dolore sed et id officiis nisi. Odit esse iure consequuntur in accusantium.', 0, '2021-02-24 14:38:24', '2021-02-24 14:38:24');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(18, '2014_10_12_000000_create_users_table', 1),
(19, '2014_10_12_100000_create_password_resets_table', 1),
(20, '2019_08_19_000000_create_failed_jobs_table', 1),
(21, '2019_11_04_173416_create_folders_table', 1),
(22, '2019_11_05_130501_create_icons_table', 1),
(23, '2019_11_05_161601_create_collections_table', 1),
(24, '2019_11_05_164201_create_collection_folder_table', 1),
(25, '2019_11_05_181515_create_types_table', 1),
(26, '2019_11_07_122709_create_files_table', 1),
(27, '2019_11_07_160926_create_messages_table', 1),
(28, '2019_11_07_170639_create_shares_table', 1),
(29, '2019_11_11_154128_create_questions_table', 1),
(30, '2019_11_11_154522_create_question_type_table', 1),
(31, '2019_11_18_110322_create_folder_type_table', 1),
(32, '2019_11_27_171417_create_trusted_folder_table', 1),
(33, '2019_12_11_163420_create_inboxes_table', 1),
(34, '2019_12_18_150850_create_answers_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `text`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Expiry Date', 'date', '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(2, 'ut rerum dolores', 'text', '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(3, 'sunt ut perferendis', 'date', '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(4, 'corrupti earum ea', 'date', '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(5, 'nesciunt dolores iure', 'text', '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(6, 'sint adipisci quisquam', 'text', '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(7, 'explicabo necessitatibus optio', 'text', '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(8, 'id ut quia', 'date', '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(9, 'nesciunt excepturi accusamus', 'date', '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(10, 'voluptate ducimus fugit', 'date', '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(11, 'sunt vel ut', 'text', '2021-02-24 14:38:24', '2021-02-24 14:38:24');

-- --------------------------------------------------------

--
-- Table structure for table `question_type`
--

CREATE TABLE `question_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `question_type`
--

INSERT INTO `question_type` (`id`, `question_id`, `type_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 2, 1, NULL, NULL),
(3, 3, 1, NULL, NULL),
(4, 4, 1, NULL, NULL),
(5, 5, 1, NULL, NULL),
(6, 6, 1, NULL, NULL),
(7, 7, 1, NULL, NULL),
(8, 8, 1, NULL, NULL),
(9, 9, 1, NULL, NULL),
(10, 10, 1, NULL, NULL),
(11, 11, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shares`
--

CREATE TABLE `shares` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `collection_id` int(11) NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `trusted_folder`
--

CREATE TABLE `trusted_folder` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `trusted_id` int(11) NOT NULL,
  `folder_id` int(11) NOT NULL,
  `access` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE `types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`id`, `name`, `enabled`, `created_at`, `updated_at`) VALUES
(1, 'Death Certificate', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(2, 'ut', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(3, 'voluptates', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(4, 'praesentium', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(5, 'nemo', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(6, 'officia', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(7, 'illum', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(8, 'voluptas', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(9, 'dignissimos', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24'),
(10, 'porro', 1, '2021-02-24 14:38:24', '2021-02-24 14:38:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `collections`
--
ALTER TABLE `collections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `collection_folder`
--
ALTER TABLE `collection_folder`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `folders`
--
ALTER TABLE `folders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `folder_type`
--
ALTER TABLE `folder_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `icons`
--
ALTER TABLE `icons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inboxes`
--
ALTER TABLE `inboxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_type`
--
ALTER TABLE `question_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shares`
--
ALTER TABLE `shares`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trusted_folder`
--
ALTER TABLE `trusted_folder`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `collections`
--
ALTER TABLE `collections`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `collection_folder`
--
ALTER TABLE `collection_folder`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `folders`
--
ALTER TABLE `folders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `folder_type`
--
ALTER TABLE `folder_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `icons`
--
ALTER TABLE `icons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `inboxes`
--
ALTER TABLE `inboxes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `question_type`
--
ALTER TABLE `question_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `shares`
--
ALTER TABLE `shares`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trusted_folder`
--
ALTER TABLE `trusted_folder`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `types`
--
ALTER TABLE `types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
