-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 22, 2021 at 03:22 PM
-- Server version: 5.7.33
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `roottrus_trustbox_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `benefits`
--

CREATE TABLE `benefits` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `page_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `boosted_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `redeem_text` longtext COLLATE utf8mb4_unicode_ci,
  `includes_file_manager` tinyint(1) NOT NULL DEFAULT '0',
  `includes_benefit_phone_button` tinyint(1) NOT NULL DEFAULT '0',
  `includes_benefit_form` tinyint(1) NOT NULL DEFAULT '0',
  `provider_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_description` text COLLATE utf8mb4_unicode_ci,
  `provider_usp` text COLLATE utf8mb4_unicode_ci,
  `unique` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `partner_percentage_cut` int(11) DEFAULT NULL,
  `provider_percentage_cut` int(11) DEFAULT NULL,
  `provider_telephone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_nochex_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `benefits`
--

INSERT INTO `benefits` (`id`, `page_id`, `name`, `description`, `boosted_description`, `thumbnail`, `banner`, `price`, `provider_id`, `featured`, `redeem_text`, `includes_file_manager`, `includes_benefit_phone_button`, `includes_benefit_form`, `provider_email`, `provider_description`, `provider_usp`, `unique`, `created_at`, `updated_at`, `partner_percentage_cut`, `provider_percentage_cut`, `provider_telephone`, `provider_nochex_id`) VALUES
(2, NULL, 'Digital Vault', '<p><span lang=\"EN-US\">The Digital Vault is the perfect place to store copies of your Attested Will and all sorts of other personal, private, and sensitive documents.</span></p>\r\n<p>Here you can create your own secure filing cabinet and arrange digital versions of your documents accessing them whenever you want via the &lsquo;Vault&rsquo; on your computer or via your app.</p>\r\n<p>When you need to, you can safely share documents with trusted people on a one-time basis or allow then to access certain files at the time of your choosing, so you know they can look after your affairs when they need.</p>', '<p>&nbsp; &nbsp;</p>', '/storage/thumbnails/JqkLT1HlcxdLFMO8xSFtchUWkAFrXLdydsTrg7W9.png', 'storage/banners/NaId7XEyhlG38tpqNjnY07fk5eF2ZjDEQIWM5oG1.jpeg', NULL, '2', '0', NULL, 0, 0, 0, 'info@yourtrustbox.co.uk', NULL, '<p>Being certain vital documents, such as your Will, can be kept safe and secure but accessible to those you trust at the time when they will need to access them can bring peace of mind and certainty that your wishes will be respected.</p>', 0, NULL, '2021-02-01 11:09:45', NULL, NULL, '01625 817 677', NULL),
(4, NULL, 'Ordinary Power of Attorney & Living Will', '<p>This benefit gives you the ability to create your Ordinary Power of Attorney and Living Will online <strong>FREE OF CHARGE.</strong></p>\r\n<p>These documents would usually cost around &pound;100 per person, but as a Trust Box member, you can get acquire these legal documents in under 30 minutes for free.</p>\r\n<p>To find out more about why these documents are important, read our article.</p>\r\n<p><a class=\"btn btn-primary\" href=\"https://www.yourtrustbox.co.uk/posts\" target=\"_blank\" rel=\"noopener\">Find out more</a></p>\r\n<h4>&nbsp;</h4>\r\n<h4>What do I need to do now?</h4>\r\n<p>To produce your Ordinary Power of Attorney and/or Living Will, simply click the \'enrol now\' button to the right, then once enrolled you will see the option to \'redeem\'.</p>\r\n<p>To gift this service to someone who might need it, go to the \'My Community\' section in your account.</p>', '<p>-</p>', '/storage/thumbnails/pxbbkcI97NdxKaJJnBrnTqypAJgWzJAdq1PVPXFT.jpeg', '/storage/banners/VnYisRUy0mBiiw5NN3OMvMMnX2awPBxyQxsSeyrx.jpeg', '0', '2', '0', '<p class=\"MsoNormal\" style=\"text-align: center;\">To take advantage of this benefit and receive your free Ordinary Power of Attorney and/or Living Will follow the steps below and use <strong>voucher code TB100</strong>:&nbsp;</p>\r\n<ol style=\"text-align: center;\">\r\n<li>Click the redeem button to be taken to our document portal.</li>\r\n<li>Enter your email address, name and set a secure password to create your account.</li>\r\n<li>Answer the setup questions, which form the basis of your legal documents.&nbsp;</li>\r\n<li>Follow the step-by-step process to create your Ordinary Power of Attorney and/or Living Will. You will see the standard prices for these are:\r\n<p class=\"MsoNormal\">&nbsp;</p>\r\n<p class=\"MsoListParagraphCxSpFirst\" style=\"text-indent: -18.0pt; mso-list: l0 level1 lfo1;\"><!-- [if !supportLists]--><span style=\"font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;\">&middot;<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]-->Ordinary Power of Attorney - &pound;48</p>\r\n<p class=\"MsoListParagraphCxSpLast\" style=\"text-indent: -18.0pt; mso-list: l0 level1 lfo1;\"><!-- [if !supportLists]--><span style=\"font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;\">&middot;<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]-->Living Will - &pound;55</p>\r\n<p class=\"MsoNormal\">However, when asked for payment use your exclusive Trust Box discount code <strong>TB100</strong> to create these vital Estate Planning documents for <strong>free</strong>.</p>\r\n</li>\r\n</ol>\r\n<p style=\"text-align: center;\"><a class=\"btn btn-primary\" href=\"https://estateplanning.yourtrustbox.co.uk/\" target=\"_blank\" rel=\"noopener\">Redeem this benefit</a></p>', 0, 0, 0, 'adam@visionsharp.co.uk', '<p>This service is a Trust Box Tool.</p>', '<p>Trust Box have worked with trusted specialists within the Estate Planning industry to create this bespoke software technology for our members.&nbsp; &nbsp;</p>', 0, '2020-04-14 15:38:55', '2020-11-03 16:52:17', 0, 0, '', NULL),
(5, NULL, 'Money Management Made Easy', '<h2>A complete financial picture</h2>\r\n<p>Connect all your accounts to the OpenMoney app to see all your money in one place. We\'ll then start advising you on how to manage your money better.</p>', '<h2>Save it.</h2>\r\n<p>Use our smart tools to help build up your savings and reduce spend on everyday items.</p>\r\n<div>\r\n<div>\r\n<h3>Savings goals</h3>\r\n</div>\r\n<div>\r\n<p>Save for the moments that matter most, in the way that works best for you.</p>\r\n</div>\r\n<h3>Save on everyday bills</h3>\r\n<div>\r\n<p>Soon we\'ll be launching our partnership with uSwitch, which automatically tells you when you can save on your energy bills.</p>\r\n</div>\r\n</div>', '/storage/thumbnails/SPbxYu6YdfEO3HKDntt4ZL8M5gZ45csBRSTWhCX0.jpeg', '', '10', '3', '1', '<p>OpenMoney combines friendly expert advice with intelligent technology to help you make the most of your money.</p>\r\n<p>Move forward with your finances</p>\r\n<p><a href=\"https://portal.open-money.co.uk/quick-check/income\" target=\"_blank\" rel=\"noopener\">Click here to Start Your Financial Health Check</a></p>', 1, 1, 1, 'adam@visionsharp.co.uk', '<p><span style=\"font-size: 16px; caret-color: #393965; color: #ffffff; font-family: Nunito, sans-serif; text-align: center;\">There are millions of people across Britain who would benefit from financial advice but can&rsquo;t afford or access it. As a result, many of these people get caught in financial cycles that they struggle to get out of. We&rsquo;re here to help these people.</span></p>', '<p><span style=\"font-size: 16px; caret-color: #393965; color: #393965; font-family: Nunito, sans-serif; text-align: center;\"><span style=\"color: #ffffff;\">We\'re here to make sure that everyone who wants access to financial advice can get it. We do this by combining sophisticated technology with real people to provide personalised financial advice and help people reach their financial goals.</span>&nbsp;</span></p>', 1, '2020-05-20 15:00:43', '2020-11-11 16:52:39', 0, 0, '0', NULL),
(6, NULL, 'Investments Simplified', '<p>Whether you\'re an experienced investor or looking to invest for the first time, OpenMoney are here to help your money grow.</p>\r\n<h2>Invest in your future, today</h2>\r\n<p>We\'ll ask you some simple questions to see if you\'re ready to invest. If you are, we\'ll recommend the right investment products for you. If you\'re not, we can set you on the right track.</p>\r\n<ul>\r\n<li>\r\n<p>&pound;1 minimum investment</p>\r\n</li>\r\n<li>\r\n<p>Low annual fees of 0.50%*</p>\r\n</li>\r\n<li>\r\n<p>Track your investments 24/7</p>\r\n</li>\r\n</ul>', '<p>Boost this benefit today to receive a free financial health check</p>', '/storage/thumbnails/f8hzavZZy749mNUf3CTi02WME6qnRysESspu2NhY.png', '', '10', '3', '0', NULL, 0, 0, 0, 'adam@visionsharp.co.uk', '<p><span style=\"caret-color: #393965; color: #ffffff; font-family: Nunito, sans-serif; font-size: 16px; text-align: center;\">There are millions of people across Britain who would benefit from financial advice but can&rsquo;t afford or access it. As a result, many of these people get caught in financial cycles that they struggle to get out of. We&rsquo;re here to help these people.</span></p>', '<p><span style=\"color: #ffffff;\">We\'re here to make sure that everyone who wants access to financial advice can get it. We do this by combining sophisticated technology with real people to provide personalised financial advice and help people reach their financial goals.</span><span style=\"caret-color: #393965; color: #393965; font-family: Nunito, sans-serif; font-size: 16px; text-align: center;\">&nbsp;</span></p>', 1, '2020-05-20 15:44:06', '2020-11-24 14:58:25', 0, 0, '0', NULL),
(7, NULL, 'Signature Mortgages', '<p>Signature Saver members have access to Whole of market advisers, enabling you to get the best mortgage that suits your individual needs.&nbsp;&nbsp;What\'s more, our recommended advisers charge our Saver members&nbsp;NO BROKER FEES!</p>', '<p>Signature Saver members have access to Whole of market advisers, enabling you to get the best mortgage that suits your individual needs.&nbsp;&nbsp;What\'s more, our recommended advisers charge our Saver members&nbsp;NO BROKER FEES!</p>\r\n<p>For a complete all round service, your Saver membership gets you:</p>\r\n<ul>\r\n<li>Access to whole of market advisers - chosen from our FCA qualified broker panel to suit YOUR needs&nbsp;</li>\r\n<li>Simple and effective mortgage service</li>\r\n<li>Residential and Buy to Let purchases*</li>\r\n<li>Remortgages, for better rates or to raise funds for home improvements, debt consolidation** and/or other reasons</li>\r\n<li>Life and critical illness cover review</li>\r\n<li>Income protection review</li>\r\n<li>The Signature promise - to give you an honest, professional and efficient experience</li>\r\n</ul>\r\n<p>&nbsp;So, if you are looking to arrange a new mortgage to buy a property or remortgage your existing property get in touch to see if our recommended broker panel can save you time and money!</p>', '/storage/thumbnails/FHjLhp9Ac4PpvYuHDovKdStn1fPxAbET10flFAbG.jpeg', NULL, '2', '5', '0', '<p>Want to read on? Only our Saver Members have access to all the info! Do you want to benefit from this and all of our other amazing savings? Simply register your details and&nbsp;<a href=\"https://www.signaturesaver.co.uk/index.php/sign-up\">sign up!</a>&nbsp;You too can start and enjoy keeping more of your money in your pocket! Everyday savings and discounts for everyday people! What are you wating for??</p>', 0, 0, 0, 'sam@trusttech42.co.uk', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel nunc eu enim facilisis lacinia. Maecenas rutrum pharetra tellus, tempor suscipit sem consectetur vel.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel nunc eu enim facilisis lacinia. Maecenas rutrum pharetra tellus, tempor suscipit sem consectetur vel.</p>', 1, '2020-05-21 14:48:46', '2020-11-24 17:03:04', 0, 0, '0', NULL),
(8, NULL, '24/7 Legal Support', '<p>No matter who you are, there will be times in your life when you need some legal advice and it&rsquo;s important to protect yourself and your family by only taking advice from the experts.</p>\r\n<p>With Trust Box, this is taken care of with our 24/7 support benefit.</p>\r\n<p>Who knows what the future may hold. At some point you may need employment advice, have an issue as a consumer or face a motoring prosecution and need efficient, expert legal advice. Trust Box&rsquo;s legal advice helpline is open 365 days a year, 24 hours a day and our experts can advise you on almost any personal legal issue or scenario.</p>\r\n<p>Specialist legal advisers are on hand to help you and your loved ones whatever modern life throws at you.</p>', '<p>Boost this benefit to get even more peace of mind; oonce boosted you are also covered financially.</p>\r\n<p>If you decide to boost your 24/7 Legal Support Benefit, you&rsquo;ll take advantage of legal expense cover for all family related matters. Providing you&rsquo;ve called the help line and benefited from our advice, any subsequent work is covered by your policy, meaning you can focus on getting the best outcome for you.</p>', '/storage/thumbnails/sP6VrtCbbDjLVgJ3sfwp0NeeIjM8Y5wbIj19a2qF.jpeg', 'storage/banners/DoGMvipl902Lgm7nwFdaR4x6sm2IwN8tV4rnfATE.jpeg', '3', '6', '0', NULL, 0, 1, 1, 'adam@visionsharp.co.uk', '<p>Stephensons is a leading UK full service law firm. They provide all of their clients with cost effective, industry leading legal advice.</p>', '<p>The team at Stephensons brings with it in depth knowledge and a wealth of experience of dealing with a range of complex legal matters.</p>', 1, '2020-05-22 11:52:01', '2021-02-01 14:28:17', 0, 0, '0', NULL),
(9, NULL, 'Legacy Assist', '<p>You probably know that proper planning during your lifetime is the only way to make sure your loved ones receive everything they&rsquo;re entitled to after you&rsquo;re gone. But how confident are you that the planning you put in place is up to date and still the best option for you and your Trusted People?</p>\r\n<p>As a Trust Box member, you will receive an annual legacy review, carried out by our estate planning experts.</p>', '<p>If you upgrade you boost this benefit, not only will you benefit from the annual review, but if you&rsquo;ve completed your Estate Planning with our partner LSGi then any updates will be included in your membership.</p>\r\n<p>This includes actions following the annual review and any amendments required due to a change in your assets, meaning you have complete peace of mind that your documents will be up to date and amended professionally.</p>', '/storage/thumbnails/v8sOpZKe99dpqR0X9OUuAMXKOCUPCfV8W4eOhwc8.png', NULL, '2', '18', '0', NULL, 0, 0, 0, 'sam@trusttech42.co.uk', NULL, NULL, 1, '2020-05-22 12:04:22', '2020-11-25 10:53:04', 0, 0, '0', NULL),
(10, NULL, 'Will Writing, Legacy Planning & Probate', '<p>Every individual over the age of 18 should have a Will - whether they are rich or poor, male or female, single or married. Here are some of the reasons why...</p>\r\n<p>A family can suffer acute financial hardship because of the delays in dealing with the estate of a person who has no Will. These delays can run into YEARS!</p>\r\n<ul>\r\n<li>The courts may appoint a guardian for your children that you would not have chosen yourself. You can nominate guardians of YOUR choice in your Will to prevent this.</li>\r\n<li>The professional costs involved in administering an estate where there is no Will are far higher than where there is a Will. Why line the pockets of banks &amp; solicitors with money that will be needed by your family?</li>\r\n<li>If you are not married to your partner then he/she will NOT automatically inherit your estate.</li>\r\n<li>Thousands of small heirlooms and treasures are lost to the auction rooms every year because no one can decide who should have them. Your Will protects your heritage.</li>\r\n<li>Banks, building societies and insurance companies hold millions of pounds belonging to people who are almost certainly dead - because their families are not aware that it exists.</li>\r\n<li>Thousands of pounds could be unnecessarily paid in inheritance tax (death duties) because a Will has not been drawn up.</li>\r\n<li>Family arguments nearly always arise over who should get personal belongings and what type of funeral should be arranged. It is unfair to thrust these decisions upon a grieving family.</li>\r\n<li>Most people would rather their money went to a charity than to the taxman if they have no one to leave it to. This can only happen through a Will.</li>\r\n</ul>', '<p>As a Signature member you have access to our legal package for Will Writing and Probate services at specially discounted prices</p>', '/storage/thumbnails/gjBRc6TxhGsqKLIfMwH0OSCn3plMiFNYapGj5oo9.jpeg', NULL, '3', '8', '0', '<p>If you would like to set up your will and/or lasting power of attorney now, at unrivaled prices, simply click the \"Setup Today\" button below and follow the instructions.</p>', 0, 0, 0, 'sam@trusttech42.co.uk', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel nunc eu enim facilisis lacinia. Maecenas rutrum pharetra tellus, tempor suscipit sem consectetur vel.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel nunc eu enim facilisis lacinia. Maecenas rutrum pharetra tellus, tempor suscipit sem consectetur vel.</p>', 1, '2020-05-29 14:12:36', '2020-11-24 16:57:37', 0, 0, '0', NULL),
(11, NULL, 'Signature Vehicle Sourcing', '<p>Looking for a new vehicle but not sure where to search?&nbsp; Look no further! Saver members have exclusive access to discounted vehicles on all types of finance deals, from Outright Purchase to Leasing, PCP &amp; HP!&nbsp; Start your search now!</p>\r\n<p>Whether you&rsquo;re a private individual looking to purchase or lease a single vehicle, or a corporate business looking to purchase or lease multiple vehicles, Signature Vehicle Sourcing covers all bases!&nbsp; Better still, we work with a number of providers and manufacturers which means Signature Saver members receive huge discounts on a range of vehicles.&nbsp;</p>\r\n<p>We don&rsquo;t just source them and leave it up to you to arrange everything, we go the whole extra mile! We specialise in all types of car finance such as PCP, HP, Finance Lease &amp; Outright Purchase, but in particular car leasing, van leasing, vehicle leasing and contract hire in the UK.</p>\r\n<p>We can cater for all needs and arrange suitable finance for the private individual, business or even if you are a large fleet customer.</p>\r\n<p>We also offer Fleet Management nationwide, where we provide bespoke services to suit your specific business needs. Budget and cost saving solutions for fleets from as small as one vehicle to fleets of more than 1000.&nbsp;</p>', '<p>&nbsp; &nbsp;&nbsp;</p>', '/storage/thumbnails/T08sDkPBV4i26TTJflhokzkGWq7muLTH1xEGpn1O.jpeg', NULL, NULL, '5', '0', '<p>So, if you are looking to buy a new vehicle at an amazing price - GET IN TOUCH NOW! Or click the button below to see our latest offers!</p>', 0, 0, 0, 'sam@trusttech42.co.uk', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel nunc eu enim facilisis lacinia. Maecenas rutrum pharetra tellus, tempor suscipit sem consectetur vel.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel nunc eu enim facilisis lacinia. Maecenas rutrum pharetra tellus, tempor suscipit sem consectetur vel.</p>', 1, '2020-05-29 14:18:26', '2020-11-24 16:57:15', 0, 0, '0', NULL),
(12, NULL, 'Signature Legal Helpline', '<h5>Sometimes you just need to talk to someone who knows the options and&nbsp;pitfalls open to you.&nbsp; Your Saver membership gives you access to a designated Legal Helpline, free of charge!</h5>\r\n<p>&nbsp;</p>\r\n<p>Legal advice should be accessible and straight forward for everyone. We recognise that legal services are not readily available to the majority and addressed this by providing a legal advisory service which provides direct access, 24/7, to legally trained call handlers. This allows you to discuss any legal issue you may have whether it be moving to a new house, your job, in fact in relation to any aspect of your daily life, our team are available to listen and give you advice.</p>\r\n<p>PROVIDING YOU WITH A 24/7, 365 LEGAL ADVISORY SERVICE</p>\r\n<p>&bull; &nbsp;Who do you turn to if you are faced with an unexpected legal issue?</p>\r\n<p>&bull; &nbsp;It may relate to your job, personal debt, child care or any other aspect of your daily life.</p>\r\n<p>&bull; &nbsp;Would you know where to go, how would you choose and how much will it cost?</p>\r\n<p>&nbsp;</p>', '<p>YOUR SAVER MEMBERSHIP GIVES YOU ACCESS TO SPECIALIST LEGAL ADVICE ACROSS ALL AREAS OF LAW, ALLOWING YOU TO MAKE INFORMED DECISIONS AND PLAN EFFECTIVELY, AND WITH NO COSTLY SURPRISES.</p>\r\n<p>&bull; &nbsp;24/7, 365 days-a-year- legal advisory service delivered by professional legally trained call handlers;</p>\r\n<p>&bull; &nbsp;Advice and guidance on all areas of law;</p>\r\n<p>&bull;&nbsp;No limit on the number of times the service can be used, subject to fair use;</p>\r\n<p>&bull;&nbsp;Free initial advice to establish the legal issues and the best way forward;</p>\r\n<p>&bull;&nbsp;&nbsp;Unlike other services where insurance is provided you will receive guidance in all&nbsp;instances, not just those covered by the insurance policy;</p>\r\n<p>&bull; &nbsp;One telephone number provides the gateway to advice on any legal issue;</p>\r\n<p>&bull; &nbsp;Where relevant if legal fees are payable, such as in relation to conveyancing or a divorce, you will benefit from a reduction in standard rates.&nbsp;</p>', '/storage/thumbnails/p9fjdkuZM1XFXgNecn9ywaFbGOyKpWlQqkNWJbad.jpeg', NULL, '2', '5', '0', '<p>If you need help don\'t hesitate to contact us today on either option below:&nbsp;</p>', 0, 0, 0, 'sam@trusttech42.co.uk', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel nunc eu enim facilisis lacinia. Maecenas rutrum pharetra tellus, tempor suscipit sem consectetur vel.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel nunc eu enim facilisis lacinia. Maecenas rutrum pharetra tellus, tempor suscipit sem consectetur vel.</p>', 1, '2020-05-29 14:27:55', '2020-11-24 16:55:04', 0, 0, '0', NULL),
(13, NULL, 'Signature Holidays', '<p>Signature Saver, in association with Designer Travel offer great holiday deals at unbeatable prices -&nbsp;Their&nbsp;Price Match Policy&nbsp;means they&nbsp;will not be beaten on price!&nbsp;Their association with Hays Travel&nbsp;ensures every holiday is 100% financially protected under ABTA or ATOL.</p>\r\n<p>&nbsp;</p>\r\n<p>All the agents at Designer Travel have a passion for travel, customer service and extensive knowledge of the travel industry.</p>\r\n<div>\r\n<p>Our association with Hays Travel&nbsp;ensures every holiday is 100% financially protected under ABTA or ATOL.&nbsp; Signature Holidays gives access to:</p>\r\n<p>&bull; Travel experts nationwide</p>\r\n<p>&bull; Competitive prices and exceptional end to end service</p>\r\n<p>&bull;&nbsp;Price Match Policy&nbsp;-&nbsp;We will not be beaten on price*</p>\r\n<p>&bull; Direct debit payment plan to help spread the cost</p>\r\n<p>We are delighted to have teamed up with Designer Travel whose repeat booking levels are much higher than the industry average, illustrating the excellent customer service.</p>\r\n<p>&nbsp;</p>\r\n</div>', '<p>&nbsp; &nbsp; &nbsp;&nbsp;</p>', '/storage/thumbnails/YS3B9MFrzDWuIril8kDHk1WLAXNTDpC0C1FhUuhn.jpeg', NULL, NULL, '17', '0', '<p>What are you waiting for? Get in touch now to start saving on your next holiday!</p>', 0, 0, 0, 'sam@trusttech42.co.uk', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel nunc eu enim facilisis lacinia. Maecenas rutrum pharetra tellus, tempor suscipit sem consectetur vel.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel nunc eu enim facilisis lacinia. Maecenas rutrum pharetra tellus, tempor suscipit sem consectetur vel.</p>', 1, '2020-05-29 14:30:50', '2020-11-24 16:51:16', 0, 0, '0', NULL),
(14, NULL, 'Signature Conveyancing', '<p>Thinking of Moving home?&nbsp; We have great news!&nbsp; We have partnered with Movin\' Legal so that all Signature Saver members receive &pound;100* off the quoted price for conveyancing and our members also receive FREE Homebuyer protection cover worth upto &pound;600!</p>\r\n<p>Signature Conveyancing, powered by Movin\' Legal, is a true cost comparison quotation facility for buying, selling or moving house, offering a fast and efficient opportunity to instruct approved solicitors with no hidden costs at a price you&rsquo;ll love, now there&rsquo;s a first!&nbsp;</p>\r\n<p>Better still it is a simple, quick and efficient way to select the ideal law firm for your conveyancing, via a comparison site, which offers an exceptional service whilst also including&nbsp;free homebuyer protection cover&nbsp;during the process.</p>\r\n<p>Here&rsquo;s how it works for you:</p>\r\n<p><strong>Quote and Instruction</strong></p>\r\n<p>You can choose your law firm by mortgage lender, location, price and service rating, with a complete legal conveyancing service through our approved solicitor panel.</p>\r\n<p><strong>Real Time All Inclusive &amp; Guaranteed Quotes</strong></p>\r\n<p>The price you are quoted is the price you pay. There&rsquo;s no hidden charges and no unexpected additions to your legal fees.</p>\r\n<p><strong>Individual Case Tracking</strong></p>\r\n<p>Homebuyers, sellers or home movers, take advantage of regular updates during the conveyancing process, you will have up-to-date status reports available allowing you to remain in complete control.</p>\r\n<p><strong>Protection - Free Home Buyer Protection</strong></p>\r\n<p>Saver membership provides cover for conveyancing fees, mortgage arrangement fees, survey fees and valuation fees up to a combined (aggregate) maximum payment of &pound;600 in the unlikely event of your property sale falling through.</p>\r\n<p>&nbsp;</p>', '<p>&nbsp; &nbsp; &nbsp;</p>', '/storage/thumbnails/JoxKw0nQnCag9rNK4FqHu6VouwCfnQWFw8SoSdiI.jpeg', NULL, NULL, '16', '0', '<p>Interested?&nbsp; Why dont you get your own quote by using the comparison tool below or contacting us via:</p>', 0, 0, 0, 'sam@trusttech42.co.uk', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel nunc eu enim facilisis lacinia. Maecenas rutrum pharetra tellus, tempor suscipit sem consectetur vel.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel nunc eu enim facilisis lacinia. Maecenas rutrum pharetra tellus, tempor suscipit sem consectetur vel.</p>', 1, '2020-05-29 14:35:08', '2020-11-24 16:49:59', 0, 0, '0', NULL),
(15, NULL, 'Private Doctor - On Call GP', '<p>We all want peace of mind when it comes to our families health, but with current challenges in&nbsp;NHS we don\'t always get the care we need, when we need it, including same day home visits with our top class doctors</p>\r\n<p>That\'s why Signature Saver have teamed up with AKEA Lite to make sure you get just that!. AKEA Lite\'s service is&nbsp;rated 5 out of 5 on doctify and their patient feedback speaks volumes.</p>\r\n<p>Signature Saver members can access:</p>\r\n<ul>\r\n<li>Unlimited telephone consultations - speak to a doctor usually within 30 minutes</li>\r\n<li>Remote prescribing - you don&rsquo;t even need to go to the pharmacy we can have your medicine delivered to you</li>\r\n<li>Same day home visits with our top class doctors</li>\r\n<li>Referrals for private and NHS on demand and use of our health concierge team</li>\r\n</ul>\r\n<p>All these services operate 7 days a week!</p>', '<p>There is a charge for this amazing product - Prices start from &pound;40 per adult per month, however, Signature Saver&nbsp;members get a&nbsp;20% discount&nbsp;making it only &pound;32/month and just&nbsp;&pound;8/month for children.&nbsp; Setup today and don\'t forget to give the team your Saver membership number to apply your discount!</p>', '/storage/thumbnails/FleOpRorZbOgi6Ovm67td3tTsBuI39PM7dxciyeS.jpeg', NULL, '32', '15', '0', '<p>Setup today and don\'t forget to give the team your Saver membership number to apply your discount!</p>', 0, 0, 0, 'sam@trusttech42.co.uk', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel nunc eu enim facilisis lacinia. Maecenas rutrum pharetra tellus, tempor suscipit sem consectetur vel.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel nunc eu enim facilisis lacinia. Maecenas rutrum pharetra tellus, tempor suscipit sem consectetur vel.</p>', 1, '2020-05-29 14:39:37', '2020-11-24 16:48:44', 0, 0, '0', NULL),
(16, NULL, 'Personal Insurance', '<p>As a signature member we want to ensure you always get the right insurance at the right price! With access to the UKs leading providers of insurance services we make sure you never spend more than you have to for all of your insurance needs.</p>\r\n<p>Whether you are looking for insurance for the first time or thinking of switching providers we have all the information to make sure the policy you need is the right deal at the right price.</p>', '<p>&nbsp; &nbsp; &nbsp;&nbsp;</p>', '/storage/thumbnails/zhHIKRDeR4cSzlmOqgjwIUWwVKGozrUFdQ5OXG4y.jpeg', NULL, NULL, '5', '0', '<p>If you are looking to take out a new or review an exisiting insurance policy why dont you get in touch to see how much you coul save with your Signature Saver membership?</p>', 0, 0, 0, 'sam@trusttech42.co.uk', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel nunc eu enim facilisis lacinia. Maecenas rutrum pharetra tellus, tempor suscipit sem consectetur vel.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel nunc eu enim facilisis lacinia. Maecenas rutrum pharetra tellus, tempor suscipit sem consectetur vel.</p>', 1, '2020-05-29 14:51:23', '2020-11-24 16:47:17', 0, 0, '0', NULL),
(17, NULL, 'Buying & Selling Your Home', '<p>Signature Saver members can now sell their property for FREE,&nbsp;NO COMMISSION, NO FEES &amp; NO TIE INS!&nbsp;</p>\r\n<p>Whether you are Buying a House for the first time, Moving from one home to a new one, purchasing a New Build property or a second home in the UK, we can help you.</p>\r\n<p>&nbsp;</p>\r\n<p>Buying or selling your home can be one of the most stressful times of your life. Your Saver Membership gives you direct access to Elopa Estate Agents whose staff are trained to understand this. They realise the importance of speed, communicating with everyone involved and providing a value for money service.&nbsp;&nbsp;</p>\r\n<p>Using Elopa and their team of residential property experts who have extensive knowledge, combined with our online process, enable us to provide an efficient and comprehensive package for clients.</p>\r\n<p>If you are looking to Sell now, your Saver membership gives you access to Elopa Estate Agents who can sell your property,&nbsp;NO&nbsp;COMMISSION, NO FEES &amp; NO TIE IN\'S!&nbsp;</p>', '<p>&nbsp; &nbsp; &nbsp;</p>', '/storage/thumbnails/VjyhzB9c4G8ZWcyMpSGV18kdcC604TkziCFRqGPg.jpeg', NULL, NULL, '14', '0', '<p>Give the Elopa team a call for your free consultation or if you are looking ot get an online valuation of your property please&nbsp;<a href=\"https://www.elopa.co.uk/\">click here!</a>&nbsp;</p>', 0, 0, 0, 'sam@trusttech42.co.uk', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel nunc eu enim facilisis lacinia. Maecenas rutrum pharetra tellus, tempor suscipit sem consectetur vel.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel nunc eu enim facilisis lacinia. Maecenas rutrum pharetra tellus, tempor suscipit sem consectetur vel.</p>', 1, '2020-05-29 14:54:59', '2020-11-24 16:38:59', 0, 0, '0', NULL),
(18, NULL, 'Accident Management', '<p>Saver members have access to Signature Accident Management to protect you just in case the unexpected happens! Our partners provide a 24/7 365 days a year claims management service handling Non-Fault, Accidental Damage, Windscreen and Theft claims, ensuring Saver members are back on the road, following an accident, in no time at all.</p>\r\n<p>&nbsp;</p>\r\n<p>We have partnered up with Kingsway Claims, who provide a 24/7 365 days a year claims management service handling Non-Fault, Accidental Damage, Windscreen and Theft claims.</p>\r\n<p>Signature Accident Management, in association with Kingsway Claims, gives you peace of mind when you need it most.&nbsp;&nbsp;</p>\r\n<ul>\r\n<li>Call regardless of fault as Kingsway Claims are here to help and offer expertise to Signature Saver members</li>\r\n<li>With no excess to pay when it&rsquo;s not your fault, avoid increases to insurance premiums by calling them if you have an accident</li>\r\n<li>Access to around 200 repairers nationwide</li>\r\n<li>They handle in the region of 30,000&nbsp;motor claims every year</li>\r\n<li>With over 50,000 vehicles accessible for hire within the UK</li>\r\n</ul>\r\n<p>Working in partnership with leading hire providers and solicitors in the UK, in addition to managing a large network of repairers nationwide your Saver membership ensures you are back on the road, following an accident, in no time at all.&nbsp;</p>', '<p>&nbsp; &nbsp; &nbsp;</p>', '/storage/thumbnails/oD2Tp4YYWwggRb11e01KL20bmdh1lK7uVvtEMIym.jpeg', NULL, NULL, '13', '0', NULL, 0, 0, 0, 'sam@trusttech42.co.uk', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel nunc eu enim facilisis lacinia. Maecenas rutrum pharetra tellus, tempor suscipit sem consectetur vel.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel nunc eu enim facilisis lacinia. Maecenas rutrum pharetra tellus, tempor suscipit sem consectetur vel.</p>', 1, '2020-05-29 14:58:19', '2020-11-24 16:37:03', 0, 0, '0', NULL),
(19, NULL, 'Signature Weekly Winner', '<h5>Signature Saver offers a fantastic exclusive weekly prize direct to members - where one lucky member receives a Gift Voucher every Sunday!</h5>\r\n<p>&nbsp;Make your money go further with Signature Deal of the Week!</p>', '<p>&nbsp; &nbsp; &nbsp;</p>', '/storage/thumbnails/2sJ4ao92oxv9JagCdlVjosWMrUN0cdLWmptQtpLx.jpeg', NULL, NULL, '5', '0', NULL, 0, 0, 0, 'sam@trusttech42.co.uk', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel nunc eu enim facilisis lacinia. Maecenas rutrum pharetra tellus, tempor suscipit sem consectetur vel.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel nunc eu enim facilisis lacinia. Maecenas rutrum pharetra tellus, tempor suscipit sem consectetur vel.</p>', 1, '2020-05-29 15:05:02', '2020-11-24 16:22:14', 0, 0, '0', NULL),
(21, NULL, 'Mortgages', '<p>Whether you are a first-time buyer, moving home or remortgaging, we offer an&nbsp;advice and recommendation service. We also offer protection&nbsp;products. Our long-standing relationships with a panel of selected lenders mean we have access on occasions to exclusive products.</p>\r\n<p>We have mortgage options specifically designed to help you purchase your new home.</p>\r\n<h4>FOR PEOPLE WHO ARE THINKING OF MOVING HOME OR REMORTGAGING</h4>\r\n<p>We will provide advice and information about all the costs and charges involved when buying a new home or remortgaging and how some of these could be reduced.</p>', '<p>&nbsp;&nbsp;</p>', '/storage/thumbnails/3gKTux2URSeP8wht5tXfWrdY5eTlmm74y9ksqVeA.png', '/storage/banners/vQ6G1AHjLmcYSLcoxi9KEvrbQt736VCBM4fbOM0b.png', NULL, '11', '0', '<p>Our dedicated Mortgage Business Unit facilitates the speedy and efficient processing of your mortgage and keeps you fully informed throughout the application process.</p>\r\n<p><a href=\"https://www.connells.co.uk/EnquiryEmail.aspx?template=4&amp;advertCode=ENQUIRYFORMPAGECONTENTBUTTON&amp;TB_iframe=true&amp;height=425&amp;width=561\">Contact your local&nbsp;Connells Mortgage Services consultant</a>, or call&nbsp;01525 213 721&nbsp;and speak to our customer service department&nbsp;who will be happy to arrange for a consultant to contact you.</p>', 0, 0, 0, 'sam@trusttech42.co.uk', '<p>Connells Mortgage Services are fully focused on customers needs and available through all our estate agency branches</p>', '<p>We take the responsibility of finding you the right mortgage very seriously, which is why&nbsp;Connells continuously monitors the quality and standard of our qualified consultants.</p>', 1, '2020-09-28 13:35:55', '2020-11-24 17:05:26', 0, 0, '0', NULL),
(25, NULL, 'Document Storage', '<p>The original versions of legal documents, such as Wills and Powers of Attorney are the only legally binding versions. Scans, photocopies and computer records are not legally valid because they don&rsquo;t have your original signature on them. The original Will is your only Will and must be kept safely.</p>\r\n<p>One place you should never keep an original Will is at home. If you have a fire, flood or burglary, you risk losing your Will. If your Will is damaged in any way, then the courts could declare the Will invalid.</p>\r\n<p>You must keep your Will in a safe place; however that place must be easily accessible when the document is needed!</p>', '<p>&nbsp; &nbsp;</p>', '/storage/thumbnails/z56OHXI5GuO9cBzApCX5HdtA5arlsFtN3GTNQ1iA.jpeg', 'storage/banners/XjevfWilZ8ZY7XJe3jyKrhOOTDwNUZYEh5Q4icgn.jpeg', NULL, '2', '0', NULL, 0, 1, 1, 'info@legalservicesguild.co.uk', NULL, NULL, 1, '2020-11-05 16:48:21', '2021-02-01 14:02:47', NULL, NULL, '0', NULL),
(26, NULL, 'Will Review Service', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer cursus, nunc vel eleifend aliquam, arcu ex mattis turpis, eu tincidunt nibh nisl vel magna. Praesent blandit, ex et congue pretium, purus ligula eleifend enim, in dapibus metus quam eu sem. Cras eu interdum ligula, eget pharetra augue. Aenean dapibus, urna a malesuada vestibulum, dolor purus auctor nisl, quis consectetur justo dui nec tellus. Maecenas eu arcu a orci euismod tincidunt. Morbi convallis finibus euismod. Aliquam ut egestas lorem, a vestibulum nulla. Sed id purus eget neque porttitor iaculis. Ut bibendum enim a lacus finibus, sed dignissim orci porta. Fusce dapibus cursus felis nec luctus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae;</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer cursus, nunc vel eleifend aliquam, arcu ex mattis turpis, eu tincidunt nibh nisl vel magna. Praesent blandit, ex et congue pretium, purus ligula eleifend enim, in dapibus metus quam eu sem. Cras eu interdum ligula, eget pharetra augue. Aenean dapibus, urna a malesuada vestibulum, dolor purus auctor nisl, quis consectetur justo dui nec tellus. Maecenas eu arcu a orci euismod tincidunt. Morbi convallis finibus euismod. Aliquam ut egestas lorem, a vestibulum nulla. Sed id purus eget neque porttitor iaculis. Ut bibendum enim a lacus finibus, sed dignissim orci porta. Fusce dapibus cursus felis nec luctus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae;</p>', '/storage/thumbnails/7ayQn34uQmUPvWF6p2obYqwZxbbHmJ2rVR3M6IsH.png', '/storage/banners/cJUIGVPbNBqeLL3P9amlrazqObaVp5ehC7IBi9kY.jpeg', NULL, '10', '1', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer cursus, nunc vel eleifend aliquam, arcu ex mattis turpis, eu tincidunt nibh nisl vel magna. Praesent blandit, ex et congue pretium, purus ligula eleifend enim, in dapibus metus quam eu sem. Cras eu interdum ligula, eget pharetra augue. Aenean dapibus, urna a malesuada vestibulum, dolor purus auctor nisl, quis consectetur justo dui nec tellus. Maecenas eu arcu a orci euismod tincidunt. Morbi convallis finibus euismod. Aliquam ut egestas lorem, a vestibulum nulla. Sed id purus eget neque porttitor iaculis. Ut bibendum enim a lacus finibus, sed dignissim orci porta. Fusce dapibus cursus felis nec luctus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae;</p>', 0, 1, 1, 'info@legalservicesguild.co.uk', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer cursus, nunc vel eleifend aliquam, arcu ex mattis turpis, eu tincidunt nibh nisl vel magna. Praesent blandit, ex et congue pretium, purus ligula eleifend enim, in dapibus metus quam eu sem. Cras eu interdum ligula, eget pharetra augue.&nbsp;</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer cursus, nunc vel eleifend aliquam, arcu ex mattis turpis, eu tincidunt nibh nisl vel magna. Praesent blandit, ex et congue pretium, purus ligula eleifend enim, in dapibus metus quam eu sem. Cras eu interdum ligula, eget pharetra augue.</p>', 1, '2020-11-06 10:23:40', '2020-11-25 10:40:11', NULL, NULL, '0', NULL),
(27, NULL, 'Bereavement Services', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer cursus, nunc vel eleifend aliquam, arcu ex mattis turpis, eu tincidunt nibh nisl vel magna. Praesent blandit, ex et congue pretium, purus ligula eleifend enim, in dapibus metus quam eu sem. Cras eu interdum ligula, eget pharetra augue.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer cursus, nunc vel eleifend aliquam, arcu ex mattis turpis, eu tincidunt nibh nisl vel magna. Praesent blandit, ex et congue pretium, purus ligula eleifend enim, in dapibus metus quam eu sem. Cras eu interdum ligula, eget pharetra augue.</p>', '/storage/thumbnails/cBwbDxdFipuTzh3Oz1iEnkuaMBp3YdHMoOpMkzcK.jpeg', '/storage/banners/n2J5ELwplsADhQ07yGSiX769Mtmla6tzkIcKnLk1.jpeg', NULL, '10', '0', NULL, 0, 1, 1, 'info@legalservicesguild.co.uk', NULL, NULL, 1, '2020-11-06 10:29:32', '2020-11-25 10:40:57', NULL, NULL, '0', NULL),
(28, NULL, 'Funeral Planning', '<p>A funeral plan is a simple and practical way to have the peace of mind that your funeral has been arranged and paid for in advance. When the time comes, not only will it give your family the reassurance that your funeral wishes are known, but it will also help to protect against rising funeral costs.</p>\r\n<p>Our Funeral Plan is available to anyone aged 50 or over. It gives you the flexibility to tailor-make a plan that&rsquo;s right for you.&nbsp;</p>', '<p>&nbsp; &nbsp;</p>', '/storage/thumbnails/6aBxxUb0cM8El2GJvlRlbJ2cUwRs4WK0OVylgXV7.jpeg', '/storage/banners/8SUqJK1nLtDsVQc4FY5jOkfDnCqXpZHmVy1w0elE.jpeg', NULL, '1', '0', NULL, 0, 1, 1, 'dominic@lsgi.co.uk', '<p>&nbsp;&nbsp;</p>', NULL, 1, '2020-11-06 10:41:51', '2021-02-01 14:04:57', NULL, NULL, '0', NULL),
(29, NULL, 'Will Writing Service', '<p><strong>Do It Yourself.&nbsp;&nbsp;&pound;49 / &pound;69&nbsp;</strong></p>\r\n<p>Write your Will in just 20 minutes and protect what&rsquo;s important to you. Your Will is legally underwritten and completed online, in simple to understand steps and then emailed to you ready for signing.</p>\r\n<p><strong>Let us do it for you.&nbsp; &pound;150 / &pound;180&nbsp;</strong></p>\r\n<p>If you would prefer, you can arrange a private telephone or video call with one of our Estate Planning Consultants who will help you explore all the options and guide you every step of the way to put in place a Will that works for you and your family.&nbsp; Your Will be legally underwritten, drafted and sent for you to approve before being presented as a your final, riveted, and ready to sign document.You can call us on&nbsp;0330 390 0535&nbsp;to make an appointment or simply book online.</p>', '<p><span style=\"color: #212529; font-family: Segoe UI, sans-serif;\"><span style=\"caret-color: #212529; background-color: #ffffff;\">&nbsp; &nbsp;</span></span></p>', '/storage/thumbnails/ABIAvX3YJEwPBljWlPHseH0YxaZRWSwk8xonWTei.png', 'storage/banners/PkqcGN7i9tO766hjrIVjICbExMIwTYJDcleOhvaH.jpeg', NULL, '23', '0', NULL, 0, 1, 1, 'dominic@lsgi.co.uk', NULL, '<p>We save you hundreds of Pounds in expensive solicitor costs by allowing you to complete your will online, at a time to suit you, or you can ask us to help you do it for you.</p>', 1, '2020-11-06 10:48:33', '2021-02-01 14:16:41', NULL, NULL, '0', NULL),
(30, NULL, 'Pension Review', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer cursus, nunc vel eleifend aliquam, arcu ex mattis turpis, eu tincidunt nibh nisl vel magna. Praesent blandit, ex et congue pretium, purus ligula eleifend enim, in dapibus metus quam eu sem. Cras eu interdum ligula, eget pharetra augue.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer cursus, nunc vel eleifend aliquam, arcu ex mattis turpis, eu tincidunt nibh nisl vel magna. Praesent blandit, ex et congue pretium, purus ligula eleifend enim, in dapibus metus quam eu sem. Cras eu interdum ligula, eget pharetra augue.</p>', '/storage/thumbnails/UN9r5EWs7c6xwWgkaksmKTpvWTbhckCn0OU1odOv.png', '/storage/banners/Ou4yRxXuU9x6kq9DfW1FoDNcoHvFTDnokmAs49CZ.jpeg', NULL, '7', '0', NULL, 0, 1, 1, 'info@tuto.com', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer cursus, nunc vel eleifend aliquam, arcu ex mattis turpis, eu tincidunt nibh nisl vel magna. Praesent blandit, ex et congue pretium, purus ligula eleifend enim, in dapibus metus quam eu sem. Cras eu interdum ligula, eget pharetra augue.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer cursus, nunc vel eleifend aliquam, arcu ex mattis turpis, eu tincidunt nibh nisl vel magna. Praesent blandit, ex et congue pretium, purus ligula eleifend enim, in dapibus metus quam eu sem. Cras eu interdum ligula, eget pharetra augue.</p>', 1, '2020-11-06 10:52:34', '2020-11-24 15:05:02', NULL, NULL, '0', NULL),
(31, NULL, 'Legal Helpline', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer cursus, nunc vel eleifend aliquam, arcu ex mattis turpis, eu tincidunt nibh nisl vel magna. Praesent blandit, ex et congue pretium, purus ligula eleifend enim, in dapibus metus quam eu sem. Cras eu interdum ligula, eget pharetra augue.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer cursus, nunc vel eleifend aliquam, arcu ex mattis turpis, eu tincidunt nibh nisl vel magna. Praesent blandit, ex et congue pretium, purus ligula eleifend enim, in dapibus metus quam eu sem. Cras eu interdum ligula, eget pharetra augue.</p>', '/storage/thumbnails/nIN96rKV6r6HgFAVu9P2TXnuw8uakcOmjOuqDD1u.jpeg', '/storage/banners/NtMSJZ7w6zsB6l8AoiQNWgJGnZD7AADGnYBYUsJx.jpeg', NULL, '6', '0', NULL, 0, 1, 1, 'info@stephensons.com', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer cursus, nunc vel eleifend aliquam, arcu ex mattis turpis, eu tincidunt nibh nisl vel magna. Praesent blandit, ex et congue pretium, purus ligula eleifend enim, in dapibus metus quam eu sem. Cras eu interdum ligula, eget pharetra augue.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer cursus, nunc vel eleifend aliquam, arcu ex mattis turpis, eu tincidunt nibh nisl vel magna. Praesent blandit, ex et congue pretium, purus ligula eleifend enim, in dapibus metus quam eu sem. Cras eu interdum ligula, eget pharetra augue.</p>', 1, '2020-11-06 11:01:06', '2020-11-24 14:47:48', NULL, NULL, '0', NULL),
(32, NULL, '1-2-1 Consulting', '<p>Ambitious accountants engage Paul for a number of reasons. Some have too much growth and need help in managing it; others have too little growth while others simply want help learning how to make their practice more efficient. All those who do engage have one thing in common &ndash; they want to be challenged; to be held accountable for their actions and to be stretched and grow personally as well as gaining help in growing their business. &nbsp;</p>\r\n<p>Programmes can be structured in a bespoke manner but usually consist of a series of meetings face-to-face in your offices (and currently via Zoom). In these meetings, identification and prioritisation of the issues facing the business is key, followed by a road-map of actions on which you will be held accountable.</p>', '<p>&nbsp; &nbsp; &nbsp;</p>', '/storage/thumbnails/3YtO3qaPPgvsyjWLjC80g0egB48q6KYqb4sdDssS.jpeg', 'storage/banners/vGussqhHk2zfj0wDyEDoLCZho9Dn5kHBug0ljoWJ.jpeg', NULL, '19', '0', '<p><span style=\"font-family: Calibri, sans-serif; font-size: 14.666666984558105px;\">Follow this website link</span></p>', 0, 1, 1, 'info@remarkablepractice.co.uk', NULL, '<p>Remarkable Practice is the business through which Paul Shrimpling supports, and is shifting, the accountancy industry.</p>', 1, '2021-01-11 13:50:59', '2021-01-27 13:27:56', NULL, NULL, '01773821689', NULL),
(33, NULL, 'Accountants Growth Academy (AGA)', '<p>Working with groups of ambitious accountants has proved to be an exceptionally rewarding way to add value, with learning not only coming from Paul and the team but also the other firms in the room. These relationships help accountants see they&rsquo;re faced with common issues, allowing them to focus on skills development and habitualising those skills. The group sessions focus on what has made other firms successful, distilling best practice into workable solutions for all firms. &nbsp; &nbsp;</p>\r\n<p>AGA groups meet 4 times a year, two face-to-face and two by Zoom, with individual accountability sessions ensuring actions are followed through.</p>', '<p>&nbsp; &nbsp;</p>', '/storage/thumbnails/C1a655KTcEodKUQkSbb6bDvE6k8vU2xzkgszZCVM.jpeg', 'storage/banners/iJgHQx1bcLOT22dc424hLWUDagUYp275wA6OmvSo.jpeg', NULL, '19', '0', '<p><span style=\"font-family: Calibri, sans-serif; font-size: 14.666666984558105px;\">Follow this website link: &nbsp;</span>https://www.remarkablepractice.com/product/aga-workshop-firstday/</p>', 0, 1, 1, 'info@remarkablepractice.com', '<p>Remarkable Practice is the business through which Paul Shrimpling supports, and is shifting, the accountancy industry.</p>', '<p>Paul Shrimpling has almost 20 years working with accountants, he&nbsp;is an author and renowned speaker, and is well-respected in the accountancy industry.</p>', 1, '2021-01-11 15:15:05', '2021-01-12 10:59:00', NULL, NULL, '01773 821 689', NULL);
INSERT INTO `benefits` (`id`, `page_id`, `name`, `description`, `boosted_description`, `thumbnail`, `banner`, `price`, `provider_id`, `featured`, `redeem_text`, `includes_file_manager`, `includes_benefit_phone_button`, `includes_benefit_form`, `provider_email`, `provider_description`, `provider_usp`, `unique`, `created_at`, `updated_at`, `partner_percentage_cut`, `provider_percentage_cut`, `provider_telephone`, `provider_nochex_id`) VALUES
(34, NULL, 'Managing Partners’ Inner Circle (MPIC)', '<p>Running an accountancy firm can be a lonely job. Where do we turn to in moments of uncertainty? Who mentors us when we&rsquo;re being relied on to mentor others? The answer lies in the Managing Partners&rsquo; Inner Circle &ndash; a group of ambitious managing partners who realise the power in sharing thoughts, ideas and issues with others in the same role. Facilitated by the RP team, the MPIC focuses on trends; market developments and the best strategies to help grow our firms. Alongside this is a strong accountability focus with peer review and challenge.&nbsp;</p>\r\n<p>The MPIC meets six times per annum via a mix of offline and online. Usually preceded by a dinner, the forum is designed to allow managing partners to work on their business in a safe environment.</p>', '<p>&nbsp;&nbsp;</p>', '/storage/thumbnails/3xrpxXilBFcFCgGj6SsTvVSuJlv1fEvdGWnkFhQc.jpeg', 'storage/banners/Jf9n6CGC8kYICU2oLI6k9rs7MiMYVwhfuZC07pdr.jpeg', NULL, '19', '0', '<p>Follow this website link</p>', 0, 1, 1, 'info@remarkablepractice.com', NULL, NULL, 1, '2021-01-11 15:24:10', '2021-01-11 16:33:47', NULL, NULL, '01773 821 689', NULL),
(35, NULL, 'Business Bitesize', '<p>Ambitious accountants realise that regular communication with clients is paramount in a trusted advisor relationship. But what do we say? How often do we communicate? What if Remarkable Practice could take that problem away from you by providing first-class content at prescribed intervals in a &ldquo;ready-to-send&rdquo; format? Business Bitesize does that for your firm, and in a manner which allows white-labelling and all the work done for you, should you prefer. &nbsp;</p>\r\n<p>Bitesize offers 6 4-page communications per annum on material business issues which will help your clients. Pricing tiers offer three options &ndash; DIY; do it with you; do it for you</p>', '<p>&nbsp; &nbsp;</p>', '/storage/thumbnails/1hEkB6jfbBbLA49XdjQXOjjiqpul32NxXoqOU6Yn.png', 'storage/banners/F1XqcqlXUhEyosT9Lbo2CzOL83Iqv8s2v4fl5eJC.jpeg', NULL, '19', '0', NULL, 0, 1, 1, 'info@remarkablepractice.com', '<p>Remarkable Practice is the business through which Paul Shrimpling supports, and is shifting, the accountancy industry.</p>', '<p>Paul Shrimpling has almost 20 years working with accountants, he&nbsp;is an author and renowned speaker, and is well-respected in the accountancy industry.</p>', 1, '2021-01-11 15:28:57', '2021-01-12 10:59:27', NULL, NULL, '01773 821 689', NULL),
(36, NULL, 'Advance Track Outsourcing', '<p>Outsourcing is now a common tool in an accountant&rsquo;s productivity box. Advance Track have a great track record in helping RP clients achieve their outsourcing goals, allowing firms to expand and grow without worrying about recruitment or labour-shortages.&nbsp;</p>', '<p>&nbsp; &nbsp;</p>', '/storage/thumbnails/ZY2uFkoSKTf44z7sjTVFcmt9eQEooAHrrY4XbEbf.jpeg', 'storage/banners/bpGWT2dhTctm5o22pp8O4xaHMp99OdLCxHgwjTE2.jpeg', NULL, '20', '0', NULL, 0, 1, 1, 'advancetrack@remarkablepractice.com', '<h4>Helping you, as an accountant, confidently choose between outsourcing and offshoring</h4>', '<p>Advance Track and Remarkable Practice have worked together for a number of years and see a great cultural and values fit.</p>', 1, '2021-01-11 15:58:16', '2021-01-11 16:01:59', NULL, NULL, '01773 821 689', NULL),
(37, NULL, 'myworkpapers', '<p>Myworkpapers is a powerful workflow tool for ambitious accountants looking to change the way they work, creating efficiencies and removing workflow blockages.</p>', '<p>&nbsp; &nbsp;</p>', '/storage/thumbnails/j94lZVRsoPIB68oIVOBHw76J28jJtwLDCPM4Wy6l.png', 'storage/banners/XnqM1noBou5yktwr7pWqmVoDfITq3ejRnLDLEfoE.png', NULL, '21', '0', NULL, 0, 1, 1, 'myworkpapers@remarkablepractice.com', '<p>MyWorkpapers is the powerful accounting solution designed for accountants. By redesigning workflow, eliminating admin and boosting productivity, we are transforming the way accountants work.</p>', '<p><span style=\"font-family: Calibri, sans-serif; font-size: 14.666666984558105px;\">Myworkpapers is the largest provider in this space in the UK and have shown exemplary flexibility and client-focus</span></p>', 1, '2021-01-11 16:14:05', '2021-01-11 16:15:16', NULL, NULL, '01773 821 689', NULL),
(38, NULL, 'Satago', '<p>Cashflow is the lifeblood of any business. Easing the cashflow for ambitious accountants means you free up time to spend in other client-facing areas</p>', '<p>&nbsp; &nbsp;</p>', '/storage/thumbnails/VIXFqIgW8jASbmbc9J3ppWmmO3funmHvrctdp02L.png', 'storage/banners/jKuHedp7ebq5saCUDa91a2ecReh38rSKRRO0sObu.jpeg', NULL, '22', '0', NULL, 0, 1, 1, 'satago@remarkablepractice.com', '<p><strong>Forward-thinking cash management for businesses and their accountants</strong></p>\r\n<p>Risk insights and credit control make getting paid on time easy. Flexible finance gives you access to cash when you need it.</p>', '<p><span style=\"font-family: Calibri, sans-serif; font-size: 14.666666984558105px;\">Satago offer a complete cash solution and have superb client feedback</span></p>', 1, '2021-01-11 16:23:23', '2021-01-11 16:23:39', NULL, NULL, '01773821689', NULL),
(39, NULL, 'Probate Property Services', '<p>Our range of services includes unoccupied property insurance, property valuations and marketing reports, contents valuations, property repairs and general property and gardening maintenance, property sales and our unique added value, fully funded renovation scheme.</p>\r\n<p>&nbsp;We can then work with your Executors to help with the sale of a vacant probate property by providing:</p>\r\n<p>&nbsp;</p>\r\n<ul style=\"margin-bottom: 0cm; margin-top: 0cm;\" type=\"disc\">\r\n<li>A free, no-obligation and comprehensive report, along with recommended marketing strategy advice.</li>\r\n<li>Independent, HMRC compliant valuations from the best estate agents to ensure top sales for each property.&nbsp;</li>\r\n<li class=\"MsoNormal\">Qualify every offer along with any property chains that might be involved.</li>\r\n</ul>', '<p>&nbsp;&nbsp;</p>', 'storage/thumbnails/S6V4IK99dOtPMOo9KH4HKvxwxHv3mCQmOOdXtBfm.jpeg', 'storage/banners/UYQZMjjufT6sqQEhEvczbNubiHPwyKaYLKj2VqbK.jpeg', NULL, '24', '0', NULL, 0, 0, 0, 'dominic@lsgi.co.uk', '<p>&nbsp;&nbsp;</p>', '<p class=\"MsoNormal\" style=\"margin: 0cm; font-size: 11pt; font-family: Calibri, sans-serif;\">Aventria delivers a professional, time-saving answer to the complicated issues that can arise when property forms part of an estate, particularly if the property in question is not local.</p>', 1, '2021-01-29 16:24:54', '2021-02-01 10:42:17', NULL, NULL, '0300 123 4567', NULL),
(40, NULL, 'Barrister Review Service', '<p>For maximum comfort that the Will you produced online is going to do what you want to do when you need to do it, we can instruct a Barrister to review your Last Will &amp; Testament with their fully insured legal advice.&nbsp;</p>', '<p>&nbsp; &nbsp;</p>', 'storage/thumbnails/GXCT2ZC8nnlnkMOzVC57LeMfoHZxvPmXSFhuZbyq.jpeg', 'storage/banners/ZuxyTZSwjKodwltZLPk9sIw7ZswcGwXXGxW4ggN6.jpeg', NULL, '25', '0', NULL, 0, 1, 1, 'dominic@lsgi.co.uk', '<p>&nbsp; &nbsp;&nbsp;</p>', '<p class=\"MsoNormal\" style=\"margin: 0cm; font-size: 11pt; font-family: Calibri, sans-serif; line-height: 15.693333625793457px;\">NWJ Legal has over 20 years of experience providing services to advisers and to private clients in the field of estate planning.&nbsp;</p>\r\n<p class=\"MsoNormal\" style=\"margin: 0cm; font-size: 11pt; font-family: Calibri, sans-serif; line-height: 15.693333625793457px;\">&nbsp;</p>', 1, '2021-01-29 16:30:44', '2021-02-01 11:12:29', NULL, NULL, '0300 123 3456', NULL),
(41, NULL, 'Estate Planning Review', '<p><span lang=\"EN-US\">We firmly believe that helping our customers ensure those wishes will always be fulfilled isn&rsquo;t about simply producing a legal document, but is the start of a relationship so that, as your circumstances change or you need to update parts of your planning, we can be there for you to help adapt planning.&nbsp;&nbsp;</span></p>\r\n<p><span lang=\"EN-US\">Your Estate Planning Coordinator is available to carry out a free review of your situation now, once a year or whenever you feel the need for one.</span></p>\r\n<p><span lang=\"EN-US\">By knowing we will be there if you ever need to change or update things, we hope we can give you the peace of mind that your planning will meet the needs of you and your family when the time comes.&nbsp;</span></p>\r\n<p><span lang=\"EN-US\">We simply run through a series of questions with you over the phone that are designed to make certain of your wishes, then we consider certain events that could affect your planning and give you the peace of mind that you&rsquo;ve covered all bases and explored all your options.&nbsp;</span></p>', '<p>&nbsp; &nbsp;</p>', '/storage/thumbnails/kBZ3QkDQCUGX3QYbFobfOtmhRKhQ5lOGeNsmlLti.jpeg', 'storage/banners/3JZ2cSO7XEU5YSCKgdjCZo9Kfld28GJs3njkoCq9.jpeg', NULL, '23', '0', NULL, 0, 1, 1, 'dominic@lsgi.co.uk', '<p>&nbsp; &nbsp;</p>', '<p>As part of our service our customers we have made available to you your very own Estate Planning Coordinator whose role is to help you be certain that your wishes will be fulfilled.&nbsp;&nbsp;</p>\r\n<p class=\"paragraph\" style=\"margin: 0cm; font-family: \'Times New Roman\', serif; vertical-align: baseline;\">&nbsp;</p>', 1, '2021-01-29 16:40:26', '2021-02-01 14:07:15', NULL, NULL, '0300 123 456', NULL),
(42, NULL, 'Care Package', '<p><strong><span lang=\"EN-US\">Will Storage</span></strong><span lang=\"EN-US\">&nbsp;&ndash; The security to know your Will is safe from damage, loss or being misplaced in a Hard Copy Will storage service.</span></p>\r\n<p><strong>Digital Vault</strong>&nbsp;- Being certain vital digital documents, such as your Will, can be kept safe and secure but accessible to those you trust at the time when they will need to access them can bring peace of mind and certainty that your wishes will be respected.</p>\r\n<p><strong><span lang=\"EN-US\">Admin Updates</span></strong><span lang=\"EN-US\">&nbsp;&ndash; whenever changes in someone&rsquo;s circumstances simply call us and, where it is required, we will update your Will for you as part of a free review.</span></p>\r\n<p><strong><span lang=\"EN-US\">50% Discount on upgraded planning</span></strong><span lang=\"EN-US\">&nbsp;&ndash; if, after 12 months of enrolling into the Care Package, you feel you want to put in place more robust planning we will take 50% off the standard planning fees for the planning to you elect to put in place.</span></p>\r\n<p><strong><span lang=\"EN-US\">Executor Assistance</span></strong><span lang=\"EN-US\">&nbsp;</span><span lang=\"EN-US\">&ndash; when the time comes your executors will be able to access free advice and support that will help them decide if they need help with any estate administration or whether they can cope with the task themselves.&nbsp;</span></p>', '<p>&nbsp; &nbsp;</p>', 'storage/thumbnails/aCQBj8TJCnaLQpZAKEu7SXtgtyyRfW2IjuYlIkRa.jpeg', 'storage/banners/JdrVIw5CHVxQFXqujx4IN10gbIk8hrbm1ppsBYNl.jpeg', NULL, '23', '0', '<p>&nbsp;&nbsp;</p>', 0, 1, 1, 'dominic@lsgi.co.uk', '<p>&nbsp;&nbsp;</p>', '<p>This selected collection of benefits and services can be accessed at a discounted price, and will enable you to keep your planning safe, secure and accessible for you and the people you trust.</p>', 1, '2021-02-01 10:28:24', '2021-02-01 13:47:24', NULL, NULL, '03455481230', NULL),
(43, NULL, 'Financial Fitness Report', '<p>With so much uncertainty at the moment, it might be time to think about your financial plans. Our straightforward Financial Fitness Report could help you to make sense of your finances.</p>\r\n<p>We\'ve found that 30% of people are worried about the future of their finances, because they don\'t have everything in order*. Having a Financial Fitness Report with us could help. It starts with a 30-minute conversation over the phone - you don\'t even need to leave the comfort of your home.</p>\r\n<p>At the end of the conversation, you&rsquo;ll receive a free report outlining the four steps that form the basis of our financial planning principles, and steps you could consider to improve your financial fitness. You\'ll also receive an insightful guide containing further information to help you with your next steps.</p>\r\n<p>*2000 UK participants were asked, research carried by One Poll for Skipton Building Society in July, 2020.</p>', '<p>&nbsp; &nbsp;&nbsp;</p>', '/storage/thumbnails/xZZzuUUV1XkHjnYQyElr8V2sNcHPaLmj8nGzPt3Y.jpeg', 'storage/banners/kgJqmBNEdQkWNb7waaY01IlnebYU9Muo2iq2X4gN.jpeg', NULL, '26', '0', NULL, 0, 1, 1, 'skipton@lsgi.co.uk', '<p>&nbsp; &nbsp;</p>', '<ul>\r\n<li>30 years&rsquo; experience in financial planning</li>\r\n<li>Financial review in your home, branch or via&nbsp;video link on your computer/tablet</li>\r\n<li>Qualified local advisers</li>\r\n<li>Personalised recommendations</li>\r\n</ul>', 1, '2021-02-18 09:55:22', '2021-02-18 10:01:24', NULL, NULL, '0234344454', NULL),
(44, NULL, 'Will Drafting Service', '<p>The simple, straightforward way to make your wishes known and probably one of the most important documents you&rsquo;ll ever own. Not only can it help safeguard your family and all the valuable things that you spend your life working so hard for, but it will also make sure they go to exactly where you want.</p>\r\n<p>Many people die without making a Will; this can cause real problems for loved ones because the deceased&rsquo;s true wishes are unknown. If you die without making a Will, you are deemed to have died &lsquo;intestate&rsquo; and the law &ndash; rather than you &ndash; decides who inherits your estate.</p>\r\n<p>Redstone Wills is a trusted Will writing service as a subsidiary of Skipton Building Society and are also a member of the Society of Will Writers. Our service include both Single and Joint or Mirror Wills</p>', '<p>&nbsp; &nbsp;</p>', '/storage/thumbnails/PplW517Mrk7m8uGRdVtxget6WjFFMCNgLMLgqkZS.jpeg', 'storage/banners/SpAinhtZHzGIec83hZ9vaqvstiNRIprEhMMz5tpl.jpeg', NULL, '4', '0', '<p class=\"MsoNormal\" style=\"text-align: center;\">To take advantage of this benefit and receive your free Ordinary Power of Attorney and/or Living Will follow the steps below and use&nbsp;<strong>voucher code TB100</strong>:&nbsp;</p>\r\n<ol style=\"text-align: center;\">\r\n<li>Click the redeem button to be taken to our document portal.</li>\r\n<li>Enter your email address, name and set a secure password to create your account.</li>\r\n<li>Answer the setup questions, which form the basis of your legal documents.&nbsp;</li>\r\n<li>Follow the step-by-step process to create your Ordinary Power of Attorney and/or Living Will. You will see the standard prices for these are:\r\n<p class=\"MsoNormal\">&nbsp;</p>\r\n<p class=\"MsoListParagraphCxSpFirst\" style=\"text-indent: -18.0pt; mso-list: l0 level1 lfo1;\"><span style=\"font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;\">&middot;<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span>Ordinary Power of Attorney - &pound;48</p>\r\n<p class=\"MsoListParagraphCxSpLast\" style=\"text-indent: -18.0pt; mso-list: l0 level1 lfo1;\"><span style=\"font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;\">&middot;<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span>Living Will - &pound;55</p>\r\n<p class=\"MsoNormal\">However, when asked for payment use your exclusive Trust Box discount code&nbsp;<strong>TB100</strong>&nbsp;to create these vital Estate Planning documents for&nbsp;<strong>free</strong>.</p>\r\n</li>\r\n</ol>\r\n<p style=\"text-align: center;\"><a class=\"btn btn-primary\" href=\"https://estateplanning.yourtrustbox.co.uk/\" target=\"_blank\" rel=\"noopener\">Redeem this benefit</a></p>', 0, 0, 0, 'redstone@lsgi.co.uk', '<p>&nbsp; &nbsp;&nbsp;</p>', '<p>At Redstone, we&rsquo;re specialists in our field and have the ability to offer our customers with a wide variety of Will writing services, from making a Will or Living Will, to Lasting &amp; General Power of Attorney, we always provide our customers with the highest levels of customer service.</p>', 1, '2021-02-18 10:45:33', '2021-02-18 12:49:27', NULL, NULL, '03453454567', NULL),
(45, NULL, 'Trust Box', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras non urna vehicula, porta lectus ac, ullamcorper orci. Aliquam posuere urna erat, blandit viverra enim commodo quis. Maecenas vel tortor convallis, suscipit purus tincidunt, ornare sem. Donec urna orci, finibus sit amet mattis nec, gravida nec tortor. Fusce commodo rhoncus est, a lacinia turpis interdum at. Maecenas ornare porta nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis in auctor elit. Phasellus at aliquet mauris, porttitor tristique nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi gravida vestibulum ex, nec varius ipsum maximus eu. Donec non purus a quam eleifend porttitor. In tempor, ipsum vel tincidunt tempor, turpis dui vehicula lectus, sit amet condimentum metus arcu ut libero.</p>', '<p>&nbsp; &nbsp;</p>', '/storage/thumbnails/JAp9M8jn8cWiAhbLM1U4A5oyUQufwGMdIf2EvypP.png', 'storage/banners/f6adMtWoTirbKctodxuqpU442IUEpW1GKqsZ432B.jpeg', NULL, '2', '0', '<p>Follow this link</p>', 0, 0, 0, 'dominic@lsgi.co.uk', NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras non urna vehicula, porta lectus ac, ullamcorper orci.&nbsp;</p>', 1, '2021-02-22 12:22:30', '2021-02-22 12:26:10', NULL, NULL, '03453456789', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `benefit_contacts`
--

CREATE TABLE `benefit_contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `customer_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `benefit_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enquiry` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `benefit_contacts`
--

INSERT INTO `benefit_contacts` (`id`, `user_id`, `customer_number`, `benefit_name`, `first_name`, `last_name`, `telephone`, `email`, `enquiry`, `created_at`, `updated_at`) VALUES
(1, 154, 'C941274889', 'Money Management Made Easy', 'sam', 'Mawhinney', '07725486899', 'sam@trusttech42.co.uk', 'I would like to redeem this benefit, please contact me via phone or email with more information. Thanks', '2020-10-30 16:37:46', '2020-10-30 16:37:46'),
(2, 154, 'C941274889', 'Money Management Made Easy', 'sam', 'Mawhinney', '07725486899', 'sam@trusttech42.co.uk', 'I would like to redeem this benefit, please contact me via phone or email with more information. Thanks', '2020-10-30 16:42:44', '2020-10-30 16:42:44'),
(3, 156, 'C401524303', 'Money Management Made Easy', 'jon', 'irvine', '07412693001', 'jonathan@visionsharp.co.uk', 'I would like to redeem this benefit, please contact me via phone or email with more information. Thanks', '2020-11-03 16:42:12', '2020-11-03 16:42:12'),
(4, 160, 'C167543387', 'Money Management Made Easy', 'sam', 'Mawhinney', '07464948863', 'sam@trusttech42.co.uk', 'I would like to redeem this benefit, please contact me via phone or email with more information. Thanks', '2020-11-05 13:05:10', '2020-11-05 13:05:10'),
(5, 160, 'C167543387', 'Money Management Made Easy', 'sam', 'Mawhinney', '07464948863', 'sam@trusttech42.co.uk', 'I would like to redeem this benefit, please contact me via phone or email with more information. Thanks', '2020-11-05 13:12:16', '2020-11-05 13:12:16'),
(6, 163, 'C892925803', 'Money Management Made Easy', 'Adam', 'Johnson', '07412693001', 'luke@visionsharp.co.uk', 'I would like to redeem this benefit, please contact me via phone or email with more information. Thanks', '2020-11-05 15:04:31', '2020-11-05 15:04:31'),
(7, 163, 'C892925803', 'Money Management Made Easy', 'Adam', 'Johnson', '07412693001', 'luke@visionsharp.co.uk', 'I would like to redeem this benefit, please contact me via phone or email with more information. Thanks', '2020-11-05 15:23:19', '2020-11-05 15:23:19'),
(8, 156, 'C401524303', 'Money Management Made Easy', 'jon', 'irvine', '07412693001', 'jonathan@visionsharp.co.uk', 'I would like to redeem this benefit, please contact me via phone or email with more information. Thanks', '2020-11-05 15:25:58', '2020-11-05 15:25:58');

-- --------------------------------------------------------

--
-- Table structure for table `benefit_partner`
--

CREATE TABLE `benefit_partner` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `benefit_id` int(11) NOT NULL,
  `partner_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `benefit_partner`
--

INSERT INTO `benefit_partner` (`id`, `benefit_id`, `partner_id`, `created_at`, `updated_at`) VALUES
(1, 4, 2, NULL, NULL),
(3, 4, 4, NULL, NULL),
(4, 5, 5, NULL, NULL),
(5, 4, 5, NULL, NULL),
(6, 6, 5, NULL, NULL),
(7, 7, 6, NULL, NULL),
(9, 8, 5, NULL, NULL),
(10, 9, 5, NULL, NULL),
(11, 10, 6, NULL, NULL),
(12, 11, 6, NULL, NULL),
(13, 12, 6, NULL, NULL),
(14, 13, 6, NULL, NULL),
(15, 14, 6, NULL, NULL),
(16, 15, 6, NULL, NULL),
(17, 16, 6, NULL, NULL),
(18, 17, 6, NULL, NULL),
(19, 18, 6, NULL, NULL),
(21, 19, 6, NULL, NULL),
(23, 4, 8, NULL, NULL),
(27, 22, 9, NULL, NULL),
(28, 23, 9, NULL, NULL),
(29, 24, 9, NULL, NULL),
(30, 2, 5, NULL, NULL),
(31, 2, 4, NULL, NULL),
(32, 2, 10, NULL, NULL),
(33, 25, 10, NULL, NULL),
(34, 26, 10, NULL, NULL),
(35, 27, 10, NULL, NULL),
(36, 28, 10, NULL, NULL),
(37, 29, 10, NULL, NULL),
(38, 30, 10, NULL, NULL),
(39, 31, 10, NULL, NULL),
(41, 20, 9, NULL, NULL),
(43, 32, 11, NULL, NULL),
(44, 33, 11, NULL, NULL),
(45, 34, 11, NULL, NULL),
(46, 35, 11, NULL, NULL),
(47, 36, 11, NULL, NULL),
(48, 37, 11, NULL, NULL),
(49, 38, 11, NULL, NULL),
(50, 2, 12, NULL, NULL),
(51, 25, 12, NULL, NULL),
(52, 28, 12, NULL, NULL),
(53, 25, 2, NULL, NULL),
(54, 39, 12, NULL, NULL),
(55, 40, 12, NULL, NULL),
(56, 41, 12, NULL, NULL),
(57, 8, 12, NULL, NULL),
(59, 42, 12, NULL, NULL),
(61, 29, 12, NULL, NULL),
(62, 43, 9, NULL, NULL),
(63, 44, 9, NULL, NULL),
(64, 45, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `benefit_user`
--

CREATE TABLE `benefit_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `benefit_id` int(11) NOT NULL,
  `boosted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `benefit_user`
--

INSERT INTO `benefit_user` (`id`, `user_id`, `benefit_id`, `boosted`, `created_at`, `updated_at`) VALUES
(11, 34, 4, 0, NULL, NULL),
(13, 23, 4, 0, NULL, NULL),
(14, 62, 4, 0, NULL, NULL),
(15, 63, 4, 0, NULL, NULL),
(16, 64, 4, 0, NULL, NULL),
(18, 71, 4, 0, NULL, NULL),
(19, 72, 4, 0, NULL, NULL),
(20, 73, 4, 0, NULL, NULL),
(22, 74, 4, 0, NULL, NULL),
(23, 78, 4, 0, NULL, NULL),
(24, 79, 4, 0, NULL, NULL),
(25, 90, 4, 0, NULL, NULL),
(26, 91, 4, 0, NULL, NULL),
(27, 92, 4, 0, NULL, NULL),
(28, 97, 4, 0, NULL, NULL),
(29, 95, 4, 0, NULL, NULL),
(33, 94, 4, 0, NULL, NULL),
(34, 105, 4, 0, NULL, NULL),
(35, 108, 4, 0, NULL, NULL),
(36, 111, 4, 0, NULL, NULL),
(38, 110, 4, 0, NULL, NULL),
(39, 113, 4, 0, NULL, NULL),
(40, 112, 10, 0, NULL, NULL),
(41, 106, 4, 0, NULL, NULL),
(42, 115, 4, 0, NULL, NULL),
(43, 116, 4, 0, NULL, NULL),
(44, 123, 4, 0, NULL, NULL),
(45, 124, 4, 0, NULL, NULL),
(46, 125, 4, 0, NULL, NULL),
(47, 127, 4, 0, NULL, NULL),
(48, 128, 4, 0, NULL, NULL),
(49, 129, 4, 0, NULL, NULL),
(50, 86, 4, 0, NULL, NULL),
(51, 130, 4, 0, NULL, NULL),
(52, 131, 4, 0, NULL, NULL),
(53, 133, 4, 0, NULL, NULL),
(54, 135, 4, 0, NULL, NULL),
(55, 136, 4, 0, NULL, NULL),
(56, 139, 5, 1, NULL, NULL),
(57, 139, 4, 0, NULL, NULL),
(58, 138, 20, 0, NULL, NULL),
(59, 138, 21, 0, NULL, NULL),
(60, 138, 22, 0, NULL, NULL),
(61, 138, 23, 0, NULL, NULL),
(62, 141, 5, 0, NULL, NULL),
(63, 141, 4, 0, NULL, NULL),
(64, 141, 6, 0, NULL, NULL),
(65, 145, 5, 0, NULL, NULL),
(66, 145, 4, 0, NULL, NULL),
(67, 145, 6, 0, NULL, NULL),
(68, 146, 5, 0, NULL, NULL),
(69, 146, 4, 0, NULL, NULL),
(70, 146, 6, 0, NULL, NULL),
(72, 34, 2, 1, NULL, NULL),
(73, 150, 5, 1, NULL, NULL),
(74, 150, 8, 0, NULL, NULL),
(75, 148, 4, 0, NULL, NULL),
(78, 154, 5, 0, NULL, NULL),
(79, 154, 8, 0, NULL, NULL),
(80, 154, 2, 0, NULL, NULL),
(81, 148, 5, 0, NULL, NULL),
(82, 148, 6, 0, NULL, NULL),
(83, 156, 5, 0, NULL, NULL),
(84, 154, 6, 0, NULL, NULL),
(85, 156, 6, 1, NULL, NULL),
(86, 157, 5, 0, NULL, NULL),
(87, 157, 4, 0, NULL, NULL),
(88, 157, 6, 0, NULL, NULL),
(89, 157, 8, 0, NULL, NULL),
(90, 158, 5, 1, NULL, NULL),
(97, 155, 5, 1, NULL, NULL),
(98, 155, 6, 1, NULL, NULL),
(99, 155, 8, 1, NULL, NULL),
(100, 155, 9, 0, NULL, NULL),
(101, 150, 6, 1, NULL, NULL),
(102, 150, 4, 0, NULL, NULL),
(103, 160, 5, 1, NULL, NULL),
(104, 160, 6, 0, NULL, NULL),
(105, 163, 5, 0, NULL, NULL),
(106, 164, 5, 1, NULL, NULL),
(107, 164, 6, 1, NULL, NULL),
(108, 164, 2, 1, NULL, NULL),
(109, 165, 2, 0, NULL, NULL),
(110, 163, 2, 0, NULL, NULL),
(111, 166, 25, 0, NULL, NULL),
(112, 166, 2, 1, NULL, NULL),
(113, 166, 26, 0, NULL, NULL),
(114, 166, 27, 0, NULL, NULL),
(115, 156, 2, 0, NULL, NULL),
(116, 167, 2, 1, NULL, NULL),
(117, 164, 9, 0, NULL, NULL),
(118, 164, 8, 0, NULL, NULL),
(136, 167, 5, 0, NULL, NULL),
(137, 171, 2, 0, NULL, NULL),
(158, 175, 2, 0, NULL, NULL),
(159, 175, 26, 0, NULL, NULL),
(160, 166, 28, 0, NULL, NULL),
(167, 187, 2, 0, NULL, NULL),
(168, 63, 22, 0, NULL, NULL),
(170, 176, 2, 1, NULL, NULL),
(171, 176, 6, 1, NULL, NULL),
(172, 176, 8, 1, NULL, NULL),
(173, 176, 9, 1, NULL, NULL),
(174, 176, 5, 0, NULL, NULL),
(176, 189, 20, 0, NULL, NULL),
(184, 192, 2, 0, NULL, NULL),
(185, 195, 2, 0, NULL, NULL),
(186, 197, 32, 0, NULL, NULL),
(187, 197, 2, 0, NULL, NULL),
(188, 197, 33, 0, NULL, NULL),
(198, 191, 35, 0, NULL, NULL),
(199, 191, 34, 0, NULL, NULL),
(200, 195, 32, 0, NULL, NULL),
(201, 187, 25, 0, NULL, NULL),
(202, 187, 8, 0, NULL, NULL),
(203, 62, 2, 0, NULL, NULL),
(204, 202, 2, 0, NULL, NULL),
(205, 62, 20, 0, NULL, NULL),
(206, 62, 23, 0, NULL, NULL),
(207, 62, 41, 0, NULL, NULL),
(208, 204, 44, 0, NULL, NULL),
(209, 199, 5, 1, NULL, NULL),
(210, 199, 6, 0, NULL, NULL),
(211, 187, 45, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blocks`
--

CREATE TABLE `blocks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `page_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `template` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `sort_order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blocks`
--

INSERT INTO `blocks` (`id`, `page_id`, `name`, `template`, `content`, `sort_order`, `created_at`, `updated_at`) VALUES
(1, 2, 'Text', 'block-text', '<div class=\"container block block-text\"><div class=\"row\"><div class=\"col-8 offset-2\"><h1 class=\"h1 member-title\">Login / Register</h1><br></div></div></div>', 1, '2020-04-29 11:23:29', '2020-05-04 15:04:29'),
(2, 2, 'Split', 'block-split', '<div data-v-56e51f70=\"\" class=\"container block block-split py-4\"><div data-v-56e51f70=\"\" class=\"row\"><div data-v-56e51f70=\"\" class=\"col block-center\"><div data-v-56e51f70=\"\" class=\"a-center bg-light p-5 rounded\"><h1 data-v-56e51f70=\"\" class=\"h2 member-title\">Already a member? Log in here</h1> <a data-v-56e51f70=\"\" href=\"login\" class=\"btn btn-primary mt-3\">Click Here</a></div></div> \n                            <div data-v-56e51f70=\"\" class=\"col block-center\"><div data-v-56e51f70=\"\" class=\"a-center bg-member text-white p-5 rounded\"><h1 data-v-56e51f70=\"\" class=\"h2\">New member? Register here</h1> <a data-v-56e51f70=\"\" href=\"register\" class=\"btn btn-default bg-white text-member mt-3\">Click Here</a></div></div></div></div>', 2, '2020-04-29 11:23:29', '2020-05-04 15:04:29');

-- --------------------------------------------------------

--
-- Table structure for table `changelogs`
--

CREATE TABLE `changelogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `action` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `changelogs`
--

INSERT INTO `changelogs` (`id`, `user_id`, `action`, `created_at`, `updated_at`) VALUES
(1, 3, 'User updated their information', '2020-04-29 12:14:45', '2020-04-29 12:14:45'),
(2, 3, 'User has logged in', '2020-04-29 12:15:41', '2020-04-29 12:15:41'),
(3, 5, 'User updated their information', '2020-04-30 12:05:23', '2020-04-30 12:05:23'),
(4, 7, 'User has logged in', '2020-04-30 12:18:27', '2020-04-30 12:18:27'),
(5, 7, 'User has logged in', '2020-04-30 12:27:11', '2020-04-30 12:27:11'),
(6, 5, 'User has logged in', '2020-04-30 12:28:02', '2020-04-30 12:28:02'),
(7, 7, 'User has logged in', '2020-04-30 12:31:20', '2020-04-30 12:31:20'),
(8, 5, 'User has logged in', '2020-04-30 12:34:55', '2020-04-30 12:34:55'),
(9, 3, 'User has logged in', '2020-04-30 13:04:04', '2020-04-30 13:04:04'),
(10, 9, 'User has logged in', '2020-04-30 13:07:05', '2020-04-30 13:07:05'),
(11, 5, 'User has logged in', '2020-04-30 13:31:48', '2020-04-30 13:31:48'),
(12, 3, 'User has logged in', '2020-04-30 13:33:07', '2020-04-30 13:33:07'),
(13, 3, 'User has logged in', '2020-04-30 14:03:41', '2020-04-30 14:03:41'),
(14, 3, 'User has logged in', '2020-04-30 15:27:26', '2020-04-30 15:27:26'),
(15, 2, 'User has logged in', '2020-04-30 15:39:35', '2020-04-30 15:39:35'),
(16, 11, 'Admin updated user information', '2020-05-01 12:08:10', '2020-05-01 12:08:10'),
(17, 13, 'User has logged in', '2020-05-01 14:33:07', '2020-05-01 14:33:07'),
(18, 5, 'User has logged in', '2020-05-01 14:34:37', '2020-05-01 14:34:37'),
(19, 13, 'User has logged in', '2020-05-01 14:43:18', '2020-05-01 14:43:18'),
(20, 5, 'User has logged in', '2020-05-01 15:11:33', '2020-05-01 15:11:33'),
(21, 13, 'User has logged in', '2020-05-04 11:25:02', '2020-05-04 11:25:02'),
(22, 13, 'User has logged in', '2020-05-04 11:27:13', '2020-05-04 11:27:13'),
(23, 5, 'User has logged in', '2020-05-04 13:51:59', '2020-05-04 13:51:59'),
(24, 2, 'User has logged in', '2020-05-05 13:52:13', '2020-05-05 13:52:13'),
(25, 2, 'User has logged in', '2020-05-05 14:12:05', '2020-05-05 14:12:05'),
(26, 2, 'User has logged in', '2020-05-05 14:12:32', '2020-05-05 14:12:32'),
(27, 13, 'User has logged in', '2020-05-06 10:52:50', '2020-05-06 10:52:50'),
(28, 13, 'User has logged in', '2020-05-06 11:13:31', '2020-05-06 11:13:31'),
(29, 5, 'User has logged in', '2020-05-07 08:57:12', '2020-05-07 08:57:12'),
(30, 5, 'Admin updated user information', '2020-05-07 10:27:33', '2020-05-07 10:27:33'),
(31, 6, 'Admin updated user information', '2020-05-07 10:28:01', '2020-05-07 10:28:01'),
(32, 7, 'Admin updated user information', '2020-05-07 10:28:20', '2020-05-07 10:28:20'),
(33, 8, 'Admin updated user information', '2020-05-07 10:28:41', '2020-05-07 10:28:41'),
(34, 9, 'Admin updated user information', '2020-05-07 10:28:55', '2020-05-07 10:28:55'),
(35, 10, 'Admin updated user information', '2020-05-07 10:29:06', '2020-05-07 10:29:06'),
(36, 16, 'User has logged in', '2020-05-07 12:25:15', '2020-05-07 12:25:15'),
(37, 16, 'User has logged in', '2020-05-07 12:48:11', '2020-05-07 12:48:11'),
(38, 16, 'User has logged in', '2020-05-07 13:00:14', '2020-05-07 13:00:14'),
(39, 16, 'User updated their information', '2020-05-07 13:02:17', '2020-05-07 13:02:17'),
(40, 2, 'User has logged in', '2020-05-07 14:14:25', '2020-05-07 14:14:25'),
(41, 3, 'Admin logged in as user', '2020-05-07 14:26:58', '2020-05-07 14:26:58'),
(42, 16, 'User has logged in', '2020-05-11 11:11:14', '2020-05-11 11:11:14'),
(43, 16, 'User has logged in', '2020-05-11 12:42:32', '2020-05-11 12:42:32'),
(44, 3, 'User has logged in', '2020-05-11 13:49:41', '2020-05-11 13:49:41'),
(45, 3, 'User has logged in', '2020-05-11 14:34:00', '2020-05-11 14:34:00'),
(46, 3, 'User has logged in', '2020-05-11 14:36:44', '2020-05-11 14:36:44'),
(47, 16, 'User has logged in', '2020-05-11 14:42:47', '2020-05-11 14:42:47'),
(48, 17, 'Admin logged in as user', '2020-05-11 15:03:17', '2020-05-11 15:03:17'),
(49, 3, 'Admin logged in as user', '2020-05-11 15:03:43', '2020-05-11 15:03:43'),
(50, 17, 'Admin logged in as user', '2020-05-11 15:07:36', '2020-05-11 15:07:36'),
(51, 17, 'Admin logged in as user', '2020-05-11 15:14:19', '2020-05-11 15:14:19'),
(52, 11, 'Admin logged in as user', '2020-05-11 15:34:22', '2020-05-11 15:34:22'),
(53, 25, 'User has logged in', '2020-05-11 17:33:12', '2020-05-11 17:33:12'),
(54, 23, 'User has logged in', '2020-05-12 11:31:28', '2020-05-12 11:31:28'),
(55, 23, 'User has logged in', '2020-05-12 14:45:30', '2020-05-12 14:45:30'),
(56, 23, 'Admin logged in as user', '2020-05-12 14:55:11', '2020-05-12 14:55:11'),
(57, 23, 'Admin logged in as user', '2020-05-12 15:00:18', '2020-05-12 15:00:18'),
(58, 23, 'Admin logged in as user', '2020-05-12 15:08:06', '2020-05-12 15:08:06'),
(59, 32, 'User has logged in', '2020-05-13 07:49:10', '2020-05-13 07:49:10'),
(60, 30, 'User has logged in', '2020-05-13 08:13:46', '2020-05-13 08:13:46'),
(61, 30, 'User has logged in', '2020-05-13 08:14:12', '2020-05-13 08:14:12'),
(62, 32, 'User has logged in', '2020-05-13 08:36:17', '2020-05-13 08:36:17'),
(63, 32, 'User has logged in', '2020-05-13 09:57:03', '2020-05-13 09:57:03'),
(64, 32, 'User has logged in', '2020-05-13 10:05:26', '2020-05-13 10:05:26'),
(65, 32, 'User has logged in', '2020-05-13 11:52:45', '2020-05-13 11:52:45'),
(66, 32, 'User has logged in', '2020-05-13 11:53:05', '2020-05-13 11:53:05'),
(67, 32, 'User has logged in', '2020-05-13 11:53:22', '2020-05-13 11:53:22'),
(68, 23, 'User has logged in', '2020-05-13 15:09:12', '2020-05-13 15:09:12'),
(69, 34, 'User has logged in', '2020-05-13 15:25:56', '2020-05-13 15:25:56'),
(70, 34, 'User has logged in', '2020-05-13 15:27:10', '2020-05-13 15:27:10'),
(71, 34, 'User has logged in', '2020-05-13 15:39:09', '2020-05-13 15:39:09'),
(72, 34, 'User has logged in', '2020-05-13 15:46:58', '2020-05-13 15:46:58'),
(73, 32, 'User has logged in', '2020-05-13 19:39:07', '2020-05-13 19:39:07'),
(74, 23, 'User has logged in', '2020-05-14 07:57:25', '2020-05-14 07:57:25'),
(75, 34, 'User has logged in', '2020-05-14 08:14:38', '2020-05-14 08:14:38'),
(76, 34, 'User has logged in', '2020-05-14 08:16:23', '2020-05-14 08:16:23'),
(77, 34, 'User has logged in', '2020-05-14 08:16:32', '2020-05-14 08:16:32'),
(78, 34, 'User has logged in', '2020-05-14 08:16:43', '2020-05-14 08:16:43'),
(79, 34, 'User has logged in', '2020-05-14 08:17:25', '2020-05-14 08:17:25'),
(80, 34, 'User has logged in', '2020-05-14 08:21:32', '2020-05-14 08:21:32'),
(81, 41, 'User has logged in', '2020-05-14 08:53:00', '2020-05-14 08:53:00'),
(82, 32, 'User has logged in', '2020-05-14 10:11:05', '2020-05-14 10:11:05'),
(83, 32, 'User has logged in', '2020-05-14 10:11:41', '2020-05-14 10:11:41'),
(84, 34, 'User has logged in', '2020-05-14 15:09:53', '2020-05-14 15:09:53'),
(85, 34, 'User has logged in', '2020-05-15 07:44:05', '2020-05-15 07:44:05'),
(86, 32, 'User has logged in', '2020-05-15 07:44:35', '2020-05-15 07:44:35'),
(87, 32, 'User has logged in', '2020-05-15 07:50:15', '2020-05-15 07:50:15'),
(88, 23, 'User has logged in', '2020-05-15 07:59:17', '2020-05-15 07:59:17'),
(89, 34, 'User has logged in', '2020-05-15 08:32:37', '2020-05-15 08:32:37'),
(90, 55, 'User has logged in', '2020-05-15 09:38:44', '2020-05-15 09:38:44'),
(91, 53, 'User has logged in', '2020-05-15 09:39:56', '2020-05-15 09:39:56'),
(92, 23, 'User has logged in', '2020-05-15 09:53:29', '2020-05-15 09:53:29'),
(93, 32, 'User has logged in', '2020-05-15 10:02:33', '2020-05-15 10:02:33'),
(94, 32, 'User has logged in', '2020-05-15 12:49:35', '2020-05-15 12:49:35'),
(95, 23, 'User has logged in', '2020-05-15 14:07:07', '2020-05-15 14:07:07'),
(96, 34, 'User has logged in', '2020-05-15 15:01:17', '2020-05-15 15:01:17'),
(97, 34, 'User has logged in', '2020-05-15 15:05:42', '2020-05-15 15:05:42'),
(98, 34, 'User has logged in', '2020-05-15 15:11:15', '2020-05-15 15:11:15'),
(99, 23, 'User has logged in', '2020-05-15 15:17:57', '2020-05-15 15:17:57'),
(100, 32, 'User has logged in', '2020-05-18 07:51:50', '2020-05-18 07:51:50'),
(101, 55, 'User has logged in', '2020-05-18 07:53:58', '2020-05-18 07:53:58'),
(102, 32, 'User has logged in', '2020-05-18 08:20:59', '2020-05-18 08:20:59'),
(103, 32, 'User has logged in', '2020-05-18 11:37:57', '2020-05-18 11:37:57'),
(104, 62, 'User has logged in', '2020-05-18 12:01:45', '2020-05-18 12:01:45'),
(105, 55, 'User has logged in', '2020-05-18 13:02:18', '2020-05-18 13:02:18'),
(106, 63, 'User has logged in', '2020-05-19 06:53:31', '2020-05-19 06:53:31'),
(107, 62, 'User has logged in', '2020-05-19 07:24:51', '2020-05-19 07:24:51'),
(108, 55, 'User has logged in', '2020-05-19 08:20:14', '2020-05-19 08:20:14'),
(109, 64, 'User has logged in', '2020-05-19 08:42:49', '2020-05-19 08:42:49'),
(110, 64, 'User updated their information', '2020-05-19 08:43:42', '2020-05-19 08:43:42'),
(111, 62, 'User has logged in', '2020-05-19 08:57:42', '2020-05-19 08:57:42'),
(112, 63, 'Admin updated user information', '2020-05-19 10:24:25', '2020-05-19 10:24:25'),
(113, 32, 'Admin logged in as user', '2020-05-19 11:30:41', '2020-05-19 11:30:41'),
(114, 65, 'User has logged in', '2020-05-19 11:39:03', '2020-05-19 11:39:03'),
(115, 66, 'Admin logged in as user', '2020-05-19 11:55:05', '2020-05-19 11:55:05'),
(116, 68, 'Admin logged in as user', '2020-05-19 12:01:28', '2020-05-19 12:01:28'),
(117, 32, 'User has logged in', '2020-05-19 12:10:47', '2020-05-19 12:10:47'),
(118, 71, 'Admin logged in as user', '2020-05-19 13:28:04', '2020-05-19 13:28:04'),
(119, 72, 'User updated their information', '2020-05-19 13:29:31', '2020-05-19 13:29:31'),
(120, 70, 'User has logged in', '2020-05-19 13:33:20', '2020-05-19 13:33:20'),
(121, 32, 'User has logged in', '2020-05-19 13:37:56', '2020-05-19 13:37:56'),
(122, 75, 'User updated their information', '2020-05-19 14:15:32', '2020-05-19 14:15:32'),
(123, 76, 'User has logged in', '2020-05-19 16:12:52', '2020-05-19 16:12:52'),
(124, 73, 'User has logged in', '2020-05-20 07:25:39', '2020-05-20 07:25:39'),
(125, 78, 'User updated their information', '2020-05-20 08:13:08', '2020-05-20 08:13:08'),
(126, 79, 'User updated their information', '2020-05-20 08:59:52', '2020-05-20 08:59:52'),
(127, 81, 'User updated their information', '2020-05-20 09:25:51', '2020-05-20 09:25:51'),
(128, 82, 'User has logged in', '2020-05-20 09:29:30', '2020-05-20 09:29:30'),
(129, 86, 'User has logged in', '2020-05-20 10:43:36', '2020-05-20 10:43:36'),
(130, 87, 'User has logged in', '2020-05-20 10:46:55', '2020-05-20 10:46:55'),
(131, 88, 'User updated their information', '2020-05-20 10:56:47', '2020-05-20 10:56:47'),
(132, 92, 'User updated their information', '2020-05-20 12:00:46', '2020-05-20 12:00:46'),
(133, 78, 'User has logged in', '2020-05-20 12:36:10', '2020-05-20 12:36:10'),
(134, 100, 'User updated their information', '2020-05-20 14:15:46', '2020-05-20 14:15:46'),
(135, 98, 'User has logged in', '2020-05-20 14:27:11', '2020-05-20 14:27:11'),
(136, 73, 'User has logged in', '2020-05-20 14:31:30', '2020-05-20 14:31:30'),
(137, 95, 'User has logged in', '2020-05-20 14:44:05', '2020-05-20 14:44:05'),
(138, 71, 'User has logged in', '2020-05-20 14:52:49', '2020-05-20 14:52:49'),
(139, 86, 'User has logged in', '2020-05-20 15:10:03', '2020-05-20 15:10:03'),
(140, 87, 'User has logged in', '2020-05-20 15:38:04', '2020-05-20 15:38:04'),
(141, 91, 'User has logged in', '2020-05-20 16:44:32', '2020-05-20 16:44:32'),
(142, 94, 'User has logged in', '2020-05-20 18:44:40', '2020-05-20 18:44:40'),
(143, 92, 'User has logged in', '2020-05-21 06:48:53', '2020-05-21 06:48:53'),
(144, 87, 'User has logged in', '2020-05-21 07:40:07', '2020-05-21 07:40:07'),
(145, 87, 'User has logged in', '2020-05-21 07:41:39', '2020-05-21 07:41:39'),
(146, 101, 'User has logged in', '2020-05-21 07:44:06', '2020-05-21 07:44:06'),
(147, 100, 'User has logged in', '2020-05-21 09:02:22', '2020-05-21 09:02:22'),
(148, 71, 'User has logged in', '2020-05-21 09:12:20', '2020-05-21 09:12:20'),
(149, 34, 'User has logged in', '2020-05-21 10:23:06', '2020-05-21 10:23:06'),
(150, 70, 'User has logged in', '2020-05-21 10:30:59', '2020-05-21 10:30:59'),
(151, 23, 'User has logged in', '2020-05-21 11:09:58', '2020-05-21 11:09:58'),
(152, 78, 'User has logged in', '2020-05-21 12:02:25', '2020-05-21 12:02:25'),
(153, 101, 'Admin logged in as user', '2020-05-21 12:13:51', '2020-05-21 12:13:51'),
(154, 101, 'User has logged in', '2020-05-21 12:53:23', '2020-05-21 12:53:23'),
(155, 32, 'User has logged in', '2020-05-21 12:57:23', '2020-05-21 12:57:23'),
(156, 101, 'User has logged in', '2020-05-21 12:57:41', '2020-05-21 12:57:41'),
(157, 107, 'User has logged in', '2020-05-21 14:42:56', '2020-05-21 14:42:56'),
(158, 99, 'User has logged in', '2020-05-21 14:48:11', '2020-05-21 14:48:11'),
(159, 99, 'User has logged in', '2020-05-21 14:48:23', '2020-05-21 14:48:23'),
(160, 92, 'User has logged in', '2020-05-22 07:31:41', '2020-05-22 07:31:41'),
(161, 107, 'User has logged in', '2020-05-22 11:06:23', '2020-05-22 11:06:23'),
(162, 110, 'User has logged in', '2020-05-22 12:10:49', '2020-05-22 12:10:49'),
(163, 71, 'User has logged in', '2020-05-22 12:34:31', '2020-05-22 12:34:31'),
(164, 112, 'User updated their information', '2020-05-22 12:50:38', '2020-05-22 12:50:38'),
(165, 112, 'User has logged in', '2020-05-22 12:51:08', '2020-05-22 12:51:08'),
(166, 70, 'User has logged in', '2020-05-22 14:28:40', '2020-05-22 14:28:40'),
(167, 95, 'User has logged in', '2020-05-22 14:56:13', '2020-05-22 14:56:13'),
(168, 62, 'User has logged in', '2020-05-26 13:32:30', '2020-05-26 13:32:30'),
(169, 70, 'User has logged in', '2020-05-26 14:29:47', '2020-05-26 14:29:47'),
(170, 70, 'User has logged in', '2020-05-27 08:16:29', '2020-05-27 08:16:29'),
(171, 107, 'User has logged in', '2020-05-27 08:17:56', '2020-05-27 08:17:56'),
(172, 112, 'User has logged in', '2020-05-28 11:56:41', '2020-05-28 11:56:41'),
(173, 70, 'User has logged in', '2020-05-28 20:37:11', '2020-05-28 20:37:11'),
(174, 112, 'User has logged in', '2020-06-01 09:12:51', '2020-06-01 09:12:51'),
(175, 112, 'User has logged in', '2020-06-01 09:16:05', '2020-06-01 09:16:05'),
(176, 112, 'User has logged in', '2020-06-01 09:20:49', '2020-06-01 09:20:49'),
(177, 71, 'User has logged in', '2020-06-01 15:25:43', '2020-06-01 15:25:43'),
(178, 106, 'User has logged in', '2020-06-02 08:42:56', '2020-06-02 08:42:56'),
(179, 112, 'User has logged in', '2020-06-02 13:24:02', '2020-06-02 13:24:02'),
(180, 62, 'User has logged in', '2020-06-03 08:51:48', '2020-06-03 08:51:48'),
(181, 112, 'User has logged in', '2020-06-03 13:52:19', '2020-06-03 13:52:19'),
(182, 64, 'User has logged in', '2020-06-03 17:02:56', '2020-06-03 17:02:56'),
(183, 64, 'User has logged in', '2020-06-05 12:51:27', '2020-06-05 12:51:27'),
(184, 64, 'User updated their information', '2020-06-05 12:51:37', '2020-06-05 12:51:37'),
(185, 62, 'User has logged in', '2020-06-05 14:52:34', '2020-06-05 14:52:34'),
(186, 64, 'User has logged in', '2020-06-07 13:24:49', '2020-06-07 13:24:49'),
(187, 78, 'User has logged in', '2020-06-08 11:35:44', '2020-06-08 11:35:44'),
(188, 117, 'Admin logged in as user', '2020-06-09 08:55:54', '2020-06-09 08:55:54'),
(189, 116, 'Admin logged in as user', '2020-06-09 08:58:45', '2020-06-09 08:58:45'),
(190, 62, 'User has logged in', '2020-06-09 12:22:09', '2020-06-09 12:22:09'),
(191, 64, 'User has logged in', '2020-06-09 16:58:36', '2020-06-09 16:58:36'),
(192, 117, 'User has logged in', '2020-06-10 08:04:31', '2020-06-10 08:04:31'),
(193, 70, 'User has logged in', '2020-06-10 10:31:02', '2020-06-10 10:31:02'),
(194, 72, 'User has logged in', '2020-06-10 14:26:42', '2020-06-10 14:26:42'),
(195, 64, 'User has logged in', '2020-06-11 10:22:55', '2020-06-11 10:22:55'),
(196, 117, 'Admin logged in as user', '2020-06-11 11:57:51', '2020-06-11 11:57:51'),
(197, 15, 'Admin logged in as user', '2020-06-11 12:19:48', '2020-06-11 12:19:48'),
(198, 32, 'Admin logged in as user', '2020-06-12 07:33:42', '2020-06-12 07:33:42'),
(199, 119, 'Admin logged in as user', '2020-06-12 08:12:44', '2020-06-12 08:12:44'),
(200, 119, 'Admin updated user information', '2020-06-12 08:14:01', '2020-06-12 08:14:01'),
(201, 119, 'Admin logged in as user', '2020-06-12 08:14:06', '2020-06-12 08:14:06'),
(202, 119, 'Admin logged in as user', '2020-06-12 08:14:48', '2020-06-12 08:14:48'),
(203, 119, 'Admin updated user information', '2020-06-12 08:16:22', '2020-06-12 08:16:22'),
(204, 119, 'Admin updated user information', '2020-06-12 08:18:07', '2020-06-12 08:18:07'),
(205, 119, 'Admin logged in as user', '2020-06-12 08:18:12', '2020-06-12 08:18:12'),
(206, 119, 'Admin updated user information', '2020-06-12 08:22:48', '2020-06-12 08:22:48'),
(207, 119, 'Admin logged in as user', '2020-06-12 08:22:58', '2020-06-12 08:22:58'),
(208, 120, 'Admin logged in as user', '2020-06-12 08:26:08', '2020-06-12 08:26:08'),
(209, 117, 'Admin logged in as user', '2020-06-12 08:40:10', '2020-06-12 08:40:10'),
(210, 1, 'Admin logged in as user', '2020-06-12 09:29:16', '2020-06-12 09:29:16'),
(211, 117, 'Admin logged in as user', '2020-06-12 09:30:02', '2020-06-12 09:30:02'),
(212, 34, 'User has logged in', '2020-06-14 15:19:40', '2020-06-14 15:19:40'),
(213, 34, 'User has logged in', '2020-06-14 16:11:52', '2020-06-14 16:11:52'),
(214, 34, 'User has logged in', '2020-06-14 17:43:31', '2020-06-14 17:43:31'),
(215, 74, 'User has logged in', '2020-06-16 11:01:19', '2020-06-16 11:01:19'),
(216, 74, 'User updated their information', '2020-06-16 11:16:15', '2020-06-16 11:16:15'),
(217, 82, 'User has logged in', '2020-06-16 11:35:25', '2020-06-16 11:35:25'),
(218, 117, 'Admin logged in as user', '2020-06-16 12:26:58', '2020-06-16 12:26:58'),
(219, 119, 'Admin logged in as user', '2020-06-16 12:29:08', '2020-06-16 12:29:08'),
(220, 120, 'Admin logged in as user', '2020-06-16 12:29:16', '2020-06-16 12:29:16'),
(221, 82, 'User has logged in', '2020-06-16 14:58:45', '2020-06-16 14:58:45'),
(222, 70, 'User has logged in', '2020-06-17 07:53:14', '2020-06-17 07:53:14'),
(223, 117, 'Admin logged in as user', '2020-06-17 08:51:16', '2020-06-17 08:51:16'),
(224, 121, 'Admin logged in as user', '2020-06-17 09:14:47', '2020-06-17 09:14:47'),
(225, 121, 'Admin updated user information', '2020-06-17 09:16:30', '2020-06-17 09:16:30'),
(226, 121, 'Admin logged in as user', '2020-06-17 09:16:42', '2020-06-17 09:16:42'),
(227, 121, 'Admin logged in as user', '2020-06-17 09:18:02', '2020-06-17 09:18:02'),
(228, 120, 'Admin logged in as user', '2020-06-17 09:18:12', '2020-06-17 09:18:12'),
(229, 119, 'Admin logged in as user', '2020-06-17 09:18:20', '2020-06-17 09:18:20'),
(230, 118, 'Admin logged in as user', '2020-06-17 09:18:26', '2020-06-17 09:18:26'),
(231, 112, 'Admin logged in as user', '2020-06-17 09:18:54', '2020-06-17 09:18:54'),
(232, 74, 'User has logged in', '2020-06-17 10:27:38', '2020-06-17 10:27:38'),
(233, 112, 'Admin logged in as user', '2020-06-17 11:06:52', '2020-06-17 11:06:52'),
(234, 72, 'User has logged in', '2020-06-22 08:35:31', '2020-06-22 08:35:31'),
(235, 32, 'User has logged in', '2020-06-22 17:14:23', '2020-06-22 17:14:23'),
(236, 62, 'User has logged in', '2020-06-23 08:42:28', '2020-06-23 08:42:28'),
(237, 74, 'User has logged in', '2020-06-24 12:38:40', '2020-06-24 12:38:40'),
(238, 122, 'User has logged in', '2020-06-30 07:11:43', '2020-06-30 07:11:43'),
(239, 126, 'User updated their information', '2020-06-30 11:28:47', '2020-06-30 11:28:47'),
(240, 62, 'User has logged in', '2020-07-01 12:25:27', '2020-07-01 12:25:27'),
(241, 127, 'User updated their information', '2020-07-07 14:09:03', '2020-07-07 14:09:03'),
(242, 62, 'User has logged in', '2020-07-07 14:09:05', '2020-07-07 14:09:05'),
(243, 127, 'User has logged in', '2020-07-08 07:08:03', '2020-07-08 07:08:03'),
(244, 128, 'User updated their information', '2020-07-08 08:42:31', '2020-07-08 08:42:31'),
(245, 127, 'User has logged in', '2020-07-08 10:13:13', '2020-07-08 10:13:13'),
(246, 86, 'User has logged in', '2020-07-09 09:44:02', '2020-07-09 09:44:02'),
(247, 86, 'User updated their information', '2020-07-09 09:44:10', '2020-07-09 09:44:10'),
(248, 74, 'Admin updated user information', '2020-07-09 12:19:58', '2020-07-09 12:19:58'),
(249, 74, 'Admin updated user information', '2020-07-09 12:21:32', '2020-07-09 12:21:32'),
(250, 62, 'User has logged in', '2020-07-10 08:42:05', '2020-07-10 08:42:05'),
(251, 127, 'User has logged in', '2020-07-10 14:21:34', '2020-07-10 14:21:34'),
(252, 62, 'User has logged in', '2020-07-13 09:11:39', '2020-07-13 09:11:39'),
(253, 127, 'User has logged in', '2020-07-13 09:11:56', '2020-07-13 09:11:56'),
(254, 70, 'User has logged in', '2020-07-14 08:12:33', '2020-07-14 08:12:33'),
(255, 74, 'User has logged in', '2020-07-14 11:27:01', '2020-07-14 11:27:01'),
(256, 131, 'User has logged in', '2020-07-14 16:50:33', '2020-07-14 16:50:33'),
(257, 131, 'User has logged in', '2020-07-14 16:55:28', '2020-07-14 16:55:28'),
(258, 131, 'User has logged in', '2020-07-15 07:55:01', '2020-07-15 07:55:01'),
(259, 24, 'Admin logged in as user', '2020-07-15 08:06:18', '2020-07-15 08:06:18'),
(260, 31, 'Admin updated user information', '2020-07-15 08:10:40', '2020-07-15 08:10:40'),
(261, 31, 'User has logged in', '2020-07-15 08:11:29', '2020-07-15 08:11:29'),
(262, 42, 'Admin updated user information', '2020-07-15 08:13:31', '2020-07-15 08:13:31'),
(263, 1, 'Admin updated user information', '2020-07-15 08:24:38', '2020-07-15 08:24:38'),
(264, 23, 'User has logged in', '2020-07-15 08:30:19', '2020-07-15 08:30:19'),
(265, 1, 'Admin updated user information', '2020-07-15 08:32:52', '2020-07-15 08:32:52'),
(266, 1, 'Admin updated user information', '2020-07-15 08:32:55', '2020-07-15 08:32:55'),
(267, 74, 'User has logged in', '2020-07-16 13:50:24', '2020-07-16 13:50:24'),
(268, 112, 'User has logged in', '2020-07-20 15:36:51', '2020-07-20 15:36:51'),
(269, 131, 'User has logged in', '2020-07-21 12:55:26', '2020-07-21 12:55:26'),
(270, 131, 'Admin updated user information', '2020-07-21 12:57:27', '2020-07-21 12:57:27'),
(271, 120, 'Admin logged in as user', '2020-07-21 12:58:48', '2020-07-21 12:58:48'),
(272, 120, 'Admin logged in as user', '2020-07-21 12:58:56', '2020-07-21 12:58:56'),
(273, 119, 'Admin logged in as user', '2020-07-21 12:59:10', '2020-07-21 12:59:10'),
(274, 120, 'Admin logged in as user', '2020-07-21 12:59:14', '2020-07-21 12:59:14'),
(275, 119, 'Admin logged in as user', '2020-07-21 12:59:21', '2020-07-21 12:59:21'),
(276, 119, 'Admin logged in as user', '2020-07-21 13:01:42', '2020-07-21 13:01:42'),
(277, 1, 'Admin logged in as user', '2020-07-21 13:23:54', '2020-07-21 13:23:54'),
(278, 119, 'Admin logged in as user', '2020-07-21 13:24:14', '2020-07-21 13:24:14'),
(279, 120, 'Admin logged in as user', '2020-07-21 13:24:19', '2020-07-21 13:24:19'),
(280, 15, 'Admin logged in as user', '2020-07-21 13:24:42', '2020-07-21 13:24:42'),
(281, 74, 'Admin logged in as user', '2020-07-21 13:24:54', '2020-07-21 13:24:54'),
(282, 41, 'Admin updated user information', '2020-07-21 13:27:45', '2020-07-21 13:27:45'),
(283, 41, 'Admin logged in as user', '2020-07-21 13:27:57', '2020-07-21 13:27:57'),
(284, 41, 'Admin logged in as user', '2020-07-21 13:42:11', '2020-07-21 13:42:11'),
(285, 25, 'Admin logged in as user', '2020-07-22 07:45:00', '2020-07-22 07:45:00'),
(286, 112, 'Admin logged in as user', '2020-07-22 07:47:06', '2020-07-22 07:47:06'),
(287, 112, 'Admin logged in as user', '2020-07-22 08:37:28', '2020-07-22 08:37:28'),
(288, 23, 'Admin logged in as user', '2020-07-22 09:19:20', '2020-07-22 09:19:20'),
(289, 112, 'Admin logged in as user', '2020-07-22 09:19:52', '2020-07-22 09:19:52'),
(290, 112, 'Admin logged in as user', '2020-07-22 09:23:35', '2020-07-22 09:23:35'),
(291, 128, 'Admin logged in as user', '2020-07-22 09:24:37', '2020-07-22 09:24:37'),
(292, 120, 'Admin logged in as user', '2020-07-22 12:12:08', '2020-07-22 12:12:08'),
(293, 41, 'Admin logged in as user', '2020-07-22 12:12:12', '2020-07-22 12:12:12'),
(294, 32, 'Admin logged in as user', '2020-07-23 13:29:12', '2020-07-23 13:29:12'),
(295, 112, 'Admin logged in as user', '2020-07-23 13:30:48', '2020-07-23 13:30:48'),
(296, 112, 'Admin logged in as user', '2020-07-23 13:34:17', '2020-07-23 13:34:17'),
(297, 112, 'Admin logged in as user', '2020-07-27 12:02:47', '2020-07-27 12:02:47'),
(298, 41, 'Admin logged in as user', '2020-07-27 12:07:15', '2020-07-27 12:07:15'),
(299, 25, 'Admin logged in as user', '2020-07-27 12:49:32', '2020-07-27 12:49:32'),
(300, 112, 'Admin logged in as user', '2020-07-27 12:57:54', '2020-07-27 12:57:54'),
(301, 41, 'Admin logged in as user', '2020-07-27 14:22:45', '2020-07-27 14:22:45'),
(302, 117, 'Admin logged in as user', '2020-07-27 14:23:51', '2020-07-27 14:23:51'),
(303, 117, 'Admin logged in as user', '2020-07-28 08:04:38', '2020-07-28 08:04:38'),
(304, 31, 'Admin logged in as user', '2020-07-28 08:10:15', '2020-07-28 08:10:15'),
(305, 74, 'Admin logged in as user', '2020-07-28 08:12:15', '2020-07-28 08:12:15'),
(306, 117, 'Admin logged in as user', '2020-07-28 08:20:19', '2020-07-28 08:20:19'),
(307, 120, 'Admin logged in as user', '2020-07-28 08:21:01', '2020-07-28 08:21:01'),
(308, 41, 'Admin logged in as user', '2020-07-28 08:21:11', '2020-07-28 08:21:11'),
(309, 120, 'Admin logged in as user', '2020-07-28 08:23:30', '2020-07-28 08:23:30'),
(310, 41, 'Admin logged in as user', '2020-07-28 08:23:36', '2020-07-28 08:23:36'),
(311, 107, 'Admin logged in as user', '2020-07-28 08:25:08', '2020-07-28 08:25:08'),
(312, 107, 'Admin logged in as user', '2020-07-28 08:31:24', '2020-07-28 08:31:24'),
(313, 74, 'Admin logged in as user', '2020-07-28 08:32:14', '2020-07-28 08:32:14'),
(314, 41, 'Admin logged in as user', '2020-07-28 13:13:04', '2020-07-28 13:13:04'),
(315, 135, 'User has logged in', '2020-07-29 08:36:17', '2020-07-29 08:36:17'),
(316, 41, 'Admin logged in as user', '2020-07-29 09:14:50', '2020-07-29 09:14:50'),
(317, 74, 'Admin logged in as user', '2020-07-29 09:53:46', '2020-07-29 09:53:46'),
(318, 11, 'Admin logged in as user', '2020-07-29 10:02:14', '2020-07-29 10:02:14'),
(319, 25, 'Admin logged in as user', '2020-07-29 10:02:30', '2020-07-29 10:02:30'),
(320, 41, 'Admin logged in as user', '2020-07-29 10:18:43', '2020-07-29 10:18:43'),
(321, 41, 'Admin logged in as user', '2020-07-30 07:01:16', '2020-07-30 07:01:16'),
(322, 41, 'Admin logged in as user', '2020-07-30 10:35:45', '2020-07-30 10:35:45'),
(323, 74, 'Admin logged in as user', '2020-07-30 10:59:32', '2020-07-30 10:59:32'),
(324, 24, 'Admin logged in as user', '2020-07-30 11:06:41', '2020-07-30 11:06:41'),
(325, 24, 'Admin logged in as user', '2020-07-31 13:00:16', '2020-07-31 13:00:16'),
(326, 41, 'Admin logged in as user', '2020-08-03 08:00:19', '2020-08-03 08:00:19'),
(327, 121, 'Admin logged in as user', '2020-08-03 08:00:52', '2020-08-03 08:00:52'),
(328, 41, 'Admin logged in as user', '2020-08-03 11:54:37', '2020-08-03 11:54:37'),
(329, 74, 'Admin logged in as user', '2020-08-03 12:04:47', '2020-08-03 12:04:47'),
(330, 25, 'Admin logged in as user', '2020-08-03 12:35:20', '2020-08-03 12:35:20'),
(331, 74, 'User has logged in', '2020-08-03 14:11:01', '2020-08-03 14:11:01'),
(332, 41, 'Admin logged in as user', '2020-08-03 14:20:06', '2020-08-03 14:20:06'),
(333, 74, 'Admin logged in as user', '2020-08-04 07:25:35', '2020-08-04 07:25:35'),
(334, 112, 'Admin logged in as user', '2020-08-07 13:09:48', '2020-08-07 13:09:48'),
(335, 112, 'Admin logged in as user', '2020-08-07 13:11:12', '2020-08-07 13:11:12'),
(336, 112, 'Admin logged in as user', '2020-08-07 13:14:59', '2020-08-07 13:14:59'),
(337, 41, 'Admin logged in as user', '2020-08-07 13:18:52', '2020-08-07 13:18:52'),
(338, 112, 'Admin logged in as user', '2020-08-07 13:41:13', '2020-08-07 13:41:13'),
(339, 112, 'Admin logged in as user', '2020-08-07 13:44:21', '2020-08-07 13:44:21'),
(340, 112, 'Admin logged in as user', '2020-08-07 14:02:21', '2020-08-07 14:02:21'),
(341, 112, 'Admin logged in as user', '2020-08-07 14:09:44', '2020-08-07 14:09:44'),
(342, 134, 'User has logged in', '2020-08-07 14:11:25', '2020-08-07 14:11:25'),
(343, 134, 'User has logged in', '2020-08-07 14:12:17', '2020-08-07 14:12:17'),
(344, 24, 'Admin updated user information', '2020-08-16 12:04:29', '2020-08-16 12:04:29'),
(345, 24, 'Admin logged in as user', '2020-08-16 12:05:03', '2020-08-16 12:05:03'),
(346, 23, 'Admin logged in as user', '2020-08-18 13:33:55', '2020-08-18 13:33:55'),
(347, 24, 'Admin logged in as user', '2020-08-20 10:04:57', '2020-08-20 10:04:57'),
(348, 134, 'User has logged in', '2020-08-26 12:00:02', '2020-08-26 12:00:02'),
(349, 64, 'User has logged in', '2020-08-26 18:47:27', '2020-08-26 18:47:27'),
(350, 24, 'Admin logged in as user', '2020-08-27 08:44:07', '2020-08-27 08:44:07'),
(351, 134, 'User has logged in', '2020-09-01 10:41:33', '2020-09-01 10:41:33'),
(352, 134, 'User has logged in', '2020-09-01 13:57:25', '2020-09-01 13:57:25'),
(353, 134, 'User has logged in', '2020-09-04 07:17:39', '2020-09-04 07:17:39'),
(354, 70, 'User has logged in', '2020-09-16 08:40:21', '2020-09-16 08:40:21'),
(355, 139, 'User has updated boosted benefits', '2020-09-16 09:38:08', '2020-09-16 09:38:08'),
(356, 134, 'User has logged in', '2020-09-18 10:06:28', '2020-09-18 10:06:28'),
(357, 134, 'User has logged in', '2020-09-18 10:09:15', '2020-09-18 10:09:15'),
(358, 134, 'User has logged in', '2020-09-23 13:31:39', '2020-09-23 13:31:39'),
(359, 134, 'User has logged in', '2020-09-23 13:39:46', '2020-09-23 13:39:46'),
(360, 125, 'User has logged in', '2020-09-25 12:15:45', '2020-09-25 12:15:45'),
(361, 134, 'User has logged in', '2020-09-28 09:31:43', '2020-09-28 09:31:43'),
(362, 15, 'Admin logged in as user', '2020-09-28 09:45:37', '2020-09-28 09:45:37'),
(363, 23, 'User has logged in', '2020-09-28 12:57:06', '2020-09-28 12:57:06'),
(364, 63, 'User has logged in', '2020-09-28 13:01:03', '2020-09-28 13:01:03'),
(365, 63, 'User has logged in', '2020-09-28 14:34:09', '2020-09-28 14:34:09'),
(366, 63, 'User has logged in', '2020-09-28 14:39:29', '2020-09-28 14:39:29'),
(367, 55, 'User has logged in', '2020-09-28 14:47:02', '2020-09-28 14:47:02'),
(368, 63, 'User has logged in', '2020-09-28 14:57:18', '2020-09-28 14:57:18'),
(369, 63, 'User has logged in', '2020-09-28 19:17:17', '2020-09-28 19:17:17'),
(370, 138, 'Admin updated user information', '2020-09-28 19:18:00', '2020-09-28 19:18:00'),
(371, 138, 'User has logged in', '2020-09-29 06:42:25', '2020-09-29 06:42:25'),
(372, 134, 'User has logged in', '2020-09-30 13:10:34', '2020-09-30 13:10:34'),
(373, 134, 'User has logged in', '2020-10-02 11:08:21', '2020-10-02 11:08:21'),
(374, 134, 'User has logged in', '2020-10-02 13:49:39', '2020-10-02 13:49:39'),
(375, 134, 'User has logged in', '2020-10-05 07:58:11', '2020-10-05 07:58:11'),
(376, 134, 'User has logged in', '2020-10-05 10:24:11', '2020-10-05 10:24:11'),
(377, 134, 'User has logged in', '2020-10-06 09:50:28', '2020-10-06 09:50:28'),
(378, 23, 'User has logged in', '2020-10-06 11:53:55', '2020-10-06 11:53:55'),
(379, 24, 'Admin logged in as user', '2020-10-06 11:54:09', '2020-10-06 11:54:09'),
(380, 134, 'User has logged in', '2020-10-06 12:27:02', '2020-10-06 12:27:02'),
(381, 15, 'Admin logged in as user', '2020-10-06 12:40:32', '2020-10-06 12:40:32'),
(382, 134, 'User has logged in', '2020-10-08 07:49:36', '2020-10-08 07:49:36'),
(383, 134, 'User has logged in', '2020-10-08 08:01:20', '2020-10-08 08:01:20'),
(384, 134, 'User has logged in', '2020-10-08 08:01:32', '2020-10-08 08:01:32'),
(385, 24, 'Admin logged in as user', '2020-10-08 08:01:50', '2020-10-08 08:01:50'),
(386, 134, 'User has logged in', '2020-10-08 08:10:35', '2020-10-08 08:10:35'),
(387, 134, 'User has logged in', '2020-10-08 08:41:53', '2020-10-08 08:41:53'),
(388, 134, 'User has logged in', '2020-10-08 08:45:52', '2020-10-08 08:45:52'),
(389, 134, 'User has logged in', '2020-10-08 09:12:22', '2020-10-08 09:12:22'),
(390, 134, 'User has logged in', '2020-10-08 09:15:17', '2020-10-08 09:15:17'),
(391, 134, 'User has logged in', '2020-10-08 09:23:38', '2020-10-08 09:23:38'),
(392, 141, 'User updated their information', '2020-10-08 09:44:07', '2020-10-08 09:44:07'),
(393, 141, 'User updated their information', '2020-10-08 09:44:13', '2020-10-08 09:44:13'),
(394, 134, 'User has logged in', '2020-10-08 09:52:32', '2020-10-08 09:52:32'),
(395, 141, 'User has logged in', '2020-10-08 09:52:43', '2020-10-08 09:52:43'),
(396, 142, 'User has logged in', '2020-10-08 10:10:30', '2020-10-08 10:10:30'),
(397, 134, 'User has logged in', '2020-10-08 11:12:20', '2020-10-08 11:12:20'),
(398, 142, 'Admin logged in as user', '2020-10-08 11:12:44', '2020-10-08 11:12:44'),
(399, 24, 'Admin logged in as user', '2020-10-08 11:14:59', '2020-10-08 11:14:59'),
(400, 134, 'User has logged in', '2020-10-08 11:17:23', '2020-10-08 11:17:23'),
(401, 131, 'User has logged in', '2020-10-08 11:17:46', '2020-10-08 11:17:46'),
(402, 141, 'User has logged in', '2020-10-08 11:17:52', '2020-10-08 11:17:52'),
(403, 146, 'User has logged in', '2020-10-08 12:48:07', '2020-10-08 12:48:07'),
(404, 145, 'User has logged in', '2020-10-08 13:02:01', '2020-10-08 13:02:01'),
(405, 134, 'User has logged in', '2020-10-08 13:03:35', '2020-10-08 13:03:35'),
(406, 134, 'User has logged in', '2020-10-09 08:57:08', '2020-10-09 08:57:08'),
(407, 63, 'User has logged in', '2020-10-09 12:27:11', '2020-10-09 12:27:11'),
(408, 55, 'User has logged in', '2020-10-09 12:27:43', '2020-10-09 12:27:43'),
(409, 63, 'User has logged in', '2020-10-12 15:48:57', '2020-10-12 15:48:57'),
(410, 23, 'User has logged in', '2020-10-13 10:58:16', '2020-10-13 10:58:16'),
(411, 24, 'Admin logged in as user', '2020-10-13 10:58:28', '2020-10-13 10:58:28'),
(412, 134, 'User has logged in', '2020-10-13 11:37:33', '2020-10-13 11:37:33'),
(413, 138, 'Admin logged in as user', '2020-10-13 11:38:36', '2020-10-13 11:38:36'),
(414, 70, 'Admin logged in as user', '2020-10-13 11:42:08', '2020-10-13 11:42:08'),
(415, 56, 'Admin logged in as user', '2020-10-13 12:58:09', '2020-10-13 12:58:09'),
(416, 145, 'User has logged in', '2020-10-13 14:19:03', '2020-10-13 14:19:03'),
(417, 134, 'User has logged in', '2020-10-13 14:28:08', '2020-10-13 14:28:08'),
(418, 134, 'User has logged in', '2020-10-14 09:46:17', '2020-10-14 09:46:17'),
(419, 134, 'User has logged in', '2020-10-14 10:09:08', '2020-10-14 10:09:08'),
(420, 55, 'User has logged in', '2020-10-14 12:14:07', '2020-10-14 12:14:07'),
(421, 145, 'User has logged in', '2020-10-14 13:21:40', '2020-10-14 13:21:40'),
(422, 63, 'User has logged in', '2020-10-14 13:49:21', '2020-10-14 13:49:21'),
(423, 148, 'User has logged in', '2020-10-14 13:54:48', '2020-10-14 13:54:48'),
(424, 148, 'User has logged in', '2020-10-15 08:40:28', '2020-10-15 08:40:28'),
(425, 134, 'User has logged in', '2020-10-15 08:41:24', '2020-10-15 08:41:24'),
(426, 134, 'User has logged in', '2020-10-15 09:27:03', '2020-10-15 09:27:03'),
(427, 148, 'User has logged in', '2020-10-15 09:27:11', '2020-10-15 09:27:11'),
(428, 134, 'User has logged in', '2020-10-15 10:03:46', '2020-10-15 10:03:46'),
(429, 148, 'User has logged in', '2020-10-15 10:05:01', '2020-10-15 10:05:01'),
(430, 34, 'User has logged in', '2020-10-15 10:05:41', '2020-10-15 10:05:41'),
(431, 148, 'User has logged in', '2020-10-15 10:13:49', '2020-10-15 10:13:49'),
(432, 148, 'User has logged in', '2020-10-15 10:21:03', '2020-10-15 10:21:03'),
(433, 148, 'User has logged in', '2020-10-15 10:24:03', '2020-10-15 10:24:03'),
(434, 148, 'User has logged in', '2020-10-15 11:05:00', '2020-10-15 11:05:00'),
(435, 148, 'User has logged in', '2020-10-15 11:12:24', '2020-10-15 11:12:24'),
(436, 134, 'User has logged in', '2020-10-15 12:30:59', '2020-10-15 12:30:59'),
(437, 34, 'User has logged in', '2020-10-16 08:05:25', '2020-10-16 08:05:25'),
(438, 134, 'User has logged in', '2020-10-16 08:14:29', '2020-10-16 08:14:29'),
(439, 34, 'User has logged in', '2020-10-16 08:23:23', '2020-10-16 08:23:23'),
(440, 148, 'User has logged in', '2020-10-16 08:30:15', '2020-10-16 08:30:15'),
(441, 147, 'User has logged in', '2020-10-16 08:30:29', '2020-10-16 08:30:29'),
(442, 148, 'User has logged in', '2020-10-16 10:04:18', '2020-10-16 10:04:18'),
(443, 148, 'User has logged in', '2020-10-16 11:26:13', '2020-10-16 11:26:13'),
(444, 148, 'User has logged in', '2020-10-16 11:26:23', '2020-10-16 11:26:23'),
(445, 134, 'User has logged in', '2020-10-16 11:27:08', '2020-10-16 11:27:08'),
(446, 34, 'User has logged in', '2020-10-16 11:27:37', '2020-10-16 11:27:37'),
(447, 148, 'User has logged in', '2020-10-16 12:40:05', '2020-10-16 12:40:05'),
(448, 134, 'User has logged in', '2020-10-16 12:43:38', '2020-10-16 12:43:38'),
(449, 148, 'User has logged in', '2020-10-16 12:43:43', '2020-10-16 12:43:43'),
(450, 148, 'User has logged in', '2020-10-16 12:44:58', '2020-10-16 12:44:58'),
(451, 148, 'User has logged in', '2020-10-16 12:50:43', '2020-10-16 12:50:43'),
(452, 148, 'User has logged in', '2020-10-16 12:52:17', '2020-10-16 12:52:17'),
(453, 148, 'User has logged in', '2020-10-16 12:55:19', '2020-10-16 12:55:19'),
(454, 34, 'User has logged in', '2020-10-16 14:16:00', '2020-10-16 14:16:00'),
(455, 34, 'User has logged in', '2020-10-16 14:16:17', '2020-10-16 14:16:17'),
(456, 34, 'User has logged in', '2020-10-16 14:17:52', '2020-10-16 14:17:52'),
(457, 34, 'User has logged in', '2020-10-16 14:22:00', '2020-10-16 14:22:00'),
(458, 34, 'User has logged in', '2020-10-16 14:26:25', '2020-10-16 14:26:25'),
(459, 34, 'User has logged in', '2020-10-16 14:31:29', '2020-10-16 14:31:29'),
(460, 34, 'User has logged in', '2020-10-16 14:36:24', '2020-10-16 14:36:24'),
(461, 148, 'User has logged in', '2020-10-16 16:38:11', '2020-10-16 16:38:11'),
(462, 148, 'User has logged in', '2020-10-17 09:40:19', '2020-10-17 09:40:19'),
(463, 148, 'User has logged in', '2020-10-18 12:30:20', '2020-10-18 12:30:20'),
(464, 148, 'User has logged in', '2020-10-18 12:44:37', '2020-10-18 12:44:37'),
(465, 148, 'User has logged in', '2020-10-19 10:40:19', '2020-10-19 10:40:19'),
(466, 148, 'User has logged in', '2020-10-19 10:44:01', '2020-10-19 10:44:01'),
(467, 148, 'User has logged in', '2020-10-20 11:32:12', '2020-10-20 11:32:12'),
(468, 148, 'User has logged in', '2020-10-21 08:28:15', '2020-10-21 08:28:15'),
(469, 151, 'User has logged in', '2020-10-21 08:28:32', '2020-10-21 08:28:32'),
(470, 34, 'User has logged in', '2020-10-21 08:40:14', '2020-10-21 08:40:14'),
(471, 70, 'User has logged in', '2020-10-21 13:22:51', '2020-10-21 13:22:51'),
(472, 148, 'User has logged in', '2020-10-22 09:13:01', '2020-10-22 09:13:01'),
(473, 32, 'User has logged in', '2020-10-23 14:48:33', '2020-10-23 14:48:33'),
(474, 55, 'User has logged in', '2020-10-23 14:48:47', '2020-10-23 14:48:47'),
(475, 63, 'User has logged in', '2020-10-23 14:49:13', '2020-10-23 14:49:13'),
(476, 55, 'User has logged in', '2020-10-23 14:50:10', '2020-10-23 14:50:10'),
(477, 134, 'User has logged in', '2020-10-25 17:49:23', '2020-10-25 17:49:23'),
(478, 134, 'User has logged in', '2020-10-25 17:53:31', '2020-10-25 17:53:31'),
(479, 134, 'User has logged in', '2020-10-26 09:25:49', '2020-10-26 09:25:49'),
(480, 23, 'User has logged in', '2020-10-27 10:26:54', '2020-10-27 10:26:54'),
(481, 55, 'User has logged in', '2020-10-27 11:01:16', '2020-10-27 11:01:16'),
(482, 32, 'User has logged in', '2020-10-27 15:25:06', '2020-10-27 15:25:06'),
(483, 134, 'User has logged in', '2020-10-29 09:14:09', '2020-10-29 09:14:09'),
(484, 134, 'User has logged in', '2020-10-30 12:03:57', '2020-10-30 12:03:57'),
(485, 147, 'User has logged in', '2020-10-30 12:04:02', '2020-10-30 12:04:02'),
(486, 112, 'User has logged in', '2020-10-30 14:12:59', '2020-10-30 14:12:59'),
(487, 112, 'User has logged in', '2020-10-30 14:14:36', '2020-10-30 14:14:36'),
(488, 112, 'User has logged in', '2020-10-30 14:34:38', '2020-10-30 14:34:38'),
(489, 34, 'User has logged in', '2020-10-30 14:40:25', '2020-10-30 14:40:25'),
(490, 147, 'User has logged in', '2020-10-30 16:00:16', '2020-10-30 16:00:16'),
(491, 23, 'User has logged in', '2020-10-30 16:32:27', '2020-10-30 16:32:27'),
(492, 34, 'User has logged in', '2020-10-30 16:32:36', '2020-10-30 16:32:36'),
(493, 134, 'User has logged in', '2020-10-30 16:32:38', '2020-10-30 16:32:38'),
(494, 154, 'User has logged in', '2020-10-30 16:34:21', '2020-10-30 16:34:21'),
(495, 42, 'User has logged in', '2020-10-30 16:34:52', '2020-10-30 16:34:52'),
(496, 134, 'User has logged in', '2020-10-30 16:35:56', '2020-10-30 16:35:56'),
(497, 154, 'User has logged in', '2020-10-30 16:37:01', '2020-10-30 16:37:01'),
(498, 134, 'User has logged in', '2020-10-30 16:38:13', '2020-10-30 16:38:13'),
(499, 154, 'User has logged in', '2020-10-30 16:42:16', '2020-10-30 16:42:16'),
(500, 154, 'User has logged in', '2020-10-30 16:46:49', '2020-10-30 16:46:49'),
(501, 153, 'User has logged in', '2020-10-30 16:53:31', '2020-10-30 16:53:31'),
(502, 154, 'User has logged in', '2020-10-30 17:05:07', '2020-10-30 17:05:07'),
(503, 154, 'User has logged in', '2020-10-30 18:57:19', '2020-10-30 18:57:19'),
(504, 154, 'User has logged in', '2020-10-31 12:37:49', '2020-10-31 12:37:49'),
(505, 134, 'User has logged in', '2020-10-31 12:38:43', '2020-10-31 12:38:43'),
(506, 64, 'Admin logged in as user', '2020-10-31 14:08:32', '2020-10-31 14:08:32'),
(507, 154, 'User has logged in', '2020-11-02 11:40:26', '2020-11-02 11:40:26'),
(508, 148, 'User has logged in', '2020-11-02 12:54:46', '2020-11-02 12:54:46'),
(509, 148, 'User has logged in', '2020-11-02 13:03:11', '2020-11-02 13:03:11'),
(510, 154, 'User has logged in', '2020-11-02 13:24:23', '2020-11-02 13:24:23'),
(511, 154, 'User has logged in', '2020-11-02 16:34:44', '2020-11-02 16:34:44'),
(512, 134, 'User has logged in', '2020-11-02 19:00:18', '2020-11-02 19:00:18'),
(513, 154, 'User has logged in', '2020-11-03 14:49:49', '2020-11-03 14:49:49'),
(514, 34, 'User has logged in', '2020-11-03 16:29:37', '2020-11-03 16:29:37'),
(515, 23, 'User has logged in', '2020-11-03 16:30:04', '2020-11-03 16:30:04'),
(516, 156, 'Admin logged in as user', '2020-11-03 16:32:49', '2020-11-03 16:32:49'),
(517, 24, 'Admin logged in as user', '2020-11-03 16:35:02', '2020-11-03 16:35:02'),
(518, 156, 'Admin logged in as user', '2020-11-03 16:37:08', '2020-11-03 16:37:08'),
(519, 23, 'User has logged in', '2020-11-03 16:38:09', '2020-11-03 16:38:09'),
(520, 156, 'Admin updated user information', '2020-11-03 16:38:36', '2020-11-03 16:38:36'),
(521, 156, 'User has logged in', '2020-11-03 16:38:49', '2020-11-03 16:38:49'),
(522, 156, 'Admin logged in as user', '2020-11-03 16:45:09', '2020-11-03 16:45:09'),
(523, 154, 'User has logged in', '2020-11-03 16:56:56', '2020-11-03 16:56:56'),
(524, 154, 'User has logged in', '2020-11-03 16:58:32', '2020-11-03 16:58:32'),
(525, 134, 'User has logged in', '2020-11-03 17:08:52', '2020-11-03 17:08:52'),
(526, 134, 'User has logged in', '2020-11-03 17:35:28', '2020-11-03 17:35:28'),
(527, 153, 'User has logged in', '2020-11-04 09:13:08', '2020-11-04 09:13:08'),
(528, 153, 'User has logged in', '2020-11-04 17:37:24', '2020-11-04 17:37:24'),
(529, 62, 'User has logged in', '2020-11-05 10:29:15', '2020-11-05 10:29:15'),
(530, 153, 'User has logged in', '2020-11-05 10:33:37', '2020-11-05 10:33:37'),
(531, 156, 'User has logged in', '2020-11-05 10:47:15', '2020-11-05 10:47:15'),
(532, 157, 'User has logged in', '2020-11-05 11:20:41', '2020-11-05 11:20:41'),
(533, 158, 'User has logged in', '2020-11-05 11:34:23', '2020-11-05 11:34:23'),
(534, 155, 'User has logged in', '2020-11-05 11:52:19', '2020-11-05 11:52:19'),
(535, 155, 'User has logged in', '2020-11-05 11:57:11', '2020-11-05 11:57:11'),
(536, 148, 'User has logged in', '2020-11-05 11:57:42', '2020-11-05 11:57:42'),
(537, 147, 'User has logged in', '2020-11-05 11:58:12', '2020-11-05 11:58:12'),
(538, 150, 'User has logged in', '2020-11-05 11:58:47', '2020-11-05 11:58:47'),
(539, 159, 'User has logged in', '2020-11-05 12:57:44', '2020-11-05 12:57:44'),
(540, 148, 'User has logged in', '2020-11-05 12:58:21', '2020-11-05 12:58:21'),
(541, 134, 'User has logged in', '2020-11-05 13:00:28', '2020-11-05 13:00:28'),
(542, 148, 'User has logged in', '2020-11-05 13:02:01', '2020-11-05 13:02:01'),
(543, 134, 'User has logged in', '2020-11-05 13:06:35', '2020-11-05 13:06:35'),
(544, 160, 'User has logged in', '2020-11-05 13:12:06', '2020-11-05 13:12:06'),
(545, 134, 'User has logged in', '2020-11-05 14:00:54', '2020-11-05 14:00:54'),
(546, 161, 'User has logged in', '2020-11-05 14:03:00', '2020-11-05 14:03:00'),
(547, 160, 'User has logged in', '2020-11-05 14:40:49', '2020-11-05 14:40:49'),
(548, 162, 'User has logged in', '2020-11-05 14:45:33', '2020-11-05 14:45:33'),
(549, 23, 'User has logged in', '2020-11-05 14:55:30', '2020-11-05 14:55:30'),
(550, 163, 'Admin logged in as user', '2020-11-05 15:03:23', '2020-11-05 15:03:23'),
(551, 162, 'User has logged in', '2020-11-05 15:05:29', '2020-11-05 15:05:29'),
(552, 148, 'User has logged in', '2020-11-05 15:07:00', '2020-11-05 15:07:00'),
(553, 34, 'User has logged in', '2020-11-05 15:14:33', '2020-11-05 15:14:33'),
(554, 164, 'User has logged in', '2020-11-05 15:17:49', '2020-11-05 15:17:49'),
(555, 163, 'Admin logged in as user', '2020-11-05 15:20:19', '2020-11-05 15:20:19'),
(556, 163, 'Admin logged in as user', '2020-11-05 15:20:30', '2020-11-05 15:20:30'),
(557, 23, 'User has logged in', '2020-11-05 15:21:44', '2020-11-05 15:21:44'),
(558, 163, 'Admin logged in as user', '2020-11-05 15:23:11', '2020-11-05 15:23:11'),
(559, 26, 'Admin logged in as user', '2020-11-05 15:25:35', '2020-11-05 15:25:35'),
(560, 156, 'Admin logged in as user', '2020-11-05 15:25:50', '2020-11-05 15:25:50'),
(561, 34, 'User has logged in', '2020-11-05 16:02:54', '2020-11-05 16:02:54'),
(562, 134, 'User has logged in', '2020-11-05 16:05:45', '2020-11-05 16:05:45'),
(563, 63, 'User has logged in', '2020-11-05 16:17:59', '2020-11-05 16:17:59'),
(564, 165, 'User has logged in', '2020-11-05 16:26:16', '2020-11-05 16:26:16'),
(565, 164, 'User has logged in', '2020-11-05 16:26:39', '2020-11-05 16:26:39'),
(566, 165, 'User has logged in', '2020-11-05 16:27:03', '2020-11-05 16:27:03'),
(567, 23, 'User has logged in', '2020-11-05 16:29:34', '2020-11-05 16:29:34'),
(568, 163, 'Admin logged in as user', '2020-11-05 16:29:49', '2020-11-05 16:29:49'),
(569, 164, 'User has logged in', '2020-11-05 16:31:14', '2020-11-05 16:31:14'),
(570, 163, 'Admin logged in as user', '2020-11-05 16:31:15', '2020-11-05 16:31:15'),
(571, 156, 'Admin logged in as user', '2020-11-05 16:32:52', '2020-11-05 16:32:52'),
(572, 166, 'Admin updated user information', '2020-11-05 16:53:34', '2020-11-05 16:53:34'),
(573, 166, 'User has logged in', '2020-11-06 10:19:37', '2020-11-06 10:19:37'),
(574, 63, 'User has logged in', '2020-11-06 10:20:12', '2020-11-06 10:20:12'),
(575, 164, 'User has logged in', '2020-11-06 11:40:04', '2020-11-06 11:40:04'),
(576, 134, 'User has logged in', '2020-11-06 13:57:53', '2020-11-06 13:57:53'),
(577, 164, 'User has logged in', '2020-11-06 14:43:50', '2020-11-06 14:43:50'),
(578, 156, 'User has logged in', '2020-11-06 14:46:43', '2020-11-06 14:46:43'),
(579, 156, 'User has logged in', '2020-11-06 15:03:25', '2020-11-06 15:03:25'),
(580, 23, 'User has logged in', '2020-11-06 15:12:27', '2020-11-06 15:12:27'),
(581, 163, 'Admin logged in as user', '2020-11-06 15:12:39', '2020-11-06 15:12:39'),
(582, 166, 'User has logged in', '2020-11-06 15:31:21', '2020-11-06 15:31:21'),
(583, 164, 'User has logged in', '2020-11-06 15:34:07', '2020-11-06 15:34:07'),
(584, 23, 'User has logged in', '2020-11-06 15:57:15', '2020-11-06 15:57:15'),
(585, 167, 'Admin updated user information', '2020-11-06 15:57:40', '2020-11-06 15:57:40'),
(586, 167, 'User has logged in', '2020-11-06 15:57:53', '2020-11-06 15:57:53'),
(587, 164, 'User has logged in', '2020-11-06 16:09:10', '2020-11-06 16:09:10'),
(588, 164, 'User has logged in', '2020-11-06 16:41:39', '2020-11-06 16:41:39'),
(589, 164, 'User has logged in', '2020-11-06 16:48:48', '2020-11-06 16:48:48'),
(590, 164, 'User has logged in', '2020-11-06 16:56:00', '2020-11-06 16:56:00'),
(591, 164, 'User has logged in', '2020-11-06 17:18:09', '2020-11-06 17:18:09'),
(592, 164, 'User has logged in', '2020-11-06 17:22:41', '2020-11-06 17:22:41'),
(593, 164, 'User has logged in', '2020-11-06 17:25:08', '2020-11-06 17:25:08'),
(594, 168, 'User has logged in', '2020-11-06 18:22:12', '2020-11-06 18:22:12'),
(595, 168, 'User has logged in', '2020-11-06 18:27:49', '2020-11-06 18:27:49'),
(596, 168, 'User has logged in', '2020-11-07 11:35:10', '2020-11-07 11:35:10'),
(597, 168, 'User has logged in', '2020-11-07 11:38:30', '2020-11-07 11:38:30'),
(598, 168, 'User has logged in', '2020-11-07 12:08:39', '2020-11-07 12:08:39'),
(599, 153, 'User has logged in', '2020-11-07 12:47:20', '2020-11-07 12:47:20'),
(600, 168, 'User has logged in', '2020-11-08 10:55:17', '2020-11-08 10:55:17'),
(601, 168, 'User has logged in', '2020-11-08 11:53:24', '2020-11-08 11:53:24'),
(602, 134, 'User has logged in', '2020-11-08 12:01:05', '2020-11-08 12:01:05'),
(603, 168, 'User has logged in', '2020-11-08 12:01:39', '2020-11-08 12:01:39'),
(604, 165, 'User has logged in', '2020-11-08 12:02:50', '2020-11-08 12:02:50'),
(605, 134, 'User has logged in', '2020-11-08 12:02:56', '2020-11-08 12:02:56'),
(606, 168, 'User has logged in', '2020-11-08 12:03:16', '2020-11-08 12:03:16'),
(607, 168, 'User has logged in', '2020-11-08 12:06:01', '2020-11-08 12:06:01'),
(608, 134, 'User has logged in', '2020-11-08 12:06:44', '2020-11-08 12:06:44'),
(609, 168, 'User has logged in', '2020-11-08 12:07:46', '2020-11-08 12:07:46'),
(610, 134, 'User has logged in', '2020-11-08 12:14:17', '2020-11-08 12:14:17'),
(611, 168, 'User has logged in', '2020-11-08 12:17:06', '2020-11-08 12:17:06'),
(612, 168, 'User has logged in', '2020-11-08 14:03:10', '2020-11-08 14:03:10'),
(613, 168, 'User has logged in', '2020-11-08 16:03:58', '2020-11-08 16:03:58'),
(614, 168, 'User has logged in', '2020-11-09 09:26:23', '2020-11-09 09:26:23'),
(615, 168, 'User has logged in', '2020-11-09 09:28:15', '2020-11-09 09:28:15'),
(616, 165, 'User has logged in', '2020-11-09 10:40:42', '2020-11-09 10:40:42'),
(617, 134, 'User has logged in', '2020-11-09 10:40:47', '2020-11-09 10:40:47'),
(618, 168, 'User has logged in', '2020-11-09 10:43:08', '2020-11-09 10:43:08'),
(619, 168, 'User has logged in', '2020-11-09 10:53:48', '2020-11-09 10:53:48'),
(620, 167, 'User has logged in', '2020-11-09 10:55:51', '2020-11-09 10:55:51'),
(621, 167, 'User has logged in', '2020-11-09 10:58:27', '2020-11-09 10:58:27'),
(622, 167, 'User has logged in', '2020-11-09 10:59:40', '2020-11-09 10:59:40'),
(623, 168, 'User has logged in', '2020-11-09 11:00:59', '2020-11-09 11:00:59'),
(624, 134, 'User has logged in', '2020-11-09 11:06:34', '2020-11-09 11:06:34'),
(625, 168, 'User has logged in', '2020-11-09 11:07:17', '2020-11-09 11:07:17'),
(626, 168, 'User has logged in', '2020-11-09 11:34:02', '2020-11-09 11:34:02'),
(627, 168, 'User has logged in', '2020-11-09 11:39:27', '2020-11-09 11:39:27'),
(628, 168, 'User has logged in', '2020-11-09 11:42:58', '2020-11-09 11:42:58'),
(629, 168, 'User has logged in', '2020-11-09 11:45:44', '2020-11-09 11:45:44');
INSERT INTO `changelogs` (`id`, `user_id`, `action`, `created_at`, `updated_at`) VALUES
(630, 134, 'User has logged in', '2020-11-09 11:59:57', '2020-11-09 11:59:57'),
(631, 169, 'User has logged in', '2020-11-09 12:00:14', '2020-11-09 12:00:14'),
(632, 169, 'User has logged in', '2020-11-09 12:10:19', '2020-11-09 12:10:19'),
(633, 169, 'User has logged in', '2020-11-09 13:11:22', '2020-11-09 13:11:22'),
(634, 169, 'User has logged in', '2020-11-09 13:12:09', '2020-11-09 13:12:09'),
(635, 169, 'User has logged in', '2020-11-09 13:18:46', '2020-11-09 13:18:46'),
(636, 134, 'User has logged in', '2020-11-09 15:31:29', '2020-11-09 15:31:29'),
(637, 169, 'User has logged in', '2020-11-09 15:54:26', '2020-11-09 15:54:26'),
(638, 134, 'User has logged in', '2020-11-09 15:56:50', '2020-11-09 15:56:50'),
(639, 169, 'User has logged in', '2020-11-09 16:07:25', '2020-11-09 16:07:25'),
(640, 169, 'User has logged in', '2020-11-09 16:23:56', '2020-11-09 16:23:56'),
(641, 169, 'User has logged in', '2020-11-09 16:25:31', '2020-11-09 16:25:31'),
(642, 169, 'User has logged in', '2020-11-09 16:49:07', '2020-11-09 16:49:07'),
(643, 169, 'User has logged in', '2020-11-09 18:49:04', '2020-11-09 18:49:04'),
(644, 169, 'User has logged in', '2020-11-09 18:53:37', '2020-11-09 18:53:37'),
(645, 170, 'User has logged in', '2020-11-09 19:12:45', '2020-11-09 19:12:45'),
(646, 170, 'User has logged in', '2020-11-09 20:56:31', '2020-11-09 20:56:31'),
(647, 170, 'User has logged in', '2020-11-10 08:59:23', '2020-11-10 08:59:23'),
(648, 63, 'User has logged in', '2020-11-10 09:47:15', '2020-11-10 09:47:15'),
(649, 166, 'User has logged in', '2020-11-10 09:51:41', '2020-11-10 09:51:41'),
(650, 167, 'User has logged in', '2020-11-10 10:10:54', '2020-11-10 10:10:54'),
(651, 170, 'User has logged in', '2020-11-10 10:28:56', '2020-11-10 10:28:56'),
(652, 23, 'User has logged in', '2020-11-10 11:44:54', '2020-11-10 11:44:54'),
(653, 171, 'Admin logged in as user', '2020-11-10 11:55:59', '2020-11-10 11:55:59'),
(654, 170, 'User has logged in', '2020-11-10 12:08:13', '2020-11-10 12:08:13'),
(655, 170, 'User has logged in', '2020-11-10 12:15:47', '2020-11-10 12:15:47'),
(656, 134, 'User has logged in', '2020-11-10 13:14:34', '2020-11-10 13:14:34'),
(657, 134, 'User has logged in', '2020-11-10 13:26:35', '2020-11-10 13:26:35'),
(658, 134, 'User has logged in', '2020-11-10 13:50:51', '2020-11-10 13:50:51'),
(659, 134, 'User has logged in', '2020-11-10 14:20:41', '2020-11-10 14:20:41'),
(660, 170, 'User has logged in', '2020-11-10 14:25:53', '2020-11-10 14:25:53'),
(661, 170, 'User has logged in', '2020-11-10 14:31:53', '2020-11-10 14:31:53'),
(662, 170, 'User has logged in', '2020-11-10 15:01:54', '2020-11-10 15:01:54'),
(663, 167, 'User has logged in', '2020-11-10 15:04:42', '2020-11-10 15:04:42'),
(664, 170, 'User has logged in', '2020-11-10 15:10:21', '2020-11-10 15:10:21'),
(665, 170, 'User has logged in', '2020-11-10 15:13:30', '2020-11-10 15:13:30'),
(666, 170, 'User has logged in', '2020-11-10 15:16:51', '2020-11-10 15:16:51'),
(667, 170, 'User has logged in', '2020-11-10 15:20:00', '2020-11-10 15:20:00'),
(668, 170, 'User has logged in', '2020-11-10 15:25:51', '2020-11-10 15:25:51'),
(669, 170, 'User has logged in', '2020-11-10 16:49:36', '2020-11-10 16:49:36'),
(670, 170, 'User has logged in', '2020-11-10 16:51:36', '2020-11-10 16:51:36'),
(671, 134, 'User has logged in', '2020-11-10 16:52:00', '2020-11-10 16:52:00'),
(672, 170, 'User has logged in', '2020-11-10 17:05:48', '2020-11-10 17:05:48'),
(673, 170, 'User has logged in', '2020-11-10 18:55:14', '2020-11-10 18:55:14'),
(674, 170, 'User has logged in', '2020-11-11 11:03:12', '2020-11-11 11:03:12'),
(675, 134, 'User has logged in', '2020-11-11 11:04:34', '2020-11-11 11:04:34'),
(676, 170, 'User has logged in', '2020-11-11 11:04:49', '2020-11-11 11:04:49'),
(677, 134, 'User has logged in', '2020-11-11 11:05:01', '2020-11-11 11:05:01'),
(678, 170, 'User has logged in', '2020-11-11 11:05:42', '2020-11-11 11:05:42'),
(679, 134, 'User has logged in', '2020-11-11 11:06:00', '2020-11-11 11:06:00'),
(680, 170, 'User has logged in', '2020-11-11 11:06:13', '2020-11-11 11:06:13'),
(681, 170, 'User has logged in', '2020-11-11 11:25:56', '2020-11-11 11:25:56'),
(682, 170, 'User has logged in', '2020-11-11 11:54:41', '2020-11-11 11:54:41'),
(683, 170, 'User has logged in', '2020-11-11 11:56:29', '2020-11-11 11:56:29'),
(684, 170, 'User has logged in', '2020-11-11 11:58:31', '2020-11-11 11:58:31'),
(685, 170, 'User has logged in', '2020-11-11 12:01:01', '2020-11-11 12:01:01'),
(686, 170, 'User has logged in', '2020-11-11 12:04:53', '2020-11-11 12:04:53'),
(687, 170, 'User has logged in', '2020-11-11 12:12:54', '2020-11-11 12:12:54'),
(688, 170, 'User has logged in', '2020-11-11 12:20:05', '2020-11-11 12:20:05'),
(689, 170, 'User has logged in', '2020-11-11 12:22:15', '2020-11-11 12:22:15'),
(690, 134, 'User has logged in', '2020-11-11 12:22:33', '2020-11-11 12:22:33'),
(691, 170, 'User has logged in', '2020-11-11 12:22:46', '2020-11-11 12:22:46'),
(692, 134, 'User has logged in', '2020-11-11 12:25:25', '2020-11-11 12:25:25'),
(693, 170, 'User has logged in', '2020-11-11 12:30:31', '2020-11-11 12:30:31'),
(694, 173, 'User has logged in', '2020-11-11 12:54:31', '2020-11-11 12:54:31'),
(695, 134, 'User has logged in', '2020-11-11 13:04:32', '2020-11-11 13:04:32'),
(696, 173, 'User has logged in', '2020-11-11 13:04:51', '2020-11-11 13:04:51'),
(697, 173, 'User has logged in', '2020-11-11 16:23:45', '2020-11-11 16:23:45'),
(698, 173, 'User has logged in', '2020-11-11 16:25:58', '2020-11-11 16:25:58'),
(699, 173, 'User has logged in', '2020-11-11 16:35:11', '2020-11-11 16:35:11'),
(700, 134, 'User has logged in', '2020-11-11 16:36:00', '2020-11-11 16:36:00'),
(701, 173, 'User has logged in', '2020-11-11 16:36:34', '2020-11-11 16:36:34'),
(702, 134, 'User has logged in', '2020-11-11 16:42:56', '2020-11-11 16:42:56'),
(703, 174, 'User has logged in', '2020-11-11 16:43:18', '2020-11-11 16:43:18'),
(704, 63, 'User has logged in', '2020-11-11 16:46:14', '2020-11-11 16:46:14'),
(705, 174, 'User has logged in', '2020-11-11 16:50:28', '2020-11-11 16:50:28'),
(706, 174, 'User has logged in', '2020-11-11 16:51:58', '2020-11-11 16:51:58'),
(707, 134, 'User has logged in', '2020-11-11 16:52:27', '2020-11-11 16:52:27'),
(708, 174, 'User has logged in', '2020-11-11 16:52:45', '2020-11-11 16:52:45'),
(709, 175, 'Admin updated user information', '2020-11-11 17:23:17', '2020-11-11 17:23:17'),
(710, 174, 'User has logged in', '2020-11-11 18:49:25', '2020-11-11 18:49:25'),
(711, 174, 'User has logged in', '2020-11-11 18:51:59', '2020-11-11 18:51:59'),
(712, 174, 'User has logged in', '2020-11-11 18:52:09', '2020-11-11 18:52:09'),
(713, 174, 'User has logged in', '2020-11-11 18:54:27', '2020-11-11 18:54:27'),
(714, 176, 'User has logged in', '2020-11-11 19:06:13', '2020-11-11 19:06:13'),
(715, 176, 'User has logged in', '2020-11-11 19:08:05', '2020-11-11 19:08:05'),
(716, 176, 'User has logged in', '2020-11-11 19:11:49', '2020-11-11 19:11:49'),
(717, 176, 'User has logged in', '2020-11-12 08:58:30', '2020-11-12 08:58:30'),
(718, 176, 'User has logged in', '2020-11-12 09:10:31', '2020-11-12 09:10:31'),
(719, 176, 'User has logged in', '2020-11-12 09:12:31', '2020-11-12 09:12:31'),
(720, 176, 'User has logged in', '2020-11-12 09:14:28', '2020-11-12 09:14:28'),
(721, 176, 'User has logged in', '2020-11-12 09:16:04', '2020-11-12 09:16:04'),
(722, 176, 'User has logged in', '2020-11-12 09:18:49', '2020-11-12 09:18:49'),
(723, 176, 'User has logged in', '2020-11-12 09:20:48', '2020-11-12 09:20:48'),
(724, 176, 'User has logged in', '2020-11-12 10:11:34', '2020-11-12 10:11:34'),
(725, 176, 'User has logged in', '2020-11-12 10:13:46', '2020-11-12 10:13:46'),
(726, 176, 'User has logged in', '2020-11-12 11:12:57', '2020-11-12 11:12:57'),
(727, 134, 'User has logged in', '2020-11-12 11:49:58', '2020-11-12 11:49:58'),
(728, 134, 'User has logged in', '2020-11-12 11:56:21', '2020-11-12 11:56:21'),
(729, 134, 'User has logged in', '2020-11-12 11:56:26', '2020-11-12 11:56:26'),
(730, 134, 'User has logged in', '2020-11-12 12:23:43', '2020-11-12 12:23:43'),
(731, 134, 'User has logged in', '2020-11-12 14:29:59', '2020-11-12 14:29:59'),
(732, 175, 'User has logged in', '2020-11-12 15:05:14', '2020-11-12 15:05:14'),
(733, 134, 'User has logged in', '2020-11-12 15:50:38', '2020-11-12 15:50:38'),
(734, 176, 'User has logged in', '2020-11-12 16:11:30', '2020-11-12 16:11:30'),
(735, 134, 'User has logged in', '2020-11-13 09:16:24', '2020-11-13 09:16:24'),
(736, 63, 'User has logged in', '2020-11-13 10:11:26', '2020-11-13 10:11:26'),
(737, 166, 'Admin updated user information', '2020-11-13 10:12:02', '2020-11-13 10:12:02'),
(738, 166, 'User has logged in', '2020-11-13 10:12:45', '2020-11-13 10:12:45'),
(739, 153, 'User has logged in', '2020-11-13 10:12:59', '2020-11-13 10:12:59'),
(740, 166, 'User has logged in', '2020-11-13 10:13:47', '2020-11-13 10:13:47'),
(741, 63, 'User has logged in', '2020-11-13 10:14:23', '2020-11-13 10:14:23'),
(742, 166, 'User has logged in', '2020-11-13 10:18:05', '2020-11-13 10:18:05'),
(743, 176, 'User has logged in', '2020-11-13 10:18:51', '2020-11-13 10:18:51'),
(744, 166, 'User has logged in', '2020-11-13 10:20:03', '2020-11-13 10:20:03'),
(745, 166, 'User has logged in', '2020-11-13 10:20:18', '2020-11-13 10:20:18'),
(746, 166, 'User has logged in', '2020-11-13 10:24:25', '2020-11-13 10:24:25'),
(747, 63, 'User has logged in', '2020-11-13 10:26:47', '2020-11-13 10:26:47'),
(748, 166, 'Admin updated user information', '2020-11-13 10:27:05', '2020-11-13 10:27:05'),
(749, 176, 'User has logged in', '2020-11-13 10:27:30', '2020-11-13 10:27:30'),
(750, 176, 'User has logged in', '2020-11-13 10:36:38', '2020-11-13 10:36:38'),
(751, 134, 'User has logged in', '2020-11-13 10:38:15', '2020-11-13 10:38:15'),
(752, 166, 'Admin logged in as user', '2020-11-13 10:38:57', '2020-11-13 10:38:57'),
(753, 66, 'Admin updated user information', '2020-11-13 10:40:11', '2020-11-13 10:40:11'),
(754, 178, 'Admin updated user information', '2020-11-13 10:41:19', '2020-11-13 10:41:19'),
(755, 176, 'User has logged in', '2020-11-13 10:45:44', '2020-11-13 10:45:44'),
(756, 55, 'Admin updated user information', '2020-11-13 10:47:48', '2020-11-13 10:47:48'),
(757, 134, 'User has logged in', '2020-11-13 10:54:06', '2020-11-13 10:54:06'),
(758, 63, 'User has logged in', '2020-11-13 10:55:00', '2020-11-13 10:55:00'),
(759, 134, 'User has logged in', '2020-11-13 10:56:42', '2020-11-13 10:56:42'),
(760, 134, 'User has logged in', '2020-11-13 10:57:55', '2020-11-13 10:57:55'),
(761, 134, 'User has logged in', '2020-11-13 11:08:21', '2020-11-13 11:08:21'),
(762, 32, 'Admin logged in as user', '2020-11-13 11:13:44', '2020-11-13 11:13:44'),
(763, 176, 'User has logged in', '2020-11-13 11:25:27', '2020-11-13 11:25:27'),
(764, 134, 'User has logged in', '2020-11-13 11:38:14', '2020-11-13 11:38:14'),
(765, 134, 'User has logged in', '2020-11-13 11:44:53', '2020-11-13 11:44:53'),
(766, 134, 'User has logged in', '2020-11-13 11:46:40', '2020-11-13 11:46:40'),
(767, 134, 'User has logged in', '2020-11-13 12:21:06', '2020-11-13 12:21:06'),
(768, 134, 'User has logged in', '2020-11-13 12:21:51', '2020-11-13 12:21:51'),
(769, 134, 'User has logged in', '2020-11-13 12:21:56', '2020-11-13 12:21:56'),
(770, 134, 'User has logged in', '2020-11-13 12:25:01', '2020-11-13 12:25:01'),
(771, 134, 'User has logged in', '2020-11-13 12:26:29', '2020-11-13 12:26:29'),
(772, 134, 'User has logged in', '2020-11-13 12:28:45', '2020-11-13 12:28:45'),
(773, 134, 'User has logged in', '2020-11-13 12:37:01', '2020-11-13 12:37:01'),
(774, 134, 'User has logged in', '2020-11-13 12:40:49', '2020-11-13 12:40:49'),
(775, 134, 'User has logged in', '2020-11-13 13:02:38', '2020-11-13 13:02:38'),
(776, 63, 'User has logged in', '2020-11-13 13:42:21', '2020-11-13 13:42:21'),
(777, 134, 'User has logged in', '2020-11-13 13:53:13', '2020-11-13 13:53:13'),
(778, 176, 'User has logged in', '2020-11-13 13:53:25', '2020-11-13 13:53:25'),
(779, 134, 'User has logged in', '2020-11-13 15:38:47', '2020-11-13 15:38:47'),
(780, 134, 'User has logged in', '2020-11-13 15:53:08', '2020-11-13 15:53:08'),
(781, 166, 'User has logged in', '2020-11-13 15:57:43', '2020-11-13 15:57:43'),
(782, 63, 'User has logged in', '2020-11-13 16:26:21', '2020-11-13 16:26:21'),
(783, 178, 'Admin updated user information', '2020-11-13 16:26:56', '2020-11-13 16:26:56'),
(784, 178, 'User has logged in', '2020-11-13 16:27:12', '2020-11-13 16:27:12'),
(785, 176, 'User has logged in', '2020-11-16 09:27:42', '2020-11-16 09:27:42'),
(786, 176, 'User has logged in', '2020-11-16 09:28:53', '2020-11-16 09:28:53'),
(787, 176, 'User has logged in', '2020-11-16 11:32:02', '2020-11-16 11:32:02'),
(788, 23, 'User has logged in', '2020-11-16 12:44:07', '2020-11-16 12:44:07'),
(789, 167, 'User has logged in', '2020-11-16 12:44:53', '2020-11-16 12:44:53'),
(790, 23, 'User has logged in', '2020-11-16 13:43:43', '2020-11-16 13:43:43'),
(791, 171, 'Admin updated user information', '2020-11-16 13:44:24', '2020-11-16 13:44:24'),
(792, 176, 'User has logged in', '2020-11-16 14:31:11', '2020-11-16 14:31:11'),
(793, 176, 'User has logged in', '2020-11-16 14:44:56', '2020-11-16 14:44:56'),
(794, 176, 'User has logged in', '2020-11-16 15:20:23', '2020-11-16 15:20:23'),
(795, 176, 'User has logged in', '2020-11-16 15:25:34', '2020-11-16 15:25:34'),
(796, 176, 'User has logged in', '2020-11-16 16:06:52', '2020-11-16 16:06:52'),
(797, 176, 'User has logged in', '2020-11-17 10:47:00', '2020-11-17 10:47:00'),
(798, 176, 'User has logged in', '2020-11-17 10:48:50', '2020-11-17 10:48:50'),
(799, 176, 'User has logged in', '2020-11-17 11:39:59', '2020-11-17 11:39:59'),
(800, 176, 'User has logged in', '2020-11-17 15:27:21', '2020-11-17 15:27:21'),
(801, 176, 'User has logged in', '2020-11-17 16:00:51', '2020-11-17 16:00:51'),
(802, 176, 'User has logged in', '2020-11-17 16:04:29', '2020-11-17 16:04:29'),
(803, 176, 'User has logged in', '2020-11-17 16:07:45', '2020-11-17 16:07:45'),
(804, 176, 'User has logged in', '2020-11-17 16:08:11', '2020-11-17 16:08:11'),
(805, 176, 'User has logged in', '2020-11-17 16:22:23', '2020-11-17 16:22:23'),
(806, 176, 'User has logged in', '2020-11-17 16:24:48', '2020-11-17 16:24:48'),
(807, 176, 'User has logged in', '2020-11-17 19:47:16', '2020-11-17 19:47:16'),
(808, 176, 'User has logged in', '2020-11-17 20:15:36', '2020-11-17 20:15:36'),
(809, 176, 'User has logged in', '2020-11-18 12:41:09', '2020-11-18 12:41:09'),
(810, 176, 'User has logged in', '2020-11-18 16:03:04', '2020-11-18 16:03:04'),
(811, 176, 'User has logged in', '2020-11-18 21:51:59', '2020-11-18 21:51:59'),
(812, 176, 'User has logged in', '2020-11-19 08:49:25', '2020-11-19 08:49:25'),
(813, 176, 'User has logged in', '2020-11-20 11:15:21', '2020-11-20 11:15:21'),
(814, 176, 'User has logged in', '2020-11-20 11:19:04', '2020-11-20 11:19:04'),
(815, 178, 'User has logged in', '2020-11-20 11:27:09', '2020-11-20 11:27:09'),
(816, 178, 'User has logged in', '2020-11-20 11:38:04', '2020-11-20 11:38:04'),
(817, 63, 'User has logged in', '2020-11-20 11:38:29', '2020-11-20 11:38:29'),
(818, 166, 'User has logged in', '2020-11-20 11:45:08', '2020-11-20 11:45:08'),
(819, 166, 'User has logged in', '2020-11-20 11:51:17', '2020-11-20 11:51:17'),
(820, 176, 'User has logged in', '2020-11-20 11:55:37', '2020-11-20 11:55:37'),
(821, 63, 'User has logged in', '2020-11-20 12:50:45', '2020-11-20 12:50:45'),
(822, 176, 'User has logged in', '2020-11-20 14:31:39', '2020-11-20 14:31:39'),
(823, 176, 'User has logged in', '2020-11-21 13:43:21', '2020-11-21 13:43:21'),
(824, 176, 'User has logged in', '2020-11-22 14:02:49', '2020-11-22 14:02:49'),
(825, 176, 'User has logged in', '2020-11-23 08:51:42', '2020-11-23 08:51:42'),
(826, 134, 'User has logged in', '2020-11-23 09:40:02', '2020-11-23 09:40:02'),
(827, 134, 'User has logged in', '2020-11-23 10:08:19', '2020-11-23 10:08:19'),
(828, 134, 'User has logged in', '2020-11-23 10:53:26', '2020-11-23 10:53:26'),
(829, 134, 'User has logged in', '2020-11-23 12:25:38', '2020-11-23 12:25:38'),
(830, 134, 'User has logged in', '2020-11-23 12:56:05', '2020-11-23 12:56:05'),
(831, 134, 'User has logged in', '2020-11-23 13:10:51', '2020-11-23 13:10:51'),
(832, 63, 'User has logged in', '2020-11-23 13:27:35', '2020-11-23 13:27:35'),
(833, 63, 'User has logged in', '2020-11-23 13:52:56', '2020-11-23 13:52:56'),
(834, 187, 'User has logged in', '2020-11-23 13:54:09', '2020-11-23 13:54:09'),
(835, 63, 'User has logged in', '2020-11-23 13:55:10', '2020-11-23 13:55:10'),
(836, 187, 'User has logged in', '2020-11-23 13:55:40', '2020-11-23 13:55:40'),
(837, 63, 'User has logged in', '2020-11-23 14:01:45', '2020-11-23 14:01:45'),
(838, 187, 'User has logged in', '2020-11-23 14:07:21', '2020-11-23 14:07:21'),
(839, 187, 'Admin logged in as user', '2020-11-23 14:48:09', '2020-11-23 14:48:09'),
(840, 63, 'User has logged in', '2020-11-23 14:48:48', '2020-11-23 14:48:48'),
(841, 187, 'Admin updated user information', '2020-11-23 14:49:03', '2020-11-23 14:49:03'),
(842, 187, 'Admin updated user information', '2020-11-23 14:51:50', '2020-11-23 14:51:50'),
(843, 134, 'User has logged in', '2020-11-23 17:09:04', '2020-11-23 17:09:04'),
(844, 176, 'User has logged in', '2020-11-23 17:09:24', '2020-11-23 17:09:24'),
(845, 63, 'User has logged in', '2020-11-24 08:52:23', '2020-11-24 08:52:23'),
(846, 134, 'User has logged in', '2020-11-24 08:58:32', '2020-11-24 08:58:32'),
(847, 15, 'Admin logged in as user', '2020-11-24 09:03:43', '2020-11-24 09:03:43'),
(848, 15, 'Admin logged in as user', '2020-11-24 09:26:30', '2020-11-24 09:26:30'),
(849, 23, 'User has logged in', '2020-11-24 09:46:34', '2020-11-24 09:46:34'),
(850, 134, 'User has logged in', '2020-11-24 10:45:08', '2020-11-24 10:45:08'),
(851, 15, 'Admin logged in as user', '2020-11-24 10:45:57', '2020-11-24 10:45:57'),
(852, 176, 'User has logged in', '2020-11-24 11:40:27', '2020-11-24 11:40:27'),
(853, 176, 'User has logged in', '2020-11-24 13:48:14', '2020-11-24 13:48:14'),
(854, 176, 'User has logged in', '2020-11-24 13:48:47', '2020-11-24 13:48:47'),
(855, 134, 'User has logged in', '2020-11-24 14:07:48', '2020-11-24 14:07:48'),
(856, 134, 'User has logged in', '2020-11-25 10:12:27', '2020-11-25 10:12:27'),
(857, 176, 'User has logged in', '2020-11-25 10:31:27', '2020-11-25 10:31:27'),
(858, 134, 'User has logged in', '2020-11-25 10:39:45', '2020-11-25 10:39:45'),
(859, 176, 'User has logged in', '2020-11-25 11:38:36', '2020-11-25 11:38:36'),
(860, 134, 'User has logged in', '2020-11-25 11:39:13', '2020-11-25 11:39:13'),
(861, 176, 'User has logged in', '2020-11-25 11:55:14', '2020-11-25 11:55:14'),
(862, 134, 'User has logged in', '2020-11-25 12:35:03', '2020-11-25 12:35:03'),
(863, 176, 'User has logged in', '2020-11-25 21:20:46', '2020-11-25 21:20:46'),
(864, 134, 'User has logged in', '2020-11-26 10:24:02', '2020-11-26 10:24:02'),
(865, 176, 'User has logged in', '2020-11-26 11:06:49', '2020-11-26 11:06:49'),
(866, 63, 'User has logged in', '2020-11-26 12:10:27', '2020-11-26 12:10:27'),
(867, 176, 'User has logged in', '2020-11-26 13:00:12', '2020-11-26 13:00:12'),
(868, 134, 'User has logged in', '2020-11-26 13:41:36', '2020-11-26 13:41:36'),
(869, 15, 'Admin logged in as user', '2020-11-26 15:14:47', '2020-11-26 15:14:47'),
(870, 134, 'User has logged in', '2020-11-26 15:24:44', '2020-11-26 15:24:44'),
(871, 176, 'User has logged in', '2020-11-26 15:35:18', '2020-11-26 15:35:18'),
(872, 134, 'User has logged in', '2020-11-26 15:41:17', '2020-11-26 15:41:17'),
(873, 176, 'User has logged in', '2020-11-27 09:43:42', '2020-11-27 09:43:42'),
(874, 134, 'User has logged in', '2020-11-27 10:43:54', '2020-11-27 10:43:54'),
(875, 176, 'User has logged in', '2020-11-27 10:44:00', '2020-11-27 10:44:00'),
(876, 176, 'User has logged in', '2020-11-27 16:07:15', '2020-11-27 16:07:15'),
(877, 134, 'User has logged in', '2020-11-30 09:15:39', '2020-11-30 09:15:39'),
(878, 15, 'Admin logged in as user', '2020-11-30 09:15:45', '2020-11-30 09:15:45'),
(879, 41, 'Admin logged in as user', '2020-11-30 09:16:09', '2020-11-30 09:16:09'),
(880, 176, 'User has logged in', '2020-11-30 09:50:48', '2020-11-30 09:50:48'),
(881, 134, 'User has logged in', '2020-11-30 09:57:49', '2020-11-30 09:57:49'),
(882, 134, 'User has logged in', '2020-11-30 10:28:11', '2020-11-30 10:28:11'),
(883, 15, 'Admin logged in as user', '2020-11-30 10:28:26', '2020-11-30 10:28:26'),
(884, 134, 'User has logged in', '2020-11-30 13:01:48', '2020-11-30 13:01:48'),
(885, 15, 'Admin logged in as user', '2020-11-30 13:02:02', '2020-11-30 13:02:02'),
(886, 147, 'User has logged in', '2020-11-30 13:38:50', '2020-11-30 13:38:50'),
(887, 185, 'User has logged in', '2020-11-30 13:38:56', '2020-11-30 13:38:56'),
(888, 185, 'User has logged in', '2020-11-30 13:47:47', '2020-11-30 13:47:47'),
(889, 134, 'User has logged in', '2020-11-30 14:41:38', '2020-11-30 14:41:38'),
(890, 15, 'Admin logged in as user', '2020-11-30 14:41:45', '2020-11-30 14:41:45'),
(891, 185, 'User has logged in', '2020-11-30 14:44:24', '2020-11-30 14:44:24'),
(892, 176, 'User has logged in', '2020-12-01 10:48:24', '2020-12-01 10:48:24'),
(893, 176, 'User has logged in', '2020-12-01 10:50:06', '2020-12-01 10:50:06'),
(894, 185, 'User has logged in', '2020-12-01 10:50:53', '2020-12-01 10:50:53'),
(895, 176, 'User has logged in', '2020-12-01 10:54:07', '2020-12-01 10:54:07'),
(896, 176, 'User has logged in', '2020-12-01 10:56:07', '2020-12-01 10:56:07'),
(897, 185, 'User has logged in', '2020-12-01 10:57:31', '2020-12-01 10:57:31'),
(898, 167, 'User has logged in', '2020-12-01 10:57:49', '2020-12-01 10:57:49'),
(899, 167, 'User has logged in', '2020-12-01 10:59:37', '2020-12-01 10:59:37'),
(900, 134, 'User has logged in', '2020-12-01 11:02:57', '2020-12-01 11:02:57'),
(901, 166, 'Admin logged in as user', '2020-12-01 11:04:35', '2020-12-01 11:04:35'),
(902, 176, 'User has logged in', '2020-12-01 11:12:26', '2020-12-01 11:12:26'),
(903, 185, 'User has logged in', '2020-12-01 11:12:36', '2020-12-01 11:12:36'),
(904, 188, 'User has logged in', '2020-12-01 11:12:47', '2020-12-01 11:12:47'),
(905, 188, 'User has logged in', '2020-12-01 11:17:38', '2020-12-01 11:17:38'),
(906, 176, 'User has logged in', '2020-12-01 12:07:57', '2020-12-01 12:07:57'),
(907, 188, 'User has logged in', '2020-12-01 14:57:28', '2020-12-01 14:57:28'),
(908, 188, 'User has logged in', '2020-12-01 14:57:53', '2020-12-01 14:57:53'),
(909, 188, 'User has logged in', '2020-12-01 15:15:29', '2020-12-01 15:15:29'),
(910, 176, 'User has logged in', '2020-12-01 17:14:27', '2020-12-01 17:14:27'),
(911, 188, 'User has logged in', '2020-12-01 20:12:35', '2020-12-01 20:12:35'),
(912, 176, 'User has logged in', '2020-12-02 14:53:08', '2020-12-02 14:53:08'),
(913, 176, 'User has logged in', '2020-12-02 15:08:53', '2020-12-02 15:08:53'),
(914, 166, 'User has logged in', '2020-12-03 09:10:22', '2020-12-03 09:10:22'),
(915, 63, 'User has logged in', '2020-12-03 09:17:06', '2020-12-03 09:17:06'),
(916, 134, 'User has logged in', '2020-12-08 09:34:52', '2020-12-08 09:34:52'),
(917, 134, 'User has logged in', '2020-12-09 10:27:40', '2020-12-09 10:27:40'),
(918, 74, 'User has logged in', '2020-12-09 14:45:28', '2020-12-09 14:45:28'),
(919, 63, 'User has logged in', '2020-12-10 21:29:35', '2020-12-10 21:29:35'),
(920, 134, 'User has logged in', '2020-12-11 10:46:37', '2020-12-11 10:46:37'),
(921, 176, 'User has logged in', '2020-12-14 12:19:04', '2020-12-14 12:19:04'),
(922, 176, 'User has logged in', '2020-12-14 12:26:58', '2020-12-14 12:26:58'),
(923, 134, 'User has logged in', '2020-12-15 10:45:14', '2020-12-15 10:45:14'),
(924, 134, 'User has logged in', '2020-12-15 11:26:48', '2020-12-15 11:26:48'),
(925, 134, 'User has logged in', '2020-12-15 15:13:37', '2020-12-15 15:13:37'),
(926, 134, 'User has logged in', '2020-12-16 09:25:51', '2020-12-16 09:25:51'),
(927, 134, 'User has logged in', '2020-12-16 09:55:34', '2020-12-16 09:55:34'),
(928, 134, 'User has logged in', '2020-12-16 16:40:53', '2020-12-16 16:40:53'),
(929, 63, 'User has logged in', '2020-12-17 09:01:10', '2020-12-17 09:01:10'),
(930, 63, 'User has logged in', '2020-12-17 09:11:09', '2020-12-17 09:11:09'),
(931, 166, 'User has logged in', '2020-12-17 09:21:19', '2020-12-17 09:21:19'),
(932, 188, 'User has logged in', '2020-12-17 09:27:42', '2020-12-17 09:27:42'),
(933, 188, 'User has logged in', '2020-12-17 09:28:00', '2020-12-17 09:28:00'),
(934, 188, 'User has logged in', '2020-12-17 09:30:25', '2020-12-17 09:30:25'),
(935, 188, 'User has logged in', '2020-12-17 09:36:50', '2020-12-17 09:36:50'),
(936, 63, 'User has logged in', '2020-12-17 10:05:41', '2020-12-17 10:05:41'),
(937, 112, 'User has logged in', '2020-12-17 10:06:50', '2020-12-17 10:06:50'),
(938, 187, 'User has logged in', '2020-12-17 10:07:45', '2020-12-17 10:07:45'),
(939, 63, 'User has logged in', '2020-12-17 10:08:03', '2020-12-17 10:08:03'),
(940, 15, 'Admin logged in as user', '2020-12-17 10:08:14', '2020-12-17 10:08:14'),
(941, 15, 'Admin logged in as user', '2020-12-17 10:11:18', '2020-12-17 10:11:18'),
(942, 63, 'User has logged in', '2020-12-17 10:16:49', '2020-12-17 10:16:49'),
(943, 63, 'User has logged in', '2020-12-17 10:40:19', '2020-12-17 10:40:19'),
(944, 189, 'Admin updated user information', '2020-12-17 10:40:38', '2020-12-17 10:40:38'),
(945, 189, 'User has logged in', '2020-12-17 10:40:55', '2020-12-17 10:40:55'),
(946, 134, 'User has logged in', '2020-12-17 11:08:23', '2020-12-17 11:08:23'),
(947, 188, 'User has logged in', '2020-12-17 11:19:16', '2020-12-17 11:19:16'),
(948, 134, 'User has logged in', '2020-12-17 11:22:26', '2020-12-17 11:22:26'),
(949, 166, 'User has logged in', '2020-12-17 11:24:19', '2020-12-17 11:24:19'),
(950, 188, 'User has logged in', '2020-12-17 11:25:42', '2020-12-17 11:25:42'),
(951, 188, 'User has logged in', '2020-12-17 11:28:29', '2020-12-17 11:28:29'),
(952, 134, 'User has logged in', '2020-12-17 11:28:38', '2020-12-17 11:28:38'),
(953, 63, 'User has logged in', '2020-12-17 11:28:41', '2020-12-17 11:28:41'),
(954, 189, 'Admin updated user information', '2020-12-17 11:29:05', '2020-12-17 11:29:05'),
(955, 134, 'User has logged in', '2020-12-17 11:29:27', '2020-12-17 11:29:27'),
(956, 189, 'Admin logged in as user', '2020-12-17 11:29:33', '2020-12-17 11:29:33'),
(957, 188, 'User has logged in', '2020-12-17 11:39:54', '2020-12-17 11:39:54'),
(958, 188, 'User has logged in', '2020-12-17 12:37:25', '2020-12-17 12:37:25'),
(959, 134, 'User has logged in', '2020-12-17 12:59:19', '2020-12-17 12:59:19'),
(960, 15, 'Admin logged in as user', '2020-12-17 12:59:26', '2020-12-17 12:59:26'),
(961, 188, 'User has logged in', '2020-12-17 13:34:10', '2020-12-17 13:34:10'),
(962, 166, 'User has logged in', '2020-12-17 14:13:40', '2020-12-17 14:13:40'),
(963, 166, 'User has logged in', '2020-12-17 14:17:46', '2020-12-17 14:17:46'),
(964, 63, 'User has logged in', '2020-12-17 14:38:19', '2020-12-17 14:38:19'),
(965, 134, 'User has logged in', '2020-12-17 14:42:25', '2020-12-17 14:42:25'),
(966, 188, 'User has logged in', '2020-12-17 14:45:15', '2020-12-17 14:45:15'),
(967, 188, 'User has logged in', '2020-12-17 14:45:32', '2020-12-17 14:45:32'),
(968, 188, 'User has logged in', '2020-12-17 14:45:38', '2020-12-17 14:45:38'),
(969, 63, 'User has logged in', '2020-12-17 15:12:19', '2020-12-17 15:12:19'),
(970, 189, 'Admin logged in as user', '2020-12-17 15:14:45', '2020-12-17 15:14:45'),
(971, 188, 'User has logged in', '2020-12-18 10:02:17', '2020-12-18 10:02:17'),
(972, 190, 'User has logged in', '2020-12-18 23:10:51', '2020-12-18 23:10:51'),
(973, 190, 'User has logged in', '2020-12-20 22:36:26', '2020-12-20 22:36:26'),
(974, 190, 'User has logged in', '2020-12-21 10:36:22', '2020-12-21 10:36:22'),
(975, 63, 'User has logged in', '2020-12-21 11:34:36', '2020-12-21 11:34:36'),
(976, 63, 'User has logged in', '2020-12-21 11:37:06', '2020-12-21 11:37:06'),
(977, 191, 'Admin updated user information', '2020-12-21 11:37:18', '2020-12-21 11:37:18'),
(978, 191, 'User has logged in', '2020-12-21 11:37:42', '2020-12-21 11:37:42'),
(979, 166, 'User has logged in', '2020-12-21 13:38:43', '2020-12-21 13:38:43'),
(980, 190, 'User has logged in', '2021-01-02 11:23:40', '2021-01-02 11:23:40'),
(981, 190, 'User has logged in', '2021-01-02 23:05:21', '2021-01-02 23:05:21'),
(982, 190, 'User has logged in', '2021-01-03 12:19:42', '2021-01-03 12:19:42'),
(983, 190, 'User has logged in', '2021-01-04 19:08:43', '2021-01-04 19:08:43'),
(984, 189, 'User has logged in', '2021-01-05 08:59:11', '2021-01-05 08:59:11'),
(985, 187, 'User has logged in', '2021-01-05 08:59:24', '2021-01-05 08:59:24'),
(986, 190, 'User has logged in', '2021-01-05 09:37:58', '2021-01-05 09:37:58'),
(987, 191, 'User has logged in', '2021-01-05 10:34:46', '2021-01-05 10:34:46'),
(988, 134, 'User has logged in', '2021-01-05 11:56:29', '2021-01-05 11:56:29'),
(989, 189, 'User has logged in', '2021-01-05 14:01:54', '2021-01-05 14:01:54'),
(990, 191, 'User has logged in', '2021-01-05 14:31:47', '2021-01-05 14:31:47'),
(991, 190, 'User has logged in', '2021-01-06 09:14:22', '2021-01-06 09:14:22'),
(992, 134, 'User has logged in', '2021-01-06 09:43:47', '2021-01-06 09:43:47'),
(993, 190, 'User has logged in', '2021-01-06 09:58:55', '2021-01-06 09:58:55'),
(994, 134, 'User has logged in', '2021-01-06 14:13:04', '2021-01-06 14:13:04'),
(995, 134, 'User has logged in', '2021-01-06 15:36:26', '2021-01-06 15:36:26'),
(996, 119, 'Admin logged in as user', '2021-01-06 15:36:37', '2021-01-06 15:36:37'),
(997, 41, 'Admin logged in as user', '2021-01-06 15:36:42', '2021-01-06 15:36:42'),
(998, 190, 'User has logged in', '2021-01-06 15:40:08', '2021-01-06 15:40:08'),
(999, 190, 'User has logged in', '2021-01-06 15:41:19', '2021-01-06 15:41:19'),
(1000, 190, 'User has logged in', '2021-01-06 15:44:12', '2021-01-06 15:44:12'),
(1001, 134, 'User has logged in', '2021-01-06 15:48:53', '2021-01-06 15:48:53'),
(1002, 15, 'Admin logged in as user', '2021-01-06 15:51:55', '2021-01-06 15:51:55'),
(1003, 190, 'User has logged in', '2021-01-07 09:30:47', '2021-01-07 09:30:47'),
(1004, 190, 'User has logged in', '2021-01-07 09:39:42', '2021-01-07 09:39:42'),
(1005, 134, 'User has logged in', '2021-01-07 09:41:55', '2021-01-07 09:41:55'),
(1006, 23, 'User has logged in', '2021-01-07 09:57:15', '2021-01-07 09:57:15'),
(1007, 171, 'Admin logged in as user', '2021-01-07 10:01:13', '2021-01-07 10:01:13'),
(1008, 192, 'Admin logged in as user', '2021-01-07 10:01:34', '2021-01-07 10:01:34'),
(1009, 171, 'Admin logged in as user', '2021-01-07 10:02:00', '2021-01-07 10:02:00'),
(1010, 192, 'Admin updated user information', '2021-01-07 10:02:30', '2021-01-07 10:02:30'),
(1011, 192, 'Admin logged in as user', '2021-01-07 10:02:34', '2021-01-07 10:02:34'),
(1012, 134, 'User has logged in', '2021-01-07 10:04:43', '2021-01-07 10:04:43'),
(1013, 171, 'Admin logged in as user', '2021-01-07 10:06:29', '2021-01-07 10:06:29'),
(1014, 192, 'Admin logged in as user', '2021-01-07 10:07:17', '2021-01-07 10:07:17'),
(1015, 134, 'User has logged in', '2021-01-08 09:27:15', '2021-01-08 09:27:15'),
(1016, 73, 'User has logged in', '2021-01-08 10:32:32', '2021-01-08 10:32:32'),
(1017, 191, 'User has logged in', '2021-01-08 10:36:01', '2021-01-08 10:36:01'),
(1018, 191, 'User has logged in', '2021-01-08 10:38:19', '2021-01-08 10:38:19'),
(1019, 191, 'User has logged in', '2021-01-08 10:43:48', '2021-01-08 10:43:48'),
(1020, 73, 'User has logged in', '2021-01-08 16:26:18', '2021-01-08 16:26:18'),
(1021, 63, 'User has logged in', '2021-01-11 13:23:33', '2021-01-11 13:23:33'),
(1022, 189, 'User has logged in', '2021-01-11 13:24:02', '2021-01-11 13:24:02'),
(1023, 63, 'User has logged in', '2021-01-11 13:30:47', '2021-01-11 13:30:47'),
(1024, 189, 'Admin updated user information', '2021-01-11 13:31:55', '2021-01-11 13:31:55'),
(1025, 189, 'Admin updated user information', '2021-01-11 13:32:14', '2021-01-11 13:32:14'),
(1026, 189, 'User has logged in', '2021-01-11 13:32:23', '2021-01-11 13:32:23'),
(1027, 189, 'Admin updated user information', '2021-01-11 13:32:45', '2021-01-11 13:32:45'),
(1028, 63, 'User has logged in', '2021-01-11 13:35:57', '2021-01-11 13:35:57'),
(1029, 63, 'User has logged in', '2021-01-11 14:31:26', '2021-01-11 14:31:26'),
(1030, 134, 'User has logged in', '2021-01-12 09:49:26', '2021-01-12 09:49:26'),
(1031, 41, 'Admin logged in as user', '2021-01-12 09:53:02', '2021-01-12 09:53:02'),
(1032, 63, 'User has logged in', '2021-01-12 10:55:38', '2021-01-12 10:55:38'),
(1033, 134, 'User has logged in', '2021-01-12 11:26:55', '2021-01-12 11:26:55'),
(1034, 41, 'Admin logged in as user', '2021-01-12 12:39:56', '2021-01-12 12:39:56'),
(1035, 63, 'User has logged in', '2021-01-12 12:43:48', '2021-01-12 12:43:48'),
(1036, 119, 'Admin logged in as user', '2021-01-12 12:46:27', '2021-01-12 12:46:27'),
(1037, 120, 'Admin logged in as user', '2021-01-12 12:46:32', '2021-01-12 12:46:32'),
(1038, 134, 'User has logged in', '2021-01-12 12:46:45', '2021-01-12 12:46:45'),
(1039, 120, 'Admin logged in as user', '2021-01-12 12:46:53', '2021-01-12 12:46:53'),
(1040, 195, 'Admin updated user information', '2021-01-12 12:47:53', '2021-01-12 12:47:53'),
(1041, 119, 'Admin logged in as user', '2021-01-12 12:48:02', '2021-01-12 12:48:02'),
(1042, 41, 'Admin logged in as user', '2021-01-12 12:48:07', '2021-01-12 12:48:07'),
(1043, 117, 'Admin logged in as user', '2021-01-12 12:48:15', '2021-01-12 12:48:15'),
(1044, 134, 'User has logged in', '2021-01-12 12:48:45', '2021-01-12 12:48:45'),
(1045, 120, 'Admin updated user information', '2021-01-12 12:49:15', '2021-01-12 12:49:15'),
(1046, 120, 'Admin logged in as user', '2021-01-12 12:49:27', '2021-01-12 12:49:27'),
(1047, 134, 'User has logged in', '2021-01-12 12:49:50', '2021-01-12 12:49:50'),
(1048, 195, 'User has logged in', '2021-01-12 12:50:25', '2021-01-12 12:50:25'),
(1049, 120, 'Admin logged in as user', '2021-01-12 12:53:39', '2021-01-12 12:53:39'),
(1050, 134, 'User has logged in', '2021-01-12 12:53:57', '2021-01-12 12:53:57'),
(1051, 134, 'User has logged in', '2021-01-12 12:54:03', '2021-01-12 12:54:03'),
(1052, 120, 'Admin logged in as user', '2021-01-12 12:54:56', '2021-01-12 12:54:56'),
(1053, 134, 'User has logged in', '2021-01-12 12:55:03', '2021-01-12 12:55:03'),
(1054, 120, 'Admin updated user information', '2021-01-12 12:55:34', '2021-01-12 12:55:34'),
(1055, 120, 'Admin updated user information', '2021-01-12 12:57:11', '2021-01-12 12:57:11'),
(1056, 120, 'Admin updated user information', '2021-01-12 12:57:37', '2021-01-12 12:57:37'),
(1057, 120, 'Admin logged in as user', '2021-01-12 12:57:48', '2021-01-12 12:57:48'),
(1058, 197, 'Admin updated user information', '2021-01-12 13:10:12', '2021-01-12 13:10:12'),
(1059, 194, 'User has logged in', '2021-01-12 14:49:01', '2021-01-12 14:49:01'),
(1060, 134, 'User has logged in', '2021-01-12 14:52:18', '2021-01-12 14:52:18'),
(1061, 198, 'Admin updated user information', '2021-01-12 14:52:54', '2021-01-12 14:52:54'),
(1062, 198, 'User has logged in', '2021-01-12 14:53:05', '2021-01-12 14:53:05'),
(1063, 134, 'User has logged in', '2021-01-12 14:54:16', '2021-01-12 14:54:16'),
(1064, 41, 'Admin logged in as user', '2021-01-12 14:54:30', '2021-01-12 14:54:30'),
(1065, 134, 'User has logged in', '2021-01-12 14:55:15', '2021-01-12 14:55:15'),
(1066, 134, 'User has logged in', '2021-01-12 14:56:41', '2021-01-12 14:56:41'),
(1067, 198, 'User has logged in', '2021-01-12 15:01:19', '2021-01-12 15:01:19'),
(1068, 134, 'User has logged in', '2021-01-12 15:03:48', '2021-01-12 15:03:48'),
(1069, 198, 'User has logged in', '2021-01-12 15:06:12', '2021-01-12 15:06:12'),
(1070, 23, 'User has logged in', '2021-01-12 15:17:22', '2021-01-12 15:17:22'),
(1071, 195, 'User has logged in', '2021-01-12 17:20:50', '2021-01-12 17:20:50'),
(1072, 198, 'User has logged in', '2021-01-13 10:16:21', '2021-01-13 10:16:21'),
(1073, 134, 'User has logged in', '2021-01-13 10:16:31', '2021-01-13 10:16:31'),
(1074, 198, 'Admin updated user information', '2021-01-13 10:20:29', '2021-01-13 10:20:29'),
(1075, 198, 'User has logged in', '2021-01-13 10:20:37', '2021-01-13 10:20:37'),
(1076, 134, 'User has logged in', '2021-01-13 10:31:39', '2021-01-13 10:31:39'),
(1077, 195, 'Admin logged in as user', '2021-01-13 11:15:10', '2021-01-13 11:15:10'),
(1078, 195, 'Admin logged in as user', '2021-01-13 11:16:53', '2021-01-13 11:16:53'),
(1079, 134, 'User has logged in', '2021-01-13 11:45:51', '2021-01-13 11:45:51'),
(1080, 198, 'User has logged in', '2021-01-13 11:45:57', '2021-01-13 11:45:57'),
(1081, 134, 'User has logged in', '2021-01-13 11:51:41', '2021-01-13 11:51:41'),
(1082, 195, 'User has logged in', '2021-01-13 12:08:36', '2021-01-13 12:08:36'),
(1083, 63, 'User has logged in', '2021-01-14 11:21:43', '2021-01-14 11:21:43'),
(1084, 198, 'User has logged in', '2021-01-15 09:56:54', '2021-01-15 09:56:54'),
(1085, 134, 'User has logged in', '2021-01-15 10:49:01', '2021-01-15 10:49:01'),
(1086, 63, 'User has logged in', '2021-01-15 11:08:51', '2021-01-15 11:08:51'),
(1087, 198, 'User has logged in', '2021-01-15 11:16:17', '2021-01-15 11:16:17'),
(1088, 198, 'User has logged in', '2021-01-15 12:08:02', '2021-01-15 12:08:02'),
(1089, 198, 'User has logged in', '2021-01-15 12:14:59', '2021-01-15 12:14:59'),
(1090, 63, 'User has logged in', '2021-01-15 13:23:51', '2021-01-15 13:23:51'),
(1091, 195, 'User has logged in', '2021-01-15 13:24:52', '2021-01-15 13:24:52'),
(1092, 63, 'User has logged in', '2021-01-15 13:25:49', '2021-01-15 13:25:49'),
(1093, 175, 'Admin updated user information', '2021-01-15 13:26:40', '2021-01-15 13:26:40'),
(1094, 191, 'Admin updated user information', '2021-01-15 13:26:56', '2021-01-15 13:26:56'),
(1095, 73, 'User has logged in', '2021-01-15 13:32:55', '2021-01-15 13:32:55'),
(1096, 191, 'User has logged in', '2021-01-15 13:35:33', '2021-01-15 13:35:33'),
(1097, 198, 'User has logged in', '2021-01-18 10:10:07', '2021-01-18 10:10:07'),
(1098, 198, 'User has logged in', '2021-01-18 11:59:20', '2021-01-18 11:59:20'),
(1099, 134, 'User has logged in', '2021-01-18 12:01:05', '2021-01-18 12:01:05'),
(1100, 134, 'User has logged in', '2021-01-18 12:03:01', '2021-01-18 12:03:01'),
(1101, 134, 'User has logged in', '2021-01-20 11:12:55', '2021-01-20 11:12:55'),
(1102, 200, 'User has logged in', '2021-01-20 11:36:35', '2021-01-20 11:36:35'),
(1103, 134, 'User has logged in', '2021-01-20 11:39:32', '2021-01-20 11:39:32'),
(1104, 191, 'User has logged in', '2021-01-20 12:38:53', '2021-01-20 12:38:53'),
(1105, 63, 'User has logged in', '2021-01-20 15:10:36', '2021-01-20 15:10:36'),
(1106, 63, 'User has logged in', '2021-01-21 09:59:52', '2021-01-21 09:59:52'),
(1107, 195, 'User has logged in', '2021-01-21 17:39:46', '2021-01-21 17:39:46'),
(1108, 195, 'User updated their information', '2021-01-21 17:40:15', '2021-01-21 17:40:15'),
(1109, 195, 'User has logged in', '2021-01-21 17:40:35', '2021-01-21 17:40:35'),
(1110, 63, 'User has logged in', '2021-01-22 12:00:20', '2021-01-22 12:00:20'),
(1111, 175, 'User has logged in', '2021-01-22 14:53:51', '2021-01-22 14:53:51'),
(1112, 72, 'User has logged in', '2021-01-22 17:35:08', '2021-01-22 17:35:08'),
(1113, 63, 'User has logged in', '2021-01-25 12:36:16', '2021-01-25 12:36:16'),
(1114, 63, 'User has logged in', '2021-01-27 13:27:13', '2021-01-27 13:27:13'),
(1115, 63, 'User has logged in', '2021-01-28 10:22:24', '2021-01-28 10:22:24'),
(1116, 195, 'User has logged in', '2021-01-28 10:23:52', '2021-01-28 10:23:52'),
(1117, 63, 'User has logged in', '2021-01-28 11:56:01', '2021-01-28 11:56:01'),
(1118, 63, 'User has logged in', '2021-01-28 13:19:40', '2021-01-28 13:19:40'),
(1119, 63, 'User has logged in', '2021-01-29 16:08:45', '2021-01-29 16:08:45'),
(1120, 63, 'User has logged in', '2021-02-01 10:23:37', '2021-02-01 10:23:37'),
(1121, 23, 'User has logged in', '2021-02-01 10:41:15', '2021-02-01 10:41:15'),
(1122, 189, 'User has logged in', '2021-02-01 10:55:30', '2021-02-01 10:55:30'),
(1123, 63, 'User has logged in', '2021-02-01 12:10:49', '2021-02-01 12:10:49'),
(1124, 63, 'User has logged in', '2021-02-01 13:47:08', '2021-02-01 13:47:08'),
(1125, 63, 'User has logged in', '2021-02-01 13:52:50', '2021-02-01 13:52:50'),
(1126, 63, 'User has logged in', '2021-02-01 14:02:31', '2021-02-01 14:02:31'),
(1127, 187, 'Admin logged in as user', '2021-02-01 14:22:36', '2021-02-01 14:22:36'),
(1128, 195, 'Admin logged in as user', '2021-02-01 14:22:55', '2021-02-01 14:22:55'),
(1129, 63, 'User has logged in', '2021-02-01 14:23:27', '2021-02-01 14:23:27'),
(1130, 187, 'Admin updated user information', '2021-02-01 14:24:23', '2021-02-01 14:24:23'),
(1131, 187, 'User has logged in', '2021-02-01 14:24:35', '2021-02-01 14:24:35'),
(1132, 187, 'User has logged in', '2021-02-01 14:35:14', '2021-02-01 14:35:14'),
(1133, 62, 'User has logged in', '2021-02-02 11:26:12', '2021-02-02 11:26:12'),
(1134, 63, 'User has logged in', '2021-02-02 13:36:56', '2021-02-02 13:36:56'),
(1135, 187, 'Admin updated user information', '2021-02-02 13:37:27', '2021-02-02 13:37:27'),
(1136, 187, 'User has logged in', '2021-02-02 13:37:38', '2021-02-02 13:37:38'),
(1137, 63, 'User has logged in', '2021-02-03 10:50:11', '2021-02-03 10:50:11'),
(1138, 187, 'User has logged in', '2021-02-03 10:50:45', '2021-02-03 10:50:45'),
(1139, 187, 'User has logged in', '2021-02-03 15:06:47', '2021-02-03 15:06:47'),
(1140, 134, 'User has logged in', '2021-02-09 08:55:43', '2021-02-09 08:55:43'),
(1141, 202, 'User has logged in', '2021-02-10 08:57:50', '2021-02-10 08:57:50'),
(1142, 63, 'User has logged in', '2021-02-10 12:01:45', '2021-02-10 12:01:45'),
(1143, 63, 'User has logged in', '2021-02-10 14:05:51', '2021-02-10 14:05:51'),
(1144, 187, 'User has logged in', '2021-02-10 14:11:03', '2021-02-10 14:11:03'),
(1145, 189, 'User has logged in', '2021-02-11 09:16:02', '2021-02-11 09:16:02'),
(1146, 191, 'User has logged in', '2021-02-11 16:56:52', '2021-02-11 16:56:52'),
(1147, 62, 'User has logged in', '2021-02-13 03:55:58', '2021-02-13 03:55:58'),
(1148, 62, 'User has logged in', '2021-02-14 11:07:10', '2021-02-14 11:07:10'),
(1149, 62, 'User has logged in', '2021-02-15 10:23:32', '2021-02-15 10:23:32'),
(1150, 202, 'User has logged in', '2021-02-16 12:12:17', '2021-02-16 12:12:17'),
(1151, 134, 'User has logged in', '2021-02-16 12:12:23', '2021-02-16 12:12:23'),
(1152, 62, 'User has logged in', '2021-02-17 14:35:16', '2021-02-17 14:35:16'),
(1153, 63, 'User has logged in', '2021-02-17 14:37:09', '2021-02-17 14:37:09'),
(1154, 204, 'Admin updated user information', '2021-02-17 14:37:26', '2021-02-17 14:37:26'),
(1155, 191, 'User has logged in', '2021-02-17 14:41:00', '2021-02-17 14:41:00'),
(1156, 63, 'User has logged in', '2021-02-18 09:48:41', '2021-02-18 09:48:41'),
(1157, 204, 'User has logged in', '2021-02-18 11:30:36', '2021-02-18 11:30:36'),
(1158, 63, 'User has logged in', '2021-02-18 12:47:33', '2021-02-18 12:47:33'),
(1159, 63, 'User has logged in', '2021-02-19 14:39:11', '2021-02-19 14:39:11'),
(1160, 202, 'User has logged in', '2021-02-22 09:03:41', '2021-02-22 09:03:41'),
(1161, 134, 'User has logged in', '2021-02-22 09:03:47', '2021-02-22 09:03:47'),
(1162, 189, 'Admin logged in as user', '2021-02-22 09:04:01', '2021-02-22 09:04:01'),
(1163, 187, 'Admin logged in as user', '2021-02-22 09:04:05', '2021-02-22 09:04:05'),
(1164, 31, 'Admin logged in as user', '2021-02-22 09:04:21', '2021-02-22 09:04:21'),
(1165, 15, 'Admin logged in as user', '2021-02-22 09:04:25', '2021-02-22 09:04:25'),
(1166, 117, 'Admin logged in as user', '2021-02-22 09:04:29', '2021-02-22 09:04:29'),
(1167, 189, 'Admin logged in as user', '2021-02-22 09:05:46', '2021-02-22 09:05:46'),
(1168, 134, 'User has logged in', '2021-02-22 09:07:46', '2021-02-22 09:07:46'),
(1169, 187, 'Admin logged in as user', '2021-02-22 09:07:55', '2021-02-22 09:07:55'),
(1170, 189, 'Admin logged in as user', '2021-02-22 09:08:29', '2021-02-22 09:08:29'),
(1171, 134, 'User has logged in', '2021-02-22 09:09:24', '2021-02-22 09:09:24'),
(1172, 15, 'Admin logged in as user', '2021-02-22 09:09:31', '2021-02-22 09:09:31'),
(1173, 15, 'Admin logged in as user', '2021-02-22 09:11:39', '2021-02-22 09:11:39'),
(1174, 134, 'User has logged in', '2021-02-22 09:12:34', '2021-02-22 09:12:34'),
(1175, 74, 'Admin logged in as user', '2021-02-22 09:12:44', '2021-02-22 09:12:44'),
(1176, 134, 'User has logged in', '2021-02-22 09:15:55', '2021-02-22 09:15:55'),
(1177, 199, 'User has logged in', '2021-02-22 09:17:38', '2021-02-22 09:17:38'),
(1178, 134, 'User has logged in', '2021-02-22 09:19:00', '2021-02-22 09:19:00'),
(1179, 134, 'User has logged in', '2021-02-22 09:24:35', '2021-02-22 09:24:35'),
(1180, 134, 'User has logged in', '2021-02-22 09:27:22', '2021-02-22 09:27:22'),
(1181, 134, 'User has logged in', '2021-02-22 09:34:33', '2021-02-22 09:34:33'),
(1182, 117, 'Admin logged in as user', '2021-02-22 09:34:43', '2021-02-22 09:34:43'),
(1183, 117, 'Admin logged in as user', '2021-02-22 09:37:05', '2021-02-22 09:37:05'),
(1184, 134, 'User has logged in', '2021-02-22 09:38:16', '2021-02-22 09:38:16'),
(1185, 189, 'Admin logged in as user', '2021-02-22 09:38:30', '2021-02-22 09:38:30'),
(1186, 15, 'Admin logged in as user', '2021-02-22 09:39:39', '2021-02-22 09:39:39'),
(1187, 117, 'Admin logged in as user', '2021-02-22 09:40:08', '2021-02-22 09:40:08'),
(1188, 189, 'Admin logged in as user', '2021-02-22 09:40:33', '2021-02-22 09:40:33'),
(1189, 189, 'Admin logged in as user', '2021-02-22 09:41:26', '2021-02-22 09:41:26'),
(1190, 15, 'Admin logged in as user', '2021-02-22 09:42:03', '2021-02-22 09:42:03'),
(1191, 117, 'Admin logged in as user', '2021-02-22 09:42:19', '2021-02-22 09:42:19'),
(1192, 187, 'Admin logged in as user', '2021-02-22 09:42:38', '2021-02-22 09:42:38'),
(1193, 189, 'Admin logged in as user', '2021-02-22 09:42:52', '2021-02-22 09:42:52'),
(1194, 63, 'User has logged in', '2021-02-22 12:20:02', '2021-02-22 12:20:02'),
(1195, 187, 'Admin updated user information', '2021-02-22 12:23:12', '2021-02-22 12:23:12'),
(1196, 63, 'User has logged in', '2021-02-22 12:23:36', '2021-02-22 12:23:36'),
(1197, 187, 'User has logged in', '2021-02-22 12:24:11', '2021-02-22 12:24:11'),
(1198, 63, 'User has logged in', '2021-02-22 12:24:26', '2021-02-22 12:24:26'),
(1199, 187, 'Admin updated user information', '2021-02-22 12:24:39', '2021-02-22 12:24:39'),
(1200, 187, 'User has logged in', '2021-02-22 12:24:47', '2021-02-22 12:24:47'),
(1201, 63, 'User has logged in', '2021-02-22 12:25:19', '2021-02-22 12:25:19'),
(1202, 63, 'User has logged in', '2021-02-22 13:40:00', '2021-02-22 13:40:00'),
(1203, 199, 'User has logged in', '2021-02-22 14:35:20', '2021-02-22 14:35:20');

-- --------------------------------------------------------

--
-- Table structure for table `connections`
--

CREATE TABLE `connections` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `partner_id` int(11) NOT NULL,
  `connection_id` int(11) NOT NULL,
  `approved` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reason_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `partner_id` int(11) NOT NULL,
  `resolved` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `reason_id`, `user_id`, `name`, `email`, `subject`, `message`, `partner_id`, `resolved`, `created_at`, `updated_at`) VALUES
(3, 6, 63, 'Claire', 'claire@lsgi.co.uk', NULL, 'Test', 2, 1, '2020-05-07 08:59:26', '2020-05-19 12:01:13'),
(4, 7, 63, 'Claire', 'claire@lsg.co.uk', NULL, 'Test - account', 2, 1, '2020-05-07 09:00:00', '2020-05-19 13:31:44'),
(5, 8, 11, 'Claire', 'claire@trustbox.com', NULL, 'Test benefits', 2, 1, '2020-05-07 09:00:17', '2020-05-07 09:02:58'),
(6, 9, 63, 'claire', 'claire@lsgi.co.uk', NULL, 'test community', 2, 1, '2020-05-07 09:00:38', '2020-05-19 13:31:47'),
(7, 10, 63, 'claire', 'claire@lsgi.co.uk', NULL, 'test other', 2, 1, '2020-05-07 09:01:03', '2020-05-19 13:31:54'),
(8, 6, 63, 'Dominic Hadfield', 'dominichadfield@gmail.com', NULL, 'This is a test', 2, 1, '2020-05-13 10:00:53', '2020-05-19 12:01:09'),
(9, 10, NULL, 'Eric Jones', 'eric@talkwithwebvisitor.com', NULL, 'My name’s Eric and I just found your site yourtrustbox.co.uk.\r\n\r\nIt’s got a lot going for it, but here’s an idea to make it even MORE effective.\r\n\r\nTalk With Web Visitor – CLICK HERE http://www.talkwithwebvisitor.com for a live demo now.\r\n\r\nTalk With Web Visitor is a software widget that’s works on your site, ready to capture any visitor’s Name, Email address and Phone Number.  It signals you the moment they let you know they’re interested – so that you can talk to that lead while they’re literally looking over your site.\r\n\r\nAnd once you’ve captured their phone number, with our new SMS Text With Lead feature, you can automatically start a text (SMS) conversation… and if they don’t take you up on your offer then, you can follow up with text messages for new offers, content links, even just “how you doing?” notes to build a relationship.\r\n\r\nCLICK HERE http://www.talkwithwebvisitor.com to discover what Talk With Web Visitor can do for your business.\r\n\r\nThe difference between contacting someone within 5 minutes versus a half-hour means you could be converting up to 100X more leads today!\r\n\r\nEric\r\nPS: Studies show that 70% of a site’s visitors disappear and are gone forever after just a moment. Don’t keep losing them. \r\nTalk With Web Visitor offers a FREE 14 days trial – and it even includes International Long Distance Calling. \r\nYou have customers waiting to talk with you right now… don’t keep them waiting. \r\nCLICK HERE http://www.talkwithwebvisitor.com to try Talk With Web Visitor now.\r\n\r\nIf you\'d like to unsubscribe click here http://talkwithwebvisitor.com/unsubscribe.aspx?d=yourtrustbox.co.uk', 1, 0, '2020-06-03 14:12:14', '2020-06-03 14:12:14'),
(10, 10, NULL, 'Eric Jones', 'eric@talkwithwebvisitor.com', NULL, 'Hey there, I just found your site, quick question…\r\n\r\nMy name’s Eric, I found yourtrustbox.co.uk after doing a quick search – you showed up near the top of the rankings, so whatever you’re doing for SEO, looks like it’s working well.\r\n\r\nSo here’s my question – what happens AFTER someone lands on your site?  Anything?\r\n\r\nResearch tells us at least 70% of the people who find your site, after a quick once-over, they disappear… forever.\r\n\r\nThat means that all the work and effort you put into getting them to show up, goes down the tubes.\r\n\r\nWhy would you want all that good work – and the great site you’ve built – go to waste?\r\n\r\nBecause the odds are they’ll just skip over calling or even grabbing their phone, leaving you high and dry.\r\n\r\nBut here’s a thought… what if you could make it super-simple for someone to raise their hand, say, “okay, let’s talk” without requiring them to even pull their cell phone from their pocket?\r\n  \r\nYou can – thanks to revolutionary new software that can literally make that first call happen NOW.\r\n\r\nTalk With Web Visitor is a software widget that sits on your site, ready and waiting to capture any visitor’s Name, Email address and Phone Number.  It lets you know IMMEDIATELY – so that you can talk to that lead while they’re still there at your site.\r\n  \r\nYou know, strike when the iron’s hot!\r\n\r\nCLICK HERE http://www.talkwithwebvisitor.com to try out a Live Demo with Talk With Web Visitor now to see exactly how it works.\r\n\r\nWhen targeting leads, you HAVE to act fast – the difference between contacting someone within 5 minutes versus 30 minutes later is huge – like 100 times better!\r\n\r\nThat’s why you should check out our new SMS Text With Lead feature as well… once you’ve captured the phone number of the website visitor, you can automatically kick off a text message (SMS) conversation with them. \r\n \r\nImagine how powerful this could be – even if they don’t take you up on your offer immediately, you can stay in touch with them using text messages to make new offers, provide links to great content, and build your credibility.\r\n\r\nJust this alone could be a game changer to make your website even more effective.\r\n\r\nStrike when  the iron’s hot!\r\n\r\nCLICK HERE http://www.talkwithwebvisitor.com to learn more about everything Talk With Web Visitor can do for your business – you’ll be amazed.\r\n\r\nThanks and keep up the great work!\r\n\r\nEric\r\nPS: Talk With Web Visitor offers a FREE 14 days trial – you could be converting up to 100x more leads immediately!   \r\nIt even includes International Long Distance Calling. \r\nStop wasting money chasing eyeballs that don’t turn into paying customers. \r\nCLICK HERE http://www.talkwithwebvisitor.com to try Talk With Web Visitor now.\r\n\r\nIf you\'d like to unsubscribe click here http://talkwithwebvisitor.com/unsubscribe.aspx?d=yourtrustbox.co.uk', 1, 0, '2020-06-03 21:16:26', '2020-06-03 21:16:26'),
(11, 7, NULL, 'Abby Randle', 'hacker@tuwlmu.icu', NULL, 'PLEASE FORWARD THIS EMAIL TO SOMEONE IN YOUR COMPANY WHO IS ALLOWED TO MAKE IMPORTANT DECISIONS!\r\n\r\nWe have hacked your website http://www.yourtrustbox.co.uk and extracted your databases.\r\n\r\nHow did this happen?\r\nOur team has found a vulnerability within your site that we were able to exploit. After finding the vulnerability we were able to get your database credentials and extract your entire database and move the information to an offshore server.\r\n\r\nWhat does this mean?\r\n\r\nWe will systematically go through a series of steps of totally damaging your reputation. First your database will be leaked or sold to the highest bidder which they will use with whatever their intentions are. Next if there are e-mails found they will be e-mailed that their information has been sold or leaked and your site http://www.yourtrustbox.co.uk was at fault thusly damaging your reputation and having angry customers/associates with whatever angry customers/associates do. Lastly any links that you have indexed in the search engines will be de-indexed based off of blackhat techniques that we used in the past to de-index our targets.\r\n\r\nHow do I stop this?\r\n\r\nWe are willing to refrain from destroying your site\'s reputation for a small fee. The current fee is $2000 USD in bitcoins (BTC). \r\n\r\nSend the bitcoin to the following Bitcoin address (Copy and paste as it is case sensitive):\r\n\r\n12KLZzgrNX2DvbWQM7yQ1V9vPwy9JPvUKM\r\n\r\nOnce you have paid we will automatically get informed that it was your payment. Please note that you have to make payment within 5 days after receiving this notice or the database leak, e-mails dispatched, and de-index of your site WILL start!\r\n\r\nHow do I get Bitcoins?\r\n\r\nYou can easily buy bitcoins via several websites or even offline from a Bitcoin-ATM. We suggest you https://cex.io/ for buying bitcoins.\r\n\r\nWhat if I don’t pay?\r\n\r\nIf you decide not to pay, we will start the attack at the indicated date and uphold it until you do, there’s no counter measure to this, you will only end up wasting more money trying to find a solution. We will completely destroy your reputation amongst google and your customers.\r\n\r\nThis is not a hoax, do not reply to this email, don’t try to reason or negotiate, we will not read any replies. Once you have paid we will stop what we were doing and you will never hear from us again!\r\n\r\nPlease note that Bitcoin is anonymous and no one will find out that you have complied.', 1, 0, '2020-06-07 21:05:10', '2020-06-07 21:05:10'),
(12, 10, NULL, 'Claudia Clement', 'claudiauclement@yahoo.com', NULL, 'Hi, We are wondering if you would be interested in our service, where we can provide you with a dofollow link from Amazon (DA 96) back to yourtrustbox.co.uk?\r\n\r\nThe price is just $67 per link, via Paypal.\r\n\r\nTo explain what DA is and the benefit for your website, along with a sample of an existing link, please read here: https://pastelink.net/1nm60\r\n\r\nIf you\'d be interested in learning more, reply to this email but please make sure you include the word INTERESTED in the subject line field, so we can get to your reply sooner.\r\n\r\nKind Regards,\r\nClaudia', 1, 0, '2020-06-20 06:23:53', '2020-06-20 06:23:53'),
(13, 8, NULL, 'Barbara Atyson', 'barbaratysonhw@yahoo.com', NULL, 'Hi,\r\n\r\nWe\'d like to introduce to you our explainer video service which we feel can benefit your site yourtrustbox.co.uk.\r\n\r\nCheck out some of our existing videos here:\r\nhttps://www.youtube.com/watch?v=zvGF7uRfH04\r\nhttps://www.youtube.com/watch?v=MOnhn77TgDE\r\nhttps://www.youtube.com/watch?v=KhSCHaI6gw0\r\n\r\nAll of our videos are in a similar animated format as the above examples and we have voice over artists with US/UK/Australian accents.\r\n\r\nThey can show a solution to a problem or simply promote one of your products or services. They are concise, can be uploaded to video such as Youtube, and can be embedded into your website or featured on landing pages.\r\n\r\nOur prices are as follows depending on video length:\r\n0-1 minutes = $259\r\n1-2 minutes = $369\r\n2-3 minutes = $479\r\n\r\n*All prices above are in USD and include a custom video, full script and a voice-over.\r\n\r\nIf this is something you would like to discuss further, don\'t hesitate to get in touch.\r\nIf you are not interested, simply delete this message and we won\'t contact you again.\r\n\r\nKind Regards,\r\nBarbara', 1, 0, '2020-06-24 21:13:53', '2020-06-24 21:13:53'),
(14, 9, NULL, 'Barbara Atyson', 'barbaratysonhw@yahoo.com', NULL, 'Hi,\r\n\r\nWe\'d like to introduce to you our explainer video service which we feel can benefit your site yourtrustbox.co.uk.\r\n\r\nCheck out some of our existing videos here:\r\nhttps://www.youtube.com/watch?v=zvGF7uRfH04\r\nhttps://www.youtube.com/watch?v=MOnhn77TgDE\r\nhttps://www.youtube.com/watch?v=KhSCHaI6gw0\r\n\r\nAll of our videos are in a similar animated format as the above examples and we have voice over artists with US/UK/Australian accents.\r\n\r\nThey can show a solution to a problem or simply promote one of your products or services. They are concise, can be uploaded to video such as Youtube, and can be embedded into your website or featured on landing pages.\r\n\r\nOur prices are as follows depending on video length:\r\n0-1 minutes = $159\r\n1-2 minutes = $269\r\n2-3 minutes = $379\r\n3-4 minutes = $489\r\n\r\n*All prices above are in USD and include a custom video, full script and a voice-over.\r\n\r\nIf this is something you would like to discuss further, don\'t hesitate to get in touch.\r\nIf you are not interested, simply delete this message and we won\'t contact you again.\r\n\r\nKind Regards,\r\nBarbara', 1, 0, '2020-06-29 13:41:09', '2020-06-29 13:41:09'),
(15, 10, NULL, 'Harriet Shedden', 'hacker@yacaibet4374.com', NULL, 'PLEASE FORWARD THIS EMAIL TO SOMEONE IN YOUR COMPANY WHO IS ALLOWED TO MAKE IMPORTANT DECISIONS!\r\n\r\nWe have hacked your website http://www.yourtrustbox.co.uk and extracted your databases.\r\n\r\nHow did this happen?\r\nOur team has found a vulnerability within your site that we were able to exploit. After finding the vulnerability we were able to get your database credentials and extract your entire database and move the information to an offshore server.\r\n\r\nWhat does this mean?\r\n\r\nWe will systematically go through a series of steps of totally damaging your reputation. First your database will be leaked or sold to the highest bidder which they will use with whatever their intentions are. Next if there are e-mails found they will be e-mailed that their information has been sold or leaked and your site http://www.yourtrustbox.co.uk was at fault thusly damaging your reputation and having angry customers/associates with whatever angry customers/associates do. Lastly any links that you have indexed in the search engines will be de-indexed based off of blackhat techniques that we used in the past to de-index our targets.\r\n\r\nHow do I stop this?\r\n\r\nWe are willing to refrain from destroying your site\'s reputation for a small fee. The current fee is .33 BTC in bitcoins ($3000 USD). \r\n\r\nSend the bitcoin to the following Bitcoin address (Copy and paste as it is case sensitive):\r\n\r\n1FjMYuEXXRSPbey42fRkHwLgH1yohE2PZF\r\n\r\nOnce you have paid we will automatically get informed that it was your payment. Please note that you have to make payment within 5 days after receiving this notice or the database leak, e-mails dispatched, and de-index of your site WILL start!\r\n\r\nHow do I get Bitcoins?\r\n\r\nYou can easily buy bitcoins via several websites or even offline from a Bitcoin-ATM. We suggest you https://cex.io/ for buying bitcoins.\r\n\r\nWhat if I don’t pay?\r\n\r\nIf you decide not to pay, we will start the attack at the indicated date and uphold it until you do, there’s no counter measure to this, you will only end up wasting more money trying to find a solution. We will completely destroy your reputation amongst google and your customers.\r\n\r\nThis is not a hoax, do not reply to this email, don’t try to reason or negotiate, we will not read any replies. Once you have paid we will stop what we were doing and you will never hear from us again!\r\n\r\nPlease note that Bitcoin is anonymous and no one will find out that you have complied.', 1, 0, '2020-06-29 19:30:05', '2020-06-29 19:30:05'),
(16, 7, NULL, 'Claudia Clement', 'claudiauclement@yahoo.com', NULL, 'Hi, We are wondering if you would be interested in our service, where we can provide you with a dofollow link from Amazon (DA 96) back to yourtrustbox.co.uk?\r\n\r\nThe price is just $77 per link, via Paypal.\r\n\r\nTo explain what DA is and the benefit for your website, along with a sample of an existing link, please read here: https://pastelink.net/1nm60\r\n\r\nIf you\'d be interested in learning more, reply to this email but please make sure you include the word INTERESTED in the subject line field, so we can get to your reply sooner.\r\n\r\nKind Regards,\r\nClaudia', 1, 0, '2020-07-02 08:29:58', '2020-07-02 08:29:58'),
(17, 9, 1, 'adam johnson', 'info@visionsharp.co.uk', NULL, 'test', 8, 1, '2020-07-15 08:22:59', '2020-07-15 08:23:24'),
(18, 9, NULL, 'Eric Jones', 'eric@talkwithwebvisitor.com', NULL, 'Hi, my name is Eric and I’m betting you’d like your website yourtrustbox.co.uk to generate more leads.\r\n\r\nHere’s how:\r\nTalk With Web Visitor is a software widget that’s works on your site, ready to capture any visitor’s Name, Email address and Phone Number.  It signals you as soon as they say they’re interested – so that you can talk to that lead while they’re still there at yourtrustbox.co.uk.\r\n\r\nTalk With Web Visitor – CLICK HERE http://www.talkwithwebvisitor.com for a live demo now.\r\n\r\nAnd now that you’ve got their phone number, our new SMS Text With Lead feature enables you to start a text (SMS) conversation – answer questions, provide more info, and close a deal that way.\r\n\r\nIf they don’t take you up on your offer then, just follow up with text messages for new offers, content links, even just “how you doing?” notes to build a relationship.\r\n\r\nCLICK HERE http://www.talkwithwebvisitor.com to discover what Talk With Web Visitor can do for your business.\r\n\r\nThe difference between contacting someone within 5 minutes versus a half-hour means you could be converting up to 100X more leads today!\r\n\r\nTry Talk With Web Visitor and get more leads now.\r\n\r\nEric\r\nPS: The studies show 7 out of 10 visitors don’t hang around – you can’t afford to lose them!\r\nTalk With Web Visitor offers a FREE 14 days trial – and it even includes International Long Distance Calling. \r\nYou have customers waiting to talk with you right now… don’t keep them waiting. \r\nCLICK HERE http://www.talkwithwebvisitor.com to try Talk With Web Visitor now.\r\n\r\nIf you\'d like to unsubscribe click here http://talkwithwebvisitor.com/unsubscribe.aspx?d=yourtrustbox.co.uk', 1, 0, '2020-07-21 09:01:39', '2020-07-21 09:01:39'),
(19, 10, NULL, 'Erick Lovins', 'erick.lovins@gmail.com', NULL, 'Hello, I have been informed to contact you. The CIA has been doing intensive research for the past fifty years researching on what we call so called life. That information has been collected and presented for you here https://bit.ly/3lqUJ3u This has been the finding as of seventeen years ago as of today. Now governments and other large organizations have develop technology around these concepts for their own deceptive uses. Soon you will be contacted by other means for counter measures and the part that you play in all this. Please get this as soon as possible because there are powers that be to take down this information about this.', 1, 0, '2020-08-30 09:27:40', '2020-08-30 09:27:40'),
(20, 7, NULL, 'Galen Junkins', 'galen.junkins@gmail.com', NULL, 'Hi,\r\n\r\nWe\'re wondering if you\'ve considered taking the written content from yourtrustbox.co.uk and converting it into videos to promote on Youtube? It\'s another method of generating traffic.\r\n\r\nThere\'s a 14 day free trial available to you at the following link: https://bit.ly/2CbkcMo\r\n\r\nRegards,\r\nGalen', 1, 0, '2020-09-29 05:16:15', '2020-09-29 05:16:15'),
(21, 7, NULL, 'Eric Jones', 'eric@talkwithwebvisitor.com', NULL, 'Hey, this is Eric and I ran across yourtrustbox.co.uk a few minutes ago.\r\n\r\nLooks great… but now what?\r\n\r\nBy that I mean, when someone like me finds your website – either through Search or just bouncing around – what happens next?  Do you get a lot of leads from your site, or at least enough to make you happy?\r\n\r\nHonestly, most business websites fall a bit short when it comes to generating paying customers. Studies show that 70% of a site’s visitors disappear and are gone forever after just a moment.\r\n\r\nHere’s an idea…\r\n \r\nHow about making it really EASY for every visitor who shows up to get a personal phone call you as soon as they hit your site…\r\n \r\nYou can –\r\n  \r\nTalk With Web Visitor is a software widget that’s works on your site, ready to capture any visitor’s Name, Email address and Phone Number.  It signals you the moment they let you know they’re interested – so that you can talk to that lead while they’re literally looking over your site.\r\n\r\nCLICK HERE http://www.talkwithcustomer.com to try out a Live Demo with Talk With Web Visitor now to see exactly how it works.\r\n\r\nYou’ll be amazed - the difference between contacting someone within 5 minutes versus a half-hour or more later could increase your results 100-fold.\r\n\r\nIt gets even better… once you’ve captured their phone number, with our new SMS Text With Lead feature, you can automatically start a text (SMS) conversation.\r\n  \r\nThat way, even if you don’t close a deal right away, you can follow up with text messages for new offers, content links, even just “how you doing?” notes to build a relationship.\r\n\r\nPretty sweet – AND effective.\r\n\r\nCLICK HERE http://www.talkwithcustomer.com to discover what Talk With Web Visitor can do for your business.\r\n\r\nYou could be converting up to 100X more leads today!\r\n\r\nEric\r\nPS: Talk With Web Visitor offers a FREE 14 days trial – and it even includes International Long Distance Calling. \r\nYou have customers waiting to talk with you right now… don’t keep them waiting. \r\nCLICK HERE http://www.talkwithcustomer.com to try Talk With Web Visitor now.\r\n\r\nIf you\'d like to unsubscribe click here http://talkwithcustomer.com/unsubscribe.aspx?d=yourtrustbox.co.uk', 1, 0, '2020-11-02 10:06:03', '2020-11-02 10:06:03'),
(22, 6, NULL, 'GREG HANNAFORD', 'greg.h@setsquared.group', NULL, 'Hello,\r\n\r\nThis might be a duplicate? Not sure if I have already registered (June 2020)?\r\n\r\nI would like to avail my mother of this offer. Thanks\r\n\r\nBW,  \r\nGreg', 4, 0, '2020-11-04 16:43:25', '2020-11-04 16:43:25'),
(23, 10, NULL, 'Eric Jones', 'ericjonesonline@outlook.com', NULL, 'Hey there, I just found your site, quick question…\r\n\r\nMy name’s Eric, I found yourtrustbox.co.uk after doing a quick search – you showed up near the top of the rankings, so whatever you’re doing for SEO, looks like it’s working well.\r\n\r\nSo here’s my question – what happens AFTER someone lands on your site?  Anything?\r\n\r\nResearch tells us at least 70% of the people who find your site, after a quick once-over, they disappear… forever.\r\n\r\nThat means that all the work and effort you put into getting them to show up, goes down the tubes.\r\n\r\nWhy would you want all that good work – and the great site you’ve built – go to waste?\r\n\r\nBecause the odds are they’ll just skip over calling or even grabbing their phone, leaving you high and dry.\r\n\r\nBut here’s a thought… what if you could make it super-simple for someone to raise their hand, say, “okay, let’s talk” without requiring them to even pull their cell phone from their pocket?\r\n  \r\nYou can – thanks to revolutionary new software that can literally make that first call happen NOW.\r\n\r\nTalk With Web Visitor is a software widget that sits on your site, ready and waiting to capture any visitor’s Name, Email address and Phone Number.  It lets you know IMMEDIATELY – so that you can talk to that lead while they’re still there at your site.\r\n  \r\nYou know, strike when the iron’s hot!\r\n\r\nCLICK HERE http://www.talkwithwebvisitors.com to try out a Live Demo with Talk With Web Visitor now to see exactly how it works.\r\n\r\nWhen targeting leads, you HAVE to act fast – the difference between contacting someone within 5 minutes versus 30 minutes later is huge – like 100 times better!\r\n\r\nThat’s why you should check out our new SMS Text With Lead feature as well… once you’ve captured the phone number of the website visitor, you can automatically kick off a text message (SMS) conversation with them. \r\n \r\nImagine how powerful this could be – even if they don’t take you up on your offer immediately, you can stay in touch with them using text messages to make new offers, provide links to great content, and build your credibility.\r\n\r\nJust this alone could be a game changer to make your website even more effective.\r\n\r\nStrike when  the iron’s hot!\r\n\r\nCLICK HERE http://www.talkwithwebvisitors.com to learn more about everything Talk With Web Visitor can do for your business – you’ll be amazed.\r\n\r\nThanks and keep up the great work!\r\n\r\nEric\r\nPS: Talk With Web Visitor offers a FREE 14 days trial – you could be converting up to 100x more leads immediately!   \r\nIt even includes International Long Distance Calling. \r\nStop wasting money chasing eyeballs that don’t turn into paying customers. \r\nCLICK HERE http://www.talkwithwebvisitors.com to try Talk With Web Visitor now.\r\n\r\nIf you\'d like to unsubscribe click here http://talkwithwebvisitors.com/unsubscribe.aspx?d=yourtrustbox.co.uk', 1, 0, '2020-11-10 12:10:00', '2020-11-10 12:10:00'),
(24, 9, NULL, 'Eric Jones', 'ericjonesonline@outlook.com', NULL, 'Hey there, I just found your site, quick question…\r\n\r\nMy name’s Eric, I found yourtrustbox.co.uk after doing a quick search – you showed up near the top of the rankings, so whatever you’re doing for SEO, looks like it’s working well.\r\n\r\nSo here’s my question – what happens AFTER someone lands on your site?  Anything?\r\n\r\nResearch tells us at least 70% of the people who find your site, after a quick once-over, they disappear… forever.\r\n\r\nThat means that all the work and effort you put into getting them to show up, goes down the tubes.\r\n\r\nWhy would you want all that good work – and the great site you’ve built – go to waste?\r\n\r\nBecause the odds are they’ll just skip over calling or even grabbing their phone, leaving you high and dry.\r\n\r\nBut here’s a thought… what if you could make it super-simple for someone to raise their hand, say, “okay, let’s talk” without requiring them to even pull their cell phone from their pocket?\r\n  \r\nYou can – thanks to revolutionary new software that can literally make that first call happen NOW.\r\n\r\nTalk With Web Visitor is a software widget that sits on your site, ready and waiting to capture any visitor’s Name, Email address and Phone Number.  It lets you know IMMEDIATELY – so that you can talk to that lead while they’re still there at your site.\r\n  \r\nYou know, strike when the iron’s hot!\r\n\r\nCLICK HERE http://www.talkwithcustomer.com to try out a Live Demo with Talk With Web Visitor now to see exactly how it works.\r\n\r\nWhen targeting leads, you HAVE to act fast – the difference between contacting someone within 5 minutes versus 30 minutes later is huge – like 100 times better!\r\n\r\nThat’s why you should check out our new SMS Text With Lead feature as well… once you’ve captured the phone number of the website visitor, you can automatically kick off a text message (SMS) conversation with them. \r\n \r\nImagine how powerful this could be – even if they don’t take you up on your offer immediately, you can stay in touch with them using text messages to make new offers, provide links to great content, and build your credibility.\r\n\r\nJust this alone could be a game changer to make your website even more effective.\r\n\r\nStrike when  the iron’s hot!\r\n\r\nCLICK HERE http://www.talkwithcustomer.com to learn more about everything Talk With Web Visitor can do for your business – you’ll be amazed.\r\n\r\nThanks and keep up the great work!\r\n\r\nEric\r\nPS: Talk With Web Visitor offers a FREE 14 days trial – you could be converting up to 100x more leads immediately!   \r\nIt even includes International Long Distance Calling. \r\nStop wasting money chasing eyeballs that don’t turn into paying customers. \r\nCLICK HERE http://www.talkwithcustomer.com to try Talk With Web Visitor now.\r\n\r\nIf you\'d like to unsubscribe click here http://talkwithcustomer.com/unsubscribe.aspx?d=yourtrustbox.co.uk', 1, 0, '2020-11-17 05:00:08', '2020-11-17 05:00:08'),
(25, 7, NULL, 'Eric Jones', 'ericjonesonline@outlook.com', NULL, 'Hello, my name’s Eric and I just ran across your website at yourtrustbox.co.uk...\r\n\r\nI found it after a quick search, so your SEO’s working out…\r\n\r\nContent looks pretty good…\r\n\r\nOne thing’s missing though…\r\n\r\nA QUICK, EASY way to connect with you NOW.\r\n\r\nBecause studies show that a web lead like me will only hang out a few seconds – 7 out of 10 disappear almost instantly, Surf Surf Surf… then gone forever.\r\n\r\nI have the solution:\r\n\r\nTalk With Web Visitor is a software widget that’s works on your site, ready to capture any visitor’s Name, Email address and Phone Number.  You’ll know immediately they’re interested and you can call them directly to TALK with them - literally while they’re still on the web looking at your site.\r\n\r\nCLICK HERE http://www.talkwithwebvisitors.com to try out a Live Demo with Talk With Web Visitor now to see exactly how it works and even give it a try… it could be huge for your business.\r\n\r\nPlus, now that you’ve got that phone number, with our new SMS Text With Lead feature, you can automatically start a text (SMS) conversation pronto… which is so powerful, because connecting with someone within the first 5 minutes is 100 times more effective than waiting 30 minutes or more later.\r\n\r\nThe new text messaging feature lets you follow up regularly with new offers, content links, even just follow up notes to build a relationship.\r\n\r\nEverything I’ve just described is extremely simple to implement, cost-effective, and profitable.\r\n \r\nCLICK HERE http://www.talkwithwebvisitors.com to discover what Talk With Web Visitor can do for your business, potentially converting up to 100X more eyeballs into leads today!\r\n\r\nEric\r\nPS: Talk With Web Visitor offers a FREE 14 days trial – and it even includes International Long Distance Calling. \r\nYou have customers waiting to talk with you right now… don’t keep them waiting. \r\nCLICK HERE http://www.talkwithwebvisitors.com to try Talk With Web Visitor now.\r\n\r\nIf you\'d like to unsubscribe click here http://talkwithwebvisitors.com/unsubscribe.aspx?d=yourtrustbox.co.uk', 1, 0, '2020-12-02 09:28:13', '2020-12-02 09:28:13'),
(27, 9, NULL, 'Eric Jones', 'ericjonesonline@outlook.com', NULL, 'Hello, my name’s Eric and I just ran across your website at yourtrustbox.co.uk...\r\n\r\nI found it after a quick search, so your SEO’s working out…\r\n\r\nContent looks pretty good…\r\n\r\nOne thing’s missing though…\r\n\r\nA QUICK, EASY way to connect with you NOW.\r\n\r\nBecause studies show that a web lead like me will only hang out a few seconds – 7 out of 10 disappear almost instantly, Surf Surf Surf… then gone forever.\r\n\r\nI have the solution:\r\n\r\nTalk With Web Visitor is a software widget that’s works on your site, ready to capture any visitor’s Name, Email address and Phone Number.  You’ll know immediately they’re interested and you can call them directly to TALK with them - literally while they’re still on the web looking at your site.\r\n\r\nCLICK HERE http://www.talkwithcustomer.com to try out a Live Demo with Talk With Web Visitor now to see exactly how it works and even give it a try… it could be huge for your business.\r\n\r\nPlus, now that you’ve got that phone number, with our new SMS Text With Lead feature, you can automatically start a text (SMS) conversation pronto… which is so powerful, because connecting with someone within the first 5 minutes is 100 times more effective than waiting 30 minutes or more later.\r\n\r\nThe new text messaging feature lets you follow up regularly with new offers, content links, even just follow up notes to build a relationship.\r\n\r\nEverything I’ve just described is extremely simple to implement, cost-effective, and profitable.\r\n \r\nCLICK HERE http://www.talkwithcustomer.com to discover what Talk With Web Visitor can do for your business, potentially converting up to 100X more eyeballs into leads today!\r\n\r\nEric\r\nPS: Talk With Web Visitor offers a FREE 14 days trial – and it even includes International Long Distance Calling. \r\nYou have customers waiting to talk with you right now… don’t keep them waiting. \r\nCLICK HERE http://www.talkwithcustomer.com to try Talk With Web Visitor now.\r\n\r\nIf you\'d like to unsubscribe click here http://talkwithcustomer.com/unsubscribe.aspx?d=yourtrustbox.co.uk', 1, 0, '2020-12-26 17:28:40', '2020-12-26 17:28:40'),
(28, 7, NULL, 'Eric Jones', 'ericjonesonline@outlook.com', NULL, 'Good day, \r\n\r\nMy name is Eric and unlike a lot of emails you might get, I wanted to instead provide you with a word of encouragement – Congratulations\r\n\r\nWhat for?  \r\n\r\nPart of my job is to check out websites and the work you’ve done with yourtrustbox.co.uk definitely stands out. \r\n\r\nIt’s clear you took building a website seriously and made a real investment of time and resources into making it top quality.\r\n\r\nThere is, however, a catch… more accurately, a question…\r\n\r\nSo when someone like me happens to find your site – maybe at the top of the search results (nice job BTW) or just through a random link, how do you know? \r\n\r\nMore importantly, how do you make a connection with that person?\r\n\r\nStudies show that 7 out of 10 visitors don’t stick around – they’re there one second and then gone with the wind.\r\n\r\nHere’s a way to create INSTANT engagement that you may not have known about… \r\n\r\nTalk With Web Visitor is a software widget that’s works on your site, ready to capture any visitor’s Name, Email address and Phone Number.  It lets you know INSTANTLY that they’re interested – so that you can talk to that lead while they’re literally checking out yourtrustbox.co.uk.\r\n\r\nCLICK HERE http://www.talkwithwebvisitors.com to try out a Live Demo with Talk With Web Visitor now to see exactly how it works.\r\n\r\nIt could be a game-changer for your business – and it gets even better… once you’ve captured their phone number, with our new SMS Text With Lead feature, you can automatically start a text (SMS) conversation – immediately (and there’s literally a 100X difference between contacting someone within 5 minutes versus 30 minutes.)\r\n\r\nPlus then, even if you don’t close a deal right away, you can connect later on with text messages for new offers, content links, even just follow up notes to build a relationship.\r\n\r\nEverything I’ve just described is simple, easy, and effective. \r\n\r\nCLICK HERE http://www.talkwithwebvisitors.com to discover what Talk With Web Visitor can do for your business.\r\n\r\nYou could be converting up to 100X more leads today!\r\n\r\nEric\r\nPS: Talk With Web Visitor offers a FREE 14 days trial – and it even includes International Long Distance Calling. \r\nYou have customers waiting to talk with you right now… don’t keep them waiting. \r\nCLICK HERE http://www.talkwithwebvisitors.com to try Talk With Web Visitor now.\r\n\r\nIf you\'d like to unsubscribe click here http://talkwithwebvisitors.com/unsubscribe.aspx?d=yourtrustbox.co.uk', 1, 0, '2021-01-04 09:25:01', '2021-01-04 09:25:01'),
(29, 11, NULL, 'Erica Jackson', 'ericajacksonmi0@yahoo.com', NULL, 'Hi, \r\n\r\nWe\'re wondering if you\'d be interested in a \'dofollow\' backlink to yourtrustbox.co.uk from our DA50 website?\r\n\r\nOur website is dedicated to facts/education, and so can host articles on pretty much any topic.\r\n\r\nYou can either write a new article yourself, or we can link from existing content. The price is just $45 and you can pay once the article/link has been published. This is a one-time fee, so there are no extra charges.\r\n\r\nAlso: Once the article has been published, and your backlink has been added, it will be shared out to over 2.8 million social media followers (if it\'s educationally based). This means you aren\'t just getting the high valued backlink, you\'re also getting the potential of more traffic to your site.\r\n\r\nIf you\'re interested, please reply to this email, including the word \'interested\' in the Subject Field.\r\n\r\nKind Regards,\r\nErica', 1, 0, '2021-01-11 19:44:03', '2021-01-11 19:44:03'),
(30, 8, NULL, 'Eric Jones', 'ericjonesonline@outlook.com', NULL, 'Hello, my name’s Eric and I just ran across your website at yourtrustbox.co.uk...\r\n\r\nI found it after a quick search, so your SEO’s working out…\r\n\r\nContent looks pretty good…\r\n\r\nOne thing’s missing though…\r\n\r\nA QUICK, EASY way to connect with you NOW.\r\n\r\nBecause studies show that a web lead like me will only hang out a few seconds – 7 out of 10 disappear almost instantly, Surf Surf Surf… then gone forever.\r\n\r\nI have the solution:\r\n\r\nTalk With Web Visitor is a software widget that’s works on your site, ready to capture any visitor’s Name, Email address and Phone Number.  You’ll know immediately they’re interested and you can call them directly to TALK with them - literally while they’re still on the web looking at your site.\r\n\r\nCLICK HERE http://www.talkwithcustomer.com to try out a Live Demo with Talk With Web Visitor now to see exactly how it works and even give it a try… it could be huge for your business.\r\n\r\nPlus, now that you’ve got that phone number, with our new SMS Text With Lead feature, you can automatically start a text (SMS) conversation pronto… which is so powerful, because connecting with someone within the first 5 minutes is 100 times more effective than waiting 30 minutes or more later.\r\n\r\nThe new text messaging feature lets you follow up regularly with new offers, content links, even just follow up notes to build a relationship.\r\n\r\nEverything I’ve just described is extremely simple to implement, cost-effective, and profitable.\r\n \r\nCLICK HERE http://www.talkwithcustomer.com to discover what Talk With Web Visitor can do for your business, potentially converting up to 100X more eyeballs into leads today!\r\n\r\nEric\r\nPS: Talk With Web Visitor offers a FREE 14 days trial – and it even includes International Long Distance Calling. \r\nYou have customers waiting to talk with you right now… don’t keep them waiting. \r\nCLICK HERE http://www.talkwithcustomer.com to try Talk With Web Visitor now.\r\n\r\nIf you\'d like to unsubscribe click here http://talkwithcustomer.com/unsubscribe.aspx?d=yourtrustbox.co.uk', 1, 0, '2021-01-18 04:56:47', '2021-01-18 04:56:47'),
(31, 8, NULL, 'Sam Mawhinney', 'sam.mawhinney@lsgi.co.uk', 'NULL', 'Test', 9, 0, '2021-01-20 10:00:56', '2021-01-20 10:00:56'),
(32, 12, NULL, 'Eric Jones', 'ericjonesonline@outlook.com', NULL, 'Good day, \r\n\r\nMy name is Eric and unlike a lot of emails you might get, I wanted to instead provide you with a word of encouragement – Congratulations\r\n\r\nWhat for?  \r\n\r\nPart of my job is to check out websites and the work you’ve done with yourtrustbox.co.uk definitely stands out. \r\n\r\nIt’s clear you took building a website seriously and made a real investment of time and resources into making it top quality.\r\n\r\nThere is, however, a catch… more accurately, a question…\r\n\r\nSo when someone like me happens to find your site – maybe at the top of the search results (nice job BTW) or just through a random link, how do you know? \r\n\r\nMore importantly, how do you make a connection with that person?\r\n\r\nStudies show that 7 out of 10 visitors don’t stick around – they’re there one second and then gone with the wind.\r\n\r\nHere’s a way to create INSTANT engagement that you may not have known about… \r\n\r\nTalk With Web Visitor is a software widget that’s works on your site, ready to capture any visitor’s Name, Email address and Phone Number.  It lets you know INSTANTLY that they’re interested – so that you can talk to that lead while they’re literally checking out yourtrustbox.co.uk.\r\n\r\nCLICK HERE http://www.talkwithwebvisitors.com to try out a Live Demo with Talk With Web Visitor now to see exactly how it works.\r\n\r\nIt could be a game-changer for your business – and it gets even better… once you’ve captured their phone number, with our new SMS Text With Lead feature, you can automatically start a text (SMS) conversation – immediately (and there’s literally a 100X difference between contacting someone within 5 minutes versus 30 minutes.)\r\n\r\nPlus then, even if you don’t close a deal right away, you can connect later on with text messages for new offers, content links, even just follow up notes to build a relationship.\r\n\r\nEverything I’ve just described is simple, easy, and effective. \r\n\r\nCLICK HERE http://www.talkwithwebvisitors.com to discover what Talk With Web Visitor can do for your business.\r\n\r\nYou could be converting up to 100X more leads today!\r\n\r\nEric\r\nPS: Talk With Web Visitor offers a FREE 14 days trial – and it even includes International Long Distance Calling. \r\nYou have customers waiting to talk with you right now… don’t keep them waiting. \r\nCLICK HERE http://www.talkwithwebvisitors.com to try Talk With Web Visitor now.\r\n\r\nIf you\'d like to unsubscribe click here http://talkwithwebvisitors.com/unsubscribe.aspx?d=yourtrustbox.co.uk', 1, 0, '2021-01-23 00:50:07', '2021-01-23 00:50:07'),
(33, 11, NULL, 'Terrence Milligan', 'terrence@stardatagroup.com', NULL, 'It is with sad regret to inform you StarDataGroup.com is shutting down.\r\nIt has been a tough year all round and we decided to go out with a bang!\r\n\r\nAny group of databases listed below is $49 or $149 for all 16 databases in this one time offer.\r\nYou can purchase it at www.StarDataGroup.com and view samples.\r\n\r\n- LinkedIn Database\r\n 43,535,433 LinkedIn Records\r\n\r\n- USA B2B Companies Database\r\n 28,147,835 Companies\r\n\r\n- Forex\r\n Forex South Africa 113,550 Forex Traders\r\n Forex Australia 135,696 Forex Traders\r\n Forex UK 779,674 Forex Traders\r\n\r\n- UK Companies Database\r\n 521,303 Companies\r\n\r\n- German Databases\r\n German Companies Database: 2,209,191 Companies\r\n German Executives Database: 985,048 Executives\r\n\r\n- Australian Companies Database\r\n 1,806,596 Companies\r\n\r\n- UAE Companies Database\r\n 950,652 Companies\r\n\r\n- Affiliate Marketers Database\r\n 494,909 records\r\n\r\n- South African Databases\r\n B2B Companies Database: 1,462,227 Companies\r\n Directors Database: 758,834 Directors\r\n Healthcare Database: 376,599 Medical Professionals\r\n Wholesalers Database: 106,932 Wholesalers\r\n Real Estate Agent Database: 257,980 Estate Agents\r\n Forex South Africa: 113,550 Forex Traders\r\n\r\nVisit www.stardatagroup.com or contact us with any queries.\r\n\r\nKind Regards,\r\nStarDataGroup.com', 1, 0, '2021-01-27 05:01:14', '2021-01-27 05:01:14'),
(34, 7, NULL, 'Jill Mathy', 'jill@stardatagroup.com', NULL, 'It is with sad regret to inform you StarDataGroup.com is shutting down.\r\n\r\nFire sale till the 7th of Feb.\r\n\r\nAny group of databases listed below is $49 or $149 for all 16 databases in this one time offer.\r\nYou can purchase it at www.StarDataGroup.com and view samples.\r\n\r\n- LinkedIn Database\r\n 43,535,433 LinkedIn Records\r\n\r\n- USA B2B Companies Database\r\n 28,147,835 Companies\r\n\r\n- Forex\r\n Forex South Africa 113,550 Forex Traders\r\n Forex Australia 135,696 Forex Traders\r\n Forex UK 779,674 Forex Traders\r\n\r\n- UK Companies Database\r\n 521,303 Companies\r\n\r\n- German Databases\r\n German Companies Database: 2,209,191 Companies\r\n German Executives Database: 985,048 Executives\r\n\r\n- Australian Companies Database\r\n 1,806,596 Companies\r\n\r\n- UAE Companies Database\r\n 950,652 Companies\r\n\r\n- Affiliate Marketers Database\r\n 494,909 records\r\n\r\n- South African Databases\r\n B2B Companies Database: 1,462,227 Companies\r\n Directors Database: 758,834 Directors\r\n Healthcare Database: 376,599 Medical Professionals\r\n Wholesalers Database: 106,932 Wholesalers\r\n Real Estate Agent Database: 257,980 Estate Agents\r\n Forex South Africa: 113,550 Forex Traders\r\n\r\nVisit www.stardatagroup.com or contact us with any queries.\r\n\r\nKind Regards,\r\nStarDataGroup.com', 1, 0, '2021-02-03 00:59:49', '2021-02-03 00:59:49'),
(35, 7, NULL, 'Eric Jones', 'ericjonesonline@outlook.com', NULL, 'Cool website!\r\n\r\nMy name’s Eric, and I just found your site - yourtrustbox.co.uk - while surfing the net. You showed up at the top of the search results, so I checked you out. Looks like what you’re doing is pretty cool.\r\n \r\nBut if you don’t mind me asking – after someone like me stumbles across yourtrustbox.co.uk, what usually happens?\r\n\r\nIs your site generating leads for your business? \r\n \r\nI’m guessing some, but I also bet you’d like more… studies show that 7 out 10 who land on a site wind up leaving without a trace.\r\n\r\nNot good.\r\n\r\nHere’s a thought – what if there was an easy way for every visitor to “raise their hand” to get a phone call from you INSTANTLY… the second they hit your site and said, “call me now.”\r\n\r\nYou can –\r\n  \r\nTalk With Web Visitor is a software widget that’s works on your site, ready to capture any visitor’s Name, Email address and Phone Number.  It lets you know IMMEDIATELY – so that you can talk to that lead while they’re literally looking over your site.\r\n\r\nCLICK HERE http://www.talkwithwebvisitors.com to try out a Live Demo with Talk With Web Visitor now to see exactly how it works.\r\n\r\nTime is money when it comes to connecting with leads – the difference between contacting someone within 5 minutes versus 30 minutes later can be huge – like 100 times better!\r\n\r\nThat’s why we built out our new SMS Text With Lead feature… because once you’ve captured the visitor’s phone number, you can automatically start a text message (SMS) conversation.\r\n  \r\nThink about the possibilities – even if you don’t close a deal then and there, you can follow up with text messages for new offers, content links, even just “how you doing?” notes to build a relationship.\r\n\r\nWouldn’t that be cool?\r\n\r\nCLICK HERE http://www.talkwithwebvisitors.com to discover what Talk With Web Visitor can do for your business.\r\n\r\nYou could be converting up to 100X more leads today!\r\nEric\r\n\r\nPS: Talk With Web Visitor offers a FREE 14 days trial – and it even includes International Long Distance Calling. \r\nYou have customers waiting to talk with you right now… don’t keep them waiting. \r\nCLICK HERE http://www.talkwithwebvisitors.com to try Talk With Web Visitor now.\r\n\r\nIf you\'d like to unsubscribe click here http://talkwithwebvisitors.com/unsubscribe.aspx?d=yourtrustbox.co.uk', 1, 0, '2021-02-03 05:07:11', '2021-02-03 05:07:11'),
(36, 9, NULL, 'Eric Jones', 'ericjonesonline@outlook.com', NULL, 'Hello, my name’s Eric and I just ran across your website at yourtrustbox.co.uk...\r\n\r\nI found it after a quick search, so your SEO’s working out…\r\n\r\nContent looks pretty good…\r\n\r\nOne thing’s missing though…\r\n\r\nA QUICK, EASY way to connect with you NOW.\r\n\r\nBecause studies show that a web lead like me will only hang out a few seconds – 7 out of 10 disappear almost instantly, Surf Surf Surf… then gone forever.\r\n\r\nI have the solution:\r\n\r\nTalk With Web Visitor is a software widget that’s works on your site, ready to capture any visitor’s Name, Email address and Phone Number.  You’ll know immediately they’re interested and you can call them directly to TALK with them - literally while they’re still on the web looking at your site.\r\n\r\nCLICK HERE http://www.talkwithcustomer.com to try out a Live Demo with Talk With Web Visitor now to see exactly how it works and even give it a try… it could be huge for your business.\r\n\r\nPlus, now that you’ve got that phone number, with our new SMS Text With Lead feature, you can automatically start a text (SMS) conversation pronto… which is so powerful, because connecting with someone within the first 5 minutes is 100 times more effective than waiting 30 minutes or more later.\r\n\r\nThe new text messaging feature lets you follow up regularly with new offers, content links, even just follow up notes to build a relationship.\r\n\r\nEverything I’ve just described is extremely simple to implement, cost-effective, and profitable.\r\n \r\nCLICK HERE http://www.talkwithcustomer.com to discover what Talk With Web Visitor can do for your business, potentially converting up to 100X more eyeballs into leads today!\r\n\r\nEric\r\nPS: Talk With Web Visitor offers a FREE 14 days trial – and it even includes International Long Distance Calling. \r\nYou have customers waiting to talk with you right now… don’t keep them waiting. \r\nCLICK HERE http://www.talkwithcustomer.com to try Talk With Web Visitor now.\r\n\r\nIf you\'d like to unsubscribe click here http://talkwithcustomer.com/unsubscribe.aspx?d=yourtrustbox.co.uk', 1, 0, '2021-02-08 23:34:23', '2021-02-08 23:34:23'),
(37, 8, NULL, 'Eric Jones', 'eric.jones.z.mail@gmail.com', NULL, 'Hey, this is Eric and I ran across yourtrustbox.co.uk a few minutes ago.\r\n\r\nLooks great… but now what?\r\n\r\nBy that I mean, when someone like me finds your website – either through Search or just bouncing around – what happens next?  Do you get a lot of leads from your site, or at least enough to make you happy?\r\n\r\nHonestly, most business websites fall a bit short when it comes to generating paying customers. Studies show that 70% of a site’s visitors disappear and are gone forever after just a moment.\r\n\r\nHere’s an idea…\r\n \r\nHow about making it really EASY for every visitor who shows up to get a personal phone call you as soon as they hit your site…\r\n \r\nYou can –\r\n  \r\nTalk With Web Visitor is a software widget that’s works on your site, ready to capture any visitor’s Name, Email address and Phone Number.  It signals you the moment they let you know they’re interested – so that you can talk to that lead while they’re literally looking over your site.\r\n\r\nCLICK HERE https://talkwithwebvisitors.com to try out a Live Demo with Talk With Web Visitor now to see exactly how it works.\r\n\r\nYou’ll be amazed - the difference between contacting someone within 5 minutes versus a half-hour or more later could increase your results 100-fold.\r\n\r\nIt gets even better… once you’ve captured their phone number, with our new SMS Text With Lead feature, you can automatically start a text (SMS) conversation.\r\n  \r\nThat way, even if you don’t close a deal right away, you can follow up with text messages for new offers, content links, even just “how you doing?” notes to build a relationship.\r\n\r\nPretty sweet – AND effective.\r\n\r\nCLICK HERE https://talkwithwebvisitors.com to discover what Talk With Web Visitor can do for your business.\r\n\r\nYou could be converting up to 100X more leads today!\r\n\r\nEric\r\nPS: Talk With Web Visitor offers a FREE 14 days trial – and it even includes International Long Distance Calling. \r\nYou have customers waiting to talk with you right now… don’t keep them waiting. \r\nCLICK HERE https://talkwithwebvisitors.com to try Talk With Web Visitor now.\r\n\r\nIf you\'d like to unsubscribe click here http://talkwithwebvisitors.com/unsubscribe.aspx?d=yourtrustbox.co.uk', 1, 0, '2021-02-10 08:12:36', '2021-02-10 08:12:36');
INSERT INTO `contacts` (`id`, `reason_id`, `user_id`, `name`, `email`, `subject`, `message`, `partner_id`, `resolved`, `created_at`, `updated_at`) VALUES
(38, 12, NULL, 'Rebekah Ulm', 'rebekah@sendbulkmails.com', NULL, 'Use SendBulkMails.com to run email campaigns from your own private dashboard.\r\n\r\nCold emails are allowed and won\'t get you blocked :)\r\n\r\n- 1Mil emails / mo @ $99 USD\r\n- Dedicated IP and Domain Included\r\n- Detailed statistical reports (delivery, bounce, clicks etc.)\r\n- Quick and easy setup with extended support at no extra cost.\r\n- Cancel anytime!\r\n\r\nRegards,\r\nwww.SendBulkMails.com', 1, 0, '2021-02-16 08:02:26', '2021-02-16 08:02:26');

-- --------------------------------------------------------

--
-- Table structure for table `debit_payments`
--

CREATE TABLE `debit_payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subscription_id` int(11) NOT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `benefit_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `benefit_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `debit_payments`
--

INSERT INTO `debit_payments` (`id`, `subscription_id`, `amount`, `created_at`, `updated_at`, `transaction_id`, `benefit_id`, `benefit_name`) VALUES
(1, 6, '10.00', '2020-11-05 11:34:39', '2020-11-05 11:34:39', '17229269316045759642424', '5', ''),
(2, 7, '10.00', '2020-11-05 11:36:33', '2020-11-05 11:36:33', '17229269316045760834242', '5', ''),
(3, 8, '10.00', '2020-11-05 11:44:44', '2020-11-05 11:44:44', '17229269316045766824242', '5', ''),
(4, 9, '3.00', '2020-11-05 11:47:39', '2020-11-05 11:47:39', '17229269316045764574242', '8', ''),
(5, 10, '10.00', '2020-11-05 11:53:04', '2020-11-05 11:53:04', '17229269316045771824242', '5', ''),
(6, 11, '10.00', '2020-11-05 11:59:49', '2020-11-05 11:59:49', '17229269316045775874242', '5', ''),
(7, 12, '10.00', '2020-11-05 12:02:02', '2020-11-05 12:02:02', '17229269316045777214242', '5', ''),
(8, 13, '10.00', '2020-11-05 13:04:54', '2020-11-05 13:04:54', '17229269316045814914242', '5', ''),
(9, 14, '10.00', '2020-11-05 15:09:33', '2020-11-05 15:09:33', '17229269316045889712424', '5', ''),
(10, 15, '5.00', '2020-11-06 11:55:39', '2020-11-06 11:55:39', '17229269316046637361111', '2', ''),
(11, 16, '5.00', '2020-11-06 16:50:12', '2020-11-06 16:50:12', '17229269316046814112424', '2', ''),
(12, 17, '5.00', '2020-11-06 16:57:38', '2020-11-06 16:57:38', '17229269316046813134242', '2', ''),
(13, 19, '3.00', '2020-11-06 18:05:31', '2020-11-06 18:05:31', '17229269316046859294242', '8', '24/7 Legal Support'),
(14, 20, '10.00', '2020-11-09 12:00:37', '2020-11-09 12:00:37', '17229269316049232354242', '5', 'Money Management Made Easy'),
(15, 21, '10.00', '2020-11-09 18:57:46', '2020-11-09 18:57:46', '17229269316049482654242', '6', 'Investments Simplified'),
(16, 22, '5.00', '2020-11-10 10:22:18', '2020-11-10 10:22:18', '17229269316050037364242', '2', 'File Manager'),
(17, 23, '5.00', '2020-11-10 12:07:33', '2020-11-10 12:07:33', '17229269316050093924242', '2', 'File Manager'),
(18, 24, '10.00', '2020-11-11 12:50:10', '2020-11-11 12:50:10', '17229269316050990084242', '6', 'Investments Simplified'),
(19, 25, '10.00', '2020-11-11 16:27:08', '2020-11-11 16:27:08', '17229269316051120264242', '6', 'Investments Simplified'),
(20, 26, '10.00', '2020-11-11 16:37:31', '2020-11-11 16:37:31', '17229269316051119954242', '6', 'Investments Simplified'),
(21, 27, '10.00', '2020-11-11 16:42:30', '2020-11-11 16:42:30', '17229269316051129484242', '6', 'Investments Simplified'),
(22, 28, '3.00', '2020-11-11 18:59:59', '2020-11-11 18:59:59', '17229269316051211984242', '8', '24/7 Legal Support'),
(23, 29, '10.00', '2020-11-11 19:06:38', '2020-11-11 19:06:38', '17229269316051215964242', '6', 'Investments Simplified'),
(24, 30, '5.00', '2020-12-14 12:27:26', '2020-12-14 12:27:26', '17229269316079488444242', '2', 'File Manager'),
(25, 31, '30.00', '2020-12-17 11:45:37', '2020-12-17 11:45:37', '17229269316082055364244', '23', 'Digital Will Writing'),
(26, 32, '30.00', '2021-01-05 09:42:22', '2021-01-05 09:42:22', '17229269316098397404242', '20', 'Customer Care Package'),
(27, 33, '10.00', '2021-01-13 10:21:44', '2021-01-13 10:21:44', '17229269316105333024242', '5', 'Money Management Made Easy'),
(28, 34, '30.00', '2021-01-20 09:57:24', '2021-01-20 09:57:24', '17229269316111366424242', '20', 'Customer Care Package'),
(29, 35, '10.00', '2021-02-22 09:18:10', '2021-02-22 09:18:10', '17229269316139854862424', '5', 'Money Management Made Easy');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invitations`
--

CREATE TABLE `invitations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `partner` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `address1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address3` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address4` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invitations`
--

INSERT INTO `invitations` (`id`, `name`, `email`, `code`, `status`, `partner`, `type`, `url`, `created_at`, `updated_at`, `address1`, `address2`, `address3`, `address4`, `postcode`) VALUES
(1, 'Simon Mattison', 'simon@lsgi.co.uk', '4008', 'sent', 'Redstone Wills', 'email', NULL, '2020-11-23 13:28:45', '2020-11-23 13:37:39', 'Unit 5', 'Millbank House', 'Riverside Business Park', 'Wilmslow', 'SK9 1BJ'),
(2, 'Dominic Hadfield', 'dominichadfield@gmail.com', '8422', 'completed', 'Redstone Wills', 'email', NULL, '2020-11-23 13:33:58', '2020-12-03 09:17:37', '2 Lanreath Close', '', '', 'Macclesfield', 'SK10 3PS'),
(159, 'Sam Mawhinney', 'sam@trusttech42.co.uk', '5903', 'sent', 'Open Money', 'email', NULL, '2020-12-17 11:33:31', '2020-12-17 11:33:31', 'Flat 4', '38 Palatine Road', 'Withington', 'Manchester', 'M20 3JL'),
(160, 'Sam Mawhinney', 'bwinnyzmate@gmail.com', '2513', 'sent', 'Open Money', 'email', NULL, '2020-12-17 11:33:31', '2020-12-17 11:33:31', 'Flat 4', '38 Palatine Road', 'Withington', 'Manchester', 'M20 3JL'),
(161, 'Sam Mawhinney', 'sam@trusttech42.co.uk', '1049', 'sent', 'Redstone Wills', 'email', NULL, '2020-12-17 11:33:54', '2020-12-17 11:33:54', 'Flat 4', '38 Palatine Road', 'Withington', 'Manchester', 'M20 3JL'),
(162, 'Sam Mawhinney', 'bwinnyzmate@gmail.com', '8487', 'sent', 'Redstone Wills', 'email', NULL, '2020-12-17 11:33:54', '2020-12-17 11:33:55', 'Flat 4', '38 Palatine Road', 'Withington', 'Manchester', 'M20 3JL'),
(163, 'Dom Hadfield', 'dominic@millbankhousegroup.com', '8728', 'sent', 'Remarkable Practice', 'email', NULL, '2021-01-11 14:28:42', '2021-01-11 14:29:09', 'Suite 15, Riverside Business Centre', 'Foundry Lane', 'Milford', 'Derbyshire	', 'DE56 0RN'),
(164, 'Doug Aitken', 'douglas@remarkablepractice.com', '4769', 'sent', 'Remarkable Practice', 'email', NULL, '2021-01-11 16:26:02', '2021-01-11 16:26:03', 'Suite 15, Riverside Business Centre', 'Foundry Lane', 'Milford', 'Derbyshire	', 'DE56 0RN'),
(165, 'Paul Shrimpling', 'paul@remarkablepractice.com', '5081', 'completed', 'Remarkable Practice', 'email', NULL, '2021-01-12 12:44:40', '2021-01-12 13:08:47', 'Suite 15, Riverside Business Centre', 'Foundry Lane', 'Milford', 'Derbyshire	', 'DE56 0RN'),
(166, 'Sam Mawhinney', 'bwinnyzmate@gmail.com', '9316', 'sent', 'Open Money', 'email', NULL, '2021-01-18 12:01:36', '2021-01-18 12:01:37', 'Flat 4', '38 Palatine Road', 'Withington', 'Manchester', 'M20 3JL'),
(167, 'Sam Mawhinney', 'bwinnyzmate@gmail.com', '8035', 'completed', 'Open Money', 'email', NULL, '2021-01-18 12:03:48', '2021-01-18 12:17:16', 'Flat 4', '38 Palatine Road', 'Withington', 'Manchester', 'M20 3JL'),
(168, 'Dominic Hadfield', 'dominichadfield@me.com', '2168', 'sent', 'Zen Wills', 'email', NULL, '2021-02-01 12:15:44', '2021-02-01 12:15:45', 'Suite 15, Riverside Business Centre', 'Foundry Lane', 'Milford', 'Derbyshire	', 'DE56 0RN'),
(169, 'Patrick Harrison', 'patrick@lsgi.co.uk', '1992', 'sent', 'Zen Wills', 'email', NULL, '2021-02-01 12:21:09', '2021-02-01 12:21:09', 'Suite 15, Riverside Business Centre', 'Foundry Lane', 'Milford', 'Derbyshire	', 'DE56 0RN');

-- --------------------------------------------------------

--
-- Table structure for table `invites`
--

CREATE TABLE `invites` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `accepted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invites`
--

INSERT INTO `invites` (`id`, `user_id`, `name`, `email`, `plan`, `accepted`, `created_at`, `updated_at`, `code`) VALUES
(34, 23, 'Test', 'adijono11@hotmail.com', 'household', 0, '2020-05-11 15:37:25', '2020-05-11 15:37:25', ''),
(35, 24, 'Claire', 'Yorkie365@aol.com', 'household', 0, '2020-05-11 16:27:25', '2020-05-11 16:27:25', ''),
(36, 24, 'Claire', 'sadler.claire88@gmail.com', 'covid-19', 0, '2020-05-11 16:27:51', '2020-05-11 16:27:51', ''),
(37, 25, 'Kathryn', 'kathryn.e.stockdale@gmail.com', 'covid-19', 0, '2020-05-11 16:45:10', '2020-05-11 16:45:10', ''),
(38, 26, 'Tom', 'sadle.tom99@gmail.com', 'covid-19', 0, '2020-05-11 16:48:36', '2020-05-11 16:48:36', ''),
(39, 26, 'Tom', 'sadler.tom99@gmail.com', 'covid-19', 0, '2020-05-11 16:49:06', '2020-05-11 16:49:06', ''),
(40, 23, 'test', 'jonathan@visionsharp.co.uk', 'household', 0, '2020-05-12 14:55:21', '2020-05-12 14:55:21', ''),
(41, 23, 'test', 'adijono11@hotmail.com', 'household', 0, '2020-05-12 14:56:13', '2020-05-12 14:56:13', ''),
(42, 32, 'Beth Hadfield', 'beth.hadfield89@gmail.com', 'household', 0, '2020-05-13 07:18:36', '2020-05-13 07:18:36', ''),
(49, 23, 'hello', 'hello@visionsharp.co.uk', 'household', 0, '2020-05-14 07:58:46', '2020-05-14 07:58:46', ''),
(51, 23, 'callum', 'callum@visionsharp.co.uk', 'household', 0, '2020-05-15 07:59:31', '2020-05-15 07:59:31', ''),
(53, 55, 'Dom Sf', 'dominic@spicerfinch.co.uk', 'covid-19', 0, '2020-05-15 10:05:23', '2020-05-15 10:05:23', ''),
(54, 55, 'Dom EPR', 'dominic@estateplannersregister.co.uk', 'household', 0, '2020-05-15 10:13:03', '2020-05-15 10:13:03', ''),
(55, 55, 'Dom TB', 'dominic@yourtrustbox.co.uk', 'household', 0, '2020-05-15 10:14:55', '2020-05-15 10:14:55', ''),
(56, 55, 'Dom TB2', 'dominic@trustboxltd.com', 'household', 0, '2020-05-15 10:16:07', '2020-05-15 10:16:07', ''),
(59, 23, 'jamie', 'jamie@visionsharp.co.uk', 'household', 0, '2020-05-15 14:08:47', '2020-05-15 14:08:47', ''),
(63, 34, 'Lewis Stanton', 'lewystanton@gmail.com', 'household', 1, '2020-05-15 15:10:07', '2020-05-15 15:11:02', 'HP6Gkv4EeqHt2LzFY8Mn'),
(64, 23, 'callum', 'callum@visionsharp.co.uk', 'household', 1, '2020-05-15 15:16:41', '2020-05-15 15:17:29', 'hCUyXXwDJd0RAkF1F7v8'),
(65, 55, 'Dom', 'dominic@trustboxltd.com', 'household', 1, '2020-05-18 13:02:39', '2020-05-18 13:03:30', 'qPChpNN1EdhxdZEpGElI'),
(66, 62, 'Carolyn Evans', 'carolynjevans57@googlemail.com', 'household', 0, '2020-05-19 09:12:03', '2020-05-19 09:12:03', 'TJWh3n55a9HCVgJ0F1C2'),
(67, 62, 'Tony Harrison', 'tah42@outlook.com', 'covid-19', 1, '2020-05-19 09:12:43', '2020-05-20 08:04:03', '2V7hmOMxSdA96tJzmbae'),
(68, 62, 'Tom Harrison', 'tomharrison679@gmail.com', 'household', 0, '2020-05-19 09:21:51', '2020-05-19 09:21:51', 'Cu2mjJXgMN7HRsuF6Yc7'),
(69, 62, 'Helen Harrison', 'helenharrison44@gmail.com', 'covid-19', 0, '2020-05-19 09:23:18', '2020-05-19 09:23:18', 'mhenvTbh3Drlrz4tSPiU'),
(70, 64, 'Elaine Iveson', 'eli@asisvc.uk', 'household', 1, '2020-05-19 10:12:50', '2020-05-19 11:55:50', 'Ni84rKZTrjPFr8gMgrvI'),
(71, 70, 'Claire Stead', 'clairoy@me.com', 'household', 0, '2020-05-19 13:12:32', '2020-05-19 13:12:32', 'pkI5eDhL3UoCdURkPpkZ'),
(72, 32, 'Simon', 'simon@legalservicesguild.co.uk', 'household', 0, '2020-05-19 13:38:11', '2020-05-19 13:38:11', 'iSL5IwqHQS33Zb2lItxc'),
(73, 73, 'Helen Hayward', 'helenhayward1664@gmail.com', 'household', 0, '2020-05-19 14:24:05', '2020-05-19 14:24:05', 'MhlAlXS5XegY8nLjqWmW'),
(74, 73, 'Iain Hayward', 'iainhayward@btinternet.com', 'household', 0, '2020-05-19 14:24:47', '2020-05-19 14:24:47', 'FLDUKT6UX6nLM0bAjVRJ'),
(75, 73, 'Alex Deacon', 'alexfd1993@outlook.com', 'household', 0, '2020-05-20 07:25:59', '2020-05-20 07:25:59', 'FJWBq3oZiPhQl3hkexbP'),
(76, 79, 'JP Outlook', 'jon.pidduck.lsgi@outlook.com', 'household', 1, '2020-05-20 09:01:50', '2020-05-20 09:05:56', 'PSF6Z0EYDJAzRLugXTdv'),
(77, 85, 'Kath Brooke-Webb', 'kath@brooke-webb.co.uk', 'covid-19', 0, '2020-05-20 10:06:30', '2020-05-20 10:06:30', 'toZEpLR4AbJm1u4YA3Fp'),
(78, 85, 'Liz Beeston', 'lizbeeston123@btinternet.com', 'covid-19', 0, '2020-05-20 10:07:19', '2020-05-20 10:07:19', 'U7CYMuWKXBWxaxVhCwWp'),
(79, 85, 'Jenny Bainbridge', 'rowen2@btinternet.com', 'covid-19', 0, '2020-05-20 10:11:07', '2020-05-20 10:11:07', 'iGTdxdZ1sT4VaLWWJVVE'),
(80, 92, 'Anthony Williams', 'Anthony@williams9.f9.co.uk', 'household', 0, '2020-05-20 12:07:37', '2020-05-20 12:07:37', 'GAdbn8eqORefXavwXPKd'),
(81, 93, 'janet chappell', 'janet_c.c.storage@hotmail.co.uk', 'household', 0, '2020-05-20 12:23:41', '2020-05-20 12:23:41', '1ajGg0rjtFPagcAcB7Pq'),
(82, 93, 'simon coates', 'tworocker4@icloud.com', 'household', 1, '2020-05-20 12:27:13', '2020-05-24 12:09:59', 'LPOyIzIJDNgCtet6SAHU'),
(83, 92, 'Kathryn Williams', 'Kathryn0960@gmail.com', 'household', 0, '2020-05-20 13:04:52', '2020-05-20 13:04:52', 'xOGDajpCjHxKsSwROboF'),
(84, 92, 'Gareth Williams', 'Gjgwilliams2@gmail.com', 'household', 0, '2020-05-20 13:14:53', '2020-05-20 13:14:53', 'TE9PcwRLVVYvvzDnCDCN'),
(85, 97, 'Hilary Eastwood', 'hilary.eastwood@sky.com', 'household', 0, '2020-05-20 13:22:35', '2020-05-20 13:22:35', 'VlgOUyJmzUOtODhm6ewi'),
(86, 97, 'Joe Mutch', 'joseph7.mutch@hotmail.co.uk', 'household', 1, '2020-05-20 13:37:54', '2020-05-20 13:41:03', 'd1CHfGYXHJ2pnqPdVqcq'),
(87, 71, 'Janet Hackett', 'hacketj1161@gmail.com', 'household', 0, '2020-05-20 14:53:41', '2020-05-20 14:53:41', 'iCMmZ6ZcdNUOsojdli0M'),
(88, 71, 'Alice Sutton', 'alice_3105@hotmail.co.uk', 'covid-19', 0, '2020-05-20 14:54:30', '2020-05-20 14:54:30', 'B58dleCaZs92ZbDS9kft'),
(89, 71, 'Emma Peters', 'Emmalogan@hotmail.co.uk', 'covid-19', 0, '2020-05-20 14:54:58', '2020-05-20 14:54:58', 'iVMR6zXRygRVC7I8xUJR'),
(90, 86, 'Peter Norman', 'P.J@hotmail.co.uk', 'household', 0, '2020-05-20 15:11:12', '2020-05-20 15:11:12', 'mxUiDjQkM3tngdOp9Oz7'),
(91, 86, 'Hilary Norman', 'hilary.norman@btinternet.com', 'household', 1, '2020-05-20 15:12:04', '2020-05-20 16:17:55', 'EYZeXlDgDfCymu6SpsKn'),
(92, 100, 'Elaine Criddle', 'elainecriddle@msn.com', 'household', 0, '2020-05-21 09:03:18', '2020-05-21 09:03:18', 'pltIaCyl1Y8EvQ9p5Wv4'),
(93, 100, 'Naomi Stones', 'nstones75@gmail.com', 'household', 0, '2020-05-21 09:03:41', '2020-05-21 09:03:41', 'GOW3dmeJmatNcVMbLkNR'),
(94, 108, 'Bev Armitt', 'bev.armitt@ntlworld.com', 'household', 0, '2020-05-21 10:44:28', '2020-05-21 10:44:28', 'EvSdhRsLqVT7sk9cHAVT'),
(95, 108, 'Jonathan Arcaini', 'salewinesltd@gmail.com', 'household', 0, '2020-05-21 10:44:55', '2020-05-21 10:44:55', 'peMCT12iy99adZ5fnHyp'),
(96, 108, 'Rachel Armitt', 'stripeylass@googlemail.com', 'household', 0, '2020-05-21 10:45:40', '2020-05-21 10:45:40', 'ptcl3DrHhOTdzM2QW3RF'),
(97, 78, 'Judith Perks', 'judeperks@supanet.com', 'household', 1, '2020-05-21 12:08:42', '2020-05-22 08:03:52', '5jRjMYoMqhSuSFktyKK7'),
(98, 78, 'Trixie Clarke', 'trixieclarke@jkhs.org.uk', 'household', 0, '2020-05-21 12:09:39', '2020-05-21 12:09:39', 'haTgVnmFtUqv7chAi3wB'),
(99, 99, 'Julian Austin', 'Julianaustin1966@gmail.com', 'covid-19', 0, '2020-05-21 14:51:02', '2020-05-21 14:51:02', '5aVIPQGtxTb9iz0odFtR'),
(100, 71, 'Alex Mills', 'alextmills@btinternet.com', 'covid-19', 0, '2020-06-01 15:27:01', '2020-06-01 15:27:01', 't25ylprtQgKdOQYC28WZ'),
(101, 71, 'Jo Dobson', 'jo.dobson123@hotmail.co.uk', 'covid-19', 0, '2020-06-01 15:27:28', '2020-06-01 15:27:28', 'dLvSmtIsRmctXtvz91jB'),
(102, 62, 'Martin Wood', 'martin@vicaragecapital.com', 'covid-19', 0, '2020-06-05 14:53:23', '2020-06-05 14:53:23', 'H0lZlCSCUTyjTPzrqQc6'),
(103, 62, 'Greg Hannaford', 'greg.hannaford@gmail.com', 'covid-19', 1, '2020-06-05 15:59:58', '2021-02-16 17:08:42', 'gX0emGinROEVHVgZXFLG'),
(104, 62, 'Greg Hannaford', 'greg.hannaford@gmail.com', 'covid-19', 0, '2020-06-05 15:59:58', '2020-06-05 15:59:58', 'Jo3CysISjoEDfnjMPnAL'),
(105, 124, 'Luke brown', 'lukebrown1992@icloud.com', 'covid-19', 0, '2020-06-24 10:23:50', '2020-06-24 10:23:50', 'RxRFohTUoZNGY0oOKPoT'),
(106, 124, 'Kirsty Brown', 'kirstybrown07@hotmail.com', 'household', 0, '2020-06-24 10:24:54', '2020-06-24 10:24:54', 'bPe4i5MK9PsdRsHAsLvb'),
(107, 127, 'joe', 'jetherington2009@hotmail.co.uk', 'covid-19', 0, '2020-07-07 14:09:55', '2020-07-07 14:09:55', 'Z5htBhzILIXyF6mem692'),
(108, 127, 'Chris Maloney', 'me@chrismaloney.co.uk', 'covid-19', 0, '2020-07-10 15:05:42', '2020-07-10 15:05:42', 'kLRErSpQQJyZ3LDrurTn'),
(109, 135, 'Paul J Newbegin', 'paulnewbegin@yahoo.com', 'covid-19', 0, '2020-07-29 09:47:24', '2020-07-29 09:47:24', 'UXRmvvpHtG41Tfrx4gOn'),
(110, 134, 'Sam', 'bwinnyzmate@gmail.com', 'household', 1, '2020-08-26 12:00:33', '2020-08-26 12:01:34', 'kgomN03GTLT9W1SVcijR'),
(111, 141, 'sam', 'sam.mawhinney93@gmail.com', 'household', 1, '2020-10-08 11:18:59', '2020-10-08 11:25:15', 'v9VRtmDJNnoVPXjhPbdX'),
(112, 147, 'Sam', 'bwinnyzmate@gmail.com', 'household', 1, '2020-10-14 13:51:25', '2020-10-14 13:53:02', 'Bzvg5dloMIMtNJpwqtk4'),
(113, 148, 'sam', 'sam.mawhinney93@gmail.com', 'covid-19', 0, '2020-10-15 12:15:51', '2020-10-15 12:15:51', 'IcW2ycLMp9k5FdxtY4MQ'),
(114, 148, 'sam', 'sam.mawhinney93@gmail.com', 'gifted', 0, '2020-10-18 12:45:01', '2020-10-18 12:45:01', 'GGmjTdQHEIynidG861Vn'),
(115, 148, 'sam', 'sam.mawhinney93@gmail.com', 'gifted', 1, '2020-10-19 10:40:37', '2020-10-19 10:41:33', 'hGUQj4Y45LhKzdB077fC'),
(116, 154, 'sam', 'sam.mawhinney@lsgi.co.uk', 'trusted', 0, '2020-11-02 11:40:39', '2020-11-02 11:40:39', 'LeI6Yid1ozdDtJKui6Mn'),
(118, 154, 'sam', 'sam.mawhinney93@gmail.com', 'gifted', 1, '2020-11-03 14:53:23', '2020-11-03 14:54:03', 'tgJUYwrInGeTKqFbi25i'),
(122, 164, 'sam', 'sam.mawhinney93@gmail.com', 'trusted', 1, '2020-11-05 15:18:02', '2020-11-05 15:19:19', 'kfiVaAEnJ8YS1kV9nQNl'),
(123, 163, 'jon irvine', 'jonathan@visionsharp.co.uk', 'trusted', 1, '2020-11-05 16:31:05', '2020-11-05 16:32:26', 'mcWKuxOrnuo9eeGDBRKq'),
(124, 176, 'sam', 'sam.mawhinney93@gmail.com', 'gifted', 1, '2020-11-12 16:13:00', '2020-11-12 16:14:32', 'VLvKlo5wtkE06OIOjn3B'),
(125, 167, 'Test', 'test@vios.com', 'household', 0, '2020-11-16 13:14:02', '2020-11-16 13:14:02', 'MGWdPwWJr0afgOR77IbM'),
(126, 187, 'Dominic Test', 'dominic@lsgi.co.uk', 'household', 0, '2020-11-23 13:52:22', '2020-11-23 13:52:22', 'huuQUMpDL1ZVblWKnmEl'),
(127, 176, 'Sam', 'sam.mawhinney93@gmail.com', 'gifted', 1, '2020-12-02 14:53:20', '2020-12-02 14:54:24', '8u2pEFSludXBTrEYMdwy'),
(128, 176, 'Sam', 'sam.mawhinney93@gmail.com', 'household', 1, '2020-12-02 15:26:20', '2020-12-02 15:27:54', '1Li5ZsAYv0nLSPqKDLpX'),
(129, 203, 'Janet Hannaford', 'janet.hannaford66@gmail.com', 'household', 0, '2021-02-16 17:22:58', '2021-02-16 17:22:58', '8CoF0xE6UizdSekG80H6');

-- --------------------------------------------------------

--
-- Table structure for table `letters`
--

CREATE TABLE `letters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `memberships`
--

CREATE TABLE `memberships` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `partner_id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `memberships`
--

INSERT INTO `memberships` (`id`, `user_id`, `partner_id`, `plan_id`, `activated`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, '2020-04-29 11:23:29', '2020-04-29 11:23:29'),
(2, 3, 2, 1, 1, '2020-04-29 12:12:12', '2020-04-29 12:12:12'),
(3, 4, 2, 4, 1, '2020-04-29 12:21:22', '2020-04-29 12:21:22'),
(4, 5, 2, 1, 1, '2020-04-30 12:03:52', '2020-04-30 12:03:52'),
(5, 6, 2, 4, 1, '2020-04-30 12:11:05', '2020-04-30 12:11:05'),
(6, 7, 2, 2, 1, '2020-04-30 12:14:03', '2020-04-30 12:14:03'),
(7, 8, 2, 4, 1, '2020-04-30 12:17:34', '2020-04-30 12:17:34'),
(8, 9, 2, 2, 1, '2020-04-30 12:26:09', '2020-04-30 12:26:09'),
(9, 10, 2, 2, 1, '2020-04-30 13:06:40', '2020-04-30 13:06:40'),
(10, 13, 2, 1, 1, '2020-05-01 14:30:13', '2020-05-01 14:30:13'),
(11, 14, 2, 2, 1, '2020-05-06 08:50:02', '2020-05-06 08:50:02'),
(12, 16, 4, 1, 1, '2020-05-07 11:26:35', '2020-05-07 11:26:35'),
(13, 17, 4, 1, 1, '2020-05-07 11:50:37', '2020-05-07 11:50:37'),
(14, 18, 4, 1, 1, '2020-05-07 11:55:11', '2020-05-07 11:55:11'),
(15, 19, 4, 1, 1, '2020-05-07 12:01:55', '2020-05-07 12:01:55'),
(16, 20, 4, 1, 1, '2020-05-07 16:21:30', '2020-05-07 16:21:30'),
(17, 21, 4, 1, 1, '2020-05-07 16:26:15', '2020-05-07 16:26:15'),
(18, 22, 4, 1, 1, '2020-05-11 09:18:07', '2020-05-11 09:18:07'),
(19, 23, 2, 1, 1, '2020-05-11 15:37:10', '2020-05-11 15:37:10'),
(20, 24, 4, 1, 1, '2020-05-11 16:26:33', '2020-05-11 16:26:33'),
(21, 25, 4, 1, 1, '2020-05-11 16:44:28', '2020-05-11 16:44:28'),
(22, 26, 2, 1, 1, '2020-05-11 16:48:19', '2020-05-11 16:48:19'),
(23, 27, 2, 1, 1, '2020-05-12 15:10:46', '2020-05-12 15:10:46'),
(24, 28, 2, 1, 1, '2020-05-12 15:29:00', '2020-05-12 15:29:00'),
(25, 29, 2, 1, 1, '2020-05-12 15:31:56', '2020-05-12 15:31:56'),
(26, 30, 2, 1, 1, '2020-05-12 15:37:29', '2020-05-12 15:37:29'),
(27, 31, 2, 1, 1, '2020-05-12 15:47:20', '2020-05-12 15:47:20'),
(29, 33, 2, 1, 1, '2020-05-13 07:38:28', '2020-05-13 07:38:28'),
(30, 34, 5, 1, 1, '2020-05-13 08:15:29', '2020-05-13 08:15:29'),
(31, 35, 2, 1, 1, '2020-05-13 15:26:52', '2020-05-13 15:26:52'),
(32, 36, 2, 1, 1, '2020-05-13 15:39:00', '2020-05-13 15:39:00'),
(33, 38, 2, 2, 1, '2020-05-13 15:44:11', '2020-05-13 15:44:11'),
(34, 40, 2, 4, 1, '2020-05-13 15:46:51', '2020-05-13 15:46:51'),
(35, 41, 2, 4, 1, '2020-05-14 07:59:29', '2020-05-14 07:59:29'),
(36, 42, 8, 1, 1, '2020-05-14 14:57:42', '2020-07-15 08:13:31'),
(37, 43, 2, 1, 1, '2020-05-14 15:08:34', '2020-05-14 15:08:34'),
(38, 45, 2, 1, 1, '2020-05-14 15:10:57', '2020-05-14 15:10:57'),
(39, 46, 2, 1, 1, '2020-05-14 15:13:17', '2020-05-14 15:13:17'),
(40, 47, 2, 1, 1, '2020-05-14 15:14:09', '2020-05-14 15:14:09'),
(41, 48, 2, 1, 1, '2020-05-14 15:15:03', '2020-05-14 15:15:03'),
(42, 49, 2, 1, 1, '2020-05-14 15:20:45', '2020-05-14 15:20:45'),
(43, 50, 2, 1, 1, '2020-05-14 15:22:46', '2020-05-14 15:22:46'),
(44, 51, 2, 1, 1, '2020-05-14 15:24:31', '2020-05-14 15:24:31'),
(45, 52, 2, 1, 1, '2020-05-14 15:31:31', '2020-05-14 15:31:31'),
(47, 54, 2, 2, 1, '2020-05-15 08:10:10', '2020-05-15 08:10:10'),
(50, 57, 2, 4, 1, '2020-05-15 11:07:07', '2020-05-15 11:07:07'),
(51, 58, 2, 4, 1, '2020-05-15 15:05:30', '2020-05-15 15:05:30'),
(52, 59, 2, 4, 1, '2020-05-15 15:10:29', '2020-05-15 15:10:29'),
(53, 60, 2, 4, 1, '2020-05-15 15:11:02', '2020-05-15 15:11:02'),
(54, 61, 2, 4, 1, '2020-05-15 15:17:29', '2020-05-15 15:17:29'),
(55, 62, 4, 1, 1, '2020-05-18 11:59:30', '2020-05-18 11:59:30'),
(56, 63, 4, 4, 1, '2020-05-18 13:03:29', '2020-05-18 13:03:29'),
(57, 64, 4, 1, 1, '2020-05-19 08:41:12', '2020-05-19 08:41:12'),
(60, 67, 4, 4, 1, '2020-05-19 11:55:49', '2020-05-19 11:55:50'),
(64, 71, 4, 1, 1, '2020-05-19 13:26:29', '2020-05-19 13:26:29'),
(65, 72, 4, 1, 1, '2020-05-19 13:29:24', '2020-05-19 13:29:24'),
(66, 73, 4, 1, 1, '2020-05-19 13:31:38', '2020-05-19 13:31:38'),
(67, 74, 4, 1, 1, '2020-05-19 13:59:54', '2020-05-19 13:59:54'),
(68, 75, 4, 1, 1, '2020-05-19 14:15:15', '2020-05-19 14:15:15'),
(69, 76, 4, 1, 1, '2020-05-19 16:09:30', '2020-05-19 16:09:30'),
(70, 77, 4, 2, 1, '2020-05-20 08:04:03', '2020-05-20 08:04:03'),
(71, 78, 4, 1, 1, '2020-05-20 08:12:10', '2020-05-20 08:12:10'),
(72, 79, 4, 1, 1, '2020-05-20 08:59:03', '2020-05-20 08:59:03'),
(73, 80, 4, 4, 1, '2020-05-20 09:05:56', '2020-05-20 09:05:56'),
(74, 81, 4, 1, 1, '2020-05-20 09:25:30', '2020-05-20 09:25:30'),
(75, 82, 4, 1, 1, '2020-05-20 09:28:59', '2020-05-20 09:28:59'),
(76, 83, 4, 1, 1, '2020-05-20 09:33:11', '2020-05-20 09:33:11'),
(77, 84, 4, 1, 1, '2020-05-20 09:34:33', '2020-05-20 09:34:33'),
(78, 85, 4, 1, 1, '2020-05-20 10:04:30', '2020-05-20 10:04:30'),
(79, 86, 4, 1, 1, '2020-05-20 10:26:02', '2020-05-20 10:26:02'),
(80, 87, 4, 1, 1, '2020-05-20 10:40:10', '2020-05-20 10:40:10'),
(81, 88, 4, 1, 1, '2020-05-20 10:55:50', '2020-05-20 10:55:50'),
(82, 89, 4, 1, 1, '2020-05-20 10:57:27', '2020-05-20 10:57:27'),
(83, 90, 4, 1, 1, '2020-05-20 11:19:28', '2020-05-20 11:19:28'),
(84, 91, 4, 1, 1, '2020-05-20 11:49:42', '2020-05-20 11:49:42'),
(85, 92, 4, 1, 1, '2020-05-20 12:00:33', '2020-05-20 12:00:33'),
(86, 93, 4, 1, 1, '2020-05-20 12:21:05', '2020-05-20 12:21:05'),
(87, 94, 4, 1, 1, '2020-05-20 12:32:01', '2020-05-20 12:32:01'),
(88, 95, 4, 1, 1, '2020-05-20 12:40:38', '2020-05-20 12:40:38'),
(89, 96, 4, 1, 1, '2020-05-20 13:08:57', '2020-05-20 13:08:57'),
(90, 97, 4, 1, 1, '2020-05-20 13:21:45', '2020-05-20 13:21:45'),
(91, 98, 4, 4, 1, '2020-05-20 13:41:03', '2020-05-20 13:41:03'),
(92, 99, 4, 1, 1, '2020-05-20 13:55:26', '2020-05-20 13:55:26'),
(93, 100, 4, 1, 1, '2020-05-20 14:12:45', '2020-05-20 14:12:45'),
(95, 102, 4, 4, 1, '2020-05-20 16:17:55', '2020-05-20 16:17:55'),
(96, 103, 4, 1, 1, '2020-05-21 06:38:16', '2020-05-21 06:38:16'),
(97, 104, 4, 1, 1, '2020-05-21 07:07:44', '2020-05-21 07:07:44'),
(98, 105, 4, 1, 1, '2020-05-21 07:25:25', '2020-05-21 07:25:25'),
(99, 106, 4, 1, 1, '2020-05-21 07:30:48', '2020-05-21 07:30:48'),
(101, 108, 4, 1, 1, '2020-05-21 10:42:45', '2020-05-21 10:42:45'),
(102, 109, 4, 1, 1, '2020-05-21 14:40:46', '2020-05-21 14:40:46'),
(103, 110, 4, 4, 1, '2020-05-22 08:03:52', '2020-05-22 08:03:52'),
(104, 111, 4, 1, 1, '2020-05-22 10:30:45', '2020-05-22 10:30:45'),
(105, 112, 6, 1, 1, '2020-05-22 12:49:10', '2020-05-22 12:49:10'),
(106, 113, 4, 4, 1, '2020-05-24 12:09:59', '2020-05-24 12:09:59'),
(107, 114, 4, 1, 1, '2020-05-27 05:49:23', '2020-05-27 05:49:23'),
(108, 115, 4, 1, 1, '2020-06-08 11:17:36', '2020-06-08 11:17:36'),
(109, 116, 4, 1, 1, '2020-06-08 15:04:35', '2020-06-08 15:04:35'),
(110, 118, 4, 1, 1, '2020-06-11 09:53:45', '2020-06-11 09:53:45'),
(111, 122, 4, 1, 1, '2020-06-23 13:58:01', '2020-06-23 13:58:01'),
(112, 123, 4, 1, 1, '2020-06-23 20:45:05', '2020-06-23 20:45:05'),
(113, 124, 4, 1, 1, '2020-06-24 10:21:28', '2020-06-24 10:21:28'),
(114, 125, 4, 1, 1, '2020-06-26 13:03:05', '2020-06-26 13:03:05'),
(115, 126, 4, 1, 1, '2020-06-30 11:28:17', '2020-06-30 11:28:17'),
(116, 127, 4, 1, 1, '2020-07-07 14:08:36', '2020-07-07 14:08:36'),
(117, 128, 7, 1, 1, '2020-07-08 08:42:17', '2020-07-08 08:42:17'),
(118, 129, 7, 1, 1, '2020-07-08 08:51:14', '2020-07-08 08:51:14'),
(119, 130, 4, 1, 1, '2020-07-10 08:44:11', '2020-07-10 08:44:11'),
(120, 131, 4, 1, 1, '2020-07-14 10:41:27', '2020-07-14 10:41:27'),
(121, 132, 7, 1, 1, '2020-07-17 11:17:57', '2020-07-17 11:17:57'),
(122, 133, 7, 1, 1, '2020-07-21 12:14:53', '2020-07-21 12:14:53'),
(123, 134, 4, 1, 1, '2020-07-22 08:15:34', '2020-07-22 08:15:34'),
(124, 135, 4, 1, 1, '2020-07-28 16:00:08', '2020-07-28 16:00:08'),
(125, 136, 4, 1, 1, '2020-07-29 07:08:15', '2020-07-29 07:08:15'),
(126, 137, 4, 1, 1, '2020-08-07 13:38:16', '2020-08-07 13:38:16'),
(127, 140, 4, 4, 1, '2020-08-26 12:01:34', '2020-08-26 12:01:34'),
(128, 138, 9, 1, 1, '2020-09-16 09:31:38', '2020-09-28 19:18:00'),
(129, 139, 5, 1, 1, '2020-09-16 09:36:22', '2020-09-16 09:36:22'),
(130, 138, 9, 1, 1, '2020-09-28 19:16:09', '2020-09-28 19:16:09'),
(131, 139, 5, 1, 1, '2020-10-08 09:26:33', '2020-10-08 09:26:33'),
(132, 140, 5, 1, 1, '2020-10-08 09:35:08', '2020-10-08 09:35:08'),
(133, 141, 5, 1, 1, '2020-10-08 09:36:18', '2020-10-08 09:36:18'),
(134, 142, 5, 1, 1, '2020-10-08 09:57:09', '2020-10-08 09:57:09'),
(135, 143, 5, 1, 1, '2020-10-08 10:14:00', '2020-10-08 10:14:00'),
(136, 144, 5, 1, 1, '2020-10-08 11:11:08', '2020-10-08 11:11:08'),
(137, 145, 5, 1, 1, '2020-10-08 11:25:15', '2020-10-08 11:25:35'),
(138, 146, 5, 1, 1, '2020-10-08 12:07:03', '2020-10-08 12:07:03'),
(140, 148, 4, 1, 1, '2020-10-14 13:53:02', '2020-10-15 08:40:47'),
(141, 149, 5, 1, 1, '2020-10-16 08:16:26', '2020-10-16 08:16:26'),
(142, 150, 5, 1, 1, '2020-10-16 11:27:51', '2020-10-16 11:27:51'),
(143, 151, 4, 2, 1, '2020-10-19 10:41:33', '2020-10-19 10:41:33'),
(144, 152, 5, 1, 1, '2020-10-29 09:31:33', '2020-10-29 09:31:33'),
(146, 154, 5, 1, 1, '2020-10-30 16:06:15', '2020-10-30 16:06:15'),
(148, 156, 5, 1, 1, '2020-11-03 14:54:03', '2020-11-03 14:54:03'),
(149, 157, 5, 1, 1, '2020-11-05 11:20:08', '2020-11-05 11:20:08'),
(150, 158, 5, 1, 1, '2020-11-05 11:32:15', '2020-11-05 11:32:15'),
(152, 160, 5, 1, 1, '2020-11-05 13:02:48', '2020-11-05 13:02:48'),
(153, 161, 4, 1, 1, '2020-11-05 14:02:36', '2020-11-05 14:02:36'),
(154, 162, 5, 1, 1, '2020-11-05 14:41:50', '2020-11-05 14:41:50'),
(155, 163, 5, 1, 1, '2020-11-05 14:57:28', '2020-11-05 14:57:28'),
(156, 164, 5, 1, 1, '2020-11-05 15:08:49', '2020-11-05 15:08:49'),
(157, 165, 5, 5, 1, '2020-11-05 15:19:19', '2020-11-05 15:19:19'),
(158, 166, 10, 1, 1, '2020-11-05 16:51:47', '2020-11-05 16:51:47'),
(159, 167, 5, 1, 1, '2020-11-06 15:56:11', '2020-11-06 15:56:11'),
(163, 171, 5, 1, 1, '2020-11-10 11:55:43', '2020-11-10 11:55:43'),
(164, 172, 4, 1, 1, '2020-11-10 13:47:44', '2020-11-10 13:47:44'),
(167, 175, 11, 1, 1, '2020-11-11 16:52:23', '2021-01-15 13:26:40'),
(169, 177, 5, 2, 1, '2020-11-12 16:14:32', '2020-11-12 16:14:32'),
(172, 183, 5, 1, 1, '2020-11-20 15:44:24', '2020-11-20 15:44:24'),
(173, 184, 7, 1, 1, '2020-11-23 09:57:16', '2020-11-23 09:57:16'),
(176, 187, 4, 1, 1, '2020-11-23 13:42:52', '2021-02-22 12:24:39'),
(178, 189, 9, 1, 1, '2020-12-17 10:30:35', '2020-12-17 10:30:35'),
(180, 191, 11, 1, 1, '2020-12-21 11:36:48', '2021-01-15 13:26:56'),
(181, 192, 5, 1, 1, '2021-01-07 10:01:18', '2021-01-07 10:02:30'),
(184, 195, 11, 1, 1, '2021-01-12 12:47:38', '2021-01-12 12:47:38'),
(185, 196, 11, 1, 1, '2021-01-12 13:08:47', '2021-01-12 13:08:47'),
(186, 197, 11, 1, 1, '2021-01-12 13:10:01', '2021-01-12 13:10:01'),
(188, 199, 5, 1, 1, '2021-01-18 12:17:16', '2021-01-18 12:17:16'),
(190, 201, 11, 1, 1, '2021-01-20 12:44:57', '2021-01-20 12:44:57'),
(191, 202, 12, 1, 1, '2021-02-09 10:29:02', '2021-02-09 10:29:02'),
(192, 203, 4, 3, 1, '2021-02-16 17:08:42', '2021-02-16 17:08:42'),
(193, 204, 9, 1, 1, '2021-02-17 14:37:01', '2021-02-17 14:37:01');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(304, '2014_10_12_000000_create_users_table', 1),
(305, '2014_10_12_100000_create_password_resets_table', 1),
(306, '2019_04_17_102025_create_pages_table', 1),
(307, '2019_04_17_163857_create_roles_table', 1),
(308, '2019_04_18_215017_create_blocks_table', 1),
(309, '2019_04_23_183410_create_partners_table', 1),
(310, '2019_04_26_075608_create_benefits_table', 1),
(311, '2019_04_26_075608_create_services_table', 1),
(312, '2019_04_28_113426_create_benefit_partner_table', 1),
(313, '2019_04_28_113426_create_service_partner_table', 1),
(314, '2019_04_28_131529_create_posts_table', 1),
(315, '2019_05_01_175932_create_contacts_table', 1),
(316, '2019_05_02_081542_create_partner_posts_table', 1),
(317, '2019_08_19_000000_create_failed_jobs_table', 1),
(318, '2019_08_19_115748_create_plans_table', 1),
(319, '2019_08_19_124005_create_memberships_table', 1),
(320, '2019_08_22_164534_create_invites_table', 1),
(321, '2019_08_25_200149_create_payments_table', 1),
(322, '2019_08_26_190353_create_benefit_user_table', 1),
(323, '2019_09_12_150818_create_providers_table', 1),
(324, '2019_09_18_082035_create_settings_table', 1),
(325, '2019_09_18_093834_create_requests_table', 1),
(326, '2019_09_23_133233_create_subscriptions_table', 1),
(327, '2019_09_24_141653_create_debit_payments_table', 1),
(328, '2019_10_04_155947_create_permissions_table', 1),
(329, '2019_10_08_142646_create_changelogs_table', 1),
(330, '2019_10_11_084548_create_sms_verifications_table', 1),
(331, '2019_10_15_110820_create_invitations_table', 1),
(332, '2019_10_15_141744_create_letters_table', 1),
(333, '2019_11_08_103651_create_sessions_table', 1),
(334, '2019_11_25_111734_create_trusted_users_table', 1),
(335, '2020_04_29_101316_create_reasons_table', 1),
(336, '2020_08_25_151201_add_transaction_id_to_payments_table', 2),
(337, '2020_08_26_073450_remove_stripe_columns_from_payments_table', 2),
(338, '2020_09_04_073432_remove_go_cardless_fields_from_debit_payments_table', 2),
(339, '2020_09_04_073558_add_transaction_id_to_debit_payments_table', 2),
(340, '2020_09_04_074036_add_schedule_id_to_subscriptions_table', 2),
(341, '2020_09_07_101845_add_provider_and_partner_percentage_fields_to_benefits_table', 2),
(342, '2020_09_11_091508_add_benefit_id_to_debit_payments_table', 2),
(343, '2020_09_16_085831_remove_go_cardless_fields_from_subscriptions_table', 2),
(344, '2020_09_16_153517_add_subscription_id_to_users_table', 2),
(345, '2020_09_23_082946_add_nullable_to_partner_and_provider_percentage_fields_in_benefits_table', 3),
(346, '2020_09_23_091513_add_nullable_to_subscription_id_in_users_table', 3),
(347, '2020_10_26_121207_add_provider_email_to_benefits_table', 4),
(348, '2020_10_26_121925_add_benefit_notify_content_field_to_settings_table', 4),
(349, '2020_10_26_155447_add_includes_benefit_form_and_button_fields_to_benefits_table', 4),
(350, '2020_10_27_094705_add_benefit_form_content_field_to_settings_table', 4),
(351, '2020_10_27_134213_create_benefit_contacts_table', 4),
(352, '2020_10_30_165447_change_payload_field_type_in_sessions_table', 5),
(355, '2020_11_06_134507_drop_nochex_id_column_from_partners_table', 6),
(356, '2020_11_06_134545_drop_nochex_id_and_provider_logo_from_providers_table', 6),
(357, '2020_11_06_135004_add_nochex_id_and_provider_logo_to_providers_table', 7),
(358, '2020_11_06_135021_add_nochex_id_column_to_partners_table', 7),
(359, '2020_11_06_175352_add_benefit_name_to_debit_payments_table', 8),
(360, '2020_11_08_102328_add_provider_telephone_to_benefits_table', 9),
(361, '2020_11_08_105946_add_provider_nochex_id_to_benefits_table', 10),
(362, '2020_11_20_165635_drop_address_field_from_invitations_table', 11),
(363, '2020_11_20_165723_add_fields_to_invitations_table', 11),
(364, '2020_11_25_112449_drop_provider_name_and_provider_logo_from_benefits_table', 12),
(365, '2020_12_18_101426_add_telephone_number_and_email_to_providers_table', 13),
(366, '2021_01_07_110649_add_new_user_benefits_content_field_to_settings_table', 13),
(367, '2021_01_11_144405_add_provider_id_to_partners_table', 14),
(368, '2021_01_13_093553_add_cancelled_boosted_email_content_field', 15),
(369, '2021_01_18_104643_add_code_invite_content_field_to_partners_table', 16),
(370, '2021_01_19_113724_add_invite_content_field_to_partners_table', 17),
(371, '2021_02_16_131329_create_connections_table', 18);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `benefit_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'false',
  `template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'default',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `benefit_id`, `name`, `url`, `enabled`, `template`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Home Page', '/', '0', 'default', '2020-04-29 11:23:29', '2020-04-29 11:23:29'),
(2, NULL, 'Login / Register', 'user-login', '0', 'default', '2020-04-29 11:23:29', '2020-04-29 11:23:29'),
(3, NULL, 'Terms & Conditions', 'terms_and_conditions', '0', 'default', '2020-04-29 11:23:29', '2020-04-29 11:23:29'),
(4, NULL, 'Privacy Policy', 'privacy_policy', '0', 'default', '2020-04-29 11:23:29', '2020-04-29 11:23:29');

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `primary_colour` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secondary_colour` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '#000000',
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'logo.png',
  `header_mode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'light',
  `subdomain` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `active` tinyint(1) DEFAULT '1',
  `address1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nochex_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_invite_content` text COLLATE utf8mb4_unicode_ci,
  `invite_content` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `user_id`, `name`, `primary_colour`, `secondary_colour`, `logo`, `header_mode`, `subdomain`, `contact_phone`, `contact_email`, `description`, `active`, `address1`, `address2`, `city`, `postcode`, `created_at`, `updated_at`, `nochex_id`, `provider_id`, `code_invite_content`, `invite_content`) VALUES
(1, 4, 'default', '#21bcbb', '#000000', 'logo.png', 'light', 'default', '0161 697 3096', 'admin@trustbox.com', NULL, 1, '85', 'McGlynn Burg', 'Port Willard', '47862-8346', '2020-04-29 11:23:29', '2020-04-29 11:23:29', NULL, NULL, NULL, NULL),
(2, 4, 'Trust Box', '#21BCBB', '#004354', 'VlB3iWZqO6ktf2CFjbDuWy2q0h4EsadGIvRCpNVS.png', 'light', 'trustbox', '0161 697 3096', 'admin@trustbox.com', NULL, 1, '61', 'Glover Prairie', 'East Annetta', '60725', '2020-04-29 11:23:29', '2021-01-18 12:05:03', '0', NULL, '<div style=\"margin-bottom: 0.28cm; line-height: 108%;\">Hi @name@</div>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">We&rsquo;re writing to you today with some exciting news.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">@partner@ offer their customers an array of additional benefits and services through TrustBox. As a valued customer of @partner@, you&rsquo;re entitled to benefit from the @partner@ TrustBox, a carefully selected collection of benefits and services, designed to lighten the modern life load and help you sleep that little more soundly at night.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Best of all, you&rsquo;re entitled to Lifetime Membership, absolutely&nbsp;<strong>free</strong>, with no strings attached.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>About TrustBox</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">We know life changes quickly and that the legal, financial and health needs of you and your family are changing every day.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">That&rsquo;s why TrustBox has got your back, is in your corner, and on your side. TrustbBox have tested the market far and wide, to find the best providers of the services we all need in life. TrustBox providers are vetted by some of the best legal minds in the country so we know that the providers we choose are the best in the business. We share our favourite providers with you, like we would with our closest friends.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Your TrustBox Lifetime Membership introduces you to a specially selected benefits suite from 24/7 Legal Support, to Legacy Planning, to Conveyancing, giving you access to the best of the best in each field.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>Create your free TrustBox account</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Creating your TrustBox account is really easy. Simply follow the link below and sign up with your email address and password. We&rsquo;ll guide you through the information we need from you.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">You&rsquo;ll then have access to a whole host of benefits for you and your family.</p>\r\n<p>Your family code:&nbsp;<code>@code@</code></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">If you need help, or you have any other questions, feel free to email&nbsp;<span style=\"color: #0563c1;\"><u><a href=\"mailto:help@trustbox.com\">help@trustbox.com</a></u></span>&nbsp;or call our customer service team on 0345 548 1230.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Thanks,</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">The TrustBox team</p>', NULL),
(4, 187, 'LSGi', '#2AB6B9', '#004354', 'CPdetYiR6YR5IhjGRX1M8QXQtUBNSmtPnk28pPcT.png', 'light', 'lsgi', '0345 548 1230', 'claire@lsgi.co.uk', NULL, 1, '5 Millbank House', 'Riverside Business Park', 'Wilmsow', 'SK9 1BJ', '2020-05-07 09:42:26', '2021-02-22 13:40:38', '0', '18', '<div style=\"margin-bottom: 0.28cm; line-height: 108%;\">Hi @name@</div>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">We&rsquo;re writing to you today with some exciting news.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">@partner@ offer their customers an array of additional benefits and services through TrustBox. As a valued customer of @partner@, you&rsquo;re entitled to benefit from the @partner@ TrustBox, a carefully selected collection of benefits and services, designed to lighten the modern life load and help you sleep that little more soundly at night.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Best of all, you&rsquo;re entitled to Lifetime Membership, absolutely&nbsp;<strong>free</strong>, with no strings attached.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>About TrustBox</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">We know life changes quickly and that the legal, financial and health needs of you and your family are changing every day.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">That&rsquo;s why TrustBox has got your back, is in your corner, and on your side. TrustbBox have tested the market far and wide, to find the best providers of the services we all need in life. TrustBox providers are vetted by some of the best legal minds in the country so we know that the providers we choose are the best in the business. We share our favourite providers with you, like we would with our closest friends.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Your TrustBox Lifetime Membership introduces you to a specially selected benefits suite from 24/7 Legal Support, to Legacy Planning, to Conveyancing, giving you access to the best of the best in each field.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>Create your free TrustBox account</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Creating your TrustBox account is really easy. Simply follow the link below and sign up with your email address and password. We&rsquo;ll guide you through the information we need from you.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">You&rsquo;ll then have access to a whole host of benefits for you and your family.</p>\r\n<p>Your family code:&nbsp;<code>@code@</code></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">If you need help, or you have any other questions, feel free to email&nbsp;<span style=\"color: #0563c1;\"><u><a href=\"mailto:help@trustbox.com\">help@trustbox.com</a></u></span>&nbsp;or call our customer service team on 0345 548 1230.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Thanks,</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">The TrustBox team</p>', NULL),
(5, 198, 'Open Money', '#32325A', '#F55117', 'qdNgXXj0mzk3DhYrgVaMX5ymgTnvys2henhLnFRX.png', 'light', 'openmoney', '0161 204 3200', 'hello@open-money.co.uk', '<p>This is the open money partner description</p>', 1, '1 St. Peter’s Square', NULL, 'Manchester', 'M2 3DE', '2020-05-20 14:53:16', '2021-01-18 12:03:36', '0', '3', '<div style=\"margin-bottom: 0.28cm; line-height: 108%;\">Hi @name@</div>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">We&rsquo;re writing to you today with some exciting news. HEY</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">@partner@ offer their customers an array of additional benefits and services through TrustBox. As a valued customer of @partner@, you&rsquo;re entitled to benefit from the @partner@ TrustBox, a carefully selected collection of benefits and services, designed to lighten the modern life load and help you sleep that little more soundly at night.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Best of all, you&rsquo;re entitled to Lifetime Membership, absolutely&nbsp;<strong>free</strong>, with no strings attached.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>About TrustBox</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">We know life changes quickly and that the legal, financial and health needs of you and your family are changing every day.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">That&rsquo;s why TrustBox has got your back, is in your corner, and on your side. TrustbBox have tested the market far and wide, to find the best providers of the services we all need in life. TrustBox providers are vetted by some of the best legal minds in the country so we know that the providers we choose are the best in the business. We share our favourite providers with you, like we would with our closest friends.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Your TrustBox Lifetime Membership introduces you to a specially selected benefits suite from 24/7 Legal Support, to Legacy Planning, to Conveyancing, giving you access to the best of the best in each field.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>Create your free TrustBox account</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Creating your TrustBox account is really easy. Simply follow the link below and sign up with your email address and password. We&rsquo;ll guide you through the information we need from you.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">You&rsquo;ll then have access to a whole host of benefits for you and your family.</p>\r\n<p>Your family code:&nbsp;<code>@code@</code></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">If you need help, or you have any other questions, feel free to email&nbsp;<span style=\"color: #0563c1;\"><u><a href=\"mailto:help@trustbox.com\">help@trustbox.com</a></u></span>&nbsp;or call our customer service team on 0345 548 1230.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Thanks,</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">The TrustBox team</p>', NULL),
(6, 117, 'Signature Saver 123', '#D1087D', '#0A0A0A', 'e2BqVlu59LLf2GyjtFcrkzSDosoZLE2d0ViflIr7.jpeg', 'light', 'signaturesaver', '0330 124 3857', 'hello@signaturebenefits.co.uk', NULL, 1, '3 Azure Court', 'Doxford International Business Park', 'Sunderland', 'SR3 3BE', '2020-05-21 13:40:09', '2021-01-18 12:05:19', '0', '5', '<div style=\"margin-bottom: 0.28cm; line-height: 108%;\">Hi @name@</div>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">We&rsquo;re writing to you today with some exciting news.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">@partner@ offer their customers an array of additional benefits and services through TrustBox. As a valued customer of @partner@, you&rsquo;re entitled to benefit from the @partner@ TrustBox, a carefully selected collection of benefits and services, designed to lighten the modern life load and help you sleep that little more soundly at night.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Best of all, you&rsquo;re entitled to Lifetime Membership, absolutely&nbsp;<strong>free</strong>, with no strings attached.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>About TrustBox</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">We know life changes quickly and that the legal, financial and health needs of you and your family are changing every day.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">That&rsquo;s why TrustBox has got your back, is in your corner, and on your side. TrustbBox have tested the market far and wide, to find the best providers of the services we all need in life. TrustBox providers are vetted by some of the best legal minds in the country so we know that the providers we choose are the best in the business. We share our favourite providers with you, like we would with our closest friends.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Your TrustBox Lifetime Membership introduces you to a specially selected benefits suite from 24/7 Legal Support, to Legacy Planning, to Conveyancing, giving you access to the best of the best in each field.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>Create your free TrustBox account</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Creating your TrustBox account is really easy. Simply follow the link below and sign up with your email address and password. We&rsquo;ll guide you through the information we need from you.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">You&rsquo;ll then have access to a whole host of benefits for you and your family.</p>\r\n<p>Your family code:&nbsp;<code>@code@</code></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">If you need help, or you have any other questions, feel free to email&nbsp;<span style=\"color: #0563c1;\"><u><a href=\"mailto:help@trustbox.com\">help@trustbox.com</a></u></span>&nbsp;or call our customer service team on 0345 548 1230.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Thanks,</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">The TrustBox team</p>', NULL),
(7, 74, 'Balfort Legal', '#1DA1F2', '#024488', 'cyXv0o8HVUomhaCFRziHHO3hSSQ7pzSCEf6peu6I.png', 'light', 'balfortlegal', '020 3282 6670', 'enquiries@balfortlegal.com', '<p>Our team has a wealth of knowledge and experience in Estate and Legacy Planning. We know how important it is to you that your loved ones receive the maximum benefit from your assets after you are no longer with them.</p>', 1, '27 Longbridge Road', 'Barking', 'London', 'IG11 8TN', '2020-06-16 09:30:09', '2021-01-18 12:05:27', '0', NULL, '<div style=\"margin-bottom: 0.28cm; line-height: 108%;\">Hi @name@</div>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">We&rsquo;re writing to you today with some exciting news.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">@partner@ offer their customers an array of additional benefits and services through TrustBox. As a valued customer of @partner@, you&rsquo;re entitled to benefit from the @partner@ TrustBox, a carefully selected collection of benefits and services, designed to lighten the modern life load and help you sleep that little more soundly at night.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Best of all, you&rsquo;re entitled to Lifetime Membership, absolutely&nbsp;<strong>free</strong>, with no strings attached.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>About TrustBox</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">We know life changes quickly and that the legal, financial and health needs of you and your family are changing every day.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">That&rsquo;s why TrustBox has got your back, is in your corner, and on your side. TrustbBox have tested the market far and wide, to find the best providers of the services we all need in life. TrustBox providers are vetted by some of the best legal minds in the country so we know that the providers we choose are the best in the business. We share our favourite providers with you, like we would with our closest friends.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Your TrustBox Lifetime Membership introduces you to a specially selected benefits suite from 24/7 Legal Support, to Legacy Planning, to Conveyancing, giving you access to the best of the best in each field.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>Create your free TrustBox account</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Creating your TrustBox account is really easy. Simply follow the link below and sign up with your email address and password. We&rsquo;ll guide you through the information we need from you.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">You&rsquo;ll then have access to a whole host of benefits for you and your family.</p>\r\n<p>Your family code:&nbsp;<code>@code@</code></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">If you need help, or you have any other questions, feel free to email&nbsp;<span style=\"color: #0563c1;\"><u><a href=\"mailto:help@trustbox.com\">help@trustbox.com</a></u></span>&nbsp;or call our customer service team on 0345 548 1230.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Thanks,</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">The TrustBox team</p>', NULL),
(8, 31, 'visionsharp', '#DB5C3A', '#000000', 'ff48Xn2TtYWHeeoZa94jCIPuqNU3iOrfLrQGFT5y.png', 'light', 'visionsharp', '01616973096', 'info@visionsharp.co.uk', '<p>visionsharp</p>', 1, 'The stables, Paradise Wharf, Ducie Street', 'the stables, paradise wharf, ducie street', 'Manchester', 'm12jn', '2020-07-15 08:09:37', '2021-01-18 12:05:35', '0', NULL, '<div style=\"margin-bottom: 0.28cm; line-height: 108%;\">Hi @name@</div>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">We&rsquo;re writing to you today with some exciting news.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">@partner@ offer their customers an array of additional benefits and services through TrustBox. As a valued customer of @partner@, you&rsquo;re entitled to benefit from the @partner@ TrustBox, a carefully selected collection of benefits and services, designed to lighten the modern life load and help you sleep that little more soundly at night.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Best of all, you&rsquo;re entitled to Lifetime Membership, absolutely&nbsp;<strong>free</strong>, with no strings attached.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>About TrustBox</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">We know life changes quickly and that the legal, financial and health needs of you and your family are changing every day.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">That&rsquo;s why TrustBox has got your back, is in your corner, and on your side. TrustbBox have tested the market far and wide, to find the best providers of the services we all need in life. TrustBox providers are vetted by some of the best legal minds in the country so we know that the providers we choose are the best in the business. We share our favourite providers with you, like we would with our closest friends.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Your TrustBox Lifetime Membership introduces you to a specially selected benefits suite from 24/7 Legal Support, to Legacy Planning, to Conveyancing, giving you access to the best of the best in each field.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>Create your free TrustBox account</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Creating your TrustBox account is really easy. Simply follow the link below and sign up with your email address and password. We&rsquo;ll guide you through the information we need from you.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">You&rsquo;ll then have access to a whole host of benefits for you and your family.</p>\r\n<p>Your family code:&nbsp;<code>@code@</code></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">If you need help, or you have any other questions, feel free to email&nbsp;<span style=\"color: #0563c1;\"><u><a href=\"mailto:help@trustbox.com\">help@trustbox.com</a></u></span>&nbsp;or call our customer service team on 0345 548 1230.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Thanks,</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">The TrustBox team</p>', NULL),
(9, 189, 'Redstone Wills', '#AB5558', '#000000', 'iFFPc6VpoTtNO2S6EXxWnooIq2ndwK9Aif4M5xZ6.png', 'light', 'redstone', '0808 281 1545', 'enquiries@redstonewills.co.uk', NULL, 1, 'Windmill Road', 'St Leonards on Sea', 'Hastings', 'TN38 9BY', '2020-09-28 13:03:53', '2021-01-18 12:05:58', '0', '4', '<p>&nbsp;<span style=\"font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Open Sans\', \'Helvetica Neue\', sans-serif;\">Hi @name@</span></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">We&rsquo;re writing to you today with some exciting news.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">@partner@ offer their customers an array of additional benefits and services through TrustBox. As a valued customer of @partner@, you&rsquo;re entitled to benefit from the @partner@ TrustBox, a carefully selected collection of benefits and services, designed to lighten the modern life load and help you sleep that little more soundly at night.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Best of all, you&rsquo;re entitled to Lifetime Membership, absolutely&nbsp;<strong>free</strong>, with no strings attached.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>About TrustBox</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">We know life changes quickly and that the legal, financial and health needs of you and your family are changing every day.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">That&rsquo;s why TrustBox has got your back, is in your corner, and on your side. TrustbBox have tested the market far and wide, to find the best providers of the services we all need in life. TrustBox providers are vetted by some of the best legal minds in the country so we know that the providers we choose are the best in the business. We share our favourite providers with you, like we would with our closest friends.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Your TrustBox Lifetime Membership introduces you to a specially selected benefits suite from 24/7 Legal Support, to Legacy Planning, to Conveyancing, giving you access to the best of the best in each field.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>Create your free TrustBox account</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Creating your TrustBox account is really easy. Simply follow the link below and sign up with your email address and password. We&rsquo;ll guide you through the information we need from you.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">You&rsquo;ll then have access to a whole host of benefits for you and your family.</p>\r\n<p>Your family code:&nbsp;<code>@code@</code></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">If you need help, or you have any other questions, feel free to email&nbsp;<span style=\"color: #0563c1;\"><u><a href=\"mailto:help@trustbox.com\">help@trustbox.com</a></u></span>&nbsp;or call our customer service team on 0345 548 1230.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Thanks,</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">The TrustBox team</p>', NULL),
(10, 166, 'Legal Services Guild', '#BC3737', '#A8A8A8', 'l2LhCdCk4pRWlD7MAS8ffBeSw3yNGFUVbh7qMa5f.png', 'light', 'lsg', '0345 548 1230', 'info@legalservicesguild.co.uk', NULL, 1, '5 Millbank House', 'Riverside Business Park', 'Wilmslow', 'SK9 1BJ', '2020-11-05 16:23:17', '2021-01-18 12:06:17', '0', NULL, '<div style=\"margin-bottom: 0.28cm; line-height: 108%;\">Hi @name@</div>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">We&rsquo;re writing to you today with some exciting news.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">@partner@ offer their customers an array of additional benefits and services through TrustBox. As a valued customer of @partner@, you&rsquo;re entitled to benefit from the @partner@ TrustBox, a carefully selected collection of benefits and services, designed to lighten the modern life load and help you sleep that little more soundly at night.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Best of all, you&rsquo;re entitled to Lifetime Membership, absolutely&nbsp;<strong>free</strong>, with no strings attached.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>About TrustBox</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">We know life changes quickly and that the legal, financial and health needs of you and your family are changing every day.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">That&rsquo;s why TrustBox has got your back, is in your corner, and on your side. TrustbBox have tested the market far and wide, to find the best providers of the services we all need in life. TrustBox providers are vetted by some of the best legal minds in the country so we know that the providers we choose are the best in the business. We share our favourite providers with you, like we would with our closest friends.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Your TrustBox Lifetime Membership introduces you to a specially selected benefits suite from 24/7 Legal Support, to Legacy Planning, to Conveyancing, giving you access to the best of the best in each field.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>Create your free TrustBox account</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Creating your TrustBox account is really easy. Simply follow the link below and sign up with your email address and password. We&rsquo;ll guide you through the information we need from you.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">You&rsquo;ll then have access to a whole host of benefits for you and your family.</p>\r\n<p>Your family code:&nbsp;<code>@code@</code></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">If you need help, or you have any other questions, feel free to email&nbsp;<span style=\"color: #0563c1;\"><u><a href=\"mailto:help@trustbox.com\">help@trustbox.com</a></u></span>&nbsp;or call our customer service team on 0345 548 1230.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Thanks,</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">The TrustBox team</p>', NULL),
(11, NULL, 'Remarkable Practice', '#408C52', '#000000', 'D8Aoxqf0SdUNoKkeAwSBev8XxsRHlvQ4RWpT9Tqc.jpeg', 'light', 'remarkablepractice', '01773 821689', 'info@remarkablepractice.co.uk', NULL, 1, '15', 'Riverside Business Centre', 'Belper', 'DE56 0RN', '2021-01-11 13:47:24', '2021-01-18 12:06:25', NULL, NULL, '<div style=\"margin-bottom: 0.28cm; line-height: 108%;\">Hi @name@</div>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">We&rsquo;re writing to you today with some exciting news.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">@partner@ offer their customers an array of additional benefits and services through TrustBox. As a valued customer of @partner@, you&rsquo;re entitled to benefit from the @partner@ TrustBox, a carefully selected collection of benefits and services, designed to lighten the modern life load and help you sleep that little more soundly at night.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Best of all, you&rsquo;re entitled to Lifetime Membership, absolutely&nbsp;<strong>free</strong>, with no strings attached.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>About TrustBox</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">We know life changes quickly and that the legal, financial and health needs of you and your family are changing every day.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">That&rsquo;s why TrustBox has got your back, is in your corner, and on your side. TrustbBox have tested the market far and wide, to find the best providers of the services we all need in life. TrustBox providers are vetted by some of the best legal minds in the country so we know that the providers we choose are the best in the business. We share our favourite providers with you, like we would with our closest friends.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Your TrustBox Lifetime Membership introduces you to a specially selected benefits suite from 24/7 Legal Support, to Legacy Planning, to Conveyancing, giving you access to the best of the best in each field.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>Create your free TrustBox account</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Creating your TrustBox account is really easy. Simply follow the link below and sign up with your email address and password. We&rsquo;ll guide you through the information we need from you.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">You&rsquo;ll then have access to a whole host of benefits for you and your family.</p>\r\n<p>Your family code:&nbsp;<code>@code@</code></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">If you need help, or you have any other questions, feel free to email&nbsp;<span style=\"color: #0563c1;\"><u><a href=\"mailto:help@trustbox.com\">help@trustbox.com</a></u></span>&nbsp;or call our customer service team on 0345 548 1230.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Thanks,</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">The TrustBox team</p>', NULL),
(12, NULL, 'Zen Wills', '#2ED2E8', '#FFC000', 'oRhR0e90wkbIYuzBti9CfuwROXmTlqywAN7PjVHi.png', 'light', 'zenwills', '0330 390 0535', 'dominic@lsgi.co.uk', NULL, 1, 'Beck House', '77a King Street', 'Knutsford', 'WA16 6DX', '2021-01-21 10:05:34', '2021-02-22 12:23:12', '12345', '23', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `partner_post`
--

CREATE TABLE `partner_post` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `partner_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `partner_service`
--

CREATE TABLE `partner_service` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service_id` int(11) NOT NULL,
  `partner_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('sadler.claire@gmail.com', '$2y$10$7bmCIe7sZ/S0orj4MBvTluTXQFXY3Ug.lL.MAJS7EkWpEN3YwSev.', '2020-04-30 12:41:15'),
('lewis@visionsharp.co.uk', '$2y$10$djGt0P4uMfv4cH2vSjZgq.Qb1dwaa89dqSV1nmcwPskMbNje1yt2O', '2020-05-05 14:12:13'),
('tpharrison69@gmail.com', '$2y$10$WNntK70cWcJIPXLbvCHMHO7wS7/pVPSmLkcvffxWFwXqg863SWlLa', '2020-05-21 06:44:20'),
('signature@trustboxltd.com', '$2y$10$i9c5iD.AVZkBFk4MLUWVMOypHWbV//.Bw8npkiIUKSzRJ4bnTkBR.', '2020-05-29 15:27:47');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `membership_id` int(11) NOT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `membership_id`, `amount`, `transaction_id`, `created_at`, `updated_at`) VALUES
(1, 135, '19.00', '17229269316021593424242', '2020-10-08 10:15:44', '2020-10-08 10:15:44'),
(2, 136, '19.00', '17229269316021626764242', '2020-10-08 11:11:18', '2020-10-08 11:11:18'),
(3, 137, '19.00', '17229269316021635334242', '2020-10-08 11:25:35', '2020-10-08 11:25:35'),
(4, 138, '19.00', '17229269316021660332424', '2020-10-08 12:07:15', '2020-10-08 12:07:15'),
(5, 140, '19.00', '17229269316027584442424', '2020-10-15 08:40:47', '2020-10-15 08:40:47'),
(6, 142, '19.00', '17229269316028548864242', '2020-10-16 11:28:08', '2020-10-16 11:28:08'),
(7, 144, '19.00', '17229269316039639044242', '2020-10-29 09:31:47', '2020-10-29 09:31:47'),
(8, 145, '19.00', '17229269316040688567938', '2020-10-30 14:40:58', '2020-10-30 14:40:58'),
(9, 172, '295.00', '17229269316058870754242', '2020-11-20 15:44:38', '2020-11-20 15:44:38'),
(10, 177, '295.00', '17229269316068209082424', '2020-12-01 11:08:30', '2020-12-01 11:08:30'),
(11, 177, '295.00', '17229269316081970984242', '2020-12-17 09:25:01', '2020-12-17 09:25:01'),
(12, 179, '295.00', '17229269316082924664242', '2020-12-18 11:54:27', '2020-12-18 11:54:27'),
(13, 182, '295.00', '17229269316100982314242', '2021-01-08 09:30:35', '2021-01-08 09:30:35'),
(14, 183, '295.00', '17229269316101028804242', '2021-01-08 10:48:02', '2021-01-08 10:48:02'),
(15, 187, '295.00', '17229269316104631004242', '2021-01-12 14:51:42', '2021-01-12 14:51:42'),
(16, 189, '295.00', '17229269316111365384242', '2021-01-20 09:55:40', '2021-01-20 09:55:40'),
(17, 191, '295.00', '17229269316128665594242', '2021-02-09 10:29:36', '2021-02-09 10:29:36');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `user_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 121, 'dashboard', '2020-06-17 09:16:30', '2020-06-17 09:16:30');

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE `plans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `plans`
--

INSERT INTO `plans` (`id`, `name`, `price`, `created_at`, `updated_at`) VALUES
(1, 'full', '0', '2020-04-29 11:23:29', '2020-04-29 11:23:29'),
(2, 'gifted', '0', '2020-04-29 11:23:29', '2020-04-29 11:23:29'),
(3, 'covid-19', '0', '2020-04-29 11:23:29', '2020-04-29 11:23:29'),
(4, 'household', '0', '2020-04-29 11:23:29', '2020-04-29 11:23:29'),
(5, 'trusted', '0', '2020-04-29 11:23:29', '2020-04-29 11:23:29'),
(6, 'staff', '0', '2020-04-29 11:23:29', '2020-04-29 11:23:29');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) DEFAULT '1',
  `enabled` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `content`, `banner`, `category_id`, `enabled`, `created_at`, `updated_at`) VALUES
(21, 'Test', '<p style=\"text-align: left;\">Powers of Attorney are legal documents that allow an individual (known as the Donor) to officially nominate someone they trust (known as the Attorney) to handle their affairs and make decisions on their behalf, if they are unable or unavailable to do so for themselves. Powers of Attorney fall into two main categories, Ordinary Power of Attorney and Lasting Powers of Attorney.</p>\r\n<pre style=\"text-align: left;\">&nbsp;</pre>\r\n<h3 style=\"text-align: left;\"><span style=\"color: #2ab6b9;\">Ordinary Power of Attorney</span></h3>\r\n<p style=\"text-align: left;\"><span style=\"background-color: #ffffff;\">Sometimes known as a General Power of Attorney, these are the simplest Power of Attorney to put in place. They don&rsquo;t need to be registered because they can only be used whilst the Donor has full capacity and are therefore valid immediately once signed and witnessed. They are designed to provide convenience for someone with full capacity, by allowing an Attorney(s) to act on their behalf when they are not available to make decisions or manage their financial affairs; making them invaluable for those isolating in the current COVID-19 pandemic. They are a legal document that must be correctly produced and properly signed and witnessed to be valid.&nbsp;</span></p>\r\n<pre style=\"text-align: left;\">&nbsp;</pre>\r\n<h3 style=\"text-align: left;\"><span style=\"color: #2ab6b9;\">Lasting Powers of Attorney</span></h3>\r\n<p style=\"text-align: left;\">Lasting Powers of Attorney (LPAs) are in fact two separate documents. One is for Property &amp; Financial Affairs and the other is for Health &amp; Welfare. They are a more robust arrangement than Ordinary Powers of Attorney and as well as producing the legal documents, they also must be registered with the Office of the Public Guardian to be valid. This is because Lasting Powers of Attorney will allow your trusted person(s) (Attorney(s)) to continue to look after your affairs and make decisions about your welfare even if you were to suffer a temporary or permanent loss of capacity. It should be noted the Office of the Public Guardian typically charge fees for the registration of Lasting Powers of Attorney. To register both a Property &amp; Financial Affairs LPA and a Health &amp; Welfare LPA the registration cost is typically &pound;164.</p>\r\n<pre style=\"text-align: left;\">&nbsp;</pre>\r\n<h3 style=\"text-align: left;\"><span style=\"color: #2ab6b9;\">What Powers of Attorney do I need?</span></h3>\r\n<p style=\"text-align: left;\">In short, you should seriously consider putting both Ordinary and Lasting Powers of Attorney in place, especially if you have been advised to stay at home at all times during the Covid-19 pandemic.</p>\r\n<p style=\"text-align: left;\">An Ordinary Power of Attorney comes into force the moment it is signed and witnessed and is therefore a very useful document to have in place whilst you wait the 12 weeks or so before your Lasting Powers of Attorney are registered. You will need Lasting Powers of Attorney if you want your trusted people to continue to look after your affairs and make welfare decisions should you suffer a loss of capacity.</p>\r\n<pre style=\"text-align: left;\">&nbsp;</pre>\r\n<h3 style=\"text-align: left;\"><span style=\"color: #2ab6b9;\">What if I don&rsquo;t have Lasting Powers of Attorney?</span></h3>\r\n<p style=\"text-align: left;\">If LPAs are not in place, and someone is deemed to be vulnerable because they have either temporarily or permanently lost capacity, all of their finances (including joint bank accounts) can be frozen and a court appointed Deputy is appointed to look after their affairs. This is designed to protect the vulnerable, but it incurs fees and adds a great deal of stress to the individual concerned, their spouse or partner, their family and Social Services. Furthermore, if LPAs are not in place, family members do not have the right to decide on the type of healthcare an individual receives.</p>\r\n<p style=\"text-align: left;\">If a person loses capacity without LPAs in place, a family member can apply to the Court of Protection for a Deputyship Order, but this is a long and drawn out process that will take many months and will often cost the person applying in excess of &pound;2,000. Furthermore, there is no guarantee that an application would be successful.</p>', 'storage/banners/2v5F0Nlan2tvVCvAsdRQUusSfZhAPdsSYlP8Z6GQ.jpeg', 1, 0, '2020-04-15 13:35:07', '2020-05-07 08:53:16'),
(22, 'What is a Living Will?', '<p style=\"text-align: left;\">A Living Will (sometimes known as an advance decision) is a decision you can make now to refuse a specific type of treatment at some time in the future. It lets your family, carers and health professionals know your wishes about refusing treatment if you\'re unable to make or communicate those decisions yourself.</p>\r\n<p style=\"text-align: left;\">The treatments you\'re deciding to refuse must all be named in the Living Will. You may want to refuse a treatment in some situations, but not others. If this is the case, you need to be clear about all the circumstances in which you want to refuse this treatment.</p>\r\n<p style=\"text-align: left;\">Deciding to refuse a treatment is not the same as asking someone to end your life or help you end your life. Euthanasia and assisted suicide are illegal in England.</p>\r\n<div style=\"text-align: left;\">&nbsp;</div>\r\n<h3 style=\"text-align: left;\"><span style=\"color: #2ab6b9;\">Life-sustaining treatment</span></h3>\r\n<p style=\"text-align: left;\">You can refuse a treatment that could potentially keep you alive, known as life-sustaining treatment. This is treatment that replaces or supports ailing bodily functions, such as:</p>\r\n<ul style=\"text-align: left;\">\r\n<li>ventilation: this may be used if you cannot breathe by yourself</li>\r\n<li>cardiopulmonary resuscitation (CPR): this may be used if your heart stops</li>\r\n<li>antibiotics: this can help your body fight infection</li>\r\n</ul>\r\n<p style=\"text-align: left;\">You may want to discuss this with a doctor or nurse who knows about your medical history before you make up your mind.</p>\r\n<div style=\"text-align: left;\">&nbsp;</div>\r\n<h3 style=\"text-align: left;\"><span style=\"color: #2ab6b9;\">Who makes a Living Will?</span></h3>\r\n<p style=\"text-align: left;\">You make your own Living Will, as long as you have the mental capacity to make such decisions. You may want to make a Living Will with the support of a clinician. If you decide to refuse life-sustaining treatment in the future, your Living Will needs to be:</p>\r\n<ul style=\"text-align: left;\">\r\n<li style=\"text-align: left;\">written down</li>\r\n<li style=\"text-align: left;\">signed by you</li>\r\n<li style=\"text-align: left;\">signed by a witness</li>\r\n</ul>\r\n<p style=\"text-align: left;\">If you wish to refuse life-sustaining treatments in circumstances where you might die as a result, you need to state this clearly in your Living Will. Life-sustaining treatment is sometimes called life-saving treatment. You may find it helpful to talk to a doctor or nurse about the kinds of treatments you might be offered in the future, and what it might mean if you choose not to have them.</p>\r\n<div style=\"text-align: left;\">&nbsp;</div>\r\n<h3 style=\"text-align: left;\"><span style=\"color: #2ab6b9;\">Is a Living Will legally binding?</span></h3>\r\n<p style=\"text-align: left;\">A Living Will is legally binding as long as it:</p>\r\n<ul style=\"text-align: left;\">\r\n<li>complies with the Mental Capacity Act</li>\r\n<li>is valid</li>\r\n<li>applies to the situation</li>\r\n</ul>\r\n<p style=\"text-align: left;\">If your Living Will is binding, it takes precedence over decisions made in your best interest by other people.</p>\r\n<p style=\"text-align: left;\">A Living Will may only be considered valid if:</p>\r\n<ul style=\"text-align: left;\">\r\n<li>you\'re aged 18 or over and had the capacity to make, understand and communicate your decision when you made it</li>\r\n<li>you specify clearly which treatments you wish to refuse</li>\r\n<li>you explain the circumstances in which you wish to refuse them</li>\r\n<li>it\'s signed by you and by a witness&nbsp;</li>\r\n<li>you have made the Living Will of your own accord, without any harassment by anyone else</li>\r\n<li>you have not said or done anything that would contradict the Living Will since you made it (for example, saying that you\'ve changed your mind)</li>\r\n</ul>\r\n<div style=\"text-align: left;\">&nbsp;</div>\r\n<h3 style=\"text-align: left;\"><span style=\"color: #2ab6b9;\">How does a Living Will help?</span></h3>\r\n<p style=\"text-align: left;\">As long as it\'s valid and applies to your situation, a Living Will gives your health and social care team clinical and legal instructions about your treatment choices. A Living Will should only be used if, at some time in the future, you\'re not able to make your own decisions about your treatment.</p>\r\n<div style=\"text-align: left;\">&nbsp;</div>\r\n<h3 style=\"text-align: left;\"><span style=\"color: #2ab6b9;\">Does a Living Will need to be signed and witnessed?</span></h3>\r\n<p style=\"text-align: left;\">Yes, if you\'re choosing to refuse life-sustaining treatment. In this case, the Living Will must be written down, and both you and a witness must sign it.</p>\r\n<div style=\"text-align: left;\">&nbsp;</div>\r\n<h3 style=\"text-align: left;\"><span style=\"color: #2ab6b9;\">Who should see it?</span></h3>\r\n<p style=\"text-align: left;\">You have the final say on who sees it, but you should make sure that your family, carers or health and social care professionals know about it and, very importantly, know where to access it.</p>\r\n<p style=\"text-align: left;\">Your family or carers may have to find it quickly if you require emergency treatment and they need to tell the healthcare professionals your wishes.</p>\r\n<p style=\"text-align: left;\">You can keep a copy in your medical records, and you can keep a copy in your Trust Box so a trusted person can access it if they need to.</p>\r\n<p style=\"text-align: left;\"><strong>Simply enrol for our benefit and we will guide you through every step to make your Living Will and be able to clearly communicate your wishes to loved ones and carers.</strong></p>', 'storage/banners/ERAu8kwYFiOiW3RwqLYPVOUTgruYWa6bq4HpvEUg.jpeg', 1, 1, '2020-05-07 08:22:48', '2020-05-19 12:37:04'),
(23, 'What are Powers of Attorney?', '<p style=\"text-align: left;\">Powers of Attorney are legal documents that allow an individual (known as the Donor) to officially nominate someone they trust (known as the Attorney) to handle their affairs and make decisions on their behalf, if they are unable or unavailable to do so for themselves. Powers of Attorney fall into two main categories, Ordinary Power of Attorney and Lasting Powers of Attorney.</p>\r\n<pre>&nbsp;</pre>\r\n<h3 style=\"text-align: left;\"><span style=\"color: #2ab6b9;\">Ordinary Power of Attorney</span></h3>\r\n<p style=\"text-align: left;\"><span style=\"background-color: #ffffff;\">Sometimes known as a General Power of Attorney, these are the simplest Power of Attorney to put in place. They don&rsquo;t need to be registered because they can only be used whilst the Donor has full capacity and are therefore valid immediately once signed and witnessed. They are designed to provide convenience for someone with full capacity, by allowing an Attorney(s) to act on their behalf when they are not available to make decisions or manage their financial affairs; making them invaluable for those isolating in the current COVID-19 pandemic. They are a legal document that must be correctly produced and properly signed and witnessed to be valid.&nbsp;</span></p>\r\n<pre>&nbsp;</pre>\r\n<h3 style=\"text-align: left;\"><span style=\"color: #2ab6b9;\">Lasting Powers of Attorney</span></h3>\r\n<p style=\"text-align: left;\">Lasting Powers of Attorney (LPAs) are in fact two separate documents. One is for Property &amp; Financial Affairs and the other is for Health &amp; Welfare. They are a more robust arrangement than Ordinary Powers of Attorney and as well as producing the legal documents, they also must be registered with the Office of the Public Guardian to be valid. This is because Lasting Powers of Attorney will allow your trusted person(s) (Attorney(s)) to continue to look after your affairs and make decisions about your welfare even if you were to suffer a temporary or permanent loss of capacity. It should be noted the Office of the Public Guardian typically charge fees for the registration of Lasting Powers of Attorney. To register both a Property &amp; Financial Affairs LPA and a Health &amp; Welfare LPA the registration cost is typically &pound;164.</p>\r\n<pre>&nbsp;</pre>\r\n<h3 style=\"text-align: left;\"><span style=\"color: #2ab6b9;\">What Powers of Attorney do I need?</span></h3>\r\n<p style=\"text-align: left;\">In short, you should seriously consider putting both Ordinary and Lasting Powers of Attorney in place, especially if you have been advised to stay at home at all times during the Covid-19 pandemic.</p>\r\n<p style=\"text-align: left;\">An Ordinary Power of Attorney comes into force the moment it is signed and witnessed and is therefore a very useful document to have in place whilst you wait the 12 weeks or so before your Lasting Powers of Attorney are registered. You will need Lasting Powers of Attorney if you want your trusted people to continue to look after your affairs and make welfare decisions should you suffer a loss of capacity.</p>\r\n<pre>&nbsp;</pre>\r\n<h3 style=\"text-align: left;\"><span style=\"color: #2ab6b9;\">What if I don&rsquo;t have Lasting Powers of Attorney?</span></h3>\r\n<p style=\"text-align: left;\">If LPAs are not in place, and someone is deemed to be vulnerable because they have either temporarily or permanently lost capacity, all of their finances (including joint bank accounts) can be frozen and a court appointed Deputy is appointed to look after their affairs. This is designed to protect the vulnerable, but it incurs fees and adds a great deal of stress to the individual concerned, their spouse or partner, their family and Social Services. Furthermore, if LPAs are not in place, family members do not have the right to decide on the type of healthcare an individual receives.</p>\r\n<p style=\"text-align: left;\">If a person loses capacity without LPAs in place, a family member can apply to the Court of Protection for a Deputyship Order, but this is a long and drawn out process that will take many months and will often cost the person applying in excess of &pound;2,000. Furthermore, there is no guarantee that an application would be successful.</p>', 'storage/banners/uSYGcaiwZ75ZoOtM29Nrdx7OVUR66TJ5XESbaSzi.jpeg', 1, 1, '2020-05-07 08:50:45', '2020-05-07 08:53:30'),
(24, 'How to save money fast on a low income', '<p>Saving money can be difficult and is often easier said than done. If you&rsquo;re on a low income or stuck in a cycle of living paycheck-to-paycheck, saving every month may feel even further out of your reach.</p>\r\n<p>A recent study by the Independent shows that&nbsp;<a href=\"https://www.independent.co.uk/news/uk/home-news/british-adults-savings-none-quarter-debt-cost-living-emergencies-survey-results-a8265111.html\">a quarter of UK adults don&rsquo;t have any savings</a>, with one third blaming too much debt and high monthly outgoings as the issue. One in ten also admitted they spend more than they earn each month.</p>\r\n<p>Whether you need to save money fast for a holiday next month or you are trying to save as much as possible each month for a wedding or house deposit, there are several ways you can cut costs and increase savings.</p>\r\n<p>At OpenMoney, we understand the importance of having enough cash savings put away, so we&rsquo;ve put together some tips of how you can save money on a low income.</p>\r\n<p><a href=\"https://www.open-money.co.uk/blog/save-money-fast-on-a-low-income\">Read more here</a></p>', 'storage/banners/oEwhaTajX1YjHOprJ976ViMmniuUqDc4eSsPCTUw.png', 1, 0, '2020-05-21 13:23:17', '2020-05-21 13:24:27');

-- --------------------------------------------------------

--
-- Table structure for table `providers`
--

CREATE TABLE `providers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nochex_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `providers`
--

INSERT INTO `providers` (`id`, `name`, `user_id`, `created_at`, `updated_at`, `nochex_id`, `provider_logo`, `telephone`, `email`) VALUES
(1, 'default', NULL, '2020-04-29 11:23:29', '2021-01-12 12:57:37', NULL, NULL, '', ''),
(2, 'Trustbox', 120, '2020-04-29 11:23:29', '2021-01-12 12:57:37', '0', 'storage/logos/vHqC62BDltK7xLbQOUnLoxzoQvYbgBuRwuEuHQza.png', '', ''),
(3, 'Open Money', 41, '2020-05-21 13:24:51', '2020-11-24 14:43:17', '0', 'storage/logos/7NMXof3rPG9vI6LnqzkkOaRwXplBfd1U2Mnw9QSi.png', '', ''),
(4, 'Redstone Wills', 189, '2020-09-28 13:13:46', '2021-02-18 10:51:20', '0', 'storage/logos/qWYDY1QQG0t4h9spwwTgK3l9pwb0FjWmEGCWtWgu.jpeg', '03454564567', 'redstone@lsgi.co.uk'),
(5, 'Signature Saver', NULL, '2020-11-12 15:53:07', '2020-11-24 14:42:18', '0', 'storage/logos/olL7IEA7UHD5gYQvwIz2rzeaDEfKGNxXcezS3Urz.jpeg', '', ''),
(6, 'Stephensons', NULL, '2020-11-24 14:47:07', '2020-11-24 15:13:49', '0', 'storage/logos/lCjNAraYPK5ZqXcfCMxmywz8utNeIXhi4tAolo6h.png', '', ''),
(7, 'Tuto', NULL, '2020-11-24 15:04:36', '2020-11-24 15:18:58', '0', 'storage/logos/YV0CRblpMFmDqm9z7tfbKjtGOZlo4eRXEYDDJyyB.png', '', ''),
(8, 'Love Legal', NULL, '2020-11-24 15:08:10', '2020-11-24 15:14:34', '0', 'storage/logos/UHcl1irBkyBPcI4OVoupk0ndX9DhvwzVUrAd1IJv.png', '', ''),
(9, 'Dignity', NULL, '2020-11-24 15:24:14', '2021-01-29 16:49:02', '1234567', 'storage/logos/y18phQxradDp4BKrieB83g9NZ8q8httVAkWwZ0Lo.jpeg', '0300 123 4567', 'dominic@lsgi.co.uk'),
(10, 'Legal Services Guild', NULL, '2020-11-24 15:26:37', '2020-11-24 15:27:55', '0', 'storage/logos/ou3Vc14AvgskVxa0zl3ZE336dzmmidTb33NfuoxT.png', '', ''),
(11, 'Connells', NULL, '2020-11-24 15:42:06', '2020-11-24 15:42:06', '0', 'storage/logos/aRlKkDPfi0x6SQo0Cpvf3O9RFWTzPpBPmCFto0qJ.png', '', ''),
(12, 'Conveyancing Direct Property Lawyers', NULL, '2020-11-24 15:46:30', '2020-11-24 15:46:30', '0', 'storage/logos/13U60CaHtaCUTBcL8luI5t6JGkX8YMtcZqkVwGfM.png', '', ''),
(13, 'Kingsway Claims', NULL, '2020-11-24 16:23:37', '2020-11-24 16:23:37', '0', 'storage/logos/D2LkryimW6T79dOQI263Pj5aD0NbJnI5QcRinLCE.png', '', ''),
(14, 'Elopa', NULL, '2020-11-24 16:38:28', '2020-11-24 16:38:28', '0', 'storage/logos/kuojapEoxf1FtfC9KsNviefisQuYxbbIcvV1tch9.png', '', ''),
(15, 'AKEA Lite', NULL, '2020-11-24 16:48:27', '2020-11-24 16:48:27', '0', 'storage/logos/n0jIIKwjFRTuDNvfxOmn6rVs3qnhwPjEZzcEnJNI.png', '', ''),
(16, 'Movin\' Legal', NULL, '2020-11-24 16:49:36', '2020-11-24 16:49:36', '0', 'storage/logos/4zoVyTaWcURmobuBOE3ay09B58KQvigzioBbV4sY.jpeg', '', ''),
(17, 'Hays Travel', NULL, '2020-11-24 16:50:59', '2020-11-24 16:50:59', '0', 'storage/logos/SKGlShAudyw7XTt55C0oTtPR0LW2UNTlHabsArex.png', '', ''),
(18, 'LSGI ltd', NULL, '2020-11-25 10:52:40', '2020-11-25 10:52:40', '0', 'storage/logos/EMAjztDRD5WTdI9hH4z6ytVNcG9aGR0AeOvxsoVT.png', '', ''),
(19, 'Remarkable Practice', NULL, '2021-01-11 13:58:34', '2021-01-11 13:58:34', '123', 'storage/logos/5rrfFfEsJJwhcZH5f5FsbuZHCKuVJAhCBlJuFGp7.jpeg', '01773 821 689', 'info@remarkablepractice.co.uk'),
(20, 'Advance Track Outsourcing', NULL, '2021-01-11 16:01:44', '2021-01-11 16:01:44', '1234', 'storage/logos/nABBGaXVUBliW6yq4F6B2R39dwqlVipcdzo4MRCT.jpeg', '01773821689', 'advancetrack@remarkablepractice.com'),
(21, 'MyWorkPapers', NULL, '2021-01-11 16:03:54', '2021-01-11 16:03:54', '1235', 'storage/logos/KHZtVRbOBM2jYrwOuQlvyi4Cxex0fng1IyU9vKfF.png', '01773821689', 'mwp@remarkablepractice.com'),
(22, 'Satago', NULL, '2021-01-11 16:18:05', '2021-01-11 16:18:05', '1236', 'storage/logos/Z1iwbZPd7MUVaP49M6PkbyibWiEKciHQ43wgmsDU.png', '01773821689', 'satago@remarkablepractice.com'),
(23, 'Zen Wills', NULL, '2021-01-21 10:05:34', '2021-02-01 13:53:25', '12345', 'storage/logos/AFdpnzl3FVdUuF4Et52zjLOrVpHbJicRZM7UbaRy.png', '0330 390 0535', 'dominic@lsgi.co.uk'),
(24, 'Aventria Property', NULL, '2021-01-29 16:18:26', '2021-02-01 11:05:50', '123455', 'storage/logos/pdd7zHiaoQBMsph64Nn2hJIHnXPsrpiAb1ZW6rPj.png', '0300 123 4567', 'dominic@lsgi.co.uk'),
(25, 'NWJ Legal', NULL, '2021-01-29 16:29:33', '2021-02-01 10:58:37', '123456', 'storage/logos/ikSoEUKPkeTNyYOwUumwmtmE469l65dFwmgjbkrM.png', '0300 123 4567', 'dominic@lsgi.co.uk'),
(26, 'Skipton Building Society', NULL, '2021-02-18 09:51:22', '2021-02-18 09:59:03', '1234567', 'storage/logos/Jg6WgMXMZYA6uSvRn2QHb0Z2MAZS4TayX2OCAWYV.jpeg', '0345 607 9756', 'skipton@lsgi.co.uk');

-- --------------------------------------------------------

--
-- Table structure for table `reasons`
--

CREATE TABLE `reasons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reasons`
--

INSERT INTO `reasons` (`id`, `name`, `visible`, `created_at`, `updated_at`) VALUES
(6, 'Registration', 1, '2020-04-30 13:28:04', '2020-05-06 07:43:14'),
(7, 'My Account Details', 1, '2020-04-30 13:28:23', '2020-05-06 07:43:14'),
(8, 'My Benefits', 1, '2020-04-30 13:28:39', '2020-05-06 07:43:14'),
(9, 'My Community', 1, '2020-04-30 13:28:50', '2020-05-06 07:43:14'),
(10, 'Other', 1, '2020-04-30 13:29:00', '2020-05-06 07:43:14'),
(11, 'My Payments', 1, '2020-12-21 10:36:47', '2020-12-21 10:36:47'),
(12, 'My Subscriptions', 1, '2020-12-21 10:37:10', '2020-12-21 10:37:10');

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entity_id` int(11) NOT NULL,
  `partner_id` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`id`, `type`, `action`, `entity_id`, `partner_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'benefit', 'disable', 4, 6, 1, '2020-06-10 08:36:21', '2020-06-11 12:29:04'),
(2, 'benefit', 'enable', 4, 8, 1, '2020-07-15 08:12:06', '2020-07-15 08:12:42'),
(3, 'benefit', 'disable', 4, 7, 1, '2020-07-29 09:54:05', '2020-11-06 12:08:15'),
(4, 'benefit', 'disable', 2, 9, 1, '2020-11-23 14:50:01', '2020-11-23 14:50:20'),
(6, 'benefit', 'disable', 21, 9, 1, '2020-11-23 14:51:04', '2020-11-23 14:51:34');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'User', '2020-04-29 11:23:29', '2020-04-29 11:23:29'),
(2, 'Admin', '2020-04-29 11:23:29', '2020-04-29 11:23:29'),
(3, 'Staff', '2020-04-29 11:23:29', '2020-04-29 11:23:29'),
(4, 'Provider', '2020-04-29 11:23:29', '2020-04-29 11:23:29'),
(5, 'Partner', '2020-04-29 11:23:29', '2020-04-29 11:23:29'),
(6, 'Member', '2020-04-29 11:23:29', '2020-04-29 11:23:29'),
(7, 'Pending', '2020-10-08 10:21:26', '2020-10-08 10:21:26');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `boosted_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `description`, `boosted_description`, `thumbnail`, `banner`, `created_at`, `updated_at`) VALUES
(1, 'dolores maxime ut dolore', 'Accusantium odit aperiam veritatis ea autem fugiat. Iure recusandae aut iste molestias. Numquam vitae tenetur enim error dolor soluta omnis dicta.\n\nNatus ipsam dolores modi. Dolorem corporis ut sit sint fugiat. Et sit dolor excepturi quae fuga minima itaque molestias. Dolor fugiat qui nihil.\n\nOfficia et est voluptatum maiores magni. Quas perspiciatis consequatur vitae quo vitae nihil. Est ipsa excepturi sunt voluptatem accusantium accusamus.\n\nMollitia qui minima adipisci. Voluptatem dolor dolor dolorem velit omnis qui velit quia. Libero et animi error aut. Dicta ipsa et accusantium et. Accusantium corrupti excepturi distinctio corrupti.\n\nRepellendus voluptatem iste laboriosam a aut saepe. Ducimus modi amet officiis consequatur aut repellendus. Ut qui illo ducimus quis.\n\nRerum vel delectus corrupti atque cumque in. Voluptas autem earum nisi voluptates sint omnis. Cupiditate corrupti excepturi eum aut placeat. Culpa ipsum optio ipsam voluptate quia sint.', 'Necessitatibus id cumque molestias magnam distinctio maiores exercitationem. Tenetur et eum aut expedita id. Magni debitis quas excepturi quibusdam repudiandae. Commodi eos ut porro aut non harum.\n\nAutem sunt quis illum. Quae quam unde eveniet impedit. Id repellendus dolore fugit aut. Sed dolorem cum sunt maiores quae aut.\n\nOdio libero autem aut et nihil. Perferendis id ipsa occaecati in minima a. Dicta quo quisquam rerum recusandae possimus voluptatum rerum quae. Odio laborum eius omnis velit.\n\nDeserunt enim doloribus omnis sequi libero. Quod temporibus illo qui omnis libero. Iure molestiae quia accusantium rem iste.', '/storage/thumbnails/thumbnail.jpg', 'storage/banners/banner.jpg', '2020-04-29 11:23:29', '2020-04-29 11:23:29'),
(2, 'est sit ut aut', 'Officiis expedita fugit blanditiis corrupti veniam. Iste architecto ratione qui.\n\nUt laborum ipsam maxime suscipit. Occaecati soluta at qui recusandae. Earum quaerat harum enim sint deleniti voluptatum laudantium. Voluptate cumque perferendis dolor ab.\n\nQuibusdam consequatur consequuntur reprehenderit perspiciatis sit harum ea. Non asperiores animi est quod eveniet. Et accusamus iusto qui dolorem mollitia. Voluptate unde iure quo dolores sint. Ipsa reprehenderit similique qui maiores est officiis.\n\nImpedit reprehenderit et libero ut quam. Et id veniam omnis architecto suscipit laudantium facere debitis. Fuga perspiciatis voluptatem cum qui quia molestias et.\n\nError alias ducimus accusamus voluptas nobis. Placeat sit quia dolorum voluptas. Ea et quo est totam sit velit architecto doloremque.\n\nIpsa temporibus tempora aut natus. Molestiae nemo doloribus tempore deleniti aut tempore voluptas. Molestias architecto qui rem cum quia.', 'Magni sequi iure et facilis aut. Nihil reprehenderit natus ducimus doloremque aut. Aut mollitia mollitia aut.\n\nAut distinctio non saepe eos occaecati eum. Dolorem omnis qui quo quisquam voluptas. Consequatur quidem libero et quia natus. Dolorem non hic odio facere atque et.\n\nConsequatur quo et officiis similique sit suscipit. Ut sed nobis est nulla nostrum suscipit omnis dolor. In fuga commodi itaque voluptatem aut eligendi.\n\nVeniam blanditiis illo quas nemo provident omnis. Velit sunt dolor dolores dolores. Incidunt iure nulla id blanditiis sequi voluptatem error. Enim qui numquam et asperiores nihil.', '/storage/thumbnails/thumbnail.jpg', 'storage/banners/banner.jpg', '2020-04-29 11:23:29', '2020-04-29 11:23:29'),
(3, 'sunt non molestiae dolor', 'Perferendis facilis illo deserunt eum sit sit quis. Alias repellendus facere ea est ipsum quam. Animi sit velit aliquam doloremque veniam distinctio. Error maiores enim maiores iusto voluptatibus ut et.\n\nSunt possimus et ut repellendus. Laudantium aliquam mollitia ipsam libero mollitia. Blanditiis exercitationem iusto occaecati consectetur tempora.\n\nDolores neque ut maxime illo fugiat. Qui molestiae ut qui quas sed. Praesentium accusamus reiciendis eum fugit ab facere libero. Aut ut molestias qui iusto dolorem omnis. Culpa aut voluptatem reiciendis iusto et quisquam.\n\nAtque magni sed nobis. Nemo cumque qui ea dolorem. Libero optio voluptatem autem aut est. Corrupti deleniti ut omnis similique neque est.\n\nNeque sed ut laudantium harum facilis. Dolor id officiis ab dolores autem voluptates aliquam. Quos porro recusandae neque sunt consequuntur qui vel. Veritatis omnis id voluptas aut voluptatem maiores non.\n\nEt exercitationem consequatur odio et tenetur ipsa. Eos itaque libero suscipit. Et doloremque voluptas dicta reprehenderit. Consequatur modi vel cupiditate dolores maxime mollitia.', 'Natus perferendis error est. Odit molestiae voluptatem a veritatis architecto culpa. Incidunt iste repellendus velit consequatur tenetur modi. Quae tenetur fugiat non qui nihil asperiores numquam.\n\nNemo omnis est facere voluptatem non recusandae ut. Sequi qui quia assumenda autem.\n\nEst et dolorum magnam itaque modi. Ducimus maiores quo quo consequuntur placeat odio animi. Laborum ad expedita quos aliquid sunt excepturi est. Accusantium et ipsum sequi rerum architecto consequatur voluptatem aliquid.\n\nUt vero quo sapiente ex ut. Vero quaerat consectetur consequuntur eum enim facere voluptas. Saepe voluptatem non quasi.', '/storage/thumbnails/thumbnail.jpg', 'storage/banners/banner.jpg', '2020-04-29 11:23:29', '2020-04-29 11:23:29'),
(4, 'a occaecati quo voluptates', 'Enim ut veritatis repudiandae facere provident voluptatem nesciunt vero. Consequatur animi incidunt molestiae et similique accusamus. Repudiandae voluptatum voluptatem minima odio ut cupiditate. Aperiam amet tempore praesentium ipsam voluptatem sint. Et autem voluptas sed soluta.\n\nIusto ea molestiae corrupti consequatur. Voluptate molestiae et quaerat illum fuga veritatis optio. Non qui aut aut earum reiciendis commodi ratione similique.\n\nDelectus consequatur inventore nostrum excepturi nesciunt qui qui at. Ea inventore dignissimos ullam veritatis. Officiis nam qui amet nobis incidunt aspernatur et. Vel et voluptatem quisquam ut rerum maiores qui.\n\nUnde officiis voluptates eos doloribus et. Aut illo autem odio nihil magni quam facilis. Et eum hic ullam.\n\nSequi est vero numquam iusto eos eos corrupti. Magni rerum dicta magnam non. Nostrum eum sapiente quia aut enim accusamus quas.\n\nConsequatur exercitationem atque amet. Hic pariatur labore impedit numquam est aperiam odio. Id deleniti tempora delectus culpa ea libero quod. Eligendi aut consequatur atque eos aliquam enim.', 'Eum excepturi eum corrupti rerum vel perspiciatis. Fugit odit magni velit facilis sequi. Autem quia voluptatem libero eos fugiat.\n\nSed perferendis ut incidunt asperiores ipsa magnam fugiat. Blanditiis voluptatem provident quia repellat rerum. Enim reiciendis ut harum explicabo possimus provident enim soluta. Velit non quis quaerat laboriosam.\n\nEt deserunt repellendus eligendi. Iste omnis soluta dolor accusantium libero dolorem mollitia enim. Dolorum ut sapiente vitae autem error beatae voluptates. Qui mollitia atque repudiandae aut. Dicta fugit voluptatum error vel magnam dolorum dignissimos qui.\n\nAut maxime magnam quam officiis corrupti voluptatem. Accusantium dolores non odio molestias neque. Nihil modi qui voluptas repellendus fuga molestiae doloremque.', '/storage/thumbnails/thumbnail.jpg', 'storage/banners/banner.jpg', '2020-04-29 11:23:29', '2020-04-29 11:23:29'),
(5, 'repellendus aut quaerat laboriosam', 'Laudantium assumenda in ut corrupti. Ullam deserunt tenetur officia tempora assumenda excepturi dolorum repudiandae. Voluptas distinctio maxime dignissimos dolore cumque aut.\n\nLabore aut quia ab sunt ut et dolores. Officiis quas animi sit maxime ea. Et quos qui illo et.\n\nModi quam nulla odio labore perferendis eligendi. Quis sit nihil ut totam porro sint. Velit aliquid et aperiam. Omnis enim ut illum sequi repellendus. Incidunt quis blanditiis sit pariatur.\n\nBlanditiis ipsam dolores provident quibusdam ratione voluptas ipsam. Nobis cupiditate laborum unde dolorem corrupti fugiat. Cupiditate et id laborum ut dolorem.\n\nQuibusdam et maiores omnis ut earum officia. Sint voluptates provident aut architecto voluptatem vel a. Eos cumque et consequatur ut itaque et. Sapiente placeat numquam eveniet et ut esse.\n\nMagni velit quos assumenda vel. Quia maiores modi quis at perferendis nobis. Et aut tempore eum saepe in. Animi quis omnis et cupiditate.', 'Neque saepe quae sed minus quia ut optio itaque. Est consequatur autem cumque labore ut laborum. Adipisci dolores voluptatem autem voluptatum suscipit.\n\nRerum magnam sunt quos quae aspernatur voluptas dolore sit. Eligendi magni rerum harum. Sequi illo inventore tenetur nesciunt distinctio aut porro. Consequuntur ex vel distinctio animi nostrum earum.\n\nEt iure voluptatibus fuga rerum possimus aspernatur. Quasi dolores optio facilis omnis numquam repudiandae incidunt. Libero est dolorum aut laborum ut minus. Nostrum iste nobis aut temporibus.\n\nRerum sed adipisci facere. Vel amet non et et assumenda ipsum at. Ad doloremque quis non. Omnis eius rerum dicta facere vel aperiam vel. Nihil minima qui ab tempore.', '/storage/thumbnails/thumbnail.jpg', 'storage/banners/banner.jpg', '2020-04-29 11:23:29', '2020-04-29 11:23:29'),
(6, 'vel et impedit omnis', 'Quo nemo doloribus magni dolor ut qui. Quia ullam et aperiam. Laborum sint sequi rerum sit sequi impedit optio.\n\nAlias ut qui porro ipsam unde omnis quis. Recusandae alias dignissimos iure possimus et repudiandae nihil pariatur. Id velit officiis odit. At quidem aut asperiores perspiciatis quis.\n\nQuas sed nemo natus aspernatur. Aut repellat et quae quod exercitationem sit aspernatur alias. Accusantium exercitationem pariatur dolores corrupti atque maiores sit.\n\nAnimi exercitationem soluta quis officiis fugit dolorem. Molestiae dolor officiis atque sequi. Illum reprehenderit aspernatur similique consequatur cupiditate adipisci asperiores.\n\nUt qui et quas qui nemo sit. Pariatur molestiae ut qui. Itaque necessitatibus ratione at voluptate.\n\nOmnis repellendus et sint. Vel omnis ut nam dignissimos libero. Aut magni aut voluptatem fugiat.', 'Accusamus rem nisi maxime maiores animi veritatis ad ut. Ab culpa nam expedita. Perferendis ut perspiciatis voluptate blanditiis in et provident. Debitis iste velit velit.\n\nIure ut sint eligendi rerum repellat illo voluptatem sit. Rerum necessitatibus qui fugiat asperiores aliquid sed et. Nostrum aspernatur dolorum voluptatem repudiandae eum quam. Ipsam omnis ea aut molestiae maxime.\n\nEt odit deserunt enim. Laudantium illum aut quas rerum ut. Totam aliquid quibusdam aut est rerum. Unde et similique id veniam.\n\nNisi placeat deserunt maxime. Id neque voluptates maiores est corrupti velit. Nostrum molestiae occaecati facilis exercitationem. Enim labore voluptatum dolor odit.', '/storage/thumbnails/thumbnail.jpg', 'storage/banners/banner.jpg', '2020-04-29 11:23:29', '2020-04-29 11:23:29');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('6hjqzjsNYp9nuEEvvLqJPhbdcy7C3voyVtjtPfY5', NULL, '82.36.27.144', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiYklXT1hhZ3liSG1OQ0JQTm1HaWNYc0VVUU9VanloMktDRHlRWll3YSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDU6Imh0dHBzOi8vb3Blbm1vbmV5LnlvdXJ0cnVzdGJveC5jby51ay9iZW5lZml0cyI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1614005976),
('hC4Nxl6lXw9maKh1UhvqDuvlcOogPbPeU3p8o84d', 63, '80.209.151.23', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.3 Safari/605.1.15', 'YTo0OntzOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjY6Il90b2tlbiI7czo0MDoiUHVLMWhySUxvbDl1RkNHWGxvZ2lRNFpONGVxbUJUOWFHYkVOQ201RSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDI6Imh0dHBzOi8vbHNnaS55b3VydHJ1c3Rib3guY28udWsvYmVuZWZpdHMvMiI7fXM6NTA6ImxvZ2luX3dlYl81OWJhMzZhZGRjMmIyZjk0MDE1ODBmMDE0YzdmNThlYTRlMzA5ODlkIjtpOjYzO30=', 1614001393),
('o3kkWJu0ny9yBlPj9XMBE4JNRzek5uBV6QTP3irc', 199, '81.108.147.181', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiRkRnalZZbUFrdjB5a2hYMEZjUWU1NFR0OFNKdlphOXBRamRSaUhLSiI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDU6Imh0dHBzOi8vb3Blbm1vbmV5LnlvdXJ0cnVzdGJveC5jby51ay9iZW5lZml0cyI7fXM6NTA6ImxvZ2luX3dlYl81OWJhMzZhZGRjMmIyZjk0MDE1ODBmMDE0YzdmNThlYTRlMzA5ODlkIjtpOjE5OTt9', 1614005211),
('pjfx7szQI594BqMePOhMokwGnonKDCmz4OGZXAZ9', NULL, '3.93.35.222', 'Slackbot-LinkExpanding 1.0 (+https://api.slack.com/robots)', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiOHZXbktPbHY3MFlGVFlYTHF3TTFXWk1MUkRSaFNRdW5UZ0RRYlB0eiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDU6Imh0dHBzOi8vb3Blbm1vbmV5LnlvdXJ0cnVzdGJveC5jby51ay9iZW5lZml0cyI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1614004163),
('Vwv2jcElddNEhkgp0SEJm6VgwiMbwFH1VrH11F0t', NULL, '80.209.151.23', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.3 Safari/605.1.15', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiRTVGQXVGWXJ2aUhZUEU1MU1LakZldW9NZzVvVkF0ampjR09BeVMybyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzE6Imh0dHBzOi8vbHNnaS55b3VydHJ1c3Rib3guY28udWsiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19', 1614001222);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `welcome_letter_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `welcome_email_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `new_user_benefits_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `invitation_letter_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_invite_email_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `signup_email_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `boosted_email_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cancelled_boosted_email_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `enrolled_email_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `invite_email_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `covid_invite_email_content` longtext COLLATE utf8mb4_unicode_ci,
  `removed_email_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `benefit_form_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `benefit_notify_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `default_storage` int(11) NOT NULL DEFAULT '5',
  `upgraded_storage` int(11) NOT NULL DEFAULT '10',
  `upgraded_storage_price` int(11) NOT NULL DEFAULT '5',
  `household_limit` int(11) NOT NULL DEFAULT '4',
  `gifted_limit` int(11) NOT NULL DEFAULT '2',
  `trusted_limit` int(11) NOT NULL DEFAULT '4',
  `fm_subdomain` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'filemanager',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`, `welcome_letter_content`, `welcome_email_content`, `new_user_benefits_content`, `invitation_letter_content`, `code_invite_email_content`, `signup_email_content`, `boosted_email_content`, `cancelled_boosted_email_content`, `enrolled_email_content`, `invite_email_content`, `covid_invite_email_content`, `removed_email_content`, `benefit_form_content`, `benefit_notify_content`, `default_storage`, `upgraded_storage`, `upgraded_storage_price`, `household_limit`, `gifted_limit`, `trusted_limit`, `fm_subdomain`, `created_at`, `updated_at`) VALUES
(1, 'default_partner', '1', 'This is the welcome content', '<p>Hi <strong>@first_name@</strong>,</p>\r\n<div><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">Congratulations</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">!</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">Y</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">ou</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">r membership to&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">Trust</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">Box</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">has now been approved.&nbsp;&nbsp;</span>&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">You will soon have access to the amazing be</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">nefits&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">included</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">in your</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">Trust</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">Box</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;member</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">ship</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">,</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;some of which include</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">access to a range of protective and cost saving benefits</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">, but&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">during our early release phase</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">,</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;your membership will&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">initially allow</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">you to create some key legal documents</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;during the Covid-19 pandemic</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">.</span>&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">Inside</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">Trust</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">Box</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;you will l</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">earn how&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">t</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">hese documents&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">can help you and your family members throughout&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">these&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">weeks and months</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;of&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">crisis.</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">Don&rsquo;t forget that&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">i</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">f you know somebody who would benefit f</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">ro</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">m&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">Ordinary&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">Powers of Attorney and</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">/or</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">&nbsp;a Living Will,&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">you can&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">invite them to create their own account</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">&nbsp;through the &lsquo;My Community&rsquo; section of your&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">Trust Box&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">account.</span>&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">Please&nbsp;</span><a href=\"https://yourtrustbox.co.uk/user-login\"><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">log in to your account&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">here</span></a><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">to access&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">your benefit and gift Trust Box to your loved ones.</span>&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">If you need help, or you have any other questions, feel free to</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;contact us using the&nbsp;<a href=\"https://yourtrustbox.co.uk/contact\">online contact form</a>&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">and a member of our customer service team will do their best to help you.</span>&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">Welcome to the Trust Box community.</span>&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">Thanks,</span>&nbsp;</div>\r\n<div><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">The Trust</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">Box team</span>&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">P.S. Please don&rsquo;t forget to support our&nbsp;<a href=\"https://yourtrustbox.co.uk/petition\">petition</a>.</span></div>', '<p>Hi <strong>@name@</strong></p>\r\n<p>Welcome to Trust Box.</p>\r\n<p>While you\'re a member of the community you instantly have access to a wide variety of different benefits that are exclusively for&nbsp;<strong>@partner_name@</strong>&nbsp;users.</p>\r\n<p>Below is a list of the different benefits they currently have to offer so feel free to visit each benefit below to see which ones are right for you!</p>', '<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Hi @name@</p>\n            <p style=\"margin-bottom: 0.28cm; line-height: 108%;\">We&rsquo;re writing to you today with some exciting news.</p>\n            <p style=\"margin-bottom: 0.28cm; line-height: 108%;\">@partner@ offer their customers an array of additional benefits and services through TrustBox. As a valued customer of @partner@, you&rsquo;re entitled to benefit from the @partner@ TrustBox, a carefully selected collection of benefits and services, designed to lighten the modern life load and help you sleep that little more soundly at night.</p>\n            <p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Best of all, you&rsquo;re entitled to Lifetime Membership, absolutely <strong>free</strong>, with no strings attached.</p>\n            <p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>About TrustBox</strong></p>\n            <p style=\"margin-bottom: 0.28cm; line-height: 108%;\">We know life changes quickly and that the legal, financial and health needs of you and your family are changing every day.</p>\n            <p style=\"margin-bottom: 0.28cm; line-height: 108%;\">That&rsquo;s why TrustBox has got your back, is in your corner, and on your side. TrustbBox have tested the market far and wide, to find the best providers of the services we all need in life. TrustBox providers are vetted by some of the best legal minds in the country so we know that the providers we choose are the best in the business. We share our favourite providers with you, like we would with our closest friends.</p>\n            <p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Your TrustBox Lifetime Membership introduces you to a specially selected benefits suite from 24/7 Legal Support, to Legacy Planning, to Conveyancing, giving you access to the best of the best in each field.</p>\n            <p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>Create your free TrustBox account</strong></p>\n            <p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Creating your TrustBox account is really easy. Simply follow the link below and sign up with your email address and password. We&rsquo;ll guide you through the information we need from you.</p>\n            <p style=\"margin-bottom: 0.28cm; line-height: 108%;\">You&rsquo;ll then have access to a whole host of benefits for you and your family.</p>\n            <p style=\"margin-bottom: 0.28cm; line-height: 108%;\">code: @code@</p>\n            <p style=\"margin-bottom: 0.28cm; line-height: 108%;\">If you need help, or you have any other questions, feel free to email <span style=\"color: #0563c1;\"><u><a href=\"mailto:help@trustbox.com\">help@trustbox.com</a></u></span> or call our customer service team on 0345 548 1230.<!-- Contact details tbc --></p>\n            <p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Thanks,</p>\n            <p style=\"margin-bottom: 0.28cm; line-height: 108%;\">The TrustBox team</p>', '<div style=\"margin-bottom: 0.28cm; line-height: 108%;\">Hi <strong>@name@</strong></div>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">We&rsquo;re writing to you today with some exciting news.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">@partner@ offer their customers an array of additional benefits and services through TrustBox. As a valued customer of @partner@, you&rsquo;re entitled to benefit from the @partner@ TrustBox, a carefully selected collection of benefits and services, designed to lighten the modern life load and help you sleep that little more soundly at night.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Best of all, you&rsquo;re entitled to Lifetime Membership, absolutely <strong>free</strong>, with no strings attached.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>About TrustBox</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">We know life changes quickly and that the legal, financial and health needs of you and your family are changing every day.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">That&rsquo;s why TrustBox has got your back, is in your corner, and on your side. TrustbBox have tested the market far and wide, to find the best providers of the services we all need in life. TrustBox providers are vetted by some of the best legal minds in the country so we know that the providers we choose are the best in the business. We share our favourite providers with you, like we would with our closest friends.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Your TrustBox Lifetime Membership introduces you to a specially selected benefits suite from 24/7 Legal Support, to Legacy Planning, to Conveyancing, giving you access to the best of the best in each field.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>Create your free TrustBox account</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Creating your TrustBox account is really easy. Simply follow the link below and sign up with your email address and password. We&rsquo;ll guide you through the information we need from you.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">You&rsquo;ll then have access to a whole host of benefits for you and your family.</p>\r\n<p>Your family code: <code>@code@</code></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">If you need help, or you have any other questions, feel free to email <span style=\"color: #0563c1;\"><u><a href=\"mailto:help@trustbox.com\">help@trustbox.com</a></u></span> or call our customer service team on 0345 548 1230.<!-- Contact details tbc --></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Thanks,</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">The Trust Box team</p>', '<p>Hi @name@,</p>\r\n<p><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">Congratulations</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">!</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">Y</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">ou</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">r lifetime membership to&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">Trust</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">Box</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">has now been approved.&nbsp;&nbsp;</span>&nbsp;</p>\r\n<div><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">You will soon have access to the amazing be</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">nefits&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">included</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">in your</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">Trust</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">Box</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;member</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">ship</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">,</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;some of which include</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">access to a range of protective and cost saving benefits</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">, but&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">during our early release phase</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">,</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;your membership will&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">initially allow</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">you to create some key legal documents</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;during the Covid-19 pandemic</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">.</span>&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">Inside</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">Trust</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">Box</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;you will l</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">earn how&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">t</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">hese documents&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">can help you and your family members throughout&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">these&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">weeks and months</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;of&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">crisis.</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">Don&rsquo;t forget that&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">i</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">f you know somebody who would benefit f</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">ro</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">m&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">Ordinary&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">Powers of Attorney and</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">/or</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">&nbsp;a Living Will,&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">you can&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">invite them to create their own account</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">&nbsp;through the &lsquo;My Community&rsquo; section of your&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">Trust Box&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">account.</span>&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">Please&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><a href=\"https://yourtrustbox.co.uk/user-login\"><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">log in to your account&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">here</span></a>&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">to access&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"auto\">your benefit and gift Trust Box to your loved ones.</span>&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">If you need help, or you have any other questions, feel free to</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;contact us using the&nbsp;<a href=\"https://yourtrustbox.co.uk/contact\">online contact form</a>&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">and a member of our customer service team will do their best to help you.</span>&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">Welcome to the Trust Box community.</span>&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">Thanks,</span>&nbsp;</div>\r\n<div><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">The Trust</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">Box team</span>&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">P.S. Please don&rsquo;t forget to support our&nbsp;</span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\"><a href=\"https://yourtrustbox.co.uk/petition\">petition</a></span><span lang=\"EN-GB\" xml:lang=\"EN-GB\" data-contrast=\"none\">.</span></div>', '<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Hi<strong> @name@</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">You&rsquo;ve just subscribed to a <strong>Trust Box </strong>benefit.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">This is confirmation of your newly subscribed benefit.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>@benefit_name@</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">In addition to this benefit&rsquo;s existing features, you now have access to:</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>@benefit_details@</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Learn how your benefits can help you and your family members throughout some of life&rsquo;s major milestones.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">If you need help, or you have any other questions, feel free to email <span style=\"color: #0563c1;\"><u><a href=\"mailto:help@trustbox.com\">help@trustbox.com</a></u></span> or call our customer service team on 0345 548 1230.<!-- Contact details tbc --></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Thanks,</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">The Trust Box team</p>', '<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Hi <strong>@name@</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">You&rsquo;ve just cancelled your benefit subscription.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">This is confirmation that the following benefit subscription has been cancelled</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>@benefit_name@</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">If you are wanting access to the additional features this subscription has to offer you will need to re-subscribe.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Learn how your benefits can help you and your family members throughout some of life&rsquo;s major milestones. Browse your range of benefits by logging in to your TrustBox account here.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">If you need help, or you have any other questions, feel free to email&nbsp;<span style=\"color: #0563c1;\"><u><a href=\"mailto:help@trustbox.com\">help@trustbox.com</a></u></span>&nbsp;or call our customer service team on 0345 548 1230.</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Thanks,</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">The Trust Box team</p>', '<p style=\"caret-color: #000000; font-family: Helvetica; font-size: 12px; text-size-adjust: auto;\"><span style=\"font-size: 12pt;\">Hi&nbsp;<strong>@name@</strong></span></p>\r\n<p style=\"caret-color: #000000; font-family: Helvetica; font-size: 12px; text-size-adjust: auto;\"><span style=\"font-size: 12pt;\">This is a quick email from Trust Box to let you know that you have successfully enrolled to the following benefit.</span></p>\r\n<p style=\"caret-color: #000000; font-family: Helvetica; font-size: 12px; text-size-adjust: auto;\"><span style=\"font-size: 12pt;\"><strong>@benefit_name@</strong></span></p>\r\n<p style=\"caret-color: #000000; font-family: Helvetica; font-size: 12px; text-size-adjust: auto;\"><span style=\"font-size: 12pt;\">Learn how your benefits can help you an your family members throughout some of life\'s major milestones. Browse your range of benefits by logging in to your Trust Box account here.</span></p>\r\n<p style=\"caret-color: #000000; font-family: Helvetica; font-size: 12px; text-size-adjust: auto;\"><span style=\"font-size: 12pt;\">If you need help, or you have any other questions, feel free to contact us using the online contact form within your account and a member of our customer service team will do their best to help you.</span></p>\r\n<p style=\"caret-color: #000000; font-family: Helvetica; font-size: 12px; text-size-adjust: auto;\"><span style=\"font-size: 12pt;\">Thanks,</span></p>\r\n<p style=\"caret-color: #000000; font-family: Helvetica; font-size: 12px; text-size-adjust: auto;\"><span style=\"font-size: 12pt;\">The Trust Box team</span></p>\r\n<p style=\"caret-color: #000000; font-family: Helvetica; font-size: 12px; text-size-adjust: auto;\">&nbsp;</p>', '<p><span style=\"font-family: helvetica, arial, sans-serif; font-size: 12pt;\">Hi <strong>@name@</strong>,</span></p>\r\n<p><span style=\"font-family: helvetica, arial, sans-serif; font-size: 12pt;\"><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">Great News! </span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\"><strong><span style=\"font-variant-ligatures: normal;\">@user@</span></strong>&nbsp;</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">is</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;now</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;a&nbsp;</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">Trust</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">Box</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;member</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">and as part of the&nbsp;</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">s</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">ame&nbsp;</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">household</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\"> </span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">y</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"SpellingError SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-repeat: repeat-x; background-position: left bottom; background-image: url(\'https://www.yourtrustbox.co.uk/img_40305918971611142895220_0\'); border-bottom: 1px solid transparent;\" data-ccp-parastyle=\"Normal (Web)\">ou</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;get</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;to share access to the&nbsp;</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">same</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;range of benefits&nbsp;</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">Trust</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">Box</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;membership has to offer.</span></span><span class=\"EOP SCXW71573157 BCX0\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; line-height: 20.2667px;\" data-ccp-props=\"{&quot;134233117&quot;:true,&quot;134233118&quot;:true,&quot;201341983&quot;:0,&quot;335559739&quot;:159,&quot;335559740&quot;:256}\">&nbsp;</span></span></p>\r\n<div><span style=\"font-size: 12pt;\"><strong><span style=\"font-family: helvetica, arial, sans-serif;\"><span class=\"EOP SCXW71573157 BCX0\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; line-height: 20.2667px;\" data-ccp-props=\"{&quot;134233117&quot;:true,&quot;134233118&quot;:true,&quot;201341983&quot;:0,&quot;335559739&quot;:159,&quot;335559740&quot;:256}\">Benefits of Trust Box membership</span></span></strong></span></div>\r\n<div class=\"OutlineElement Ltr  BCX0 SCXW71573157\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: \'Segoe UI\', \'Segoe UI Web\', Arial, Verdana, sans-serif; font-size: 12px; background-color: #ffffff;\"><span style=\"font-family: helvetica, arial, sans-serif; font-size: 12pt;\"><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">Trust Box&nbsp;provides a range of tools and benefits that enable your legal, financial and health needs to keep up with our ever-changing lives and the lives of our loved ones.&nbsp;&nbsp;</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"AdvancedProofingIssue SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-repeat: repeat-x; background-position: left bottom; background-image: url(\'https://www.yourtrustbox.co.uk/img_40305918971611142895220_1\'); border-bottom: 1px solid transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">In the near future</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;Trust Box will give you access to more benefits and services like 24/7 Legal Support, Legacy Planning, Conveyancing, Financial Advice and Funeral Plans all chosen from our favourite trusted providers.</span></span><span class=\"EOP SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; line-height: 20.2667px;\" data-ccp-props=\"{&quot;134233117&quot;:true,&quot;134233118&quot;:true,&quot;201341983&quot;:0,&quot;335559739&quot;:159,&quot;335559740&quot;:256}\">&nbsp;</span></span></div>\r\n<div class=\"OutlineElement Ltr  BCX0 SCXW71573157\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: \'Segoe UI\', \'Segoe UI Web\', Arial, Verdana, sans-serif; font-size: 12px; background-color: #ffffff;\">&nbsp;</div>\r\n<div class=\"OutlineElement Ltr  BCX0 SCXW71573157\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: \'Segoe UI\', \'Segoe UI Web\', Arial, Verdana, sans-serif; font-size: 12px; background-color: #ffffff;\"><span style=\"font-size: 12pt;\"><strong><span style=\"font-family: helvetica, arial, sans-serif;\"><span class=\"EOP SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; line-height: 20.2667px;\" data-ccp-props=\"{&quot;134233117&quot;:true,&quot;134233118&quot;:true,&quot;201341983&quot;:0,&quot;335559739&quot;:159,&quot;335559740&quot;:256}\">Confirm Your Membership</span></span></strong></span></div>\r\n<div class=\"OutlineElement Ltr  BCX0 SCXW71573157\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: \'Segoe UI\', \'Segoe UI Web\', Arial, Verdana, sans-serif; font-size: 12px; background-color: #ffffff;\"><span style=\"font-family: helvetica, arial, sans-serif; font-size: 12pt;\"><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">Confirming your Trust Box account is&nbsp;</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"AdvancedProofingIssue SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-repeat: repeat-x; background-position: left bottom; background-image: url(\'https://www.yourtrustbox.co.uk/img_40305918971611142895220_2\'); border-bottom: 1px solid transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">really easy</span></span><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">. Simply follow the link below and enter your basic contact information to register your account.&nbsp;</span></span><span class=\"EOP SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; line-height: 20.2667px;\" data-ccp-props=\"{&quot;134233117&quot;:true,&quot;134233118&quot;:true,&quot;201341983&quot;:0,&quot;335559739&quot;:159,&quot;335559740&quot;:256}\">&nbsp;</span></span></div>\r\n<div class=\"OutlineElement Ltr  BCX0 SCXW71573157\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: \'Segoe UI\', \'Segoe UI Web\', Arial, Verdana, sans-serif; font-size: 12px; background-color: #ffffff;\">&nbsp;</div>\r\n<div class=\"OutlineElement Ltr  BCX0 SCXW71573157\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: \'Segoe UI\', \'Segoe UI Web\', Arial, Verdana, sans-serif; font-size: 12px; background-color: #ffffff;\"><span style=\"font-family: helvetica, arial, sans-serif; font-size: 12pt;\"><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">You can then go ahead and create your free Ordinary&nbsp;Power of Attorney and/or Living Will immediately.&nbsp;</span></span><span class=\"EOP SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; line-height: 20.2667px;\" data-ccp-props=\"{&quot;134233117&quot;:true,&quot;134233118&quot;:true,&quot;201341983&quot;:0,&quot;335559739&quot;:159,&quot;335559740&quot;:256}\">&nbsp;</span></span></div>\r\n<div class=\"OutlineElement Ltr  BCX0 SCXW71573157\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: \'Segoe UI\', \'Segoe UI Web\', Arial, Verdana, sans-serif; font-size: 12px; background-color: #ffffff;\">&nbsp;</div>\r\n<div class=\"OutlineElement Ltr  BCX0 SCXW71573157\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: \'Segoe UI\', \'Segoe UI Web\', Arial, Verdana, sans-serif; font-size: 12px; background-color: #ffffff;\"><span style=\"font-family: helvetica, arial, sans-serif; font-size: 12pt;\"><span class=\"TextRun SCXW71573157 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; color: #222222; font-kerning: none; line-height: 19px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"none\"><span class=\"NormalTextRun SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\">If you need help, or you have any other questions, feel free to&nbsp;contact us using the <a href=\"https://yourtrustbox.co.uk/contact\"><span style=\"text-decoration: underline;\">online contact form</span></a>&nbsp;and a member of our customer service team will do their best to help you.</span></span><span class=\"EOP SCXW71573157 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; line-height: 19px;\" data-ccp-props=\"{&quot;134233117&quot;:true,&quot;201341983&quot;:2,&quot;335559739&quot;:159,&quot;335559740&quot;:211}\">&nbsp;</span></span></div>\r\n<div class=\"OutlineElement Ltr  BCX0 SCXW71573157\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: \'Segoe UI\', \'Segoe UI Web\', Arial, Verdana, sans-serif; font-size: 12px; background-color: #ffffff;\">&nbsp;</div>\r\n<div class=\"OutlineElement Ltr  BCX0 SCXW71573157\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: \'Segoe UI\', \'Segoe UI Web\', Arial, Verdana, sans-serif; font-size: 12px; background-color: #ffffff;\"><span style=\"color: #222222; font-family: helvetica, arial, sans-serif; font-size: 12pt; font-variant-ligatures: none;\">We hope you find Trust Box useful.&nbsp;</span></div>\r\n<p><span style=\"font-family: helvetica, arial, sans-serif; font-size: 12pt;\">Thanks,</span></p>\r\n<p><span style=\"font-family: helvetica, arial, sans-serif; font-size: 12pt;\">The Trust Box team</span></p>\r\n<p><span style=\"font-size: 12pt;\"><strong><span style=\"font-family: helvetica, arial, sans-serif;\">P.S. We would be very grateful if you could spare 2 minutes to support our<a href=\"https://yourtrustbox.co.uk/petition\"> petition</a></span></strong></span></p>', '<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><span style=\"font-family: helvetica, arial, sans-serif; font-size: 10pt;\">Hi @name@,</span></p>\r\n<p style=\"margin-right: 0cm; margin-left: 0cm; font-size: 12pt; font-family: \'Times New Roman\', serif; line-height: 16.96px;\"><span style=\"font-family: helvetica, arial, sans-serif; font-size: 10pt;\"><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">Great News!&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\"><span style=\"font-variant-ligatures: normal;\">@user@</span><span style=\"font-variant-ligatures: normal;\"> </span></span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">who&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">is a&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">Trust</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">Box</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;member</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;has invited you to share</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">access to the great range of benefits&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">Trust</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">Box</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;membership has to offer.</span></span><span class=\"EOP SCXW51560696 BCX0\" style=\"color: windowtext; margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; line-height: 20.2667px;\" data-ccp-props=\"{&quot;134233117&quot;:true,&quot;134233118&quot;:true,&quot;201341983&quot;:0,&quot;335559739&quot;:159,&quot;335559740&quot;:256}\">&nbsp;</span></span></p>\r\n<div class=\"OutlineElement Ltr  BCX0 SCXW51560696\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: \'Segoe UI\', \'Segoe UI Web\', Arial, Verdana, sans-serif; font-size: 12px; background-color: #ffffff;\">\r\n<p class=\"Paragraph SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow-wrap: break-word; vertical-align: baseline; background-color: transparent; color: windowtext;\"><span style=\"color: #2ab6b9; font-family: helvetica, arial, sans-serif; font-size: 10pt;\"><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-weight: bold; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-charstyle=\"Strong\">Benefits of&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-weight: bold; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-charstyle=\"Strong\">Trust</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-weight: bold; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-charstyle=\"Strong\">&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-weight: bold; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-charstyle=\"Strong\">Box</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-weight: bold; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-charstyle=\"Strong\">&nbsp;membership</span></span><span class=\"EOP SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; line-height: 20.2667px;\" data-ccp-props=\"{&quot;134233117&quot;:true,&quot;134233118&quot;:true,&quot;201341983&quot;:0,&quot;335559739&quot;:159,&quot;335559740&quot;:256}\">&nbsp;</span></span></p>\r\n</div>\r\n<div class=\"OutlineElement Ltr  BCX0 SCXW51560696\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: \'Segoe UI\', \'Segoe UI Web\', Arial, Verdana, sans-serif; font-size: 12px; background-color: #ffffff;\">\r\n<p class=\"Paragraph SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow-wrap: break-word; vertical-align: baseline; background-color: transparent; color: windowtext;\"><span style=\"font-family: helvetica, arial, sans-serif; font-size: 10pt;\"><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">Trust</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">Box</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">provides&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">a range of tools and benefits&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">that enable your&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">legal, financial and health needs to keep up with</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">our&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">ever-changing</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;lives and&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">the lives of our loved ones.</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"AdvancedProofingIssue SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-repeat: repeat-x; background-position: left bottom; background-image: url(\'data:image/gif;base64,R0lGODlhBQAEAPABAJttFAAAACH5BAUAAAEALAAAAAAFAAQAAAIFjA2JelcAOw==\'); border-bottom: 1px solid transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">In the near future</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">Trust</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">Box</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;will&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">give you access to</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;more benefits&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">and services&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">like&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">24/7 Legal Support, Legacy Planning, Conveyancing,&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">F</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">inancial&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">A</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">dvice and&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">F</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">uneral&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">P</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">lans</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">a</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">ll chosen from our favourite</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;trusted</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;providers</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">.</span></span><span class=\"EOP SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; line-height: 20.2667px;\" data-ccp-props=\"{&quot;134233117&quot;:true,&quot;134233118&quot;:true,&quot;201341983&quot;:0,&quot;335559739&quot;:159,&quot;335559740&quot;:256}\">&nbsp;</span></span></p>\r\n<p class=\"Paragraph SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow-wrap: break-word; vertical-align: baseline; background-color: transparent; color: windowtext;\">&nbsp;</p>\r\n</div>\r\n<div class=\"OutlineElement Ltr  BCX0 SCXW51560696\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: \'Segoe UI\', \'Segoe UI Web\', Arial, Verdana, sans-serif; font-size: 12px; background-color: #ffffff;\">\r\n<p class=\"Paragraph SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow-wrap: break-word; vertical-align: baseline; background-color: transparent; color: windowtext;\"><span style=\"color: #2ab6b9; font-family: helvetica, arial, sans-serif; font-size: 10pt;\"><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-weight: bold; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">Early release in&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-weight: bold; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">response</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-weight: bold; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;to Covid-19</span></span><span class=\"EOP SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; line-height: 20.2667px;\" data-ccp-props=\"{&quot;134233117&quot;:true,&quot;134233118&quot;:true,&quot;201341983&quot;:0,&quot;335559739&quot;:159,&quot;335559740&quot;:256}\">&nbsp;</span></span></p>\r\n</div>\r\n<div class=\"OutlineElement Ltr  BCX0 SCXW51560696\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: \'Segoe UI\', \'Segoe UI Web\', Arial, Verdana, sans-serif; font-size: 12px; background-color: #ffffff;\">\r\n<p class=\"MsoNormal\" style=\"margin: 0cm 0cm 0.0001pt; font-size: 11pt; font-family: Calibri, sans-serif;\"><span style=\"font-size: 10pt; font-family: Helvetica, sans-serif; color: #222222;\">Trust Box was due to be launched in July, however, we have taken the decision to release Trust Box earlier than scheduled to offer support in response to the Covid-19 pandemic.&nbsp; We want everyone in the UK, but especially those who have been advised by the NHS to &ldquo;</span><em><span style=\"font-size: 10pt; font-family: Helvetica, sans-serif;\">stay at home at all times</span></em><span style=\"font-size: 10pt; font-family: Helvetica, sans-serif; color: #222222;\">&rdquo;, to be able to rely on a family member, or a trusted person, to officially take care of their affairs if they are unable to do so. &nbsp;</span><span style=\"font-size: 10pt; font-family: Helvetica, sans-serif; color: #222222;\">With that in mind we have enabled one key benefit only for this time of crisis; The production of an Ordinary Power of Attorney and a Living Will.</span></p>\r\n<p class=\"Paragraph SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow-wrap: break-word; vertical-align: baseline; background-color: transparent; color: windowtext;\">&nbsp;</p>\r\n</div>\r\n<div class=\"OutlineElement Ltr  BCX0 SCXW51560696\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: \'Segoe UI\', \'Segoe UI Web\', Arial, Verdana, sans-serif; font-size: 12px; background-color: #ffffff;\">\r\n<p class=\"Paragraph SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow-wrap: break-word; vertical-align: baseline; background-color: transparent; color: windowtext;\"><span style=\"font-family: helvetica, arial, sans-serif; font-size: 10pt;\"><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">What&rsquo;s more, we&rsquo;ve made it available fo</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">r free</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;to anyone so, even you have these key documents in place perhaps you know someone who could use your help to access them for free.</span></span><span class=\"EOP SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; line-height: 20.2667px;\" data-ccp-props=\"{&quot;134233117&quot;:true,&quot;134233118&quot;:true,&quot;201341983&quot;:0,&quot;335559739&quot;:159,&quot;335559740&quot;:256}\">&nbsp;</span></span></p>\r\n<p class=\"Paragraph SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow-wrap: break-word; vertical-align: baseline; background-color: transparent; color: windowtext;\">&nbsp;</p>\r\n</div>\r\n<div class=\"OutlineElement Ltr  BCX0 SCXW51560696\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: \'Segoe UI\', \'Segoe UI Web\', Arial, Verdana, sans-serif; font-size: 12px; background-color: #ffffff;\">\r\n<p class=\"Paragraph SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow-wrap: break-word; vertical-align: baseline; background-color: transparent; color: windowtext;\"><span style=\"color: #2ab6b9; font-family: helvetica, arial, sans-serif; font-size: 10pt;\"><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-weight: bold; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-charstyle=\"Strong\">Confirm Your Membership</span></span><span class=\"EOP SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; line-height: 20.2667px;\" data-ccp-props=\"{&quot;134233117&quot;:true,&quot;134233118&quot;:true,&quot;201341983&quot;:0,&quot;335559739&quot;:159,&quot;335559740&quot;:256}\">&nbsp;</span></span></p>\r\n</div>\r\n<div class=\"OutlineElement Ltr  BCX0 SCXW51560696\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: \'Segoe UI\', \'Segoe UI Web\', Arial, Verdana, sans-serif; font-size: 12px; background-color: #ffffff;\">\r\n<p class=\"Paragraph SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow-wrap: break-word; vertical-align: baseline; background-color: transparent; color: windowtext;\"><span style=\"font-family: helvetica, arial, sans-serif; font-size: 10pt;\"><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">C</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">onfirming</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;your&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">Trust</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">Box</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;account is&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"AdvancedProofingIssue SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-repeat: repeat-x; background-position: left bottom; background-image: url(\'data:image/gif;base64,R0lGODlhBQAEAPABAJttFAAAACH5BAUAAAEALAAAAAAFAAQAAAIFjA2JelcAOw==\'); border-bottom: 1px solid transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">really easy</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">. Simply follow the link below and</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">enter</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">&nbsp;your basic contact information to register your account.&nbsp;</span></span><span class=\"EOP SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; line-height: 20.2667px;\" data-ccp-props=\"{&quot;134233117&quot;:true,&quot;134233118&quot;:true,&quot;201341983&quot;:0,&quot;335559739&quot;:159,&quot;335559740&quot;:256}\">&nbsp;</span></span></p>\r\n</div>\r\n<div class=\"OutlineElement Ltr  BCX0 SCXW51560696\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: \'Segoe UI\', \'Segoe UI Web\', Arial, Verdana, sans-serif; font-size: 12px; background-color: #ffffff;\">\r\n<p class=\"Paragraph SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow-wrap: break-word; vertical-align: baseline; background-color: transparent; color: windowtext;\" xml:lang=\"EN-GB\">&nbsp;</p>\r\n<p class=\"Paragraph SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow-wrap: break-word; vertical-align: baseline; background-color: transparent; color: windowtext;\" xml:lang=\"EN-GB\"><span style=\"font-family: helvetica, arial, sans-serif; font-size: 10pt;\"><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">You can then go ahead and create your <strong>free&nbsp;</strong></span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">Ordinary&nbsp;</span></span><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"auto\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">Power of Attorney and/or Living Will immediately.&nbsp;</span></span><span class=\"EOP SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; line-height: 20.2667px;\" data-ccp-props=\"{&quot;134233117&quot;:true,&quot;134233118&quot;:true,&quot;201341983&quot;:0,&quot;335559739&quot;:159,&quot;335559740&quot;:256}\">&nbsp;</span></span></p>\r\n<p class=\"Paragraph SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow-wrap: break-word; vertical-align: baseline; background-color: transparent; color: windowtext;\" xml:lang=\"EN-GB\">&nbsp;</p>\r\n</div>\r\n<div class=\"OutlineElement Ltr  BCX0 SCXW51560696\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: \'Segoe UI\', \'Segoe UI Web\', Arial, Verdana, sans-serif; font-size: 12px; background-color: #ffffff;\">\r\n<p class=\"Paragraph SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow-wrap: break-word; vertical-align: baseline; background-color: transparent; color: windowtext;\"><span style=\"font-family: helvetica, arial, sans-serif; font-size: 10pt;\"><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; color: #222222; font-kerning: none; line-height: 19px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"none\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\">If you need help, or you have any other questions, feel free to&nbsp;contact us using the <a href=\"https://yourtrustbox.co.uk/contact\"><span style=\"text-decoration: underline;\">online contact form</span></a> and a member of our customer service team will do their best to help you.</span></span><span class=\"EOP SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; line-height: 19px;\" data-ccp-props=\"{&quot;134233117&quot;:true,&quot;201341983&quot;:2,&quot;335559739&quot;:159,&quot;335559740&quot;:211}\">&nbsp;</span></span></p>\r\n</div>\r\n<div class=\"OutlineElement Ltr  BCX0 SCXW51560696\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: \'Segoe UI\', \'Segoe UI Web\', Arial, Verdana, sans-serif; font-size: 12px; background-color: #ffffff;\">\r\n<p class=\"Paragraph SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow-wrap: break-word; vertical-align: baseline; background-color: transparent; color: windowtext;\">&nbsp;</p>\r\n<p class=\"Paragraph SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow-wrap: break-word; vertical-align: baseline; background-color: transparent; color: windowtext;\"><span style=\"font-family: helvetica, arial, sans-serif; font-size: 10pt;\"><span class=\"TextRun SCXW51560696 BCX0\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; color: #222222; font-kerning: none; line-height: 19px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"none\"><span class=\"NormalTextRun SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\">We hope you find Trust Box useful.&nbsp;</span></span></span></p>\r\n<p class=\"Paragraph SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow-wrap: break-word; vertical-align: baseline; background-color: transparent; color: windowtext;\">&nbsp;</p>\r\n<p class=\"Paragraph SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow-wrap: break-word; vertical-align: baseline; background-color: transparent; color: windowtext;\"><span class=\"EOP SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-size: 10pt; line-height: 19px; font-family: helvetica, arial, sans-serif;\" data-ccp-props=\"{&quot;134233117&quot;:true,&quot;201341983&quot;:2,&quot;335559739&quot;:159,&quot;335559740&quot;:211}\">Thanks,</span></p>\r\n<p class=\"Paragraph SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow-wrap: break-word; vertical-align: baseline; background-color: transparent; color: windowtext;\"><span class=\"EOP SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-size: 10pt; line-height: 19px; font-family: helvetica, arial, sans-serif;\" data-ccp-props=\"{&quot;134233117&quot;:true,&quot;201341983&quot;:2,&quot;335559739&quot;:159,&quot;335559740&quot;:211}\">The Trust Box team</span></p>\r\n<p class=\"Paragraph SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow-wrap: break-word; vertical-align: baseline; background-color: transparent; color: windowtext;\">&nbsp;</p>\r\n<p class=\"Paragraph SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow-wrap: break-word; vertical-align: baseline; background-color: transparent; color: windowtext;\"><span style=\"font-family: helvetica, arial, sans-serif; font-size: 10pt;\"><strong><span class=\"EOP SCXW51560696 BCX0\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; line-height: 19px; color: #2ab6b9;\" data-ccp-props=\"{&quot;134233117&quot;:true,&quot;201341983&quot;:2,&quot;335559739&quot;:159,&quot;335559740&quot;:211}\"><span class=\"TextRun  BCX0 SCXW258724823\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"none\"><span class=\"NormalTextRun  BCX0 SCXW258724823\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">P.S. We would be very grateful if you could spare 2 minutes to support our <a href=\"https://yourtrustbox.co.uk/petition\"><span style=\"text-decoration: underline;\">petition</span></a></span></span><span class=\"TextRun  BCX0 SCXW258724823\" lang=\"EN-GB\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-kerning: none; line-height: 20.2667px; font-variant-ligatures: none !important;\" xml:lang=\"EN-GB\" data-contrast=\"none\"><span class=\"NormalTextRun  BCX0 SCXW258724823\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; background-color: inherit;\" data-ccp-parastyle=\"Normal (Web)\">.</span></span><span class=\"EOP  BCX0 SCXW258724823\" style=\"margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; line-height: 20.2667px;\" data-ccp-props=\"{&quot;134233117&quot;:true,&quot;134233118&quot;:true,&quot;201341983&quot;:0,&quot;335559739&quot;:159,&quot;335559740&quot;:256}\">&nbsp;</span></span></strong></span></p>\r\n</div>', '<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Hi <strong>@name@</strong></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\"><strong>@user@</strong> has requested that you be removed from their list of &lsquo;gifted members&rsquo;. We&rsquo;re sorry to see you go!</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">But fear not. You don&rsquo;t need to lose access to all your amazing TrustBox benefits, we can set you up your own personal TrustBox account.<!-- I’m not sure whether this will be the exact placeholder but you can add this in as a placeholder and we can discuss with Claire/Simon. --></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Simply contact our customer service team by <span style=\"color: #0563c1;\"><u><a href=\"mailto:help@trustbox.com\">help@trustbox.com</a></u></span> or call us directly on 0345 548 1230.<!-- Contact details tbc --></p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">Thanks,</p>\r\n<p style=\"margin-bottom: 0.28cm; line-height: 108%;\">The TrustBox team</p>', '<p>Hi,&nbsp;<span style=\"font-size: 16px; color: #2ab6b9; font-family: arial, helvetica, sans-serif;\">@provider@</span></p>\r\n<p>&nbsp;</p>\r\n<p><strong>@name@</strong>&nbsp;<span style=\"font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Open Sans\', \'Helvetica Neue\', sans-serif;\">has shown interest in the&nbsp;</span><span style=\"color: #2ab6b9; font-family: arial, helvetica, sans-serif; font-size: 16px;\">@benefit_name@&nbsp;</span><span style=\"font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Open Sans\', \'Helvetica Neue\', sans-serif;\">benefit.</span></p>\r\n<p>&nbsp;</p>\r\n<p>Customer Number: @customer_number@</p>\r\n<p>Customer Name: @name@</p>\r\n<p>Email Address: @email@</p>\r\n<p>Phone Number: @telephone@</p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"text-decoration: underline;\"><strong><span style=\"font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Open Sans\', \'Helvetica Neue\', sans-serif;\">Customer\'s Message</span></strong></span></p>\r\n<p>@enquiry@</p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Open Sans\', \'Helvetica Neue\', sans-serif;\">If you need help, or you have any other questions, feel free to email&nbsp;<span style=\"color: #0563c1;\"><u><a href=\"mailto:help@trustbox.com\">help@trustbox.com</a></u></span>&nbsp;or call our customer service team on 0345 548 1230.</span></p>\r\n<p>Thanks,</p>\r\n<p>The TrustBox team</p>', '<p>Hi,&nbsp;<span style=\"font-size: 16px; color: #2ab6b9; font-family: arial, helvetica, sans-serif;\">@provider@</span></p>\r\n<p>&nbsp;</p>\r\n<p><strong>@name@</strong>&nbsp;<span style=\"font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Open Sans\', \'Helvetica Neue\', sans-serif;\">has shown interest in the&nbsp;</span><span style=\"color: #2ab6b9; font-family: arial, helvetica, sans-serif; font-size: 16px;\">@benefit_name@&nbsp;</span><span style=\"font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Open Sans\', \'Helvetica Neue\', sans-serif;\">benefit.</span></p>\r\n<p><span style=\"font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Open Sans\', \'Helvetica Neue\', sans-serif;\">@customer_number@</span></p>\r\n<p>@email@</p>\r\n<p>@telephone@</p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Open Sans\', \'Helvetica Neue\', sans-serif;\">You should expect a call to speak more in-depth about this benefit.</span></p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Open Sans\', \'Helvetica Neue\', sans-serif;\">If you need help, or you have any other questions, feel free to email&nbsp;<span style=\"color: #0563c1;\"><u><a href=\"mailto:help@trustbox.com\">help@trustbox.com</a></u></span>&nbsp;or call our customer service team on 0345 548 1230.</span></p>\r\n<p>Thanks,</p>\r\n<p>The TrustBox team</p>', 5, 10, 5, 4, 2, 4, 'filemanager', '2020-04-29 11:23:29', '2021-01-20 11:48:01');

-- --------------------------------------------------------

--
-- Table structure for table `sms_verifications`
--

CREATE TABLE `sms_verifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `contact_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sms_verifications`
--

INSERT INTO `sms_verifications` (`id`, `contact_number`, `code`, `status`, `created_at`, `updated_at`) VALUES
(28, '7900268656', '6562', 'verified', '2020-05-11 16:37:21', '2020-05-11 16:37:30'),
(29, '7900268656', '1926', 'verified', '2020-05-11 17:34:07', '2020-05-11 17:34:15'),
(30, '07412693001', '6054', 'verified', '2020-05-12 14:46:04', '2020-05-12 14:46:23'),
(31, '07412693001', '9166', 'verified', '2020-05-12 15:17:16', '2020-05-12 15:17:44'),
(32, '07412693001', '4834', 'verified', '2020-05-13 15:19:27', '2020-05-13 15:20:02'),
(33, '07412693001', '7168', 'verified', '2020-05-14 15:04:17', '2020-05-14 15:04:26'),
(34, '07412693001', '7209', 'verified', '2020-05-14 15:10:25', '2020-05-14 15:10:32'),
(35, '07412693001', '4012', 'verified', '2020-05-14 15:15:30', '2020-05-14 15:15:38'),
(36, '07412693001', '1114', 'verified', '2020-05-14 15:22:00', '2020-05-14 15:22:07'),
(37, '07412693001', '2626', 'verified', '2020-05-14 15:27:21', '2020-05-14 15:27:43'),
(38, '07412693001', '6509', 'verified', '2020-05-15 07:42:43', '2020-05-15 07:43:51'),
(39, '07412693001', '7859', 'verified', '2020-05-19 10:22:00', '2020-05-19 10:22:14'),
(40, '7814187847', '2208', 'verified', '2020-05-19 10:31:53', '2020-05-19 10:31:57'),
(41, '07412693001', '1549', 'verified', '2020-05-19 11:41:36', '2020-05-19 11:42:06'),
(42, '7814187847', '1050', 'verified', '2020-05-19 11:47:54', '2020-05-19 11:47:59'),
(43, '7814187847', '8930', 'verified', '2020-05-19 11:57:46', '2020-05-19 11:57:50'),
(44, '7814187847', '2522', 'verified', '2020-05-19 11:59:32', '2020-05-19 11:59:36'),
(45, '7814187847', '1146', 'verified', '2020-05-19 13:33:18', '2020-05-19 13:33:21'),
(46, '7814187847', '6294', 'verified', '2020-05-20 05:49:00', '2020-05-20 05:49:06'),
(47, '7814187847', '1330', 'verified', '2020-05-20 07:46:10', '2020-05-20 07:46:15'),
(48, '7814187847', '4835', 'verified', '2020-05-20 08:10:15', '2020-05-20 08:10:18'),
(49, '7814187847', '6848', 'verified', '2020-05-20 09:26:20', '2020-05-20 09:26:24'),
(50, '7814187847', '4250', 'verified', '2020-05-20 10:18:59', '2020-05-20 10:19:02'),
(51, '7814187847', '8227', 'verified', '2020-05-20 11:41:47', '2020-05-20 11:41:51'),
(52, '7814187847', '5189', 'verified', '2020-05-20 12:02:25', '2020-05-20 12:02:28'),
(53, '7814187847', '6874', 'verified', '2020-05-20 14:57:34', '2020-05-20 14:57:38'),
(54, '07412693001', '6824', 'verified', '2020-05-20 15:04:53', '2020-05-20 15:05:10'),
(55, '7814187847', '1489', 'verified', '2020-05-21 07:48:52', '2020-05-21 07:48:56'),
(56, '07412693001', '5630', 'verified', '2020-05-21 09:51:48', '2020-05-21 09:52:25'),
(57, '7814187847', '9481', 'verified', '2020-05-21 10:05:24', '2020-05-21 10:05:28'),
(58, '07412693001', '4295', 'verified', '2020-05-21 10:57:37', '2020-05-21 10:57:54'),
(59, '07412693001', '7057', 'verified', '2020-05-21 12:06:14', '2020-05-21 12:06:26'),
(60, '7814187847', '2437', 'verified', '2020-05-21 12:36:19', '2020-05-21 12:36:22'),
(61, '7814187847', '6850', 'verified', '2020-05-22 09:16:20', '2020-05-22 09:16:24'),
(62, '7814187847', '1853', 'verified', '2020-05-22 11:38:20', '2020-05-22 11:38:23'),
(63, '7814187847', '3270', 'verified', '2020-05-22 12:42:13', '2020-05-22 12:42:56'),
(64, '07412693001', '3003', 'verified', '2020-05-26 07:58:54', '2020-05-26 07:59:05'),
(65, '07412693001', '5893', 'pending', '2020-05-26 13:40:30', '2020-05-26 13:40:30'),
(66, '07412693001', '6850', 'verified', '2020-05-26 13:41:07', '2020-05-26 13:41:15'),
(67, '7814187847', '2427', 'verified', '2020-05-29 14:06:03', '2020-05-29 14:06:09'),
(68, '7814187847', '6097', 'verified', '2020-06-01 09:16:27', '2020-06-01 09:16:30'),
(69, '7900268656', '7270', 'verified', '2020-06-04 07:25:49', '2020-06-04 07:26:02'),
(70, '7814187847', '4651', 'verified', '2020-06-09 08:52:51', '2020-06-09 08:52:57'),
(71, '7814187847', '3980', 'verified', '2020-06-10 16:43:11', '2020-06-10 16:43:45'),
(72, '7900268656', '7140', 'verified', '2020-06-10 17:30:05', '2020-06-10 17:30:33'),
(73, '7900268656', '3325', 'verified', '2020-06-11 08:17:21', '2020-06-11 08:17:31'),
(74, '7900268656', '4362', 'verified', '2020-06-11 11:33:59', '2020-06-11 11:34:12'),
(75, '7900268656', '5549', 'verified', '2020-06-11 15:16:44', '2020-06-11 15:19:06'),
(76, '7814187847', '1950', 'verified', '2020-06-12 07:33:08', '2020-06-12 07:33:13'),
(77, '7900268656', '5503', 'verified', '2020-06-12 08:06:03', '2020-06-12 08:06:14'),
(78, '7900268656', '6756', 'verified', '2020-06-12 08:14:26', '2020-06-12 08:14:34'),
(79, '7900268656', '9195', 'verified', '2020-06-12 08:15:32', '2020-06-12 08:15:45'),
(80, '7900268656', '7706', 'verified', '2020-06-12 08:17:30', '2020-06-12 08:17:37'),
(81, '7900268656', '8902', 'verified', '2020-06-12 08:19:20', '2020-06-12 08:19:30'),
(82, '7900268656', '9984', 'verified', '2020-06-12 08:21:27', '2020-06-12 08:21:38'),
(83, '7900268656', '3660', 'verified', '2020-06-12 08:24:10', '2020-06-12 08:24:18'),
(84, '7900268656', '4943', 'verified', '2020-06-12 08:39:35', '2020-06-12 08:39:49'),
(85, '07412693001', '8071', 'verified', '2020-06-14 15:44:50', '2020-06-14 15:46:33'),
(86, '7814187847', '9655', 'verified', '2020-06-15 10:18:08', '2020-06-15 10:18:19'),
(87, '7814187847', '1751', 'verified', '2020-06-16 09:20:00', '2020-06-16 09:20:16'),
(88, '7814187847', '3744', 'verified', '2020-06-16 11:53:45', '2020-06-16 11:53:51'),
(89, '7814187847', '3579', 'verified', '2020-06-16 12:01:08', '2020-06-16 12:01:12'),
(90, '07412693001', '2205', 'verified', '2020-06-16 12:25:50', '2020-06-16 12:25:59'),
(91, '07412693001', '5744', 'verified', '2020-06-16 12:29:54', '2020-06-16 12:30:01'),
(92, '7814187847', '6965', 'verified', '2020-06-16 16:05:05', '2020-06-16 16:05:09'),
(93, '7900268656', '2322', 'verified', '2020-06-17 08:50:12', '2020-06-17 08:50:25'),
(94, '7900268656', '2918', 'verified', '2020-06-17 09:17:39', '2020-06-17 09:17:53'),
(95, '07412693001', '8150', 'verified', '2020-06-17 11:03:00', '2020-06-17 11:03:08'),
(96, '7814187847', '7335', 'verified', '2020-06-30 15:22:32', '2020-06-30 15:23:02'),
(97, '7814187847', '2033', 'verified', '2020-07-08 11:55:23', '2020-07-08 11:55:47'),
(98, '7814187847', '8901', 'verified', '2020-07-08 12:01:09', '2020-07-08 12:01:35'),
(99, '7814187847', '8139', 'verified', '2020-07-09 12:18:28', '2020-07-09 12:18:31'),
(100, '07412693001', '3176', 'verified', '2020-07-15 07:45:35', '2020-07-15 07:45:51'),
(101, '07412693001', '7300', 'verified', '2020-07-15 08:31:38', '2020-07-15 08:31:58'),
(102, '07412693001', '9714', 'verified', '2020-07-15 08:34:02', '2020-07-15 08:34:15'),
(103, '07412693001', '8309', 'verified', '2020-07-21 12:56:24', '2020-07-21 12:56:45'),
(104, '7464948863', '6931', 'verified', '2020-07-21 12:57:40', '2020-07-21 12:57:47'),
(105, '7464948863', '9368', 'verified', '2020-07-21 12:59:40', '2020-07-21 12:59:59'),
(106, '7464948863', '1619', 'verified', '2020-07-21 13:01:20', '2020-07-21 13:01:26'),
(107, '7464948863', '5061', 'verified', '2020-07-21 13:01:44', '2020-07-21 13:01:52'),
(108, '7464948863', '3713', 'verified', '2020-07-21 13:25:37', '2020-07-21 13:25:44'),
(109, '7464948863', '7514', 'pending', '2020-07-22 07:18:47', '2020-07-22 07:18:47'),
(110, '7464948863', '8217', 'verified', '2020-07-22 07:44:15', '2020-07-22 07:44:22'),
(111, '7464948863', '2968', 'verified', '2020-07-22 07:57:21', '2020-07-22 07:57:28'),
(112, '7464948863', '1964', 'verified', '2020-07-22 08:18:12', '2020-07-22 08:18:19'),
(113, '7464948863', '6631', 'verified', '2020-07-22 12:11:30', '2020-07-22 12:11:41'),
(114, '7464948863', '8589', 'verified', '2020-07-22 13:58:02', '2020-07-22 13:58:14'),
(115, '7464948863', '6825', 'verified', '2020-07-23 13:24:20', '2020-07-23 13:24:28'),
(116, '7464948863', '6785', 'verified', '2020-07-23 13:41:21', '2020-07-23 13:41:29'),
(117, '7464948863', '7106', 'pending', '2020-07-23 17:32:45', '2020-07-23 17:32:45'),
(118, '7464948863', '4259', 'verified', '2020-07-24 07:05:53', '2020-07-24 07:06:00'),
(119, '7464948863', '5367', 'verified', '2020-07-24 10:26:21', '2020-07-24 10:26:29'),
(120, '7464948863', '2675', 'verified', '2020-07-24 10:29:10', '2020-07-24 10:29:16'),
(121, '7464948863', '9694', 'verified', '2020-07-24 10:40:47', '2020-07-24 10:40:54'),
(122, '7464948863', '1572', 'verified', '2020-07-24 12:55:16', '2020-07-24 12:55:22'),
(123, '7464948863', '1150', 'verified', '2020-07-27 12:01:03', '2020-07-27 12:01:11'),
(124, '7464948863', '7788', 'verified', '2020-07-28 08:04:12', '2020-07-28 08:04:18'),
(125, '07412693001', '9876', 'verified', '2020-07-28 08:22:57', '2020-07-28 08:23:10'),
(126, '7464948863', '4914', 'verified', '2020-07-28 08:22:59', '2020-07-28 08:23:07'),
(127, '07412693001', '5528', 'verified', '2020-07-28 08:24:41', '2020-07-28 08:24:50'),
(128, '7464948863', '4831', 'verified', '2020-07-28 13:12:25', '2020-07-28 13:12:31'),
(129, '7464948863', '6550', 'verified', '2020-07-30 07:00:53', '2020-07-30 07:00:58'),
(130, '7464948863', '8009', 'verified', '2020-07-30 10:35:26', '2020-07-30 10:35:35'),
(131, '7464948863', '4367', 'verified', '2020-07-31 12:59:58', '2020-07-31 13:00:07'),
(132, '7464948863', '1034', 'verified', '2020-08-03 10:55:18', '2020-08-03 10:55:24'),
(133, '7464948863', '4831', 'verified', '2020-08-03 12:35:05', '2020-08-03 12:35:12'),
(134, '7464948863', '3291', 'verified', '2020-08-04 07:25:18', '2020-08-04 07:25:25'),
(135, '7464948863', '2426', 'verified', '2020-08-04 12:38:31', '2020-08-04 12:38:38'),
(136, '7464948863', '8558', 'verified', '2020-08-07 13:09:14', '2020-08-07 13:09:28'),
(137, '7464948863', '6297', 'verified', '2020-08-07 13:18:12', '2020-08-07 13:18:20'),
(138, '7464948863', '2315', 'verified', '2020-08-07 14:02:04', '2020-08-07 14:02:09'),
(139, '7464948863', '1538', 'verified', '2020-08-07 14:11:48', '2020-08-07 14:11:54'),
(140, '7464948863', '4708', 'verified', '2020-08-07 14:15:43', '2020-08-07 14:15:49'),
(141, '07412693001', '2792', 'verified', '2020-08-11 13:35:43', '2020-08-11 13:35:54'),
(142, '07412693001', '5767', 'verified', '2020-08-12 08:04:50', '2020-08-12 08:05:09'),
(143, '7464948863', '5949', 'verified', '2020-08-14 10:22:33', '2020-08-14 10:22:41'),
(144, '7464948863', '8030', 'verified', '2020-08-16 12:03:42', '2020-08-16 12:03:51'),
(145, '07412693001', '9641', 'verified', '2020-08-17 11:37:39', '2020-08-17 11:37:51'),
(146, '07412693001', '8825', 'verified', '2020-08-18 13:33:28', '2020-08-18 13:33:47'),
(147, '7814187847', '6269', 'verified', '2020-08-19 13:49:42', '2020-08-19 13:49:47'),
(148, '7464948863', '1259', 'verified', '2020-08-20 10:04:46', '2020-08-20 10:04:50'),
(149, '7464948863', '7318', 'verified', '2020-08-26 11:58:56', '2020-08-26 11:59:23'),
(150, '7464948863', '9429', 'verified', '2020-08-27 08:42:07', '2020-08-27 08:44:02'),
(151, '7464948863', '3456', 'pending', '2020-08-27 10:45:16', '2020-08-27 10:45:16'),
(152, '7464948863', '8324', 'pending', '2020-09-01 10:43:18', '2020-09-01 10:43:18'),
(153, '07412693001', '3675', 'pending', '2020-09-01 10:43:48', '2020-09-01 10:43:48'),
(154, '7464948863', '8430', 'verified', '2020-09-01 10:45:13', '2020-09-01 10:45:27'),
(155, '7814187847', '7031', 'pending', '2020-09-11 13:07:25', '2020-09-11 13:07:25'),
(156, '7464948863', '9836', 'pending', '2020-09-23 13:31:17', '2020-09-23 13:31:17'),
(157, '07464948863', '1362', 'verified', '2020-09-28 09:33:05', '2020-09-28 09:33:19'),
(158, '07464948863', '4929', 'verified', '2020-09-28 09:45:08', '2020-09-28 09:45:19'),
(159, '7814187847', '8463', 'pending', '2020-09-28 10:14:14', '2020-09-28 10:14:14'),
(160, '7814187847', '1678', 'pending', '2020-09-28 10:14:31', '2020-09-28 10:14:31'),
(161, '7814187847', '4526', 'pending', '2020-09-28 10:14:34', '2020-09-28 10:14:34'),
(162, '7814187847', '7683', 'pending', '2020-09-28 10:14:44', '2020-09-28 10:14:44'),
(163, '07814187847', '7242', 'pending', '2020-09-28 11:08:27', '2020-09-28 11:08:27'),
(164, '07412693001', '3486', 'pending', '2020-09-28 11:13:31', '2020-09-28 11:13:31'),
(165, '07814187847', '3917', 'pending', '2020-09-28 11:14:23', '2020-09-28 11:14:23'),
(166, '07814187847', '9363', 'pending', '2020-09-28 11:17:20', '2020-09-28 11:17:20'),
(167, '07412693001', '1017', 'pending', '2020-09-28 11:32:29', '2020-09-28 11:32:29'),
(168, '07412693001', '7017', 'pending', '2020-09-28 11:32:37', '2020-09-28 11:32:37'),
(169, '07412693001', '8102', 'pending', '2020-09-28 11:32:50', '2020-09-28 11:32:50'),
(170, '7814187847', '2890', 'pending', '2020-09-28 12:33:20', '2020-09-28 12:33:20');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `schedule_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `user_id`, `schedule_id`, `status`, `total`, `created_at`, `updated_at`) VALUES
(1, 139, NULL, 'Subscription Active', '10', '2020-09-16 09:37:30', '2020-09-16 09:38:08'),
(2, 134, NULL, NULL, NULL, '2020-09-18 10:09:23', '2020-09-18 10:09:23'),
(3, 146, NULL, 'Subscription Pending', '10', '2020-10-08 12:11:11', '2020-10-08 12:11:11'),
(6, 158, 'TT-56853eb08b39423c8fc0f3fb1c0d445c', 'Subscription Active', '10', '2020-11-05 11:34:39', '2020-11-05 11:34:39'),
(7, 158, 'TT-44774f8b83644722a05bc4f30e0ce60a', 'Subscription Active', '10', '2020-11-05 11:36:33', '2020-11-05 11:36:33'),
(10, 155, 'TT-18d1a925ac594efc88f145b161d29666', 'Subscription Active', '23', '2020-11-05 11:53:04', '2020-11-05 11:57:31'),
(11, 150, 'TT-9a481f21766f46fdbc9a7d63d866594c', 'Subscription Active', '20', '2020-11-05 11:59:49', '2020-11-05 11:59:59'),
(12, 156, 'TT-e27431a7221549c4942c8274e6d51499', 'Subscription Active', '10', '2020-11-05 12:02:02', '2020-11-06 15:34:19'),
(13, 160, 'TT-9f12c343c70d412699e5aaa2f9fb425b', 'Subscription Active', '10', '2020-11-05 13:04:54', '2020-11-05 13:04:54'),
(15, 166, 'TT-6791a9b9f1de444abfbb3a0343838f65', 'Subscription Active', '5', '2020-11-06 11:55:39', '2020-11-06 11:55:39'),
(22, 167, 'TT-7c74e709d4cd4e9f80aa627987524ba3', 'Subscription Active', '5', '2020-11-10 10:22:18', '2020-11-10 11:00:47'),
(23, 171, NULL, 'Subscription Pending', '5', '2020-11-10 12:07:33', '2020-11-10 12:07:33'),
(34, 200, 'TT-e3e4508cb7c24088ad58255bce22c649', 'Subscription Active', '30', '2021-01-20 09:57:24', '2021-01-20 09:59:53'),
(35, 199, 'TT-6ddbd732538d432ca3f9ad5c8ec69221', 'Subscription Active', '10', '2021-02-22 09:18:10', '2021-02-22 09:18:49');

-- --------------------------------------------------------

--
-- Table structure for table `trusted_users`
--

CREATE TABLE `trusted_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `trusted_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trusted_users`
--

INSERT INTO `trusted_users` (`id`, `user_id`, `trusted_id`, `active`, `created_at`, `updated_at`) VALUES
(1, 162, 162, 1, NULL, NULL),
(2, 163, 156, 1, NULL, NULL),
(3, 176, 190, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `has_boosted` tinyint(1) NOT NULL DEFAULT '0',
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_birth` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `change_password` tinyint(1) NOT NULL DEFAULT '0',
  `referred` int(11) DEFAULT NULL,
  `letter_printed` tinyint(1) NOT NULL DEFAULT '0',
  `storage` int(11) NOT NULL DEFAULT '0',
  `storage_used` int(11) NOT NULL DEFAULT '0',
  `storage_upgraded` tinyint(1) NOT NULL DEFAULT '0',
  `file_manager_trial_date` timestamp NULL DEFAULT NULL,
  `deceased` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `marketing_consent` tinyint(1) DEFAULT '1',
  `subscription_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `role_id`, `has_boosted`, `first_name`, `last_name`, `address1`, `address2`, `city`, `postcode`, `telephone`, `mobile`, `customer_number`, `date_of_birth`, `change_password`, `referred`, `letter_printed`, `storage`, `storage_used`, `storage_upgraded`, `file_manager_trial_date`, `deceased`, `deleted_at`, `marketing_consent`, `subscription_id`) VALUES
(1, 'Alan Olson PhD', 'admin@trustbox.com', '2020-04-29 11:23:29', '$2y$10$MffJA0x5SxEiTwU0GuGbb.ZaOeH3jIs0HC5noTvarh0/a7d9jnKzO', 'Dhtg8hRGkXIWBvi2SR18Khc7nlxOUA5VP9nA5m64nZcfgpN0Hwhe62ZflCOs', '2020-04-29 11:23:29', '2020-07-15 08:32:55', 2, 0, 'Vivianee', 'Christiansen', '9', 'Russel Tunnel', 'Chesleyberg', '87807-8867', '889-759-1244 x540', NULL, 'C692592905', '2015-11-06', 0, NULL, 0, 5, 0, 0, NULL, 0, NULL, 1, 0),
(11, 'Claire Sadler', 'claire@trustbox.com', NULL, '$2y$10$mO9KF9cJroMysjpuXapgD.e6EWqBsp23hbPVwROBXa9K7p.rwTcVq', NULL, '2020-04-30 13:32:00', '2020-05-01 12:08:10', 2, 0, 'Claire', 'Sadler', 'the stables, paradise wharf, ducie street', NULL, 'manchester', 'm12jn', '07412693001', '7900268656', 'C437816988', '2020-12-30', 1, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(15, 'Claire Sadler', 'claire@lsgi.co.uk', NULL, '$2y$10$O1208wjFKUSQux6JIlwqO.mydE14PGYTzevILS9Mis0kMaWlS5M4W', NULL, '2020-05-07 10:30:45', '2020-05-07 10:30:45', 5, 0, 'Claire', 'Sadler', '5 Millbank House', 'Riverside Business Park', 'Wilmslow', 'SK9 1BJ', '0345 548 1230', NULL, 'C802461464', '1988-07-03', 1, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(23, 'Adam Johnson', 'adam@visionsharp.co.uk', NULL, '$2y$10$6dTnORXCqT1lfTse7a847OkPN2f73Og/sVOgOi07wq379sk35361.', NULL, '2020-05-11 15:37:10', '2020-05-11 15:37:10', 2, 0, 'Adam', 'Johnson', 'The Stables, Paradise Wharf', 'Ducie Street', 'Manchester', 'M1 2JN', '07412693001', '07412693001', 'C477133117', '1993-01-04', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(24, 'Claire Sadler', 'sadler.claire@gmail.com', NULL, '$2y$10$8WKB/Hqzk77BYIAegt8uvu851fKufMh1s0s0CKTV4spX/e5.f4vES', NULL, '2020-05-11 16:26:33', '2020-05-11 16:26:33', 1, 0, 'Claire', 'Sadler', '11 Hazel Avenue', NULL, 'Sale', 'M33 3DY', '07900 268 656', NULL, 'C501513907', '1988-07-03', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(25, 'Claire Sadler', 'kay19891@yahoo.co.uk', NULL, '$2y$10$LT73HeOS7Ihw4jMP8IQkC.Ln.sdS6N/EYlFPpcWijXsHnGaXz4VR.', NULL, '2020-05-11 16:44:28', '2020-05-11 16:44:28', 1, 0, 'Claire', 'Sadler', '11 Hazel Ave', NULL, 'Sale', 'M33 3DY', '07900268656', NULL, 'C671859658', '1988-07-03', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(26, 'Kathryn Stockdale', 'kathryn.e.stockdale@gmail.com', NULL, '$2y$10$PkBKSqBbFoLFPhnVsqXyueXfdh0Lj6bLgi.fpaA2M.D4WKrYIEu1e', NULL, '2020-05-11 16:48:19', '2020-05-11 16:48:19', 1, 0, 'Kathryn', 'Stockdale', '11 Hazel Ave', NULL, 'Sale', 'M33 3DY', '07900268656', NULL, 'C661326334', '2002-01-01', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(31, 'Adam Johnson', 'adijono11@hotmail.com', NULL, '$2y$10$9V3navvsyzpbO5rTeCjvC.5fnsub5jvRdizX7.eZkAgSVhLaM.gi2', NULL, '2020-05-12 15:47:20', '2020-07-15 08:10:40', 5, 0, 'Adam', 'Johnson', '119 Denbydale Way', NULL, 'Oldham', 'OL2 5UH', '07412693001', NULL, 'C727990052', '1993-01-04', 0, 23, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(33, 'Beth Hadfield', 'beth.hadfield89@gmail.com', NULL, '$2y$10$KCVCLfxM4ZCl4B0YjAJ05epqkHUdKJ8mwD1SycyaCKfc7NfMzhali', NULL, '2020-05-13 07:38:28', '2020-05-13 07:38:28', 1, 0, 'Beth', 'Hadfield', '18 Toll Bar Road', NULL, 'Macclesfield', 'SK11 8TY', '07704921055', NULL, 'C115673880', '1989-09-30', 0, 32, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(34, 'Lewis Stanton', 'lewis@visionsharp.co.uk', NULL, '$2y$10$qcbo.OtSQymuzv03Yk4G9ehYregPNaQCtQySDPFDTTsrEdU2djkpu', NULL, '2020-05-13 08:15:29', '2020-11-06 17:18:15', 1, 1, 'Lewis', 'Stanton', 'The Stables', NULL, 'Manchester', 'm12jn', '07572 398259', NULL, 'C794904118', '1990-10-12', 0, NULL, 0, 5, 0, 0, '2020-11-05 17:16:54', 0, NULL, 1, 0),
(41, 'hello visionsharp', 'hello@visionsharp.co.uk', NULL, '$2y$10$nbaeDxvU/FtEWGvcmHvB6erojVMaOGMgGTm0leDGzax7aahbN7RcC', NULL, '2020-05-14 07:59:29', '2020-07-21 13:27:45', 4, 0, 'hello', 'visionsharp', 'Visionsharp, The stables', 'Paradise Wharf, Ducie Street', 'manchester', 'M1 2JN', '07412693001', NULL, 'C119562586', '2002-01-01', 0, 41, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(42, 'adam johnson', 'adijono11@gmail.com', NULL, '$2y$10$SFqajZd5fBEpkz9xzEP2EuKCKK5KHMtdo4nDZYwt60kyaUGLM5mDK', NULL, '2020-05-14 14:57:42', '2020-05-14 14:57:42', 1, 0, 'adam', 'johnson', 'Visionsharp, The stables', 'Paradise Wharf, Ducie Street', 'manchester', 'M1 2JN', '07412693001', NULL, 'C300182587', '2001-01-01', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(43, 'adam johnson', 'social@visionsharp.co.uk', NULL, '$2y$10$7dZzkn9kJcqDvWcdMK6rV.R4zOyLCexmVnLnxq/WTZOf9WUVNG.yq', NULL, '2020-05-14 15:08:34', '2020-05-14 15:08:34', 1, 0, 'adam', 'johnson', 'Visionsharp, The stables', 'Paradise Wharf, Ducie Street', 'manchester', 'M1 2JN', '07412693001', NULL, 'C235199574', '2002-01-02', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(60, 'Lewis Stanton', 'draconic619@gmail.com', NULL, '$2y$10$GHZ1kNVX7OybiggCavqsMOYqvmu6LaqUVRZX92XaOhCFZIwQkE8OK', NULL, '2020-05-15 15:11:02', '2020-05-15 15:11:02', 1, 0, 'Lewis', 'Stanton', 'The Stables', NULL, 'Manchester', 'm12jn', '07572 398259', NULL, 'C725520201', '1990-10-12', 0, 34, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(61, 'Adam Johnson', 'testing@visionsharp.co.uk', NULL, '$2y$10$86JezCZjrCpy5RqMwGkUeOz.BJvKU5yTcEwhqCEW6k/fQoD3WwcD6', NULL, '2020-05-15 15:17:29', '2020-05-15 15:17:29', 1, 0, 'Adam', 'Johnson', 'The stables, Paradise Wharf, Ducie Street', 'test', 'Manchester', 'M12JN', '07412693001', NULL, 'C165675260', '2002-01-01', 0, 23, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(62, 'Patrick Harrison', 'tpharrison69@gmail.com', NULL, '$2y$10$pMafRzc8umMFLMsWuPlLy.LDLOzaQvNQuoU9wwSadmcZ8FNh8dTba', 'kH8Q8R1RfnkvQlZqJiC6qvudrilA8CSaGKgQtBGjelwxl4N4upsheFz5kAZH', '2020-05-18 11:59:30', '2021-02-02 11:26:37', 1, 0, 'Patrick', 'Harrison', '13 Plymouth Road', NULL, 'Tavistock', 'PL19 8AU', '07932155182', NULL, 'C150514652', '1969-04-03', 0, NULL, 0, 5, 0, 0, '2021-03-05 11:26:37', 0, NULL, 1, 0),
(63, 'Dominic Hadfield', 'dominic@trustboxltd.com', NULL, '$2y$10$OQHfOxwXDGSyrXHlU.k1leA43C.VV5Kvq2OW3Q7iEITFgdwx7NqZ2', NULL, '2020-05-18 13:03:29', '2020-05-19 10:24:25', 2, 0, 'Dominic', 'Hadfield', '5 Millbank House', 'Riverside Business Park', 'Wilmslow', 'SK9 1BJ', '07814187847', '7814187847', 'C535360175', '1989-08-05', 0, 63, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(64, 'Geoff Iveson', 'ghi@asisvc.uk', NULL, '$2y$10$fccLxlhmueVFV0uo8MwKvu1GqastG4NzIwnedEW4/lvYcdF6808va', NULL, '2020-05-19 08:41:12', '2020-05-19 08:43:42', 1, 0, 'Geoff', 'Iveson', 'Mereside', 'Jacobs Way', 'Knutsford', 'WA16 0GZ', '07831272222', NULL, 'C733031891', '1948-06-21', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(67, 'Elaine Iveson', 'eli@asisvc.uk', NULL, '$2y$10$aUnRapn84HEA8rpBa9ZhI.7ZBaTebdL5HraBx3AttBe5xSDaV.Xea', NULL, '2020-05-19 11:55:49', '2020-05-19 11:55:49', 1, 0, 'Elaine', 'Iveson', 'Mereside', '11 Jacobs Way, Pickmere', 'Knutsford', 'WA16 0GZ', '07831855585', NULL, 'C713294727', '1951-02-22', 0, 64, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(71, 'Jessica Hackett', 'Jess.Hackett2912@gmail.com', NULL, '$2y$10$ebW897MhiLtvuPg/cuKGA.1QunmJL5TsXvZQASygLf15Anqjo.uYG', NULL, '2020-05-19 13:26:29', '2020-05-19 13:26:29', 1, 0, 'Jessica', 'Hackett', '64 Bath Vale', NULL, 'Congleton', 'CW12 2HY', '07985603881', NULL, 'C370662923', '1989-12-29', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(72, 'Kieran Osborne', 'kieran.osborne@squiggleconsult.co.uk', NULL, '$2y$10$IdACMRJoJe3C6M.60sBJLuiQREgFs/Nuwv/DJr80s8oT1GIdTYdcW', NULL, '2020-05-19 13:29:24', '2020-05-19 13:29:24', 1, 0, 'Kieran', 'Osborne', '127 Grosvenor Road', NULL, 'Ashford', 'TN24 9PN', '07746334329', NULL, 'C357755944', '1991-06-07', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(73, 'Anthony Hayward', 'anthayward12@gmail.com', NULL, '$2y$10$d9FWOpGK2u5MGfX8yNmRROKxttJtrW09tAljDBpIMqPqhkCpdr4qa', 'Ir9ZCdxD9MzUGfNxYdGioOyFgsZ34C6GKxDO1v9Vf2j5G7TkVAoveLcqMOJU', '2020-05-19 13:31:38', '2020-05-20 14:31:16', 1, 0, 'Anthony', 'Hayward', '7 Wallworths Bank', NULL, 'Congleton', 'CW12 3DP', '07860251949', NULL, 'C423106687', '1993-01-22', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(74, 'Bal Gill', 'bal.gill@balfortlgeal.com', NULL, '$2y$10$CJqxY65Lr0HfOW40Rz.x9OnOov/fyDDJOpiOzV2rAD.E6TZSIp.7O', NULL, '2020-05-19 13:59:54', '2020-07-09 12:21:32', 5, 0, 'Bal', 'Gill', '3', 'Strathfield Gardens', 'Essex', 'IG11 9UJ', '07956819946', NULL, 'C713666511', '1990-01-17', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(75, 'Kaci Cook', 'kacicook@live.com', NULL, '$2y$10$ffxVnQXbZ0hKFgRj2Sj.W.fyCfK1NFWF25KegjQ/KMOehWjVW38xG', NULL, '2020-05-19 14:15:15', '2020-05-19 14:15:15', 1, 0, 'Kaci', 'Cook', '15 Hanson Mews', 'Stockport', 'Cheshire', 'SK1 4HS', '07585705391', NULL, 'C822949930', '2001-08-30', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(76, 'Beth Hadfield', 'bethdassoulas@gmail.com', NULL, '$2y$10$w6zHyCAobGhZc4TRPKzpH./uEZD5k/F/q/BojFLx9/LpczlgFXxam', NULL, '2020-05-19 16:09:30', '2020-05-19 16:09:30', 1, 0, 'Beth', 'Hadfield', '18 Toll Bar Road', NULL, 'Macclesfield', 'SK11 8TY', '07704921055', NULL, 'C615331427', '1989-09-30', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(77, 'Thomas Harrison', 'tah42@outlook.com', NULL, '$2y$10$Y6MOhmJmNkSAbWisdltegOQlqcEJoE8jSkgbRKTYHfgwgwvRYdYWe', NULL, '2020-05-20 08:04:03', '2020-05-20 08:04:03', 1, 0, 'Thomas', 'Harrison', 'Wulfruna Cottage Wulfruna Cottage, The Cross', 'The Cross, Child Okeford', 'Blandford Forum', 'DT11 8ED', '01258860571', NULL, 'C129340068', '1942-08-16', 0, 62, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(78, 'David Clarke', 'davidclarke101@btinternet.com', NULL, '$2y$10$ldLcbnwKik0JzNJhCRaM4uo0pI4SqRbCyqgjdUTDGZD82Fde.9f.6', NULL, '2020-05-20 08:12:10', '2020-05-20 08:12:10', 1, 0, 'David', 'Clarke', '43 Web Tree Avenue', NULL, 'Hereford', 'HR2 6HQ', '07702332095', NULL, 'C134291402', '1961-01-22', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(79, 'Jon Pidduck', 'jon.pidduck@lsgi.co.uk', NULL, '$2y$10$fuDlODnAutTPUrU4rSR75eRXC6RoXNbOWAn7N6uXLmrNcqXTTjI9y', NULL, '2020-05-20 08:59:03', '2020-05-20 08:59:03', 1, 0, 'Jon', 'Pidduck', '5 Millbank House', NULL, 'Wilmslow', 'SK9 1BJ', '0345 548 1230', NULL, 'C538548736', '1983-09-13', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(80, 'JP Outlook', 'jon.pidduck.lsgi@outlook.com', NULL, '$2y$10$2bssaeaJG96CDFpVqVB10OOXVZZt8ZVl6oY.V6QeLmw0B2pcp3JSW', NULL, '2020-05-20 09:05:56', '2020-05-20 09:05:56', 1, 0, 'JP', 'Outlook', '5 Millbank House', NULL, 'Wilmslow', 'SK9 1BJ', '03455481230', NULL, 'C887425902', '1953-09-13', 0, 79, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(81, 'molly allen', 'mollyallen21@outlook.com', NULL, '$2y$10$4Nc4n4fp5GV/nL3FDuv9NO7Egcqg/XW8gmH7iOUau8GyaUEyCaxTK', NULL, '2020-05-20 09:25:30', '2020-05-20 09:25:51', 1, 0, 'Molly', 'Allen', '21 Tatland Drive', NULL, 'Manchester', 'M22 5DQ', '07532379813', NULL, 'C384611809', '2002-01-15', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(82, 'James Johnston', 'james.johnston@lsgmember.co.uk', NULL, '$2y$10$s2h6St3NtFECQSbbAXmsWuQHa.gxjURWXdkJJud98K3b5SUw6GhBi', NULL, '2020-05-20 09:28:59', '2020-05-20 09:28:59', 1, 0, 'James', 'Johnston', '142 Packer Avenue', 'Leicester Forest East', 'Leicester', 'LE3 3QH', '07917131279', NULL, 'C212109535', '1980-02-09', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(83, 'John Smith', 'john.smith@email.com', NULL, '$2y$10$LEqXFFLs5RAvL/Aj3MUVme3GqU1SPvbLSg/WNAzjmAvCa20VwfFbm', NULL, '2020-05-20 09:33:11', '2020-05-20 09:33:11', 1, 0, 'John', 'Smith', '1 The Road', NULL, 'Cityville', 'SK9 1BJ', '01625 123456', NULL, 'C212163328', '1972-02-19', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(84, 'John Smith', 'john.smith@email.test', NULL, '$2y$10$6wES2oC4vFs0PkGPUk5gyOhu4wPf.Aj0SmrPR.SRZgjC4patwhtfe', NULL, '2020-05-20 09:34:33', '2020-05-20 09:34:33', 1, 0, 'John', 'Smith', '1 The Road', NULL, 'Cityville', 'SK10 1BJ', '01625 123456', NULL, 'C880271622', '1981-02-17', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(85, 'Nicola Harrison', 'nickyh2@hotmail.co.uk', NULL, '$2y$10$hRs4ZCrHUbe5HwjI/JluT.MnyucLdqrc4Maq6VSzJzR0h2j5QOXf.', NULL, '2020-05-20 10:04:30', '2020-05-20 10:04:30', 1, 0, 'Nicola', 'Harrison', 'Woburn Lodge', '13', 'Tavistock', 'PL19 8AU', '01822618445', NULL, 'C216556660', '1967-12-30', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(86, 'Charles Norman', 'Charlien_66@hotmail.co.uk', NULL, '$2y$10$njuTGKDP7R3xYH4VPRhcCuEvjV1JLPagAOpT/Xt6ZN0jmjBV2Zzzi', NULL, '2020-05-20 10:26:02', '2020-05-20 10:26:02', 1, 0, 'Charles', 'Norman', 'The Hayloft', 'Houndsmoor Farm', 'Milverton', 'TA4 1PU', '07575482630', NULL, 'C144979181', '1996-06-19', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(87, 'Sarah Danbury', 'sarah.danbury@lsgmember.co.uk', NULL, '$2y$10$.YVHVxpdq4PpxbfKDNC2zes3RNYHLoF4Nct7A/StUf17VuoGjb7.C', NULL, '2020-05-20 10:40:10', '2020-05-20 10:40:10', 1, 0, 'Sarah', 'Danbury', '18', 'Sandford Close, Hill Ridware', 'Rugeley', 'WS15 3RH', '07880367417', NULL, 'C510165934', '1972-05-21', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(88, 'Steve McFarlane', 'smcfarlane56@gmail.com', NULL, '$2y$10$MmkR.tu6p3WmiVJ4Zzi.o.TGmCF0hXwAHYfJS9DchOGqZNbvwxK0O', NULL, '2020-05-20 10:55:50', '2020-05-20 10:55:50', 1, 0, 'Steve', 'McFarlane', '13, Winston Drive', 'Isham', 'Kettering', 'NN141HS', '07748633418', NULL, 'C186889336', '1963-06-14', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(89, 'Helen Gregory', 'helen.gregory@lsgmember.co.uk', NULL, '$2y$10$B7OhnyvFt5Su3HbMA4QD3.B3v7rCbS369wsEA406N/hFeFGN7uYFG', 'SfmWt0BnyBvnV9HVIbZxZJxVQK5YZVDh3LoHx5nPSWyn6lHQr3y7a0XfAbbF', '2020-05-20 10:57:27', '2020-12-04 09:29:47', 1, 0, 'Helen', 'Gregory', 'Sydenham', 'Bridgwater Road', 'Lympsham', 'BS24 0JN', '07889604644', NULL, 'C918128725', '1968-06-01', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(90, 'Clare Shuttleworth-Richardson', 'clare.sr@lsgmember.co.uk', NULL, '$2y$10$STw0fogj9KrLSra4wmTm/O.sp/.SEwvMHK4PI22NxL9XaC60Y4y92', NULL, '2020-05-20 11:19:28', '2020-05-20 11:19:28', 1, 0, 'Clare', 'Shuttleworth-Richardson', '37 Barley Rise', NULL, 'York', 'YO32 5AB', '07902545030', NULL, 'C328235943', '1974-10-02', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(91, 'Roisin Mills', 'roisin.mills98@gmail.com', NULL, '$2y$10$qlhJ30EPLvyzt2RgUu13Telz3keomfg/XORUnjwBptw9WJqKC5wny', NULL, '2020-05-20 11:49:42', '2020-05-20 11:49:42', 1, 0, 'Roisin', 'Mills', '10 Altrincham Road', NULL, 'Wilmslow', 'SK9 5ND', '07568309185', NULL, 'C636630008', '1998-12-30', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(92, 'Daniel Williams', 'daniel.williams@legalservicesguild.co.uk', NULL, '$2y$10$mPQugJUThTEZ7JWJm063DO/M1oJ2Oe6PtUGhtS3Mc0OHGRq12ea2W', NULL, '2020-05-20 12:00:33', '2020-05-20 12:00:33', 1, 0, 'Daniel', 'Williams', '6 Mill Street', NULL, 'Wilmslow', 'SK9 1BA', '07547602054', NULL, 'C707751501', '1988-04-18', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(93, 'rebecca coates', 'rebeccaleadavies@hotmail.com', NULL, '$2y$10$LM7G1fkpmeo46jy5FJNeXupgq0NLBRCp/7ag0RQhaH2SSs0ct4wtu', NULL, '2020-05-20 12:21:05', '2020-05-20 12:21:05', 1, 0, 'rebecca', 'coates', '153', 'castlemilk court', 'winsfrd', 'cw7 1gj', '07474743691', NULL, 'C273692680', '1987-11-02', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(94, 'Hollie Bowcock', 'holliebow@virginmedia.com', NULL, '$2y$10$b5o4ezhft3XtGMqM.jHDle14EvoWA3HoKQH2LdSVHhcn9IlKVydTq', 'INQVL2303fdU1DuBcbkuWQJuy3LqNOhrPiMHS4VbyJVpOxGOJ9Umfq9cfbjJ', '2020-05-20 12:32:01', '2020-06-10 18:28:13', 1, 0, 'Hollie', 'Bowcock', '32 Garlick Street', 'Gorton', 'Manchester', 'M18 8UB', '07769331046', NULL, 'C347748676', '1994-12-06', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(95, 'Laura Doyle', 'laura91doyle@gmail.com', NULL, '$2y$10$CQ8Q0nPlI2qnniTyTplOV.ClnSCK6WuCTerBwGR2qO3vcF3wzS.uO', NULL, '2020-05-20 12:40:38', '2020-05-20 12:40:38', 1, 0, 'Laura', 'Doyle', '12 Foxford Walk', 'Peel Estate', 'Manchester', 'M22 5WQ', '07305017537', NULL, 'C470102298', '1991-02-25', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(96, 'Abby Newell', 'abby_newell89@hotmail.co.uk', NULL, '$2y$10$q4kOgV4G4kc.KyIXmOuKluZa7mFt5QabN1lb6zKwozW2O5z0NfM8m', NULL, '2020-05-20 13:08:57', '2020-05-20 13:08:57', 1, 0, 'Abby', 'Newell', '23 Cherry Tree Close', NULL, 'Wilmslow', 'SK9 2QF', '07767364879', NULL, 'C496972001', '1989-06-15', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(97, 'Joseph Mutch', 'joe.mutch@legalservicesguild.co.uk', NULL, '$2y$10$pjNPMS0aZsN0K6RklQrCeOluELO1qZgRcgXG9iXSDmwpEZDsE.sTm', NULL, '2020-05-20 13:21:45', '2020-05-20 13:21:45', 1, 0, 'Joseph', 'Mutch', '22 Keane Street', NULL, 'Ashton-Under-Lyne', 'OL7 9DL', '07488254547', NULL, 'C197668897', '1998-09-06', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(98, 'Joe Mutch', 'joseph7.mutch@hotmail.co.uk', NULL, '$2y$10$b0mERFu3En3Zckb5NI/5MOwgXEZqMcXewFzaFkp7lEV3zsY9kDecy', NULL, '2020-05-20 13:41:03', '2020-05-20 13:41:03', 1, 0, 'Joe', 'Mutch', '22 Keane Street', NULL, 'Ashton-Under-Lyne', 'OL7 9DL', '07488254547', NULL, 'C494857710', '1998-09-06', 0, 97, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(99, 'Susan McClure', 'susanmcclure3@gmail.com', NULL, '$2y$10$P.VowcOEFPbFgHc1f5BBuu7M0E.OLps.rS4GzVQ44eqJIXRVKx3V.', NULL, '2020-05-20 13:55:26', '2020-05-20 13:55:26', 1, 0, 'Susan', 'McClure', '29', 'Hawthorn Street', 'Wilmslow', 'SK9 5EH', '07759184689', NULL, 'C424699260', '2000-10-03', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(100, 'Bethany Gardner', 'bethanyygardnerr@gmail.com', NULL, '$2y$10$B.G7CiFrUDHl.BJ0LOfoUuPBsCmL79HgIP1mMxvWyWAIIN029Bi.2', NULL, '2020-05-20 14:12:45', '2020-05-20 14:12:45', 1, 0, 'Bethany', 'Gardner', 'LG12 AdelphiWharf 1', '11 Adelphi Street', 'Salford', 'M3 6DZ', '07895064825', NULL, 'C369746825', '1995-10-10', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(102, 'Hilary Norman', 'hilary.norman@btinternet.com', NULL, '$2y$10$ENr1dGht9aJ02kXHFb90s.kaPhkNnuD/2RNmxZqdidofCkTh1ErcC', NULL, '2020-05-20 16:17:55', '2020-05-20 16:17:55', 1, 0, 'Hilary', 'Norman', 'Shells Bungalow', 'Croford', 'Wiveliscombe', 'Ta4 2TP', '07969448793', NULL, 'C339357243', '1956-12-25', 0, 86, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(103, 'Allen Hurst', 'allenhurst@btinternet.com', NULL, '$2y$10$qf9QybEeONVD.eB64pxTmugamnSBpsCIhNjd5ngva6f04K611TJm.', NULL, '2020-05-21 06:38:16', '2020-05-21 06:38:16', 1, 0, 'Allen', 'Hurst', '58 Greenway Road', NULL, 'Taunton', 'TA2 6LE', '07740620522', NULL, 'C806853973', '1961-03-14', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(104, 'elena gough', 'elena.gough@lsgmember.co.uk', NULL, '$2y$10$eSgPskk8Om/W2yKRF172SOcf1Fx4IdxFEdsltmKVOjJXdgj5InTKO', NULL, '2020-05-21 07:07:44', '2020-05-21 07:07:44', 1, 0, 'elena', 'gough', '18a Burgh Heath Road', NULL, 'Epsom', 'KT174LS', '07956358808', NULL, 'C725250189', '1992-09-02', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(105, 'Joanna Dawson', 'joanna@jdiyorkshire.co.uk', NULL, '$2y$10$gLRQ.K0i3B98YBByXdmoB.18Rkp6wXjeiQTHgqeAk1JCo9A2HjBJu', NULL, '2020-05-21 07:25:25', '2020-05-21 07:25:25', 1, 0, 'Joanna', 'Dawson', '11 Anvil St', NULL, 'Brighouse', 'HD6 1TP', '07551230832', NULL, 'C444765908', '1975-09-26', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(106, 'Andrew Hesketh', 'andrew.hesketh@lsgmember.co.uk', NULL, '$2y$10$U10o.DusqnJDpVf4FvO4NeoYOGmXxqmrRgCHuNfJ5ekm9ja/RNGmS', NULL, '2020-05-21 07:30:48', '2020-05-21 07:30:48', 1, 0, 'Andrew', 'Hesketh', '8', 'Leeward Close', 'Lower Darwen', 'BB3 0SD', '07946 523298', NULL, 'C804065811', '1959-09-21', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(108, 'Rebecca Armitt', 'rebeccaarmitt1980@gmail.com', NULL, '$2y$10$t7FAGv1KUGUhEzwnmM/cvOPZODdM2xf64OqaIFtr4V5hXLxt0QVFS', NULL, '2020-05-21 10:42:45', '2020-05-21 10:42:45', 1, 0, 'Rebecca', 'Armitt', '42 Warbreck Grove', NULL, 'Sale', 'M332ST', '07786427744', NULL, 'C506077112', '1980-12-06', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(109, 'Suki Kaur', 'suki.kaur94@gmail.com', NULL, '$2y$10$X0dFk191J6IxBZAiou.A8eQvn5IOCA4SJyF1IZWEcsJThdbVefd.G', NULL, '2020-05-21 14:40:46', '2020-05-21 14:40:46', 1, 0, 'Suki', 'Kaur', '762 Hyde Road', 'Gorton', 'Manchester', 'M18 7EF', '07712163019', NULL, 'C231910079', '1994-04-12', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(110, 'Judy Perks', 'judeperks@supanet.com', NULL, '$2y$10$xsMOVC8qNecnHmi6Pvweh.Npd6za0uzGrZlyS.swVLFpWw77pJjUq', NULL, '2020-05-22 08:03:52', '2020-05-22 08:03:52', 1, 0, 'Judy', 'Perks', '33 Stephens Close', NULL, 'Hereford', 'HR4 0HU', '07817831169', NULL, 'C894840879', '1968-10-07', 0, 78, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(111, 'Jodie Kjelstrup', 'jodie@kieranosborne.co.uk', NULL, '$2y$10$QjE7kL9LXuwEIgU3qrWPZu2lDvUQuBZnwp2/mYAkFn9v1SeQlgORe', NULL, '2020-05-22 10:30:45', '2020-05-22 10:30:45', 1, 0, 'Jodie', 'Kjelstrup', '1 Rowland Cottages', 'Harman Avenue', 'Kent', 'CT21 4ly', '07595296787', NULL, 'C802176198', '1988-07-17', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(112, 'Signature Saver', 'signature@trustboxltd.com', NULL, '$2y$10$EKb5iKPAw9zwWMFPZAO/v.4Gs4e04a7ViGsfnCnb84SRpT5k5BJSC', NULL, '2020-05-22 12:49:10', '2020-05-22 12:50:38', 1, 0, 'Signature', 'Saver', '5 Millbank House', 'Riverside Business Park', 'Wilmslow', 'SK9 1BJ', '07814187847', NULL, 'C906391487', '1987-05-06', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(113, 'Simon Coates', 'tworocker4@icloud.com', NULL, '$2y$10$SrAYi1aevlnVfDv016gSpOSPqWI4Em3HH2H1HCzJXrNn4zwsyphu2', NULL, '2020-05-24 12:09:59', '2020-05-24 12:09:59', 1, 0, 'Simon', 'Coates', '17', 'Spencer Street', 'Reddish', 'SK56UH', '07900006141', NULL, 'C142419829', '1961-04-04', 0, 93, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(114, 'Ashley Blood-Halvorsen', 'ashley.halvorsen@3pb.co.uk', NULL, '$2y$10$vrs1ZOnpkX82A9OmqOtLGejM2TcExqjVxFupLpdnNulgRzPgzlbqy', NULL, '2020-05-27 05:49:23', '2020-05-27 05:49:23', 1, 0, 'Ashley', 'Blood-Halvorsen', '56 Matthew Street', 'Alvaston', 'Derby', 'DE24 0ER', '07427694502', NULL, 'C201420824', '1988-05-05', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(115, 'Amanda Gillespie', 'absolutemand@icloud.com', NULL, '$2y$10$LO9P9r9HUvTJ1cPBftrXfOT24s2HBiwofqyl3OwyPgEg26HyRT/4O', NULL, '2020-06-08 11:17:36', '2020-06-08 11:17:36', 1, 0, 'Amanda', 'Gillespie', '69', 'graham street', 'St Helens', 'wa91ly', '07449737259', NULL, 'C460850996', '1980-01-21', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(116, 'Joanna Felgate', 'jofelgate@btinternet.com', NULL, '$2y$10$AgJYCAkqw3LoO2ue8x/bWOSnqF1Q9qEKfohbpC7E.X6BGs8/0iv2S', '1GZaXDn52KZs3Ei6Pw82JLlFBT9wT5fC929TZULMkuir1QTponPQsgoIEScS', '2020-06-08 15:04:35', '2020-09-04 14:45:40', 1, 0, 'Joanna', 'Felgate', '12 Christchurch Avenue', NULL, 'Teddington', 'TW11 9AB', '07957348674', NULL, 'C224472835', '1969-01-11', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(117, 'Ian Sadler', 'ian@signaturebenefits.co.uk', NULL, '$2y$10$q/KnMPWRFjkwXRt17ZbST.qx4ESnkwPiKMuWFD.3wCxbmnYECdXdK', 'K58bjAYGuFTOgLb4fRYEyobvfbwvsuG9nixGc3LKByyLy3SWwlPfb1KLDvEz', '2020-06-09 08:55:22', '2020-06-09 10:32:05', 5, 0, 'Ian', 'Sadler', '3 Azure Court', 'Doxford International Business Park', 'Sunderland', 'SR3 3BE', '+44 (0) 3301243857', NULL, 'C701531929', NULL, 1, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(118, 'Ian Bascombe', 'Ian@squirrelgroup.co.uk', NULL, '$2y$10$PCcUzkPso2RbJOyBcDq0KuE3L/EPIuwnrGTRY1fHZF3oxNQGs8gYu', NULL, '2020-06-11 09:53:45', '2020-06-11 09:53:45', 1, 0, 'Ian', 'Bascombe', 'The Old Post Office', 'Weeford', 'Lichfield', 'WS14 0PW', '07973192590', NULL, 'C614290338', '1963-11-07', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(119, 'Claire Sadler (Provider)', 'claire@legalservicesguild.co.uk', NULL, '$2y$10$Trx6kvsmG48QWYNlut0EyuTT3qkEiDMpuB/Xs/jrrVa7Fjd93TGXO', NULL, '2020-06-12 08:12:15', '2020-06-12 08:22:48', 4, 0, 'Claire', 'Sadler (Provider)', '5 MillbankHouse', NULL, 'Wilmslow', 'SK9 1BJ', '07900268656', NULL, 'C867862997', '1988-07-03', 1, NULL, 0, 0, 0, 0, NULL, 0, '2020-06-12 08:22:48', 1, 0),
(120, 'C Sadler', 'yorkie365@aol.com', NULL, '$2y$10$08pc02BYAKQAFO.AA/ItB.TOmoOR4qhjLDegv9x2E3GPpkDtT.CD.', NULL, '2020-06-12 08:25:58', '2021-01-12 12:57:37', 4, 0, 'C', 'Sadler', '5 Millbank', NULL, 'Wilmslow', 'SK9 1BJ', '07900268656', NULL, 'C489480430', '1988-08-03', 1, NULL, 0, 0, 0, 0, NULL, 0, '2021-01-12 12:57:37', 1, 0),
(121, 'Rebecca Armitt', 'Rebecca@lsgi.co.uk', NULL, '$2y$10$A3qNasEm41ybVi5VdQX/eOa9eaSwf2hcVd4JwAyfVsZWa8xZ8kYpG', NULL, '2020-06-17 09:14:36', '2020-06-17 09:16:30', 3, 0, 'Rebecca', 'Armitt', '5 Millbak House', 'Riverside Business Park', 'Wilmslow', 'SK9 1BJ', '0345 548 1230', '7900268656', 'C170470899', '2001-01-01', 1, NULL, 0, 0, 0, 0, NULL, 0, '2020-06-17 09:16:30', 1, 0),
(122, 'James Connor', 'james_connor@shelter.org.uk', NULL, '$2y$10$TQWuY5hdQU5TtCu9myBgFOzkcmF/YDU0tj518V9WLniOP9TUmT8zW', NULL, '2020-06-23 13:58:01', '2020-06-23 13:58:01', 1, 0, 'James', 'Connor', '60C Tressillian Road', NULL, 'London', 'SE4 1YB', '07580135941', NULL, 'C515724932', '1993-05-16', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(123, 'Janet Hackett', 'hackettj1161@gmail.com', NULL, '$2y$10$ej7/n3aPN7CXI54ptSLyEewOdEpwenx4iIKDNFaVi4RRqp3fochg6', NULL, '2020-06-23 20:45:05', '2020-06-23 20:45:05', 1, 0, 'Janet', 'Hackett', '64 Bath Vale', NULL, 'Congleton', 'CW12 2HY', '07799 537123', NULL, 'C846399279', '1961-01-01', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(124, 'Eric Brown', 'e.adam.brown@outlook.com', NULL, '$2y$10$SZHldve5M6bkWzeDzoUQmO3liB0dSUkcceD0ANiYennqII9qG2aH2', NULL, '2020-06-24 10:21:28', '2020-06-24 10:21:28', 1, 0, 'Eric', 'Brown', '47 Dalebrook Road', NULL, 'Congleton', 'Cw124yd', '07960044904', NULL, 'C194689697', '1990-03-24', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(125, 'Christian Bell', 'chris@rugbywills.co.uk', NULL, '$2y$10$PnN0PL4mFlxjvEQpRmWWIutCff.31O2Ho3qMp5CuB2Mzf/K83tYaW', NULL, '2020-06-26 13:03:05', '2020-06-26 13:03:05', 1, 0, 'Christian', 'Bell', '4 Main Street', 'Newbold', 'Rugby Warwickshire', 'CV21 1HW', '07393302111', NULL, 'C657173234', '1961-08-09', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(126, 'Edward  aka Ted Yeates', 'ted@yeatesuk', NULL, '$2y$10$U46psCIKutVYZut9cV5.Z.IvDKAcbcKGJjDx3KosNsbF0mSusI6sy', NULL, '2020-06-30 11:28:17', '2020-06-30 11:28:17', 1, 0, 'Edward  aka Ted', 'Yeates', '5 Mandeville Close', NULL, 'Abingdon', 'OX14 2BD', '07539645514', NULL, 'C334143036', '1944-05-08', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(127, 'Joseph Etherington', 'info@ewp-legal.co.uk', NULL, '$2y$10$RZgkwdRO6L30GvC8wp0G4eXmmDQ8JQ195WIU4HQ7Ta6/f5gsZPEJK', NULL, '2020-07-07 14:08:36', '2020-07-07 14:08:36', 1, 0, 'Joseph', 'Etherington', '19 Boulton Close', NULL, 'Chesterfield', 'S40 4XJ', '07883976996', NULL, 'C556076641', '1988-02-22', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(128, 'Bhavesh Sanghani', 'bsanghani@msn.com', NULL, '$2y$10$Dc5UUZSa04YjWo48I3JSZOkmXoXHvTSZ1TvVIpY4ECZ.yNWxeaWni', NULL, '2020-07-08 08:42:17', '2020-07-08 08:42:17', 1, 0, 'Bhavesh', 'Sanghani', '6', 'Woodlands Court', 'Oadby', 'LE24QE', '07808065999', NULL, 'C405753755', '1966-03-16', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(129, 'Selina Donald', 'selina.donald06@gmail.com', NULL, '$2y$10$bHs6VaT8d5bj8UmW2VSYYu05V0QSSozl8RxPPD7VEkvXGimUetDAm', NULL, '2020-07-08 08:51:14', '2020-07-08 08:51:14', 1, 0, 'Selina', 'Donald', '133 Cropston Road', 'Anstey', 'Leicester', 'LE7 7BR', '07766493441', NULL, 'C648731450', '1983-08-06', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(130, 'Jon Pryse-Jones', 'jpj@thp.co.uk', NULL, '$2y$10$wGhWYJVnLc/it6R6RyeFQO5U8VPeE/ntMoyU9eCsWLbCDMfPisE5q', NULL, '2020-07-10 08:44:11', '2020-07-10 08:44:11', 1, 0, 'Jon', 'Pryse-Jones', '34 Hight Street', NULL, 'Wanstead', 'E112rj', '01371820947', NULL, 'C518606812', '1956-08-09', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(132, 'Bharath Vijayarangam', 'bxvijay@yahoo.com', NULL, '$2y$10$xqPuIMeIRsrNNXBDW3Q8f.J2jP9grI5BOBVWDm3USlpZf9it8ajAm', NULL, '2020-07-17 11:17:57', '2020-07-17 11:17:57', 1, 0, 'Bharath', 'Vijayarangam', '21', 'Central park', 'Halifax', 'HX1 2BT', '07825870548', NULL, 'C202343917', '1971-05-02', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(133, 'Baldev Kaur', 'bhuppymann@gmail.com', NULL, '$2y$10$D8f0plfDpqc/cqAPdXC3X.6pfuJ6xdZR9MLEqZc8GX7/TNgrz6Xd6', NULL, '2020-07-21 12:14:53', '2020-07-21 12:14:53', 1, 0, 'Baldev', 'Kaur', '41 Keble Grove', NULL, 'Walsall', 'WS1 3TB', '07944064901', NULL, 'C174734463', '1933-08-18', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(134, 'Sam Mawhinney', 'sam.852@hotmail.co.uk', NULL, '$2y$10$EkuqIznzmD9lQEhUbHqKe.aL1aASoSfGkpb56h62aMMJM/W095n8.', NULL, '2020-07-22 08:15:34', '2020-07-22 08:15:34', 2, 0, 'Sam', 'Mawhinney', 'Flat 4, 38 Palatine Road', 'Withington', 'Manchester', 'M20 3JL', '07464948863', '07464948863', 'C243517689', '1993-12-03', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(135, 'steven bradshaw', 'sjbradshaw63@gmail.com', NULL, '$2y$10$epXsePGHrguHk7ALdWuyC.rU3mL8NapcTRlYUu1csmziF662NYzDm', NULL, '2020-07-28 16:00:08', '2020-07-28 16:00:08', 1, 0, 'steven', 'bradshaw', '7 National Terrace', 'Bermondsey Wall East', 'London', 'SE16 4TZ', '07545321524', NULL, 'C246077232', '1963-07-29', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(136, 'Ian Henman', 'ih@thp.co.uk', NULL, '$2y$10$1fuf.1zUXhzo1x618pwgzeH1cymaOKhyJa4idau0uAhmCB0gX9xw2', NULL, '2020-07-29 07:08:15', '2020-07-29 07:08:15', 1, 0, 'Ian', 'Henman', '34-40 High Street', 'Wanstead', 'London', 'E11 2RJ', '020 8989 5147', NULL, 'C550351581', '1968-08-03', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(137, 'Luke Cresswell', 'lcresswell101@gmail.com', NULL, '$2y$10$okROCSWLUoDeYfieN7LnA.yq9YTx7aL15qNdQEKh4zY54Vqq9wQU2', NULL, '2020-08-07 13:38:16', '2020-08-07 13:38:16', 2, 0, 'Luke', 'Cresswell', '371b Stretford Road', NULL, 'Manchester', 'M15 4aw', '07931379478', NULL, 'C358408635', '2000-03-03', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(138, 'Redstone Customer', 'redstone@lsgi.co.uk', NULL, '$2y$10$aci2zZ0jodH0FsEMSVf.KuVWree9bo3qMRCJvqt3FSWhPy0gVBcBW', NULL, '2020-09-28 19:16:09', '2020-09-28 19:16:09', 1, 0, 'Redstone', 'Customer', '5', 'Millbank House', 'Wilmslow', 'SK9 1BJ', '07814187847', NULL, 'C643404573', '1987-08-15', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, 0),
(149, 'Lewis Stanton', 'lewis@outlook.com', NULL, '$2y$10$u3diFk1iCpWmuXcLp6enSetgGRXn.zPRTvIdHXWk0KsVkZ8Xj9AWO', NULL, '2020-10-16 08:16:26', '2020-10-16 08:16:26', 2, 0, 'Lewis', 'Stanton', '3', 'Stopford Avenue', 'Blackpool', 'FY20QQ', '07572398259', NULL, 'C277874823', '1990-10-12', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, NULL),
(156, 'jon irvine', 'jonathan@visionsharp.co.uk', NULL, '$2y$10$jT0DoLbA3XBgwjN7//7GZ.ttjplEnVtI6oMkYo6AOPWXbaSin91PC', NULL, '2020-11-03 16:32:08', '2020-11-06 15:03:49', 1, 0, 'jon', 'irvine', 'the stables, paradise wharf, ducie street', NULL, 'manchester', 'm12jn', '07412693001', NULL, 'C401524303', '1992-11-01', 1, NULL, 0, 5, 0, 0, '2020-07-06 23:00:00', 0, NULL, 1, 12),
(163, 'Adam Johnson', 'luke@visionsharp.co.uk', NULL, '$2y$10$WOTsXj96mdVfPlMehvdCvOhx6D.hqkH0pT/qCsYY3ZXP1vy7/WsUq', NULL, '2020-11-05 14:57:28', '2020-11-05 16:29:59', 1, 0, 'Adam', 'Johnson', 'The stables, Paradise Wharf, Ducie Street, test', 'test', 'Manchester', 'M12JN', '07412693001', NULL, 'C892925803', '2001-01-01', 1, NULL, 0, 5, 0, 0, '2020-08-31 23:00:00', 0, NULL, 1, NULL),
(166, 'Dominic Hadfield', 'dominic@legalservicesguild.co.uk', NULL, '$2y$10$A9aL.B0GGyrH9RiBy40k1.aBkA4pcaWN0Q1E7geUEAwV6ZJ2O2AZm', NULL, '2020-11-05 16:51:47', '2020-11-13 10:27:05', 1, 0, 'Dominic', 'Hadfield', '2 Lanreath Close', 'Macclesfield', 'Cheshire', 'SK10 3PS', '07814187847', NULL, 'C623799685', '1987-08-15', 0, NULL, 0, 5, 0, 0, '2020-12-07 00:00:00', 0, NULL, 1, 15),
(167, 'Jamie Johnson', 'jamie@visionsharp.co.uk', NULL, '$2y$10$T0GeohQhI6GXQJpt27Pxe.VHBpEbchTWdXsM.DXmpS.q9o442hwlu', NULL, '2020-11-06 15:56:11', '2020-11-10 11:00:47', 1, 0, 'Jamie', 'Johnson', 'test', 'test', 'test', 'm12jn', '07777777777', NULL, 'C411135477', '2020-11-03', 1, NULL, 0, 5, 0, 0, NULL, 0, NULL, 1, 22),
(171, 'Secarma User', 'user@secarma.com', NULL, '$2y$10$KofvIzwAC5jiqEgdst4ZF.ORtXpsFUzILv/wI1qS.d64YyiQ9MRVW', NULL, '2020-11-10 11:55:43', '2020-11-16 13:44:24', 1, 0, 'Secarma', 'User', 'Visionsharp, The stables', 'Paradise Wharf, Ducie Street', 'manchester', 'M1 2JN', '07412693001', NULL, 'C659083044', '2005-01-10', 1, NULL, 0, 5, 0, 0, NULL, 0, NULL, 1, 23),
(175, 'Andrew Baiden', 'andrew.baiden@nochex.com', NULL, '$2y$10$QqS33eZQXAvw6DGPRz4Lcel9Jei5xWsPkY9cyVm5efBON0u18siY2', NULL, '2020-11-11 16:52:23', '2020-11-12 15:06:31', 1, 0, 'Andrew', 'Baiden', 'Cornelious House', 'geldard road', 'Leeds', 'LS12 6DS', '01133466200', NULL, 'C620279847', '1974-02-18', 0, NULL, 0, 5, 0, 0, '2020-12-13 15:06:31', 0, NULL, 1, NULL),
(184, 'Bhupinder Mann', 'bhuppy@hotmail.com', NULL, '$2y$10$QrFwFKBQ6tkeZW0b0kibOO5jF1hb0tWKCTdqdnB7Bedn4bqXmkeEy', NULL, '2020-11-23 09:57:16', '2020-11-23 09:57:16', 7, 0, 'Bhupinder', 'Mann', '39 Keble Grove', 'Walsall', 'West Midlands', 'WS1 3TB', '07944064901', NULL, 'C562298702', '1987-08-27', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, NULL),
(187, 'Dominic Hadfield', 'dominichadfield@gmail.com', NULL, '$2y$10$Ven1cj.KyKVgJjZd2DqfkekzKskfyh7eZ/UurIgRsQ5RqFF4rmRxa', NULL, '2020-11-23 13:42:52', '2021-02-22 12:24:39', 1, 0, 'Dominic', 'Hadfield', '2', 'Lanreath Close', 'Macclesfield', 'SK10 3PS', '07814187847', NULL, 'C185066523', '15/08/1987', 0, NULL, 0, 5, 0, 0, '2020-12-24 13:56:04', 0, NULL, 1, NULL),
(189, 'Dominic Test', 'dominichadfield@icloud.com', NULL, '$2y$10$71N/HwP0Kj8i0ftYCCxNhO9gbtpQPzfFlyCD/j0Frc9AOa4Eaascu', NULL, '2020-12-17 10:30:35', '2021-01-11 13:32:45', 5, 0, 'Dominic', 'Test', '2', 'Lanreath Close', 'Macclesfield', 'SK10 3PS', '07814187847', NULL, 'C631666843', '1985-05-02', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, NULL),
(191, 'James Martin', 'James.martin@nochex.com', NULL, '$2y$10$MYio6Su7FxJleGO02k1eFubAJAVVMNzi/w3wVxQ67Ncouu89UspOC', NULL, '2020-12-21 11:36:48', '2020-12-21 11:37:18', 1, 0, 'James', 'Martin', '5', 'Millbank House', 'Wilmslow', 'SK9 1BJ', '07702846614', NULL, 'C306257190', '1985-01-01', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, NULL),
(192, 'secarma2 user2', 'user2@secarma.com', NULL, '$2y$10$Z6tQze5W/kXKlLVnuGndLuNCcVKcn1VvsPwTypZUF2796leI3cjHC', NULL, '2021-01-07 10:01:18', '2021-01-07 10:02:44', 1, 0, 'secarma2', 'user2', 'Visionsharp, The stables', 'Paradise Wharf, Ducie Street', 'Manchester', 'M1 2JN', '07412693001', NULL, 'C204464805', '1990-02-01', 1, NULL, 0, 5, 0, 0, '2021-02-07 10:02:44', 0, NULL, 1, NULL),
(195, 'Dominic Hadfield', 'dominic@millbankhousegroup.com', NULL, '$2y$10$O2R1n.dc2TBxpfZD0GvcYekzSh3rZPgieIYeWZznWBJiNaiVUHvmu', NULL, '2021-01-12 12:47:38', '2021-01-21 17:40:15', 1, 0, 'Dominic', 'Hadfield', '2', 'Lanreath Close', 'Macclesfield', 'SK10 3PS', '07814187847', NULL, 'C642247263', '1987-08-15', 0, NULL, 0, 5, 0, 0, '2021-02-12 12:48:20', 0, NULL, 1, NULL),
(196, 'paul shrimpling', 'paul@remarkablepractice.com', NULL, '$2y$10$iyDItVKM47nKcaXRbuzqOuRRJe0MYcv/96OD5xi2wZg0gz1yeON56', NULL, '2021-01-12 13:08:47', '2021-01-12 13:08:47', 1, 0, 'paul', 'shrimpling', '3', 'cedar court', 'hulland ward', 'de6 3eu', '07815', NULL, 'C502660655', '1965-02-22', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, NULL),
(197, 'Doug Aitken', 'douglas@remarkablepractice.com', NULL, '$2y$10$liWjhljIQcGv/Jz/l3lw8ulu1pSHnBSP2XJ26Zo/tgiWK0pFDeK46', NULL, '2021-01-12 13:10:01', '2021-01-12 13:18:27', 1, 0, 'Doug', 'Aitken', '8', 'Halleys Court', 'Kirkcaldy', 'KY1 1NZ', '07484124115', NULL, 'C309326742', '1965-12-15', 0, NULL, 0, 5, 0, 0, '2021-02-12 13:18:27', 0, NULL, 1, NULL),
(199, 'Sam Mawhinney', 'bwinnyzmate@gmail.com', NULL, '$2y$10$25oFfdPu.Tdg0elZrrA0W.4Y7PHQql32Hcx6e3ZdyljkHSzHpZ3jS', NULL, '2021-01-18 12:17:16', '2021-02-22 09:18:10', 1, 0, 'Sam', 'Mawhinney', 'Flat 4, 38 Palatine Road', 'Withington', 'Manchester', 'M20 3JL', '07464948863', NULL, 'C414674013', '1993-12-03', 0, NULL, 0, 5, 0, 0, '2021-03-25 09:17:48', 0, NULL, 1, 35),
(201, 'peter biscian', 'boyan.tonev@e-comprocessing.com', NULL, '$2y$10$AkoABWJu1.DjvL6d8K8sW.y09XFAcPTf8yG8h3bf9qvEPjxzAEZfG', NULL, '2021-01-20 12:44:57', '2021-01-20 12:44:57', 7, 0, 'peter', 'biscian', '47 Butt Road', '47 Butt Road', 'Colchester', '14206', '7792944320', NULL, 'C605543553', '1980-03-03', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, NULL),
(202, 'Sam Mawhinney', 'sam.mawhinney@lsgi.co.uk', NULL, '$2y$10$a49oealCYo03LmrQLVwQPuwiqOKXHmhU3PteIeQK4.e4LS21GGP.G', NULL, '2021-02-09 10:29:02', '2021-02-10 08:58:04', 1, 0, 'Sam', 'Mawhinney', 'Flat 4, 38 Palatine Road', 'Withington', 'Manchester', 'M20 3JL', '07464948863', NULL, 'C907947167', '1993-12-03', 0, NULL, 0, 5, 0, 0, '2021-03-13 08:58:04', 0, NULL, 1, NULL),
(203, 'Robert Hannaford', 'rg.hannaford@gmail.com', NULL, '$2y$10$NbOvhWz6lvKc4cM0qVKGS.fMTyCrVc5Vch5zMSXsAzMthfPGYqTsq', NULL, '2021-02-16 17:08:42', '2021-02-16 17:08:42', 1, 0, 'Robert', 'Hannaford', '11 Cedar Avenue', 'Barnet', 'Herts', 'EN4 8DY', '07402 626357', NULL, 'C140419869', '1960-12-06', 0, 62, 0, 0, 0, 0, NULL, 0, NULL, 1, NULL),
(204, 'Patrick Harrison', 'patrick@lsgi.co.uk', NULL, '$2y$10$onPUnCvBELzjp09KIIcdxunEt8HZi1BbPLcy8hXIkCs9iQ64eMugq', NULL, '2021-02-17 14:37:01', '2021-02-17 14:37:26', 1, 0, 'Patrick', 'Harrison', '13 Plymouth Road', '13 Plymouth Road', 'Tavistock', 'PL19 8AU', '07932155182', NULL, 'C430136307', '1969-04-03', 0, NULL, 0, 0, 0, 0, NULL, 0, NULL, 1, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `benefits`
--
ALTER TABLE `benefits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `benefit_contacts`
--
ALTER TABLE `benefit_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `benefit_partner`
--
ALTER TABLE `benefit_partner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `benefit_user`
--
ALTER TABLE `benefit_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blocks`
--
ALTER TABLE `blocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `changelogs`
--
ALTER TABLE `changelogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `connections`
--
ALTER TABLE `connections`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `connections_partner_id_connection_id_unique` (`partner_id`,`connection_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `debit_payments`
--
ALTER TABLE `debit_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invitations`
--
ALTER TABLE `invitations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invites`
--
ALTER TABLE `invites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `letters`
--
ALTER TABLE `letters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `memberships`
--
ALTER TABLE `memberships`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partner_post`
--
ALTER TABLE `partner_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partner_service`
--
ALTER TABLE `partner_service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `providers`
--
ALTER TABLE `providers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reasons`
--
ALTER TABLE `reasons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms_verifications`
--
ALTER TABLE `sms_verifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trusted_users`
--
ALTER TABLE `trusted_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `benefits`
--
ALTER TABLE `benefits`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `benefit_contacts`
--
ALTER TABLE `benefit_contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `benefit_partner`
--
ALTER TABLE `benefit_partner`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `benefit_user`
--
ALTER TABLE `benefit_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=212;

--
-- AUTO_INCREMENT for table `blocks`
--
ALTER TABLE `blocks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `changelogs`
--
ALTER TABLE `changelogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1204;

--
-- AUTO_INCREMENT for table `connections`
--
ALTER TABLE `connections`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `debit_payments`
--
ALTER TABLE `debit_payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invitations`
--
ALTER TABLE `invitations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;

--
-- AUTO_INCREMENT for table `invites`
--
ALTER TABLE `invites`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT for table `letters`
--
ALTER TABLE `letters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `memberships`
--
ALTER TABLE `memberships`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=194;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=372;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `partner_post`
--
ALTER TABLE `partner_post`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `partner_service`
--
ALTER TABLE `partner_service`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `plans`
--
ALTER TABLE `plans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `providers`
--
ALTER TABLE `providers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `reasons`
--
ALTER TABLE `reasons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sms_verifications`
--
ALTER TABLE `sms_verifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `trusted_users`
--
ALTER TABLE `trusted_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=205;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
