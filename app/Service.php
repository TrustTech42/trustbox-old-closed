<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = ['name', 'description', 'boosted_description', 'thumbnail', 'banner'];

    public function partners(){
        return $this->belongsToMany('App\Partner');
    }
}
