<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection, WithHeadings
{
    /**
     * Returns the user model and sets the data columns
     */
    public function collection()
    {
        return User::all(
            'id',
            'customer_number',
            'first_name',
            'last_name',
            'email',
            'address1',
            'address2',
            'city',
            'postcode',
            'telephone',
            'mobile',
            'date_of_birth',
            'created_at'
        );
    }

    /**
     * Sets the headings for the CSV file
     */
    public function headings(): array
    {
        return [
            'ID',
            'Customer Number',
            'First Name',
            'Last Name',
            'Email',
            'Address 1',
            'Address 2',
            'City',
            'Postcode',
            'Telephone',
            'Mobile',
            'Date Of Birth',
            'Created At'
        ];
    }
}
