<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DebitPayment extends Model
{
    protected $fillable = [
        'subscription_id',
        'amount',
        'transaction_id',
        'benefit_id',
        'benefit_name'
    ];

    public function subscription(){
        return $this->belongsTo('App\Subscription');
    }
}
