<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'membership_id',
        'amount',
        'transaction_id'
    ];

    public function membership()
    {
        return $this->belongsTo('App\Membership');
    }
}
