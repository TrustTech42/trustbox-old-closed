<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Membership extends Model
{
    protected $fillable = ['user_id', 'partner_id', 'plan_id', 'activated'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function partner()
    {
        return $this->belongsTo('App\Partner');
    }

    public function plan()
    {
        return $this->belongsTo('App\Plan');
    }

    public function payment()
    {
        return $this->hasOne('App\Payment');
    }
}
