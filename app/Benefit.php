<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Benefit extends Model
{
    protected $fillable = [
        'name',
        'page_id',
        'description',
        'boosted_description',
        'thumbnail',
        'banner',
        'price',
        'provider_id',
        'redeem_text',
        'featured',
        'unique',
        'includes_file_manager',
        'includes_benefit_phone_button',
        'includes_benefit_form',
        'provider_description',
        'provider_usp',
        'provider_email',
        'provider_nochex_id',
        'provider_telephone',
        'partner_percentage_cut',
        'provider_percentage_cut',
        'type'
    ];

    /**
     * Many-to-Many relationship between benefits and categories
     */
    public function categories()
    {
        return $this->belongsToMany('App\Category',
            'benefit_categories',
            'benefit_id',
            'category_id'
        )->withTimestamps();
    }

    /**
     * Many-to-Many relationship between benefits and types
     */
    public function types()
    {
        return $this->belongsToMany('App\Type',
            'benefit_types',
            'benefit_id',
            'type_id'
        )->withTimestamps();
    }

    public function partners(){
        return $this->belongsToMany('App\Partner')->withPivot('sort_order');
    }

    public function users(){
        return $this->belongsToMany('App\User');
    }

    public function provider(){
        return $this->belongsTo('App\Provider');
    }

    public function page()
    {
        return $this->hasOne('App\Page');
    }

    /**
     * Many-to-Many relationship between benefits and posts
     */
    public function posts()
    {
        return $this->belongsToMany('App\Post',
            'post_benefits',
            'benefit_id',
            'post_id'
        )->withTimestamps();
    }

    public function getBenefits($provider)
    {
        $benefits = $provider->benefits()->pluck('name')->all();

        $benefitsArr = [];

        $benefitsArr[null] = 'Default';

        foreach($benefits as $benefit)
        {
            $benefitsArr[strtolower($benefit)] = $benefit;
        }

        $benefitsArr[1] = 'Boosted';

        return $benefitsArr;
    }
}
