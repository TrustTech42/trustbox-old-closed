<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'id',
        'title',
        'headline',
        'content',
        'banner',
        'colour',
        'enabled',
        'author',
        'category_id',
        'snippet',
        'benefit_snippet',
        'is_guest',
        'is_connected',
        'notify',
        'notify_from',
        'notify_email_sent',
        'is_global'
    ];

    /**
     * Relationships
     */
    public function partners()
    {
        return $this->belongsToMany('App\Partner',
            'partner_post',
            'post_id',
            'partner_id'
        )->withTimestamps();
    }

    /**
     * Many-to-Many relationship between posts and categories
     */
    public function categories()
    {
        return $this->belongsToMany('App\Category',
            'post_categories',
            'post_id',
            'category_id'
        )->withTimestamps();
    }

    /**
     * Many-to-Many relationship between posts and benefits
     */
    public function benefits()
    {
        return $this->belongsToMany('App\Benefit',
            'post_benefits',
            'post_id',
            'benefit_id'
        )->withTimestamps();
    }

    /**
     * Utility
     */
    public function hasPartner($id){
        return $this->partners()->find($id) ? 1 : 0;
    }


    public function saveQuietly(array $options = [])
    {
        return static::withoutEvents(function () use ($options) {
            return $this->save($options);
        });
    }
}
