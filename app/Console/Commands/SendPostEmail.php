<?php

namespace App\Console\Commands;

use App\Mail\NewPost;
use App\Post;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendPostEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:sendemail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send schedule post notify';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $posts = Post::where('notify_email_sent', 0)->where('notify', 1)
//            ->whereDate('notify_from', '<=', Carbon::now())->get();
//
//        foreach ($posts as $post) {
//            foreach ($post->partners as $partner) {
//                foreach ($partner->members as $member) {
//                    Mail::to($member->email)->send(new NewPost($partner, $member, $post));
//                }
//            }
//
//            $post->update([
//               'notify_email_sent' => 1
//            ]);
//        }
    }
}
