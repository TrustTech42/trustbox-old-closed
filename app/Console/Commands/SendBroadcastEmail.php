<?php

namespace App\Console\Commands;

use App\Mail\BroadcastSent;
use App\Message;
use App\Partner;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendBroadcastEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'broadcast:sendemail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send scheduled broadcast email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $messages = Message::where('type', 'broadcast')->where('email_sent', 0)
//            ->whereDate('valid_from', '<=', Carbon::now())->get();
//
//        foreach ($messages as $message) {
//            $partner = Partner::findOrFail($message->sender_id);
//
//            foreach ($partner->members as $member) {
//                Mail::to($member->email)->send(new BroadcastSent($partner, $member, $message));
//            }
//
//            $message->update([
//                'email_sent' => 1
//            ]);
//        }
    }
}
