<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CleanApplication extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleans the application.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Resetting Application - You can edit this command at Console/Commands/CleanApplication.php');
        $this->info('');
        $bar = $this->output->createProgressBar(3);
        $bar->advance();

        $this->info(' > Deleting media...');
        $this->deleteMedia("logos");
        $this->deleteMedia("banners");
        $this->deleteMedia("blocks");
        $this->deleteMedia("thumbnails");
        $bar->advance();

        $this->info(' > Migrating tables...');
        $this->migrateData();
        $bar->advance();

        $this->info(' > Complete.');
    }

    private function deleteMedia($type){
        $files = glob(__DIR__ . "/../../../storage/app/public/".$type."/*");

        foreach($files as $file){
            if($file !== "/home/vagrant/code/famben/app/Console/Commands/../../../storage/app/public/logos/logo.png" &&
               $file !== "/home/vagrant/code/famben/app/Console/Commands/../../../storage/app/public/banners/banner.jpg"){
                echo "Deleting $type file -> ".$file;
                unlink($file);
                echo $file." [success].\n";
            }
        }
    }

    private function migrateData(){
        $this->callSilent('migrate:refresh', ['--seed' => true]);
    }
}
