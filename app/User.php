<?php

namespace App;

use App\Notifications\PasswordReset;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Carbon;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    // the static benefit id for the file manager
    protected $benefit_id = 2;

    # #####################################
    # Protected Variables
    # #####################################
    protected $fillable = [
        'name',
        'email',
        'password',
        'first_name',
        'last_name',
        'address1',
        'address2',
        'city',
        'postcode',
        'telephone',
        'mobile',
        'referred',
        'role_id',
        'change_password',
        'customer_number',
        'date_of_birth',
        'letter_printed',
        'storage',
        'storage_used',
        'storage_upgraded',
        'has_file_manager',
        'is_trial',
        'file_manager_trial_date',
        'deceased',
        'marketing_consent',
        'api_token',
        'subscription_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    # #####################################
    # Model Relationships
    # #####################################
    public function invites()
    {
        return $this->hasMany('App\Invite');
    }

    public function partner()
    {
        return $this->hasOne('App\Partner');
    }

    public function provider()
    {
        return $this->hasOne('App\Provider');
    }

    public function membership()
    {
        return $this->hasOne('App\Membership');
    }

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function permissions()
    {
        return $this->hasMany('App\Permission');
    }

    public function benefits()
    {
        return $this->belongsToMany('App\Benefit')->withPivot('boosted');
    }

    public function subscription()
    {
        return $this->hasOne('App\Subscription');
    }

    public function linked_user()
    {
        return $this->belongsTo('App\User', 'referred', 'id');
    }

    public function referred_users()
    {
        return $this->hasMany('App\User', 'referred', 'id');
    }

    public function changelogs()
    {
        return $this->hasMany('App\Changelog');
    }

    public function letter()
    {
        return $this->hasOne('App\Letter');
    }

    public function contacts()
    {
        return $this->hasMany('App\Contact');
    }

    public function benefitContact()
    {
        return $this->hasMany('App\BenefitContact');
    }

    public function trusted()
    {
        return $this->belongsToMany("App\User", 'trusted_users', 'user_id', 'trusted_id');
    }

    public function trustees()
    {
        return $this->belongsToMany("App\User", 'trusted_users', 'trusted_id', 'user_id');
    }

    /**
     * This grabs the broadcasted messages from the users partner
     */
    public function broadcasts(){

        return $this->membership->partner->allBroadcasts()
            ->whereDate('valid_from', '<=', Carbon::now())->get();
    }

    # #####################################
    # Attributes
    # #####################################

    public function getTrialDateAttribute()
    {
        $date      = Carbon::now();
        $trialDate = Carbon::parse($this->file_manager_trial_date);

        $diff = $trialDate->diffInDays($date);

        return $diff;
    }

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getLinkedUsersAttribute()
    {
        return $this->referred_users->merge($this->trusted);
    }

    # File Manager Specific ####################
    public function getFileManagerEnrolledAttribute()
    {
        if(in_array($this->benefit_id, $this->benefits()->pluck('benefits.id')->toArray())){
            return true;
        }

        foreach($this->benefits()->get() as $benefit){
            if($benefit->includes_file_manager){
                return true;
            }
        }

        return false;
    }

    public function getTrialExpiredAttribute()
    {
        foreach($this->benefits()->get() as $benefit){
            if($benefit->includes_file_manager && $benefit->pivot->boosted){
                return false;
            }
        }

        return Carbon::parse($this->file_manager_trial_date) < Carbon::now();
    }

    public function getBoostedFileManagerAttribute()
    {
        foreach($this->benefits()->get() as $benefit){
            if($benefit->includes_file_manager && $benefit->pivot->boosted){
                return true;
            }
        }

        return $this->benefits()->where("benefits.id", $this->benefit_id)->first()
            ? $this->benefits()->where("benefits.id", $this->benefit_id)->first()->pivot->boosted
            : false;
    }

    public function getIsFileManagerTrialAttribute()
    {
        # Check to see if user has enrolled on the File Manager Benefit
        if (!$this->fileManagerEnrolled)
            return false;

        # Check to see if the user has boosted the benefit
        if ($this->boostedFileManager)
            return false;

        # Check to see if the user's trial date is less than today (expired)
        if ($this->trialExpired)
            return false;

        # If not failed on any of above, return true
        return true;
    }

    # #####################################
    # Utility Functions
    # #####################################
    public function sendPasswordResetNotification($token)
    {
        session()->flash('success', 'A password reset email has been sent to the registered email address. Please check your inbox and follow the instructions.');
        $this->notify(new PasswordReset($token, $this->partner));
    }
}
