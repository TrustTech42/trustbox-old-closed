<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = ['benefit_id', 'name', 'url', 'template', 'enabled'];

    public function blocks(){
        return $this->hasMany("App\Block");
    }

    public function benefit()
    {
        return $this->hasOne('App\Benefit');
    }
}
