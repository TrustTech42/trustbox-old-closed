<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = ['name', 'price'];

    public function users(){
        return $this->hasMany('App\Membership');
    }
}
