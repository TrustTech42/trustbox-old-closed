<?php

namespace App;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Database\Eloquent\Model;

class Invitations extends Model
{
    protected $fillable = [
        'name',
        'email',
        'address1',
        'address2',
        'address3',
        'address4',
        'postcode',
        'code',
        'status',
        'partner',
        'type',
        'url'
    ];

    public function createInvitation($user)
    {
        $data = [];
        $data['content'] = Setting::first()->invitation_letter_content;
        $data['user'] = $user;

        $path = public_path().'/letters/letter-'.$user->name.'-'.time().'.pdf';

        PDF::loadView('letters.invitations-welcome', $data)->save($path);

        return $path;
    }
}
