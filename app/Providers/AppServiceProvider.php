<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Form;
use App\Page;
use App\Block;
use App\Partner;
use App\Setting;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        # database fix
        Schema::defaultStringLength(191);

        # Register custom form components
        Form::component('bsText', 'components.forms.text', ['name', 'value', 'attributes', 'label']);
        Form::component('bsEmail', 'components.forms.email', ['name', 'value', 'attributes', 'label']);
        Form::component('bsPassword', 'components.forms.password', ['name', 'label', 'password', 'attributes']);
        Form::component('bsSelect', 'components.forms.select', ['name', 'values', 'value', 'attributes', 'label']);
        Form::component('bsSwitch', 'components.forms.switch', ['name', 'value', 'label']);


        View::composer('*', function ($view) {
            $url = url()->current();
            $url = explode('/', $url);

            if (!isset($url[3])) {
                $blocks = Page::where('url', '/')->first()->blocks()->orderBy('sort_order', 'asc')->get();
            } else {
                if(!isset($url[4]))
                {
                    if (Page::where('url', $url[3])->first()) {
                        $blocks = Page::where('url', $url[3])->first()->blocks()->orderBy('sort_order', 'asc')->get();
                    } else {
                        $blocks = [];
                    }
                }else
                {
                    if (Page::where('url', $url[4])->first()) {
                        $blocks = Page::where('url', $url[4])->first()->blocks()->orderBy('sort_order', 'asc')->get();
                    } else {
                        $blocks = [];
                    }
                }
            }

            $url       = explode('.', $url[2]);
            $subdomain = $url[0];

            $global_partner = Partner::where('subdomain', "$subdomain")->with('posts')->first();

            if ($global_partner === null) {
                $global_partner = Partner::find(Setting::where('name', 'default_partner')->first()->value);
            }

            $view->with(['blocks' => $blocks, 'global_partner' => $global_partner]);;
        });
    }
}
