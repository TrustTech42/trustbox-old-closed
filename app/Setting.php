<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'name',
        'value',
        'welcome_letter_content',
        'code_invite_email_content',
        'invitation_letter_content',
        'signup_email_content',
        'boosted_email_content',
        'cancelled_boosted_email_content',
        'enrolled_email_content',
        'invite_email_content',
        'removed_email_content',
        'benefit_form_content',
        'benefit_notify_content',
        'welcome_email_content',
        'new_user_benefits_content',
        // storage options
        'default_storage',
        'upgraded_storage',
        // invite limit options
        'household_limit',
        'gifted_limit',
        'trusted_limit'
    ];
}
