<?php

namespace App\Observers;

use App\Mail\BroadcastSent;
use App\Message;
use App\Partner;
use Illuminate\Support\Facades\Mail;

class MessageObserver
{
    /**
     * Handle the message "created" event.
     *
     * @param  \App\Message  $message
     * @return void
     */
    public function created(Message $message)
    {
//        $user = auth()->user();
//
//        $partner = Partner::findOrFail($user->partner->id);
//
//        if ($message->type === 'broadcast') {
//            foreach ($partner->members as $member) {
//                Mail::to($member->email)->send(new BroadcastSent($partner, $member, $message));
//            }
//
//            $message->update([
//                'email_sent' => 1
//            ]);
//        }
    }

    /**
     * Handle the message "updated" event.
     *
     * @param  \App\Message  $message
     * @return void
     */
    public function updated(Message $message)
    {
        //
    }

    /**
     * Handle the message "deleted" event.
     *
     * @param  \App\Message  $message
     * @return void
     */
    public function deleted(Message $message)
    {
        //
    }

    /**
     * Handle the message "restored" event.
     *
     * @param  \App\Message  $message
     * @return void
     */
    public function restored(Message $message)
    {
        //
    }

    /**
     * Handle the message "force deleted" event.
     *
     * @param  \App\Message  $message
     * @return void
     */
    public function forceDeleted(Message $message)
    {
        //
    }
}
