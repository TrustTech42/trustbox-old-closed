<?php

namespace App\Observers;

use App\Mail\NewPost;
use App\Message;
use App\Partner;
use App\Post;
use Illuminate\Support\Facades\Mail;

class PostObserver
{
    /**
     * Handle the post "created" event.
     *
     * @param \App\Post $post
     * @return void
     */
    public function created(Post $post)
    {
        $user = auth()->user();

        $partner = Partner::findOrFail($user->partner->id);

//        if ($post->is_connected == 1) {
//            foreach ($partner->allConnections as $connection) {
//                Mail::to($connection->contact_email)->send(new NewPost($partner, $connection, $post));
//            }
//        }

        // CREATES A BROADCAST FOR ALL USERS ABOUT POST
        Message::create([
            'sender_id' => $partner->id,
            'headline' => $post->headline,
            'body' => 'The post ' . $post->title . ' is now live',
            'author' => $partner->name,
            'type' => 'new post',
            'is_read' => 0,
            'email_sent' => 0
        ]);
    }

    /**
     * Handle the post "updated" event.
     *
     * @param \App\Post $post
     * @return void
     */
    public function updated(Post $post)
    {
        //
    }

    /**
     * Handle the post "deleted" event.
     *
     * @param \App\Post $post
     * @return void
     */
    public function deleted(Post $post)
    {
        //
    }

    /**
     * Handle the post "restored" event.
     *
     * @param \App\Post $post
     * @return void
     */
    public function restored(Post $post)
    {
        //
    }

    /**
     * Handle the post "force deleted" event.
     *
     * @param \App\Post $post
     * @return void
     */
    public function forceDeleted(Post $post)
    {
        //
    }
}
