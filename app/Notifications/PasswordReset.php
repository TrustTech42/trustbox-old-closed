<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Config;

class PasswordReset extends Notification
{
    protected $token;
    protected $partner;

    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token, $partner)
    {
        $this->token = $token;
        $this->partner = $partner;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        return (new MailMessage)
                    ->line('Hello '.$notifiable->name)
                    ->line('We received a request to reset your password for your TrustBox account: '.$notifiable->email)
                    ->line('We’re here to help!')
                    ->line('Simply click on the button below to set a new password:')
                    ->line('If you didn’t ask to change your password, don’t worry! Your password is still safe, and you can delete this email.')
                    ->line('If you need help, or you have any other questions, feel free to email help@trustbox.com or call our customer service team on 0345 548 1230.')
                    ->line('Thanks,')
                    ->line('The TrustBox team')
                    ->action('Reset Password', url('password/reset', $this->token));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
