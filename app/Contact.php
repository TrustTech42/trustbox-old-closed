<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'email',
        'subject',
        'message',
        'partner_id',
        'resolved',
        'reason_id'
    ];

    public function partner(){
        return $this->belongsTo('App\Partner');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function reason(){
        return $this->belongsTo('App\Reason');
    }
}
