<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'id',
        'name'
    ];

    public function posts()
    {
        return $this->belongsToMany(
            'App\Post',
            'post_categories',
            'category_id',
            'post_id'
        )->withTimestamps();
    }

    /**
     * Many-to-Many relationship between benefits and categories
     */
    public function benefits()
    {
        return $this->belongsToMany('App\Benefit',
            'benefit_categories',
            'category_id',
            'benefit_id'
        )->withTimestamps();
    }
}
