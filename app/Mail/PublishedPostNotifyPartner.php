<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PublishedPostNotifyPartner extends Mailable
{
    use Queueable, SerializesModels;

    protected $partner;

    protected $postOwner;

    protected $post;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($partner, $postOwner, $post)
    {
        $this->partner = $partner;
        $this->postOwner = $postOwner;
        $this->post = $post;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from(['address' => "{$this->partner->subdomain}@yourtrustbox.co.uk", 'name' => "{$this->partner->name}"])
            ->subject("Post has been published to Partner!")
            ->view('mail.posts.new-publish-notify-partner')
            ->with([
                'partner' => $this->partner,
                'postOwner' => $this->postOwner,
                'post' => $this->post
            ]);
    }
}
