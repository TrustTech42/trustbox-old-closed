<?php

namespace App\Mail;

use App\Benefit;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyProvider extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var
     */
    protected $vars;

    /**
     * NotifyProvider constructor.
     * @param $vars
     */
    public function __construct($vars)
    {
        $this->vars = $vars;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from(['address' => 'trustbox@yourtrustbox.co.uk', 'name' => 'Trust Box'])
            ->subject($this->vars->subject)
            ->view($this->vars->template)
            ->with(['vars' => $this->vars]);
    }
}
