<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserInvite extends Mailable
{
    use Queueable, SerializesModels;

    // protected
    protected $user;
    protected $partner;
    protected $plan;
    protected $content;
    protected $code;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $partner, $plan, $content, $code)
    {
        $this->user = $user;
        $this->partner = $partner;
        $this->plan = $plan;
        $this->content = $content;
        $this->code = $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from(['address' => 'trustbox@yourtrustbox.co.uk', 'name' => 'Trust Box'])
            ->subject('Join ' . $this->user->first_name . ' ' . $this->user->last_name . '\'s Trust Box Community')
            ->view('mail.invite')
            ->with([
                'user' => $this->user,
                'partner' => $this->partner,
                'plan' => $this->plan,
                'content' => $this->content,
                'code' => $this->code
            ]);
    }
}
