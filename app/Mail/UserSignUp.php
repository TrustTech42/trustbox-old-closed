<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserSignUp extends Mailable
{
    use Queueable, SerializesModels;

    protected $vars;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($vars)
    {
        $this->vars = $vars;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from(['address' => 'trustbox@yourtrustbox.co.uk', 'name' => 'Trust Box'])
            ->subject('Welcome to Trust Box')
            ->view('mail.user-signup')
            ->with(['vars' => $this->vars]);
    }
}
