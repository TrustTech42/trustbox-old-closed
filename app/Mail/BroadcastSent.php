<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BroadcastSent extends Mailable
{
    use Queueable, SerializesModels;


    protected $partner;

    protected $member;

    protected $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($partner, $member, $message)
    {
        $this->partner = $partner;
        $this->member = $member;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from(['address' => "{$this->partner->subdomain}@yourtrustbox.co.uk", 'name' => "{$this->partner->name}"])
            ->subject('New Broadcast!')
            ->view('mail.broadcasts.new-broadcast')
            ->with([
                'partner' => $this->partner,
                'member' => $this->member,
                'message' => $this->message
            ]);
    }
}
