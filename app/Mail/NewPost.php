<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewPost extends Mailable
{
    use Queueable, SerializesModels;

    protected $partner;

    protected $member;

    protected $post;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($partner, $member, $post)
    {
        $this->partner = $partner;
        $this->member = $member;
        $this->post = $post;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from(['address' => "{$this->partner->subdomain}@yourtrustbox.co.uk", 'name' => "{$this->partner->name}"])
            ->subject('New blog post!')
            ->view('mail.posts.new-blog')
            ->with([
                'partner' => $this->partner,
                'member' => $this->member,
                'post' => $this->post
            ]);
    }
}
