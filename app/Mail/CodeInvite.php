<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CodeInvite extends Mailable
{
    protected $vars;

    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($vars)
    {
        $this->vars = $vars;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from(['address' => "{$this->vars->partner->subdomain}@yourtrustbox.co.uk", 'name' => "{$this->vars->partner->name}"])
            ->subject('Code Invite')
            ->view('mail.code-invite')
            ->with(['vars' => $this->vars]);
    }
}
