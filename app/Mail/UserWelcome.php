<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserWelcome extends Mailable
{
    use Queueable, SerializesModels;

    protected $vars;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($vars)
    {
        $this->vars = $vars;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from(['address' => "{$this->vars->partner->subdomain}@yourtrustbox.co.uk", 'name' => "{$this->vars->partner->name}"])
            ->subject('Welcome to Trust Box')
            ->view('mail.user-welcome')
            ->with(['vars' => $this->vars]);
    }
}
