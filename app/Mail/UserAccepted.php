<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserAccepted extends Mailable
{
    use Queueable, SerializesModels;

    // protected
    protected $user;
    protected $referred;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $referred)
    {
        $this->user = $user;
        $this->referred = $referred;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from(['address' => $this->user->email, 'name' => 'Trust Box'])
            ->view('mail.accepted')
            ->with([
                'referred' => $this->referred
            ]);
    }
}
