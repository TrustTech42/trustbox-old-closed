<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BenefitForm extends Mailable
{
    use Queueable, SerializesModels;

    protected $vars;

    /**
     * BenefitForm constructor.
     * @param $vars
     */
    public function __construct($vars)
    {
        $this->vars = $vars;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from(['address' => 'trustbox@yourtrustbox.co.uk', 'name' => 'Trust Box'])
            ->subject($this->vars->subject)
            ->view($this->vars->template)
            ->with(['vars' => $this->vars]);
    }
}
