<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PublishedPostNotifyMembers extends Mailable
{
    use Queueable, SerializesModels;

    protected $partner;

    protected $member;

    protected $post;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($partner, $member, $post)
    {
        $this->partner = $partner;
        $this->member = $member;
        $this->post = $post;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from(['address' => "{$this->partner->subdomain}@yourtrustbox.co.uk", 'name' => "{$this->partner->name}"])
            ->subject("Post has been published to members!")
            ->view('mail.posts.new-publish-notify-members')
            ->with([
                'partner' => $this->partner,
                'member' => $this->member,
                'post' => $this->post
            ]);
    }
}