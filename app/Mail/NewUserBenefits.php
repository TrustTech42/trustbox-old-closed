<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewUserBenefits extends Mailable
{
    use Queueable, SerializesModels;

    protected $vars;

    /**
     * @param $vars
     */
    public function __construct($vars)
    {
        $this->vars = $vars;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from(['address' => "{$this->vars->partner->subdomain}@yourtrustbox.co.uk", 'name' => "{$this->vars->partner->name}"])
            ->subject("Redeem any of these benefits.")
            ->view('mail.new-user-benefits')
            ->with(['vars' => $this->vars]);
    }
}
