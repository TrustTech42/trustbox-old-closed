<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Benefit;
use App\User;

class Provider extends Model
{
    protected $fillable = [
        'name',
        'user_id',
        'provider_logo',
        'nochex_id',
        'telephone',
        'email'
    ];

    /**
     * Relationships
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function partner()
    {
        return $this->hasOne('App\Partner');
    }

    public function benefits()
    {
        return $this->hasMany('App\Benefit');
    }

    public function members(){
        return User::with('benefits')->whereHas('benefits', function ($q) {
            $q->where('provider_id', $this->id);
        })->paginate(10);
    }

    public function membersCSV($get = '*')
    {
        return User::with('benefits')->whereHas('benefits', function ($q) {
            $q->where('provider_id', $this->id);
        })->get($get);
    }
}
