<?php


namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = [
        'schedule_id',
        'status',
        'total'
    ];

    public function payments(){
        return $this->hasMany('App\DebitPayment');
    }

    public function user() {
        return $this->hasOne('App\User');
    }
}
