<?php

namespace App\Http\Middleware;

use Closure;

class DataProtection
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->route('user') && !auth()->user()){
            abort('403', 'You do not have permission to view this page.');
        }

        if($request->route('user') && $request->user() && $request->user()->id !== $request->route('user')->id)
            return redirect('/dashboard');

        return $next($request);
    }
}
