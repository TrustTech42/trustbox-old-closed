<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Route;

class CheckPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();

        if(strtolower($user->role->name) !== 'admin')
        {
            $route = explode('.', Route::currentRouteName())[0];

            if(!in_array($route, $user->permissions()->get()->pluck('name')->toArray()))
            {
                return redirect()->back();
            }
        }

        return $next($request);
    }
}
