<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class Subdomain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = auth()->user();
        $currentSubdomain = explode('.', $_SERVER['HTTP_HOST'])[0];

        if ($user->role->name === "Partner") {
            $partnerSubdomain = $user->partner->subdomain;
        } else {
            $partnerSubdomain = !is_null($user->membership) ? $user->membership->partner->subdomain : '';
        }

        if ($currentSubdomain !== $partnerSubdomain) {
            return redirect('https://' . $partnerSubdomain . '.' . env("APP_DOMAIN") . '/dashboard');
        }

        return $next($request);
    }
}
