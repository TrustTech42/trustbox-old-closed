<?php

namespace App\Http\Middleware;

use App\SmsVerification;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Support\Facades\Auth as A;
use Closure;

class Authenticate extends Middleware
{
    public function handle($request, Closure $next, ...$guards)
    {
        $this->authenticate($request, $guards);


        $user = A::user();

//        $code = SmsVerification::where('contact_number', '=', $user->mobile)->exists() ? SmsVerification::where('contact_number', '=', $user->mobile)->latest()->first() : false;
//
//        if((strtolower($user->role->name) == 'admin' || strtolower($user->role->name) == 'staff') && $code && $code->status == 'pending')
//        {
//            return redirect()->route('sms-verification.start.contact', compact('user'));
//        }

        return $next($request);
    }

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('login');
        }
    }
}
