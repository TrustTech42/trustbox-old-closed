<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Http\Request;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::user();

        if($user)
        {
            if(session()->has('og_user'))
            {
                \Auth::logout();
                \Auth::loginUsingId(session()->get('og_user')['id']);

                session()->forget('og_user');
            }

	        if(request()->user()->role->name !== 'Admin' && $user->role->name !== 'Staff')
	        {
	            return redirect()->back();
	        }

        }	return $next($request);
    }
}
