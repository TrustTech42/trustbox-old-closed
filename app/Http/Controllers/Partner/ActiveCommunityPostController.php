<?php

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\Controller;
use App\Partner;
use App\Post;

class ActiveCommunityPostController extends Controller
{
    /**
     * Endpoint used to show active community posts
     */
    public function showActivePosts(string $partnerId, string $postId)
    {
        $partner = Partner::findOrFail($partnerId);

        $post = Post::findOrFail($postId);

        return view('frontend.partner.dashboard.active_posts.show', compact('partner', 'post'));
    }

//    /**
//     * Endpoint used to list active community posts
//     */
//    public function listActivePosts()
//    {
//        $user = auth()->user();
//
//        $partner = Partner::findOrFail($user->partner->id);
//
//        $activePosts = $partner->posts->where('pivot.published', 1);
//
//        return view('frontend.partner.tabs.posts.community.active.index', compact('activePosts', 'partner'));
//    }

    /**
     * Endpoint used to delete active(published) community posts
     */
    public function deleteActivePosts(string $partnerId, string $postId)
    {
        $partner = Partner::findOrFail($partnerId);

        $post = Post::findOrFail($postId);

        $partner->posts()->detach($post);

        session()->flash('success', 'Active Post Deleted');

        return redirect('partner/' . $partner->id . '/dashboard/active_posts/index');
    }
}