<?php

namespace App\Http\Controllers\Partner;

use App\Contact;
use App\Http\Controllers\Controller;
use App\Partner;

class InboxController extends Controller
{
    /**
     * Endpoint Used to list received contact messages
     */
    public function listCustomerMessages()
    {
        $user = auth()->user();

        $partner = Partner::findOrFail($user->partner->id);

        $contactMessages = Contact::where('partner_id', $partner->id)->get();

        return view('frontend.partner.tabs.inbox.index', compact('partner', 'contactMessages'));
    }

    /**
     * Endpoint used to view individual received contact message
     */
    public function showCustomerMessage(string $partnerId, string $contactId)
    {
        $partner = Partner::findOrFail($partnerId);

        $contact = Contact::findOrFail($contactId);

        $contact->update([
            'resolved' => true
        ]);

        return view('frontend.partner.dashboard.inbox.show', compact('partner', 'contact'));
    }
}
