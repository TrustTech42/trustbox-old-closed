<?php

namespace App\Http\Controllers\Partner;

use App\Category;
use App\Http\Controllers\Controller;
use App\Partner;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class PostController extends Controller
{
    /**
     * Endpoint used to open up the create post/blog form
     */
    public function createMyPosts()
    {
        $user = auth()->user();

        $partner = Partner::findOrFail($user->partner->id);

        $categories = Category::all();

        $benefits = $partner->benefits()->get();

        return view('frontend.partner.dashboard.post.create', compact('partner', 'categories', 'benefits'));
    }

    /**
     * Endpoint used to store the created post/blog
     */
    public function storeMyPosts(Request $request)
    {
        $user = auth()->user();

        $partner = Partner::findOrFail($user->partner->id);

        $data = $request->all();

        if ($request->banner) {
            $file = $request->banner->store('public/banners');
            $file = str_replace('public', 'storage', $file);
            $data['banner'] = $file;
        }

        $post = Post::create($data);

        if ($post->notify == 0) {
            $post->update([
                'notify_from' => null
            ]);
        }

        $partner->posts()->attach($post);

        $categories = Input::get('categories');

        $benefitAttached = Input::get('benefitSwitch');

        $benefits = Input::get('benefits');

        $post->categories()->sync($categories);

        if ($benefitAttached == 'on') {
            $post->benefits()->sync($benefits);
        }

        session()->flash('success', 'Post Created.');

        return back();
    }

    /**
     * Endpoint used to list the partners posts.
     */
    public function listMyPosts()
    {
        $user = auth()->user();

        $partner = Partner::findOrFail($user->partner->id);

        $posts = $partner->posts;

        return view('frontend.partner.tabs.posts.index', compact('posts', 'partner'));
    }

    /**
     * Endpoint used to show partners individual posts.
     */
    public function showMyPosts(string $partnerId, string $postId)
    {
        $partner = Partner::findOrFail($partnerId);

        $post = Post::findOrFail($postId);

        $benefits = $partner->benefits()->get();

        $categories = Category::all();

        return view('frontend.partner.dashboard.post.show', compact('partner', 'post', 'benefits', 'categories'));
    }

    /**
     * Endpoint used to delete partners individual posts.
     */
    public function deleteMyPosts(string $partnerId, string $postId)
    {
        $post = Post::findOrFail($postId);

        $post->delete();

        $post->partners()->detach();
        $post->categories()->detach();
        $post->benefits()->detach();

        session()->flash('success', 'Post Deleted.');

        return redirect('partner/' . $partnerId . '/dashboard/post/index');
    }

    /**
     * Endpoint used to update individual post.
     */
    public function updateMyPosts(Request $request, string $partnerId, string $postId)
    {
        $post = Post::findOrFail($postId);

        $data = $request->all();

        if ($request->banner) {
            $file = $request->banner->store('public/banners');
            $file = str_replace('public', 'storage', $file);
            $data['banner'] = $file;
        }

        $benefits = Input::get('benefits');
        $categories = Input::get('categories');
        $unlinkBenefit = Input::get('unlinkBenefit');

        $post->benefits()->sync($benefits);
        $post->categories()->sync($categories);

        if ($unlinkBenefit == 'on') {
            $post->benefits()->detach();
        }

        $post->update($data);

        session()->flash('success', 'Post Updated.');

        return back();
    }

    /**
     * Endpoint used to search through the partners posts
     */
    public function searchMyPosts(Request $request)
    {
        $user = auth()->user();

        $partner = Partner::findOrFail($user->partner->id);

        $search = $request->get('search');

        $posts = $partner->posts()->where(function ($query) use ($search) {
            return $query->whereHas('categories', function ($q) use ($search) {
                $q->where('name', 'like', "%$search%");
            })->orWhere('title', 'like', "%$search%");
        })->paginate(10);

        return view('frontend.partner.dashboard.post.index', compact('posts', 'partner'));
    }
}
