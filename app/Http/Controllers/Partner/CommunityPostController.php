<?php

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\Controller;
use App\Mail\NewPost;
use App\Mail\PublishedPostNotifyMembers;
use App\Mail\PublishedPostNotifyPartner;
use App\Partner;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CommunityPostController extends Controller
{
    /**
     * Endpoint used to show individual post
     */
    public function showPost(string $partnerId, string $postId)
    {
        $partner = Partner::findOrFail($partnerId);

        $post = Post::findOrFail($postId);

        return view('frontend.partner.dashboard.community_posts.show', compact('partner', 'post'));
    }

//    /**
//     * Endpoint used to list available community posts
//     */
//    public function listPosts()
//    {
//        $user = auth()->user();
//
//        $partner = Partner::findOrFail($user->partner->id);
//
//        $connections = $partner->allConnections;
//
//        return view('frontend.partner.dashboard.community_posts.index', compact('connections', 'partner'));
//    }

    /**
     * Endpoint used to update community post
     */
    public function updatePost(Request $request, string $partnerId, string $postId)
    {
        // grab values from toggles and dropdowns
        $published = $request->get('published');
        $notify_members = $request->get('notify_members');
        $benefit = $request->get('benefits');

        // checks if the toggle has been turned on to publish
        if ($published == 1) {
            $partner = Partner::findOrFail($partnerId);
            $post = Post::findOrFail($postId);

            // creates a new pivot record for the partner publishing the connected post
            $post->partners()->attach($partner);

            // checks if benefit has/hasn't been selected
            if (!$benefit == '0') {
                $linked_benefit = 1;
            }
            else {
                $linked_benefit = 0;
            }

            // updates that pivot record to published and assigns the connection id to match the posts original owner
            Post::findOrFail($post->id)->partners()->updateExistingPivot($partner->id, [
                'published' => 1,
                'connection' => $post->partners->first()->name,
                'linked_benefit' => $linked_benefit
            ]);

            // email post owner of the publish
//            Mail::to($post->partners->first()->contact_email)
//                ->send(new PublishedPostNotifyPartner($partner, $post->partners->first(), $post));
        }

        // if toggle is on send email to partner members
//        if ($notify_members == 1) {
//            $this->sendPublishEmail($partner, $post);
//        }

        session()->flash('success', 'Post published.');

        return redirect('partner/' . $partnerId . '/dashboard/active_posts/index');
    }

    /**
     * Endpoint used to search through connected partner posts
     */
    public function searchPosts(Request $request)
    {
        $user = auth()->user();

        $partner = Partner::findOrFail($user->partner->id);

        $search = $request->get('search');

        // grab posts from connected partners where partner_id = logged in partner
        $connections1 = $partner->connections()->with(['posts' => function ($query) use ($search) {
            $query->where('title', 'like', "%$search%");
        }])->get();

        // grab posts from connected partners where connected_id = logged in partner
        $connections2 = $partner->connections2()->with(['posts' => function ($query) use ($search) {
            $query->where('title', 'like', "%$search%");
        }])->get();

        // merge both queries together
        $connections = $connections1->merge($connections2);

        return view('frontend.partner.dashboard.community_posts.index', compact('connections', 'partner'));
    }

    /**
     * Endpoint used to filter connections on posts
     */
    public function filterConnections(Request $request)
    {
        $user = auth()->user();

        $partner = Partner::findOrFail($user->partner->id);

        $filter = $request->get('filter');

        $connections = $partner->allConnections->where('name', '=', $filter)
            ->where('pivot.approved', 1)->all();

        return view('frontend.partner.dashboard.community_posts.index',
            compact('connections', 'partner')
        );
    }

    /**
     * Endpoint used to filter categories on posts
     */
    public function filterCategories(Request $request)
    {
        $user = auth()->user();

        $partner = Partner::findOrFail($user->partner->id);

        $filter = $request->get('filter');

        // grab posts from connected partners where partner_id = logged in partner
        $connections1 = $partner->connections()->with(['posts' => function ($query) use ($filter) {
            $query->whereHas('categories', function ($subQuery) use ($filter) {
                $subQuery->where('name', '=', $filter);
            });
        }])->get();

        // grab posts from connected partners where connected_id = logged in partner
        $connections2 = $partner->connections2()->with(['posts' => function ($query) use ($filter) {
            $query->whereHas('categories', function ($subQuery) use ($filter) {
                $subQuery->where('name', '=', $filter);
            });
        }])->get();

        // merge both queries together
        $connections = $connections1->merge($connections2);

        return view('frontend.partner.dashboard.community_posts.index',
            compact('connections', 'partner')
        );
    }

//    /**
//     * Private function to send email to partner members
//     */
//    private function sendPublishEmail($partner, $post)
//    {
//        foreach ($partner->members as $member) {
//            Mail::to($member->email)->send(new PublishedPostNotifyMembers($partner, $member, $post));
//        }
//
//        return $this;
//    }
}
