<?php

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\Controller;
use App\Message;
use App\Partner;
use Illuminate\Http\Request;

class BroadcastController extends Controller
{
    /**
     * Endpoint used to open up create broadcast message form.
     */
    public function create()
    {
        $user = auth()->user();

        $partner = Partner::findOrFail($user->partner->id);

        return view('frontend.partner.tabs.broadcasts.create', compact('partner'));
    }


    /**
     * Endpoint used to store a new partner broadcast message.
     */
    public function store(Request $request)
    {
        $user = auth()->user();

        $partner = Partner::findOrFail($user->partner->id);

        $request->validate([
            'headline' => 'required|string',
            'author' => 'required|string',
            'body' => 'required|string'
        ]);

        $message = Message::create([
            'sender_id' => $partner->id,
            'headline' => $request->headline,
            'author' => $request->author,
            'body' => $request->body,
            'type' => 'broadcast',
            'colour' => $request->colour,
            'is_read' => 0,
            'email_sent' => 0,
            'valid_from' => $request->valid_from
        ]);

        if ($request->banner) {
            $banner = $request->banner->store('/public/banners');
            $banner = str_replace('public', 'storage', $banner);
            $data['banner'] = $banner;

            $message->update([
                'banner' => $banner
            ]);
        }

        session()->flash('success', 'Broadcast Sent.');

        return redirect('/partner/' . $partner->id . '/dashboard/broadcast/index');
    }

    /**
     * Endpoint used to list previous partner broadcast messages.
     */
    public function list()
    {
        $user = auth()->user();

        $partner = Partner::findOrFail($user->partner->id);

        $broadcasts = $partner->broadcasts;

        return view('frontend.partner.tabs.broadcasts.index', compact('broadcasts', 'partner'));
    }

    /**
     * Endpoint used to show an individual partner message
     */
    public function show(string $partnerId, string $messageId)
    {
        $partner = Partner::findOrFail($partnerId);

        $broadcast = Message::findOrFail($messageId);

        return view('frontend.partner.tabs.broadcasts.show', compact('broadcast', 'partner'));
    }
}
