<?php


namespace App\Http\Controllers;


use App\BenefitContact;
use App\Mail\BenefitForm;
use App\Mail\NotifyProvider;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class BenefitContactController extends Controller
{
    /**
     * Endpoint used for submitting the benefit form and firing off an email
     *
     * @param Request $request
     * @param string $id
     */
    public function benefitForm(Request $request, string $id)
    {
        $setting = Setting::find(1);

        $user = auth()->user();

        $benefit = $user->benefits->find($id);

        $benefitContact = BenefitContact::create($request->all());

        $content = str_replace('@provider@', $benefit->provider->name, $setting->benefit_form_content);
        $content = str_replace('@customer_number@', $benefitContact->customer_number, $content);
        $content = str_replace('@telephone@', $benefitContact->telephone, $content);
        $content = str_replace('@email@', $benefitContact->email, $content);
        $content = str_replace('@name@', $benefitContact->first_name . ' ' . $benefitContact->last_name, $content);
        $content = str_replace('@benefit_name@', $benefitContact->benefit_name, $content);
        $content = str_replace('@enquiry@', $benefitContact->enquiry, $content);

        $vars = new \stdClass();
        $vars->email = $benefit->provider_email;
        $vars->template = 'mail.benefit-form';
        $vars->partner = $user->membership->partner;
        $vars->subject = 'Benefit Enquiry - ' . $benefit->name;
        $vars->content = $content;

        Mail::to($vars->email)->send(new BenefitForm($vars));

        return redirect('benefits/' . $benefit->id . '/form-success');
    }

    /**
     * Endpoint used for firing off an email to a benefit provider
     *
     * @param string $id
     */
    public function benefitNotify(string $id)
    {
        $setting = Setting::find(1);

        $user = auth()->user();
        $benefit = $user->benefits->find($id);

        $content = str_replace('@provider@', $benefit->provider->name, $setting->benefit_notify_content);
        $content = str_replace('@name@', $user->name, $content);
        $content = str_replace('@customer_number@', $user->customer_number, $content);
        $content = str_replace('@email@', $user->email, $content);
        $content = str_replace('@telephone@', $user->telephone, $content);
        $content = str_replace('@benefit_name@', $benefit->name, $content);

        $vars = new \stdClass();
        $vars->email = $benefit->provider_email;
        $vars->template = 'mail.call-benefit-provider';
        $vars->subject = 'Expect A Call - ' . $benefit->name;
        $vars->partner = $user->membership->partner;

        $vars->content = $content;

        Mail::to($vars->email)->send(new NotifyProvider($vars));

        return redirect('benefits/' . $benefit->id . '/call-success');
    }

    /**
     * @param string $id
     */
    public function formSuccess(string $id)
    {
        $user = auth()->user();

        $benefit = $user->benefits->find($id);

        return view('frontend.benefits.contact.form-contact-success', compact('user', 'benefit'));
    }

    /**
     * @param string $id
     */
    public function callSuccess(string $id)
    {
        $user = auth()->user();

        $benefit = $user->benefits->find($id);

        return view('frontend.benefits.contact.call-contact-success', compact('user', 'benefit'));
    }
}
