<?php

namespace App\Http\Controllers;

use App\Mail\BenefitRequested;
use App\Mail\NotifyProvider;
use App\Membership;
use App\Plan;
use GuzzleHttp\Client;
use App\Partner;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use App\Benefit;
use App\User;
use Illuminate\View\View;

class BenefitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();

        if (isset($user)) {
            $partner_id = $user->membership->partner_id;
            $partner = Partner::where('id', $partner_id)->first();
         }

        return view('frontend.benefits.index', compact('partner'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = auth()->user() ? User::findOrFail(auth()->user()->id) : new \stdClass();

        $benefit = Benefit::find($id);

        if (isset($user->subscription)) {
            $newSubscriptionCost = (int)$benefit->price + (int)$user->subscription->total;
        }

        if (isset($user->membership)) {
            $membership = Plan::find($user->membership()->latest()->first()->plan_id)->name;
        }

        return view('frontend.benefits.show', compact('benefit', 'user', 'newSubscriptionCost', 'membership'));
    }

    public function enrol(Request $request)
    {
        $hash = $request->get('hash');

        $userId = $request->get('user_id');
        $benefitId = $request->get('benefit_id');

        $user = User::findorfail($userId);
        $userBenefits = $user->benefits;
        $finalUserBenefits = [];
        $hasFileManager = false;

        foreach ($userBenefits as $benefit) {
            array_push($finalUserBenefits, $benefit->id);
        }

        array_push($finalUserBenefits, $benefitId);

        $user->benefits()->sync($finalUserBenefits);

        $benefit = $user->benefits()->latest()->first();

        $setting = Setting::find(1);

        $content = str_replace('@name@', $user->name, $setting->enrolled_email_content);
        $content = str_replace('@benefit_name@', $benefit->name, $content);

        $url = url()->current();
        $url = explode('/', $url);
        $url = explode('.', $url[2]);
        $subdomain = $url[0];
        $partner = Partner::where('subdomain', "$subdomain")->with('posts')->first();

        if ($partner === null) {
            $partner = Partner::find(Setting::where('name', 'default_partner')->first()->value);
        }

        $vars = new \stdClass();
        $vars->email = $user->email;
        $vars->template = 'mail.benefit-enrol';
        $vars->subject = 'Benefit Enrol';
        $vars->partner = $partner;
        $vars->content = $content;

        Mail::to($vars->email)->send(new BenefitRequested($vars));

        if ($benefitId === "2" || Benefit::find($benefitId)->includes_file_manager) {
            $user->update([
                'file_manager_trial_date' => Carbon::now()->addDays(31),
                'storage' => 5
            ]);
        }

        session()->flash('success', 'Successfully enrolled to benefit');

        return Redirect::to(URL::previous() . "#" . $hash);
    }

    /**
     * Endpoint which is used to POST to NoChex with the benefit updating params
     *
     * @param string $id
     */
    public
    function subsequentBoost(string $id)
    {
        $user = User::findOrFail(auth()->user()->id);
        $subscription = $user->subscription()->first();
        $schedule_id = $subscription->schedule_id;
        $benefit = $user->benefits()->find($id);

        $client = new Client;

        // creates a Guzzle POST request to the NoChex listener page with the appropriate headers and params for NEW_BOOST
        $client->request('POST', 'https://secure.nochex.com/schedulelistener.aspx', [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Host' => 'secure.nochex.com'
            ],
            'form_params' => [
                'action' => 'NEW_BOOST',
                'schedule_id' => $schedule_id,
                'amount' => $benefit->price,
                'boost_id' => $benefit->id,
                'optional_1' => $benefit->provider_id, $benefit->provider_percentage_cut,
                'optional_2' => $user->membership->partner->id, $benefit->partner_percentage_cut
            ]
        ]);

        return redirect('/dashboard?fragment=boosted_benefits');
    }

    /**
     * Endpoint which is used to POST to NoChex with the benefit cancellation params
     *
     * @param string $id
     */
    public
    function cancelSubsequentBoost(string $id)
    {
        $user = User::findOrFail(auth()->user()->id);
        $subscription = $user->subscription()->first();
        $schedule_id = $subscription->schedule_id;
        $benefit = $user->benefits()->find($id);

        $client = new Client;

        // creates a Guzzle POST request to the NoChex listener page with the appropriate headers and params for CANCEL_BOOST
        $client->request('POST', 'https://secure.nochex.com/schedulelistener.aspx', [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Host' => 'secure.nochex.com'
            ],
            'form_params' => [
                'action' => 'CANCEL_BOOST',
                'schedule_id' => $schedule_id,
                'boost_id' => $benefit->id,
            ]
        ]);

        return redirect('/dashboard?fragment=boosted_benefits');
    }

    /**
     * Method which returns the success page after the first initial boost payment.
     *
     * @return View
     */
    public
    function boostSuccess()
    {
        return redirect('/dashboard?fragment=boosted_benefits');
    }

    /**
     * Method for returning the benefit in the payment view
     *
     * @param string $id
     *
     * @return View
     */
    public
    function boostBenefitNoChex(string $id)
    {
        $user = Auth::user();

        $benefit = Benefit::findOrFail($id);

        return view('frontend.users.nochex.boost', compact('benefit', 'user'));
    }

    public
    function redeem($id)
    {
        $user = Auth::user();

        $benefit = Benefit::findorfail($id);

        return view('frontend.benefits.redeem', compact('benefit', 'user'));
    }
}
