<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class OtherController extends Controller
{
    public function showDashboard(Request $request)
    {
        $user = auth()->user();
        $hash = $request->get('fragment');

        !is_null($request->get('upgrade')) ? session()->put('upgrade', ($request->get('upgrade') === 'true')) : '';

        if($user)
        {
            $role = strtolower($user->role->name);

            switch ($role) {
                case 'provider':
                    return redirect('/provider/' . $user->provider->id);
                    break;
                case 'partner':
                    return redirect('/partner/' . $user->partner->id);
                    break;
                case 'staff':
                    return redirect('/admin');
                    break;
                case 'admin':
                    return redirect('/admin');
                    break;
            }

            return redirect('/user/' . $user->id . '#'. $hash);
        }

        return redirect()->route('login');
    }
}
