<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use App\Http\Controllers\SmsVerificationController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;

use App\SmsVerification;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    protected function authenticated(Request $request, $user)
    {
        if (strtolower($user->role->name) == 'pending') {

            return redirect()->route('create.payment', $user->id);
        }

//        if(strtolower($user->role->name) == 'admin' || strtolower($user->role->name) == 'staff')
//        {
//            return redirect()->route('sms-verification.send.code', compact('user'));
//        }

        $user->changelogs()->create(['action' => 'User has logged in']);

        return redirect('/dashboard');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
}
