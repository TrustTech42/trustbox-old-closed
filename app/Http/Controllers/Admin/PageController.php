<?php

namespace App\Http\Controllers\Admin;

use App\Benefit;
use App\Page;
use Illuminate\Http\Request;

class PageController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::all();

        return view('admin.pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $page = Page::create($request->all());

//        if ($page->template == 'benefit') {
//            $page->blocks()->create(
//                [
//                    'name'       => 'Redeem',
//                    'template'   => 'block-redeem',
//                    'content'    => '<div class="block block-redeem text-white col-12">
//                                        <div class="block-inner bg-dark rounded p-4">
//                                            <div class="a-center">
//                                                <h4 class="h4 mb-0">
//                                                     <span id="redeemText">To redeem this benefit</span>
//                                                    <a href="#" class="btn btn-primary ml-5 uneditable">REDEEM</a>
//                                                </h4>
//                                            </div>
//                                        </div>
//                                    </div>',
//                    'sort_order' => 1
//                ]
//            );
//
//            $page->blocks()->create(
//                [
//                    'name'       => 'Boost',
//                    'template'   => 'block-boost',
//                    'content'    => '<div class="block block-boosted text-white col-12">
//                                        <div class="block-inner bg-partner rounded px-5 py-3">
//                                            <h2 class="h2 text-center mb-4">
//                                                Boosted for Plus
//                                                <i class="fa fa-bars"></i>
//                                                Partners
//                                            </h2>
//                                            <span contenteditable="true">
//                                                Velit illo labore perspiciatis quas. Libero occaecati aliquid maiores sit praesentium consequatur.
//                                                Suscipit et laborum et blanditiis consequuntur aut molestiae. Similique et natus omnis numquam.
//                                            </span>
//                                            <h3 class="h3 text-center mt-3">
//                                                <a href="/dashboard?hash=boosted_benefits" class="btn btn-default bg-white text-member">
//                                                BOOST THIS BENEFIT</a>
//                                            </h3>
//                                        </div>
//                                    </div>',
//                    'sort_order' => 1
//                ]
//            );
//        }

        $request->session()->flash('status', 'Page Created.');

        return redirect()->route('pages.edit', compact('page'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        $benefits = Benefit::all();

        return view('admin.pages.edit', compact('page', 'benefits'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Page $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        $page->update($request->all());
        $request->session()->flash('status', "{$page->name} Updated.");

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        //
    }
}
