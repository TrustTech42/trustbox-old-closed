<?php

namespace App\Http\Controllers\Admin;

use App\Provider;
use App\User;
use App\Role;
use Illuminate\Http\Request;

class ProviderController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $providers = Provider::all();

        return view('admin.providers.index', compact('providers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role  = Role::where('name', 'Provider')->first();
        $users = User::where('role_id', '!=', $role->id)->pluck('email', 'id');

        return view('admin.providers.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:providers'
        ]);

        $data = $request->all();

        if ($request->provider_logo) {
            $logo         = $request->provider_logo->store('public/logos');
            $logo         = str_replace('public', 'storage', $logo);
            $data['provider_logo'] = $logo;
        }

        Provider::create($data);

        session()->flash('success', 'New Provider added successfully.');

        return redirect('/admin/providers/');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Provider $provider
     * @return \Illuminate\Http\Response
     */
    public function show(Provider $provider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Provider $provider
     * @return \Illuminate\Http\Response
     */
    public function edit(Provider $provider)
    {
        return view('admin.providers.edit', compact('provider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Provider $provider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Provider $provider)
    {
        $data = $request->all();

        if ($request->provider_logo) {
            $logo         = $request->provider_logo->store('public/logos');
            $logo         = str_replace('public', 'storage', $logo);
            $data['provider_logo'] = $logo;
        }

        $provider->update($data);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Provider $provider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Provider $provider)
    {
        $provider->delete();

        session()->flash('success', 'Provider Deleted.');

        return back();
    }

    public function search(Request $request)
    {
        $search = $request->get('search');

        $providers = Provider::where('id', 'like', "%$search%")
                     ->orWhere('name', 'like', "%$search%")
                     ->orWhereHas('user', function ($q) use ($search) {
                         $q->where('name', 'like', "%$search%")
                             ->orWhere('email', 'like', "%$search%");
                     })->get();

        $search_active = true;

        return view('admin.providers.index', compact('providers', 'search_active'));
    }
}
