<?php

namespace App\Http\Controllers\Admin;

use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ContactController extends AdminController
{
    public function index(){
        $contacts = Contact::all();
        $filter = $filter2 = '';

        return view('admin.contacts.index', compact('contacts', 'filter', 'filter2'));
    }

    public function show(Contact $contact){
        return view('admin.contacts.show', compact('contact'));
    }

    public function update(Contact $contact, Request $request){

        $request['user_id'] = auth::user()->id;
        $contact->update($request->all());

        session()->flash('success', 'Message marked as resolved.');
        return redirect('/admin/contacts');
    }

    public function filter(Request $request)
    {
        $filter = $request->get('filter') ?? '';
        $filter2 = $request->get('filter2') ?? '';

        $contacts = Contact::where('resolved', 'like', "%$filter%")->where('reason_id', $filter2)->get();


        $filter_active = true;

        return view('admin.contacts.index', compact('contacts', 'filter_active', 'filter', 'filter2'));
    }
}
