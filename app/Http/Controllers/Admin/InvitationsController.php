<?php

namespace App\Http\Controllers\Admin;

use App\Invitations;
use App\Mail\CodeInvite;
use App\Partner;
use App\Provider;
use App\Setting;
use iio\libmergepdf\Merger;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class InvitationsController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $email_invites = Invitations::where('type', '=', 'email')->get();
        $mail_invites = Invitations::where('type', '=', 'mail')->get();
        $providers = Provider::all()->pluck('name', 'name');
        $type = 'invites';

        return view('admin.invites.index', compact('email_invites', 'mail_invites', 'providers', 'type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'type' => 'required|in:mail,email',
            'csv' => 'required|mimes:csv,txt'
        ]);

        $file = $request->file('csv');
        $type = $request->get('type');

        $csv = array_map('str_getcsv', file($file));
        array_shift($csv);

        foreach ($csv as $c) {
            if ($type == 'email') {
                if (empty($c[0]) || empty($c[1]) || empty($c[7])) {
                    return back()->withErrors('The uploaded file has missing values');
                }
            } else {
                if (empty($c[0]) || empty($c[2]) || empty($c[3]) || empty($c[4] || empty($c[5]) || empty($c[6]) || empty($c[7]))) {
                    return back()->withErrors('The uploaded file has missing values');
                }
            }
        }

        foreach ($csv as $c) {
            $vars = [];
            $vars['name'] = $c[0];
            $vars['email'] = $c[1];
            $vars['address1'] = $c[2];
            $vars['address2'] = $c[3];
            $vars['address3'] = $c[4];
            $vars['address4'] = $c[5];
            $vars['postcode'] = $c[6];
            $vars['partner'] = $c[7];
            $vars['code'] = rand(1000, 9999);
            $vars['status'] = 'pending';
            $vars['type'] = $type;

            Invitations::create($vars);
        }

        $invitations = Invitations::where('status', '=', 'pending')->where('type', '=', $type)->get();
        $setting = Setting::find(1);

        if ($type == 'email') {

            foreach ($invitations as $i) {
                $email_vars = new \stdClass();
                $email_vars->partner = Partner::where('name', '=', $i->partner)->first();
                $email_vars->id = $i->id;
                $email_vars->name = $i->name;
                $email_vars->email = $i->email;
                $email_vars->code = $i->code;

                if ($email_vars->partner->code_invite_content) {
                    $content = str_replace('@name@', $i->name, $email_vars->partner->code_invite_content);
                }
                else {
                    $content = str_replace('@name@', $i->name, $setting->code_invite_email_content);
                }

                $content = str_replace('@code@', $i->code, $content);
                $content = str_replace('@partner@', $i->partner, $content);

                $email_vars->content = $content;

                Mail::to($email_vars->email)->send(new CodeInvite($email_vars));

                $i->update([
                    'status' => 'sent'
                ]);

            }

            session()->flash('success', 'Users successfully imported and emailed');
            return redirect()->route('invitations.index', ['#email']);

        } else {
            foreach ($invitations as $i) {
                $content = str_replace('@name@', $i->name, $setting->invitation_letter_content);
                $content = str_replace('@code@', $i->code, $content);
                $content = str_replace('@partner@', $i->partner, $content);

                $mail_vars = new \stdClass();
                $mail_vars->partner = Partner::where('name', '=', $i->partner)->first();
                $mail_vars->id = $i->id;
                $mail_vars->name = $i->name;
                $mail_vars->address1 = $i->address1;
                $mail_vars->address2 = $i->address2;
                $mail_vars->address3 = $i->address3;
                $mail_vars->address4 = $i->address4;
                $mail_vars->postcode = $i->postcode;
                $mail_vars->content = $content;

                $inv = new Invitations();
                $path = $inv->createInvitation($mail_vars);

                $i->update([
                    'url' => $path
                ]);
            }

            session()->flash('success', 'Users successfully imported and letters generated');
            return redirect()->route('invitations.index', ['#mail']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Invitations $invitations
     * @return \Illuminate\Http\Response
     */
    public function show(Invitations $invitations)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Invitations $invitations
     * @return \Illuminate\Http\Response
     */
    public function edit(Invitations $invitations)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Invitations $invitations
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invitations $invitations)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Invitations $invitations
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invitations $invitations)
    {
        //
    }

    public function print(Request $request)
    {
        $request->validate([
            'fields' => 'required'
        ]);

        $emails = $request['fields']['check'];
        $merger = new Merger();

        foreach ($emails as $e) {

            $invitation = Invitations::find($e);

            $invitation->update([
                'status' => 'printed'
            ]);

            $path = $invitation->url;

            $merger->addFile($path);
        }

        $pdf = $merger->merge();

        $path = 'public/letters/letter-invitations-' . time() . '.pdf';

        Storage::disk('local')->put($path, $pdf);

        $url = Storage::url($path);

        $url = str_replace('/public/', '/storage/', $url);

        return redirect($url);
    }

    public function send(Request $request)
    {
        $request->validate([
            'fields' => 'required'
        ]);

        $emails = $request['fields']['check'];

        $setting = Setting::find(1);

        foreach ($emails as $e) {
            $code = rand(1000, 9999);

            $invitation = Invitations::where('email', '=', $e)->where('type', '=', 'email')->latest()->first();
            $content = str_replace('@name@', $invitation->name, $setting->code_invite_email_content);
            $content = str_replace('@code@', $code, $content);
            $content = str_replace('@partner@', $invitation->partner, $content);


            $invitation->update([
                'code' => $code
            ]);

            $email_vars = new \stdClass();
            $email_vars->partner = Partner::where('name', '=', $invitation->partner)->first();
            $email_vars->id = $invitation->id;
            $email_vars->name = $invitation->name;
            $email_vars->email = $invitation->email;
            $email_vars->code = $invitation->code;
            $email_vars->content = $content;

            Mail::to($email_vars->email)->send(new CodeInvite($email_vars));
        }


        session()->flash('success', 'Users successfully emailed');
        return redirect()->route('invitations.index', ['#email']);
    }

    public function search(Request $request)
    {
        $search = $request->get('search');
        $type = $request->get('type');

        $email_invites = Invitations::where('name', 'like', "%$search%")->orWhere('email', 'like', "%$search%")->where('type', '=', $type)->get();
        $mail_invites = Invitations::where('type', '=', 'mail')->get();

        $providers = Provider::all()->pluck('name', 'name');

        $search_active = true;

        return view('admin.invites.index', compact('email_invites', 'mail_invites', 'providers', 'search_active', 'type'));
    }

    public function filter(Request $request)
    {
        $filter = $request->get('filter');
        $type = $request->get('type');

        $mail_invites = Invitations::where('status', '=', $filter)->where('type', '=', $type)->get();
        $email_invites = Invitations::where('status', '=', $filter)->where('type', '=', $type)->get();

        $providers = Provider::all()->pluck('name', 'name');

        $filter_active = true;

        return view('admin.invites.index', compact('email_invites', 'mail_invites', 'providers', 'filter_active', 'type'));
    }

    public function filterProviders(Request $request)
    {
        $filter = $request->get('filter');
        $type = $request->get('type');

        $mail_invites = Invitations::where('partner', '=', $filter)->where('type', '=', $type)->get();
        $email_invites = Invitations::where('partner', '=', $filter)->where('type', '=', $type)->get();

        $providers = Provider::all()->pluck('name', 'name');

        $filter_providers_active = true;

        return view('admin.invites.index', compact('email_invites', 'mail_invites', 'providers', 'filter_providers_active', 'type'));
    }

    /**
     * Function used to download CSV file
     */
    public function download()
    {
        return response()->download(storage_path("app/public/template.csv"));
    }
}
