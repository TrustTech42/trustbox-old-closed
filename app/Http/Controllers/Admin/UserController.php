<?php

namespace App\Http\Controllers\Admin;

use App\Exports\UsersExport;
use Illuminate\Http\Request;
use App\Membership;
use App\User;
use App\Role;
use App\Plan;
use App\Partner;
use App\Provider;
use App\Permission;
use App\Folder;
use App\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::withTrashed()->paginate(10);
        $roles = Role::all()->pluck('name', 'name');

        return view('admin.users.index', compact('users', 'roles'));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('admin.users.show', compact('user'));
    }

    /**
     * Display the form for editing a User
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($user)
    {
        $user = User::withTrashed()->findOrFail($user);
        $roles = Role::all()->pluck('name', 'id');
        $plans = Plan::all()->pluck('name', 'id');
        $users = User::all()->pluck('name', 'id');
        $partners = Partner::all()->pluck('name', 'id');
        $providers = Provider::all()->pluck('name', 'id');
        $isAdmin = auth()->user()->role->name === "Admin";
        $changelogs = $user->changelogs()->paginate(10);

        return view('admin.users.edit', compact('user', 'plans', 'roles', 'partners', 'providers', 'users', 'isAdmin', 'changelogs'));
    }

    /**
     * Save changes to a User using the given data
     *
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $user)
    {
        $user = User::withTrashed()->findOrFail($user);

        $request->validate([
            'password' => 'nullable:sometimes|string|min:6|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
            'mobile' => 'sometimes|required|regex:/^(7)([0-9]{9})$/'
        ]);

        if($request->get('password')){
            $request['password'] = bcrypt($request->get('password'));
        }

        $request['mobile'] = $request->get('mobile');

        $user->update($request->all());
        $role = $request->get('role_id');


        if ($role && $role == Role::where('name', 'User')->first()->id) {
            $user->membership->plan_id = $request->get('plan_id');
            $user->membership->activated = $request->get('activated');
            $user->membership->partner_id = $request->get('partner_id');
            $user->membership->save();
        }


        if ($role && $role == Role::where('name', 'Partner')->first()->id) {
            $partner = Partner::where('user_id', $user->id)->first();

            if ($partner) {
                $partner->user_id = null;
                $partner->save();
            }

            $partner = Partner::find($request->get('partner'));
            $partner->user_id = $user->id;
            $partner->save();
        }

        if ($role && $role == Role::where('name', 'Provider')->first()->id) {
            $provider = Provider::where('user_id', $user->id)->first();

            if ($provider) {
                $provider->user_id = null;
                $provider->save();
            }

            $provider = Provider::find($request->get('provider'));
            $provider->user_id = $user->id;
            $provider->save();
        }

        if ($role && $role == Role::where('name', 'Staff')->first()->id) {
            Permission::where('user_id', $user->id)->delete();
            $permissions = $request->get('permission');

            if ($permissions) {
                foreach ($permissions as $name => $status) {
                    if ($status === 'on') {
                        $user->permissions()->create([
                            'name' => $name
                        ]);
                    }
                }
            }
        }

        $user->changelogs()->create(['action' => 'Admin updated user information']);

        if (!$request->get('activated')) {
            $user->delete();
        }

        if ($request->get('activated') && $user->trashed()) {
            $user->restore();
        }

        if ($request->get('referred'))
            $user->referred = $request->get('referred');

        session()->flash('success', 'User updated successfully.');

        return back();
    }

    public function create()
    {
        $roles = Role::all()->pluck('name', 'id');
        $plans = Plan::all()->pluck('name', 'id');
        $users = User::all()->pluck('name', 'id');
        $partners = Partner::all()->pluck('name', 'id');
        $providers = Provider::all()->pluck('name', 'id');


        return view('admin.users.create', compact('roles', 'plans', 'partners', 'providers', 'users'));
    }

    public function store(Request $request)
    {
        // Prepare data for saving
        $data = $request->all();
        $data['name'] = $data['first_name'] . ' ' . $data['last_name'];
        $data['password'] = bcrypt($request->password ? $request->password : 'password');
        $data['change_password'] = true;
        $data['customer_number'] = $this->createCustomerNumber();

        // Create user
        $user = User::create($data);


        if (isset($data['role_id']) && ($data['role_id'] == Role::where('name', 'User')->first()->id)) {
            $membership = Membership::create([
                'user_id' => $user->id,
                'partner_id' => $data['partner_id'],
                'plan_id' => $data['plan_id'],
                'activated' => $data['activated']
            ]);

            $user->membership()->save($membership);
        }

        if (isset($data['role_id']) && $data['role_id'] == Role::where('name', 'Partner')->first()->id) {
            $partner = Partner::find($data['partner']);
            $partner->user_id = $user->id;
            $partner->save();
        }

        if (isset($data['role_id']) && $data['role_id'] == Role::where('name', 'Provider')->first()->id) {
            $provider = Provider::find($data['provider']);
            $provider->user_id = $user->id;
            $provider->save();
        }

        // Return Success message back to user
        session()->flash('success', 'User created successfully.');

        // Redirect back
        return back();
    }

    private function createCustomerNumber()
    {
        $customerNumber = "C" . rand(100000000, 999999999);
        $customer = User::where('customer_number', $customerNumber)->first();

        if (!$customer)
            return $customerNumber;

        return $this->createCustomerNumber();
    }

    public function search(Request $request)
    {
        $search = $request->get('search');

        $users = User::where('customer_number', 'like', "%$search%")
            ->orWhere('name', 'like', "%$search%")
            ->orWhere('first_name', 'like', "%$search%")
            ->orWhere('last_name', 'like', "%$search%")
            ->orWhere('email', 'like', "%$search%")
            ->orWhereHas('role', function ($q) use ($search) {
                $q->where('name', 'like', "%$search%");
            })->paginate(10);

        $search_active = true;
        $roles = Role::all()->pluck('name', 'name');

        return view('admin.users.index', compact('users', 'search_active', 'roles'));
    }

    public function filter(Request $request)
    {
        $filter = $request->get('filter');

        $users = User::withTrashed()->whereHas('role', function ($q) use ($filter) {
            $q->where('name', '=', $filter);
        })->paginate(10);

        $roles = Role::all()->pluck('name', 'name');

        $filter_active = true;

        return view('admin.users.index', compact('users', 'roles', 'filter_active'));
    }

    /**
     * Export users to CSV
     */
    public function export()
    {
        return Excel::download(new UsersExport, 'users.csv');
    }
}
