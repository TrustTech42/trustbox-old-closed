<?php

namespace App\Http\Controllers\Admin;

use App\Reason;
use Illuminate\Http\Request;

class ReasonController extends AdminController
{
    public function store(Request $request)
    {
        if(Reason::create($request->all())){
            session()->flash('success', 'Reason successfully added.');
        }else{
            session()->flash('success', 'Failed to create reason.');
        }

        return back();
    }

    public function massUpdate(Request $request){
        $data = $request->get('reasons');
        $reasons = Reason::all();

        foreach($reasons as $reason){
            $reason->visible = false;
            $reason->save();
        }

        foreach($data as $reason){
            $reason = Reason::find($reason);
            $reason->visible = true;
            $reason->save();
        }

        return back();
    }
}
