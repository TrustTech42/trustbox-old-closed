<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Setting;
use App\Partner;
use App\Reason;

class SettingController extends AdminController
{
    public function index()
    {
        $settings = Setting::all();
        $partners = Partner::all()->pluck('name', 'id');
        $reasons = Reason::pluck('name', 'id');
        $visibleReasons = Reason::where('visible', true)->pluck('id');

        return view('admin.settings.index', compact('settings', 'partners', 'reasons', 'visibleReasons'));
    }

    public function massUpdate(Request $request)
    {
        $data = $request->all();


        foreach ($data as $name => $value) {
            $setting = Setting::find(1);

            if ($setting->$name) {
                $setting->$name = $value;
                $setting->save();
            }
        }

        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Setting $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        $setting          = Setting::find(1);
        $emailsCollection = Setting::all(
            'code_invite_email_content',
            'signup_email_content',
            'new_user_benefits_content',
            'boosted_email_content',
            'cancelled_boosted_email_content',
            'enrolled_email_content',
            'invite_email_content',
            'removed_email_content',
            'welcome_email_content',
            'benefit_form_content',
            'benefit_notify_content'
        );
        $emails           = $emailsCollection[0]->toArray();

        $lettersCollection = Setting::all('welcome_letter_content', 'invitation_letter_content');
        $letters           = $lettersCollection[0]->toArray();

        return view('admin.emails.edit', compact('emails', 'setting', 'letters'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Setting $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Setting $setting)
    {
        $setting->update($request->all());

        session()->flash('success', 'The Email content has been updated');

        return redirect()->back();
    }

}
