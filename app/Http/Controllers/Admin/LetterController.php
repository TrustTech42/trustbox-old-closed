<?php

namespace App\Http\Controllers\Admin;

use App\Letter;
use App\Provider;
use App\User;
use Illuminate\Http\Request;
use iio\libmergepdf\Merger;
use iio\libmergepdf\Pages;
use Illuminate\Support\Facades\Storage;

class LetterController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users     = User::where('role_id', '=', 1)->get();
        $providers = Provider::all()->pluck('name', 'name');

        return view('admin.letters.index', compact('users', 'providers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Letter $letter
     * @return \Illuminate\Http\Response
     */
    public function show(Letter $letter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Letter $letter
     * @return \Illuminate\Http\Response
     */
    public function edit(Letter $letter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Letter $letter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Letter $letter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Letter $letter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Letter $letter)
    {
        //
    }

    public function print(Request $request)
    {
        $request->validate([
                               'fields' => 'required'
                           ]);

        $user_ids = $request['fields']['check'];
        $merger   = new Merger();
        $count = 0;

        foreach ($user_ids as $id) {
            $user = User::find($id);

            if($user->letter()->exists())
            {
                $user->update([
                                  'letter_printed' => 1
                              ]);

                $path = $user->letter()->latest()->first()->url;

                $merger->addFile($path);

                $count++;
            }
        }

        if($count>0)
        {
            $pdf = $merger->merge();

            $path = 'public/letters/letter-welcome-' . time() . '.pdf';

            Storage::disk('local')->put($path, $pdf);

            $url = Storage::url($path);

            $url = str_replace('/storage/', '/public/', $url);

            return redirect($url);
        }

        return back();
    }

    public function search(Request $request)
    {
        $search = $request->get('search');

        $users = User::where('name', 'like', "%$search%")->get();

        $providers = Provider::all()->pluck('name', 'name');

        $search_active = true;

        return view('admin.letters.index', compact('users', 'search_active', 'providers'));
    }

    public function filterProviders(Request $request)
    {
        $filter = $request->get('filter');

        $users = User::whereHas('provider', function ($q) use ($filter) {
            $q->where('name', '=', $filter);
        })->get();

        $providers = Provider::all()->pluck('name', 'name');

        $filter_providers_active = true;

        return view('admin.letters.index', compact('users', 'filter_providers_active', 'providers'));
    }

    public function filter(Request $request)
    {
        $filter = $request->get('filter');

        $users = User::where('letter_printed', '=', $filter)->get();

        $providers = Provider::all()->pluck('name', 'name');

        $filter_active = true;

        return view('admin.letters.index', compact('users', 'filter_active', 'providers'));
    }
}
