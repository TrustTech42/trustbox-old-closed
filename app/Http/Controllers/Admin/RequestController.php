<?php

namespace App\Http\Controllers\Admin;

use App\Mail\BenefitRequested;
use Illuminate\Http\Request;

use App\Request as UserRequest;
use App\Partner;
use App\Benefit;
use Illuminate\Support\Facades\Mail;

class RequestController extends AdminController
{
    public function index()
    {
        $requests = UserRequest::all();

        return view('admin.requests.index', compact('requests'));
    }

    public function update(Request $request, $userRequest)
    {
        $userRequest         = UserRequest::find($userRequest);

        $userRequest->status = $request->get('status');
        $userRequest->save();

        $partner = Partner::find($userRequest->partner_id);
        $type = $userRequest->type . '('. Benefit::find($userRequest->entity_id)->name . ')';

        if ($userRequest->status === '1' && $userRequest->type === 'benefit' && $userRequest->action === 'enable') {
            $partner->benefits()->attach($userRequest->entity_id);
        }

        if ($userRequest->status === '1' && $userRequest->type === 'benefit' && $userRequest->action === 'disable') {
            $partner->benefits()->detach($userRequest->entity_id);
        }

        $vars = new \stdClass();
        $vars->email = $partner->contact_email;
        $vars->template = 'mail.benefit-request';
        $vars->status = $userRequest->status ? 'Accepted' : 'Rejected';
        $vars->partner = $partner;
        $vars->subject = 'Benefit '.$vars->status;
        $vars->name = $partner->name;
        $vars->type = $type;

        Mail::to($vars->email)->send(new BenefitRequested($vars));

        session()->flash('success', 'Request updated.');

        return back();
    }

    public function filter(Request $request)
    {
        $filter = $request->get('filter');

        $requests = UserRequest::where('status', '=', $filter)->get();

        $filter_active = true;

        return view('admin.requests.index', compact('requests', 'filter_active'));
    }
}
