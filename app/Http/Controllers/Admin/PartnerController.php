<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminController;
use App\Partner;
use App\Provider;
use App\User;
use App\Benefit;
use App\Service;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PartnerController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partners = Partner::all();
        $defaultPartner = Setting::where('name', 'default_partner')->first()->value;

        return view('admin.partners.index', compact('partners', 'defaultPartner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all()->pluck('name', 'id');

        return view('admin.partners.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $partner = Partner::create($request->all());

        if ($request->file('logo')) {
            $file = Storage::put('public/logos', $request->file('logo'));
            $file = "/" . str_replace("public/logos", "", $file);
            $file = str_replace('//', '', $file);

            $partner->logo = $file;
            $partner->save();
        }

        $provider = Provider::create([
            'name' => $partner->name,
            'email' => $partner->contact_email,
            'provider_logo' => $partner->logo,
            'telephone' => $partner->contact_phone,
            'nochex_id' => $partner->nochex_id
        ]);

        $provider->save();

        $partner->update([
           'provider_id' => $provider->id
        ]);

        $partner->save();

        return redirect('admin/partners');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Partner $partner
     * @return \Illuminate\Http\Response
     */
    public function show(Partner $partner)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Partner $partner
     * @return \Illuminate\Http\Response
     */
    public function edit(Partner $partner)
    {
        $users = User::all()->pluck('name', 'id');
        $benefits = Benefit::where('unique', '!=', true)->get();
        $services = Service::all();
        $unique_benefits = $partner->benefits()->exists() ? $partner->benefits()->where('unique', 1)->get() : false;

        return view('admin.partners.edit', compact('partner', 'users', 'benefits', 'services', 'unique_benefits'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Partner $partner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Partner $partner)
    {
        $data = $request->all();
        $benefits = [];
        $services = [];

        if (isset($data['benefits']))
            $benefits = array_keys(array_filter($request->get('benefits')));

        if (isset($data['services']))
            $services = array_keys(array_filter($request->get('services')));

        $partner->benefits()->sync($benefits);
        $partner->services()->sync($services);
        $partner->update($data);

        if ($request->file('logo')) {
            $file = Storage::put('public/logos', $request->file('logo'));
            $file = "/" . str_replace("public/logos", "", $file);
            $file = str_replace('//', '', $file);

            $partner->logo = $file;
            $partner->save();
        }

        session()->flash('success', 'Partner information updated.');

        return redirect('/admin/partners/' . $partner->id . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Partner $partner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Partner $partner)
    {
        $benefits = $partner->benefits();
        $partner->delete();

        session()->flash('success', 'Partner deleted successfully. Please note, users that were assigned to this partner are now orphans.');

        return back();
    }

    public function search(Request $request)
    {
        $search = strtolower($request->get('search'));

        if ($search == 'yes') {
            $search = 1;
        } else if ($search == 'no') {
            $search = 0;
        }

        $partners = Partner::where('id', 'like', "%$search%")
            ->orWhere('name', 'like', "%$search%")
            ->orWhere('active', 'like', "%$search%")
            ->get();

        $search_active = true;

        $defaultPartner = Setting::where('name', 'default_partner')->first()->value;

        return view('admin.partners.index', compact('partners', 'defaultPartner', 'search_active'));
    }
}
