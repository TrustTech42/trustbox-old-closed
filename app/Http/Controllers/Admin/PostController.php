<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Post;
use App\Partner;
use Illuminate\Http\Request;

class PostController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::paginate(10);

        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $partners = Partner::all();

        $categories = Category::all();

        return view('admin.posts.create', compact('partners', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        if ($request->banner) {
            $file           = $request->banner->store('public/banners');
            $file           = str_replace('public', 'storage', $file);
            $data['banner'] = $file;
        }

        // disable observer
        $post = new Post;
        $post->unsetEventDispatcher();
        $post = $post->create($data);

        $post->categories()->sync($request->get('categories'));

//        if (isset($request->partners)) {
//            $partners = array_keys(array_filter($request->get('partners')));
//            $post->partners()->sync($partners);
//        }

        // sets admin posts to global
        if ($request->get('is_global') == "1") {
            $post->update([
                'is_global' => 1
            ]);
        }

        session()->flash('success', 'Post Created');

        return redirect('/admin/posts/');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\post $post
     * @return \Illuminate\Http\Response
     */
    public function show(post $post)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(post $post)
    {
        $partners = Partner::all();

        $categories = Category::all();

        return view('admin.posts.edit', compact('post', 'partners', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\post $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, post $post)
    {
        $data    = $request->all();
//        $partners = [];

        if ($request->banner) {
            $file           = $request->banner->store('public/banners');
            $file           = str_replace('public', 'storage', $file);
            $data['banner'] = $file;
        }
//
//        if (isset($data['partners'])) {
//            $partners = array_keys(array_filter($request->get('partners')));
//        }
//
//        $post->partners()->sync($partners);
        $post->update($data);

        $categories = (int)$request->get('categories');
        $post->categories()->sync($categories);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(post $post)
    {
        $post->delete();
        $post->partners()->detach();
        $post->categories()->detach();
        $post->benefits()->detach();

        session()->flash('success', 'Post Deleted');

        return back();
    }
}
