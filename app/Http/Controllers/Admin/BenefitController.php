<?php

namespace App\Http\Controllers\Admin;

use App\Benefit;
use App\Category;
use App\Http\Controllers\SubscriptionController;
use App\Provider;
use App\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\View\View;

class BenefitController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $benefits = Benefit::where('unique', '!=', true)->get();

        $types = Type::all()->pluck('name', 'name');

        $categories = Category::all()->pluck('name', 'name');

        $unique = false;

        return view('admin.benefits.index', compact('benefits', 'title', 'unique', 'types', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $unique = Input::get('u');
        $providers = Provider::all()->pluck('name', 'id');
        $categories = Category::all();
        $types = Type::all();

        return view('admin.benefits.create', compact('providers', 'unique', 'categories', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'provider_email' => 'required|email'
        ]);

        $data = $request->all();

        if ($request->thumbnail) {
            $thumbnail = $request->thumbnail->store('public/thumbnails');
            $thumbnail = str_replace('public', 'storage', $thumbnail);
            $data['thumbnail'] = $thumbnail;
        }

        if ($request->banner) {
            $banner = $request->banner->store('public/banners');
            $banner = str_replace('public', 'storage', $banner);
            $data['banner'] = $banner;
        }

        $data['provider_id'] = 1;
        $benefit = Benefit::create($data);

        $benefit->categories()->sync($request->get('categories'));
        $benefit->types()->sync($request->get('types'));

        return redirect('/admin/benefits');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Benefit $benefit
     * @return \Illuminate\Http\Response
     */
    public function show(Benefit $benefit)
    {
        //dd("Showing");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Benefit $benefit
     * @return \Illuminate\Http\Response
     */
    public function edit(Benefit $benefit)
    {
        $providers = Provider::all()->pluck('name', 'id');
        $unique = false;
        $categories = Category::all();
        $types = Type::all();

        return view('admin.benefits.edit', compact('benefit', 'providers', 'unique', 'categories', 'types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Benefit $benefit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Benefit $benefit)
    {
        $data = $request->all();

        if ($request->thumbnail) {
            $thumbnail = $request->thumbnail->store('public/thumbnails');
            $thumbnail = str_replace('public/thumbnails', '/storage/thumbnails', $thumbnail);
            $data['thumbnail'] = $thumbnail;
        }

        if ($request->banner) {
            $banner = $request->banner->store('public/banners');
            $banner = str_replace('public', 'storage', $banner);
            $data['banner'] = $banner;
        }

        session()->flash('success', 'Benefit successfully updated.');

        $benefit->update($data);

        $categories = (int)$request->get('categories');
        $types = (int)$request->get('types');

        $benefit->categories()->sync($categories);
        $benefit->types()->sync($types);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Benefit $benefit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Benefit $benefit)
    {
        $benefit->delete();

        $benefit->categories()->detach();
        $benefit->types()->detach();

        session()->flash('success', 'Benefit successfully deleted.');

        return back();
    }

    /**
     * Search for benefits endpoint
     *
     * @param Request $request
     * @return View
     */
    public function search(Request $request): View
    {
        $search = $request->get('search');

        $benefits = Benefit::where('unique', false);

        $benefits = $benefits->where(function ($query) use ($search) {
            $query->where('name', 'like', "%$search%");
        })->paginate(10);

        $unique = false;
        $types = Type::all()->pluck('name', 'name');
        $categories = Category::all()->pluck('name', 'name');

        return view('admin.benefits.index', compact('benefits', 'title', 'unique', 'types', 'categories'));
    }

    /**
     * Endpoint used to filter benefit types
     */
    public function filterType(Request $request)
    {
        $filter = $request->get('filter');

        $benefits = Benefit::whereHas('types', function ($q) use ($filter) {
            $q->where('name', '=', $filter);
        })->where('unique', false)->paginate(10);

        $types = Type::all()->pluck('name', 'name');

        $categories = Category::all()->pluck('name', 'name');

        $filter_active = true;

        $unique = false;

        return view('admin.benefits.index',
            compact('benefitTypes', 'filter_active', 'types', 'benefits', 'categories', 'unique')
        );
    }

    /**
     * Endpoint used to filter benefit categories
     */
    public function filterCategory(Request $request)
    {
        $filter = $request->get('filter');

        $benefits = Benefit::whereHas('categories', function ($q) use ($filter) {
            $q->where('name', '=', $filter);
        })->where('unique', false)->paginate(10);

        $types = Type::all()->pluck('name', 'name');

        $categories = Category::all()->pluck('name', 'name');

        $filter_active = true;

        $unique = false;

        return view('admin.benefits.index',
            compact('benefits', 'filter_active', 'types', 'categories', 'unique')
        );
    }
}
