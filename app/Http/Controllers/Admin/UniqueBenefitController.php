<?php

namespace App\Http\Controllers\Admin;

use App\Benefit;
use App\Category;
use App\Partner;
use App\Provider;
use App\Type;
use Illuminate\Http\Request;
use Illuminate\View\View;

class UniqueBenefitController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $benefits = Benefit::where('unique', true)->get();
        $unique = true;

        $types = Type::all()->pluck('name', 'name');
        $categories = Category::all()->pluck('name', 'name');

        return view('admin.benefits.index', compact('benefits', 'title', 'unique', 'types', 'categories'));
    }

    public function edit($id)
    {
        $benefit   = Benefit::find($id);
        $benefit_partners = $benefit->partners()->get()->pluck('name');
        $providers = Provider::all()->pluck('name', 'id');
        $partners  = Partner::all();
        $unique    = true;
        $categories = Category::all();
        $types = Type::all();

        return view('admin.benefits.edit', compact('benefit', 'providers', 'partners', 'unique', 'benefit_partners',
            'categories', 'types'));
    }

    public function update(Request $request, $id)
    {
        $benefit = Benefit::find($id);

        $data = $request->all();

        if ($request->thumbnail) {
            $thumbnail         = $request->thumbnail->store('public/thumbnails');
            $thumbnail         = str_replace('public/thumbnails', '/storage/thumbnails', $thumbnail);
            $data['thumbnail'] = $thumbnail;
        }

        if ($request->banner) {
            $banner         = $request->banner->store('public/banners');
            $banner         = str_replace('public', 'storage', $banner);
            $data['banner'] = $banner;
        }

        $partners   = $request->get('partners');
        $partnerIds = [];

        foreach ($partners as $id => $status) {
            if ($status === 'on') {
                array_push($partnerIds, $id);
            }
        }

        $benefit->partners()->sync($partnerIds);

        session()->flash('success', 'Benefit successfully updated.');

        $benefit->update($data);

        $categories = (int)$request->get('categories');
        $types = (int)$request->get('types');

        $benefit->categories()->sync($categories);
        $benefit->types()->sync($types);

        return back();
    }

    /**
     * Search for unique benefits.
     *
     * @param Request $request
     * @return View
     */
    public function search(Request $request): View
    {
        $search = $request->get('search');

        $benefits = Benefit::where('unique', true);

        $benefits = $benefits->where(function ($query) use ($search) {
            $query->where('name', 'like', "%$search%");

        })->paginate(10);

        $unique = true;

        $types = Type::all()->pluck('name', 'name');

        $categories = Category::all()->pluck('name', 'name');

        return view('admin.benefits.index', compact('benefits', 'title', 'unique', 'types', 'categories'));
    }

    /**
     * Endpoint used to filter benefit types
     */
    public function filterType(Request $request)
    {
        $filter = $request->get('filter');

        $benefits = Benefit::whereHas('types', function ($q) use ($filter) {
            $q->where('name', '=', $filter);
        })->where('unique', true)->paginate(10);

        $types = Type::all()->pluck('name', 'name');

        $categories = Category::all()->pluck('name', 'name');

        $filter_active = true;

        $unique = true;

        return view('admin.benefits.index',
            compact('benefitTypes', 'filter_active', 'types', 'benefits', 'categories', 'unique')
        );
    }

    /**
     * Endpoint used to filter benefit categories
     */
    public function filterCategory(Request $request)
    {
        $filter = $request->get('filter');

        $benefits = Benefit::whereHas('categories', function ($q) use ($filter) {
            $q->where('name', '=', $filter);
        })->where('unique', true)->paginate(10);

        $types = Type::all()->pluck('name', 'name');

        $categories = Category::all()->pluck('name', 'name');

        $filter_active = true;

        $unique = true;

        return view('admin.benefits.index',
            compact('benefits', 'filter_active', 'types', 'categories', 'unique')
        );
    }
}
