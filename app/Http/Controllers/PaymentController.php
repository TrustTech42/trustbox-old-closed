<?php

namespace App\Http\Controllers;

use App\Payment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\View\View;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Create NoChex payment for User
     *
     * @param Request $request
     * @param User $user
     *
     * @return View
     */
    public function create(User $user)
    {
        // set static values to be passed to form
        $ncxApiKey = env("MIX_NOCHEX_KEY");
        $ncxDescription = 'Full Membership';
        $ncxMerchantId = 'info@visionsharp.co.uk';
        $ncxAmount = 295;

        return view('frontend.payment.create',
            compact('ncxApiKey', 'ncxDescription', 'ncxMerchantId', 'ncxAmount', 'user'));
    }

    /**
     * Update NoChex payment for User
     *
     * @param User $user
     *
     * @return View
     */
    public function updateMembershipPayment(User $user)
    {
        // set static values to be passed to form
        $ncxApiKey = env("MIX_NOCHEX_KEY");
        $ncxDescription = 'Full Membership Upgrade';
        $ncxMerchantId = 'info@visionsharp.co.uk';
        $ncxAmount = 295;

        return view('frontend.payment.update-membership-payment',
            compact('ncxApiKey', 'ncxDescription', 'ncxMerchantId', 'ncxAmount', 'user')
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Payment $payment
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $payment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Payment $payment
     * @return \Illuminate\Http\Response
     */
    public function edit(Payment $payment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Payment $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payment $payment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Payment $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        //
    }
}
