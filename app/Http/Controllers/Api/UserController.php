<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends ApiController
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function show(Request $request){
        return $request->user();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function update(Request $request){
        $user = $request->user();
        $user->update($request->all());

        if($request->get('old_password')){
            $credentials = $request->only(['email', 'old_password']);

            if (Hash::check($request->get('old_password'), $user->password)) {
                if($request->get('new_password') === $request->get('new_password_confirm')){
                    $newPassword = bcrypt($request->get('new_password'));
                    $user->password = $newPassword;
                    $user->save();
                }else{
                    return response()->json(["error" => "'New password' must match 'Confirm New Password'.", 403]);
                }
            }else{
                return response()->json(["error" => "Old password is incorrect"], 403);
            }
        }

        return $user;
    }
}
