<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\User;

class AuthController extends ApiController
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request){
        $credentials = $request->only('email', 'password');

        # Login success - return token
        if (Auth::attempt($credentials)){
            return response()->json([
                'token' => User::where('email', $credentials['email'])->first()->api_token
            ]);
        }

        # Login failed
        return response()->json(null, 403);
    }
}
