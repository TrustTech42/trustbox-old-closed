<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $globalPosts = Post::where('is_global', 1)->get();

        return view('frontend.posts.index', compact('globalPosts'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);

        return view('frontend.posts.show', compact('post'));
    }

    public function search(Request $request){
        $term = $request->get('q');
        $posts = Post::where('enabled', 1)->where('title', 'like', '%' . $term . '%')->get();

        return view('frontend.posts.index', compact('posts', 'term'));
    }
}
