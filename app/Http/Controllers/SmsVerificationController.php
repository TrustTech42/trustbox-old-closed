<?php

namespace App\Http\Controllers;


use App\SmsVerification;
use App\User;
use Illuminate\Http\Request;

use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;

class SmsVerificationController extends Controller
{

    protected $code, $smsVerification;

    function __construct()
    {
        $this->smsVerification = new SmsVerification();

    }

    public function sendCode(User $user)
    {
        $details['code'] = rand(1000, 9999);
        $details['contact_number'] = $user->mobile;

        $smsVerification = new smsVerification();
        $smsVerification->store($details);

        $smsVerificationController = new SmsVerificationController();
        $smsVerificationController->sendSms($details);

        return redirect()->route('sms-verification.start.contact', compact('user'));
    }

    public function startContact(User $user)
    {
        $url = explode('//', config('app.url'));

        if($url[1] == $_SERVER['HTTP_HOST'])
        {
            return view('auth.admin-sms-verification', compact('user'));
        }

        return view('auth.sms-verification', compact('user'));
    }

    public function sendSms($details)
    {
        $accountSid = env('TWILIO_ACCOUNT_SID');
        $authToken = env('TWILIO_AUTH_TOKEN');

        try
        {
            $client = new Client($accountSid, $authToken);

            $result = $client->messages->create(
                '+44'.$details['contact_number'],
                [
                    'from' => 'Trustbox',
                    'body' => 'CODE: '. $details['code']
                ]
            );

            return $result;
        }catch(TwilioException $ex)
        {
            echo 'Error: ' . $ex->getMessage();
        }
    }

    public function verifyContact(Request $request)
    {
        $user = auth()->user();
        $smsVerification = $this->smsVerification::where('contact_number', '=', $user->mobile)->latest()->first();

        $request->validate([
            'code' => 'required|in:'.$smsVerification->code
                           ]);

        $request['status'] = 'verified';
        $smsVerification->updateModel($request);

        return redirect('/admin');
    }
}
