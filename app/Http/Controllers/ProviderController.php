<?php

namespace App\Http\Controllers;

use App\Benefit;
use App\Provider;
use App\User;
use Illuminate\Http\Request;

class ProviderController extends Controller
{
    public function show(Provider $provider)
    {
        $members = $provider->members();

        $benefit = new Benefit();
        $benefitsArr = $benefit->getBenefits($provider);

        return view('frontend.providers.show', compact('provider', 'members', 'benefitsArr'));
    }

    public function export($id)
    {
        // resources
        $provider = Provider::find($id);
        $hidden   = ['email', 'telephone', 'created_at', 'updated_at', 'city', 'postcode', 'has_boosted', 'role_id', 'remember_token', 'email_verified_at', 'id', 'change_password', 'referred', 'address1', 'address2', 'first_name', 'last_name', 'letter_printed', 'mobile', 'deleted_at'];
        $members  = $provider->membersCSV()->makeHidden($hidden)->toArray();
        $headers  = ['Name', 'Customer Number', 'Date of Birth', 'Benefit'];

        // file
        $filename = "members.csv";
        $handle   = fopen($filename, 'w');

        // write  headers
        fputcsv($handle, $headers);

        // write to file
        foreach ($members as $member) {
            $benefits = $member['benefits'];
            unset($member['benefits']);

            foreach ($benefits as $benefit) {
                $boosted = $benefit['pivot']['boosted'] ? 'Boosted' : 'Enrolled';
                array_push($member, $benefit['name'] . ' - ' . $boosted);
            }

            fputcsv($handle, $member);
        }

        fclose($handle);

        return response()->download($filename, $filename);
    }


    public function search(Provider $provider, Request $request)
    {
        $search = $request->get('search');
        $filter = $request->get('filter');

        $members = User::has('benefits')
                        ->where('customer_number', 'like', "%$search%")
                        ->orWhere('name', 'like', "%$search%")
                        ->whereHas('benefits', function ($q) use ($provider, $filter) {
                            $q->where('provider_id', $provider->id);
                        })
                        ->paginate(10);

        $benefit = new Benefit();
        $benefitsArr = $benefit->getBenefits($provider);

        $search_active = true;

        return view('frontend.providers.show', compact('provider', 'members', 'search_active', 'benefitsArr'));
    }

    public function filter(Provider $provider, Request $request)
    {
        $filter = $request->get('filter');

        $members = User::has('benefits')
                        ->whereHas('benefits', function ($q) use ($provider, $filter) {
                            $q->where('provider_id', $provider->id)
                              ->where('name', 'like', "%$filter%")
                              ->orWhere('boosted', 'like', "%$filter%");
                        })
                       ->paginate(10);

        $benefit = new Benefit();
        $benefitsArr = $benefit->getBenefits($provider);

        $filter_active = true;

        return view('frontend.providers.show', compact('provider', 'members', 'filter_active', 'benefitsArr'));
    }
}
