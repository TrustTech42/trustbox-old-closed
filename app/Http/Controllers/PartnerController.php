<?php

namespace App\Http\Controllers;

use App\Category;
use App\Contact;
use App\Provider;
use Illuminate\Database\Eloquent\Model;
use function foo\func;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Partner;
use App\Benefit;
use Illuminate\Support\Facades\Validator;

class PartnerController extends Controller
{
    public function show(Partner $partner, $part1 = 'index', $part2 = '', $part3 = '')
    {
        $benefits = $partner->benefits;

        $unique_benefits = $partner->getUniqueBenefits($partner);

        $total_invites = $partner->getMemberInvites($partner);

        $total_boosted = $partner->getBoostedMembers($partner);

        $provider = Provider::findOrFail($partner->provider_id);

        $providerMembers = $provider->members();

        $benefit = new Benefit();
        $benefitsArr = $benefit->getBenefits($provider);

        $broadcasts = $partner->broadcasts;
        $contactMessages = Contact::where('partner_id', $partner->id)->get();
        $partnerBenefits = $partner->benefits()->get();

        $connectionsList = Partner::whereDoesntHave('connections', function ($q) use ($partner) {
            $q->where('connection_id', '=', $partner->id);
        })->whereDoesntHave('connections2', function ($q) use ($partner) {
            $q->where('partner_id', '=', $partner->id);
        })->where('id', '!=', auth()->user()->partner->id)->get()->pluck('name', 'id');

        $connections = $partner->allConnections->where('pivot.approved', 1);

        $activePosts = $partner->posts->where('pivot.published', 1);

        $posts = $partner->posts;

        $categories = Category::all();

        if($part2 == 'view'){
            // figure out how to get a model from a string

            switch($part1){
                case 'broadcast':
                    $model = 'App\Message';
                    break;
                default:
                    $model = 'App\\' . $part1;
            }

            $$part1 = $model::find($part3);

            return view('frontend.partner.dashboard.' . $part1 . '.show', compact('partner', $part1, 'categories', 'benefits', 'partnerBenefits'));
        }else{
            $view = 'frontend.partner.dashboard.' . $part1 . ($part2 ? '.' . $part2 : '') . ($part3 ? '.' . $part3 : '');

            return view($view,
                compact('partner', 'benefits', 'unique_benefits', 'total_invites', 'total_boosted',
                    'provider', 'providerMembers', 'benefitsArr', 'connectionsList', 'broadcasts', 'contactMessages',
                    'connections', 'activePosts', 'categories', 'posts', 'partnerBenefits'));
        }
    }

    public function update(Request $request, Partner $partner)
    {
        $partner->update($request->all());

        if ($request->file('logo')) {
            $file = Storage::put('public/logos', $request->file('logo'));
            $file = "/" . str_replace("public/logos", "", $file);
            $file = str_replace('//', '', $file);

            $partner->logo = $file;
            $partner->save();
        }

        session()->flash('success', 'Your information has been successfully updated.');

        return back();
    }

    /**
     * Endpoint used to create and send a connection to a partner
     */
    public function sendConnectionRequest(string $partnerId)
    {
        $partner = Partner::findOrFail($partnerId);

        $selectedPartners = Input::get('partners');

        foreach ($selectedPartners as $selectedPartner) {
            $connection = Partner::findOrFail($selectedPartner);
        }

        $partner->connections()->attach($connection->id);

        session()->flash('success', 'Connection Sent.');

        return redirect('/partner/' . $partnerId . '#connections');
    }

    /**
     * Endpoint used to delete an existing partner connection and connection request
     */
    public function deleteConnectionRequest(string $partnerId, string $connectionId)
    {
        $partner = Partner::findOrFail($partnerId);

        $partner->connections()->detach($connectionId);

        $partner->connections2()->detach($connectionId);

        session()->flash('success', 'Connection Removed.');

        return redirect('/partner/' . $partnerId . '#connections');
    }

    /**
     * Endpoint used to accept the partner message connection
     */
    public function acceptConnectionRequest(string $partnerId, string $connectionId)
    {
        $partner = Partner::findOrFail($partnerId);

        $partner->connections2()->updateExistingPivot($connectionId, ['approved' => 1]);

        session()->flash('success', 'Connection Accepted');

        return redirect('/partner/' . $partnerId . '#connections');
    }

    public function updateSortOrder(Request $request)
    {
        $partner = Partner::find($request->get('partner_id'));
        $partner->benefits()->updateExistingPivot($request->get('benefit_id'), ['sort_order' => $request->get('sort_order')]);

        return response()->json('ok');
    }
}

// php artisan migrate --path=/database/migrations/2021_02_26_091846_create_messages_table.php
