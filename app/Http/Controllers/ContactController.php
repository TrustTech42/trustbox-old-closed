<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Reason;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ContactController extends Controller
{
    public function index(){
        $reasons = Reason::where('visible', true)->pluck('name', 'id');

        return view('frontend.contact.index', compact('reasons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Contact::create($request->all());
        session()->flash('success', "Thanks! We've received your message and will be in touch within 2 days.");

        return back();
    }
}
