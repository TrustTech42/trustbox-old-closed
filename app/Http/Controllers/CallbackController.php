<?php

namespace App\Http\Controllers;

use App\User;

class CallbackController extends Controller
{
    /**
     * Redirect to the user.show GET request
     *
     * @param User $user
     */
    public function redirectSuccess(\App\User $user)
    {
        return redirect('/user/' . $user->id);
    }
}
