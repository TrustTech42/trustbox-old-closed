<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Message;
use App\User;

class BroadcastController extends Controller
{
    // ROUTE ISNT NEEDED WITH THE CURRENT TABBED NAV FOR USERS

//    /**
//     * Endpoint used to list all the broadcasts received from Partner on user page
//     */
//    public function list()
//    {
//        $user = auth()->user();
//
//        $broadcasts = $user->broadcasts();
//
//        return view('frontend.user.tabs.broadcasts.index', compact('broadcasts'));
//    }

    /**
     * Endpoint used to view individual broadcast from Partner on user page
     */
    public function show(string $userId, string $broadcastId)
    {
        $user = User::findOrFail($userId);

        $broadcast = Message::findOrFail($broadcastId);

        return view('frontend.users.tabs.broadcasts.show', compact('user', 'broadcast'));
    }
}
