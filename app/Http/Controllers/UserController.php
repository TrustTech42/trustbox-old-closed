<?php

namespace App\Http\Controllers;

use App\Mail\NewUserBenefits;
use App\Reason;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

use App\User;
use App\Membership;
use App\Payment;
use App\Partner;
use App\Plan;
use App\Setting;
use App\Subscription;
use App\Invitations;
use App\Mail\UserSignUp;
use App\Mail\UserWelcome;

class UserController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'unique:users',
            'password' => 'required|string|min:6|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
        ]);

        if ($request->has('invitation')) {
            $invitation = Invitations::find($request->get('invitation'));

            if ($request->get('code') !== $invitation->code) {
                return back()->withErrors('The code you provided is not valid');
            } else {
                $invitation->update([
                    'status' => 'completed'
                ]);
            }
        }

        // Prepare data for saving
        $data = session()->get('customer_data');

        if (!$data) {
            $data = $request->all();
        }

        if (isset($data['dob_year']) && isset($data['dob_year']) && isset($data['dob_year'])) {
            $data['date_of_birth'] = $data["dob_year"] . '-' . $data['dob_month'] . '-' . $data['dob_day'];
        }

        $data['name'] = $data['first_name'] . ' ' . $data['last_name'];
        $data['customer_number'] = $this->createCustomerNumber();

        if (isset($data['password'])) {
            $data['password'] = bcrypt($data['password']);
        } else {
            $data['password'] = bcrypt('password');
        }

        // if normal registration set plan to lifetime with role as pending to restrict access until payment

        if ($data['plan'] == 1 & !(isset($invitation))) {
            $data['role_id'] = 7;
        } else {
            // if invited and plan is set to any other plan (household)
            // set the role id as user allowing access to app
            $data['role_id'] = 1;
        }

//        $data['api_token'] = Str::random(60);

        $user = User::create($data);

        if (isset($data['plan'])) {
            # If trusted, do something special
            if ($data['plan'] === "4") { // 4 = trusted
                # Get the user that made the referral
                $referringUser = User::findorfail($data['referred']);
                $this->trustUser($referringUser, $user);
            }

            $membership = Membership::create([
                'user_id' => $user->id,
                'partner_id' => $data['partner'],
                'plan_id' => $data['plan'],
                'activated' => 0
            ]);

            $user->membership()->save($membership);
        }

        $membership->activated = true;
        $membership->save();
        $setting = Setting::find(1);

        if (isset($data['referred'])) {
            $partner = Partner::find($data['partner']);

            $content = str_replace('@first_name@', $user->name, $setting->welcome_email_content);
            $content = str_replace('@partner@', $partner->name, $content);

            $vars = new \stdClass();
            $vars->partner = $partner;
            $vars->content = $content;

//            Mail::to($user->email)->send(new UserWelcome($vars));

        } else {
            $url = url()->current();
            $url = explode('/', $url);
            $url = explode('.', $url[2]);
            $subdomain = $url[0];
            $partner = Partner::where('subdomain', "$subdomain")->with('posts')->first();

            if ($partner === null) {
                $partner = Partner::find(Setting::where('name', 'default_partner')->first()->value);
            }

            $content = str_replace('@name@', $user->name, $setting->signup_email_content);

            $vars = new \stdClass();
            $vars->partner = $partner;
            $vars->content = $content;

//            Mail::to($user->email)->send(new UserSignUp($vars));

//            Mail::to($user->email)->send(new NewUserBenefits($vars));
        }

        if(isset($data['code']) && $data['code']){
            if ($invite = \App\Invite::where('code', $data['code'])->first()) {
                $invite->accepted = 1;
                $invite->save();
            }
        }

        // Log the user in
        Auth::login($user);

        ##$letter = new Letter();
        ##$letter->createLetter();

        // if its a new life time user direct to payment page
        if ($membership->plan_id == 1 & !(isset($invitation))) {
            return redirect()->route('create.payment', compact('user'));

        } else {

            // if the user is gifted, household etc redirect into there account
            return redirect('/user/' . $user->id);
        }
    }

    /**
     * @param Request $request
     * @param User $user
     */
    public function show(Request $request, User $user)
    {
        if ($user->role->name == 'Pending') {
            return redirect()->route('create.payment', $user);
        } else {
            $boostedTotal = $user->benefits()->where('boosted', '1')->sum('price');
            $payments = $user->subscription->payments ?? [];
            $nochexPayment = $user->membership->payment;

            $emails = $user->referred_users()->pluck('email')->toArray();

            $membership = Plan::find($user->membership()->latest()->first()->plan_id)->name;

            if ($membership == 'lifetime') {
                $invites = $user->invites()->where('accepted', '=', 0)->whereNotIn('email', $emails)->get();
            } else {
                $invites = $user->invites()->where('plan', '!==', 'gifted')->where('accepted', '=', 0)
                    ->whereNotIn('email', $emails)
                    ->get();
            }

            $household = $user->referred_users()->whereHas('membership', function ($query) {
                $query->where('plan_id', 4);
            })->count();

            $gifted = $user->referred_users()->whereHas('membership', function ($query) {
                $query->where('plan_id', 2);
            })->count();

            // grabs all boosted benefits and if one includes file manager hide file manager from view
            // if file m
            $boostedBenefits = $user->benefits()->where('boosted', 1)->get();
            if ($boostedBenefits->contains('includes_file_manager', 1)) {
                $boostedBenefits = $boostedBenefits->except(2);
            }

            // grabs all boosted benefits and if one includes file manager hide the file manager benefit from the count
            $countBenefits = $user->benefits()->where('boosted', 1)->get();
            if ($countBenefits->contains('includes_file_manager', 1)) {
                $countBenefits = $countBenefits->except(2);
            }
            $countBoostedBenefits = $countBenefits->count();

            $subscription = new Subscription();

            $reasons = Reason::where('visible', true)->pluck('name', 'id');
        }

        $subscription = $user->subscription()->exists();

        $broadcasts = $user->broadcasts();

        return view('frontend.users.show',
            compact('user', 'boostedTotal', 'payments', 'nochexPayment',  'invites', 'gifted', 'household',
                'membership', 'subscription', 'boostedBenefits', 'countBoostedBenefits', 'reasons', 'broadcasts'));
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'password' => 'nullable:sometimes|string|min:6|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
        ]);

        // check to see if user is trying to change password
        if ($request->get('password') !== null) {
            $credentials = ['email' => $request->get('email'), 'password' => $request->get('old_password')];

            if (!Auth::attempt($credentials)) {
                session()->flash('error', 'The existing password given does not match our records.');
                return back();
            }
        }

        if ($request->get('password')) {
            $request['password'] = bcrypt($request->get('password'));
        }

        $user->update($request->all());

        $user->changelogs()->create([
            'action' => 'User updated their information'
        ]);

        session()->flash('success', 'Your account details were updated successfully.');

        return back();
    }

    private function createCustomerNumber()
    {
        $customerNumber = "C" . rand(100000000, 999999999);
        $customer = User::where('customer_number', $customerNumber)->first();

        if (!$customer)
            return $customerNumber;

        return $this->createCustomerNumber();
    }

    public function trustUser(User $user, User $linkedUser)
    {
        $user->trusted()->attach($linkedUser->id);
    }
}
