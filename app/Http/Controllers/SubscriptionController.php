<?php
//
//namespace App\Http\Controllers;
//
//use App\Mail\BenefitRequested;
//use App\Partner;
//use App\Setting;
//use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Auth;
//
//use App\User;
//use App\Benefit;
//use App\Subscription;
//use Illuminate\Support\Facades\Mail;
//
//class SubscriptionController extends Controller
//{
//    public function updateSubscription(Request $request)
//    {
//        if ($this->subscription && $this->subscription->gocardless_sub_id) {
//            // User already has a subscription, cancel and update
//            $nextPayment = $this->getGocardlessSubscriptionDate();
//
//            if ($this->subscription->status === "Subscription Active") {
//                $this->cancelGocardlessSub();
//            }
//
//            $this->calculateTotal();
//
//
//            if ($this->total > 0) {
//                $this->subscription->update(['total' => $this->total]);
//                $this->createGocardlessSub($nextPayment);
//            }
//
//            $this->boostBenefits();
//
//            $this->user->changelogs()->create([
//                'action' => 'User updated boosted benefits'
//            ]);
//
//            session()->flash('success', 'Direct Debit successfully updated.');
//        } else {
//            // User is new, create a subscription
//            $this->subscription = $this->user->subscription()->create();
//            $this->redirectToGocardless();
//        }
//
//
//        return redirect('/dashboard?fragment=boosted_benefits');
//    }
//
//    public function updatesub(Request $request)
//    {
//        $redirectFlowId = $request->get('redirect_flow_id');
//        $this->completeRedirectFlow($redirectFlowId);
//        $this->calculateTotal();
//        $this->createGocardlessSub();
//        $this->boostBenefits();
//
//        if ($this->total > 0) {
//            $this->subscription->update(['total' => $this->total]);
//        }
//
//        $this->user->changelogs()->create([
//            'action' => 'User has updated boosted benefits'
//        ]);
//
//        $setting = Setting::find(1);
//
//
//        foreach ($this->benefits as $id => $value) {
//            $benefit = auth()->user()->benefits->find($id);
//
//            $content = str_replace('@name@', $this->user->name, $setting->boosted_email_content);
//            $content = str_replace('@benefit_name@', $benefit->name, $content);
//
//            $content = str_replace('@benefit_details@', $benefit->boosted_description, $content);
//
//            $url       = url()->current();
//            $url       = explode('/', $url);
//            $url       = explode('.', $url[2]);
//            $subdomain = $url[0];
//            $partner   = Partner::where('subdomain', "$subdomain")->with('posts')->first();
//
//            if ($partner === null) {
//                $partner = Partner::find(Setting::where('name', 'default_partner')->first()->value);
//            }
//
//            $vars           = new \stdClass();
//            $vars->email    = $this->user->email;
//            $vars->template = 'mail.benefit-boost';
//            $vars->subject  = 'Benefit Boosted';
//            $vars->partner  = $partner;
//            $vars->content  = $content;
//
//            Mail::to($vars->email)->send(new BenefitRequested($vars));
//        }
//
//        session()->put('boosted', 1);
//
//        return redirect('/dashboard?fragment=boosted_benefits');
//    }
//
//    /******************************************************
//     * Private methods
//     *****************************************************/
//    private function redirectToGocardless()
//    {
//        $redirectFlow = $this->client->redirectFlows()->create([
//            "params" => [
//                "description"          => "Boosted Benefit Subscription",
//                "session_token"        => "dummy_session_token",
//                "success_redirect_url" => "http://" . $this->user->membership->partner->subdomain . '.' . env("APP_DOMAIN") . "/subscription/" . $this->subscription->id . '/updatesub',
////                "success_redirect_url" => "http://" . $this->user->membership->partner->subdomain . ".family-benefits.test/subscription/" . $this->subscription->id . '/updatesub',
//                "prefilled_customer"   => [
//                    "given_name"    => $this->user->first_name,
//                    "family_name"   => $this->user->last_name,
//                    "email"         => $this->user->email,
//                    "address_line1" => $this->user->address1,
//                    "address_line2" => $this->user->address2,
//                    "city"          => $this->user->city,
//                    "postal_code"   => $this->user->postcode
//                ]
//            ]
//        ]);
//
//        if ($this->total > 0) {
//            $this->subscription->update(['total' => $this->total]);
//        }
//
//        header("Location: " . $redirectFlow->redirect_url);
//        exit();
//    }
//
//    private function completeRedirectFlow($redirectFlowId)
//    {
//        $redirectFlow = $this->client->redirectFlows()->complete(
//            $redirectFlowId, ["params" => ["session_token" => "dummy_session_token"]]
//        );
//
//        $this->subscription->mandate_id  = $redirectFlow->links->mandate;
//        $this->subscription->customer_id = $redirectFlow->links->customer;
//        $this->subscription->status      = "Awaiting Subscription";
//
//        if ($this->total > 0) {
//            $this->subscription->total = $this->total;
//        }
//
//        $this->subscription->save();
//    }
//
//    public function getGocardlessSubscriptionDate()
//    {
//        $sub = $this->client->subscriptions()->get($this->subscription->gocardless_sub_id);
//
//        if ($sub->upcoming_payments)
//            $nextPayment = $sub->upcoming_payments[0]->charge_date;
//        else
//            $nextPayment = null;
//
//        return $nextPayment;
//    }
//
//    private function calculateTotal()
//    {
//        foreach ($this->benefits as $id => $value) {
//            if ($value === "1") {
//                $this->total += Benefit::find($id)->price;
//            }
//        }
//    }
//
//    private function boostBenefits()
//    {
//        foreach ($this->benefits as $id => $value) {
//            $benefit                 = auth()->user()->benefits->find($id);
//            $benefit->pivot->boosted = $value;
//            $benefit->pivot->save();
//        }
//    }
//
//    private function createGocardlessSub($nextPaymentDate = null, $total = null)
//    {
//        $newSubscription = $this->client->subscriptions()->create([
//            "params" => [
//                "amount"        => $total ? $total * 100 : $this->total * 100,
//                "currency"      => "GBP",
//                "interval_unit" => "monthly",
//                "start_date"    => $nextPaymentDate,
//                "links"         => [
//                    "mandate" => $this->subscription->mandate_id
//                ],
//                "name"          => "TEST SUBSCRIPTION"
//            ]
//        ]);
//
//        $this->subscription->gocardless_sub_id = $newSubscription->id;
//        $this->subscription->status            = "Subscription Active";
//
//        if ($total > 0 || $this->total > 0) {
//            $this->subscription->total = $total ? $total : $this->total;
//        }
//
//        $this->subscription->save();
//    }
//
//    private function cancelGocardlessSub()
//    {
//        $this->client->subscriptions()->cancel($this->subscription->gocardless_sub_id);
//        $this->subscription->update(['status' => 'Subscription Cancelled']);
//    }
//
//    public function updateGocardlessSubDate(Request $request)
//    {
//        if ($this->subscription && $this->subscription->gocardless_sub_id) {
//            $originalDate        = $request->get('subscription_date');
//            $originalDateAsArray = explode('-', $originalDate);
//
//            $originalYear  = $originalDateAsArray[0];
//            $originalMonth = $originalDateAsArray[1];
//            $newMonth      = $request->get("month");
//
//            $newDate = "";
//
//            if ($newMonth === "1" && $originalMonth === "12") {
//                $originalYear++;
//            }
//
//            $newDate = $originalYear . '-' . $newMonth . '-' . $request->get('day');
//
//            // User already has a subscription, cancel and update
//            $nextPayment = $newDate;
//
//            if ($this->subscription->status === "Subscription Active") {
//                $this->cancelGocardlessSub();
//            }
//
//            $this->calculateTotal();
//
//            if ($this->total > 0) {
//                $this->createGocardlessSub($nextPayment);
//            }
//
//            $this->user->changelogs()->create([
//                'action' => 'User updated subscription date'
//            ]);
//
//            session()->flash('success', 'Direct Debit successfully updated.');
//            session()->forget('boosted');
//
//            return redirect()->back();
//        }
//    }
//
//    public function updateSubscriptionForStorage(Request $request)
//    {
//        $planId = $request->get('plan_id');
//
//        if($planId > 1 || $planId === null) abort(403);
//
//        if($planId == 1){
//            $storageAmount = Setting::first()->upgraded_storage;
//        }else{
//            $storageAmount = Setting::first()->default_storage;
//        }
//
//        $this->user->update([
//            'storage_upgraded' => $planId,
//            'storage' => $storageAmount
//        ]);
//
//        $upgradePrice = Setting::first()->upgraded_storage_price;
//        $total = $planId === '1' ? $this->subscription->total + $upgradePrice : $this->subscription->total - $upgradePrice;
//        $total = (string)$total;
//
//        $this->cancelGocardlessSub();
//        $this->createGocardlessSub(null, $total);
//
//        return  redirect('/dashboard?fragment=boosted_benefits');
//    }
//
//    public function forget()
//    {
//        session()->forget('boosted');
//
//        return back();
//    }
//}
