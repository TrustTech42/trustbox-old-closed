<?php

namespace App\Http\Controllers\NoChex;

use App\Benefit;
use App\Http\Controllers\Controller;
use App\Mail\BenefitRequested;
use App\Setting;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class BoostScheduleController extends Controller
{
    /**
     * Receives response from NoChex
     */
    public function NoChexListener()
    {
        Log::debug($_POST);
        // checks if the result key is present within the response
        if (isset($_POST['result'])) {
            // if the result key has a success value continue check
            if ($_POST['result'] === 'SUCCESS') {
                // checks if trans_type response field has been set
                if (isset($_POST['trans_type'])) {
                    // if the transaction type contains initial execute the initial boost
                    if ($_POST['trans_type'] == 'INITIAL') {
                        $this->initialBoost();
                    }
                }
                // if the action field returned from the response has been set
                if (isset($_POST['action'])) {
                    // if the action contains a new boost execute the subsequent boost method
                    if ($_POST['action'] == 'NEW_BOOST') {
                        $this->subsequentBoost();
                    }
                    // if the action contains a cancel boost execute the cancel boost method
                    if ($_POST['action'] == 'CANCEL_BOOST') {
                        $this->cancelBoost();
                    }
                }
            }
        }
    }

    /**
     * Method for initial boost
     */
    private function initialBoost()
    {
        // grabs the debit payment record which matches the transaction id returned from the response
        $debitPayment = \App\DebitPayment::where('transaction_id', $_POST['transaction_id'])->first();

        // grabs the subscription and benefit associated with the payment record
        $subscription = $debitPayment->subscription()->first();
        $benefit_id = $debitPayment->benefit_id;
        $benefit = Benefit::findOrFail($benefit_id);

        // updates the subscription to active and stores the schedule_id returned from the response
        $subscription->update([
            'status' => 'Subscription Active',
            'schedule_id' => $_POST['schedule_id']
        ]);

        // grabs the user from subscription then grabs the benefit associated with the user and sets the benefit
        $user = $subscription->user;
        $userBenefit = $user->benefits->find($benefit);
        $userBenefit->pivot->boosted = 1;
        $userBenefit->pivot->save();

        // file manager
        if ($userBenefit->id === 2) {
            $user->update([
                'file_manager_trial_date' => null,
                'storage' => 5
            ]);
        }

        // check if the boosted benefit has file manager turned on
        if ($userBenefit->includes_file_manager) {
            $this->boostFileManager($user);
        }

        $setting = Setting::find(1);

        $content = str_replace('@name@', $user->first_name . ' ' . $user->last_name, $setting->boosted_email_content);
        $content = str_replace('@benefit_name@', $userBenefit->name, $content);
        $content = str_replace('@benefit_details@', $userBenefit->boosted_description, $content);


        $vars = new \stdClass();
        $vars->email = $user->email;
        $vars->template = 'mail.benefit-boost';
        $vars->subject = 'Benefit Subscribed';
        $vars->partner = $user->membership->partner;
        $vars->content = $content;

        Mail::to($vars->email)->send(new BenefitRequested($vars));
    }

    /**
     * Method for boosting a subsequent benefit
     *
     */
    private function subsequentBoost()
    {
        // grab benefit id from nochex response
        $benefit_id = $_POST['boostid'];
        $benefit = Benefit::findOrFail($benefit_id);

        // grabs the subscription associated with the  schedule_id returned from the response
        $subscription = \App\Subscription::where('schedule_id', $_POST['schedule_id'])->first();

        $user = $subscription->user;

        // takes the subscription total then adds the new benefit price
        $amount = $subscription->total + $benefit->price;

        // updates the subscription to include the new total
        $subscription->update([
            'total' => $amount
        ]);

        // grabs the user from subscription then grabs the benefit associated with the user and sets the benefit to boosted
        $userBenefit = $user->benefits->find($benefit);
        $userBenefit->pivot->boosted = 1;
        $userBenefit->pivot->save();

        // file manager individual boost
        if ($userBenefit->id === 2) {
            $user->update([
                'file_manager_trial_date' => null,
                'storage' => 5
            ]);
        }
        // check if the boosted benefit has file manager turned on
        if ($userBenefit->includes_file_manager) {
            $this->boostFileManager($user);
        }

        $setting = Setting::find(1);

        $content = str_replace('@name@', $user->first_name . ' ' . $user->last_name, $setting->boosted_email_content);
        $content = str_replace('@benefit_name@', $userBenefit->name, $content);
        $content = str_replace('@benefit_details@', $userBenefit->boosted_description, $content);


        $vars = new \stdClass();
        $vars->email = $user->email;
        $vars->template = 'mail.benefit-boost';
        $vars->subject = 'Benefit Subscribed';
        $vars->partner = $user->membership->partner;
        $vars->content = $content;

        Mail::to($vars->email)->send(new BenefitRequested($vars));
    }

    /**
     * Method for cancelling a boosted benefit
     *
     */
    private function cancelBoost()
    {
        // grab benefit id from nochex response
        $benefit_id = $_POST['boostid'];
        $benefit = Benefit::findOrFail($benefit_id);

        // grabs the subscription associated with the  schedule_id returned from the response
        $subscription = \App\Subscription::where('schedule_id', $_POST['schedule_id'])->first();

        $user = $subscription->user;

        // takes the subscription total then reduces the benefit price from the total
        $amount = $subscription->total - $benefit->price;

        // updates the subscription total
        $subscription->update([
            'total' => $amount
        ]);

        // grabs the user from subscription then grabs the benefit associated with the user and sets the benefit to unboosted
        $userBenefit = $user->benefits->find($benefit);
        $userBenefit->pivot->boosted = 0;
        $userBenefit->pivot->save();

        if ($userBenefit->id === 2) {
            $user->update([
                'file_manager_trial_date' => Carbon::now()->subDays(1),
                'storage' => 0
            ]);
        }

        // check if the boosted benefit has file manager turned on
        if ($userBenefit->includes_file_manager) {
            $this->cancelFileManager($user);
        }

        // fire off cancellation of benefit email
        $setting = Setting::find(1);

        $content = str_replace('@name@', $user->first_name . ' ' . $user->last_name, $setting->cancelled_boosted_email_content);
        $content = str_replace('@benefit_name@', $userBenefit->name, $content);

        $vars = new \stdClass();
        $vars->email = $user->email;
        $vars->template = 'mail.cancelled-benefit-boost';
        $vars->subject = 'Benefit Subscription Cancelled';
        $vars->partner = $user->membership->partner;
        $vars->content = $content;

        Mail::to($vars->email)->send(new BenefitRequested($vars));
    }

    /**
     * Method for boosting the file manager if the chosen benefit has file manager turned on
     *
     * @param $user
     */
    private function boostFileManager($user)
    {
        // grabs the file manager for the user and turns it on
        $fileManager = Benefit::findOrFail(2);
        $userFileManager = $user->benefits->find($fileManager);
        $userFileManager->pivot->boosted = 1;
        $userFileManager->pivot->save();

        // updates the file manager to non trial and upgrades the storage
        $user->update([
            'file_manager_trial_date' => null,
            'storage' => 5
        ]);
    }

    /**
     * Method for boosting the file manager if the chosen benefit has file manager turned on
     *
     * @param $user
     */
    private function cancelFileManager($user)
    {
        // grabs the file manager for the user and turns it on
        $fileManager = Benefit::findOrFail(2);
        $userFileManager = $user->benefits->find($fileManager);
        $userFileManager->pivot->boosted = 0;
        $userFileManager->pivot->save();

        // updates the file manager trial and removes storage
        $user->update([
            'file_manager_trial_date' => Carbon::now()->subDays(1),
            'storage' => 0
        ]);
    }
}
