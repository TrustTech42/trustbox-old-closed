<?php

namespace App\Http\Controllers\NoChex;

use App\Benefit;
use App\Http\Controllers\Controller;
use App\Mail\NewUserBenefits;
use App\Mail\UserWelcome;
use App\Setting;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Mail;

class LifetimeCallbackController extends Controller
{
    /**
     * Creates a new Guzzle POST requst to the NoChex server.
     *
     * @param $vars
     */
    public function http_post($vars)
    {
        $client = new Client();

        // creates a Guzzle POST request to the NoChex callback page with the appropriate headers and params
        $response = $client->request('POST', 'https://secure.nochex.com/callback/callback.aspx', [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Host' => 'secure.nochex.com'
            ],
            'form_params' => $vars
        ]);

        $result = (string)$response->getBody();

        return $result;
    }

    public function NoChexCallback()
    {
        $response = $this->http_post($_POST);

        if ($_POST['transaction_status'] == "100") {
            $status = " TEST";
        } else {
            $status = " LIVE";
        }

        // check on if the response contains authorization
        if (!$response == "AUTHORISED") {
            // sets message incase we want to send an email
            $msg = "Callback was not AUTHORISED.";

        } else {
            // sets message incase we want to send an email
            $msg = "Callback was AUTHORISED.";

            // grab the fields from the response
            $user_id = $_POST['optional_1'];
            $transaction_id = $_POST['transaction_id'];
            $amount = $_POST['amount'];

            $user = User::findOrFail($user_id);

            $membership = $user->membership()->first();

            // creates a new payment record and stores the fields
            \App\Payment::create([
                'membership_id' => $membership->id,
                'amount' => $amount,
                'transaction_id' => $transaction_id
            ]);

            // updates the role of the account to user and updating the plan to lifetime
            $user->update([
                'role_id' => 1
            ]);

            $membership->update([
                'plan_id' => 1
            ]);

            $setting = Setting::find(1);

            $partner = $user->membership->partner;

            // welcome email content
            $content = str_replace('@first_name@', $user->name, $setting->welcome_email_content);
            $content = str_replace('@partner@', $partner->name, $content);

            // new user benefits content
            $benefitContent = str_replace('@name@', $user->name, $setting->new_user_benefits_content);
            $benefitContent = str_replace('@partner_name@', $user->membership->partner->name, $benefitContent);

            $vars = new \stdClass();
            $vars->partner = $partner;
            $vars->content = $content;
            $vars->benefitContent = $benefitContent;

            Mail::to($user->email)->send(new UserWelcome($vars));

            Mail::to($user->email)->send(new NewUserBenefits($vars));
        }
    }
}
