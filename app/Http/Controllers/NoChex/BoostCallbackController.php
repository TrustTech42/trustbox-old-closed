<?php


namespace App\Http\Controllers\NoChex;

use App\Benefit;
use App\Http\Controllers\Controller;
use App\User;
use GuzzleHttp\Client;

class BoostCallbackController extends Controller
{
    /**
     * Creates a new Guzzle POST requst to the NoChex server.
     *
     * @param $vars
     */
    public function http_post($vars)
    {
        $client = new Client();

        // creates a Guzzle POST request to the NoChex callback page with the appropriate headers and params
        $response = $client->request('POST','https://secure.nochex.com/callback/callback.aspx', [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Host' => 'secure.nochex.com'
            ],
            'form_params' => $vars
        ]);

        $result = (string) $response->getBody();

        return $result;
    }

    /**
     * Receives POST response from NoChex
     */
    public function NoChexCallback()
    {
        $response = $this->http_post($_POST);

        if ($_POST['transaction_status'] == "100") {
            $status = " TEST";
        } else {
            $status = " LIVE";
        }

        // check on if the response contains authorization
        if (!$response == "AUTHORISED") {
            // sets message incase we want to send an email
            $msg = "Callback was not AUTHORISED.";

        } else {
            // sets message incase we want to send an email
            $msg = "Callback was AUTHORISED.";

            $user_id = $_POST['optional_3'];
            $user = User::findOrFail($user_id);

            $benefit_id = $_POST['optional_4'];
            $benefit = Benefit::findOrFail($benefit_id);

            // creates a subscription for the user with the status to pending.
            $subscription = $user->subscription()->create([
                'status' => 'Subscription Pending',
                'total' => $benefit->price
            ]);

            // updates the user to contain the new subscription id
            $user->update([
                'subscription_id' => $subscription->id
            ]);

            // creates a payment for the subscription with fields posted back from response.
            $subscription->payments()->create([
                'amount' => $_POST['amount'],
                'status' => 'Payment Complete',
                'gocardless_payment_id' => 'test',
                'transaction_id' => $_POST['transaction_id'],
                'benefit_id' => $benefit_id,
                'benefit_name' => $benefit->name
            ]);
        }
    }
}
