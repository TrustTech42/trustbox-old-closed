<?php

namespace App\Http\Controllers;

use App\Mail\UserRemove;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

use App\Http\Requests\InviteRequest;

// Models
use App\User;
use App\Partner;
use App\Invite;

// Mailables
use App\Mail\UserInvite;
use App\Mail\TrustedInvite;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class MailController extends Controller
{

    /**
     * * Set when a new member is invited by an existing member
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function invite(Request $request)
    {
        $plan = $request->get('plan');

        if ($plan !== "trusted") {
            $validator = Validator::make($request->all(), [
                'new_user_email' => 'unique:users,email'
            ]);

            if ($validator->fails()) {
                return Redirect::to(URL::previous() . '#additional_members')->withErrors($validator);
            }
        }

        $setting = Setting::find(1);
        $user = User::findorfail($request->get('user'));
        $partner = Partner::findorfail($request->get('partner'));

        if ($partner->invite_content) {
            $content = $partner->invite_content;
        } else {
            $content = $setting->invite_email_content;
        }

        $content = str_replace('@name@', $request->get('new_user_name'), $content);
        $content = str_replace('@user@', $user->name, $content);

        $new_email = $request->get('new_user_email');
        $code = Str::random(20);

        if ($plan === 'trusted') {
            if (User::where('email', $request->get('new_user_email'))->count() === 0) {
                # If email already has an account, prompt to accept / decline
                Mail::to($new_email)->send(new TrustedInvite($user, $partner, $plan, $content, $code, true, null));
            } else {
                # If email does not exist as an account, prompt to register
                Mail::to($new_email)->send(new TrustedInvite($user, $partner, $plan, $content, $code, false, User::where('email', $request->get('new_user_email'))->first()));
            }
        } else {
            Mail::to($new_email)->send(new UserInvite($user, $partner, $plan, $content, $code));
        }

        session()->flash('success', 'Your Invite has been sent to ' . $request->get('new_user_name') . ' at ' . $request->get('new_user_email'));

        Invite::create([
            'user_id' => $user->id,
            'name' => $request->get('new_user_name'),
            'email' => $new_email,
            'plan' => $plan,
            'code' => $code
        ]);

        return Redirect::to(URL::previous() . "#additional_members");
    }

    public function remove(Request $request)
    {
        $setting = Setting::find(1);
        $user = User::findorfail($request->get('user'));

        $content = str_replace('@name@', $user->name, $setting->removed_email_content);
        $content = str_replace('@user@', auth::user()->name, $content);
        $partner = Partner::findorfail($request->get('partner'));


        $vars = new \stdClass();
        $vars->partner = $partner;
        $vars->content = $content;

        Mail::to($user->email)->send(new UserRemove($vars));

        $user->delete();

        session()->flash('success', 'The user has been removed.');

        return back();
    }
}

