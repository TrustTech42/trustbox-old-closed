<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InviteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function messages()
    {
        return [
            'new_user_email.unique' => 'The person you tried to invite is already a member. You may want try an invite someone else who can benefit.',
            'email.unique' => 'The person you tried to invite is already a member. You may want try an invite someone else who can benefit.',
        ];
    }
}
