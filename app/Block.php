<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    protected $fillable = [
        "page_id", "name", "content", "template", "sort_order"
    ];

    public function page(){
        return $this->belongsTo("App\Page");
    }
}
