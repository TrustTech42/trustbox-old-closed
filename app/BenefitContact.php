<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BenefitContact extends Model
{
    protected $fillable = [
        'user_id',
        'customer_number',
        'benefit_name',
        'first_name',
        'last_name',
        'telephone',
        'email',
        'enquiry'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function benefit() {
        return $this->hasOne('App\Benefit');
    }
}
