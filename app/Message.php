<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'id',
        'sender_id',
        'recipient_id',
        'headline',
        'author',
        'type',
        'body',
        'banner',
        'colour',
        'is_read',
        'email_sent',
        'valid_from'
    ];

    /**
     * one to many inverse relation link between partners and messages
     */
    public function partners()
    {
        return $this->belongsTo('App\Partner');
    }
}
