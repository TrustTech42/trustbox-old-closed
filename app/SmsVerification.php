<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class SmsVerification extends Model
{
    protected $fillable = [
        'contact_number',
        'code',
        'status'
    ];

    public function store($details)
    {
        $this->fill($details);
        $sms = $this->save();

        return response()->json($sms, 200);
    }

    public function updateModel(Request $request)
    {
        $this->update($request->all());

        return $this;
    }
}
