<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    protected $fillable = [
        'type',
        'action',
        'entity_id',
        'partner_id',
        'status'
    ];

    public function partner()
    {
        return $this->belongsTo('App\Partner');
    }
}
