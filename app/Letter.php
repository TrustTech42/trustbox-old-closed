<?php

namespace App;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Database\Eloquent\Model;

class Letter extends Model
{
    protected $fillable = [
        'user_id',
        'url'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function createLetter()
    {
        $user = auth()->user();

        $data = [];
        $data['content'] = Setting::first()->welcome_letter_content;
        $data['user'] = $user;

        $path = public_path().'/letters/letter-'.$user->id.'-'.time().'.pdf';

        PDF::loadView('letters.welcome', $data)->save($path);

        $user->letter()->create([
            'url' => $path
                                ]);
    }
}
