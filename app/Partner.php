<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'primary_colour',
        'secondary_colour',
        'active',
        'header_mode',
        'logo',
        'subdomain',
        'address1',
        'address2',
        'city',
        'postcode',
        'contact_phone',
        'contact_email',
        'description',
        'nochex_id',
        'provider_id',
        'code_invite_content',
        'invite_content'
    ];

    /**
     * Relationships
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function benefits()
    {
        return $this->belongsToMany('App\Benefit')->withPivot('sort_order')->orderBy('sort_order', 'asc');
    }

    public function services()
    {
        return $this->belongsToMany('App\Service');
    }

    public function provider()
    {
        return $this->hasOne('App\Partner');
    }

    public function posts()
    {
        return $this->belongsToMany('App\Post',
            'partner_post',
            'partner_id',
            'post_id'
        )->withPivot('published')
            ->withTimestamps();
    }

    public function members(){
        return $this->hasManyThrough('App\User', 'App\Membership', 'partner_id', 'id', 'id', 'user_id');
    }

    /**
     * Many to Many relation linking partner to partner connection
     */
    public function connections()
    {
        return $this->belongsToMany('App\Partner', 'connections', 'partner_id', 'connection_id')
            ->withPivot('approved')
            ->withTimestamps();
    }
    public function connections2(){
        return $this->belongsToMany('App\Partner', 'connections', 'connection_id', 'partner_id')
            ->withPivot('approved')
            ->withTimestamps();
    }

    public function broadcasts()
    {
        return $this->hasMany('App\Message', 'sender_id')->where('type', 'broadcast');
    }

    public function allBroadcasts()
    {
        return $this->hasMany('App\Message', 'sender_id');
    }

    public function messages()
    {
        return $this->hasMany('App\Message', 'recipient_id');
    }

    public function getAllConnectionsAttribute()
    {
        return $this->connections->merge($this->connections2);
    }


    /**
     * Utility functions
     */
    public function hasBenefit($id)
    {
        return $this->benefits()->find($id) ? 1 : 0;
    }

    public function hasService($id)
    {
        return $this->services()->find($id) ? 1 : 0;
    }

    public function hasPost($id)
    {
        return $this->posts()->find($id) ? 1 : 0;
    }

    /**
     * Query to grab the unique benefits for a partner
     *
     * @param Partner $partner
     */
    public function getUniqueBenefits(Partner $partner)
    {
        return $partner->benefits()->exists() ? $partner->benefits()
            ->where('unique', '=', true)->get() : false;
    }

    /**
     * Query to grab members based on the invites plan
     *
     * @param Partner $partner
     */
    public function getMemberInvites(Partner $partner)
    {
        return $partner->members()->whereHas('invites', function ($q) {
            $q->where('plan', 'gifted')->orWhere('plan', 'household');
        });
    }

    /**
     * Query to grab partner members which were boosted
     *
     * @param Partner $partner
     */
    public function getBoostedMembers(Partner $partner)
    {
        return $partner->members()->whereHas('benefits', function ($q) {
            $q->where('boosted', true);
        });
    }
}
