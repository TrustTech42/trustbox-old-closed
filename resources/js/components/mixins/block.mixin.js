export const blockMixin = {
    props: ['index'],
    methods: {
        updateBlock: function (event, block) {
            this.$emit('updateBlock', event, block);
        },
        removeBlock: function (block, index) {
            this.$emit('removeBlock', block, index)
        },
        attachBlockImage: function (block, event) {
            let element = $(this.$el).find('.jumbotron, .image-col');
            let _this = this;

            var xhr = new XMLHttpRequest();
            var formData = new FormData();
            formData.append('file', event.target.files[0]);

            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4) {
                    element.css('background', "linear-gradient(rgba(0, 0, 0, 0.3), rgba(0,0,0,0.3)), url(/" + xhr.response + ")");
                }
            };

            xhr.open('POST', '/api/block/image/upload', false);
            xhr.send(formData);
        },
        sortMoveUp: function(block, index){
            this.$emit('sortMoveUp', block, index);
        },
        sortMoveDown: function(block, index){
            this.$emit('sortMoveDown', block, index);
        },
        changeSize(event, block, index)
        {
            this.$emit('changeSize', event, block, index);
        }
    },
    mounted: function () {
        let links = $(this.$el).find('a:not(.uneditable)');
        let _this = this;

        links.click(function (e) {
            e.preventDefault();

            let href = prompt('Please enter a href for this link', $(this).attr('href'));

            if (href) {
                $(this).attr('href', href);
            }
        });
    }
};
