// *********************************************
// Javascript Libraries
// *********************************************
require('./libraries/bootstrap');
require('bootstrap-colorpicker/dist/js/bootstrap-colorpicker');
require('./includes/admin/validation');
require('underscore/underscore-min');
require('slick-carousel/slick/slick.min');
// *********************************************
// Declare global variables
// *********************************************
window.Vue = require('vue');
// *********************************************
// Autoload vue components
// *********************************************
const adminComponents = require.context('./components/admin', true, /\.vue$/i);
adminComponents.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], adminComponents(key).default));

const files2 = require.context('./components/shared', true, /\.vue$/i);
files2.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files2(key).default));
// *********************************************
// Create a new vue app
// *********************************************
const app = new Vue({
    el: '#app'
});

// Colour picker
$(document).ready(function () {
    //$('.datepicker').datepicker();
    $('.colour-picker').colorpicker();

    // Custom file upload
    $(".custom-file-input").change(function (e) {
        let fileName = this.value.replace("C:\\fakepath\\", '');
        $(this).next('label').text(fileName);
    });

    // Navigation
    let url = window.location.toString();
    let uri = url.split('admin');
    uri     = uri[uri.length - 1].replace('/', '');

    $("a[href*='/" + uri.split('/')[0] + "']").addClass('active');

    if (uri === ''){
        $('#sidebar a').removeClass('active');
        $("#sidebar a").first().addClass('active');
    }
});

$(document).ready(function () {

    $('.nav-pills a').click(function (e) {
        e.preventDefault();

        let href = $(this).attr('href');

        if (href.includes('#'))
            $(this).tab('show');
        else
            window.location.href = href;
    });

// store the currently selected tab in the hash value
    $(".nav-pills a").on("show.bs.tab", function (e) {
        var id               = $(e.target).attr("href").substr(1);
        window.location.hash = id;
        $("[name='hash']").val(id);
    });

// on load of the page: switch to the currently selected tab
    var hash = window.location.hash;
    $("[name='hash']").val(hash);
    $('.nav-pills a[href="' + hash + '"]').tab('show');

    $('input[name="check_all"]').on('change', function()
    {
        var checkBoxes = $('.checked');
        checkBoxes.prop('checked', !checkBoxes.prop('checked'));
    })
});
