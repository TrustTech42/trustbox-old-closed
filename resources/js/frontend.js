// *********************************************
// Javascript Libraries
// *********************************************
require('./libraries/bootstrap');
require('slick-carousel/slick/slick.min');
require('bootstrap-colorpicker/dist/js/bootstrap-colorpicker');
require('./includes/mobile.js');
import Sortable from 'sortablejs';
// *********************************************
// Declare global variables
// *********************************************
window.Vue = require('vue');
// *********************************************
// Autoload vue components
// *********************************************
const files2 = require.context('./components/shared', true, /\.vue$/i);
files2.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files2(key).default));
// *********************************************
// Create a new vue app
// *********************************************
const app = new Vue({
    el: '#app'
});


$(document).ready(function () {
    // Sliders
    $(".block-slider").slick();

    // Alerts
    setTimeout(function () {
        $('.alert').fadeOut();
    }, 5000);

    $(".custom-file-input").change(function (e) {
        let fileName = this.value.replace("C:\\fakepath\\", '');
        $(this).next('label').text(fileName);
    });

    // Colour Picker
    $('.colour-picker').colorpicker();
});

// BS4 Validation
(function () {
    'use strict';
    window.addEventListener('load', function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

$(document).ready(function () {
    $('.nav-pills a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    // store the currently selected tab in the hash value
    $(".nav-pills a").on("show.bs.tab", function (e) {
        let id = $(e.target).attr("href").substr(1);
        window.location.hash = id;
        $("[name='hash']").val(id);
    });

    // on load of the page: switch to the currently selected tab
    var hash = window.location.hash;
    $("[name='hash']").val(hash);
    $('.nav-pills a[href="' + hash + '"]').tab('show');

    // Remove this
    //$("#storage-upgrade-modal").modal('show');
});

$(".benefit-toggle").change(function () {
    let includesFileManager = $(this).data('includes-file-manager');
    console.log(includesFileManager);

    if (includesFileManager == "1") {
        console.log("includes file manager");
        if (this.checked) {
            $("#boosted-benefit-2").prop("checked", false);
            $("#boosted-benefit-2").attr("checked", false);
        }
    }
});

$(document).ready(function () {
    /**
     * For the notify toggle
     */
    $('#notify').change(function (e) {
        $('#notify-date').toggleClass('hidden');
    });
});

$(document).ready(function () {
    $("form").on('propertychange change click keyup input paste', "input[type=password]", function () {
        console.log(!$(this)[0].checkValidity());
        if (!$(this)[0].checkValidity()) {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    })

    function getElementsByText(str, tag = 'a') {
        return Array.prototype.slice.call(document.getElementsByTagName(tag)).filter(el => el.textContent.trim() === str.trim());
    }

    let warnings = getElementsByText('The new user email has already been taken.', 'div');

    if (warnings.length > 0) {
        $(warnings[0]).text('This person is already a trustbox member. Feel free to invite someone else who you think could benefit');
    }
});

// Sortable
$(document).ready(function () {
    var el = document.getElementById('benefits-list-inner');
    var sortable = Sortable.create(el, {
        draggable: '.row',
        onSort: function (evt) {
            // get all benefits
            $('#benefits-list-inner .row').each(function (index) {
                $(this).find('[name="sort_order"]').val(index);

                let form = $(this).find('.sort_form');

                // console.log($(form).find('[name="benefit_id"]').val());

                $.post('/partner/sort', {
                    partner_id: $(form).find('[name="partner_id"]').val(),
                    benefit_id: $(form).find('[name="benefit_id"]').val(),
                    sort_order: $(form).find('[name="sort_order"]').val(),
                }).done(function (data) {
                    console.log(data);
                })
            })
        }
    });
});

$(document).ready(function () {
    /**
     * Used specifically for watching the inputs on the broadcast creation flow to do validation
     */
    validateSection(['headline', 'author', 'body'], '#confirm');

});

$(document).ready(function () {

    /**
     * Used specifically for watching the inputs on the blog creation flow to do validation
     */
    validateSection(['title', 'author'], '#write');
    validateSection(['headline', 'snippet', 'content'], '#additional');

    /**
     * For the benefit toggle
     */
    $('#benefitSwitch').change(function(e){
       $('#benefit_information').toggleClass('hidden');
    });
});

function validateSection(fields, button){
    setInterval(function(){
        let validated = true;
        let buttonElement = $('[href="' + button + '"]');

        buttonElement.attr('disabled', false);

        for(let field of fields) {
            if ($('#' + field).val() === '') {
                validated = false;
            }
        }

        if(!validated){
            buttonElement.attr('disabled', true);
        }
    }, 500);
}