$(document).ready(function () {
    // Mobile only
    if (window.innerWidth < 768) {
        // Variables
        let pillsArea = $("#sidebar .nav.nav-pills");
        let pills = pillsArea.find('.nav-link');
        let activeTab = $("<div></div>");
        let logoutButton = $(".logout-button").clone();

        // Move the logout button in to the tabs menu
        $(".logout-button").remove();
        pillsArea.append(logoutButton);

        // Turn pills in to collapsable menu
        activeTab.addClass('active-tab bg-partner');
        activeTab.html(`<i class="fa fa-user"></i> Account Details`);
        activeTab.click(function () {
            pills.toggle();
            logoutButton.toggle();
            $(this).toggleClass('open');
        });

        pills.hide();
        logoutButton.hide();
        pillsArea.prepend(activeTab);

        // On click, change content of activeTab to match clicked pill
        pills.click(function () {
            activeTab.html($(this).html());
            pills.hide();
            activeTab.removeClass('open');
            logoutButton.hide();
        });
    }
});

/* **********************************************************
    MOVE THIS TO ANOTHER FILE AT SOME POINT
 */

$(document).ready(function () {
    // Used for moving between step tabs
    $('button[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        let target = $(e.target).attr('href');
        let step = $('a[href="' + target + '"]');

        $('a[data-toggle="tab"]').removeClass('active');
        step.addClass('active');
    });

    // Used for copying the content of text fields in to their respective previews
    $('.steps input, .steps textarea').on('change', function () {
        let value = $(this).val();
        let target = $(this).attr('id');
        let previewBox = $('#' + target + '_preview');

        previewBox.text(value);
    });

    // Specifically for the banner preview
    $("#banner_preview").css({
        'width': '100%',
        'height': '100px',
        'backgroundColor': 'red',
        'display': 'block'
    });

    $('[name="colour"]').change(function(){
        $("#banner_preview").css('backgroundColor', $(this).val());
    });

    // Some quick manual validation for the post creation form
    setInterval(function(){

    }, 2000)
});