@if (session()->get('success'))
    <div class="alert alert-success mt-3 col-md-4">
        <div class="d-flex align-items-center justify-content-center">
            <i class="fal fa-check mr-3"></i> {{session()->get('success')}}
        </div>
    </div>
@endif
