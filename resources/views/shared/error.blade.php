@if (session()->get('error') || session()->get('errors'))
    <div class="alert alert-danger mt-3 col-md-4">
        <div class="d-flex align-items-center justify-content-center flex-wrap">
            <h5 class="w-100 text-center">There has been a problem </h5>
            {{session('error')}}
                @if($errors)
                    @foreach ($errors->all() as $error)
                        <div class="text-center mb-3">{{ $error }}</div>
                    @endforeach
                @else
                    {{session('error')}}
                @endif
        </div>
    </div>
@endif

