@extends('layouts.1col')

@section('content')
    <div class="error-container">
        <h1 class="h1 partner-title">404</h1>
        <p>Sorry, but we can't find the page you're looking for.</p>
    </div>
@endsection