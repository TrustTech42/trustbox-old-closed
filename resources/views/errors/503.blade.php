@extends('layouts.error', ['header_footer' => false])

@section('content')
    <div class="error-container text-center {{$global_partner->header_mode}}">
        <img src="{{$global_partner->logo}}" class="mb-5" alt="">
        <h1 class="h1 partner-title">We're down for maintenance</h1>
        <p>Sorry, but we can't find the page you're looking for.</p>
    </div>
@endsection
