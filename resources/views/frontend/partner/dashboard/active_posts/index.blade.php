@extends('layouts.2col')
@section('sidebar')
    <a href="/partner/{{$partner->id}}/dashboard" class="btn btn-primary btn-block dash-btn">
        <i class="fa fa-chevron-left"></i> Back
    </a>
@endsection
@section('content')
    <div class="container">
        <h2 class="h2 partner-title">View all active community posts</h2>
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table" style="display: inline-table">
                        <thead>
                        <tr style="color: {{$partner->primary_colour}}">
                            <th>Title</th>
                            <th>Date Published</th>
                            <th>Connection</th>
                            <th>Category</th>
                            <th>On/Off</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($activePosts as $post)
                            <tr>
                                @if($post->is_connected == 1)
                                    <td>{{$post->title}}</td>
                                    <td>{{$post->created_at->format('d/m/Y')}}</td>
                                    <td>{{$post->partners->first()->name}}</td>
                                    @foreach($post->categories as $category)
                                        <td>{{$category->name}}</td>
                                    @endforeach
                                    <td>
                                        {{$post->pivot->published  ? 'Yes' : 'No'}}
                                    </td>
                                    <td>
                                        <a href="{{route('partner.connection.active.posts.show',[$partner->id, $post->id])}}">View</a>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
