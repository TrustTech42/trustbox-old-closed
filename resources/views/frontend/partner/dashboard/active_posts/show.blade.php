@extends('layouts.2col')
@section('sidebar')
    <a href="{{url()->previous()}}" class="btn btn-primary btn-block dash-btn">
        <i class="fa fa-chevron-left"></i> Back
    </a>
@endsection
@section('content')
    <div class="container">
        <h2 class="h2 partner-title">View active community post</h2>
        <div class="row mt-4">
            <div class="col-md-2">
                <label for="title" style="font-weight: bold">Title:</label>
            </div>
            <div class="col-md-4">
                <input type="text" name="title" id="title" value="{{$post->title}}"
                       class="form-control" readonly>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-2">
                <label for="category" style="font-weight: bold">Category:</label>
            </div>
            <div class="col-md-4">
                @foreach($post->categories as $category)
                    <input type="text" name="category" id="category" value="{{$category->name}}"
                           class="form-control" readonly>
                @endforeach
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-2">
                <label for="benefit" style="font-weight: bold">Linked Benefit:</label>
            </div>
            <div class="col-md-4">
                @foreach($post->benefits as $benefit)
                    <input type="text" name="benefit" id="benefit" value="{{$benefit->name}}"
                           class="form-control" readonly>
                @endforeach
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-2">
                <label for="benefit_snippet" style="font-weight: bold">Benefit Snippet:</label>
            </div>
            <div class="col-md-6">
                 <textarea type="text" class="form-control" id="benefit_snippet" name="benefit_snippet" rows="3"
                           readonly>{{$post->benefit_snippet}}
                                </textarea>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-2">
                <label for="author" style="font-weight: bold">Author:</label>
            </div>
            <div class="col-md-4">
                <input type="text" name="author" id="author" value="{{$post->author}}"
                       class="form-control" readonly>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-2">
                <label for="headline" style="font-weight: bold">Headline:</label>
            </div>
            <div class="col-md-4">
                <input type="text" name="headline" id="headline" value="{{$post->headline}}"
                       class="form-control" readonly>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-2">
                <label for="snippet" style="font-weight: bold">Snippet:</label>
            </div>
            <div class="col-md-6">
               <textarea type="text" class="form-control" id="snippet" name="snippet" rows="3"
                         readonly>{{$post->snippet}}
                                </textarea>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-2">
                <label for="content" style="font-weight: bold">Full Post:</label>
            </div>
            <div class="col-md-6">
                 <textarea type="text" class="form-control" id="content" name="content" rows="3"
                           readonly>{{$post->content}}
                                </textarea>
            </div>
        </div>
        {!! Form::open(['route' => ['partner.connection.posts.update', $partner->id, $post->id], 'files' => true]) !!}
        @method('delete')
        @csrf
        <div class="row mt-4">
            {!! Form::close() !!}
            {!! Form::open(['route' => ['partner.connection.active.posts.delete', $partner->id, $post->id]]) !!}
            @method('delete')
            @csrf
            <div class="col-md-2">
                <button class="btn btn-danger" >Delete</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
