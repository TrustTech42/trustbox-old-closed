@extends('layouts.2col')
@section('sidebar')
    <a href="{{url()->previous()}}" class="btn btn-primary btn-block dash-btn">
        <i class="fa fa-chevron-left"></i> Back
    </a>
@endsection
@section('content')
    <div class="container">
        <h2 class="h2 partner-title">View customer contact message</h2>
        <div class="row mt-5">
            <div class="col-md-2">
                <p style="font-weight: bold">Title</p>
            </div>
            <div class="col-md-4">
                <input type="text" name="headline" value="{{$contact->name}}"
                       class="form-control" readonly="readonly">
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-2">
                <p style="font-weight: bold">Author</p>
            </div>
            <div class="col-md-4">
                <input type="text" name="author" value="{{$contact->email}}"
                       class="form-control" readonly="readonly">
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-2">
                <p style="font-weight: bold">Reason</p>
            </div>
            <div class="col-md-4">
                <input type="text" name="author" value="{{$contact->reason->name}}"
                       class="form-control" readonly="readonly">
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-2">
                <p style="font-weight: bold">Message</p>
            </div>
            <div class="col-md-6">
                <textarea type="text" class="form-control" id="message" style="text-align: left" readonly="readonly"
                          name="message" rows="5" required>{{$contact->message}}</textarea>
            </div>
        </div>
    </div>
@endsection
