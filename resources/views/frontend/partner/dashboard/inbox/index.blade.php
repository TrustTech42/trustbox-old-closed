@extends('layouts.2col')
@section('sidebar')
    <a href="/partner/{{$partner->id}}/dashboard" class="btn btn-primary btn-block dash-btn">
        <i class="fa fa-chevron-left"></i> Back
    </a>
@endsection
@section('content')
    <div class="container">
        <h2 class="h2 partner-title">View customer contact messages</h2>
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table" style="display: inline-table">
                        <thead>
                        <tr style="color: {{$partner->primary_colour}}">
                            <th>Name</th>
                            <th>Email</th>
                            <th>Reason</th>
                            <th>Read</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($contactMessages as $contact)
                            <tr>
                                <td>{{$contact->name}}</td>
                                <td>{{$contact->email}}</td>
                                <td>{{$contact->reason->name}}</td>
                                <td>{{$contact->resolved ? 'Yes' : 'No'}}</td>
                                <td>
                                    <a href="/partner/{{$partner->id}}/inbox/members/{{$contact->id}}">
                                        View
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection