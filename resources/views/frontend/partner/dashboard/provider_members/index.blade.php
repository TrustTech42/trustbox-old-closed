@extends('layouts.2col')
@section('sidebar')
    <a href="/partner/{{$partner->id}}/dashboard" class="btn btn-primary btn-block dash-btn">
        <i class="fa fa-chevron-left"></i> Back
    </a>
@endsection
@section('content')
    <div class="" id="provider_members">
        <h1 class="h1 partner-title">Provider</h1>
        <div class="row align-items-center">
            <div class="col-md">
                <h2 class="h2 member-title float-left">Enrolled/Subscribed Members</h2>
            </div>

            <div class="col d-none d-md-block">
                {!! Form::model($provider, ['route' => ['provider.user.filter', $provider->id]]) !!}
                <div class="input-group mr-3">
                    @if(isset($filter_active))
                        <div class="input-group-prepend">
                            <a href="/provider/{{ $provider->id }}" class="btn btn-primary" id="button-addon1"><i
                                        class="fa fa-sync-alt text-light"></i></a>
                        </div>
                    @endif
                    {{ Form::select('filter', $benefitsArr, null, ['class'=> 'form-control', 'aria-describedby' => 'button-addon1']) }}
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit" id="button-addon1"><i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>

            <div class="col d-none d-md-block">
                {!! Form::model($provider, ['route' => ['provider.user.search', $provider->id]]) !!}
                <div class="input-group">
                    @if(isset($search_active))
                        <div class="input-group-prepend">
                            <a href="/provider/{{ $provider->id }}" class="btn btn-primary" id="button-addon1"><i
                                        class="fa fa-sync-alt text-light"></i></a>
                        </div>
                    @endif

                    <input type="text" name="search" class="form-control" aria-label="Recipient's username"
                           aria-describedby="button-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit" id="button-addon2"><i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>

            <div class="col-md mb-2">
                <a id="provider-export" class="btn btn-primary float-right" href="/provider/{{$provider->id}}/export">
                    <i class="far fa-arrow-alt-circle-down mr-2"></i> Export as CSV
                </a>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table" style="display: inline-table">
                        <thead>
                        <tr>
                            <th>Customer Number</th>
                            <th>Name</th>
                            <th>DOB</th>
                            <th>Benefits</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($providerMembers as $member)
                            <tr>
                                <td>{{$member->customer_number}}</td>
                                <td>{{$member->name}}</td>
                                <td>{{ date('d/m/Y', strtotime($member->date_of_birth))}}</td>
                                <td>
                                    @foreach($member->benefits->where('provider_id', $provider->id) as $benefit)
                                        <div>
                                            @if($benefit->pivot->boosted === 1) <span
                                                    class="badge badge-primary">subscribed</span>  @endif {{$benefit->name}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            {{$providerMembers->links()}}
        </div>
    </div>

@endsection