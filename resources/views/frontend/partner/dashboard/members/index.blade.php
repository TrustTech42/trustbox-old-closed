@extends('layouts.2col')
@section('sidebar')
    <a href="/partner/{{$partner->id}}/dashboard" class="btn btn-primary btn-block dash-btn">
        <i class="fa fa-chevron-left"></i> Back
    </a>
@endsection
@section('content')
    <div class="" id="active_members">
        <h2 class="h2 partner-title mb-3">Active Members ({{$partner->members->count()}})</h2>
        <div class="row">
            <div class="col">
                <div class="table-responsive">
                    <table class="table" style="display: inline-table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>DOB</th>
                            <th>Phone Number</th>
                            <th>Email Address</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($partner->members as $member)
                            <tr>
                                <td>{{$member->name}}</td>
                                <td>{{ date('d/m/Y', strtotime($member->date_of_birth))}}</td>
                                <td>{{$member->telephone}}</td>
                                <td>{{$member->email}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection