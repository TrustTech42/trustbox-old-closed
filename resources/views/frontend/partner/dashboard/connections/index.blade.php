@extends('layouts.2col')
@section('sidebar')
    <a href="/partner/{{$partner->id}}/dashboard" class="btn btn-primary btn-block dash-btn">
        <i class="fa fa-chevron-left"></i> Back
    </a>
@endsection
@section('content')
    <div class="" id="connections">
        <h1 class="h1 partner-title">Connections</h1>
        <div class="row align-items-center">
            <div class="col-md mt-4 mb-4">
                <h2 class="h2 member-title float-left">New Connections</h2>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-md">
                <small class="text-success">
                    If you want to connect with new partners select an option from our existing Partners below:
                </small>
                {!! Form::open(['route' => ['partner.create.connection', $partner->id],'method' => 'post', 'class' => 'needs-validation','novalidate', null]) !!}
                {!! Form::select('partners[]', $connectionsList, '', ['class' => 'custom-select', 'multiple'], ['required']) !!}
                <button class="btn btn-primary btn-sm mt-3 float-right">
                    <i class="fas fa-fw fa-plus-circle mr-2"></i>
                    Add New Connection</button>
                {!! Form::close() !!}
            </div>
        </div>
        <hr>
        <div class="row align-items-center">
            <div class="col-md">
                <h2 class="h2 member-title float-left">Current Connections</h2>
            </div>
        </div>
        <div class="container mt-5">
            @foreach($partner->connections()->where('approved', '=', 1)->get() as $connection)
                <div class="row">
                    <div class="col-md-8">
                        <h3 style="color: {{$connection->primary_colour}}; font-weight: bold;"
                            class="h3">{{$connection->name}}</h3>
                        <p>{{strip_tags(\Illuminate\Support\Str::words($connection->description, 20, '...'))}}</p>
                    </div>
                    <div class="col-md-4">
                        {!! Form::open(['route' => ['partner.delete.connection', $partner->id, $connection->id]]) !!}
                        @method('delete')
                        @csrf
                        <button href="#" class="btn btn-danger float-right">
                            <input type="hidden" name="action" value="disable">
                            <i class="fa fa-times mr-1"></i>
                            Disable
                        </button>
                        {!! Form::close() !!}
                    </div>
                </div>

                <hr>
            @endforeach
            @foreach($partner->connections2()->where('approved', '=', 1)->get() as $connection)
                <div class="row">
                    <div class="col-md-8">
                        <h3 style="color: {{$connection->primary_colour}}; font-weight: bold;"
                            class="h3">{{$connection->name}}</h3>
                        <p>{{strip_tags(\Illuminate\Support\Str::words($connection->description, 20, '...'))}}</p>
                    </div>
                    <div class="col-md-4">
                        {!! Form::open(['route' => ['partner.delete.connection', $partner->id, $connection->id]]) !!}
                        @method('delete')
                        @csrf
                        <button href="#" class="btn btn-danger float-right mr-4">
                            <i class="fa fa-times mr-1"></i>
                            Disable
                        </button>
                        {!! Form::close() !!}
                    </div>
                </div>

                <hr>
            @endforeach
        </div>
    </div>

@endsection