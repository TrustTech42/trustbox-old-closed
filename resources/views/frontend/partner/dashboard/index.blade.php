@extends('layouts.1col')
@section('sidebar')
    <a href="/partner/{{$partner->id}}/dashboard" class="btn btn-primary btn-block dash-btn">
        <i class="fa fa-chevron-left"></i> Back
    </a>
@endsection
@section('content')
    <div class="container py-5" style="max-width: 1000px;">
        <p class="text-partner">Welcome <em>{{Auth::user()->name}}</em>, what would you like to do today?</p>

        <div class="row mt-4">
            <div class="col-4 pr-4">
                <a class="btn btn-primary btn-block dash-btn"
                   href="/partner/{{$partner->id}}/dashboard/company_details/index">Company Details</a>
            </div>
            <div class="col-4 px-4">
                <a class="btn btn-primary btn-block dash-btn" href="/partner/{{$partner->id}}/dashboard/benefits/index">Benefits</a>
            </div>
            <div class="col-4 pl-4">
                <a class="btn btn-primary btn-block dash-btn" href="/partner/{{$partner->id}}/dashboard/members/index">Active
                    Members</a>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-4 pr-4">
                <a class="btn btn-primary btn-block dash-btn"
                   href="/partner/{{$partner->id}}/dashboard/provider_members/index">Provider Members</a>
            </div>
            <div class="col-4 px-4">
                <a class="btn btn-primary btn-block dash-btn"
                   href="/partner/{{$partner->id}}/dashboard/connections/index">Connections</a>
            </div>
            <div class="col-4 pl-4">
                <a class="btn btn-primary btn-block dash-btn" href="/partner/{{$partner->id}}/dashboard/messages/index">Messages
                    ({{$partner->connections2()->where('approved', '=', 0)->count()}})</a>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-4 pr-4">
                <a class="btn btn-primary btn-block dash-btn"
                   href="/partner/{{$partner->id}}/dashboard/broadcast/index">Broadcasts</a>
            </div>
            <div class="col-4 px-4">
                <a class="btn btn-primary btn-block dash-btn" href="/partner/{{$partner->id}}/dashboard/inbox/index">Inbox</a>
            </div>
            <div class="col-4 pl-4">
                <a class="btn btn-primary btn-block dash-btn" href="/partner/{{$partner->id}}/dashboard/post/index">My
                    Posts</a>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-4 pr-4">
                <a class="btn btn-primary btn-block dash-btn" href="/partner/{{$partner->id}}/dashboard/post/create">Create
                    Post</a>
            </div>
            <div class="col-4 px-4">
                <a class="btn btn-primary btn-block dash-btn"
                   href="/partner/{{$partner->id}}/dashboard/community_posts/index">Community Posts</a>
            </div>
            <div class="col-4 pl-4">
                <a class="btn btn-primary btn-block dash-btn"
                   href="/partner/{{$partner->id}}/dashboard/active_posts/index">Active
                    Posts</a>
            </div>
        </div>
    </div>
@endsection