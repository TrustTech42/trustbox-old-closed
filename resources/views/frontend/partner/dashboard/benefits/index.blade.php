@extends('layouts.2col')
@section('sidebar')
    <a href="/partner/{{$partner->id}}/dashboard" class="btn btn-primary btn-block dash-btn">
        <i class="fa fa-chevron-left"></i> Back
    </a>
@endsection
@section('content')
    <div id="benefits">
        <h2 class="h2 partner-title mb-3">Turn On/Off Benefits</h2>

        @foreach($benefits as $benefit)
            <div class="row">
                <div class="col-md-8">
                    <h3 class="h3">{{$benefit->name}}</h3>
                    <p>{{strip_tags(\Illuminate\Support\Str::words($benefit->description, 20, '...'))}}</p>
                </div>
                <div class="col-md-4">
                    {{ Form::open(['route' => ['request.store']]) }}
                    <input type="hidden" name="entity_id" value="{{$benefit->id}}">
                    <input type="hidden" name="partner_id" value="{{$partner->id}}">
                    <input type="hidden" name="type" value="benefit">
                    @if($partner->hasBenefit($benefit->id))
                        @if(App\Request::where('type', 'benefit')->where('entity_id', $benefit->id)->where('partner_id', $partner->id)->where('status', null)->count() > 0)
                            <button class="btn btn-warning float-right" type="button">
                                Awaiting Approval
                            </button>
                        @else
                            <button href="#" class="btn btn-danger float-right">
                                <input type="hidden" name="action" value="disable">
                                <i class="fa fa-times mr-1"></i>
                                Disable
                            </button>
                        @endif
                    @else
                        @if(App\Request::where('type', 'benefit')->where('entity_id', $benefit->id)->where('partner_id', $partner->id)->where('status', null)->count() > 0)
                            <button class="btn btn-warning float-right" type="button">
                                Awaiting Approval
                            </button>
                        @else
                            <input type="hidden" name="action" value="enable">
                            <button href="#" class="btn btn-success float-right">
                                <i class="fa fa-check mr-1"></i>
                                Enable
                            </button>
                        @endif
                    @endif
                    {!! Form::close() !!}
                </div>
            </div>

            <hr>
        @endforeach

        @if($unique_benefits)
            @foreach($unique_benefits as $u_benefit)
                <div class="row">
                    <div class="col-8">
                        <h3 class="h3">{{$u_benefit->name}}</h3>
                        <p>{{\Illuminate\Support\Str::words($u_benefit->description, 20, '...')}}</p>
                    </div>
                    <div class="col-4">
                        {{ Form::open(['route' => ['request.store']]) }}
                        <input type="hidden" name="entity_id" value="{{$u_benefit->id}}">
                        <input type="hidden" name="partner_id" value="{{$partner->id}}">
                        <input type="hidden" name="type" value="benefit">
                        @if($partner->hasBenefit($u_benefit->id))
                            @if(App\Request::where('type', 'benefit')->where('entity_id', $u_benefit->id)->where('partner_id', $partner->id)->where('status', null)->count() > 0)
                                <button class="btn btn-warning float-right" type="button">
                                    Awaiting Approval
                                </button>
                            @else
                                <button href="#" class="btn btn-danger float-right">
                                    <input type="hidden" name="action" value="disable">
                                    <i class="fa fa-times mr-1"></i>
                                    Disable
                                </button>
                            @endif
                        @else
                            @if(App\Request::where('type', 'benefit')->where('entity_id', $u_benefit->id)->where('partner_id', $partner->id)->where('status', null)->count() > 0)
                                <button class="btn btn-warning float-right" type="button">
                                    Awaiting Approval
                                </button>
                            @else
                                <input type="hidden" name="action" value="enable">
                                <button href="#" class="btn btn-success float-right">
                                    <i class="fa fa-check mr-1"></i>
                                    Enable
                                </button>
                            @endif
                        @endif
                        {!! Form::close() !!}
                    </div>
                </div>

                <hr>
            @endforeach
        @endif
    </div>
@endsection