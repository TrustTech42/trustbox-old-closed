@extends('layouts.2col')
@section('sidebar')
    <a href="/partner/{{$partner->id}}/dashboard" class="btn btn-primary btn-block dash-btn">
        <i class="fa fa-chevron-left"></i> Back
    </a>
@endsection
@section('content')
    <div class="container">
        <h2 class="h2 partner-title">Browse Community Posts</h2>
        <div class="row mt-5">
            <div class="col-md-2 offset-2">
                <p class="h2 partner-title">Title</p>
            </div>
            <div class="col-md-6">
                {!! Form::open(['route' => [ 'partner.connection.posts.search', $partner->id]])!!}
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <a href="/partner/{{$partner->id}}/dashboard/community_posts/index" class="btn btn-primary"
                           id="button-addon1"><i class="fa fa-sync-alt text-light"></i></a>
                    </div>
                    <input type="text" name="search" class="form-control"
                           aria-describedby="button-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit" id="button-addon2"><i
                                    class="fa fa-search"></i></button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-2 offset-2">
                <p class="h2 partner-title">Connection</p>
            </div>
            <div class="col-md-6">
                {!! Form::open(['route' => ['partner.connection.posts.connections.filter', $partner->id]]) !!}
                <div class="input-group mr-3">
                    <div class="input-group-prepend">
                        <a href="/partner/{{$partner->id}}/dashboard/community_posts/index" class="btn btn-primary"
                           id="button-addon1"><i class="fa fa-sync-alt text-light"></i></a>
                    </div>
                    {{ Form::select('filter', $partner->allConnections->where('pivot.approved', 1)->pluck('name', 'name'), '', ['placeholder' => '','class'=> 'form-control', 'aria-describedby' => 'button-addon1']) }}
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit" id="button-addon1"><i class="fa fa-filter"></i>
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-2 offset-2">
                <p class="h2 partner-title">Category</p>
            </div>
            <div class="col-md-6">
                {!! Form::open(['route' => ['partner.connection.posts.categories.filter', $partner->id]]) !!}
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <a href="/partner/{{$partner->id}}/dashboard/community_posts/index" class="btn btn-primary"
                           id="button-addon1"><i class="fa fa-sync-alt text-light"></i></a>
                    </div>
                    {{ Form::select('filter', \App\Category::all()->pluck('name', 'name'), '', ['placeholder' => '', 'class'=> 'form-control', 'aria-describedby' => 'button-addon1']) }}
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit" id="button-addon1"><i class="fa fa-filter"></i>
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table" style="display: inline-table">
                        <thead>
                        <tr style="color: {{$partner->primary_colour}}">
                            <th>Title</th>
                            <th>Date Published</th>
                            <th>Connection</th>
                            <th>Category</th>
                            <th>On/Off</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($connections as $connection)
                            @foreach($connection->posts->where('enabled', 1)->where('pivot.published', 0) as $post)
                                <tr>
                                    @if($post->is_connected == 1)
                                        <td>{{$post->title}}</td>
                                        <td>{{$post->created_at->format('d/m/Y')}}</td>
                                        <td>{{$connection->name}}</td>
                                        @foreach($post->categories as $category)
                                            <td>{{$category->name}}</td>
                                        @endforeach
                                        @if($partner->posts->contains('id', $post->id))
                                            <td>
                                                On
                                            </td>
                                        @else
                                            <td>
                                                Off
                                            </td>
                                        @endif
                                        <td>
                                            @if($partner->posts->contains('id', $post->id))
                                                <a href="{{route('partner.connection.active.posts.show',[$partner->id, $post->id])}}">View</a>
                                            @else
                                                <a href="{{route('partner.connection.posts.show',[$partner->id, $post->id])}}">View</a>
                                            @endif
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
