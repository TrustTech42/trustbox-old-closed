@extends('layouts.2col')
@section('sidebar')
    <a href="/partner/{{$partner->id}}/dashboard" class="btn btn-primary btn-block dash-btn">
        <i class="fa fa-chevron-left"></i> Back
    </a>
@endsection
@section('content')
    <div class="steps">
        <nav>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                @include('frontend.partner.dashboard.post.tabs.steps')
            </ul>
        </nav>
        {!! Form::open(['route' => ['partner.mine.posts.store', $partner->id], 'files' => true]) !!}
        @method('POST')
        <div class="tab-content" id="myTabContent">
            @include('frontend.partner.dashboard.post.tabs.basic')
            @include('frontend.partner.dashboard.post.tabs.write')
            @include('frontend.partner.dashboard.post.tabs.additional')
            @include('frontend.partner.dashboard.post.tabs.confirm')
        </div>
        {!! Form::close() !!}
    </div>
@endsection