@extends('layouts.2col')
@section('sidebar')
    <a href="/partner/{{$partner->id}}/dashboard" class="btn btn-primary btn-block dash-btn">
        <i class="fa fa-chevron-left"></i> Back
    </a>
@endsection
@section('content')
    <div class="container">
        {!! Form::open(['route' => ['partner.mine.posts.update', $partner->id, $post->id], 'files' => true]) !!}
        @method('patch')
        @csrf
        <h2 class="h2 partner-title">View Own Post</h2>
        <div class="row mt-3">
            <div class="col-md-2">
                <label for="published" style="font-weight: bold">Published:</label>
            </div>
            <div class="col-md-4">
                {!! Form::bsSwitch('enabled', $post->enabled ?? '') !!}
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-2">
                <label for="title" style="font-weight: bold">Title:</label>
            </div>
            <div class="col-md-4">
                <input type="text" name="title" id="title" value="{{$post->title}}"
                       class="form-control">
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-2">
                <label for="categories" style="font-weight: bold">Category:</label>
            </div>
            <div class="col-md-2">
                <select class="form-control" name="categories" id="categories">
                    @foreach($post->categories as $linkedCategory)
                        <option value="{{$linkedCategory->id}}">{{$linkedCategory->name}}</option>
                    @endforeach
                    @foreach($categories->except($linkedCategory->id) as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        @if(!$post->benefits->isEmpty())
            <div class="row mt-3">
                <div class="col-md-2">
                    <label for="benefits" style="font-weight: bold">Linked Benefit:</label>
                </div>
                <div class="col-md-4">
                    <select class="form-control" name="benefits" id="benefits">
                        @foreach($post->benefits as $linkedBenefit)
                            <option value="{{$linkedBenefit->id}}">{{$linkedBenefit->name}}</option>
                        @endforeach
                        @foreach($partnerBenefits->except($linkedBenefit->id) as $benefit)
                            <option value="{{$benefit->id}}">{{$benefit->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="unlinkBenefit" name="unlinkBenefit">
                        <label class="custom-control-label" for="unlinkBenefit">Unlink benefit</label>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-md-2">
                    <label for="benefit_snippet" style="font-weight: bold">Benefit Snippet:</label>
                </div>
                <div class="col-md-6">
                 <textarea type="text" class="form-control" id="benefit_snippet" name="benefit_snippet" rows="3">{{$post->benefit_snippet}}
                                </textarea>
                </div>
            </div>
        @endif
        <div class="row mt-3">
            <div class="col-md-2">
                <label for="author" style="font-weight: bold">Author:</label>
            </div>
            <div class="col-md-4">
                <input type="text" name="author" id="author" value="{{$post->author}}"
                       class="form-control">
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-2">
                <label for="headline" style="font-weight: bold">Headline:</label>
            </div>
            <div class="col-md-4">
                <input type="text" name="headline" id="headline" value="{{$post->headline}}"
                       class="form-control">
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-2">
                <label for="snippet" style="font-weight: bold">Snippet:</label>
            </div>
            <div class="col-md-6">
               <textarea type="text" class="form-control" id="snippet" name="snippet" rows="3"
                         required>{{$post->snippet}}
                                </textarea>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-2">
                <label for="content" style="font-weight: bold">Full Post:</label>
            </div>
            <div class="col-md-6">
                 <textarea type="text" class="form-control" id="content" name="content" rows="3"
                           required>{{$post->content}}
                                </textarea>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-3">
                <label for="is_guest" style="font-weight: bold">Post available when not logged in</label>
            </div>
            <div class="col-md-1">
                {!! Form::bsSwitch('is_guest', $post->is_guest ?? '') !!}
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-md-3">
                <label for="is_connected" style="font-weight: bold">Post available to connected members</label>
            </div>
            <div class="col-md-1">
                {!! Form::bsSwitch('is_connected', $post->is_connected ?? '') !!}
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-2">
                <button class="btn btn-primary mr-2">Update</button>
            </div>
            {!! Form::close() !!}
            {!! Form::open(['route' => ['partner.mine.posts.delete', $partner->id, $post->id]]) !!}
            @method('delete')
            @csrf
            <div class="col-md-2">
                <button class="btn btn-danger">Delete</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    </div>
@endsection
