@extends('layouts.2col')
@section('sidebar')
    <a href="/partner/{{$partner->id}}/dashboard" class="btn btn-primary btn-block dash-btn">
        <i class="fa fa-chevron-left"></i> Back
    </a>
@endsection
@section('content')
    <div class="container">
        <h2 class="h2 partner-title">My Posts</h2>
        <div class="row mt-4">
            <div class="col-md-6">
                {!! Form::open(['route' => [ 'partner.mine.posts.search', $partner->id]])!!}
                <div class="input-group mb-3">
                    <input type="text" name="search" class="form-control"
                           aria-describedby="button-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit" id="button-addon2"><i
                                    class="fa fa-search"></i></button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table" style="display: inline-table">
                        <thead>
                        <tr style="color: {{$partner->primary_colour}}">
                            <th>Title</th>
                            <th>Date Published</th>
                            <th>Shared With Community</th>
                            <th>Category</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($posts->where('pivot.published', null) as $post)
                            <tr>
                                <td>{{$post->title}}</td>
                                <td>{{$post->created_at->format('d-m-Y')}}</td>
                                <td>{{$post->is_connected ? 'Yes' : 'No'}}</td>
                                @foreach($post->categories as $category)
                                    <td>{{$category->name}}</td>
                                @endforeach
                                <td>
                                    <a href="{{route('partner.mine.posts.show',[$partner->id, $post->id])}}">View</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
