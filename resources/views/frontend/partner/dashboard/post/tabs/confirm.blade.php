<div class="tab-pane fade py-5" id="confirm">
    <p class="text-center">Based on your previous option, this post will [now] be published and your TrustBox customers [will] be notified.</p>

    <div class="row">
        <div class="col">
            <button class="btn btn-primary btn-lg btn-block" type="button" data-toggle="tab" href="#additional">GO BACK</button>
        </div>
        <div class="col">
            <button class="btn btn-primary btn-lg btn-block" type="button" data-toggle="modal" data-target="#exampleModal">PREVIEW POST</button>
        </div>
        <div class="col">
            <button class="btn btn-primary btn-lg btn-block">CONFIRM & SEND</button>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Post Preview</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="blog_preview">
                    <div id="banner_preview">
                        <div class="inner text-center">
                            <div id="title_preview"></div>
                            <div id="date_preview">Date Posted: {{\Illuminate\Support\Carbon::today()->toFormattedDateString()}}</div>
                        </div>
                    </div>
                    <div id="content_preview" class="text-center mt-4"></div>
                </div>
                <!-- benefit preview in next dev -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>