<div class="tab-pane fade py-5" id="write">
    <div class="row mt-4">
        <div class="col-md-2">
            <label for="headline" style="font-weight: bold">Headline: <small>(required)</small></label>
        </div>
        <div class="col-md-4">
            <input type="text" id="headline" name="headline" class="form-control" required>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-md-2">
            <label for="banner" style="font-weight: bold">Image:</label>
        </div>
        <div class="col-md-3">
            <div class="input-group mb-3">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="banner" name="banner">
                    <label class="custom-file-label" for="banner">Select Image</label>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <colour-dropdown name="colour"></colour-dropdown>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <label for="snippet" class="form-label" style="font-weight: bold">Snippet <small>(required)</small></label>
                <textarea class="form-control" name="snippet" id="snippet" rows="4" required>
            </textarea>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <label for="content" class="form-label" style="font-weight: bold">Full Post <small>(required)</small></label>
                <textarea class="form-control" name="content" id="content" rows="6" required>
            </textarea>
            </div>
        </div>
    </div>

    <div class="form-group">
        <button class="btn btn-primary float-right" type="button" data-toggle="tab" href="#additional">NEXT STEP</button>
    </div>
</div>