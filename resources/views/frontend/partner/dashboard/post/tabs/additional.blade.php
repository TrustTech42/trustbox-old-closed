<div class="tab-pane fade py-5" id="additional">
    <div class="custom-control custom-switch">
        <input type="checkbox" class="custom-control-input" id="benefitSwitch" name="benefitSwitch">
        <label class="custom-control-label" for="benefitSwitch">Attach benefit to post</label>
    </div>

    <div id="benefit_information" class="hidden">
        <div class="row mt-4">
            <div class="col-md-2">
                <label for="benefits" style="font-weight: bold">Please select a benefit:</label>
            </div>
            <div class="col-md-4">
                <select class="form-control" name="benefits" id="benefits">
                    @foreach($partnerBenefits as $benefit)
                        <option value="{{$benefit->id}}">{{$benefit->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label for="benefit_snippet" class="form-label" style="font-weight: bold;">Introduction to benefit
                        text</label>
                    <textarea class="form-control" name="benefit_snippet" id="benefit_snippet" rows="6">
            </textarea>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-8">
            {!! Form::bsSwitch('is_guest', $post->is_guest ?? '', 'Should this post be available when not logged in?') !!}
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-md-8">
            {!! Form::bsSwitch('is_connected', $post->is_connected ?? '', 'Do you want this post to be available for your connections?') !!}
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-4">
            {!! Form::bsSwitch('notify', $post->notify ?? '', 'Do you want to notify your customers of the new post?') !!}
        </div>
        <div id="notify-date" class="hidden col-md-4">
            {{Form::date('notify_from', \Carbon\Carbon::now()), ['class' => 'form-control']}}
        </div>
    </div>

    <div class="form-group">
        <button class="btn btn-primary float-right" type="button" data-toggle="tab" href="#confirm">NEXT STEP</button>
    </div>
</div>