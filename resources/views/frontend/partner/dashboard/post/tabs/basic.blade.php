<div class="tab-pane fade show active py-5" id="basic">
    <div class="row mt-3">
        <div class="col-md-2">
            <label for="published" style="font-weight: bold">Publicly Visible:</label>
        </div>
        <div class="col-md-4">
            {!! Form::bsSwitch('enabled', $post->enabled ?? '') !!}
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-md-2">
            <label for="title" style="font-weight: bold">Title: <small>(required)</small></label>
        </div>
        <div class="col-md-4">
            <input type="text" id="title" name="title" class="form-control" required>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-md-2">
            <label for="categories" style="font-weight: bold">Tags: <small>(required)</small></label>
        </div>
        <div class="col-md-4">
            <select class="form-control" name="categories" id="categories">
                @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-md-2">
            <label for="author" style="font-weight: bold">Author: <small>(required)</small></label>
        </div>
        <div class="col-md-4">
            <input type="text" id="author" name="author" class="form-control" required>
        </div>
    </div>

    <div class="form-group">
        <button class="btn btn-primary float-right" type="button" data-toggle="tab" href="#write" disabled>NEXT STEP</button>
    </div>
</div>