<li class="nav-item">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#basic">
        <span>Basic Information</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link" data-toggle="tab" id="profile-tab" href="#write">
        <span>Write the Post</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link" data-toggle="tab" id="profile-tab" href="#additional">
        <span>Additional Options</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link" data-toggle="tab" id="profile-tab" href="#confirm">
        <span>Confirm</span>
    </a>
</li>