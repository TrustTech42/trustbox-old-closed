@extends('layouts.2col')
@section('sidebar')
    <a href="/partner/{{$partner->id}}/dashboard" class="btn btn-primary btn-block dash-btn">
        <i class="fa fa-chevron-left"></i> Back
    </a>
@endsection
@section('content')
    <div class="steps">
        <nav>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                @include('frontend.partner.dashboard.broadcast.tabs.steps')
            </ul>
        </nav>
        {!! Form::open(['route' => [ 'partner.store.broadcasts', $partner->id], 'enctype' => 'multipart/form-data', 'class' => 'needs-validation','novalidate']) !!}
        @method('POST')
        <div class="tab-content" id="myTabContent">
            @include('frontend.partner.dashboard.broadcast.tabs.compose')
            @include('frontend.partner.dashboard.broadcast.tabs.confirm')
        </div>
        {!! Form::close() !!}
    </div>
@endsection
