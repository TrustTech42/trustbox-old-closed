@extends('layouts.2col')
@section('sidebar')
    <a href="/partner/{{$partner->id}}/dashboard" class="btn btn-primary btn-block dash-btn">
        <i class="fa fa-chevron-left"></i> Back
    </a>
@endsection
@section('content')
    <div class="container">
        <h2 class="h2 partner-title d-flex align-items-center">
            View previous broadcast messages
            <a href="/partner/{{$partner->id}}/dashboard/broadcast/create" class="btn btn-primary ml-auto">Create new Broadcast</a>
        </h2>
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table" style="display: inline-table">
                        <thead>
                        <tr style="color: {{$partner->primary_colour}}">
                            <th>Title</th>
                            <th>Date Created</th>
                            <th>Schedule Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($broadcasts as $broadcast)
                            <tr>
                                <td>{{$broadcast->headline}}</td>
                                <td>{{$broadcast->created_at->format('d-m-Y')}}</td>
                                <td>{{Carbon\Carbon::parse($broadcast->valid_from)->format('d-m-Y') }}</td>
                                <td>
                                    <a href="/partner/{{$partner->id}}/dashboard/broadcast/view/{{$broadcast->id}}">
                                        View
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection