<div class="tab-pane fade show active py-5" id="compose">
    <div class="form-group row">
        <label for="headline" class="col-md-3 font-weight-bold">Message Heading <small>(required)</small></label>
        <div class="col-md-9">
            <input type="text" id="headline" name="headline" class="form-control" required>
        </div>
    </div>

    <div class="form-group row">
        <label for="author" class="col-md-3 font-weight-bold">Who is the message from? <small>(required)</small></label>
        <div class="col-md-9">
            <input type="text" id="author" name="author" class="form-control" required>
        </div>
    </div>

    <div class="form-group row">
        <label for="author" class="col-md-3 font-weight-bold">Would you like to add a banner? <small>(required)</small></label>
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-auto">
                    <label for="banner" class="btn btn-primary">
                        <i class="fa fa-upload mr-2"></i> Upload Banner
                    </label>
                </div>
                <div class="col-md-auto">
                    <input type="file" name="banner" id="banner" class="d-none">
                    <colour-dropdown name="colour"></colour-dropdown>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label for="" class="col-md-3 font-weight-bold">Compose Message <small>(required)</small></label>
        <div class="col-md-9">
            <textarea type="text" class="form-control text-left" name="body" rows="8" id="body" required></textarea>
        </div>
    </div>
    <div class="form-group row">
        <label for="" class="col-md-3 font-weight-bold">Schedule Broadcast</label>
        <div class="col-md-3">
            {{Form::date('valid_from', \Carbon\Carbon::now()), ['class' => 'form-control']}}
        </div>
    </div>
    <div class="form-group">
        <button class="btn btn-primary float-right" type="button" data-toggle="tab" href="#confirm">Review & Send
        </button>
    </div>
</div>