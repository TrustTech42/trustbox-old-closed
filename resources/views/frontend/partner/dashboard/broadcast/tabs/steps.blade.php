<li class="nav-item">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#compose">
        <span>Compose Your Message</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link" data-toggle="tab" id="profile-tab" href="#confirm">
        <span>Confirm & Send</span>
    </a>
</li>