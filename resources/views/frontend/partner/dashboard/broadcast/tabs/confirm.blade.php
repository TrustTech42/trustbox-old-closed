<div class="tab-pane fade" id="confirm">
    <div id="broadcast-preview" class="preview">
        <div class="previewBlock" id="banner_preview"></div>
        <div class="previewBlock" id="headline_preview"></div>
        <div class="previewBlock" id="author_preview"></div>
        <div class="previewBlock" id="body_preview"></div>
    </div>

    <div class="form-group">
        <button class="btn btn-primary float-right" type="submit">CONFIRM & SEND
        </button>
    </div>
</div>