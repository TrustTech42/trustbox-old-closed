@extends('layouts.2col')
@section('sidebar')
    <a href="/partner/{{$partner->id}}/dashboard" class="btn btn-primary btn-block dash-btn">
        <i class="fa fa-chevron-left"></i> Back
    </a>
@endsection
@section('content')
    <div class="tab-pane fade active show " id="company_details">
        <h2 class="h2 partner-title mb-3">Company Details</h2>

        {!! Form::open(['route' => ['partner.update', $partner->id], 'files' => true]) !!}
        @method('patch')
        @csrf
        <div class="row mb-3">
            <div class="col-md mb-3 mb-md-0">
                <h3 class="h3 partner-title mb-3">Change Logo</h3>

                <logo-changer current="{{$partner->logo}}" header_mode="{{$partner->header_mode}}"></logo-changer>
            </div>
            <div class="col-md">
                <h3 class="h3 partner-title mb-3">Change Brand Colours</h3>

                <header-dropdown current="{{$partner->header_mode}}" class="mb-3"></header-dropdown>
                <colour-dropdown current="{{$partner->primary_colour}}"></colour-dropdown>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <h3 class="h3 partner-title mb-3">Account Details</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name" class="form-label">Company Name</label>
                    <input type="text" class="form-control" name="name" value="{{$partner->name}}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="address1" class="form-label">House No / Name</label>
                    <input type="text" class="form-control" name="address1" value="{{$partner->address1}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="address2" class="form-label">Address 2</label>
                    <input type="text" class="form-control" name="address2" value="{{$partner->address2}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="city" class="form-label">City</label>
                    <input type="text" class="form-control" name="city" value="{{$partner->city}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="postcode" class="form-label">Postcode</label>
                    <input type="text" class="form-control" name="postcode" value="{{$partner->postcode}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="contact_phone" class="form-label">Contact Telephone</label>
                    <input type="text" class="form-control" name="contact_phone" value="{{$partner->contact_phone}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="contact_email" class="form-label">Email Address</label>
                    <input type="text" class="form-control" name="contact_email" value="{{$partner->contact_email}}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md">
                <button class="btn btn-primary float-right">Save Changes</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection