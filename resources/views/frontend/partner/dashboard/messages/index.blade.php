@extends('layouts.2col')
@section('sidebar')
    <a href="/partner/{{$partner->id}}/dashboard" class="btn btn-primary btn-block dash-btn">
        <i class="fa fa-chevron-left"></i> Back
    </a>
@endsection
@section('content')
    <div class="" id="messages">
        <h2 class="h2 partner-title mb-3">Messages</h2>
        @foreach($partner->connections2()->where('approved', '=', 0)->get() as $connection)
            <div class="row">
                <div class="col-md-8">
                    <h3 style="color: {{$connection->primary_colour}}; font-weight: bold;"
                        class="h3">{{$connection->name}}
                    </h3>
                    <p>Want's to connect!</p>
                </div>
                <div class="col-md-4">
                    {!! Form::open(['route' => ['partner.delete.connection', $partner->id, $connection->id]]) !!}
                    @method('delete')
                    @csrf
                    <button href="#" class="btn btn-danger float-right mr-4">
                        <i class="fa fa-times mr-1"></i>
                        Reject
                    </button>
                    {!! Form::close() !!}
                    {!! Form::open(['route' => ['partner.update.connection', $partner->id, $connection->id]]) !!}
                    @method('patch')
                    @csrf
                    <button href="#" class="btn btn-success float-right mr-4">
                        <input type="hidden" name="action" value="enable">
                        <i class="fa fa-check mr-1"></i>
                        Connect
                    </button>
                    {!! Form::close() !!}
                </div>
            </div>
            <hr>
        @endforeach
    </div>
@endsection