<!DOCTYPE html>
@extends('layouts.1col')
@section('header_scripts')
    <script src="https://secure.nochex.com/exp/jquery.js"></script>
    <script src="https://secure.nochex.com/exp/nochex_lib.js"></script>
@endsection
<body>
<form id="nochexForm" class="ncx-form" name="nochexForm">
    <script id="ncx-config"
            ncxField-api_key="{{$ncxApiKey}}"
            ncxField-description="{{$ncxDescription}}"
            ncxField-merchant_id="{{$ncxMerchantId}}"
            ncxField-amount="{{$ncxAmount}}"
            ncxField-fullname="{{$user->first_name}} {{$user->last_name}}"
            ncxField-email="{{$user->email}}"
            ncxField-phone="{{$user->telephone}}"
            ncxField-address="{{$user->address2}}"
            ncxField-city="{{$user->city}}"
            ncxField-postcode="{{$user->postcode}}"
            ncxField-use_apc="true"
            ncxField-success_url="{{route('direct.show.user', $user->id)}}"
            ncxField-callback_url="https://yourtrustbox.co.uk/api/lifetime/callback"
            ncxField-test_transaction="true"
            ncxField-optional_1="{{$user->id}}"
    ></script>
    @csrf
</form>
</body>
@section('content')
    <div class="container block block-text py-5">
        <div class="row">
            <div class="col-6 offset-3">
                <h1 class="h1 partner-title text-capitalize">Payment Information</h1>
                Please click the pay now button below and fill in the relevant information within the checkout.
            </div>
        </div>
    </div>

    @include('shared.error')
    <div class="container">
        <div class="row mb-5">
            <div class="col-6 offset-3">
                <div class="form bg-light p-3 p-5">
                    <h2 class="h4 mb-2">Amount: £295.00</h2>
                    <h2 class="h5 mb-5">For: Upgrade - Full Membership</h2>
                    <button id="ncx-show-checkout" class="btn btn-primary btn-block mt-3" type="button">Pay Now</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="payment-success">
        <div class="modal-dialog">
            <div class="modal-content">

            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
