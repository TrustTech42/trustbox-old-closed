@extends('layouts.empty')
@section('content')
   <div class="container py-3">
       <h1 class="h1 text-partner text-center my-5">Terms & Conditions</h1>

       <p><strong>1.0 General terms</strong></p>
       <p>1.1 Important information</p>
       <p>In these Terms, references to &ldquo;Trust Box&rdquo;, &ldquo;us&rdquo;, &ldquo;our&rdquo; and/or &ldquo;we&rdquo; means Trustbox Ltd.</p>
       <p>This is our standard Terms of Business Agreement upon which we intend to rely.&nbsp; These Terms contain important information regarding the services that we provide to you so, for your own benefit and protection, please read these Terms carefully before accepting them. &nbsp;If there is anything you don&rsquo;t understand please send your query via the contact form and we will do our best to explain things.</p>
       <p>1.2 Your agreement with us</p>
       <p>These Terms, together with (a) the information we have provided to you via either the App or the Portal about our services and fees; and (b) the information you have provided to us via the App or the Portal under these Terms in relation to your Trust Box Account constitutes the agreement (&ldquo;the Trust Box Agreement&rdquo;) between you and us in the provision of our services to you. &nbsp;Trustbox Ltd is registered in England &amp; Wales under Company Registration Number <strong>11077262</strong>; its registered address being 5 Millbank House, Riverside Business Court, Cheshire, Wilmslow, England, SK9 1BJ.</p>
       <p>1.3 Commencement and status of the Trust Box agreement</p>
       <p>The Trust Box Agreement will be legally binding when you accept these Terms. &nbsp;However, we will only start to provide our services to you after we have notified you that your Trust Box Account with us has been opened and, accordingly, those terms relating to your Trust Box Account will only be applicable after that time. &nbsp;The Trust Box Agreement has no duration period.</p>
       <p>The Trust Box Agreement is governed by the laws of England and Wales. &nbsp;If any provision of the Trust Box Agreement is, or becomes, invalid or unenforceable, the relevant provision will be treated as if it were not in the Trust Box Agreement, and the remaining provisions will still be valid and enforceable.</p>
       <p><strong>2.0 How Trust Box provides services</strong></p>
       <p>2.1 The Trust Box account and the Trust Box services</p>
       <p>We will provide services (the &ldquo;Trust Box Services &ldquo;) to you in connection with the set up and operation of your Trust Box Account. &nbsp;We will provide the Trust Box Services to you in accordance with the Terms of the Trust Box Agreement, all applicable laws.</p>
       <p>We will do our best to ensure the App and your Trust Box Account is available and up and running for you to use at any time, however, this is not something we can guarantee. &nbsp;There may be times where the App and your Trust Box Account may not be available and where we may not be able to facilitate the services. There may be interruptions and/or delays to our services. &nbsp;If this happens, we will do our best to contact you to let you know what you should do. &nbsp;</p>
       <p>We may suspend the operation of our services where we consider it necessary, including (but not limited to) where we have to suspend operations for technical problems, emergencies, maintenance, regulatory reasons, where we decide it is sensible for your protection, in periods of exceptional trading activity or to ensure the continued availability of other services. If this happens, we will do our best to contact you to let you know what you should do.</p>
       <p>2.2 Your use of our portal app</p>
       <p>In addition to the Terms set out here, you acknowledge and confirm your continuing agreement to our&nbsp;Privacy Policy&nbsp;and the Portal &amp; App Terms relating to the use of our App (together, the &ldquo;Data Terms &ldquo;). If there are any terms contained in the Data Terms that are inconsistent with or conflict with the terms, conditions and provisions set out in these Terms then, the relevant term, condition or provision set out in these Terms shall prevail.</p>
       <p>Trust Box will provide you with security details to access your Trust Box Account. You must keep these details safe and confidential and notify Trust Box immediately if you think that someone else may know these details and/or may have unauthorised access to your Account (otherwise in accordance with these terms).</p>
       <p>You must not undertake any action that could compromise the security or effective working of the Trust Box Services. Any such action will be considered a material breach of these Terms.</p>
       <p>You are responsible for monitoring your Trust Box Account, ensuring that you read all messages that have been sent to you (whether through the notifications and messaging centres in the App and the Portal, via the email address you have provided to us, or via any other means as agreed between us) and informing us if there is something wrong on your Trust Box Account or you suspect that it has been subject to unauthorised use.</p>
       <p>You may download or print information and documents that we provide to you strictly for personal use, provided that you keep all copyright and proprietary notices intact. You must not otherwise reproduce or distribute any material without our written consent.</p>
       <p>You acknowledge and agree that all intellectual property rights in the App, the Portal, all documents and related technology anywhere in the world belong to us or our licensors - you have no rights in or to the App, the Portal, the documents and related technology other than the right to use each of them in accordance with these Terms and the Data Terms.</p>
       <p><strong>3.0 Communications</strong></p>
       <p>We may communicate with you at any time about your account using the information you have given us, including, when appropriate, by telephone, SMS text message, by email, via the web portal or App.</p>
       <p>It is your responsibility to ensure the contact information we have for you is up to date. You must ensure we have a current and valid email address for you so that we can notify you when important documents are delivered to you via the web portal or App. You accept that where either we or the Product Provider are required to provide you with written notice then this will be given to you by means of electronic correspondence.</p>
       <p>We cannot guarantee that electronic communications will be successfully delivered, or that they will be secure and virus free. &nbsp;Except for cases of negligence or where we have breached applicable law we will not be liable for any loss, damage, expense, harm or inconvenience caused as a result of an email being lost, delayed, intercepted, corrupted or otherwise altered or for failing to be delivered for any reason beyond our control.</p>
       <p>We will record and monitor telephone conversations that we have with you for training, monitoring and record keeping purposes. &nbsp;We will store recordings for a period required by law, or for as long as we consider necessary to be able to provide the Trust Box Services to you, whilst complying with the General Data Protection Regulations (GDPR). In the event of a disagreement between you and us, we can give you a copy of our records on request. Please note that you cannot 'opt out' of communications being recorded as it is a regulatory requirement.</p>
       <p>All communications in relation to the services provided under this Agreement will be in English.</p>
       <p><strong>4.0 Complaints</strong></p>
       <p>4.1 How to complain</p>
       <p>Should you wish to complain please contact us via one of the below methods:</p>
       <ul>
           <li>In writing: Customer Care Manager, Trust Box, 5 Millbank House, Riverside Business Park, Wilmslow, SK9 1BJ</li>
           <li>By email: complaints@yourtrustbox.co.uk</li>
       </ul>
       <p>4.2 Referrals to the online dispute resolution (odr) platform</p>
       <p>Where your complaint refers to products or services purchased online, you may refer your complaint to the ODR Platform:</p>
       <ul>
           <li>Website:&nbsp;<a href="http://ec.europa.eu/odr">http://ec.europa.eu/odr</a></li>
       </ul>
       <p><strong>5.0 Your information and data</strong></p>
       <p>We will base our recommendations on the information you have provided us with, whether this is via the Trust Box portal, in a conversation with an adviser, via. telephone call or email. &nbsp;You are required to check the information we have recorded to ensure it is complete and accurate and update us whenever there is a change to your personal circumstances or details.</p>
       <p>We may use your personal information, including sensitive personal information and store it on our systems and may otherwise process it for the purposes of providing the Trust Box Account and the Trust Box Services. We will never send you marketing materials and other information regarding any of our products or services without your explicit consent. &nbsp;You can provide or withdraw your consent at any time, either by unsubscribing via an email received, or emailing&nbsp;data@yourtrustbox.co.uk.</p>
       <p>Services listed within Trust Box are delivered by third party partners, but we will never share your personal information to any of these third-party partners or anyone else without your express consent. &nbsp;</p>
       <p>We are registered with the Information Commissioner&rsquo;s Office (ICO) for the handling and processing of personal information and shall always comply with applicable data protection legislation. &nbsp;We will take all reasonable steps to ensure that the database containing this information is constantly updated and is securely protected against unauthorised entry and that personal information is kept strictly confidential.</p>
       <p>We may pass personal information relating to you to third parties appointed by us for the purpose of administration and verifying your identity and you confirm that you accept and consent to this. &nbsp;We may need to seek additional information from you in order to verify your identity.</p>
       <p>We will only disclose your personal information if we are required to do so by law.</p>
       <p>Your personal data may be transferred to third parties outside the EEA as well as within it, solely in connection with our provision of the Trust Box Account and the Trust Box Services. &nbsp;You should be aware that in territories outside the EEA, laws and practices relating to the protection of personal data are likely to be different and, in some cases, may be weaker than those within the EEA. &nbsp;Where transfers outside of the EEA are necessary, Trust Box complies with stringent safeguards, required by law, to protect your personal data. &nbsp;By entering into the Trust Box Agreement, you consent to such processing of your data.</p>
       <p>You are entitled to see all personal data relating to you, which is held on any database controlled by us. Please contact us via&nbsp;data@yourtrustbox.co.uk&nbsp;if you wish to request this information.</p>
       <p>5.1 Referrals</p>
       <p>Where appropriate, we may offer to refer you to third parties to provide certain additional services. We will not make any such referral without your agreement. &nbsp;We may pay (or receive from third parties) fees in relation to referrals of business. &nbsp;In making or receiving any such referral and making or receiving such payments, we will act in accordance with all applicable laws &amp; regulations.</p>
       <p><strong>6.0 Amendments to the Trust Box Agreement</strong></p>
       <p>We may amend the Trust Box Agreement:</p>
       <ul>
           <li>to reflect changes in the costs and expenses that we incur (or reasonably expect to incur) in providing the services to you, including to take account of changes in the rates of inflation, taxes or interest.</li>
           <li>to make these terms fairer to you or easier to understand, or to correct mistakes;</li>
           <li>to reflect changes in market practice or conditions;</li>
           <li>to enable us to make reasonable changes to the way we provide our services as a result of changes in the financial services sector, technology, or available products;</li>
           <li>to reflect changes to our arrangements with the Product Provider or other Service providers; or</li>
           <li>If we propose to make a change to these Terms that is not detrimental to you, we can make the change immediately. We will make information available to you about the change within 30 days of the change.</li>
       </ul>
       <p>If we make a change to these Terms that may be detrimental to you, we will provide you with at least 30 days&rsquo; prior notice (unless we are required to make the change sooner, for example, for legal or regulatory reasons, in which case we will make information available about the change within 5 days of making the relevant change).</p>
       <p>If we make any change that is detrimental to you, you may notify us within 30 days from the date of such change to terminate your Trust Box Agreement and close your Trust Box Account without charge.</p>
       <p><strong>7.0 Withdrawals and termination</strong></p>
       <p>7.1 Cancellation periods</p>
       <p>Each Product or Service Provider Terms &amp; Conditions details your rights to cancel your products once you have taken them out. &nbsp;</p>
       <p>7.2 Commencement and termination</p>
       <p>This Terms of Business Agreement takes effect from the date it is given to you and will apply until terminated. &nbsp;You, or we, may terminate our authority to act on your behalf under this agreement at any time, without penalty. &nbsp;You may terminate your Trust Box Agreement at any time and for any reason by providing us with notice, subject to the settlement of all outstanding transactions. Transactions already in progress will be completed in the normal course of business. &nbsp;We will close your Trust Box Account as soon as practicable after receiving your notice to terminate the Trust Box Agreement.</p>
       <p>We may terminate this Agreement at any time by giving you 30 days prior notice subject to the settlement of all outstanding transactions.</p>
       <p>We may terminate this Agreement immediately, freeze your Trust Box Account and take steps to freeze transactions through your Platform Product(s) without giving you advance notice if we reasonably believe that you:</p>
       <ul>
           <li>have materially breached any of the terms of the Trust Box Agreement or have otherwise provided us with false or misleading information;</li>
           <li>are using, or allowing another person to use your Trust Box Account illegally or for criminal activity;</li>
           <li>have failed to pay any of the amounts owed under the Trust Box Agreement; or</li>
           <li>have become bankrupt, insolvent or are unable to pay your debts as they fall due.</li>
       </ul>
       <p>Where we terminate the Trust Box Agreement immediately or freeze your Trust Box Account as set out above, we will inform you in writing immediately of our decision and our reasons for making that decision.</p>
       <p><strong>8.0 Governing law</strong></p>
       <p>The Trust Box Agreement and the Product Provider Agreement and any dispute or claim arising out of, or in connection with, either of them (including non-contractual disputes or claims) will be governed by and construed in accordance with the law of England and Wales.</p>
       <p>The parties irrevocably agree that the courts of England and Wales will have exclusive jurisdiction to settle any dispute or claim that arises out of, or in connection with, either the Trust Box Agreement or the Product Provider Agreement (including non-contractual disputes or claims).</p>
       <p><strong>9.0 Third parties</strong></p>
       <p>Except for any relevant Service Provider who may enforce, as applicable, provisions herein for a person who is not a party to this agreement cannot enforce or enjoy the benefit of any term of this Agreement under the Contracts (Rights of Third Parties) Act 1999.</p>
       <p><strong>10.0 Summary of conflicts management</strong></p>
       <p>10.1 Introduction</p>
       <p>Trust Box is committed to maintaining the highest professional standards and, therefore, we endeavour to identify, consider and manage potential conflicts of interest to ensure that we treat all of our customers fairly and in accordance with relevant regulation and best practice guidelines.</p>
       <p>10.2 Nature of a conflict of interest</p>
       <p>In essence, a conflict of interest is a situation in which Trust Box (or any of its personnel) finds itself in a position where its own interests conflict with the duties and obligations owed to its clients or, a situation in which Trust Box&rsquo;s duty to one client conflicts with its duty to another.</p>
       <p>10.3 Identifying conflicts of interest</p>
       <p>For the purposes of identifying the types of conflict and potential conflicts that arise which may entail a material risk of damage to the interests of a client, we must take into account whether Trust Box or its directors, employees or contractors (&ldquo;Trust Box Personnel&ldquo;) or any other person linked by control to Trust Box:</p>
       <ul>
           <li>is likely to make a financial gain, or avoid a financial loss, at the expense of a client;</li>
           <li>has an interest in the outcome of a service provided to a client or transaction carried out on behalf of a client, which is distinct from the client&rsquo;s interest in that outcome;</li>
           <li>has an incentive to favour the interest of another client or group of clients over the interests of the client;</li>
           <li>carries on the same business as a client; or</li>
           <li>receives or will receive from a person, other than a client, an inducement in relation to a service provided to the client, in the form of remuneration, goods or services.</li>
       </ul>
       <p>10.4 Conflict situations</p>
       <p>We have identified a number of situations which may give rise to a potential conflict of interest. These situations include, but are not limited to, the following:</p>
       <ul>
           <li>we receive gifts, entertainment or other monetary and non-monetary benefits from our service providers or business partners which could give rise to a conflict with respect to the duties that we owe to our clients.</li>
           <li>Trust Box Personnel, who have outside commitments (e.g. directorships or other outside business interests), may be influenced to act in a manner that conflicts with the interests of Trust Box or its clients.</li>
       </ul>
       <p>10.5 Conflict avoidance</p>
       <p>We seek to organise our business in such a way as to avoid conflicts of interest arising.</p>
       <p>10.6 Conflict management</p>
       <p>For conflicts of interest which are unavoidable, we have put in place procedures which are designed to ensure that the management of any conflict takes place in such a way that Trust Box (and its personnel) are not advantaged and that no client is disadvantaged. Trust Box&rsquo;s Board of Directors are responsible for ensuring that such procedures are appropriate and that employees act accordingly.&nbsp; Specifically, we have put in place the following procedures to assist in our identification and management of conflicts of interest:</p>
       <ul>
           <li>Conflicts Log: we maintain a list of all potential conflicts of interest identified. With respect to each conflict, the log details the measure put in place to monitor and manage the conflict of interest;</li>
           <li>Gifts &amp; Entertainment Policy: we ensure that all Trust Box Personnel are subject to appropriate restrictions and monetary limits for any gifts or entertainment received;</li>
           <li>Personal Account Dealing Policy: we ensure that all Trust Box Personnel pre-clear and report any personal trading activity which may conflict with the interests of our clients;</li>
           <li>Pre-approval process for all Outside Business Interests: we ensure that Trust Box Personnel&rsquo;s external commitments do not conflict with the interests of Trust Box or its clients;</li>
           <li>Protection of information: we maintain appropriate safeguards to protect sensitive or confidential information which could give rise to conflicts of interest. These measures are designed to prevent unauthorised access, inappropriate use, or inappropriate dissemination of such information. Details of these arrangements are described in our Privacy Policy; or</li>
           <li>Separation of functions: where our internal functions might give rise to conflicts of interest, we put in place arrangements to separate such functions and ensure that separate management and reporting lines are established.</li>
       </ul>
       <p>10.7 Conflicts disclosure</p>
       <p>If we are not reasonably confident that we can manage a particular conflict to adequately protect the interest of the client, we will disclose the conflict of interest before undertaking any business, so the client has an opportunity to decide whether or not to continue.</p>
       <p>&nbsp;</p>
   </div>
@endsection
