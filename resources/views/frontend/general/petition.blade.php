@extends("layouts.empty")
@section("content")
    <div class="container py-5">
        <h1 class="h1 text-partner">Reducing the costs & prioritising the most vulnerable</h1>
        <h2 class="h2 text-partner mb-3">Our petition to the Government and the Office of the Public Guardian </h2>

        <h2 class="h2 text-partner">What do we want the government to do:</h2>
        <p>
            For those considered to be at <strong>“risk of severe illness”</strong> and have been advised by the NHS to <strong>“stay at home at all times”</strong>
            for at least 12 weeks during the Covid-19 crisis, we want the Office of the Public Guardian to waive the fees charged for the registration
            of Lasting Powers of Attorney (LPAs) and to prioritise their applications.
        </p>
        <br>
        <h2 class="h2 text-partner">Why are we asking?</h2>
        <p>
            Whilst Trust Box will create Ordinary Powers of Attorney at no cost for those in need, we believe everyone should also consider putting Lasting Powers of Attorney (LPAs) in place. LPAs are the only way to guarantee that a family member, or trusted person, can take over our financial affairs and make decisions about our health & welfare if we were unable to do so ourselves due to a loss of capacity.  <br><br>
            LPAs need to be registered with the Office of the Public Guardian (OPG) to be valid and they charge a fee of £82 per LPA. As most of us will need two, total fees would be £164 per person.
            This fee can be a barrier for many who would benefit greatly from having Lasting Powers of Attorney in place. In addition, a typical timeframe for processing the registration is, in our experience, approximately three months. <br><br>
            We want the OPG to waive the fees and fast track the registration of LPAs for those that have been contacted by the Government / NHS stating that they are at increased risk from Covid-19 and are therefore
            subject to stricter isolation guidelines, expected to be in ‘lockdown’ for longer and in the worst circumstances, more likely to suffer a loss of capacity if they were to contract the virus.
        </p>
        <br>
        <h2 class="h2 text-partner">Background on why Lasting Powers of Attorney are vital</h2>
        <p>
            If LPAs are not in place, and someone is deemed to be vulnerable because they have either temporarily or permanently lost capacity, all of their finances (including joint bank accounts) can be frozen and a Deputy is appointed by the courts
            to look after their affairs. This is designed to protect the vulnerable, but it incurs fees and adds a great deal of stress to the individual concerned, their spouse or partner, their
            family and social services. Furthermore, if LPAs are not in place, family members do not have the right to decide on the type of healthcare an individual receives.<br><br>
            If a person loses capacity without LPAs in place, a family member can apply to the Court of Protection for a Deputyship Order, but this is a long and drawn out process that will take many months and will often cost the person applying in excess of £2,000.
            Furthermore, there is no guarantee that an application would be successful.<br><br>
            The best solution is to have LPAs in place as early as possible so that an individual has peace of mind knowing that a family member, or trusted person, will be allowed to look after their affairs and make decisions about their healthcare.<br><br>
            <strong>Thank You!</strong>
            <br><br>

            <a href="https://petition.parliament.uk/petitions/319026/" class="btn btn-dark text-white mt-3">Sign Here</a>
        </p>
    </div>
@endsection
