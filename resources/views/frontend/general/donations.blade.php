@extends("layouts.empty")
@section("content")
    <div class="container py-5">
        <div class="row">
            <div class="col">
                <h1 class="h1 text-partner mb-5"> Please help us to help them.</h1>
                <p class="text-partner">
                    We are happy to provide our professional services and make our resources available for free during the current crisis and we hope to help thousands of people and families.<br><br>
                    Those for whom the normal costs of our services may not have been an issue, please consider visiting our donation pages and making a gift to one of our chosen charities; we and they would really appreciate it.<br><br>
                </p>
            </div>
            <div class="col">
                <h3 class="h3 mb-3 font-weight-bold text-dark">NHS</h3>
                <p>No explanation required as to why we want to support the amazing, brave and dedicated people of our NHS. They are the heart of our nation and are there when we need them. Let's all be there for them. </p>
                <a href="https://uk.virginmoneygiving.com/Team/TrustBoxNHSFundraising" class="btn btn-dark text-white btn-sm p-1 float-right">Donate</a>
                <br><br><br>
                <h3 class="h3 mb-3 font-weight-bold text-dark">Refuge against domestic violence</h3>
                <p>It's hard to imagine the increased horrors during lockdown of living in a home where violence and abuse is an everyday ordeal. Recent reports say calls to the amazing people at Refuge are up 25%. They need our help!  </p>
                <a href="https://uk.virginmoneygiving.com/Team/TrustBoxRefugeAgainstDomesticViolence" class="btn btn-dark text-white btn-sm p-1 float-right">Donate</a>
                <br><br><br>
                <h3 class="h3 mb-3 font-weight-bold text-dark">Shelter</h3>
                <p>How do you stay safe by staying at home if you don’t have one? Thankfully, the fantastically dedicated people at Shelter are there to support our homeless population. They urgently need to invest in technology so their support lines can keep pace with an increasing demand.  </p>
                <a href="https://uk.virginmoneygiving.com/Team/TrustBoxShelterFundraising" class="btn btn-dark text-white btn-sm p-1 float-right">Donate</a>
                <br><br><br>
            </div>
        </div>
    </div>
@endsection
