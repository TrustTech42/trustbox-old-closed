@extends('layouts.1col')
@section('content')
    <div class="container py-3">
        <h1 class="h1 text-partner text-center my-5">Privacy Policy</h1>

        <p><strong>Privacy Policy</strong></p>
        <p>TrustBox Ltd is registered in England &amp; Wales</p>
        <p>This policy sets out how Trust Box uses and protects your data.&nbsp; Trust Box is committed to ensuring that your privacy is protected and so will only ever request data that is necessary to meet our legal obligations or to fulfil our contract to you. &nbsp;We are registered with the Information Commissioners Officer (ICO) as a Data Controller and are compliant with the requirements of the GDPR and the Data Protection Act 2018.</p>
        <p>Trust Box reserves the right to change this policy at any time. &nbsp;We will contact you to let you know of any material amendments to this policy, however, you should check this page periodically to ensure that you are aware of and accepting of any incremental changes. This policy is effective from 6 May 5th April 2020.</p>
        <p><strong>The information we gather</strong></p>
        <p>We will gather any information which is required to enable us to provide you with the service you have requested. &nbsp;We may also require additional information to meet our legal obligations. &nbsp;Below is a non-exhaustive list of information we may gather to meet these requirements:</p>
        <ul>
            <li>Basic personal details, such as name, date of birth and address;</li>
            <li>Contact information, such as email address and phone number;</li>
            <li>Details to enable us to provide tax-beneficial products, such as National Insurance number;</li>
            <li>Details on your marketing preferences; and</li>
            <li>Device information if you are using the Trust Box Portal or App, and other information relevant to customer surveys and offers.</li>
        </ul>
        <p>We may be required to verify our customers identity and residency information. &nbsp;We will strive to do this electronically, however this is not possible in all cases and so we may require either original or certified copies of documents as evidence.</p>
        <p><strong>What We Do with This Information</strong></p>
        <p>We require this information to fulfil our legal obligations and our contract to you, allowing us to provide you with advice, where necessary.</p>
        <p>If you have consented to marketing communications:</p>
        <ul>
            <li>To let you know about any new products, services, offers and other relevant updates;</li>
            <li>To contact you for market research purposes;</li>
            <li>To let you know about updates in the market, new blogs and updates from our Executive.</li>
        </ul>
        <p>We will never share information which is not necessary for the transaction of your contract, or to fulfil our legal obligations. &nbsp;We have legal contracts in place with any company we share data with, so we know exactly how they store and use this information. &nbsp;We will never sell your data.</p>
        <p><strong>Security</strong></p>
        <p>Trust Box is committed to ensuring that your data is secure.&nbsp;&nbsp; In order to ensure no unauthorised access or disclosure, we have implemented a range of physical, electronic, and human measures to secure the information we gather.&nbsp;</p>
        <p><strong>Removing your Data</strong></p>
        <p>GDPR introduced the &lsquo;right to erasure&rsquo;, which means you are able to ask companies to delete your data. &nbsp;To do so please send an email to
            <a href="mailto:data@yourtrustbox.co.uk">data@yourtrustbox.co.uk</a>.</p>
        <p><strong>Recommendations</strong></p>
        <p>While Trust Box takes all possible measures in order to secure your account, we recommend you follow best practice to help keep things safe:</p>
        <ul>
            <li>Password:</li>
        </ul>
        <p>Never share your passwords, passcodes or any other login details with anyone else. &nbsp;We also recommend that you don&rsquo;t store passwords, passcodes or any other login details in a device. &nbsp;In the event that you lose your device, or if you believe someone else may know your login details, please inform Trust Box immediately so we can stop the service on that device.&nbsp; Get in touch with us as soon as possible if you feel someone may know your login details or if you lose your device. &nbsp;If you haven&rsquo;t already, we also recommend that you setup a passcode or fingerprint recognition to access your device where possible (details of how to do this should be found in your device instructions).</p>
        <ul>
            <li><strong>Your device</strong></li>
        </ul>
        <p>We recommend not leaving your device unattended while using the App, or unlocked after using the APP. Think carefully about when and where you access the App; try not to do so when people are able to look over your shoulder and see your screen. The App is designed for use on standard, unmodified devices; use of the App on a jailbroken or rooted device is entirely at the user&rsquo;s own risk. Trust Box will always aim for the App to be updated in line with each major device operating system release; we recommend that you keep your device up-to-date with the latest version of the operating system available to you, together with any security patches as and when they are released.</p>
        <p><strong>Trust Box and Cookies</strong></p>
        <p>You may be given the option to accept cookies when you use the Trust Box website.</p>
        <p>A cookie is a small text file that is downloaded to your device when you access a website, allowing that website to recognise your device and to store information about your preferences and actions. Trust Box uses cookies to tell which pages of our website you access, helping us to improve our website and services.</p>
        <p>Cookies let us monitor which pages you use and how you use them. Consequently, the data we collect feeds into our development ensuring our website remains as relevant as possible to our users. Cookies do not give us access to your device, nor do they give us access to any data other than that which you have expressly chosen to share with us elsewhere in the App or website.</p>
        <p>You do not have to accept cookies, though failure to do so may stop you from getting the best experience from the Trust Box website.</p>
        <p>To find out more about the way cookies work, how to see what cookies have been set and how to manage and delete them, visit
            <a href="www.allaboutcookies.org">www.allaboutcookies.org</a>.</p>
        <p><strong>Third Party Links</strong></p>
        <p>The Trust Box website may contain links to websites that we think you may find relevant or interesting. Upon using any third-party links, you may leave the Trust Box website, at which point we cease to have any control over the destination website.&nbsp; As such, though we will always strive to ensure that the links are useful, we can in no way be responsible for the protection and privacy of any data you provide when visiting external sites. &nbsp;It is important to note that this privacy statement covers Trust Box websites only, and that any external sites are not covered or governed by this statement. When visiting any website, you should always exercise caution and routinely consider the privacy policy of that website.</p>
        <p><strong>Your Personal Information</strong></p>
        <p>Your personal information belongs to you, and you can opt to restrict its collection or use. If you are filling in forms on the website, you must give your explicit consent for us to send you direct marketing. &nbsp;If at any point you wish to change your preference for whether your information can be used for direct marketing, please contact us via the contact us form within Trust Box. Alternatively, all our marketing communications contain the option to unsubscribe.</p>
        <p>Should you believe any of the information we hold about you to be incorrect, please either make the necessary amendments via the App or Portal; if this is not possible, please contact us via the contact us form.</p>
        <p>Unless we either have your express permission, or are required by law so to do, Trust Box will not sell, distribute or lease your personal information to third parties.</p>
        <p>Under the terms of the GDPR, you can request details of your personal information as held by us. &nbsp;To make such a request, please contact us via the contact us form on the website or send an email to
            <a href="mailto:data@yourtrustbox.co.uk">data@yourtrustbox.co.uk</a>.</p>
    </div>
@endsection
