@extends('layouts.1col')
@section('content')
    <div class="container text-center py-5">
        <h1 class="h1 partner-title">
            Contact Us
        </h1>

        <p>
            We would love to hear from you. Kindly get in touch with us if you have any queries and we will get back to you <br>
            as soon as possible.
        </p>
    </div>

    <div class="container mb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col bg-light p-5 contact-form">
                            {!! Form::open(['route' => 'contact.store', 'class' => 'needs-validation', 'novalidate']) !!}
                            <input type="hidden" name="partner_id" value="{{$global_partner->id}}">

                            <h3 class="h3 mb-5 text-center">Contact Us</h3>

                            <div class="row">
                                <div class="col-md-6">{!! Form::bsText('name', null, ['required']) !!}</div>
                                <div class="col-md-6">{!! Form::bsEmail('email', null, ['required']) !!}</div>
                            </div>

                            <label for="subject">Reason for Contact</label>
                            {!! Form::select('reason_id', $reasons, null, ['class' => 'custom-select']) !!}

                            <div class="form-group position-relative mt-3">
                                <label for="message" class="form-label">Message</label>
                                <textarea name="message" id="message" cols="30" rows="5" class="form-control" required></textarea>

                                <div class="invalid-tooltip">
                                    This is a required field
                                </div>
                            </div>

                            <button class="btn btn-primary float-right">Send Message</button>

                            {!! Form::close() !!}
                        </div>
{{--                        <div class="col-md-4 bg-partner text-white contact-right">--}}
{{--                            <h3 class="h3">Other Ways to Contact Us</h3>--}}

{{--                            <ul class="list-group list-group-flush mb-5">--}}
{{--                                <li class="list-group-item">--}}
{{--                                    <a href="#">--}}
{{--                                        <i class="fa fas fa-phone mr-2"></i> 0333 999 7889--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li class="list-group-item">--}}
{{--                                    <a href="#">--}}
{{--                                        <i class="fa fas fa-envelope mr-2"></i> info@lawcard.co.uk--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}

{{--                            <h3 class="h3">Find Us Here</h3>--}}

{{--                            <ul class="list-unstyled">--}}
{{--                                <li>{{ $global_partner->address1 }}</li>--}}
{{--                                <li>{{ $global_partner->address2 }}</li>--}}
{{--                                <li>{{ $global_partner->city }}</li>--}}
{{--                                <li>{{ $global_partner->postcode }}</li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
