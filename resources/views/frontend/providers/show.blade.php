@extends('layouts.dashboard')
@section('top')
    <h1 class="h1 partner-title">My Account (Provider)</h1>
@endsection
@section('sidebar')
    <a class="nav-link active" id="members_tab" data-toggle="pill" href="#members" role="tab">
        <i class="fa fa-user"></i> Active Members
    </a>
@endsection
@section('content')
    <div class="tab-content">
        @include('frontend.providers.tabs.members')
    </div>
@endsection