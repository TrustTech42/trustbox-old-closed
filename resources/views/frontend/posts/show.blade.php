@extends('layouts.1col')
@section('content')
    <div class="container-fluid p-0">
        <div class="card border-0 full-image background-fixed">
            <div class="card-body blog-banner" style="background: url('/{{$post->banner}}')">
                <h1 class="h1 heading">{{$post->title}}</h1>
                <h5 class="h5">Date Posted: {{date('d/m/Y', strtotime($post->created_at))}}</h5>
            </div>
        </div>
    </div>

    <div class="container p-3 text-center">
        <div class="row">
            <div class="col-8 offset-2 py-3">
                {!! $post->content !!}
            </div>
        </div>
        @if(!$post->benefits->isEmpty())
            <div class="row">
                <div class="col-6 offset-6 py-3">
                    <div class="col-md">
                        <p>Go and enroll for this new benefit in TrustBox using this link</p>
                    </div>
                    <div class="col-md">
                        @foreach($post->benefits as $benefit)
                            <a href="/benefits/{{$benefit->id}}" class="btn btn-primary" type="submit" id="button-addon1">
                                {{$benefit->name}}
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        @include('frontend.partials.html.questions')
    </div>
@endsection
