@extends('layouts.1col')
@section('content')
    <div class="container-fluid p-0">
        <div class="card border-0 full-image background-fixed">
            <div class="card-body bg-secondary-dark">
                <h1 class="h1 heading mb-5">Blog</h1>

                {!! Form::open(['route' => 'posts.search']) !!}
                <div class="row">
                    <div class="col-4 offset-4">
                        <div class="input-group input-group-lg d-none d-md-flex">
                            <input type="text" class="form-control" name="q" placeholder="What are you looking for?">
                            @if(isset($term))
                                <div class="input-group-append">
                                    <a href="/posts" class="input-group-text bg-danger border-0 text-white"
                                       id="inputGroup-sizing-lg">
                                        CLEAR
                                    </a>
                                </div>
                            @endif
                            <div class="input-group-append rounded-right">
                                <button class="input-group-text bg-partner text-white" id="inputGroup-sizing-lg">
                                    SEARCH
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="container p-3">
        <div class="row">
            <div class="col-md-3">
                <div class="card bg-partner rounded mb-3 mb-md-0">
                    <div class="card-body">
                        <h2 class="h2 text-white text-center mb-5">Categories</h2>
                        <button class="btn btn-outline-light btn-block">ALL CATEGORIES</button>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="row">
                    @foreach($globalPosts as $globalPost)
                        <div class="col-md-6 mb-5">
                            <div class="card h-100 rounded bg-light p-3">
                                <div class="card-body">
                                    <h2 class="h2 partner-title">{{$globalPost->title}}</h2>
                                    <p>{!! Str::words($globalPost->content, 50, '...')!!}</p>

                                    <a href="/posts/{{$globalPost->id}}" class="btn btn-primary">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    @foreach($global_partner->posts->where('enabled', 1) as $post)
                        @if($post->is_guest == 1)
                            <div class="col-md-6 mb-5">
                                <div class="card h-100 rounded bg-light p-3">
                                    <div class="card-body">
                                        <h2 class="h2 partner-title">{{$post->title}}</h2>
                                        <p>{!! Str::words($post->content, 50, '...')!!}</p>

                                        <a href="/posts/{{$post->id}}" class="btn btn-primary">READ MORE</a>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if($post->is_guest == 0)
                            @auth
                                <div class="col-md-6 mb-5">
                                    <div class="card h-100 rounded bg-light p-3">
                                        <div class="card-body">
                                            <h2 class="h2 partner-title">{{$post->title}}</h2>
                                            <p>{!! Str::words($post->content, 50, '...')!!}</p>

                                            <a href="/posts/{{$post->id}}" class="btn btn-primary">READ MORE</a>
                                        </div>
                                    </div>
                                </div>
                            @endauth
                        @endif
                    @endforeach
                </div>
            </div>

        </div>
    </div>
@endsection
