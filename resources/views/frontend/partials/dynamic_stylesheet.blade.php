@if($global_partner)
    <style>
        .btn-primary,
        .bg-partner,
        .bg-member,
        .nav-pills .nav-link.active,
        .custom-control-input:checked ~ .custom-control-label::before,
        .badge-primary,
        .lds-ellipsis div,
        ol li::before,
        ol li::after,
        .js-cookie-consent button,
        .steps .nav-item a:before{
            background-color : {{$global_partner->primary_colour}}   !important;
            border-color     : {{$global_partner->primary_colour}}   !important;
        }
        a,
        li,
        .h1.partner-title,
        .h2.partner-title,
        .h3.partner-title,
        .p.partner-title,
        .fas.partner-icon,
        .text-partner,
        .text-member,
        .btn-link,
        .circular-chart.primary .circle {
            color  : {{$global_partner->primary_colour}};
            stroke : {{$global_partner->primary_colour}}
        }

        .member-title,
        .benefit-content h1,
        .benefit-content h2,
        .benefit-content h3,
        .benefit-content h4,
        .benefit-content h5,
        .benefit-content h6{
            color  : {{$global_partner->primary_colour}} !important;
        }

        .alert-success {
            color : white;
        }

        .border-partner{
            border-color: {{$global_partner->primary_colour}} !important;
        }

        .svg-fill{
            fill: {{$global_partner->primary_colour}} !important;
        }

        .btn-dark,
        .bg-secondary-dark,
        .js-cookie-consent{
            background-color: {{$global_partner->secondary_colour}} !important;
        }

        .benefit-banner,
        .blog-banner{
            background-color {{$global_partner->secondary_colour}};
        }
    </style>
@endif
