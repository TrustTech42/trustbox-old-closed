<div class="container py-4">
    <div class="row">
        <div class="col block-center">
            <div class="a-center bg-light p-4 rounded">
                <h1 class="h2 partner-title {{!isset($subtitle1) ? 'mb-5' : null}}">{{$title1}}</h1>
                @isset($subtitle1)<h2 class="h3 mb-5">{!! $subtitle1 !!}</h2>@endisset
                <a href="{{$link1 ?? ''}}" class="btn btn-primary">Click Here</a>
            </div>
        </div>
        <div class="col block-center">
            <div class="a-center bg-partner text-white p-4 rounded">
                <h1 class="h2 {{!isset($subtitle2) ? 'mb-5' : null}}">{{$title2}}</h1>
                @isset($subtitle2)<h2 class="h3 mb-5">{!! $subtitle2 !!}</h2>@endisset
                <a href="{{$link2 ?? ''}}" class="btn btn-primary bg-white text-partner">Click Here</a>
            </div>
        </div>
    </div>
</div>
