<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
</head>
<body>
<div class="petition-block bg-partner text-white row {{isset($center) ? 'text-center px-5  py-4' : null}} ">
    <div class="{{isset($mobile) ? 'col-12 col-xl-6 pr-xl-5' : 'col-xl-6 pr-xl-5'}} border-xl-right">
        Please take the time to review and consider signing our petition to the Government and the Office of the Public
        Guardian.
        <a href="/petition" class="btn btn-dark btn-block mt-3">Petition</a>
    </div>
    <div class="{{isset($mobile) ? 'col-12 pl-0 col-xl-6 pl-xl-5 pt-5 pt-xl-0' : 'col-xl-6 pl-xl-5 pt-5 pt-xl-0'}}  ">
        Whilst we will provide our services at no cost during the Coronavirus crisis, please consider donating to our
        nominated charities.
        <a href="/donations" class="btn btn-dark btn-block mt-3">Our Nominated Charities</a>
    </div>
</div>
</body>
</html>
