<div class="jumbotron jumbotron-fluid block block-description bg-partner text-white mb-0">
    <div class="container px-4 py-md-5">
        <div class="row">
            <div class="col-sm-9 col-md-6 mb-3 mb-md-0  d-flex align-items-center flex-wrap">
                <div class="d-flex align-items-center flex-wrap w-100">
                    <div class="content">
                        <h1>Welcome to the {{$global_partner->name}} Trust Box</h1>
                        <br>
                    </div>

{{--                    <div class="d-none d-md-block w-100">--}}
{{--                        @include('frontend.partials.html.petition')--}}
{{--                    </div>--}}
                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-3 offset-xl-3">
                @guest
                    <div class="bg-light d-flex align-items-center flex-wrap p-md-5 py-5 px-4 rounded justify-content-center home-login-form">
                        <p class="text-center w-100 float-left text-partner mb-5">
                            Register or log in to your <strong>Trust Box</strong> account below to redeem a variety of different benefits.
                        </p>
                        {!! Form::open(['route' => 'login', 'method' => 'POST', 'novalidate', 'class' => 'needs-validation w-100']) !!}

                        <div class="inner w-100 px-md-2">
                            {!! Form::bsEmail('email', old('email'), ['required', 'autofocus', 'placeholder' => 'Email Address', 'class' => 'form-control text-partner', 'no-label' => true]) !!}
                            {!! Form::bsPassword('password', null, null, ['required', 'placeholder' => 'Password', 'class' => 'form-control mb-0', 'no-label' => true, 'no-margin' => true]) !!}
                            <small class="w-100">
                                <a href="/password/reset" class="btn btn-link w-100 text-right"
                                   style="font-size: 12px;">Forgotten Password?</a>
                            </small>
                            <button class="btn btn-primary float-right w-100 mt-3">LOG IN</button>
                            <a href="/register" class="btn btn-link w-100 text-center mb-5">Register</a>
                        </div>

                        {!! Form::close() !!}
                        <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1912.55 536.57" class="logo svg-fill d-inline" style="width:150px;"><defs><style>.cls-1{fill:#21bbba;}.cls-2{isolation:isolate;}</style></defs><title>Trustbox logo new</title><path class="cls-1" d="M395.17,0V536.57H908.91V0ZM652.31,470.7H584.84V449.08A72.57,72.57,0,0,1,558,467.72a87.69,87.69,0,0,1-33.18,6.33q-39.14,0-62.43-23.11t-23.3-69.33V268.67h70.83V370.06q0,22.74,8.57,33.17t25,10.44q16.78,0,27.4-11.74t10.62-36V268.67h70.83Zm207.24-28.89q-11.56,15.1-34.11,23.67t-54.23,8.57a217.83,217.83,0,0,1-50-5.77q-24.6-5.77-39.51-15.1L703,404.72q13.8,8.58,32.81,13.61a146.81,146.81,0,0,0,37.64,5q17.14,0,24.42-3.54t7.27-10.25q0-6.7-8.76-9.51t-27.77-5.4q-24.24-3-41.19-7.83a63.52,63.52,0,0,1-29.26-17.89q-12.3-13.05-12.3-36.53a55.25,55.25,0,0,1,11.55-34.48q11.55-15.09,33.74-23.86t53.11-8.75a215,215,0,0,1,43.62,4.47q21.6,4.47,36.15,12.67l-21.24,48.09a117.82,117.82,0,0,0-58.15-14.91q-16.78,0-24.61,3.91T752.2,329.8q0,7.08,8.57,9.88t28.33,5.78q25,3.72,41.37,8.57A63.19,63.19,0,0,1,859,371.74q12.1,12.86,12.11,36A54.67,54.67,0,0,1,859.55,441.81Z" transform="translate(-1.49)"/><g class="cls-2"><path class="cls-1" d="M81.63,268.3H1.49V209.78H235.21V268.3H155.44V470.7H81.63Z" transform="translate(-1.49)"/><path class="cls-1" d="M351,272.21q17.32-6.88,39.7-6.89v63.74a144,144,0,0,0-16-1.12q-22.74,0-35.6,12.3t-12.86,37.65V470.7H255.34v-202H322.8V292.9A66,66,0,0,1,351,272.21Z" transform="translate(-1.49)"/></g><g class="cls-2"><path class="cls-1" d="M1064.94,462.5a71.21,71.21,0,0,1-21.81,8.57,120.93,120.93,0,0,1-27.4,3q-39.51,0-60.75-19.38T933.73,396.9V329.06H904.28V276.13h29.45V223.57h70.82v52.56h46.22v52.93h-46.22v67.09q0,10.82,5.78,17t15.47,6.15a37.5,37.5,0,0,0,21.62-6.34Z" transform="translate(-1.49)"/><path class="cls-1" d="M1439.54,359.06q12.68,16.59,12.67,40.07,0,34.31-27.58,52.93t-79.77,18.64H1204V209.78h133.45q49.93,0,76,18.45T1439.54,278a61.94,61.94,0,0,1-9.13,33.36q-9.14,14.73-25.91,23.3Q1426.87,342.48,1439.54,359.06Zm-162.52-96V313.4h50.7q37.26,0,37.27-25.35,0-25-37.27-25Zm100.64,127.85q0-26.46-38.76-26.46H1277V417.4h61.88Q1377.66,417.4,1377.66,390.93Z" transform="translate(-1.49)"/></g><g class="cls-2"><path class="cls-1" d="M1510.75,460.64q-26.09-13.42-40.81-37.28t-14.73-54.05q0-29.82,14.73-53.67a100.19,100.19,0,0,1,40.63-37.09q25.9-13.23,58.71-13.23t58.89,13.23q26.08,13.23,40.63,36.9t14.54,53.86q0,30.19-14.54,54.05t-40.63,37.28q-26.1,13.41-58.89,13.41Q1536.85,474.05,1510.75,460.64Zm89.09-55.73q11.92-12.86,11.93-35.6,0-22.37-11.93-35t-30.56-12.67q-18.65,0-30.57,12.67t-11.93,35q0,22.74,11.93,35.6t30.57,12.86Q1587.91,417.77,1599.84,404.91Z" transform="translate(-1.49)"/><path class="cls-1" d="M1833.16,470.7,1797,417.77l-38.76,52.93h-76.79l76.79-101-74.55-101h79.76l35.79,51.07,37.27-51.07h74.93l-74.55,98.41,77.16,103.62Z" transform="translate(-1.49)"/></g></svg>
                    </div>
                @endguest
            </div>
        </div>
    </div>
</div>
