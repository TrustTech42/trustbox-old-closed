@extends('layouts.1col')
@section('content')
    <div class="container text-center py-5">
        <h1 class="h1 partner-title">
            {{$global_partner->name}} Services
        </h1>

        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        </p>
    </div>

    <div class="container mb-5">
        <div class="row">
            @foreach($global_partner->services as $service)
                <div class="col-4 mb-4">
                    <div class="card full-image rounded">
                        <div class="card-body" style="background: linear-gradient(rgba(0, 0, 0, 0.3), #1c1d1e), url({{$service->thumbnail}}">
                            <h2 class="h2 text-white">{{$service->name}}</h2>

                            <a href="/services/{{$service->id}}" class="btn btn-primary">FIND OUT MORE</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <div class="container-fluid d-flex align-items-center justify-content-center bg-partner text-white p-5">
        <div class="container text-center">
            <h1 class="w-100">Not a {{$global_partner->name}} Partner?</h1>

            <div class="row">
                <div class="col-6 offset-3">
                    Don't worry, if you instruct an approved service partner, you will receive a {{$global_partner->name}} Full Partnership for free, a saving of £295. Explore the {{$global_partner->name}} benefits
                    <a href="/benefits" class="text-white">here.</a>
                </div>
            </div>
        </div>
    </div>
@endsection
