@extends('layouts.1col')
@section('content')
    <div class="container-fluid p-0">
        <div class="card border-0 full-image background-fixed">
            <div class="card-body" style="background-image: url('/{{$service->banner}}')">
                <h1 class="h1 heading">{{$service->name}}</h1>
            </div>
        </div>
    </div>


    <div class="container py-5">
        <div class="row">
            <div class="col-7">
                {!! $service->description !!}

                <div class="block-center bg-dark rounded p-4 text-white mt-5">
                    <div class="a-center">
                        <h4 class="h4 mb-0">To redeem this service - click here
                            <a href="#" class="btn btn-primary ml-5">CLICK HERE</a>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="col-5">
                <div class="d-block bg-partner text-white rounded px-5 py-3">
                    <h2 class="h2 text-center mb-4">Boosted for Plus
                        <i class="fa fa-bars"></i>
                        Partners
                    </h2>
                    {!! $service->boosted_description !!}

                    <h3 class="h3 text-center">
                        Join now for £11 per month
                        <a href="#" class="btn btn-primary bg-white text-partner ml-3">CLICK HERE</a>
                    </h3>
                </div>
            </div>
        </div>
    </div>

    <div class="container mb-5">
        @include('frontend.partials.html.questions')
    </div>
@endsection
