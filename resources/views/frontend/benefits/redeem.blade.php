@extends('layouts.1col')
@section('content')
    <div class="container text-center my-5">
        <h1 class="text-center partner-title mb-5">
            Redeem {{$benefit->name}}
        </h1>

        <div class="row">
            <div class="col-md-6 offset-md-3">
                {!! $benefit->redeem_text !!}
            </div>
        </div>
        @auth
            @if($benefit->includes_benefit_form & !$benefit->includes_benefit_phone_button)
                <div class="row mb-5" style="padding-top: 50px">
                    <div class="col-xl-6 offset-xl-3">
                        <div class="form bg-light p-3 px-5 border rounded">
                            <h2 class="h2 partner-title text-md-left">Benefit Contact Form</h2>
                            <br>
                            <form action="{{route('benefit.form', $benefit->id)}}" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <input type="hidden" name="user_id" value="{{$user->id}}">
                                        <div class="form-group">
                                            <label class="benefit-contact-form" for="customer_number">Customer
                                                Number</label>
                                            <input type="text" name="customer_number" value="{{$user->customer_number}}"
                                                   class="form-control" readonly="readonly">
                                        </div>
                                        <div class="form-group">
                                            <label class="benefit-contact-form" for="first_name">First Name</label>
                                            <input type="text" name="first_name" value="{{$user->first_name}}"
                                                   class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label class="benefit-contact-form float-left"
                                                   for="telephone">Telephone</label>
                                            <input type="text" name="telephone" value="{{$user->telephone}}"
                                                   class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label class="benefit-contact-form" for="benefit_name">Benefit Name</label>
                                            <input type="text" name="benefit_name" value="{{$benefit->name}}"
                                                   class="form-control" readonly="readonly">
                                        </div>
                                        <div class="form-group">
                                            <label class="benefit-contact-form" for="first_name">Last Name</label>
                                            <input type="text" name="last_name" value="{{$user->last_name}}"
                                                   class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label class="benefit-contact-form" for="email">Email</label>
                                            <input type="text" name="email" value="{{$user->email}}"
                                                   class="form-control"
                                                   required>
                                        </div>
                                    </div>
                                </div>
                                <label class="benefit-contact-form" for="enquiry">Enquiry</label>
                                <textarea type="text" class="form-control" id="enquiry" name="enquiry" rows="5"
                                          required>I would like to redeem this benefit, please contact me via phone or email with more information. Thanks
                                </textarea>
                                <br>
                                <button type="submit" class="btn btn-primary">Send Form</button>
                                <br>
                                <br>
                            </form>
                        </div>
                    </div>
                </div>
            @endif
            @if($benefit->includes_benefit_phone_button & !$benefit->includes_benefit_form)
                @if($benefit->provider_email)
                    <div class="col-xl-4 offset-xl-4 mb-5 mt-4">
                        <div class="form bg-partner p-3 px-5 border rounded text-white">
                            <h2 class="h2 text-center">Notify Provider</h2>
                            <br>
                            @if($benefit->provider_email)
                                <p class="text-center">
                                    You can also notify {{$benefit->provider->name}} that
                                    you
                                    will be calling to redeem
                                    this
                                    benefit. </p>
                                <p class="text-center">
                                    Please click this button below to send them an email with your
                                    current
                                    account details.
                                </p>
                                <p class="text-center">
                                    Within the notify success page you will also have access to the provider contact number if you wish to contact them directly.
                                </p>
                                <form action="{{route('benefit.notify', $benefit->id)}}" method="POST">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6 mt-4 offset-md-3 text-center">
                                            <button id="notify-provider" class="btn btn-light text-partner"
                                                    type="submit">Notify
                                                Provider
                                            </button>
                                        </div>
                                    </div>
                                    <br>
                                </form>
                            @endif
                        </div>
                        @endif
                        @endif
                        @endauth
                    </div>
    </div>
    <br>
    @auth
        @if($benefit->includes_benefit_form & $benefit->includes_benefit_phone_button)
            <div class="container benefit-contact-container text-center">
                <div class="row">
                    <div class="col-xl-8 mb-3 mb-xl-0">
                        <div class="form bg-light p-3 px-5 border rounded" style="font-size: 16px">
                            <h2 class="h2 partner-title text-center">Benefit Contact Form</h2>
                            <br>
                            <form action="{{route('benefit.form', $benefit->id)}}" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <input type="hidden" name="user_id" value="{{$user->id}}">
                                        <div class="form-group">
                                            <label class="benefit-contact-form" for="customer_number">Customer
                                                Number</label>
                                            <input type="text" name="customer_number"
                                                   value="{{$user->customer_number}}"
                                                   class="form-control" readonly="readonly">
                                        </div>
                                        <div class="form-group">
                                            <label class="benefit-contact-form" for="first_name">First
                                                Name</label>
                                            <input type="text" name="first_name"
                                                   value="{{$user->first_name}}"
                                                   class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label class="benefit-contact-form float-left"
                                                   for="telephone">Telephone</label>
                                            <input type="text" name="telephone"
                                                   value="{{$user->telephone}}"
                                                   class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label class="benefit-contact-form" for="benefit_name">Benefit
                                                Name</label>
                                            <input type="text" name="benefit_name"
                                                   value="{{$benefit->name}}"
                                                   class="form-control" readonly="readonly">
                                        </div>
                                        <div class="form-group">
                                            <label class="benefit-contact-form" for="first_name">Last
                                                Name</label>
                                            <input type="text" name="last_name"
                                                   value="{{$user->last_name}}"
                                                   class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label class="benefit-contact-form"
                                                   for="email">Email</label>
                                            <input type="text" name="email" value="{{$user->email}}"
                                                   class="form-control"
                                                   required>
                                        </div>
                                    </div>
                                </div>
                                <label class="benefit-contact-form" for="enquiry">Enquiry</label>
                                <textarea type="text" class="form-control" id="enquiry" name="enquiry"
                                          rows="5"
                                          required>I would like to redeem this benefit, please contact me via phone or email with more information. Thanks
                                </textarea>
                                <br>
                                <button type="submit" class="btn btn-primary">Send
                                    Form
                                </button>
                                <br>
                                <br>
                            </form>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="form bg-partner p-3 px-5 border rounded text-white" style="font-size: 16px">
                            <h2 class="h2 text-center">Notify Provider</h2>
                            <br>
                            @if($benefit->provider_email)
                                <p class="text-center">
                                    You can also notify {{$benefit->provider->name}} that
                                    you
                                    will be calling to redeem
                                    this
                                    benefit. </p>
                            <p class="text-center">
                                    Please click this button below to send them an email with your
                                    current
                                    account details.
                            </p>
                            <p class="text-center">
                                Within the notify success page you will also have access to the provider contact number if you wish to contact them directly.
                            </p>
                                <form action="{{route('benefit.notify', $benefit->id)}}" method="POST">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6 mt-4 offset-md-3 text-center">
                                            <button id="notify-provider"
                                                    class="btn btn-light text-partner"
                                                    type="submit">Notify
                                                Provider
                                            </button>
                                        </div>
                                    </div>
                                    <br>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        @endif
    @endif
    @endauth
@endsection

