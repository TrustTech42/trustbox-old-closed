@extends('layouts.1col')
@section('content')
    <div class="container text-center my-5">
        <i class="fa fa-check-circle fa-7x mb-4" style="color: green"></i>
        <p>You've shown interest within the <span class="p partner-title"
                                                  style="font-weight: bold">{{$benefit->name}}</span> benefit.
        <p>We have notified the provider <span class="p partner-title"
                                               style="font-weight: bold">{{$benefit->provider->name}}</span> to let them
            know you will be calling.</p>
        <p>Alternatively you can give the provider a call directly.</p>
            <p>You will be asked to verify your customer reference number upon calling:</p>
        <p>Provider Contact Number: <span class="p partner-title"
                 style="font-weight: bold">{{$benefit->provider_telephone}}</span>.</p>
        <p>Customer Reference Number: <span class="p partner-title" style="font-weight: bold">{{$user->customer_number}}</span></p>

        <a href="/dashboard?fragment=boosted_benefits"
           class="btn btn-primary mt-4">
            View Benefits
        </a>
    </div>
    </div>
@endsection