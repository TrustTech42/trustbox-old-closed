@extends('layouts.1col')
@section('content')
    <div class="container text-center my-5">
        <i class="fa fa-check-circle fa-7x mb-4" style="color: green"></i>
        <p>You've successfully submitted the benefit contact form for the <span class="p partner-title" style="font-weight: bold">{{$benefit->name}}</span> benefit.
        <p>Please allow 1-5 working days for <span class="p partner-title" style="font-weight: bold">{{$benefit->provider->name}}</span> to contact you via email or phone.</p>

        <a href="/dashboard?fragment=boosted_benefits"
           class="btn btn-primary mt-4">
            View Benefits
        </a>
    </div>
@endsection