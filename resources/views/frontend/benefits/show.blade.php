@extends('layouts.full-page')
@section('content')
    <div class="jumbotron benefit-banner text-white justify-content-center"
         style="background-image: url('/{{$benefit->banner}}')">
        <div class="container">
            @auth
                <a href="/dashboard?fragment=my_benefits"
                   class="btn btn-primary benefit-show-gift-back-btn">
                    GO BACK
                </a>
            @endauth
            @guest
                <a href="/benefits"
                   class="btn btn-primary benefit-show-gift-back-btn">
                    GO BACK
                </a>
            @endguest
        </div>
    </div>

    <div class="container benefit-content-container">
        <div class="row">
            <div class="col-xl-7 mb-3 mb-xl-0">
                <div class="benefit-content bg-white p-5">
                    <div class="row align-items-center mb-5">
                        @if($benefit->provider->provider_logo)
                            <div class="col-auto">
                                <div class="benefit-provider-container shadow"
                                     style="background-image: url('/{{$benefit->provider->provider_logo}}')">
                                </div>
                            </div>
                        @endif
                        <div class="col-12 col-md-8 text-sm-center">
                            <h1 class="h1 benefit-title"
                                style="color: black !important; font-weight: 400 !important;">{{\Illuminate\Support\Str::limit($benefit->name, 50, $end='...') }}</h1>
                        </div>
                    </div>
                    <hr>
                    <h2 class="h2 mb-4 mt-5">About This Benefit</h2>
                    {!! $benefit->description !!}
                    @if($benefit->price)
                        <h2 class="h2 my-4">Benefits with Boosted</h2>
                        {!! $benefit->boosted_description !!}
                    @endif
                </div>
            </div>
            <div class="col-xl-5">
                <div class="provider-content bg-white p-5">
                    <h2 class="h2 benefit-provider-h2 mb-5" style="font-weight:500">Your Redemption Options</h2>
                    @if(!$benefit->price == 0)
                        <div class="row">
                            <div class="col-md">
                                <p style="font-weight:400">Benefit price</p>
                                <p class="text-partner"
                                   style="margin-top: -25px; margin-bottom: -10px; font-weight: bold; font-size: 45px">
                                    £{{sprintf('%0.2f', $benefit->price)}}</p>
                                <p class="text-partner" style="font-weight: bold">Per Month</p>
                            </div>
                            {{--                        <div class="col-md">--}}
                            {{--                            <p style="font-weight:400">Boost for an extra</p>--}}
                            {{--                            <p--}}
                            {{--                               style="margin-top: -25px; margin-bottom: -10px; font-weight: bold; font-size: 45px">--}}
                            {{--                                £{{sprintf('%0.2f', $benefit->price)}}</p>--}}
                            {{--                            <p class style="font-weight: bold">Per Month</p>--}}
                            {{--                        </div>--}}
                        </div>
                    @endif
                    <br>
                    <div class="row">
                        @if(Auth::user())
                            <div class="col-md">
                                @if(in_array($benefit->id, Auth::user()->benefits->pluck('id')->toArray()))
                                    @if($benefit->id === 2)
                                        <a href="/dashboard?fragment=file_manager"
                                           class="btn btn-primary benefit-content-show-btn text-white btn-block">
                                            REDEEM NOW
                                        </a>
                                    @else
                                        <a href="/benefits/{{$benefit->id}}/redeem"
                                           class="btn btn-primary benefit-content-show-btn text-white btn-block">
                                            REDEEM NOW
                                        </a>
                                    @endif
                                @else
                                    {!! Form::open(['route' => 'enrol', 'class' => 'benefit-' . $benefit->id]) !!}
                                    <input type="hidden" name="benefit_id" value="{{$benefit->id}}">
                                    <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
                                    <input type="hidden" name="hash" value="">
                                    <button class="btn btn-primary benefit-content-show-btn text-white btn-block"
                                            type="button"
                                            data-toggle="modal" data-target="#enroll-confirm"
                                            data-benefit="{{$benefit->id}}" data-title="{{$benefit->name}}"
                                            data-provider="{{$benefit->provider->name}}">ENROL NOW
                                    </button>
                                    {!! Form::close() !!}
                                @endif
                            </div>
                            <br><br><br>
                            <div class="col-md">
                                @if(in_array($benefit->id, Auth::user()->benefits->pluck('id')->toArray()) && !Auth::user()->benefits()->where('benefit_id', $benefit->id)->first()->pivot->boosted && $benefit->price)
                                    @if($user->subscription)
                                        @if($benefit->price)
                                            <button class="btn btn-primary benefit-content-show-btn text-white btn-block"
                                                    type="button"
                                                    data-toggle="modal" data-target="#boost-edit">BOOST NOW
                                            </button>
                                        @endif
                                    @else
                                        @if($benefit->price)
                                            <button class="btn btn-primary benefit-content-show-btn text-white btn-block"
                                                    type="button"
                                                    data-toggle="modal" data-target="#boost-confirm">BOOST NOW
                                            </button>
                                            @endif
                                            @endif
                                            </form>
                                        @else
                                            @if(in_array($benefit->id, Auth::user()->benefits->pluck('id')->toArray()) && Auth::user()->benefits()->where('benefit_id', $benefit->id)->first()->pivot->boosted)
                                                <a href="/dashboard?fragment=boosted_benefits"
                                                   class="btn btn-primary benefit-content-show-btn btn-block"
                                                   type="button"> <i
                                                            class="fas fa-check"
                                                            style="color: green; font-weight: bold; padding-right: 10px"></i>
                                                    SUBSCRIBED
                                                </a>
                                                {{--                                                <i class="fas fa-check fa-2x ml-5 mr-1 mt-1"--}}
                                                {{--                                                   style="color: green; font-weight: bold"></i>&nbsp;--}}
                                                {{--                                                &nbsp;<a style="color: green; font-size: 25px">Boosted</a>--}}
                                            @endif
                                        @endif
                            </div>
                        @else
                            <div class="col-md-6">
                                <form action="/user-login">
                                    <input type="hidden" name="fragment" value="my_benefits">
                                    <button class="btn btn-primary benefit-content-show-btn">ENROL NOW</button>
                                </form>
                            </div>
                        @endif
                    </div>
                    <hr class="mt-5">
                    <div>
                        <br>
                        <h3 class="h3 text-partner" style="font-weight: 500;">Service provided
                            by {{$benefit->provider->name}}</h3>
                    </div>
                    <p>{!! $benefit->provider_description !!}</p>
                    <h3 class="h3 mt-4 text-partner" style="font-weight: 500;">Why this Provider?</h3>
                    <p>{!! $benefit->provider_usp !!}</p>
                </div>
            </div>
            <div class="container">
                @auth
                    @if(in_array($benefit->id, Auth::user()->benefits->pluck('id')->toArray()))
                        <button style="margin-top: 20px; margin-bottom: 20px"
                                class="btn btn-primary benefit-show-gift-back-btn text-white btn-block"
                                data-toggle="modal"
                                data-target="#user_details_modal">
                            GIFT
                        </button>
                    @else
                        @isset($user->membership)
                            <button style="margin-top: 20px;"
                                    class="btn btn-primary benefit-show-gift-back-btn text-white btn-block"
                                    data-toggle="modal"
                                    data-target="#user_details_modal">
                                GIFT NOW
                            </button>
                        @endisset
                    @endif
                @endauth
            </div>
            {{--       MODALS    --}}
            @isset($user->subscription)
                {!! Form::open(['route' => ['subsequent.benefit.boost', $benefit->id] , 'class' => 'benefit-show-form']) !!}
                <div id="boost-edit" class="modal fade">
                    <div class="modal-dialog modal-dialog-centered modal">
                        <div class="modal-content">
                            <div class="modal-body p-5 d-flex justify-content-center flex-wrap">
                                <h3 class="h2 w-100 text-center">
                                    You are changing your subscription
                                </h3>
                                <p class="w-100 text-center">Confirm your new monthly price below</p>
                                <hr class="w-100">
                                <div class="boosted-price w-100 text-center">
                                    <strong class="text-muted old-price"
                                            style="text-decoration: line-through">£ {{$user->subscription->total}}</strong>
                                    <strong class="ml-3 text-member new-price">£ {{$newSubscriptionCost}} per
                                        month</strong>
                                </div>
                                <hr class="w-100">

                                <p class="text-muted text-center">
                                    By Confirming you are agreeing to our Website Terms of Use. NoChex uses personal
                                    data as
                                    described in our Privacy Notice.
                                </p>

                                <div class="row my-4 w-100">
                                    <div class="col d-flex justify-content-center">
                                        <button class="btn btn-primary boost-edit-modal-button mt-3">
                                            <i class="fa fa-lock mr-2"></i>
                                            CONFIRM SUBSCRIPTION PRICE
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            @endisset
            <div id="boost-confirm" class="modal hide fade">
                <div class="modal-dialog modal-dialog-centered modal-lg">
                    <div class="modal-content">
                        <div class="modal-body p-5 d-flex justify-content-center flex-wrap">
                            <h3 class="w-100 text-center">Boosting New Benefits</h3>
                            <hr class="w-100">
                            <div class="boosted-price w-100 text-center text-member">
                                <strong class="new-price">£{{sprintf('%0.2f', $benefit->price)}} per month</strong>
                            </div>
                            <hr class="w-100">

                            <p class="text-muted text-center">
                                As it’s your first time boosting benefits, you will need to set up a direct debit
                                payment
                                via the NoChex
                                checkout to create a new subscription for your account.
                                <br>
                                <br>
                                You will only need to do this once.
                            </p>
                            <a href="{{route('nochex.boost-benefit', $benefit->id)}}" id="ncx-show-checkout"
                               class="btn btn-primary btn-modal" type="button">Pay Now</a>&nbsp;&nbsp;&nbsp;
                            <button data-dismiss="modal" class="btn btn-danger btn-modal" type="button">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
            <div id="enroll-confirm" class="modal fade" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered modal-lg">
                    <div class="modal-content">
                        <div class="modal-body p-5 d-flex justify-content-center flex-wrap">
                            <h3 class="h2 w-100 text-center">
                                <span class="name">24 Hour Support</span>
                                - Enrollment
                            </h3>
                            <p class="w-100 text-center">Confirm that you agree to the Terms & Conditions below</p>
                            <hr class="w-100">
                            <p class="text-muted text-center">
                                By enrolling on this benefit you are confirming that you are agree to us to sharing your
                                data
                                with
                                <a class="provider">{{$benefit->provider->name}}</a>
                                , the provider of this benefit.
                            </p>

                            <button class="btn btn-primary mt-3">
                                <i class="fa fa-check mr-2"></i>
                                CONFIRM BENEFIT ENROLLMENT
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container my-5">
        @include('frontend.partials.html.questions')
    </div>
    <!-- add new modal -->
    @isset($user->membership)
        <div class="modal fade" id="user_details_modal">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-body p-5">
                        <h3 class="h2 w-100 text-center">
                    <span class="name">Invite Friends or Family

                    </span>
                        </h3>

                        <hr>
                        <p class="text-muted text-center">
                            Use the form below to invite friends or family to join you on the
                            <span class="text-capitalize">{{$user->membership->partner->name}}</span>
                            platform. You can choose to extend a Household membership.
                            <br>
                            <br>
                            Household invites are limited to 4 per user.
                        </p>
                        <hr>

                        {!! Form::open(['route' => 'invite']) !!}
                        <div class="input-group position-relative mb-3 d-block d-xl-flex">
                            <input type="hidden" name="user" value="{{$user->id}}">
                            <input type="hidden" name="partner" value="{{$user->membership->partner->id}}">

                            <input type="text" name="new_user_name"
                                   class="form-control mb-3 mb-xl-0 invite-member-form"
                                   placeholder="Recipient's Name" required>
                            <input type="email" class="form-control mb-3 mb-xl-0 invite-member-form"
                                   placeholder="Recipient's Email Address" name="new_user_email" required>
                            <div class="input-group-append">
                                <select name="plan" id="plan" class="custom-select">
                                    <option value="household">Household</option>
                                    @if($membership !== 'gifted')
                                        <option value="gifted">Gifted</option>
                                    @endif

                                    @if($membership === 'full')
                                        <option value="trusted">Trusted</option>
                                    @endif
                                </select>
                            </div>

                            <div class="invalid-tooltip">
                                Email Address is required
                            </div>
                        </div>
                        {{--test comment--}}
                        {{--                    test comment 2--}}
                        {{-- <div class="mobile my-3 d-xl-none">
                             <div class="form-group">
                                 <input type="text" name="new_user_name" class="form-control" placeholder="Recipient's Name" required>
                             </div>
                             <div class="form-group">
                                 <input type="email" class="form-control" placeholder="Recipient's Email Address" name="new_user_email" required>
                             </div>
                             <select name="plan" id="plan" class="custom-select">
                                 <option value="household">Household</option>

                                 @if($membership !== 'gifted')
                                     <option value="gifted">Gifted</option>
                                 @endif

                                 @if($membership === 'full')
                                     <option value="trusted">Trusted</option>
                                 @endif
                             </select>
                         </div>--}}

                        <button class="btn btn-primary btn-block" type="submit">
                            SEND INVITE
                        </button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    @endisset
    <!-- end of modal -->
@endsection
