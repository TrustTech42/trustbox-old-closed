<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>TrustBox - Dashboard</title>
    <script src="{{ mix('js/frontend.js') }}" defer></script>
    <link href="{{ mix('css/frontend.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('images/favicon.jpg') }}">

@yield('header_scripts')

<!-- Dynamic stylesheet -->
    @include('frontend.partials.dynamic_stylesheet')
</head>
<body>
@include('shared.error')
@include('shared.success')
<div id="app">
    <div class="container py-5">
        <div class="row">
            <div class="col-12 text-center mb-5">
                @yield('top')
            </div>
        </div>
        <div class="col-md-10">
            <div id="user-main">
                <table class="table">
                    <tr>
                        <h2>Thank You!</h2>
                        <p class="ncx-alert-success">
                            Your transaction has been completed successfully
                        </p>
                        <p>Date:
                            {{$_POST['transaction_date']}}
                        </p>
                        <p>Description:
                            {{$_POST['description']}}
                        </p>
                        <p>Amount:
                            £{{$_POST['amount']}}</p>
                    </tr>
                </table>
                <button id="close-success" class="btn btn-primary btn-block mt-3" type="button" onclick="window.top.postMessage('redirect', '*');">View Benefits</button>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/includes/shared/other.js') }}" defer></script>
@yield("scripts")
</body>
</html>
