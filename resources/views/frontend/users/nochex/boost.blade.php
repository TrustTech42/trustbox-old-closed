@extends('layouts.nochex')
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>TrustBox</title>
    <script src="{{ mix('js/frontend.js') }}" defer></script>
    <link rel="shortcut icon" href="{{ asset('images/favicon.jpg') }}">
    <link href="{{ mix('css/frontend.css') }}" rel="stylesheet">

    <script src="https://secure.nochex.com/exp/jquery.js"></script>
    <script src="https://secure.nochex.com/exp/nochex_lib.js"></script>

<!-- Dynamic stylesheet -->
    @include('frontend.partials.dynamic_stylesheet')
</head>
<body>

@include('shared.error')
@include('shared.success')

<div id="app">
    <form id="nochexForm" class="ncx-form" name="nochexForm">
        <input type="hidden" name="back_colour" value="#0066ff">
        <script id="ncx-config"
                ncxField-api_key="{{env("MIX_NOCHEX_KEY")}}"
                ncxField-merchant_id="info@visionsharp.co.uk"
                ncxField-description="Benefit Boost"
                ncxField-fullname="{{$user->first_name}} {{$user->last_name}}"
                ncxField-email="{{$user->email}}"
                ncxField-phone="{{$user->telephone}}"
                ncxField-address="{{$user->address2}}"
                ncxField-city="{{$user->city}}"
                ncxField-postcode="{{$user->postcode}}"
                ncxField-amount="{{$benefit->price}}"
                ncxField-optional_1="{{$user->membership->partner->nochex_id}}, {{$benefit->partner_percentage_cut}}"
                ncxField-optional_2="{{$benefit->provider_nochex_id}},{{$benefit->provider_percentage_cut}}"
                ncxField-optional_3="{{$user->id}}"
                ncxField-optional_4="{{$benefit->id}}"
                ncxField-use_apc="true"
                ncxField-callback_url="https://yourtrustbox.co.uk/api/boost/callback"
                ncxField-success_url="{{route('boost.success')}}"
                ncxField-test_transaction="true"
        ></script>
    </form>

    <button id="ncx-show-checkout" class="btn btn-primary btn-block mt-3" hidden="hidden" type="button">Pay Now</button>

</div>

<script src="{{asset('js/includes/shared/other.js')}}" defer></script>

<script>
    (function($){
        $(document).ready(function(){
            $('#ncx-show-checkout').click();

            $('#ncx-exit-btn').on('click', function()
            {
                window.top.postMessage('closed', '*');
            })
        })
    }(jQuery))
</script>
</body>
</html>