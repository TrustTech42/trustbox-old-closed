<div class="tab-pane fade show active" id="account_details">
    <div class="row">
        <div class="col-md-4">
            <h2 class="h2 partner-title mb-3">Account Level</h2>

            <div class="d-flex align-items-center">
                <strong class="text-capitalize">{{$user->membership->plan->name}} membership</strong>
                @if($membership !== 'full')
                    <a href="{{ route('update.membership-payment', compact('user')) }}" class="btn btn-primary ml-3">Upgrade Membership</a>
                @endif
            </div>

            <h2 class="h2 partner-title my-3">Customer Number</h2>
            <span>
                {{ $user->customer_number }}
            </span>

            @if($user->linked_user()->exists())
                <h2 class="h2 partner-title my-3">Referred By</h2>
                <span>
            {{ $user->linked_user()->first()->name }}
        </span>
            @endif
        </div>
{{--        <div class="col-md-8 mt-3 mt-md-0">--}}
{{--            <div class="tip bg-partner text-white">--}}
{{--                <div class="row align-items-center">--}}
{{--                    <div class="col-2 d-none d-md-block text-center">--}}
{{--                        <i class="fal fa-question-circle"></i>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-10">--}}
{{--                        Click the <strong>‘My Benefits’</strong> button to receive guidance and create Powers of Attorney and a Living Will at no cost. <br><br>--}}
{{--                        @if($membership !== 'household')Click the <strong>‘My Community’</strong> button if you know somebody who would benefit from this service and invite them to create their own account. We will also produce their documents at no cost.@endif--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>

    <h2 class="h2 partner-title my-3">Account Details</h2>
    {!! Form::open(['route' => ['user.update', $user->id], 'class' => 'needs-validation', 'novalidate']) !!}
    @method('patch')

    <div class="row">
        <div class="col-md-6">{!! Form::bsText('first_name', $user->first_name, ['required'], 'First Name') !!}</div>
        <div class="col-md-6">{!! Form::bsText('last_name', $user->last_name, ['required'], 'Last Name') !!}</div>
        <div class="col-md-6">{!! Form::bsText('address1', $user->address1, [], 'House Name / Number') !!}</div>
        <div class="col-md-6">{!! Form::bsText('address2', $user->address2, [], 'Address Line 2') !!}</div>
        <div class="col-md-6">{!! Form::bsText('city', $user->city, ['required'], 'City') !!}</div>
        <div class="col-md-6">{!! Form::bsText('postcode', $user->postcode, ['required'], 'Postcode') !!}</div>
        <div class="col-md-6">{!! Form::bsText('telephone', $user->telephone, ['required', 'pattern' => "^(?:\W*\d){11}\W*$"], 'Telephone Number') !!}</div>
        <div class="col-md-6">{!! Form::bsEmail('email', $user->email, ['required', 'type' => 'email'], 'Email Address') !!}</div>
    </div>

    <password showold="true"></password>

    <div class="row">
        <div class="col">
            <button class="btn btn-primary float-right">Update Details</button>
        </div>
    </div>

    {!! Form::close() !!}
</div>
