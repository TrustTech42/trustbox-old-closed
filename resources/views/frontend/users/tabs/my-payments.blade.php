<div class="tab-pane fade" id="my_payments">
    @if(!$payments & !$nochexPayment)
        <div class="card-deck">
            @component("components.reusable.filemanager-card")
                <div class="text-center">
                    <div class="display-1 mb-3">
                        <i class="fas fa-credit-card"></i>
                    </div>
                    No Payments
                </div>
            @endcomponent
        </div>
    @else
        @if($payments)
            <h2 class="h2 partner-title mb-4">Initial Subscription Payment</h2>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="table-responsive">
                            <table class="table" style="display: inline-table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Benefit Name</th>
                                    <th>Amount</th>
                                    <th>Created At</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($payments as $payment)
                                    <tr>
                                        <td>{{$payment->id}}</td>
                                        <td>{{$payment->benefit_name}}</td>
                                        <td>£{{$payment->amount}}</td>
                                        <td>{{$payment->created_at->format('d-m-Y')}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if($nochexPayment)
            <h2 class="h2 partner-title mb-4">Full Payments</h2>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="table-responsive">
                            <table class="table" style="display: inline-table">
                                <thead>
                                <div class="row table-head d-none d-md-flex">
                                    <th>ID</th>
                                    <th>Plan</th>
                                    <th>Amount</th>
                                    <th>Created At</th>
                                </div>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{$nochexPayment->id}}</td>
                                    <td>{{$user->membership->plan->name}}</td>
                                    <td>£{{$nochexPayment->amount}}</td>
                                    <td>{{$nochexPayment->created_at->format('d-m-Y')}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endif
</div>
