<div class="tab-pane fade" id="boosted_benefits">
    @if(!$user->subscription)
        <div class="card-deck">
            @component("components.reusable.filemanager-card")
                <div class="text-center">
                    <div class="display-1 mb-3">
                        <i class="fas fa-rocket"></i>
                    </div>
                    No Subscriptions
                </div>
            @endcomponent
        </div>
    @else
        <h2 class="h2 partner-title mb-3">
            Your Subscriptions
        </h2>

        {{--    @if(session()->get('boosted'))--}}
        {{--        <dd-modal sub_date="{{ date('l jS \of F Y', strtotime($subscriptionDate)) }}" sub_date_unform="{{ $subscriptionDate }}"></dd-modal>--}}
        {{--    @endif--}}

        @if(session()->has('upgrade'))
            <upgrade-modal upgrade="{{ session()->pull('upgrade') }}"
                           default_size="{{App\Setting::first()->default_storage}}"
                           size="{{App\Setting::first()->upgraded_storage}}"
                           price="{{App\Setting::first()->upgraded_storage_price}}"></upgrade-modal>
        @endif

        <div class="row">
            <div class="col-xl-3">
                <div class="flex-wrapper">
                    <div class="single-chart">
                        <svg viewbox="0 0 42 42" class="circular-chart mobile-circular-chart primary">
                            <path class="circle-bg"
                                  d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
                            />

                            @if($user->benefits()->count() > 0)
                                <path class="circle"
                                      stroke-dasharray="{{$countBoostedBenefits / $user->benefits()->count()  * 100}}, 100"
                                      d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
                                />
                                <text x="18" y="20.35"
                                      class="percentage">{{$countBoostedBenefits . ' / ' . $user->benefits->count()}}</text>
                                <text x="18" y="25" class="description">Subscriptions</text>
                            @endif
                        </svg>
                    </div>
                </div>

                <div class="monthly-total">Monthly Total:
                    <span class="price text-member">£
                    <span class="value">{{$boostedTotal}} {{$user->storage_upgraded ? ' + ' . App\Setting::first()->upgraded_storage_price : null}}</span>
                </span>
                </div>
                <div class="new-total">New Total:
                    <span class="price text-member">£<span class="value"></span></span>
                </div>
                @if($user->storage_upgraded)
                    <div class="boosted-filemanager bg-member text-white p-3 text-center rounded">
                        Additional £{{App\Setting::first()->upgraded_storage_price}} for Upgraded Storage Plan
                    </div>
                @endif
            </div>
            <div class="col-xl-9">
                <div class="row">
                    @foreach($boostedBenefits as $benefit)
                        <div class="col-xl-6 p-0 p-xl-4 mb-5 mb-xl-0">
                            <form action="{{route('cancel.boost', $benefit->id)}}" method="POST">
                                @csrf
                                <div class="card card-benefit benefit-border-rounded border">
                                    <div class="card-header benefit-img border rounded-top"
                                         style="background-image: url({{$benefit->thumbnail}})"></div>
                                    <div class="card-block">
                                        <div class="benefit-overlay-container benefit-border-rounded border"
                                             style="background-image: url('/{{$benefit->provider->provider_logo}}')">
                                        </div>
                                        <div class="card-body">
                                            <div class="card-text-container">
                                                <h5 class="card-text text-md-left">
                                                    {{ \Illuminate\Support\Str::limit($benefit->name, 70, $end='...') }}
                                                </h5>
                                            </div>
                                            <div class="row w-100 no-gutters mt-3">
                                                <div class="d-flex d-md-block col-8 col-md-8">
                                                    <button class="btn btn-danger benefit-btn-block"
                                                            style="font-size: 0.85rem;"
                                                            type="button" data-toggle="modal"
                                                            data-target="#boost-edit-{{ $benefit->id }}">Cancel
                                                    </button>
                                                </div>
                                                <div class="col-4 col-md-4">
                                                    <div class="benefit-enrolled-container">
                                                        @auth
                                                            <b>£{{$benefit->price}} a month</b>
                                                        @endauth
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if($user->subscription)
                                    <div id="boost-edit-{{ $benefit->id }}" class="modal fade">
                                        <div class="modal-dialog modal-dialog-centered modal">
                                            <div class="modal-content">
                                                <div class="modal-body p-5 d-flex justify-content-center flex-wrap">
                                                    <h3 class="h2 w-100 text-center">
                                                        You are changing your subscription
                                                    </h3>
                                                    <p class="w-100 text-center">Confirm your new monthly price
                                                        below</p>
                                                    <hr class="w-100">
                                                    <div class="boosted-price w-100 text-center">
                                                        <strong class="text-muted old-price"
                                                                style="text-decoration: line-through">£ {{$user->subscription->total}}
                                                        </strong>
                                                        <strong class="ml-3 text-member new-price">£<span
                                                                    class="value"></span> {{(int)$user->subscription->total - (int)$benefit->price}}
                                                            per month</strong>
                                                    </div>
                                                    <hr class="w-100">
                                                    <p class="text-muted text-center">
                                                        By Confirming you are agreeing to our Website Terms of Use.
                                                        NoChex
                                                        uses
                                                        personal
                                                        data as
                                                        described in our Privacy Notice.
                                                    </p>

                                                    <div class="row my-4 w-100">
                                                        <div class="col d-flex justify-content-center">
                                                            <button class="btn btn-primary boost-edit-modal-button mt-3"
                                                                    type="submit">
                                                                <i class="fa fa-lock mr-2"></i>
                                                                CONFIRM SUBSCRIPTION PRICE
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </form>
                        </div>
                    @endforeach
                </div>
                {{--            <boost-form benefits="{{ json_encode($boostedBenefits) }}" subscription="{{ $subscription }}" user="{{ $user }}"></boost-form>--}}
            </div>
        </div>
    @endif
</div>