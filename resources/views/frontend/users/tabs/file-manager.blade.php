<div class="tab-pane fade" id="file_manager">
    <div class="card-deck">
        @if($user->fileManagerEnrolled && $user->isFileManagerTrial)
            @component("components.reusable.filemanager-card")
                <i class="fas fa-calendar full-size"></i>

                <div class="text-center">
                    <div class="display-1">
                        {{$user->trialDate}}
                    </div>
                    Days of trial remaining
                </div>


                <small class="trial-end-date">
                    Ending on {{\Illuminate\Support\Carbon::parse($user->file_manager_trial_date)->toFormattedDateString()}}
                </small>
            @endcomponent
        @endif

        @if($user->fileManagerEnrolled && !$user->boostedFileManager && $user->trialExpired)
            @component("components.reusable.filemanager-card")
                <div class="text-center">
                    <div class="display-1 mb-3">
                        <i class="fal fa-calendar"></i>
                    </div>
                    Your Trial has Expired

                    <a class="btn btn-primary btn-sm btn-block mt-3" href="/benefits">
                        <i class="fal fa-rocket mr-2"></i>
                        Boost Now
                    </a>
                </div>
            @endcomponent
        @endif

        @if(!$user->fileManagerEnrolled)
            @component("components.reusable.filemanager-card")
                <div class="text-center">
                    <div class="display-1 mb-3">
                        <i class="fas fa-star"></i>
                    </div>
                    You are not enrolled for the Document Vault Benefit
                </div>
            @endcomponent
        @endif

        @if($user->fileManagerEnrolled && ($user->isFileManagerTrial || $user->boostedFileManager))
            @component("components.reusable.filemanager-card")
                <div class="display-1 mb-3">
                    <i class="fal fa-file"></i>
                </div>
                Your Document Vault

                <a href="http://filemanager.{{env("APP_DOMAIN")}}" class="btn btn-primary btn-sm btn-block mt-3">
                    Go
                    <i class="fal fa-arrow-right ml-2"></i>
                </a>
            @endcomponent
        @endif

        @if($user->fileManagerEnrolled && ($user->isFileManagerTrial || $user->boostedFileManager))
            @component("components.reusable.filemanager-card")
                @if($user->storage > 0)
                    <svg viewbox="0 0 36 36" class="circular-chart primary smaller">
                        <path class="circle-bg" d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"/>
                        <path class="circle"
                              stroke-dasharray="{{$user->storage_used / $user->storage * 100}}, 100"
                              d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"/>
                        <text x="18" y="20.35" class="percentage">{{$user->storage_used . ' / ' . $user->storage}}GB</text>
                    </svg>
                @endif
{{--                <button class="btn btn-primary btn-sm px-5" data-toggle="modal" data-target="#storage-upgrade-modal">--}}
{{--                    <i class="fal fa-upload mr-2"></i>--}}
{{--                    Change Plan--}}
{{--                </button>--}}
            @endcomponent
        @endif

        @foreach($user->trustees as $trustee)
            @component("components.reusable.filemanager-card")
                <div class="text-center">
                    <div class="display-1 mb-3">
                        <i class="fal fa-unlock"></i>
                    </div>
                    You are trusted by
                    <br>
                    <strong class="text-partner">{{$trustee->name}}</strong>
                    <a href="http://filemanager.{{env("APP_DOMAIN")}}/trusted/{{$trustee->id}}" class="btn btn-primary btn-sm btn-block mt-3">
                        Go
                        <i class="fal fa-arrow-right ml-2"></i>
                    </a>
                </div>
            @endcomponent
        @endforeach
    </div>
</div>

@component("components.reusable.modal", ["id"=>"storage-upgrade-modal", "size"=>"lg"])
    <section class="pricing">
        <div class="row">
            <!-- Free Tier -->
            <div class="col-lg-6">
                <div class="card border-partner mb-5 mb-lg-0">
                    <div class="card-body">
                        <h5 class="card-title text-muted text-uppercase text-center">Basic</h5>
                        <h6 class="card-price text-center">{{$user->storage_upgraded ? 'Free' : 'Current'}}</h6>
                        <hr>
                        <ul class="fa-ul">
                            <li>
                                <span class="fa-li"><i class="fas fa-check"></i></span>
                                {{App\Setting::first()->default_storage}}GB Storage
                            </li>
                        </ul>
                        @if(!$user->storage_upgraded)
                            <button class="btn btn-block btn-primary disabled">YOUR CURRENT PLAN</button>
                        @else
                            <a href="/dashboard?fragment=boosted_benefits&upgrade=false" class="btn btn-primary btn-block">Choose This Plan</a>
                        @endif
                    </div>
                </div>
            </div>
            <!-- Plus Tier -->
            <div class="col-lg-6">
                <div class="card border-partner mb-5 mb-lg-0">
                    <div class="card-body">
                        <h5 class="card-title text-muted text-uppercase text-center">Plus</h5>
                        @if(!$user->storage_upgraded)
                        <h6 class="card-price text-center">£{{App\Setting::first()->upgraded_storage_price}}
                            <span class="period">/month</span>
                        </h6>
                        @else
                            <h6 class="card-price text-center">Current</h6>
                        @endif
                        <hr>
                        <ul class="fa-ul">
                            <li>
                                <span class="fa-li"><i class="fas fa-check"></i></span>
                                <strong>{{App\Setting::first()->upgraded_storage}}GB Storage</strong>
                            </li>
                        </ul>
                        @if(!$user->storage_upgraded)
                            <a href="/dashboard?fragment=boosted_benefits&upgrade=true" class="btn btn-primary btn-block">Choose This Plan</a>
                        @else
                            <button class="btn btn-block btn-primary disabled">YOUR CURRENT PLAN</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endcomponent
