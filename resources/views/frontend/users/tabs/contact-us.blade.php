<div class="tab-pane fade" id="contact_us">
    <h2 class="h2 partner-title my-3">Contact Us</h2>
    {!! Form::open(['route' => 'contact.store', 'class' => 'needs-validation', 'novalidate']) !!}
    <input type="hidden" name="partner_id" value="{{$global_partner->id}}">
    <input type="hidden" name="subject" value="NULL">

    <div class="row">
        <div class="col">
            <div class="row">
                <div class="col" style="padding-bottom: 5px">
                    {!! Form::open(['route' => 'contact.store', 'class' => 'needs-validation', 'novalidate']) !!}
                    <input type="hidden" name="partner_id" value="{{$global_partner->id}}">
                    <div class="row">
                        <div class="col-md-6 mb-4">
                            <label for="name">Name</label>
                            <input type="text" name="name" value="{{$user->first_name .' '. $user->last_name}}"
                                   class="form-control" readonly="readonly">
                        </div>
                        <div class="col-md-6 mb-4">
                            <label for="email">Email</label>
                            <input type="text" name="email" value="{{$user->email}}"
                                   class="form-control" readonly="readonly"
                                   required>
                        </div>
                    </div>
                    <label for="subject">Reason for Contact</label>
                    {!! Form::select('reason_id', $reasons, null, ['class' => 'custom-select']) !!}

                    <div class="form-group position-relative mt-4">
                        <label for="message" class="form-label">Message</label>
                        <textarea name="message" id="message" cols="30" rows="5" class="form-control"
                                  required></textarea>

                        <div class="invalid-tooltip">
                            This is a required field
                        </div>
                    </div>

                    <button class="btn btn-primary float-right mt-4">Send Message</button>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
