<div class="tab-pane fade" id="my_benefits">
    <div class="row">
        <div class="col-sm-8">
            <h1 class="h1 partner-title col-12">
                Your {{$global_partner->name}} Benefits
            </h1>
            <br>
            <p class="col-sm-12">As a {{$global_partner->name}} member, you, along with any family member in your
                household, have full access to a range
                of protective and cost saving benefits, including:
            </p>
        </div>
        @auth
            <div class="col-6 col-xl-2">
                <div class="single-chart">
                    <svg viewbox="0 0 36 36" class="circular-chart primary">
                        <path class="circle-bg"
                              d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
                        />

                        @if($global_partner->benefits()->count() > 0)
                            <path class="circle"
                                  stroke-dasharray="{{auth()->user()->benefits()->count() / $global_partner->benefits()->count() * 100}}, 100"
                                  d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
                            />
                            <text x="18" y="20.35"
                                  class="percentage">{{auth()->user()->benefits()->count() . ' / ' . $global_partner->benefits()->count()}}</text>
                            <text x="18" y="25" class="description">Enrolled</text>
                        @endif
                    </svg>
                </div>
            </div>
            <div class="col-6 col-xl-2">
                <div class="flex-wrapper">
                    @if(auth()->user()->benefits()->count() > 0)
                        <div class="single-chart">
                            <svg viewbox="0 0 36 36" class="circular-chart primary">
                                <path class="circle-bg"
                                      d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
                                />
                                <path class="circle"
                                      stroke-dasharray="{{auth()->user()->benefits()->where('boosted', 1)->count() / auth()->user()->benefits()->count()  * 100}}, 100"
                                      d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
                                />
                                <text x="18" y="20.35"
                                      class="percentage">{{auth()->user()->benefits()->where('boosted', 1)->count() . ' / ' . auth()->user()->benefits->count()}}</text>
                                <text x="18" y="25" class="description">Subscribed</text>
                            </svg>
                        </div>
                    @endif
                </div>
            </div>
        @endauth
    </div>
    <div class="row">
        @foreach($global_partner->benefits->sortBy('sort_order') as $benefit)
            <div class="col-xl-4 col-lg-6 p-xl-4 mb-5 mb-xl-0">
                <div class="card card-benefit benefit-border-rounded border">
                    <a class="card-header benefit-img border rounded-top"
                       style="background-image: url({{ $benefit->thumbnail }}"
                       href="/benefits/{{$benefit->id}}"></a>
                    <div class="card-block">
                        <div class="benefit-overlay-container benefit-border-rounded border"
                             style="background-image: url('/{{$benefit->provider->provider_logo}}')">
                        </div>
                        <div class="card-body">
                            <div class="card-text-container">
                                <h5 class="card-text text-md-left">
                                    <a href="/benefits/{{$benefit->id}}" class="benefit-name-card"
                                       style="text-decoration: none">
                                        {{ \Illuminate\Support\Str::limit($benefit->name, 60, $end='...') }}
                                    </a>
                                </h5>
                            </div>

                            <div class="row w-100 no-gutters mt-3">
                                <div class="d-flex d-md-block col-8 col-md-8">
                                    <a href="/benefits/{{$benefit->id}}"
                                       class="btn btn-primary my-benefit-btn-block text-white"
                                       style="font-size: 0.85rem;">FIND OUT MORE</a>
                                </div>
                                <div class="col-4 col-md-4">
                                    <div class="benefit-enrolled-container">
                                        @auth
                                            @if(auth()->user()->benefits()->where('boosted', 1)->find($benefit->id))
                                                <i class="fas fa-check" style="color: green"></i>&nbsp;
                                                &nbsp;<a
                                                        style="color: green">Subscribed</a>
                                            @elseif(auth()->user()->benefits()->find($benefit->id))
                                                <i class="fas fa-check" style="color: green"></i>&nbsp;
                                                &nbsp;<a
                                                        style="color: green">Enrolled</a>
                                            @endif
                                        @endauth
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
