<div class="tab-pane fade" id="additional_members">
    <!-- quotas -->
    <div class="row mb-3">
        <div class="col-6 col-xl-2">
            <h2 class="h2 member-title text-center">Household</h2>
            <chart tag="Household Invites" total="{{App\Setting::first()->household_limit}}" accepted="{{ $household }}"></chart>
        </div>
        @if($membership == 'full')
            <div class="col-6 col-xl-2">
                <h2 class="h2 member-title text-center">Gifted</h2>
                <chart tag="Gifted Invites" total="{{App\Setting::first()->gifted_limit}}" accepted="{{ $gifted }}"></chart>
            </div>
        @endif
        {{--        <div class="col-xl-6">--}}
        {{--            <div class="tip bg-partner text-white">--}}
        {{--                <div class="row align-items-center">--}}
        {{--                    <div class="col-2 d-none d-xl-block text-center">--}}
        {{--                        <i class="fal fa-question-circle"></i>--}}
        {{--                    </div>--}}
        {{--                    <div class="col-xl-10">--}}
        {{--                        If you know somebody who might benefit from Powers of Attorney and a Living Will, invite them to create their own account and explore if they would indeed benefit. We will provide guidance and produce their documents at no cost. <br><br>--}}
        {{--                        <strong>We hope this helps!</strong>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
    </div>

    <div class="row align-items-center">
        <div class="col">
            <h2 class="h2 member-title">Additional Members</h2>
        </div>
        @if($household < App\Setting::first()->household_limit && $gifted < App\Setting::first()->gifted_limit && $membership !== 'household')
            <div class="col">
                <button class="btn btn-primary float-right" data-toggle="modal" data-target="#user_details_modal">Invite New</button>
            </div>
        @endif
    </div>
    <br>
    <div class="row">
        <div class="col">
            <div class="table-responsive">
                <table class="table" style="display: inline-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email Address</th>
                        <th>Member Status</th>
                    </tr>
                    <tbody>
                    @foreach($user->linkedUsers as $referred_user)
                        <tr>
                            <td>{{$referred_user->name}}</td>
                            <td>{{$referred_user->email}}</td>
                            <td>{{!is_null($referred_user->membership) ? $referred_user->membership->plan->name : ''}}</td>
                        </tr>
                    @endforeach
                    @foreach($invites as $invite)
                        <tr>
                            <td>{{$invite->name}}</td>
                            <td>{{$invite->email}}</td>
                            <td>Pending</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

{{--    <div class="row">--}}
{{--        <div class="col">--}}
{{--            <div class="table">--}}
{{--                <div class="row table-head d-none d-md-flex">--}}
{{--                    <div class="col">Name</div>--}}
{{--                    <div class="col">Email Address</div>--}}
{{--                    <div class="col">Member Status</div>--}}
{{--                    <div class="col">Amend</div>--}}
{{--                </div>--}}


{{--                @foreach($user->linkedUsers as $referred_user)--}}
{{--                    <div class="row table-row">--}}
{{--                        <div class="col-md">{{$referred_user->name}}</div>--}}
{{--                        <div class="col-md">{{$referred_user->email}}</div>--}}
{{--                        <div class="col-md text-capitalize">{{!is_null($referred_user->membership) ? $referred_user->membership->plan->name : ''}}</div>--}}
{{--                        <div class="col">--}}
{{--                            {!! Form::open(['route' => 'remove']) !!}--}}
{{--                            <input type="hidden" name="user" value="{{ $referred_user->id }}">--}}

{{--                            <button class="btn btn-secondary btn-sm">--}}
{{--                                <i class="fal fa-trash mr-2"></i> Remove--}}
{{--                            </button>--}}
{{--                            {!! Form::close() !!}--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                @endforeach--}}

{{--                @foreach($invites as $invite)--}}
{{--                    <div class="row table-row">--}}
{{--                        <div class="col-md">{{$invite->name}}</div>--}}
{{--                        <div class="col-md">{{$invite->email}}</div>--}}
{{--                        <div class="col-md">Pending</div>--}}
{{--                    </div>--}}
{{--                @endforeach--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}


<!-- add new modal -->
    <div class="modal fade" id="user_details_modal">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-body p-5">
                    <h3 class="h2 w-100 text-center">
                    <span class="name">Invite Friends or Family

                    </span>
                    </h3>

                    <hr>
                    <p class="text-muted text-center">
                        Use the form below to invite friends or family to join you on the
                        <span class="text-capitalize">{{$user->membership->partner->name}}</span>
                        platform. You can choose to extend a Household membership.
                        <br>
                        <br>
                        Household invites are limited to 4 per user.
                    </p>
                    <hr>

                    {!! Form::open(['route' => 'invite']) !!}
                    <div class="input-group position-relative mb-3 d-block d-xl-flex">
                        <input type="hidden" name="user" value="{{$user->id}}">
                        <input type="hidden" name="partner" value="{{$user->membership->partner->id}}">

                        <input type="text" name="new_user_name" class="form-control mb-3 mb-xl-0 invite-member-form"
                               placeholder="Recipient's Name" required>
                        <input type="email" class="form-control mb-3 mb-xl-0 invite-member-form"
                               placeholder="Recipient's Email Address" name="new_user_email" required>
                        <div class="input-group-append">
                            <select name="plan" id="plan" class="custom-select">
                                <option value="household">Household</option>
                                @if($membership !== 'gifted')
                                    <option value="gifted">Gifted</option>
                                @endif

                                @if($membership === 'full')
                                    <option value="trusted">Trusted</option>
                                @endif
                            </select>
                        </div>

                        <div class="invalid-tooltip">
                            Email Address is required
                        </div>
                    </div>
                    {{--test comment--}}
                    {{--                    test comment 2--}}
                    {{-- <div class="mobile my-3 d-xl-none">
                         <div class="form-group">
                             <input type="text" name="new_user_name" class="form-control" placeholder="Recipient's Name" required>
                         </div>
                         <div class="form-group">
                             <input type="email" class="form-control" placeholder="Recipient's Email Address" name="new_user_email" required>
                         </div>
                         <select name="plan" id="plan" class="custom-select">
                             <option value="household">Household</option>

                             @if($membership !== 'gifted')
                                 <option value="gifted">Gifted</option>
                             @endif

                             @if($membership === 'full')
                                 <option value="trusted">Trusted</option>
                             @endif
                         </select>
                     </div>--}}

                    <button class="btn btn-primary btn-block" type="submit">
                        SEND INVITE
                    </button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- end of modal -->
</div>
