<div class="tab-pane fade" id="list_broadcasts">
    <h2 class="h2 partner-title">View Broadcasts</h2>
    @if(!$broadcasts->isEmpty())
        <div class="row mt-4">
            <div class="col mt-4">
                <div class="table-responsive">
                    <table class="table" style="display: inline-table">
                        <thead>
                        <tr style="color: {{$global_partner->primary_colour}}">
                            <th>Title</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($broadcasts as $broadcast)
                            <tr>
                                <td>{{$broadcast->headline}}</td>
                                <td>{{Carbon\Carbon::parse($broadcast->valid_from)->format('d-m-Y') }}</td>
                                <td>
                                    <a href="/user/{{$user->id}}/broadcasts/{{$broadcast->id}}">
                                        View
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @else
        <div class="card-deck">
            @component("components.reusable.filemanager-card")
                <div class="text-center">
                    <div class="display-1 mb-3">
                        <i class="fas fa-envelope"></i>
                    </div>
                    No Broadcasts
                </div>
            @endcomponent
        </div>
    @endif
</div>
