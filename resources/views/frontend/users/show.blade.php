@extends('layouts.2col')
@section('top')
    <h1 class="h1 partner-title">My Account</h1>
@endsection
@section('sidebar')
    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
        <a class="nav-link active" id="account_details_tab" data-toggle="pill" href="#account_details" role="tab">
            <i class="fa fa-user"></i>
            Account Details
        </a>

        @if($membership !== 'household')
            <a class="nav-link" id="additional_members_tab" data-toggle="pill" href="#additional_members" role="tab">
                <i class="fa fa-users"></i>
                My Community
            </a>
        @endif

        <a class="nav-link" id="my_benefits_tab" data-toggle="pill" href="#my_benefits" role="tab">
            <i class="fa fa-star"></i>
            My Benefits
        </a>
        <a class="nav-link" id="boosted_benefits_tab" data-toggle="pill" href="#boosted_benefits" role="tab">
            <i class="fa fa-rocket"></i>
            Subscriptions
        </a>
        <a class="nav-link" id="my_payments_tab" data-toggle="pill" href="#my_payments" role="tab">
            <i class="fa fa-credit-card-front"></i>
            My Payments
        </a>
        <a class="nav-link" id="document_vault_tab" data-toggle="pill" href='#file_manager' role="tab">
            {{--        <span class="nav-link" id="#" data-toggle="pill" href='#' role="tab" disabled>--}}
            <i class="fa fa-file-alt"></i>
            Document Vault
            {{--        </span>--}}
        </a>
        <a class="nav-link" id="list_broadcasts_tab" data-toggle="pill" href="#list_broadcasts" role="tab">
            <i class="fa fa-envelope"></i>
            Broadcasts
        </a>
        <a class="nav-link" id="contact_us_tab" data-toggle="pill" href="#contact_us" role="tab">
            <i class="fa fa-envelope"></i>
            Contact Us
        </a>
    </div>
@endsection
@section('content')
    <div class="tab-content p-md-5">
        <!-- Account Details Tab -->
        @include('frontend.users.tabs.account-details')
        @include('frontend.users.tabs.additional-members')
        @include('frontend.users.tabs.my-benefits')
        @include('frontend.users.tabs.my-payments')
        @include('frontend.users.tabs.boosted-benefits')
        @include('frontend.users.tabs.file-manager')
        @include('frontend.users.tabs.contact-us')
        @include('frontend.users.tabs.list-broadcasts')
    </div>
@endsection


