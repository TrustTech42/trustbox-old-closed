@extends('layouts.mail')
@section('content')
    <table class="table" width="600px">
        <tr>
            <td>
                Dear {{$postOwner->name}}

                <p>{{$partner->name}} has published your {{$post->title}} post</p>
            </td>
        </tr>
        <tr>
            <td>
                <a class="btn btn-primary" href="https://{{$partner->subdomain}}.{{env("APP_DOMAIN")}}/posts/{{$post->id}}">Link to Post</a>
            </td>
        </tr>
    </table>
@endsection
