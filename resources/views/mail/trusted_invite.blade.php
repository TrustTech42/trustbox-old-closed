@extends('layouts.mail')
@section('content')
    <table class="table" width="600px">
        <tr>
            <td>
                {!! $content !!}
            </td>
        </tr>
        <tr>
            <td>
                @if($register)
                    <a class="btn btn-primary" href="http://{{$partner->subdomain}}.{{env("APP_DOMAIN")}}/invite/{{$user->id}}/{{$code}}">Start Now!</a>
                @else
                    <a class="btn btn-dark" href="#">
                        <i class="fal fa-times mr-2"></i> Decline <small>(nope)</small>
                    </a>
                    <a class="btn btn-primary" href="http://{{$partner->subdomain}}.{{env("APP_DOMAIN")}}/accept/{{$user->id}}/{{$linkedUser->id}}">
                        <i class="fal fa-check"></i> Accept
                    </a>
                @endif
            </td>
        </tr>
    </table>
@endsection
