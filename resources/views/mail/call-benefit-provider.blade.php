@extends('layouts.mail')
@section('content')
    <table class="table" width="600px">
        <tr>
            <td>
                {!! $vars->content !!}
            </td>
        </tr>
    </table>
@endsection
