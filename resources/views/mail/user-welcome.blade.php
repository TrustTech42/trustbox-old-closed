@extends('layouts.mail')
@section('content')
    <table class="table" width="600px">
        <tr>
            <td>
                {!! str_replace('https://', 'https://' . $vars->partner->subdomain . '.', $vars->content) !!}
            </td>
        </tr>
    </table>
@endsection
