@extends('layouts.mail')
@section('content')
    <table class="table" width="600px">
        <tr>
            <td>
                <p>Dear {{$member->first_name}}</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>You've received a new broadcast message from
                    <span style="font-weight: bold; color: {{$partner->primary_colour}}">{{$partner->name}}</span>
                </p>

                <p>Please visit the link below to log into your Trust Box account to view the message.</p>
            </td>
        </tr>
        <tr>
            <td style="text-align: center">

                <a href="{{ 'https://'.$partner->subdomain.'.yourtrustbox.co.uk/login' }}"
                   class="btn btn-primary">Login</a>
            </td>
        </tr>
    </table>
@endsection