@extends('layouts.mail')
@section('content')
    <table class="table" width="600px">
        <tr>
            <td>
                {!! str_replace('https://', 'https://' . $partner->subdomain . '.', $content) !!}
            </td>
        </tr>
        <tr>
            <td>
                <a class="btn btn-primary" href="https://{{$partner->subdomain}}.{{env("APP_DOMAIN")}}/invite/{{$user->id}}/{{$code}}">Join Now!</a>
            </td>
        </tr>
    </table>
@endsection
