@extends('layouts.mail')
@section('content')
    <table class="table" width="600px">
        <tr>
            <td>
                {!! $vars->content !!}

                <a href="{{ 'https://'.$vars->partner->subdomain.'.yourtrustbox.co.uk/invite/'.$vars->id.'/register/invite/user' }}" class="btn btn-primary">Register Link</a>
            </td>
        </tr>
    </table>
@endsection
