<style>
    * {
        font-family : "Lato", sans-serif;
    }

    table {
        width   : 600px;
        padding : 40px 0;
        margin  : 40px auto;
    }

    table td .btn {
        background-color : {{$partner->primary_colour}};
        color            : white;
        font-size        : 20px;
        text-decoration  : none;
        display          : inline-block;
        padding          : 20px 40px;
        margin-top       : 20px;
        transition       : background-color .2s ease;
    }

    table td .btn.btn-dark{
        background-color : #4c4d4e;
    }

    table td .btn:hover {
        cursor           : pointer;
        background-color : #4c4d4e;
    }

    table td .btn.btn-dark:hover{
        background-color : {{$partner->primary_colour}}
    }

    header img,  footer img
    {
        display: block;
        margin: 0 auto;
    }
    code
    {
        font-size: 22px;
        color: white;
        padding: .5rem;
        background-color: {{$partner->primary_colour}};
    }
    
    strong
    {
        color: {{$partner->primary_colour}}
    }

    footer .text-muted
    {
        text-align: center;
        color: #2c6471;
    }

    header, footer
    {
        padding: 1rem 0;
        background-color: #004454;
    }
</style>
