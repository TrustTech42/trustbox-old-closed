@extends('layouts.mail')
@section('content')
    <table class="table" width="600px">
        <tr>
            <td>
                {!! $vars->benefitContent !!}
                @foreach($vars->partner->benefits as $benefit)
                    <div class="row table-row">
                        <div class="col" style="padding-bottom: 10px"><strong>
                                <a href="{{'https://'.$vars->partner->subdomain.'.yourtrustbox.co.uk/benefits/'.$benefit->id}}"
                                   style="text-decoration: none; color: {{$vars->partner->primary_colour}};">{{$benefit->name}}</a></strong></div>
                    </div>
                @endforeach
                <p>If you need help, or you have any other questions, feel free to contact us using the online contact
                    form and a member of our customer service team will do their best to help you.</p>

                <p> Thanks, </p>

                <p>The Trust Box team</p>
            </td>
        </tr>
    </table>
@endsection