@extends('layouts.mail')
@section('content')
    <table class="table" width="600px">
        <tr>
            <td>
                <h1>{{$vars->name}}, your benefit request for {{ $vars->type }} has been {{ $vars->status }}</h1>
            </td>
        </tr>
    </table>
@endsection
