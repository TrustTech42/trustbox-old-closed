@extends('layouts.1col')
@section('content')
    <table class="table" width="600px">
        <tr>
            <td>
                <h1>{{$referred->name}} has</h1>
                <h2>Click below to begin creating your new account.</h2>
            </td>
        </tr>
        <tr>
            <td>
                <a class="btn btn-primary" href="http://{{$partner->subdomain}}.development-visionsharp.co.uk/invite/{{$user->id}}/household" style="">Start Now!</a>
            </td>
        </tr>
    </table>
@endsection
