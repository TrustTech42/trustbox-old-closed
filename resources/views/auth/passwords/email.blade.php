@extends('layouts.full-page')

@section('content')
    <div class="container mt-5">
        @include('shared.error')
    </div>

    <div class="container block block-text mb-4">
        <div class="row">
            <div class="col-6 offset-3">
                <h1 class="h1 partner-title text-capitalize">{{ __('Reset Password') }}</h1>
            </div>
        </div>
    </div>

    <div class="container p-3 my-5">
        <div class="row">
            <div class="col-6 offset-3 bg-light p-5 light-rounded">
                <form method="POST" action="{{ route('password.email') }}">
                    @csrf

                    <div class="form-group row">
                        <label for="email" class="col-form-label">{{ __('E-Mail Address') }}</label>

                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group row mb-0">
                        <button type="submit" class="btn btn-primary float-right">
                            {{ __('Send Password Reset Link') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
