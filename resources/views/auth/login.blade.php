@extends('layouts.full-page')
@section('content')
    <div class="container mt-5">
        @include('shared.error')
    </div>

    <div class="container block block-text mb-4">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <h1 class="h1 partner-title text-capitalize">User Login</h1>
                Log in to your account using the form below
            </div>
        </div>
    </div>

    <div class="container p-3 my-5">
        <div class="row px-3 px-md-0">
            <div class="col-md-6 offset-md-3 bg-light p-5 light-rounded login-form">
                {!! Form::open(['route' => 'login', 'method' => 'POST', 'novalidate', 'class' => 'needs-validation']) !!}
                    {!! Form::bsText('email', old('email'), ['required', 'autofocus', 'class' => 'form-control']) !!}
                    {!! Form::bsPassword('password', null, null, ['required', 'class' => 'form-control']) !!}

                    <a href="/password/reset" class="btn btn-link float-left">Forgotten Password?</a>
                    <button class="btn btn-primary float-right">LOG IN</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
