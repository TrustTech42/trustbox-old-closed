@extends('layouts.frontend')
@section('content')
    <div class="container py-3 mb-5">
        <div class="container block block-text mb-4">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <h1 class="h1 partner-title text-capitalize">Full Membership</h1>
                    Please complete the form below to purchase your <span class="text-capitalize">Full</span>.
                    A partner of the team will contact you within 2 business days to process your payment. Alternatively,
                    please call us on <a href="tel:03339997889">0333 999 7889</a>
                </div>
            </div>
        </div>

        @include('shared.error')

        <div class="row mb-5">
            <div class="col-md-6 offset-md-3">
                <div class="form bg-light p-3 px-5">
                    {!! Form::open(['route' => 'payment.create', 'class' => 'needs-validation', 'novalidate']) !!}
                    @method('get')
                    <input type="hidden" name="plan" value="1">
                    <input type="hidden" name="partner" value="{{$global_partner->id}}">

                    <div class="row">
                        <div class="col-12 col-md-6">
                            {!! Form::bsText('first_name', old('first_name'), ['required', 'tabindex' => 1]) !!}
                            {!! Form::bsText('address1', old('address1'), ['required', 'tabindex' => 3], 'House Name / No & Street') !!}
                            {!! Form::bsText('city', old('city'), ['required', 'tabindex' => 5]) !!}
                            {!! Form::bsText('telephone', old('telephone'), ['required', 'tabindex' => 7]) !!}

                            <div class="form-group">
                                <label for="date_of_birth" class="form-label">Date of Birth</label>
                                {{--<input type="date" name="date_of_birth" id="date_of_birth" class="form-control" value="{{old('date_of_birth')}}" tabindex="9">--}}
                                <div class="row">
                                    <div class="col border-right">
                                        <select name="dob_day" id="dob_day" class="custom-select" required>
                                            <option value="">DD</option>
                                            @for($i = 1; $i <= 31; $i++)
                                                <option value="{{str_pad($i, 2, '0', STR_PAD_LEFT)}}">{{str_pad($i, 2, '0', STR_PAD_LEFT)}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col border-right">
                                        <select name="dob_month" id="dob_month" class="custom-select" required>
                                            <option value="">MM</option>
                                            @for($i = 1; $i <= 12; $i++)
                                                <option value="{{str_pad($i, 2, '0', STR_PAD_LEFT)}}">{{str_pad($i, 2, '0', STR_PAD_LEFT)}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col">
                                        <select name="dob_year" id="dob_year" class="custom-select" required>
                                            <option value="">YYYY</option>
                                            @for($i = \Illuminate\Support\Carbon::now()->year - 18; $i >= 1890; $i--)
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            {!! Form::bsText('last_name', old('last_name'), ['required', 'tabindex' => 2]) !!}
                            {!! Form::bsText('address2', old('address2'), ['required', 'tabindex' => 4]) !!}
                            {!! Form::bsText('postcode', old('postcode'), ['required', 'tabindex' => 6]) !!}
                            {!! Form::bsEmail('email', old('email'), ['required', 'tabindex' => 8], 'Email Address') !!}
                        </div>
                    </div>

                    <password always="true" tab="9"></password>

                    <div class="row align-items-center">
                        <div class="col-md-7">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="customSwitch1" tabindex="11" required>
                                <label class="custom-control-label" for="customSwitch1">
                                    I agree to the <a href="page/privacy_policy">privacy policy</a> and <a href="page/terms_and_conditions">terms & conditions</a>
                                </label>

                                <div class="invalid-tooltip">
                                    Please accept the terms and conditions
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 mt-3 mt-md-0">
                            <button class="btn btn-primary btn-block" tabindex="12">PROCEED TO PAYMENT</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

{{--    <div class="container-fluid p-0">
        <div class="card full-image full-width">
            <div class="card-body" style="background: linear-gradient(rgba(0, 0, 0, 0.3), #1c1d1e), url('/storage/banners/banner.jpg');">
                <h3 class="h2 heading mb-4">Boost your Benefits with Plus
                    <img src="/storage/assets/boost.png" alt="">
                    for only £11 a month
                </h3>
                <div class="heading mb-4">
                    Get all of our benefits plus extra support and savings with {{$global_partner->name}} Plus
                    <br>
                    Click below to find out how you can benefit from Plus.
                </div>

                <a href="#" class="btn btn-primary mr-2">FIND OUT MORE</a>
            </div>
        </div>
    </div>--}}
@endsection
