@extends('layouts.full-page')
@section('content')
    <div class="container py-5">
        <div class="row">
            <div class="col-lg-6 offset-lg-3 text-center">
                <h1 class="h1 partner-title text-capitalize">{{$type->name}} Membership</h1>
                @if($type->name === 'household')
                    <p>You have been invited by <strong>{{$user->name}}</strong> to join their TrustBox household. To
                        accept this offer, please fill in the form below</p>
                @endif
            </div>
        </div>
    </div>

    @include('shared.error')

    <div class="container">
        <div class="row mb-5">
            <div class="col-xl-6 offset-xl-3">
                <div class="form bg-light p-3 ">
                    {!! Form::open(['route' => 'user.store', 'class' => 'needs-validation', 'novalidate']) !!}

                    <input type="hidden" name="plan" value="{{$type->id}}">
                    <input type="hidden" name="referred" value="{{$user->id}}">
                    <input type="hidden" name="partner" value="{{$global_partner->id}}">
                    <input type="hidden" name="code" value="{{$code}}">

                    <div class="row mb-5">
                        <div class="form col-12 p-0 bg-light p-3 px-5">
                            {!! Form::open(['route' => 'user.store', 'class' => 'needs-validation', 'novalidate', 'method' => 'post']) !!}
                            @csrf

                            <div class="row">
                                <div class="col-md-6">
                                    {!! Form::bsText('first_name', old('first_name'), ['required', 'tabindex' => 1]) !!}
                                </div>
                                <div class="col-md-6">
                                    {!! Form::bsText('last_name', old('last_name'), ['required', 'tabindex' => 2]) !!}
                                </div>
                                <div class="col-md-6">
                                    {!! Form::bsText('address1', old('address1'), ['required', 'tabindex' => 3], 'House Name / No & Street') !!}
                                </div>
                                <div class="col-md-6">
                                    {!! Form::bsText('address2', old('address2'), ['tabindex' => 4], 'Address Line 2') !!}
                                </div>
                                <div class="col-md-6">
                                    {!! Form::bsText('city', old('city'), ['required', 'tabindex' => 5]) !!}
                                </div>
                                <div class="col-md-6">
                                    {!! Form::bsText('postcode', old('postcode'), ['required', 'tabindex' => 6]) !!}
                                </div>
                                <div class="col-md-6">
                                    {!! Form::bsText('telephone', old('telephone'), ['required', 'tabindex' => 7, 'pattern' => "^(?:\W*\d){11}\W*$"]) !!}
                                </div>
                                <div class="col-md-6">
                                    {!! Form::bsEmail('email', old('email'), ['required', 'tabindex' => 8], 'Email Address') !!}
                                </div>
                                <div class="form-group">
                                    <label for="date_of_birth" class="form-label">Date of Birth</label>
                                    {{--<input type="date" name="date_of_birth" id="date_of_birth" class="form-control" value="{{old('date_of_birth')}}" tabindex="9">--}}
                                    <div class="row">
                                        <div class="col border-right">
                                            <select name="dob_day" id="dob_day" class="custom-select" tabindex="9" required>
                                                <option value="">DD</option>
                                                @for($i = 1; $i <= 31; $i++)
                                                    <option value="{{str_pad($i, 2, '0', STR_PAD_LEFT)}}" {{old('dob_day') == str_pad($i, 2, '0', STR_PAD_LEFT) ? 'selected' : ''}}>{{str_pad($i, 2, '0', STR_PAD_LEFT)}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="col border-right">
                                            <select name="dob_month" id="dob_month" class="custom-select" tabindex="10" required>
                                                <option value="">MM</option>
                                                @for($i = 1; $i <= 12; $i++)
                                                    <option value="{{str_pad($i, 2, '0', STR_PAD_LEFT)}}" {{old('dob_month') == str_pad($i, 2, '0', STR_PAD_LEFT) ? 'selected' : ''}}>{{str_pad($i, 2, '0', STR_PAD_LEFT)}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="col">
                                            <select name="dob_year" id="dob_year" class="custom-select" tabindex="11" required>
                                                <option value="">YYYY</option>
                                                @for($i = \Illuminate\Support\Carbon::now()->year - 18; $i >= 1890; $i--)
                                                    <option value="{{$i}}" {{old('dob_year') == $i ? 'selected' : ''}}>{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <password always="true" tab="13"></password>

                                    <div class="row align-items-center">
                                        <div class="col-12">
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch1" tabindex="14"
                                                       required>
                                                <label class="custom-control-label" for="customSwitch1">
                                                    I agree to the <a href="/privacy-policy">privacy policy</a> and <a
                                                        href="/terms-and-conditions">terms & conditions</a>
                                                </label>

                                                <div class="invalid-tooltip">
                                                    Please accept the terms and conditions
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 mt-3">
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" name="marketing_consent" value="1" class="custom-control-input"
                                                       id="marketing_consent" tabindex="15">
                                                <label class="custom-control-label" for="marketing_consent">
                                                    I wish to receive marketing communications from trustbox to keep me up to date with
                                                    the latest news, benefits and platform updates. For details on how your information
                                                    is used and stored, see our
                                                    <a href="/privacy-policy">Privacy Policy</a>
                                                </label>

                                                <div class="invalid-tooltip">
                                                    Please accept the terms and conditions
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 mt-3">
                                            <button class="btn btn-primary btn-block" tabindex="16">REGISTER FOR FREE</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
