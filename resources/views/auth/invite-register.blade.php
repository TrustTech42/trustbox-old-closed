@extends('layouts.full-page')
@section('content')
    <div class="container py-3 mb-5">
        <div class="container py-5">
            <div class="row">
                <div class="col-lg-6 offset-xl-3 text-center">
                    <h1 class="h1 partner-title text-capitalize">Invitation</h1>
                </div>
            </div>
        </div>
        @include('shared.error')
        <div class="row mb-5">
            <div class="col-lg-6 offset-lg-3">
                <div class="form bg-light p-3 px-5">
                    {!! Form::open(['route' => 'user.store', 'class' => 'needs-validation', 'novalidate']) !!}
                    <input type="hidden" name="plan" value="1">
                    <input type="hidden" name="partner" value="{{$global_partner->id}}">
                    <input type="hidden" name="invitation" value="{{$invitation}}">
                    <div class="row">
                        <div class="col-lg-6">{!! Form::bsText('first_name', null, ['required']) !!}</div>
                        <div class="col-lg-6">{!! Form::bsText('last_name', null, ['required']) !!}</div>
                        <div class="col-lg-6">{!! Form::bsText('address1', null, ['label' => 'House Name / Number', 'required']) !!}</div>
                        <div class="col-lg-6">{!! Form::bsText('address2', null, ['label' => 'Address Line 2', 'required']) !!}</div>
                        <div class="col-lg-6">{!! Form::bsText('city', null, ['required']) !!}</div>
                        <div class="col-lg-6">{!! Form::bsText('postcode', null, ['required']) !!}</div>
                        <div class="col-lg-6">{!! Form::bsText('telephone', null) !!}</div>
                        <div class="col-lg-6">{!! Form::bsEmail('email', null, ['required']) !!}</div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="date_of_birth" class="form-label">Date of Birth</label>
                                <input type="date" name="date_of_birth" id="date_of_birth" class="form-control" value="{{old('date_of_birth')}}" tabindex="9">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">{!! Form::bsPassword('password', null) !!}</div>
                        <div class="col-lg-6">{!! Form::bsPassword('password_confirmation', null) !!}</div>
                    </div>
                    <div class="row">
                        <div class="col">
                            {!! Form::bsText('code', null, ['required']) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <button class="btn btn-primary float-right mt-3">Register</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection