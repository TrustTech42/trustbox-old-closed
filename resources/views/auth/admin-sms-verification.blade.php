@extends('layouts.admin-login')
@section('content')
    <div class="container mt-5">
        @include('shared.error')
    </div>

    <div class="container block block-text mb-4">
        <div class="row">
            <div class="col-6 offset-3">
                <h1 class="h1 partner-title text-capitalize">Sms Verification</h1>
                Log in to your account using the form below
            </div>
        </div>
    </div>

    <div class="container p-3 my-5">
        <div class="row">
            <div class="col-6 offset-3 bg-light p-5 light-rounded">
                {!! Form::open(['route' => 'sms-verification.verify.contact', 'method' => 'POST']) !!}
                {!! Form::bsText('code', null, ['required', 'autofocus', 'class' => 'form-control']) !!}

                <a href="{{ url()->route('sms-verification.send.code', ['user' => $user]) }}" class="btn btn-primary float-left">RESEND CODE</a>

                <button class="btn btn-primary float-right">SUBMIT</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
