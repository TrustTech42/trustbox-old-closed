<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<div style="width: 30%; margin-right: auto; margin-top: 190px">
    {!! $user->address1 !!}
    <br>
    {!! $user->address2 !!}
    <br>
    {!! $user->address3 !!}
    <br>
    {!! $user->address4 !!}
    <br>
    {!! $user->postcode !!}
</div>

<p>
    {!! $user->content  !!}
</p>

<p>
    {{ 'https://'.$user->partner->subdomain.'.yourtrustbox.co.uk/invite/'.$user->id.'/register/invite/user' }}
</p>


