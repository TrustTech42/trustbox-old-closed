<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<div style="width: 30%; margin-right: auto; margin-top: 190px">
    {{ $user->address1 }}
    <br>
    {{ $user->address2 }}
    <br>
    {{ $user->city }}
    <br>
    {{ $user->postcode }}
</div>

<p>{{ $user->name }}</p>

<p>{!! $content  !!}</p>

