@php @endphp

<div class="modal fade"id="{{$id ?? 'test-modal'}}">
    <div class="modal-dialog modal-dialog-centered modal-{{$size ?? 'xl'}}">
        <div class="modal-content bg-transparent border-0">
            <div class="modal-body">
                {{$slot}}
            </div>
        </div>
    </div>
</div>