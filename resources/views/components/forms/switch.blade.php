<input type="hidden" name="{{$name}}" value="0">
<div class="custom-control custom-switch">
  <input type="checkbox" class="custom-control-input" id="{{$name}}" name="{{$name}}" value="1" {{$value == "1" ? "checked" : null}}>
  <label class="custom-control-label" for="{{$name}}">{{$label}}</label>
</div>