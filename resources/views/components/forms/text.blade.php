<div class="form-group position-relative">
    @if(!isset($attributes['no-label']))
    {{ Form::label($label ?? $name, $attributes['label'] ?? null, ['class' => 'control-label']) }}
    @endif
    {{ Form::text($name, $value, array_merge(['class' => 'form-control'], $attributes ?? [])) }}

    <div class='invalid-tooltip'>
      Please enter a valid {{$label ?? $name}}
    </div>
</div>
