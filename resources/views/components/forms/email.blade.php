<div class="form-group position-relative">
    @if(!isset($attributes['no-label']))
    {{ Form::label($label ?? $name, null, ['class' => 'control-label']) }}
    @endif
    {{ Form::email($name, $value ?? '', array_merge(['class' => 'form-control'], $attributes ?? [])) }}

    <div class='invalid-tooltip'>
      Please enter a valid {{$name}}
    </div>
</div>
