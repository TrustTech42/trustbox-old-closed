<div class="form-group position-relative {{isset($attributes['no-margin']) ? 'mb-0' : ''}}">
    @if(!isset($attributes['no-label']))
    {{ Form::label($name, $label, ['class' => 'control-label']) }}
    @endif
    <input type="password" placeholder="Password" name="{{$name}}" class="form-control text-partner" value="{{$password ?? ''}}">

    <div class='invalid-tooltip'>
      Please enter a valid {{$name}}
    </div>
</div>
