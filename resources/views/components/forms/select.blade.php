<div class="form-group position-relative">
    {{ Form::label($label ?? $name, null, ['class' => 'control-label']) }}
    {{ Form::select($name, $values, $value ?? '', array_merge(['class' => 'custom-select'], $attributes ?? [])) }}

    <div class="invalid-tooltip">
        Please enter a required {{$label ?? $name}}
    </div>
</div>
