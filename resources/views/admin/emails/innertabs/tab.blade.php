<li class="nav-item">
    <a class="nav-link text-capitalize {{ $loop->first ? 'active' : '' }} px-3" id="pills-home-tab" data-toggle="pill" href="#{{ $key }}">
        <i class="fa fa-envelope"></i>
        {{ str_replace('_', ' ', $key) }}
    </a>
</li>