<div class="tab-pane fade {{ $loop->first ? 'show active' : '' }}" id="{{ $key }}" role="tabpanel" aria-labelledby="{{ $key }}-tab">
    <div class="row">
        <div class="col">
            <div class="form bg-light p-3">
                {!! Form::model($setting, ['route' => ['settings.update', $setting->id]]) !!}
                    <div class="form-group position-relative">
                        <label for="{{ $key }}" class="form-label text-capitalize">{{ str_replace('_', ' ', $key) }}</label>
                        <textarea name="{{ $key }}" id="{{ $key }}" cols="30" rows="30" class="form-control">{{ $e }}</textarea>
                    </div>

                    @method('patch')

                    <button class="btn btn-primary">
                        Update
                    </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
