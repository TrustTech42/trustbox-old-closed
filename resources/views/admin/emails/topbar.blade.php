<ul class="topbar nav nav-pills nav-fill mb-3" id="pills-tab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#emails">
            <i class="fa fa-envelope"></i>
            Emails
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link " id="pills-profile-tab" data-toggle="pill" href="#letters">
            <i class="fa fa-user"></i>
           Letters
        </a>
    </li>
</ul>