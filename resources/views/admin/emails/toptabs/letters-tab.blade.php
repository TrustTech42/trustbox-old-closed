<div class="tab-pane fade" id="letters" role="tabpanel" aria-labelledby="letters-tab">
    <div class="row">
        <ul class="topbar nav nav-pills nav-fill flex-nowrap mb-3" id="pills-tab" role="tablist">
            @foreach($letters as $key => $e)
                @include('admin.emails.innertabs.tab')
            @endforeach
        </ul>

        <div class="tab-content w-100" id="pills-tabContent">
            @foreach($letters as $key => $e)
                @include('admin.emails.innertabs.tab-content')
            @endforeach
        </div>
    </div>
</div>