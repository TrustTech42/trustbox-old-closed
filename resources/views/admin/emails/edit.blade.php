@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        @include('admin.emails.topbar')
        <div class="form bg-light light-rounded px-3 pb-3">

            <div class="tab-content" id="pills-tabContent">
                @include('admin.emails.toptabs.email-tab')
                @include('admin.emails.toptabs.letters-tab')
            </div>
        </div>
    </div>
@endsection
