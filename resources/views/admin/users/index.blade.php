@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        <div class="form bg-light p-3">
            <div class="row">
                <div class="col">
                    <h1>Accounts</h1>
                </div>
                <div class="col">
                    {!! Form::open(['route' => 'user.filter']) !!}
                    <div class="input-group mr-3">
                        @if(isset($filter_active))
                            <div class="input-group-prepend">
                                <a href="/admin/users" class="btn btn-primary" id="button-addon1"><i class="fa fa-sync-alt text-light"></i></a>
                            </div>
                        @endif
                        {{ Form::select('filter', $roles ?? [], null, ['class'=> 'form-control', 'aria-describedby' => 'button-addon1']) }}
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit" id="button-addon1"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="col">
                    {!! Form::open(['route' => 'user.search']) !!}
                        <div class="input-group mb-3">

                            @if(isset($search_active))
                                <div class="input-group-prepend">
                                    <a href="/admin/users" class="btn btn-primary" id="button-addon1"><i class="fa fa-sync-alt text-light"></i></a>
                                </div>
                            @endif

                            <input type="text" name="search" class="form-control" aria-label="Recipient's username" aria-describedby="button-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit" id="button-addon2"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
                <div class="col">
                    <a href="/admin/users/create" class="btn btn-primary float-right">
                        <i class="fa fa-plus-circle mr-2"></i>
                        Add New User
                    </a>
                </div>
            </div>

            <div class="table">
                <div class="row table-head">
                    <div class="col">Customer Number</div>
                    <div class="col">Name</div>
                    <div class="col">Type</div>
                    <div class="col">Email</div>
                    <div class="col">Plan</div>
                    <div class="col">Actions</div>
                </div>

                @foreach($users as $user)
                    <div class="row table-row">
                        <div class="col">{{$user->customer_number}}</div>
                        <div class="col">{{$user->name}}</div>
                        <div class="col">
                            @if($user->role->name === 'Provider')
                                {{$user->role->name}} {{ !is_null($user->provider) ? '('.$user->provider->name.')' : '' }}
                            @elseif($user->role->name === 'Partner')
                                <a href="" class="text-capitalize">{{$user->role->name}}{{ !is_null($user->partner) ? ' ('.$user->partner->name.')' : '' }}</a>
                            @else
                                {{$user->role->name}}
                            @endif
                        </div>
                        <div class="col">{{$user->email}}</div>
                        <div class="col text-capitalize">{{$user->plan}}</div>
                        <div class="col">
                            <a href="/admin/users/{{$user->id}}/edit" class="btn btn-primary btn-sm">
                                <i class="fas fa-eye"></i>
                            </a>
                            <a href="/users/{{$user->id}}/log_in_as" class="btn btn-primary btn-sm ml-2">
                                <i class="fas fa-key"></i>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="col-md">
                <a id="user-export" class="btn btn-primary float-right" href={{action('Admin\UserController@export')}}>
                    <i class="far fa-arrow-alt-circle-down mr-2"></i> Export as CSV
                </a>
            </div>
            <div class="row">
                <div class="col">
                    {{$users->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection
