@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        @include('admin.users.topbar')

        <div class="form bg-light p-3">

            <div class="tab-content" id="pills-tabContent">
                @include('admin.users.toptabs.edit-user')
                @include('admin.users.toptabs.changelog')
            </div>
        </div>
    </div>
@endsection
