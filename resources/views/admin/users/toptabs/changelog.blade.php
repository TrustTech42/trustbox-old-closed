<div class="tab-pane fade" id="changelog" role="tabpanel" aria-labelledby="changelog-tab">
    <div class="table">
        @foreach($changelogs as $changelog)
            <div class="row table-row">
                <div class="col-12">
                    {{ $changelog->action }}
                    <span class="float-right">{{ date('d/m/Y H:i:s', strtotime($changelog->created_at)) }}</span>
                </div>
            </div>
        @endforeach

        {{ $changelogs->fragment('changelog')->render() }}
    </div>
</div>