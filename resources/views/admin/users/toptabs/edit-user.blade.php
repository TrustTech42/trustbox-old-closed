<div class="tab-pane fade show active" id="edit-user" role="tabpanel" aria-labelledby="edit-user-tab">
    <h1>Edit Account - {{$user->name}}</h1>

    {!! Form::open(['route' => ['users.update', $user->id],  'enctype' => 'multipart/form-data', 'class' => 'needs-validation','novalidate']) !!}
    @method('patch')
    @include('admin.forms.users')
    <a href="/users/{{$user->id}}/log_in_as/" class="btn btn-secondary float-left mt-3">
        <i class="fa fa-key mr-2"></i>
        Log in as {{$user->name}}
    </a>

    <button class="btn btn-primary float-right mt-3">Update User</button>
    {!! Form::close() !!}

    <mark-deceased user="{{ json_encode($user) }}" url="{{ 'http://filemanager.'.env('APP_DOMAIN').'/api/user/'.$user->id.'/mark-as-deceased' }}"></mark-deceased>
</div>  
