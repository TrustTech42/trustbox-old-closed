@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        <div class="form bg-light p-3">
            <h1>New User Account</h1>

            {!! Form::open(['route' => 'users.store', 'class' => 'needs-validation','novalidate','enctype' => 'multipart/form-data',]) !!}
            @include('admin.forms.users')
            <button class="btn btn-primary float-right">Add User</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection