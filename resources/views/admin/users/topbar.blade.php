<ul class="topbar nav nav-pills nav-fill mb-3" id="pills-tab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#edit-user">
            <i class="fa fa-user"></i>
           Edit User
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#changelog">
            <i class="far fa-list-alt"></i>
            Changelog
        </a>
    </li>
</ul>