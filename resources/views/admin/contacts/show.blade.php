@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        <div class="form bg-light p-3">
            <h1 class="mb-2">Message From: <span class="text-capitalize">{{$contact->name}}</span></h1>
            <h3 class="mb-5">Contact Email: {{$contact->email}}</h3>

            <div class="row">
                <div class="col">
                    <label for="" class="form-label">Message Body</label>
                    <p>{{$contact->message}}</p>
                </div>
            </div>

            @if($contact->resolved)
                <div class="card rounded bg-partner p-3 text-light">
                    Resolved by {{ $contact->user->name }} on {{ date('d/m/Y H:i:s', strtotime($contact->updated_at)) }}
                </div>
            @endif

            @if(!$contact->resolved)


                <div class="row">
                    <div class="col">
                        {!! Form::open(['route' => ['contacts.update', $contact->id]]) !!}
                        @method('patch')
                        @csrf
                        <input type="hidden" name="resolved" value="1">
                        <button class="btn btn-primary float-right mt-5">Mark Resolved</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
