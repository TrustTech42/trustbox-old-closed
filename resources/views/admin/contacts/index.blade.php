@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        <div class="form bg-light p-3">
            <div class="row">
                <div class="col">
                    <h1 class="float-left">Messages</h1>
                </div>

                <div class="col">
                    {!! Form::open(['route' => 'contacts.filter']) !!}
                    <div class="input-group mr-3">
                        @if(isset($filter_active))
                            <div class="input-group-prepend">
                                <a href="/admin/contacts/" class="btn btn-primary" id="button-addon1"><i class="fa fa-sync-alt text-light"></i></a>
                            </div>
                        @endif
                        {{ Form::select('filter', [0 => 'Unresolved', 1 => 'Resolved'], $filter, ['class'=> 'form-control', 'aria-describedby' => 'button-addon1']) }}
                        {{ Form::select('filter2', App\Reason::all()->pluck('name', 'id'), $filter2, ['class'=> 'form-control', 'aria-describedby' => 'button-addon1']) }}
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit" id="button-addon1"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

            <div class="table">
                <div class="row table-head">
                    <div class="col">Partner</div>
                    <div class="col">Name</div>
                    <div class="col">Email</div>
                    <div class="col">Reason</div>
                    <div class="col">Resolved</div>
                    <div class="col">Actions</div>
                </div>

                @foreach($contacts as $contact)
                    <div class="row table-row">
                        <div class="col text-capitalize">{{$contact->partner->name}}</div>
                        <div class="col">{{$contact->name}}</div>
                        <div class="col">{{$contact->email}}</div>
                        <div class="col">{{$contact->reason->name}}</div>
                        <div class="col">{{$contact->resolved ? 'Yes' : 'No'}}</div>
                        <div class="col">
                            <a href="/admin/contacts/{{$contact->id}}" class="btn btn-primary btn-sm">
                                <i class="fas fa-eye"></i> View
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
