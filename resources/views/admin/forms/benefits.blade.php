<div class="row">
    <div class="col-md-4">
        {!! Form::bsText('name', $benefit->name ?? null, ['required']) !!}
    </div>
    <div class="col-md-4">
        @if(isset($benefit))
            <label for="types" class="form-label">Type</label>
            <select class="form-control" name="types" id="types">
                @foreach($benefit->types as $linkedType)
                    <option value="{{$linkedType->id}}">{{$linkedType->name}}</option>
                @endforeach
                @foreach($types->except($linkedType->id) as $type)
                    <option value="{{$type->id}}">{{$type->name}}</option>
                @endforeach
            </select>
        @else
            <label for="types" class="form-label">Type</label>
            <select class="form-control" name="types" id="types">
                @foreach($types as $type)
                    <option value="{{$type->id}}">{{$type->name}}</option>
                @endforeach
            </select>
        @endif
    </div>
    <div class="col-md-4">
        @if(isset($benefit))
            <label for="categories" class="form-label">Category</label>
            <select class="form-control" name="categories" id="categories">
                @foreach($benefit->categories as $linkedCategory)
                    <option value="{{$linkedCategory->id}}">{{$linkedCategory->name}}</option>
                @endforeach
                @foreach($categories->except($linkedCategory->id) as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
        @else
            <label for="categories" class="form-label">Categories</label>
            <select class="form-control" name="categories" id="categories">
                @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
        @endif
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="input-group mb-3">
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="thumbnail" name="thumbnail">
                <label class="custom-file-label" for="thumbnail">Choose thumbnail</label>
                <div class="invalid-tooltip">A Thumbnail is required</div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="input-group mb-3">
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="banner" name="banner">
                <label class="custom-file-label" for="banner">Choose banner</label>

                <div class="invalid-tooltip">A Banner is required</div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="form-group position-relative">
            <label for="description" class="form-label">Description</label>
            <textarea name="description" id="description" cols="30" rows="10" class="form-control"
                      required>{{$benefit->description ?? ''}}</textarea>

            <div class="invalid-tooltip">A description for the benefit is required.</div>
        </div>
    </div>
    <div class="col">
        <div class="form-group position-relative">
            <label for="boosted_description" class="form-label">Boosted Description</label>
            <textarea name="boosted_description" id="description" cols="30" rows="10" class="form-control"
                      required>{{$benefit->boosted_description ?? ''}}</textarea>

            <div class="invalid-tooltip">A description for the boosted benefit is required.</div>
        </div>
    </div>
</div>

<h3 class="h3 partner-title mb-3">
    Redemption Details
</h3>

<div class="row">
    <div class="col-6">
        <label for="redeem_text">Redemption Page</label>
        <textarea name="redeem_text" id="redeem_text" cols="30" rows="15"
                  class="form-control">{{$benefit->redeem_text ?? ''}}</textarea>
    </div>
    <div class="col-6">
{{--        <div class="custom-control custom-switch mb-3">--}}
{{--            <input type="hidden" name="featured" value="0">--}}
{{--            @if(isset($benefit) && $benefit->featured === "1")--}}
{{--                <input type="checkbox" class="custom-control-input" id="featured" name="featured" value="1" checked>--}}
{{--            @else--}}
{{--                <input type="checkbox" class="custom-control-input" id="featured" name="featured" value="1">--}}
{{--            @endif--}}
{{--            <label class="custom-control-label" for="featured">Featured Benefit</label>--}}
{{--        </div>--}}

        <label for="price mt-3">Boosted Price (£GBP)</label>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">
                    <i class="fa fa-pound-sign"></i>
                </span>
            </div>
            <input type="number" class="form-control" name="price" id="price" value="{{$benefit->price ?? ''}}">
        </div>

        <label for="percentage mt-3">Partner Split (%)</label>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">
                    <i class="fa fa-percent"></i>
                </span>
            </div>
            <input type="number" class="form-control" name="partner_percentage_cut" id="partner_percentage_cut"
                   value="{{$benefit->partner_percentage_cut ?? ''}}">
        </div>

        <label for="percentage mt-3">Provider Split (%)</label>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">
                    <i class="fa fa-percent"></i>
                </span>
            </div>
            <input type="number" class="form-control" name="provider_percentage_cut" id="provider_percentage_cut"
                   value="{{$benefit->provider_percentage_cut ?? ''}}">
        </div>

        <div class="form-group position-relative mt-3">
        </div>
    </div>
</div>

<h3 class="h3 partner-title mt-5">Provider Information</h3>
<div class="card my-3">
    <div class="card-body">
        <div class="row">
            <div class="col">
                <div class="form-group">
                    {!! Form::bsSelect('provider_id', $providers, $benefit->provider->id ?? '', []) !!}
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    {!! Form::bsEmail('provider_email', $benefit->provider_email ?? null, ['required']) !!}
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    {!! Form::bsText('provider_telephone', $benefit->provider_telephone ?? null, ['required']) !!}
                </div>
            </div>
            {{--            <div class="col">--}}
            {{--                <div class="custom-file">--}}
            {{--                    <input type="file" class="custom-file-input" id="provider_logo" name="provider_logo">--}}
            {{--                    <label class="custom-file-label" for="provider_logo">Provider Logo</label>--}}
            {{--                </div>--}}
            {{--            </div>--}}
        </div>
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="provider_description" class="form-label">Provider Description</label>
                    <textarea name="provider_description" id="provider_description" cols="30"
                              rows="10" class="form-control">{{$benefit->provider_description ?? ''}}</textarea>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="provider_usp" class="form-label">Why this Provider</label>
                    <textarea name="provider_usp" id="provider_usp" cols="30"
                              rows="10" class="form-control">{{$benefit->provider_usp ?? ''}}</textarea>
                </div>
            </div>
        </div>
    </div>
</div>
