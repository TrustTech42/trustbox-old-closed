{{--<div class="modal fade" id="testModal" tabindex="-1" role="dialog">--}}
{{--    <div class="modal-dialog modal-lg">--}}
{{--        <div class="modal-content">--}}
{{--            <div class="modal-body">--}}
{{--                @isset($partners)--}}
{{--                    <div class="form-group">--}}
{{--                        <label for="services" class="form-label">Partners allowd to view this post</label>--}}

{{--                        <ul class="list-group">--}}
{{--                            @foreach($partners as $partner)--}}
{{--                                <li class="list-group-item">--}}
{{--                                    {!! Form::bsSwitch('partners[' . $partner->id .']', $post->hasPartner($partner->id) ?? 0, $partner->name) !!}--}}
{{--                                </li>--}}
{{--                            @endforeach--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                @endisset--}}

{{--                <button class="btn btn-primary float-right" type="button" data-dismiss="modal">Done</button>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

<div class="row py-3">
    <div class="col">
        {!! Form::bsSwitch('enabled', $post->enabled ?? '', 'Publicly Visible') !!}
    </div>
</div>

<div class="row">
    <div class="col">
        {!! Form::bsText('title', $post->title ?? '', ['required']) !!}
    </div>
    <div class="col">
        <label for="banner" class="form-label">Post Banner</label>
        <div class="input-group mb-3">
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="banner" name="banner">
                <label class="custom-file-label" for="banner">Choose Banner</label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        @if(isset($post))
            <label for="categories" class="form-label">Category</label>
            <select class="form-control" name="categories" id="categories">
                @foreach($post->categories as $linkedCategory)
                    <option value="{{$linkedCategory->id}}">{{$linkedCategory->name}}</option>
                @endforeach
                @foreach($categories->except($linkedCategory->id) as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
        @else
            <label for="categories" class="form-label">Categories</label>
            <select class="form-control" name="categories" id="categories">
                @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
        @endif
    </div>
    <div class="col">
        {!! Form::bsText('headline', $post->headline ?? '', ['required']) !!}
    </div>
</div>
<div class="row">
    <div class="col">
        {!! Form::bsText('author', $post->author ?? '', ['required']) !!}
    </div>
    <div class="col">
        <label for="colour" class="form-label">Colour</label>
        <colour-dropdown current="{{$post->colour ?? '#000000'}}" name="colour"></colour-dropdown>
    </div>
</div>

<div class="form-group">
    <label class="form-label" for="snippet">Snippet</label>
    <textarea name="snippet" id="snippet" cols="3" rows="3"
              class="form-control">{!! $post->snippet ?? '' !!}</textarea>
</div>

<div class="form-group">
    <label class="form-label" for="content">Content</label>
    <textarea name="content" id="content" cols="30" rows="5"
              class="form-control">{!! $post->content ?? '' !!}</textarea>
</div>

{{--<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#testModal">Allowed Partners</button>--}}
