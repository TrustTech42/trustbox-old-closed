{!! Form::bsText('name', $service->name ?? null, ['required']) !!}

<div class="row">
    <div class="col">
        <div class="input-group mb-3">
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="thumbnail" name="thumbnail">
                <label class="custom-file-label" for="thumbnail">Choose thumbnail</label>

                <div class="invalid-tooltip">A Thumbnail is required</div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="input-group mb-3">
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="banner" name="banner">
                <label class="custom-file-label" for="banner">Choose banner</label>

                <div class="invalid-tooltip">A Banner is required</div>
            </div>
        </div>
    </div>
</div>

<div class="form-group position-relative">
    <label for="description" class="form-label">Description</label>
    <textarea name="description" id="description" cols="30" rows="10" class="form-control" required>{{$service->description ?? ''}}</textarea>

    <div class="invalid-tooltip">A description for the service is required.</div>
</div>

<div class="form-group position-relative">
    <label for="boosted_description" class="form-label">Boosted Description</label>
    <textarea name="boosted_description" id="description" cols="30" rows="10" class="form-control" required>{{$service->boosted_description ?? ''}}</textarea>

    <div class="invalid-tooltip">A description for the boosted service is required.</div>
</div>