<!-- File manager sub-domain -->
<h2 class="h5">File Manager</h2>
<div class="row">
    <div class="col-6">
        <label for="fm_subdomain" class="form-label">File Manager Subdomain</label>
        <div class="input-group mb-3">
            <input type="text" class="form-control" name="fm_subdomain" value="{{$settings->first()->fm_subdomain}}" readonly>
            <div class="input-group-append">
                <span class="input-group-text">.{{env('APP_DOMAIN')}}</span>
            </div>
        </div>
    </div>
</div>

<h2 class="h5">Invite Limits</h2>
<!-- Invite limits -->
<div class="row">
    <div class="col-auto">
        <div class="form-group">
            <label for="household_limit" class="form-label">Household Limit</label>
            <input type="text" class="form-control" name="household_limit" value="{{$settings->first()->household_limit}}">
        </div>
    </div>

    <div class="col-auto">
        <div class="form-group">
            <label for="gifted_limit" class="form-label">Gifted Limit</label>
            <input type="text" class="form-control" name="gifted_limit" value="{{$settings->first()->gifted_limit}}">
        </div>
    </div>

    <div class="col-auto">
        <div class="form-group">
            <label for="trusted_limit" class="form-label">Trusted Limit</label>
            <input type="text" class="form-control" name="trusted_limit" value="{{$settings->first()->trusted_limit}}" readonly>
        </div>
    </div>
</div>


<h2 class="h5">Storage Options</h2>
<!-- Storage settings -->
<div class="row">
    <div class="col-3">
        <label for="default_storage" class="form-label">Default Storage Amount</label>
        <div class="input-group mb-3">
            <input type="text" class="form-control" name="default_storage" value="{{$settings->first()->default_storage}}" readonly>
            <div class="input-group-append">
                <span class="input-group-text">GB</span>
            </div>
        </div>
    </div>
    <div class="col-3">
        <label for="upgraded_storage" class="form-label">Upgraded Storage Amount</label>
        <div class="input-group mb-3">
            <input type="text" class="form-control" name="upgraded_storage" value="{{$settings->first()->upgraded_storage}}" readonly>
            <div class="input-group-append">
                <span class="input-group-text">GB</span>
            </div>
        </div>
    </div>
</div>