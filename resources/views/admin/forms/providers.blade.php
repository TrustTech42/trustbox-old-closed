<h3 class="h3 partner-title mt-5">Provider Information</h3>
<div class="card my-3">
    <div class="card-body">
        <div class="row">
            <div class="col">
                {!! Form::bsText('name', $provider->name ?? null, ['required']) !!}
            </div>
            <div class="col">
                {!! Form::bsText('nochex_id', $provider->nochex_id ?? null, ['required']) !!}
            </div>
            <div class="col">
                <div class="custom-file mt-4">
                    <input type="file" class="custom-file-input" id="provider_logo" name="provider_logo">
                    <label class="custom-file-label" for="provider_logo">Provider Logo</label>
                </div>
            </div>
            {{--    <div class="col">--}}
            {{--        @isset($provider->user_id)--}}
            {{--            {!! Form::bsSelect('user_id', $users, $provider->user_id ?? null, ['disabled']) !!}--}}
            {{--        @endisset--}}
            {{--    </div>--}}
        </div>
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    {!! Form::bsEmail('email', $provider->email ?? null, ['required']) !!}
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    {!! Form::bsText('telephone', $provider->telephone ?? null, ['required']) !!}
                </div>
            </div>
        </div>
    </div>
</div>