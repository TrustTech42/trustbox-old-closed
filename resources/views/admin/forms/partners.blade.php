<h2 class="h2 partner-title mb-3">Partner Details</h2>

<div class="row mb-3">
    <div class="col">
        <h3 class="h3 partner-title mb-3">Upload Logo</h3>

        <logo-changer current="{{$partner->logo ?? null}}"
                      header_mode="{{$partner->header_mode ?? null}}"></logo-changer>
    </div>
    <div class="col">
        <h3 class="h3 partner-title mb-3">Set Brand Colours</h3>

        <header-dropdown current="{{$partner->header_mode ?? 'light'}}" class="mb-3"></header-dropdown>
        <colour-dropdown current="{{$partner->primary_colour ?? '#000000'}}" name="primary_colour"></colour-dropdown>
        <colour-dropdown current="{{$partner->secondary_colour ?? '#000000'}}"
                         name="secondary_colour"></colour-dropdown>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label for="name" class="form-label">Company Name</label>
            <input type="text" class="form-control" name="name" value="{{$partner->name ?? null}}">
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="subdomain" class="form-label">Trustbox Subdomain</label>
            <input type="text" class="form-control" name="subdomain" value="{{$partner->subdomain ?? null}}">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label for="address1" class="form-label">House No / Name</label>
            <input type="text" class="form-control" name="address1" value="{{$partner->address1 ?? null}}">
        </div>
        <div class="form-group">
            <label for="city" class="form-label">City</label>
            <input type="text" class="form-control" name="city" value="{{$partner->city ?? null}}">
        </div>
        <div class="form-group">
            <label for="contact_phone" class="form-label">Contact Telephone (or adopted)</label>
            <input type="text" class="form-control" name="contact_phone" value="{{$partner->contact_phone ?? null}}">
        </div>
        <div class="form-group">
            <label for="description" class="form-label">Description</label>
            <textarea class="form-control" name="description">
                {{ $partner->description ?? null }}
            </textarea>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="address2" class="form-label">Address 2</label>
            <input type="text" class="form-control" name="address2" value="{{$partner->address2 ?? null}}">
        </div>
        <div class="form-group">
            <label for="postcode" class="form-label">Postcode</label>
            <input type="text" class="form-control" name="postcode" value="{{$partner->postcode ?? null}}">
        </div>
        <div class="form-group">
            <label for="contact_email" class="form-label">Email Address</label>
            <input type="text" class="form-control" name="contact_email" value="{{$partner->contact_email ?? null}}">
        </div>
        <div class="form-group">
            <label for="nochex_id" class="form-label">NoChex ID</label>
            <input type="text" class="form-control" name="nochex_id" value="{{$partner->nochex_id ?? null}}">
        </div>
    </div>
</div>
<h3 class="h3 partner-title mt-5 mb-4">Invite Information</h3>
<div class="form-group">
    <label for="code_invite_content" class="form-label">Code Invite Content</label>
    <textarea class="form-control" name="code_invite_content">
                {{ $partner->code_invite_content ?? null }}
            </textarea>
</div>
<div class="form-group">
    <label for="invite_content" class="form-label">User Invite Content</label>
    <textarea class="form-control" name="invite_content">
                {{ $partner->invite_content ?? null }}
            </textarea>
</div>
