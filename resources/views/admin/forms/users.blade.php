<!-- login information -->

@include('shared.error')

<div class="card mb-3">
    <div class="card-body">
        <h2 class="h5">Login Information</h2>

        <div class="row">
            <!-- email -->
            @if(isset($user) && $user->email === 'admin@trustbox.com')
                <div class="col-12">{!! Form::bsEmail('email', $user->email ?? null, ['required', 'disabled']) !!}</div>
            @else
                <div class="col-12">{!! Form::bsEmail('email', $user->email ?? null, ['required']) !!}</div>
            @endif
        </div>

        <!-- password confirmation -->
        <password/>
    </div>
</div>

<!-- general details -->
<div class="card mb-3">
    <div class="card-body">
        <h2 class="h4">General Information</h2>

        <div class="row">
            <div class="col">{!! Form::bsText('first_name', $user->first_name ?? null, ['required']) !!}</div>
            <div class="col">{!! Form::bsText('last_name', $user->last_name ?? null, ['required']) !!}</div>
        </div>
    </div>
</div>

<!-- address information -->
<div class="card mb-3">
    <div class="card-body">
        <h2 class="h4">Address Information</h2>

        <div class="row">
            <div class="col-6">{!! Form::bsText('address1', $user->address1 ?? null, ['required', 'label' => 'Address Line 1']) !!}</div>
            <div class="col-6">{!! Form::bsText('address2', $user->address2 ?? null, ['label' => 'Address Line 2']) !!}</div>
            <div class="col-6">{!! Form::bsText('city', $user->city ?? null, ['required']) !!}</div>
            <div class="col-6">{!! Form::bsText('postcode', $user->postcode ?? null, ['required']) !!}</div>
            <div class="col-6">{!! Form::bsText('telephone', $user->telephone ?? null, ['required']) !!}</div>

            <div class="col-6">
                <div class="form-group">
                    <label for="date_of_birth" class="form-label">Date of Birth</label>
                    <input type="date" class="form-control" name="date_of_birth" id="date_of_birth" value="{{$user->date_of_birth ?? null}}">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- user type and permissions -->
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col">
                <h2 class="h5">User Type / Permissions </h2>
            </div>
            <div class="col">
                <div class="float-right">
                    {!! Form::bsSwitch('activated', $user->membership->activated ?? null, 'Account Activated') !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                @if(isset($user) && $user->email === 'admin@trustbox.com')
                    <span class="text-danger">Cannot change the plan of the admin@trustbox.com account.</span>
                @else
                    <plan-selector plans="{{$plans}}" plan="{{$user->membership->plan->id ?? null}}" users="{{$users}}" user="{{$user->id ?? null}}" partners="{{$partners}}" partner="{{$user->membership->partner->id ?? null}}"></plan-selector>
                @endif
            </div>
            <div class="col">
                <role-selector selected="{{$user->role->id ?? null}}" roles="{{$roles}}" partners="{{$partners}}" partner="{{$user->partner->id ?? null}}" providers="{{$providers}}" email="{{$user->email ?? null}}" mobile="{{ $user->mobile ?? null }}"></role-selector>
            </div>
        </div>
    </div>
</div>

@if(isset($user))
    @if($user->role->name === "Staff")
        <!-- staff permissions -->
        <h2 class="h5">Staff Permissions </h2>
        <div class="card mt-3">
            <div class="card-body card-body-scroll">
                <div class="row d-flex flex-column">
                        <div class="col mb-3">
                            <!-- Dashboard -->
                            <div class="custom-control custom-switch d-inline mr-3">
                                <input type="checkbox" class="custom-control-input" id="permission[dashboard]" name="permission[dashboard]" checked>
                                <label class="custom-control-label" for="permission[dashboard]" {{$user->permissions()->where('name', 'dashboard')->count() > 0 ? 'checked' : null}}>Dashboard</label>
                            </div>
                        </div>

                        <div class="col mb-3">
                            <!-- Partners -->
                            <div class="custom-control custom-switch d-inline mr-3">
                                <input type="checkbox" class="custom-control-input" id="permission[partners]" name="permission[partners]" {{$user->permissions()->where('name', 'partners')->count() > 0  ? 'checked' : null}}>
                                <label class="custom-control-label" for="permission[partners]">Partners</label>
                            </div>
                        </div>

                        <div class="col mb-3">
                            <!-- Benefits -->
                            <div class="custom-control custom-switch d-inline mr-3">
                                <input type="checkbox" class="custom-control-input" id="permission[benefits]" name="permission[benefits]" {{$user->permissions()->where('name', 'benefits')->count() > 0  ? 'checked' : null}}>
                                <label class="custom-control-label" for="permission[benefits]">Benefits</label>
                            </div>
                        </div>

                        <div class="col mb-3">
                            <!-- Partner Benefits -->
                            <div class="custom-control custom-switch d-inline mr-3">
                                <input type="checkbox" class="custom-control-input" id="permission[ubenefits]" name="permission[ubenefits]" {{$user->permissions()->where('name', 'ubenefits')->count() > 0  ? 'checked' : null}}>
                                <label class="custom-control-label" for="permission[ubenefits]">Partner Benefits</label>
                            </div>
                        </div>

                        <div class="col mb-3">
                            <!-- Posts -->
                            <div class="custom-control custom-switch d-inline mr-3">
                                <input type="checkbox" class="custom-control-input" id="permission[posts]" name="permission[posts]" {{$user->permissions()->where('name', 'posts')->count() > 0  ? 'checked' : null}}>
                                <label class="custom-control-label" for="permission[posts]">Posts</label>
                            </div>
                        </div>

                        <div class="col mb-3">
                            <!-- Settings -->
                            <div class="custom-control custom-switch d-inline mr-3">
                                <input type="checkbox" class="custom-control-input" id="permission[settings]" name="permission[settings]" {{$user->permissions()->where('name', 'settings')->count() > 0  ? 'checked' : null}}>
                                <label class="custom-control-label" for="permission[settings]">Settings</label>
                            </div>
                        </div>

                        <div class="col mb-3">
                            <!-- Pages -->
                            <div class="custom-control custom-switch d-inline mr-3">
                                <input type="checkbox" class="custom-control-input" id="permission[pages]" name="permission[pages]" {{$user->permissions()->where('name', 'pages')->count() > 0  ? 'checked' : null}}>
                                <label class="custom-control-label" for="permission[pages]">Pages</label>
                            </div>
                        </div>

                        <div class="col mb-3">
                            <!-- Messages -->
                            <div class="custom-control custom-switch d-inline mr-3">
                                <input type="checkbox" class="custom-control-input" id="permission[messages]" name="permission[messages]" {{$user->permissions()->where('name', 'messages')->count() > 0  ? 'checked' : null}}>
                                <label class="custom-control-label" for="permission[messages]">Messages</label>
                            </div>
                        </div>

                        <div class="col mb-3">
                            <!-- Accounts -->
                            <div class="custom-control custom-switch d-inline mr-3">
                                <input type="checkbox" class="custom-control-input" id="permission[accounts]" name="permission[accounts]" {{$user->permissions()->where('name', 'accounts')->count() > 0  ? 'checked' : null}}>
                                <label class="custom-control-label" for="permission[accounts]">Accounts</label>
                            </div>
                        </div>

                        <div class="col mb-3">
                            <!-- Providers -->
                            <div class="custom-control custom-switch d-inline mr-3">
                                <input type="checkbox" class="custom-control-input" id="permission[providers]" name="permission[providers]" {{$user->permissions()->where('name', 'providers')->count() > 0  ? 'checked' : null}}>
                                <label class="custom-control-label" for="permission[providers]">Providers</label>
                            </div>
                        </div>

                        <div class="col mb-3">
                            <!-- Requests -->
                            <div class="custom-control custom-switch d-inline mr-3">
                                <input type="checkbox" class="custom-control-input" id="permission[requests]" name="permission[requests]" {{$user->permissions()->where('name', 'requests')->count() > 0  ? 'checked' : null}}>
                                <label class="custom-control-label" for="permission[requests]">Requests</label>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    @endif
@endif
<!--
- Full
- Partner
- Household
- Gifted
- Trusted
- Staff
- Admin
-->
