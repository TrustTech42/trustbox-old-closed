<ul class="topbar nav nav-pills nav-fill mb-3" id="pills-tab" role="tablist">
    <li class="nav-item">
        <a class="nav-link {{ $type == 'invites' ? 'active' : '' }}" id="pills-home-tab" data-toggle="pill" href="#invites">
            <i class="fa fa-envelope"></i>
            Invites
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ $type == 'email' ? 'active' : '' }}" id="pills-profile-tab" data-toggle="pill" href="#email">
            <i class="fa fa-user"></i>
            Email Invites
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ $type == 'mail' ? 'active' : '' }}" id="pills-contact-tab" data-toggle="pill" href="#mail">
            <i class="fa fa-user"></i>
            Mail Invites
        </a>
    </li>
</ul>