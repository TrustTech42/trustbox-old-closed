@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        @include('admin.invites.topbar')

        <a href="{{action('Admin\InvitationsController@download')}}" class="btn btn-primary mb-3">Download template</a>
        <div class="form bg-light light-rounded p-3">

            <div class="tab-content" id="pills-tabContent">
                @include('admin.invites.toptabs.invites')
                @include('admin.invites.toptabs.email-invite')
                @include('admin.invites.toptabs.mail-invite')
            </div>
        </div>
    </div>
@endsection
