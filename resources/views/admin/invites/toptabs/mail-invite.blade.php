<div class="tab-pane fade {{ $type == 'mail' ? 'show active' : '' }}" id="mail" role="tabpanel" aria-labelledby="mails-tab">
    <div class="row">
        <div class="col">
            <h1>Invitations</h1>
        </div>
        <div class="col">
            {!! Form::open(['route' => 'invitations.filter.providers']) !!}
            <div class="input-group mr-3">
                @if(isset($filter_providers_active))
                    <div class="input-group-prepend">
                        <a href="/admin/invitations#mail" class="btn btn-primary" id="button-addon1"><i class="fa fa-sync-alt text-light"></i></a>
                    </div>
                @endif
                {{ Form::select('filter', $providers, null, ['class'=> 'form-control', 'aria-describedby' => 'button-addon1']) }}
                <div class="input-group-append">
                    <button class="btn btn-primary" type="submit" id="button-addon1"><i class="fa fa-search"></i></button>
                </div>

            </div>
            <input type="hidden" name="type" value="mail">
            {!! Form::close() !!}
        </div>

        <div class="col">
            {!! Form::open(['route' => 'invitations.filter']) !!}
            <div class="input-group mr-3">
                @if(isset($filter_active))
                    <div class="input-group-prepend">
                        <a href="/admin/invitations#mail" class="btn btn-primary" id="button-addon1"><i class="fa fa-sync-alt text-light"></i></a>
                    </div>
                @endif
                {{ Form::select('filter', ['sent' => 'Sent', 'pending' => 'Pending','confirmed' => 'Confirmed'], null, ['class'=> 'form-control', 'aria-describedby' => 'button-addon1']) }}
                <div class="input-group-append">
                    <button class="btn btn-primary" type="submit" id="button-addon1"><i class="fa fa-search"></i></button>
                </div>
            </div>
            <input type="hidden" name="type" value="mail">
            {!! Form::close() !!}
        </div>
        <div class="col">
            {!! Form::open(['route' => 'invitations.search']) !!}
            <div class="input-group mb-3">

                @if(isset($search_active))
                    <div class="input-group-prepend">
                        <a href="/admin/invitations#mail" class="btn btn-primary" id="button-addon1"><i class="fa fa-sync-alt text-light"></i></a>
                    </div>
                @endif

                <input type="text" name="search" class="form-control" aria-label="Recipient's username" aria-describedby="button-addon2">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="submit" id="button-addon2"><i class="fa fa-search"></i></button>
                </div>
            </div>
            <input type="hidden" name="type" value="mail">
            {!! Form::close() !!}
        </div>
        <div class="col">
            <button class="btn btn-primary float-right" onclick="document.getElementById('check-form-mail').submit()">
                <i class="fas fa-fw fa-print mr-2"></i> Print Invitations
            </button>
        </div>
    </div>

    {!! Form::open(['method' => 'post', 'route' => 'invitations.print', 'id' => 'check-form-mail']) !!}
        <div class="table">
            <div class="row table-head">
                <div class="col">Name</div>
                <div class="col">Email</div>
                <div class="col">Status</div>
                <div class="col">Partner</div>
                <div class="col-1">
                    <input type="checkbox" name="check_all" id="check_all">
                </div>
            </div>

            @foreach($mail_invites as $invite)
                <div class="row table-row">
                    <div class="col">{{$invite->name}}</div>
                    <div class="col">{{$invite->email}}</div>
                    <div class="col">{{$invite->status}}</div>
                    <div class="col">{{$invite->partner}}</div>
                    <div class="col-1">
                        <input type="checkbox" name="fields[check][]" value="{{ $invite->id }}" class="checked">
                    </div>
                </div>
            @endforeach
        </div>
    {!! Form::close() !!}
</div>