<div class="tab-pane fade {{ $type == 'invites' ? 'show active' : '' }}" id="invites" role="tabpanel" aria-labelledby="invites-tab">
    <h1>Invite Users</h1>

    <div class="row">
        <div class="col-6 mx-auto">

            {!! Form::open(['route' => ['invitations.store'], 'enctype' => 'multipart/form-data']) !!}

            <div class="form-group">
                {!! Form::bsSelect('type', ['email' => 'Email', 'mail' => 'Mail'], null, ['class' => 'form-control']) !!}
            </div>

            <div class="input-group mb-3">
                <div class="custom-file">
                    <input type="file" name="csv" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" accept=".csv">
                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                </div>
            </div>
            <button class="btn btn-primary float-right">
                Submit
            </button>
            {!! Form::close() !!}
        </div>
    </div>
</div>
