@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        <div class="form bg-light p-3">
            <h1 class="float-left">Services List</h1>

            <div class="table-actions float-right">
                <a href='/admin/services/create/' class="btn btn-primary ml-auto">
                    <i class="fas fa-fw fa-plus-circle mr-2"></i> Add New Service
                </a>
            </div>

            <div class="table">
                <div class="row table-head">
                    <div class="col">ID</div>
                    <div class="col">Name</div>
                    <div class="col">Actions</div>
                </div>

                @foreach($services as $service)
                    <div class="row table-row">
                        <div class="col">{{$service->id}}</div>
                        <div class="col">{{$service->name}}</div>
                        <div class="col">
                            <a href="/admin/services/{{$service->id}}/edit" class="btn btn-primary btn-sm">
                                <i class="fas fa-edit"></i> Edit
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection