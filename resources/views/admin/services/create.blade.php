@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        <div class="form bg-light p-3">
            <h1>New Service</h1>

            {!! Form::open(['route' => 'services.store', 'class' => 'needs-validation','novalidate','enctype' => 'multipart/form-data',]) !!}
            @include('admin.forms.services')
            <button class="btn btn-primary float-right">Add Service</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection