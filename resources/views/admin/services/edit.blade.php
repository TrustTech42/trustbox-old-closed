@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        <div class="form bg-light p-3">
            <h1>Edit Service - {{$service->name}}</h1>

            {!! Form::open(['route' => ['services.update', $service->id],  'enctype' => 'multipart/form-data', 'class' => 'needs-validation','novalidate']) !!}
            @method('patch')
            @include('admin.forms.services')
            <button class="btn btn-primary float-right">Update Service</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection