@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        <div class="form bg-light p-3">
            <h1>New Post</h1>

            {!! Form::open(['route' => 'posts.store', 'class' => 'needs-validation','novalidate','enctype' => 'multipart/form-data',]) !!}
            <input type="hidden" name="is_global" value="1">
            @include('admin.forms.posts')
            <button class="btn btn-primary float-right">Add Post</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
