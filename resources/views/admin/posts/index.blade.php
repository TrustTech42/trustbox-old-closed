@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        <div class="form bg-light p-3">
            <h1 class="float-left">Posts List</h1>

            <div class="table-actions float-right">
                <a href='/admin/posts/create/' class="btn btn-primary ml-auto">
                    <i class="fas fa-fw fa-plus-circle mr-2"></i> Add New Post
                </a>
            </div>

            <div class="table">
                <div class="row table-head">
                    <div class="col-1">ID</div>
                    <div class="col">Title</div>
                    <div class="col">Snippet</div>
                    <div class="col-2">Actions</div>
                </div>

                @foreach($posts as $post)
                    <div class="row table-row">
                        <div class="col-1">{{$post->id}}</div>
                        <div class="col">{{$post->title}}</div>
                        <div class="col">{!! Str::words($post->content, '10')!!}</div>
                        <div class="col-2">
                            <a href="/admin/posts/{{$post->id}}/edit" class="btn btn-primary btn-sm">
                                <i class="fas fa-edit"></i> Edit
                            </a>
                            {!! Form::open(['route' => ['posts.destroy', $post->id], 'class' => 'd-inline']) !!}
                            @csrf
                            @method('delete')

                            <button class="btn btn-danger btn-sm">
                                <i class="fa fa-trash"></i>
                            </button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        {{ $posts->links() }}
    </div>
@endsection
