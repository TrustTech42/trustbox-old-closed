@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        <div class="form bg-light p-3">
            <h1>Edit Post - {{$post->id}}</h1>

            {!! Form::open(['route' => ['posts.update', $post->id],  'enctype' => 'multipart/form-data', 'class' => 'needs-validation','novalidate']) !!}
            @method('patch')
            @include('admin.forms.posts')
            <button class="btn btn-primary float-right mb-4">Update Post</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
