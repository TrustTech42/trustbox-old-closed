@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        @include('admin.partners.topbar')
        <div class="form bg-light light-rounded p-3">

            {!! Form::open(['route' => ['partners.update', $partner->id],  'enctype' => 'multipart/form-data', 'class' => 'needs-validation','novalidate']) !!}
            @method('patch')

            <div class="tab-content" id="pills-tabContent">
                @include('admin.partners.toptabs.edit-partner')
                @include('admin.partners.toptabs.edit-partner-benefits')
                @include('admin.partners.toptabs.list-users')
            </div>

            <div class="row">
                <div class="col">
                    <button class="btn btn-primary float-right">Update Partner</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
