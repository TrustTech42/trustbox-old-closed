@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        <div class="form bg-light p-3">
            <div class="row">

                <div class="col">
                    <h1>Partners List ({{$partners->count()}})</h1>
                </div>
                <div class="col">
                    {!! Form::open(['route' => 'partner.search']) !!}
                    <div class="input-group mb-3">

                        @if(isset($search_active))
                            <div class="input-group-prepend">
                                <a href="/admin/partners" class="btn btn-primary" id="button-addon1"><i class="fa fa-sync-alt text-light"></i></a>
                            </div>
                        @endif

                        <input type="text" name="search" class="form-control" aria-label="Recipient's username" aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit" id="button-addon2"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="col">
                    <div class="table-actions">
                        <a href='/admin/partners/create/' class="btn btn-primary ml-auto">
                            <i class="fas fa-fw fa-plus-circle mr-2"></i>
                            Add New Partner
                        </a>
                    </div>
                </div>
            </div>

            <div class="table">
                <div class="row table-head">
                    <div class="col">ID</div>
                    <div class="col">Name</div>
                    <div class="col">Colour Scheme</div>
                    <div class="col">Active</div>
                    <div class="col">Actions</div>
                </div>

                @foreach($partners as $partner)
                    <div class="row table-row">
                        <div class="col">{{$partner->id}}</div>
                        <div class="col">{{$partner->name}}</div>
                        <div class="col">
                            <span class="colour-swatch" style="background-color: {{$partner->primary_colour}};"></span>
                            <span class="colour-swatch" style="background-color: {{$partner->header_mode === 'dark' ? '#000000' : '#eeeeee'}};"></span>
                        </div>
                        <div class="col">{{$partner->active ? 'Yes' : 'No'}}</div>
                        <div class="col">
                            @if($partner->id != $defaultPartner)
                                <a href="/admin/partners/{{$partner->id}}/edit" class="btn btn-primary btn-sm">
                                    <i class="fas fa-edit"></i>
                                </a>

                                {!! Form::open(['route' => ['partners.destroy', $partner->id], 'class' => 'd-inline delete-partner']) !!}
                                @method('delete')
                                @csrf
                                <button class="btn btn-danger ml-2 btn-sm" type="button" data-toggle="modal" data-target=".modal-confirm">
                                    <i class="fa fa-trash"></i>
                                </button>
                                {!! Form::close() !!}
                            @else
                                <span class="badge badge-warning p-3 float-right">Default Partner</span>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <confirm form=".delete-partner"></confirm>
@endsection