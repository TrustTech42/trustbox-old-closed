@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        <div class="form bg-light p-3">
            <h1>New Partner</h1>

            {!! Form::open(['route' => 'partners.store', 'class' => 'needs-validation','novalidate','enctype' => 'multipart/form-data',]) !!}
            @include('admin.forms.partners')
            <button class="btn btn-primary float-right">Add Partner</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection