<ul class="topbar nav nav-pills nav-fill mb-3" id="pills-tab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#edit-partner">
            <i class="fa fa-user"></i>
            Partner Details
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#edit-partner-benefits">
            <i class="fa fa-user"></i>
            Benefits
        </a>
    </li>
{{--    <li class="nav-item">--}}
{{--        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#edit-partner-services" >--}}
{{--            <i class="fa fa-user"></i>--}}
{{--            Partner Services--}}
{{--        </a>--}}
{{--    </li>--}}
    <li class="nav-item">
        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#list-users" >
            <i class="fa fa-user"></i>
            Partner Users
        </a>
    </li>
</ul>
