<div class="tab-pane fade" id="list-users" role="tabpanel">
    <h3 class="h3 partner-title">Partner Members ({{$partner->members->count()}})</h3>

    <div class="table">
        <div class="row table-head">
            <div class="col">Name</div>
            <div class="col">Type</div>
            <div class="col">Email</div>
            <div class="col">Plan</div>
            <div class="col">Actions</div>
        </div>
        @foreach($partner->members as $user)
            <div class="row table-row">
                <div class="col">{{$user->first_name}} {{$user->last_name}}</div>
                <div class="col">{{$user->role->name}}</div>
                <div class="col">{{$user->email}}</div>
                <div class="col text-capitalize">{{$user->plan}}</div>
                <div class="col">
                    <a href="/admin/users/{{$user->id}}/edit" class="btn btn-primary btn-sm">
                        <i class="fas fa-eye"></i>
                    </a>
                    <a href="/users/{{$user->id}}/log_in_as" class="btn btn-primary btn-sm ml-2">
                        <i class="fas fa-key"></i>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
</div>