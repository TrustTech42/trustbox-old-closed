<div class="tab-pane fade" id="edit-partner-services" role="tabpanel" aria-labelledby="edit-partner-services-tab">
    @isset($services)
        @isset($services)
            <h2 class="h2 partner-title mb-3">Partner Services</h2>

            @foreach($services as $service)
                <div class="row align-items-center">
                    <div class="col-8">
                        <h3 class="h3">{{$service->name}}</h3>
                        <p>{{\Illuminate\Support\Str::words($service->description, 20, '...')}}</p>
                    </div>
                    <div class="col-4 d-flex justify-content-center">
                        {!! Form::bsSwitch('services[' . $service->id .']', $partner->hasService($service->id), 'Enabled / Disabled') !!}
                    </div>
                </div>

                <hr>
            @endforeach
        @endisset
    @endisset
</div>