<div class="tab-pane fade" id="edit-partner-benefits" role="tabpanel" aria-labelledby="edit-partner-benefits-tab">
    @isset($benefits)
        <h2 class="h2 partner-title mb-3">Core Benefits</h2>

        @foreach($benefits as $benefit)
            <div class="row align-items-center">
                <div class="col-8">
                    <h3 class="h3">{{$benefit->name}}</h3>
                    <p>{{\Illuminate\Support\Str::words($benefit->description, 20, '...')}}</p>
                </div>
                <div class="col-4 d-flex justify-content-center">
                    {!! Form::bsSwitch('benefits[' . $benefit->id .']', $partner->hasBenefit($benefit->id), 'Enabled / Disabled') !!}
                </div>
            </div>
            <hr>
        @endforeach
    @endisset

    @if($unique_benefits)
        <h2 class="h2 partner-title mb-3">Partner Benefits</h2>

        @foreach($unique_benefits as $u_benefit)
            <div class="row align-items-center">
                <div class="col-8">
                    <h3 class="h3">{{$u_benefit->name}}</h3>
                    <p>{{\Illuminate\Support\Str::words($u_benefit->description, 20, '...')}}</p>
                </div>
                <div class="col-4 d-flex justify-content-center">
                    {!! Form::bsSwitch('benefits[' . $u_benefit->id .']', $partner->hasBenefit($u_benefit->id), 'Enabled / Disabled') !!}
                </div>
            </div>
            <hr>
        @endforeach
    @endif
</div>