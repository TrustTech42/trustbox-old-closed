@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        <div class="form bg-light p-3">
            <h1>Edit Benefit - {{$benefit->name}}</h1>


            @if($unique)
                {!! Form::open(['route' => ['ubenefits.update', $benefit->id],  'enctype' => 'multipart/form-data', 'class' => 'needs-validation','novalidate']) !!}
            @else
                {!! Form::open(['route' => ['benefits.update', $benefit->id],  'enctype' => 'multipart/form-data', 'class' => 'needs-validation','novalidate']) !!}
            @endif
            @method('patch')
            @include('admin.forms.benefits')

            @if($unique)
                <h3 class="h3 partner-title mt-5">Attached Partners</h3>
                <div class="card my-3">
                    <div class="card-body card-body-scroll">
                        <div class="row d-flex flex-column">

                            @foreach($partners as $partner)
                                <div class="col mb-3">
                                    <input type="hidden" name="partners[{{$partner->id}}]" value="off">
                                    @if(in_array($partner->name, $benefit_partners->toArray()))
                                        <div class="custom-control custom-switch d-inline mr-5">
                                            <input type="checkbox" class="custom-control-input"
                                                   id="partner{{$partner->id}}" name="partners[{{$partner->id}}]"
                                                   value="on" checked>
                                            <label class="custom-control-label"
                                                   for="partner{{$partner->id}}">{{$partner->name}}</label>
                                        </div>
                                    @else
                                        <div class="custom-control custom-switch d-inline mr-5">
                                            <input type="checkbox" class="custom-control-input"
                                                   id="partner{{$partner->id}}" name="partners[{{$partner->id}}]"
                                                   value="on">
                                            <label class="custom-control-label"
                                                   for="partner{{$partner->id}}">{{$partner->name}}</label>
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <h3 class="h3 partner-title mt-5">Filing Cabinet</h3>
                <div class="card my-3">
                    <div class="card-body card-body-scroll">
                        <div class="row d-flex flex-column">
                            <div class="custom-control custom-switch d-inline mr-5">
                                <input type="hidden" name="includes_file_manager" value="0">
                                <input type="checkbox" class="custom-control-input" id="includes_file_manager"
                                       name="includes_file_manager"
                                       value="1" {{$benefit->includes_file_manager ? 'checked' : null}}>
                                <label class="custom-control-label"
                                       for="includes_file_manager">Has File Manager</label>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <h3 class="h3 partner-title mt-5">Benefit Contact Options</h3>
            <div class="card my-3">
                <div class="card-body card-body-scroll">
                    <div class="row d-flex flex-column">
                        <div class="custom-control custom-switch d-inline mr-5">
                            <input type="hidden" name="includes_benefit_form" value="0">
                            <input type="checkbox" class="custom-control-input" id="includes_benefit_form"
                                   name="includes_benefit_form"
                                   value="1" {{$benefit->includes_benefit_form ? 'checked' : null}}>
                            <label class="custom-control-label"
                                   for="includes_benefit_form">Has Benefit Form</label>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-scroll">
                    <div class="row d-flex flex-column">
                        <div class="custom-control custom-switch d-inline mr-5">
                            <input type="hidden" name="includes_benefit_phone_button" value="0">
                            <input type="checkbox" class="custom-control-input" id="includes_benefit_phone_button"
                                   name="includes_benefit_phone_button"
                                   value="1" {{$benefit->includes_benefit_phone_button ? 'checked' : null}}>
                            <label class="custom-control-label"
                                   for="includes_benefit_phone_button">Has Benefit Phone Button</label>
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-primary float-right">Update Benefit</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
