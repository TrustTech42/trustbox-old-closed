@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        <div class="form bg-light p-3">
            <h1>New Benefit</h1>

            {!! Form::open(['route' => 'benefits.store', 'class' => 'needs-validation','novalidate','enctype' => 'multipart/form-data',]) !!}
            @include('admin.forms.benefits')

            @if($unique)
                <input type="hidden" name="unique" value="1">
            @endif

            <button class="btn btn-primary float-right">Add Benefit</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection