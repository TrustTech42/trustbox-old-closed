@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        <div class="form bg-light p-3">
            <div class="row mb-4">
                <div class="col">
                    @if($unique)
                        <h1 class="float-left">Unique Partner Benefits</h1>
                    @else
                        <h1 class="float-left">Benefits for all Partners</h1>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    @if($unique)
                        {!! Form::open(['route' => 'ubenefits.filter.types']) !!}
                    @else
                        {!! Form::open(['route' => 'benefits.filter.types']) !!}
                    @endif
                    <div class="input-group mr-3">
                        <div class="input-group-prepend">
                            @if($unique)
                                <a href="/admin/ubenefits/" class="btn btn-primary" id="button-addon1"><i
                                            class="fa fa-sync-alt text-light"></i></a>
                            @else
                                <a href="/admin/benefits/" class="btn btn-primary" id="button-addon1"><i
                                            class="fa fa-sync-alt text-light"></i></a>
                            @endif
                        </div>
                        {{ Form::select('filter', $types, null, ['class'=> 'form-control', 'aria-describedby' => 'button-addon1']) }}
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit" id="button-addon1"><i
                                        class="fa fa-search"></i></button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="col-md-3">
                    @if($unique)
                        {!! Form::open(['route' => 'ubenefits.filter.categories']) !!}
                    @else
                        {!! Form::open(['route' => 'benefits.filter.categories']) !!}
                    @endif
                    <div class="input-group mr-3">
                        <div class="input-group-prepend">
                            @if($unique)
                                <a href="/admin/ubenefits/" class="btn btn-primary" id="button-addon1"><i
                                            class="fa fa-sync-alt text-light"></i></a>
                            @else
                                <a href="/admin/benefits/" class="btn btn-primary" id="button-addon1"><i
                                            class="fa fa-sync-alt text-light"></i></a>
                            @endif
                        </div>
                        {{ Form::select('filter', $categories, null, ['class'=> 'form-control', 'aria-describedby' => 'button-addon1']) }}
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit" id="button-addon1"><i
                                        class="fa fa-search"></i></button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="col-md-4">
                    @if($unique)
                        {!! Form::open(['route' => 'ubenefits.search'])!!}
                    @else
                        {!! Form::open(['route' => 'benefits.search'])!!}
                    @endif
                    <div class="input-group mb-3">
                        <input type="text" name="search" class="form-control" aria-label="Benefits name"
                               aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit" id="button-addon2"><i
                                        class="fa fa-search"></i></button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="table-actions float-right">
                    <a href='/admin/benefits/create/{{ isset($unique) ? '?u='.$unique : '' }}'
                       class="btn btn-primary ml-auto">
                        <i class="fas fa-fw fa-plus-circle mr-2"></i> Add New Benefit
                    </a>
                </div>
            </div>

            <div class="table">
                <div class="row table-head">
                    <div class="col">ID</div>
                    <div class="col">Name</div>
                    <div class="col">Type</div>
                    <div class="col">Category</div>
                    <div class="col">Actions</div>
                </div>
                @foreach($benefits as $benefit)
                    <div class="row table-row">
                        <div class="col">{{$benefit->id}}</div>
                        <div class="col">{{$benefit->name}}</div>
                        @foreach($benefit->types as $type)
                            <div class="col">{{$type->name}}</div>
                        @endforeach
                        @foreach($benefit->categories as $category)
                            <div class="col">{{$category->name}}</div>
                        @endforeach
                        <div class="col">
                            @if($benefit->unique)
                                <a href="/admin/ubenefits/{{$benefit->id}}/edit" class="btn btn-primary btn-sm">
                                    <i class="fas fa-edit"></i>
                                </a>
                            @else
                                <a href="/admin/benefits/{{$benefit->id}}/edit" class="btn btn-primary btn-sm">
                                    <i class="fas fa-edit"></i>
                                </a>
                            @endif

                            {!! Form::open(['route' => ['benefits.destroy', $benefit->id], 'class' => 'd-inline']) !!}
                            @csrf
                            @method('delete')

                            <button class="btn btn-danger btn-sm">
                                <i class="fa fa-trash"></i>
                            </button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    </div>
@endsection