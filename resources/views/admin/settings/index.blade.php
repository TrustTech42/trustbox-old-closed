@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        <div class="form bg-light p-3">
            <h1>Edit Settings</h1>

            {!! Form::open(['route' => ['settings.mass_update'], 'class' => 'needs-validation','novalidate']) !!}
            @include('admin.forms.settings')
            <button class="btn btn-primary float-right">Update Settings</button>
            {!! Form::close() !!}
        </div>

        <div class="form bg-light p-3 mt-3">
            <h2 class="h2">Edit Contact Reasons</h2>
            <div class="row">
                <div class="col-6">
                    <h3 class="h3">Add New</h3>
                    {!! Form::open(['route' => ['reasons.store'], 'method' => 'post', 'class' => 'needs-validation','novalidate']) !!}
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Reason Text', 'required']) !!}
                        <button class="btn btn-primary btn-sm mt-3 float-right">Add Reason</button>
                    {!! Form::close() !!}
                </div>
                <div class="col-6">
                    <h3 class="h3">Existing Reasons</h3>
                    {!! Form::open(['route' => ['reasons.mass_update'],'method' => 'post', 'class' => 'needs-validation','novalidate']) !!}
                        {!! Form::select('reasons[]', $reasons, $visibleReasons, ['class' => 'custom-select', 'multiple']) !!}
                        <small class="text-danger">Highlighted reasons are visible. To select or deselect multiple reasons, crtl+click.</small>
                        <button class="btn btn-primary btn-sm mt-3 float-right">Update Visibility</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
