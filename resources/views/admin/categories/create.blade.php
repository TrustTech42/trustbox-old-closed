@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        <div class="form bg-light p-3">
            <h1>New Category</h1>

            {!! Form::open(['route' => 'categories.store', 'class' => 'needs-validation','novalidate','enctype' => 'multipart/form-data',]) !!}
            @include('admin.forms.categories')
            <button class="btn btn-primary float-right">Add Category</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
