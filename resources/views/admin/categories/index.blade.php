@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        <div class="form bg-light p-3">
            <h1 class="float-left">Categories List</h1>
            <div class="table-actions float-right">
                <a href='/admin/categories/create/' class="btn btn-primary ml-auto">
                    <i class="fas fa-fw fa-plus-circle mr-2"></i> Add New Category
                </a>
            </div>

            <div class="table" style="display: inline-table">
                <div class="row table-head">
                    <div class="col">ID</div>
                    <div class="col">Name</div>
                    <div class="col">Linked Benefits</div>
                    <div class="col">Action</div>
                </div>

                @foreach($categories as $category)
                    <div class="row table-row">
                        <div class="col">{{$category->id}}</div>
                        <div class="col">{{$category->name}}</div>
                        <div class="col">{{$category->benefits->count()}}</div>
                        <div class="col">
                            @if($category->benefits()->count() == 0 && $category->name != 'None')
                                {!! Form::open(['route' => ['categories.destroy', $category->id], 'class' => 'd-inline']) !!}
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger btn-sm">
                                    <i class="fa fa-trash"></i>
                                </button>
                                {!! Form::close() !!}
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
