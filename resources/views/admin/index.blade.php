@extends("layouts.admin")
@section("content")
    <div class="container p-3">
        <div class="row">
            <div class="col">
                <div class="rounded bg-light p-3">
                    <h3 class="h3 partner-title">Total Members</h3>

                    {{ $members }}

                </div>
            </div>
            <div class="col">
                <div class="rounded bg-light p-3">
                    <h3 class="h3 partner-title">Total Partners</h3>

                    {{ $partners }}
                </div>
            </div>
            <div class="col">
                <div class="rounded bg-light p-3">
                    <h3 class="h3 partner-title">Sign Up Revenue</h3>
                </div>
            </div>
            <div class="col">
                <div class="rounded bg-light p-3">
                    <h3 class="h3 partner-title">Monthly Boost Revenue</h3>

                    {{ '£'.number_format($total, 2) }}
                </div>
            </div>
        </div>
    </div>
@endsection