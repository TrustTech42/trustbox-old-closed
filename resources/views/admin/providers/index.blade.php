@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        <div class="form bg-light p-3">
            <div class="row">
                <div class="col">
                    <h1>Providers List</h1>
                </div>

                <div class="col">
                    {!! Form::open(['route' => 'provider.search']) !!}
                    <div class="input-group mb-3">

                        @if(isset($search_active))
                            <div class="input-group-prepend">
                                <a href="/admin/providers" class="btn btn-primary" id="button-addon1"><i
                                            class="fa fa-sync-alt text-light"></i></a>
                            </div>
                        @endif

                        <input type="text" name="search" class="form-control" aria-label="Recipient's username"
                               aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit" id="button-addon2"><i
                                        class="fa fa-search"></i></button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

                <div class="col">
                    <div class="table-actions">
                        <a href='/admin/providers/create/' class="btn btn-primary ml-auto">
                            <i class="fas fa-fw fa-plus-circle mr-2"></i>
                            Add New Provider
                        </a>
                    </div>
                </div>
            </div>

            <div class="table">
                <div class="row table-head">
                    <div class="col">ID</div>
                    <div class="col">Name</div>
                    <div class="col">User</div>
                    <div class="col"></div>
                </div>

                @foreach($providers as $provider)
                    <div class="row table-row">
                        <div class="col">{{$provider->id}}</div>
                        <div class="col">{{$provider->name}}</div>
                        @if($provider->user)
                            <div class="col">
                                <a href=/admin/users/{{$provider->user->id}}/edit>{{$provider->user->name}}
                                    ({{$provider->user->email}})</a>
                            </div>
                        @else
                            <div class="col">No user currently assigned.</div>
                        @endif
                        <div class="col">
                            {!! Form::open(['route' => ['providers.edit', $provider->id]]) !!}
                            @method('get')
                            @csrf
                            <button class="btn btn-primary btn-sm float-left text-white">
                                <i class="fas fa-edit"></i> Edit
                            </button>
                            {!! Form::close() !!}
                            {!! Form::open(['route' => ['providers.destroy', $provider->id]]) !!}
                            @method('delete')
                            @csrf

                            @if($provider->id !== 1)
                                <button class="btn btn-danger btn-sm float-right ">
                                    <i class="fa fa-trash mr-2"></i>
                                    Delete Provider
                                </button>

                            @else
                                <span class="badge badge-warning p-3 float-right">Default Provider</span>
                            @endif
                            {!! Form::close() !!}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection