@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        <div class="form bg-light p-3">
            <h1>Edit Provider</h1>
            {!! Form::open(['route' => ['providers.update', $provider->id], 'class' => 'needs-validation','novalidate','enctype' => 'multipart/form-data',]) !!}
            @include('admin.forms.providers')
            @method('patch')
            <button class="btn btn-primary float-right">Update Provider</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection