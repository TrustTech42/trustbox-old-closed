@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        <div class="form bg-light p-3">
            <h1>New Provider</h1>

            {!! Form::open(['route' => 'providers.store', 'class' => 'needs-validation','novalidate','enctype' => 'multipart/form-data',]) !!}
            @include('admin.forms.providers')
            <button class="btn btn-primary float-right">Add Provider</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection