@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        <div class="form bg-light p-3">
            <div class="row">
                <div class="col">
                    <h1 class="float-left">Requests</h1>
                </div>

                <div class="col">
                    {!! Form::open(['route' => 'requests.filter']) !!}
                    <div class="input-group mr-3">
                        @if(isset($filter_active))
                            <div class="input-group-prepend">
                                <a href="/admin/requests/" class="btn btn-primary" id="button-addon1"><i class="fa fa-sync-alt text-light"></i></a>
                            </div>
                        @endif
                        {{ Form::select('filter', [null => 'Pending', 0 => 'Denied', 1 => 'Approved'], null, ['class'=> 'form-control', 'aria-describedby' => 'button-addon1']) }}
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit" id="button-addon1"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

            <div class="table">
                <div class="row table-head">
                    <div class="col">Partner</div>
                    <div class="col">Type</div>
                    <div class="col">ID</div>
                    <div class="col">Action</div>
                    <div class="col">Status</div>
                    <div class="col"></div>
                </div>

                @foreach($requests as $request)
                    <div class="row table-row">
                        <div class="col">{{$request->partner->name}}</div>
                        <div class="col">{{$request->type}} @if($request->type === 'benefit') ({{\App\Benefit::find($request->entity_id)->name}}) @endif</div>
                        <div class="col">{{$request->entity_id}}</div>
                        <div class="col">{{$request->action}}</div>
                        <div class="col">
                            @if($request->status !== null)
                                @if($request->status == 1)
                                    <span class="badge badge-success">Approved</span>
                                @else
                                    <span class="badge badge-danger">Denied</span>
                                @endif
                            @else
                                Pending
                            @endif
                        </div>
                        <div class="col">
                            @if($request->status === null)
                                {!! Form::open(['route' => ['requests.update', $request->id], 'class' => 'd-inline']) !!}
                                @method('patch')
                                @csrf
                                <input type="hidden" name="status" value="0">
                                <button class="btn btn-danger btn-sm">
                                    <i class="fa fa-times"></i>
                                </button>
                                {!! Form::close() !!}

                                {!! Form::open(['route' => ['requests.update', $request->id], 'class' => 'd-inline']) !!}
                                @method('patch')
                                @csrf
                                <input type="hidden" name="status" value="1">
                                <button class="btn btn-success btn-sm">
                                    <i class="fa fa-check"></i>
                                </button>
                                {!! Form::close() !!}
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
