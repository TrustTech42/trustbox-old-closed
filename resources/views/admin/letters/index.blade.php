@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        <div class="form bg-light p-3">

                <div class="row">

                    <div class="col-2">
                        <h1>Letters</h1>
                    </div>

                    <div class="col">
                        {!! Form::open(['route' => 'letter.filter.providers']) !!}
                        <div class="input-group mr-3">
                            @if(isset($filter_providers_active))
                                <div class="input-group-prepend">
                                    <a href="/admin/letters/" class="btn btn-primary" id="button-addon1"><i class="fa fa-sync-alt text-light"></i></a>
                                </div>
                            @endif
                            {{ Form::select('filter', $providers, null, ['class'=> 'form-control', 'aria-describedby' => 'button-addon1']) }}
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit" id="button-addon1"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>

                    <div class="col">
                        {!! Form::open(['route' => 'letter.filter']) !!}
                        <div class="input-group mr-3">
                            @if(isset($filter_active))
                                <div class="input-group-prepend">
                                    <a href="/admin/letters/" class="btn btn-primary" id="button-addon1"><i class="fa fa-sync-alt text-light"></i></a>
                                </div>
                            @endif
                            {{ Form::select('filter', [0 => 'Not Printed', 1 => 'Printed'], null, ['class'=> 'form-control', 'aria-describedby' => 'button-addon1']) }}
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit" id="button-addon1"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>

                    <div class="col">
                        {!! Form::open(['route' => 'letter.search']) !!}
                        <div class="input-group">
                            @if(isset($search_active))
                                <div class="input-group-prepend">
                                    <a href="/admin/letters/" class="btn btn-primary" id="button-addon1"><i class="fa fa-sync-alt text-light"></i></a>
                                </div>
                            @endif

                            <input type="text" name="search" class="form-control" aria-label="Recipient's username" aria-describedby="button-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit" id="button-addon2"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>

                    <div class="col">
                        <button class="btn btn-primary float-right" onclick="document.getElementById('check-form').submit()">
                            <i class="fas fa-fw fa-print mr-2"></i> Print Letters
                        </button>
                    </div>
                </div>
            {!! Form::open(['method' => 'post', 'route' => 'letter.print', 'id' => 'check-form']) !!}
                <div class="table">
                    <div class="row table-head">
                        <div class="col">Name</div>
                        <div class="col">Printed</div>
                        <div class="col">Partner</div>
                        <div class="col-1">
                            <input type="checkbox" name="check_all" id="check_all">
                        </div>
                    </div>

                    @foreach($users as $user)
                        <div class="row table-row">
                            <div class="col">{{$user->name}}</div>
                            <div class="col">{{ $user->letter_printed ? 'Yes' : 'No' }}</div>
                            <div class="col">{{ $user->membership()->exists() ? $user->membership->partner->name : '' }}</div>
                            <div class="col-1">
                                <input type="checkbox" name="fields[check][]" value="{{ $user->id }}" class="checked">
                            </div>
                        </div>
                    @endforeach
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection