@extends('layouts.admin')
@section('content')
<div class="container p-3">
    <div class="form bg-light p-3">
        <h1 class="float-left">Pages List</h1>

        <div class="table-actions float-right">
            <a href='/admin/pages/create/' class="btn btn-primary ml-auto">
                <i class="fas fa-fw fa-plus-circle mr-2"></i> Create New Page
            </a>
        </div>

        <div class="table">
            <div class="row table-head">
                <div class="col">ID</div>
                <div class="col">Name</div>
                <div class="col">Template</div>
                <div class="col">Enabled</div>
                <div class="col">Blocks</div>
                <div class="col">Actions</div>
            </div>

            @foreach($pages as $page)
                <div class="row table-row">
                    <div class="col">{{$page->id}}</div>
                    <div class="col">{{$page->name}}</div>
                    <div class="col">{{$page->template}}</div>
                    <div class="col">{{$page->enabled === '1' ? 'Enabled' : 'Disabled'}}</div>
                    <div class="col">{{$page->blocks()->count()}}</div>
                    <div class="col">
                        <a class="btn btn-primary btn-sm" href="/admin/pages/{{$page->id}}/edit">
                            <i class="fas fa-edit"></i> Edit
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
