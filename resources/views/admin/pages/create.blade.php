@extends('layouts.admin')
@section('content')
    <div class="container p-3">
        <div class="form bg-light p-3">
            <h1>Create a Page</h1>

            {!! Form::open(['route' => 'pages.store']) !!}

                {!! Form::bsText('name', null, ['required']) !!}
                {!! Form::bsText('url', null, ['required']) !!}
                {!! Form::bsSelect('template', ['default' => 'Default', 'benefit' => 'Benefit'], ['required']) !!}

                {!! Form::submit('Submit', ['class' => 'btn btn-primary float-right']) !!}

            {!! Form::close() !!}
        </div>
    </div>
@endsection
