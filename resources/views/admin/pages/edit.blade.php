@extends('layouts.admin')
@section('content')
    <div class="container-fluid has-page-builder py-3">
        <div class="form p-3">
            <div class="row">
                <div class="col-12 bg-light p-3 mb-3">
                    <h1>Edit Page</h1>
                </div>
            </div>

            <page-builder page="{{$page->id}}" data="{{$page->blocks}}" description="{{ $global_partner->description }}" template="{{ $page->template }}" benefits="{{ $benefits }}" cur_benefit="{{ $page->benefit()->exists() ? $page->benefit()->first()->id : false }}"/>
        </div>
    </div>
@endsection
