<div class="nav admin nav-pills">
    @if(auth()->user()->permissions()->where('name', 'dashboard')->count() || auth()->user()->role->name === 'Admin')
        <a href="/admin" class="nav-link">
            <i class="fas fa-fw fa-columns"></i>
            Dashboard
        </a>
    @endif

    @if(auth()->user()->permissions()->where('name', 'partners')->count() || auth()->user()->role->name === 'Admin')
        <a href="/admin/partners" class="nav-link">
            <i class="fas fa-fw fa-handshake"></i>
            Partners
        </a>
    @endif

    @if(auth()->user()->permissions()->where('name', 'benefits')->count() || auth()->user()->role->name === 'Admin')
        <a href="/admin/benefits" class="nav-link">
            <i class="fas fa-fw fa-star"></i>
            Core Benefits
        </a>
    @endif

    @if(auth()->user()->permissions()->where('name', 'ubenefits')->count() || auth()->user()->role->name === 'Admin')
        <a href="/admin/ubenefits" class="nav-link">
            <i class="fas fa-fw fa-ballot-check"></i>
            Partner Benefits
        </a>
    @endif

    @if(auth()->user()->permissions()->where('name', 'posts')->count() || auth()->user()->role->name === 'Admin')
        <a href="/admin/posts" class="nav-link">
            <i class="fas fa-fw fa-newspaper"></i>
            Posts
        </a>
    @endif

    @if(auth()->user()->permissions()->where('name', 'settings')->count() || auth()->user()->role->name === 'Admin')
        <a href="/admin/settings" class="nav-link">
            <i class="fas fa-fw fa-cog"></i>
            Settings
        </a>
    @endif

    @if(auth()->user()->permissions()->where('name', 'pages')->count() || auth()->user()->role->name === 'Admin')
        <a href="/admin/pages" class="nav-link">
            <i class="fas fa-fw fa-copy"></i>
            Pages
        </a>
    @endif

    @if(auth()->user()->permissions()->where('name', 'messages')->count() || auth()->user()->role->name === 'Admin')
        <a href="/admin/contacts" class="nav-link">
            <i class="fas fa-fw fa-comments"></i>
            Messages
        </a>
    @endif

    @if(auth()->user()->permissions()->where('name', 'accounts')->count() || auth()->user()->role->name === 'Admin')
        <a href="/admin/settings/{{ App\Setting::find(1)->id }}/edit" class="nav-link">
            <i class="fa fa-fw fa-envelope"></i>
            Emails
        </a>
    @endif

    @if(auth()->user()->permissions()->where('name', 'accounts')->count() || auth()->user()->role->name === 'Admin')
        <a href="/admin/users" class="nav-link">
            <i class="fas fa-fw fa-users"></i>
            Accounts
        </a>
    @endif

    @if(auth()->user()->permissions()->where('name', 'accounts')->count() || auth()->user()->role->name === 'Admin')
        <a href="/admin/letters" class="nav-link">
            <i class="fas fa-fw fa-envelope-open"></i>
            Letters
        </a>
    @endif

    @if(auth()->user()->permissions()->where('name', 'accounts')->count() || auth()->user()->role->name === 'Admin')
        <a href="/admin/invitations" class="nav-link">
            <i class="fas fa-fw fa-hands-heart"></i>
            Invites
        </a>
    @endif

    @if(auth()->user()->permissions()->where('name', 'providers')->count() || auth()->user()->role->name === 'Admin')
        <a href="/admin/providers" class="nav-link">
            <i class="fas fa-fw fa-id-badge"></i>
            Providers
        </a>
    @endif

    @if(auth()->user()->permissions()->where('name', 'requests')->count() || auth()->user()->role->name === 'Admin')
        <a href="/admin/requests" class="nav-link">
            <i class="fas fa-fw fa-business-time"></i>
            Requests
        </a>
    @endif
    @if(auth()->user()->role->name === 'Admin')
        <a href="/admin/categories" class="nav-link">
            <i class="fas fa-fw fa-list"></i>
            Categories
        </a>
    @endif
</div>
