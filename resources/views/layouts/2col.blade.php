<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <title>TrustBox</title>
    <script src="{{ mix('js/frontend.js') }}" defer></script>
    <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=o3fahonheiz88ukamd2d9j0wzh16av24lldteufyy6wsuzoc"></script>
    <script>
        tinyMCE.init({
            mode  : "textareas",
            height: '300px',
            plugins: "link lists ",
            toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |",
            link_class_list: [
                {title: 'Normal', value: ''},
                {title: 'Button', value: 'btn btn-primary'},
            ],
            relative_urls : false,
            remove_script_host : false,
            document_base_url : "https://www.yourtrustbox.co.uk/",
            selector: "textarea",
            setup: function (editor) {
                editor.on('change', function () {
                    tinymce.triggerSave();
                });
            }
        });
    </script>
    <link rel="shortcut icon" href="{{ asset('images/favicon.jpg') }}">
    <link href="{{ mix('css/frontend.css') }}" rel="stylesheet">

@yield('header_scripts')

<!-- Dynamic stylesheet -->
    @include('frontend.partials.dynamic_stylesheet')
</head>
<body>
<style>
    .ie-warning {
        display : none;
    }
    /*------Specific style for IE11---------*/
    _:-ms-fullscreen, :root
    .ie-warning {
        width            : 100%;
        height           : 100%;
        background-color : rgba(0, 0, 0, 0.7);
        display          : block;
        position         : fixed;
        top              : 0;
        left             : 0;
        z-index          : 9999999999999999999;
    }
    .ie-warning .inner {
        color         : white;
        margin        : 0;
        position      : absolute;
        font-size     : 30px;
        top           : 50%;
        left          : 50%;
        text-align    : center;
        -ms-transform : translate(-50%, -50%);
        transform     : translate(-50%, -50%);
    }
</style>
<div class="ie-warning">
    <div class="inner">
        Please use another browser to view our website, this browser is outdated. We recommend Google Chrome, Microsoft Edge or Mozilla Firefox
    </div>
</div>

@include('shared.error')
@include('shared.success')

@include('frontend.partials.html.header')
<div id="app">
    <div class="container py-5">
        <div class="row">
            <div class="col-2">
                <div id="sidebar">

                    @yield('sidebar')

{{--                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">--}}
{{--                    </div>--}}

                    <a href="#" class="btn btn-primary btn-block my-3 mb-md-0# mt-md-3 logout-button" onclick="document.getElementById('logout_form').submit()">Logout</a>
                    <form id="logout_form" method="POST" action="/logout">
                        @csrf
                    </form>
                </div>
            </div>
            <div class="col-10">
                @yield("content")
            </div>
        </div>
    </div>
</div>
@include('frontend.partials.html.footer')

<script src="{{asset('js/includes/shared/other.js')}}" defer></script>

@yield('scripts')
</body>
</html>
