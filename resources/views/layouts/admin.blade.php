<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>TrustBox - Admin</title>
    <script src="{{ mix('js/admin.js') }}" defer></script>
    <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=o3fahonheiz88ukamd2d9j0wzh16av24lldteufyy6wsuzoc"></script>
    <script>
        tinyMCE.init({
            mode  : "textareas",
            height: '300px',
            plugins: "link lists ",
            toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |",
            link_class_list: [
                {title: 'Normal', value: ''},
                {title: 'Button', value: 'btn btn-primary'},
            ],
            relative_urls : false,
            remove_script_host : false,
            document_base_url : "https://www.yourtrustbox.co.uk/"
        });
    </script>
    <link href="{{ mix('css/admin.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('images/favicon.jpg') }}">

    <!-- Dynamic stylesheet -->
    @include('frontend.partials.dynamic_stylesheet')
</head>
<body>
@include('shared.success')
@include('shared.error')

<div id="app">
    @include('frontend.partials.html.header')
    <div class="container px-0 py-5">
        <div class="row">
            <div class="col-2">
                <div id="sidebar" class="mt-3">
                    @include('admin.partials.sidebar')
                </div>

                <a href="#" class="btn btn-primary btn-block mt-3" onclick="document.getElementById('logout_form').submit()">Logout</a>
                <form id="logout_form" method="POST" action="/logout">
                    @csrf
                </form>

                <a href="/" class="btn btn-link btn-block">Back to Main Website</a>
            </div>
            <div id="content" class="col-10">
                @yield("content")
            </div>
        </div>
    </div>
    @include('frontend.partials.html.footer')
</div>
</body>
</html>
