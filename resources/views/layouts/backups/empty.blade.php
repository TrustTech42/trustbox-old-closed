<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>TrustBox</title>
    <script src="{{ mix('js/frontend.js') }}" defer></script>
    <link rel="shortcut icon" href="{{ asset('images/favicon.jpg') }}">
    <link href="{{ mix('css/frontend.css') }}" rel="stylesheet">

@yield('header_scripts')

<!-- Dynamic stylesheet -->
    @include('frontend.partials.dynamic_stylesheet')
</head>
<body>

@include('shared.error')
@include('shared.success')

<div id="app">
    @include('frontend.partials.html.header')
    @yield('content')

    <div class="mb-5">
        @include('frontend.partials.html.questions')
    </div>
    @include('frontend.partials.html.footer')
</div>

<script src="{{asset('js/includes/shared/other.js')}}" defer></script>

@yield('scripts')
</body>
</html>
