<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Trustbox - Admin</title>
    <script src="{{ mix('js/admin.js') }}" defer></script>
    <link href="{{ mix('css/admin.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('images/favicon.jpg') }}">

    <!-- Dynamic stylesheet -->
    @include('frontend.partials.dynamic_stylesheet')
</head>
<body>
@include('shared.success')
@include('shared.error')

<div id="app" class="admin-app">
    <div class="container h-100 d-flex flex-column justify-content-center">
        @yield("content")
    </div>
</div>
</body>
</html>
