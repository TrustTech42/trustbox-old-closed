{{--
@if(!isset($header_footer))
    @php $header_footer = true; @endphp
@endif
--}}

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <script src="{{ mix('js/frontend.js') }}" defer></script>
    <link href="{{ mix('css/frontend.css') }}" rel="stylesheet">

    <!-- Dynamic stylesheet -->
    @include('frontend.partials.dynamic_stylesheet')
</head>
{{--<body class="{{$global_partner->header_mode != 'dark' && !$header_footer ?: 'bg-dark text-white'}}">--}}
<body class="{{$global_partner->header_mode != 'dark' ?: 'bg-dark text-white'}}">
<div id="app" class="fixed">
{{--    @if($header_footer)--}}
    @include('frontend.partials.html.header')
{{--    @endif--}}
    <div class="content">
        @yield("content")
    </div>
    {{--@if($header_footer)--}}
    @include('frontend.partials.html.footer')
    {{--@endif--}}
</div>
</body>
</html>
