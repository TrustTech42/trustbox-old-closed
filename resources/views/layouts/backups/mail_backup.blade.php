<!DOCTYPE html>
<html>
<head>
    @include('mail.styles')

</head>
​
@if(!isset($partner))
    @php($partner = isset($vars->partner) ? $vars->partner : $global_partner)
@endif

<body>

<table style="margin:0 auto; width: 100%;" cellspacing="0" cellpadding="0" width="100%" bgcolor="#004354">
    <tr>
        <td ytyle="text-align: center; padding: 25px;">
            <a href="#"><img class="w320 img-fluid" width="200" src="{{ $message->embed('images/email-logo.jpg') }}" alt="Trustbox" style="margin-left: 15px; max-width: 200px;" ></a>
        </td>
    </tr>
</table>

@yield('content')
​
<table style="margin:0 auto; width: 100%;" cellspacing="0" cellpadding="0" bgcolor="#004354" width="100%"  class="force-full-width">
    <tr>
        <td ytyle="text-align: center; padding: 25px;">
            <a href="#"><img class="w320 img-fluid" width="200" src="{{ $message->embed('images/email-logo.jpg') }}" alt="Trustbox" style="margin-left: 15px; max-width: 250px;" ></a>
        </td>
    </tr>
    <tr>
        <td style="color:#858585; font-size: 14px; text-align:center; padding-bottom:45px;">
            Copyright © 2020 TrustBox LTD Registered in England, Registration number: 11437803
        </td>
    </tr>
</table>
</body>
</html>
