<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>TrustBox</title>
    <script src="{{ mix('js/frontend.js') }}" defer></script>
    <link rel="shortcut icon" href="{{ asset('images/favicon.jpg') }}">
    <link href="{{ mix('css/frontend.css') }}" rel="stylesheet">

    @yield('header_scripts')

    <!-- Dynamic stylesheet -->
    @include('frontend.partials.dynamic_stylesheet')
</head>
<body>
<style>
    .ie-warning {
        display : none;
    }
    /*------Specific style for IE11---------*/
    _:-ms-fullscreen, :root
    .ie-warning {
        width            : 100%;
        height           : 100%;
        background-color : rgba(0, 0, 0, 0.7);
        display          : block;
        position         : fixed;
        top              : 0;
        left             : 0;
        z-index          : 9999999999999999999;
    }
    .ie-warning .inner {
        color         : white;
        margin        : 0;
        position      : absolute;
        font-size     : 30px;
        top           : 50%;
        left          : 50%;
        text-align    : center;
        -ms-transform : translate(-50%, -50%);
        transform     : translate(-50%, -50%);
    }
</style>
<div class="ie-warning">
    <div class="inner">
        Please use another browser to view our website, this browser is outdated. We recommend Google Chrome, Microsoft Edge or Mozilla Firefox
    </div>
</div>
@include('shared.error')
@include('shared.success')

<div id="app">
    @include('frontend.partials.html.header')
    @include('frontend.partials.html.banner-login')
    @yield('content')

    @include('frontend.partials.html.footer')
</div>

<script src="{{asset('js/includes/shared/other.js')}}" defer></script>

@yield('scripts')
</body>
</html>
