<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>TrustBox - Dashboard</title>
    <script src="{{ mix('js/frontend.js') }}" defer></script>
    <link href="{{ mix('css/frontend.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('images/favicon.jpg') }}">

    <!-- Dynamic stylesheet -->
    @include('frontend.partials.dynamic_stylesheet')
</head>
<body>

<div id="app">
    @include('frontend.partials.html.header')

    @include('shared.error')
    @include('shared.success')

    <div class="container py-5 account-dashboard-title-top">
        <div class="row">
            <div class="col-12 text-center mb-5 account-dashboard-title-bottom">
                @yield('top')
            </div>

            <div class="col-md-2">
                <div id="sidebar">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        @yield('sidebar')
                    </div>
                </div>

                <a href="#" class="btn btn-primary btn-block my-3 mb-md-0# mt-md-3 logout-button" onclick="document.getElementById('logout_form').submit()">Logout</a>
                <form id="logout_form" method="POST" action="/logout">
                    @csrf
                </form>
            </div>
            <div class="col-md-10">
                <div id="user-main">
                    @yield("content")
                </div>
            </div>
        </div>
    </div>

    @include('frontend.partials.html.footer')
</div>
<script src="{{ asset('js/includes/shared/other.js') }}" defer></script>
</body>
</html>
