<!DOCTYPE html>
<html>
<head>
    @if(isset($vars))
        @include('mail.styles', ['partner' => $vars->partner])
    @else
        @include('mail.styles', ['partner' => $partner])
    @endif
</head>
​
@if(!isset($partner))
    @php($partner = isset($vars->partner) ? $vars->partner : $global_partner)
@endif

<body>

<table style="margin:0 auto; width: 100%;" border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="{{$partner->primary_colour}}">
    <tr>
        <td align="center">
                <a href="#"><img style="width: 300px; padding: 10px; background-color: white" src="{{ $message->embed(asset('/storage/logos/' . $partner->logo)) }}" alt="Trustbox"></a>
        </td>
    </tr>
</table>

@yield('content')
​
<table style="margin:0 auto; width: 100%;" cellspacing="0" cellpadding="0" bgcolor="{{$partner->primary_colour}}" width="100%"  class="force-full-width">
    <tr>
        <td align="center" style="padding: 20px">
            <a href="#"><img style="width: 300px; padding: 10px; background-color: white" src="{{ $message->embed(asset('/storage/logos/' . $partner->logo)) }}" alt="Trustbox"></a>
        </td>
    </tr>
    <tr>
        <td style="color:#858585; font-size: 14px; text-align:center; padding-bottom:45px;">
            Copyright © 2020 TrustBox LTD Registered in England, Registration number: 11437803
        </td>
    </tr>
</table>
</body>
</html>
