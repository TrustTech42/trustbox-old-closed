@extends('layouts.backups.admin-login')
@section('content')
    <div class="container">
        @include('shared.error')
    </div>

    <div class="container p-3 my-5">
        <div class="row">
            <div class="col-xl-6 offset-xl-3 bg-light p-5 light-rounded">
                <img src="{{ asset('images/logo.png') }}" alt="logo" class="img-fluid w-25 mb-5">
                {!! Form::open(['route' => 'login', 'method' => 'POST', 'novalidate', 'class' => 'needs-validation']) !!}
                {!! Form::bsText('email', old('email'), ['required', 'autofocus', 'class' => 'form-control']) !!}
                {!! Form::bsPassword('password', null, null, ['required', 'class' => 'form-control']) !!}

                <a href="/password/reset" class="btn btn-link float-left">Forgotten Password?</a>
                <button class="btn btn-primary float-right">LOG IN</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
