@extends("layouts.full-page")
@section("content")
    @include('frontend.partials.html.banner-login')
    @yield('content')
@endsection
