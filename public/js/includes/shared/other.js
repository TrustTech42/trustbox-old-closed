$(document).ready(function () {
    // Variables used in multiple places
    let boostTimer;
    let boostTimerSeconds = 3;

    // Enrollment confirmation modal
    $(document).on('show.bs.modal', '#enroll-confirm', function (event) {
        let button        = $(event.relatedTarget);
        let benefit       = button.data('benefit');
        let name          = button.data('title');
        let provider      = button.data('provider');
        let form          = $('form.benefit-' + benefit);
        let confirmButton = $(this).find('button').first();

        $(this).find('.name').text(name);
        $(this).find('.provider').text(provider);

        confirmButton.on('click', function () {
            form.submit();
        })
    });

    // Boosting benefits confirmation window
    $(document).on('show.bs.modal', '#boost-confirm', function (event) {
        // start timer
        let timeBox = $(".seconds");
        let switches = $(".benefit-toggle");
        let total    = 0;

        $.each(switches, function () {
            if ($(this).is(":checked")) {
                total += parseFloat($(this).data('price'));
            }
        });

        $(this).find('.new-price .value').text(total);
    });

    $('#closemodal').click(function() {
        $('#modalwindow').modal('hide');
    });

    // Boosting benefits new total
    $(document).on('change', '.benefit-toggle', function (event) {
        let currentTotalBox = $('.monthly-total');
        let newTotalBox     = $(".new-total");

        // Fade out monthly total and show new
        currentTotalBox.css('opacity', '0.4');
        newTotalBox.css('opacity', '1');

        let switches = $(".benefit-toggle");
        let total    = 0;

        $.each(switches, function () {
            if ($(this).is(":checked")) {
                total += parseFloat($(this).data('price'));
            }
        });

        newTotalBox.find('.value').text(total);
    });

    $(document).on('show.bs.modal', '#boost-edit', function () {
        let switches = $(".benefit-toggle");
        let total    = 0;
        let currentTotalBox = $('.monthly-total');
        let newTotalBox     = $(".new-total");

        $.each(switches, function () {
            if ($(this).is(":checked")) {
                total += parseFloat($(this).data('price'));
            }
        });

        $(this).find('.old-price .value').text(currentTotalBox.find('.value').text());
        $(this).find('.new-price .value').text(newTotalBox.find('.value').text());

        $(".boost-edit-modal-button").click(function(){
            $(".boosted-benefits-form").submit();
        })
    });

    // Show modal when page loads, only used for testing - remove before pushing to production
    //$("#storage-upgrade-modal").modal();
});
