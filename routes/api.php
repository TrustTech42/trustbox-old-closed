<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


# Requires token
use App\User;
use Illuminate\Support\Facades\Log;

Route::middleware('auth:api')->group(function () {
    # users
    Route::get('user', 'Api\UserController@show');
    Route::post('user/update', 'Api\UserController@update');
});

# Doesn't require token
Route::middleware('api')->group(function () {
    Route::post('login', 'Api\AuthController@login');

    Route::post('/lifetime/callback', 'NoChex\LifetimeCallbackController@NoChexCallback')->name('lifetime.callback');
    Route::post('/boost/callback', 'NoChex\BoostCallbackController@NoChexCallback')->name('boost.callback');
    Route::post('/schedule/listener', 'NoChex\BoostScheduleController@NoChexListener')->name('boost.listener');
});

