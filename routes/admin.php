<?php

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::get('/', function () {

        $members = App\Membership::count();
        $partners = App\Partner::count();

        $total = 0;

        return view("admin.index", compact('members', 'partners', 'total'));
    });

    Route::get('invitations/download', 'Admin\InvitationsController@download')->name('invitations.download');

    Route::middleware(['permissions'])->group(function () {
        # -------------------------------------
        # Resource Routes
        # -------------------------------------
        Route::resources([
            'pages' => 'Admin\PageController',
            'blocks' => 'Admin\BlockController',
            'partners' => 'Admin\PartnerController',
            'benefits' => 'Admin\BenefitController',
            'ubenefits' => 'Admin\UniqueBenefitController',
            'posts' => 'Admin\PostController',
            'contacts' => 'Admin\ContactController',
            'categories' => 'Admin\CategoryController',
            'users' => 'Admin\UserController',
            'providers' => 'Admin\ProviderController',
            'settings' => 'Admin\SettingController',
            'categories' => 'Admin\CategoryController',
            'requests' => 'Admin\RequestController',
            'changelog' => 'Admin\ChangelogController',
            'letters' => 'Admin\LetterController',
            'invitations' => 'Admin\InvitationsController',
            'reasons' => 'Admin\ReasonController'
        ]);
    });

    /* BENEFITS */
    Route::post('benefits/search', 'Admin\BenefitController@search')->name('benefits.search');
    Route::post('ubenefits/search', 'Admin\UniqueBenefitController@search')->name('ubenefits.search');
    Route::post('benefits/filter/types', 'Admin\BenefitController@filterType')->name('benefits.filter.types');
    Route::post('ubenefits/filter/types', 'Admin\UniqueBenefitController@filterType')->name('ubenefits.filter.types');
    Route::post('benefits/filter/categories', 'Admin\BenefitController@filterCategory')->name('benefits.filter.categories');
    Route::post('ubenefits/filter/categories', 'Admin\UniqueBenefitController@filterCategory')->name('ubenefits.filter.categories');

    /* USERS */
    Route::get('user/export', 'Admin\UserController@export')->name('user.export');
    Route::post('user/search', 'Admin\UserController@search')->name('user.search');
    Route::post('user/filter', 'Admin\UserController@filter')->name('user.filter');

    /*
     * LETTERS
     * */
    Route::post('letter/print', 'Admin\LetterController@print')->name('letter.print');
    Route::post('letter/search', 'Admin\LetterController@search')->name('letter.search');
    Route::post('letter/filter', 'Admin\LetterController@filter')->name('letter.filter');
    Route::post('letter/filter/providers', 'Admin\LetterController@filterProviders')->name('letter.filter.providers');

    /*
     * INVITATIONS
     * */
    Route::post('invitations/send', 'Admin\InvitationsController@send')->name('invitations.send');
    Route::post('invitations/print', 'Admin\InvitationsController@print')->name('invitations.print');
    Route::post('invitations/search', 'Admin\InvitationsController@search')->name('invitations.search');
    Route::post('invitations/filter', 'Admin\InvitationsController@filter')->name('invitations.filter');
    Route::post('invitations/filter/providers', 'Admin\InvitationsController@filterProviders')->name('invitations.filter.providers');


    /* CONTACTS */
    Route::post('contacts/filter', 'Admin\ContactController@filter')->name('contacts.filter');

    /* REQUESTS */
    Route::post('requests/filter', 'Admin\RequestController@filter')->name('requests.filter');


    Route::post('settings/mass_update', 'Admin\SettingController@massUpdate')->name('settings.mass_update');
    Route::post('reasons/mass_update', 'Admin\ReasonController@massUpdate')->name('reasons.mass_update');
    Route::post('providers/search', 'Admin\ProviderController@search')->name('provider.search');
    Route::post('partners/search', 'Admin\PartnerController@search')->name('partner.search');

    # -------------------------------------
    # Special Routes
    # -------------------------------------
    Route::post('blocks/update_many', 'Admin\BlockController@updateMany');
});

