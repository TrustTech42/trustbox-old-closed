<?php

use App\User;
use App\Benefit;
use App\Page;
use App\Service;
use App\Post;
use App\Invite;
use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| Temporary Routes
|--------------------------------------------------------------------------
*/

//    // Partner Account Broadcast Routes
//    Route::get('partner/{id}/broadcasts/list', 'Partner\BroadcastController@list')->name('partner.list.broadcasts');
//    Route::get('partner/{id}/broadcasts/create', 'Partner\BroadcastController@create')->name('partner.create.broadcasts');

//    Route::get('partner/{partnerId}/broadcasts/{messageId}', 'Partner\BroadcastController@show')->name('partner.show.broadcast');
//
//    // Partner Account Inbox Routes
//    Route::get('partner/{id}/inbox/members/list', 'Partner\InboxController@listCustomerMessages')->name('partner.inbox.members.list');

Route::post('partner/{id}/broadcasts/store', 'Partner\BroadcastController@store')->name('partner.store.broadcasts');
Route::get('partner/{id}/inbox/members/{contactId}', 'Partner\InboxController@showCustomerMessage')->name('partner.inbox.members.show');

Route::get('/partner/{partner}/dashboard/{part1?}/{part2?}/{part3?}', 'PartnerController@show');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Default Routes
Auth::routes();

Route::get('/', function () {
    if (config('app.url') == url()->current()) {
        return view('admin-welcome');
    }

    return view('welcome');
});

Route::post('partner/sort', 'PartnerController@updateSortOrder');

Route::post('posts.search', 'PostController@search')->name('posts.search');

Route::view('privacy-policy', 'frontend.general.privacy');
Route::view('terms-and-conditions', 'frontend.general.terms');

Route::get('/send-code/{user}', 'SmsVerificationController@sendCode')->name('sms-verification.send.code');
Route::get('/start-contact/{user}', 'SmsVerificationController@startContact')->name('sms-verification.start.contact');
Route::post('/verify-contact', 'SmsVerificationController@verifyContact')->name('sms-verification.verify.contact');

Route::resource('posts', 'PostController')->only(['show', 'index']);

// Resource Routes
Route::middleware(['subdomain', 'auth', 'protected'])->group(function () {
    Route::resource('benefits', 'BenefitController')->except(['index']);
    Route::resource('user', 'UserController')->except(['store']);
    Route::patch('user/{user}/change-storage-plan', 'UserController@changeStoragePlan')->name('user.change-storage-plan');

    Route::resources([
        'partner' => 'PartnerController',
        'provider' => 'ProviderController',
        'request' => 'RequestController',
        'services' => 'ServiceController'
    ]);

    Route::resource('posts', 'PostController')->except(['show', 'index']);

    Route::post('benefits/{id}/contact', 'BenefitContactController@benefitForm')->name('benefit.form');
    Route::post('benefits/{id}/notify', 'BenefitContactController@benefitNotify')->name('benefit.notify');
    Route::get('benefits/{id}/form-success', 'BenefitContactController@formSuccess')->name('benefit.form.success');
    Route::get('benefits/{id}/call-success', 'BenefitContactController@callSuccess')->name('benefit.call.success');

    // Partner Connection Routes
    Route::delete('partner/{partnerId}/{connectionId}/delete-connection', 'PartnerController@deleteConnectionRequest')->name('partner.delete.connection');
    Route::patch('partner/{partnerId}/{connectionId}/update-connection', 'PartnerController@acceptConnectionRequest')->name('partner.update.connection');
    Route::post('partner/{partnerId}/create-connection', 'PartnerController@sendConnectionRequest')->name('partner.create.connection');

    // User Broadcast Routes
    Route::GET('user/{id}/broadcasts/list', 'User\BroadcastController@list')->name('user.list.broadcasts');
    Route::GET('user/{id}/broadcasts/{broadcastId}', 'User\BroadcastController@show')->name('user.show.broadcasts');

//    Route::post('benefits/{id}/contact', 'BenefitController@contactBe')

    // Authenticated get routes
    Route::get('benefits/{id}/redeem', 'BenefitController@redeem');
    Route::get('provider/{id}/export', 'ProviderController@export');

    Route::post('provider/{provider}/search', 'ProviderController@search')->name('provider.user.search');
    Route::post('provider/{provider}/filter', 'ProviderController@filter')->name('provider.user.filter');

    Route::get('update-membership-payment/{user}', 'PaymentController@updateMembershipPayment')->name('update.membership-payment');
    Route::get('create-payment/{user}', 'PaymentController@create')->name('create.payment');

    // PARTNER BLOG ROUTES
    Route::get('partner/{partnerId}/dashboard/post/create', 'Partner\PostController@createMyPosts')->name('partner.mine.posts.create');
    Route::post('partner/{partnerId}/dashboard/post/store', 'Partner\PostController@storeMyPosts')->name('partner.mine.posts.store');
    Route::get('partner/{partnerId}/dashboard/post/index', 'Partner\PostController@listMyPosts')->name('partner.mine.posts.index');
    Route::get('partner/{partnerid}/dashboard/post/view/{postid}', 'Partner\PostController@showMyPosts')->name('partner.mine.posts.show');
    Route::patch('partner/{partnerId}/dashboard/post/{postId}/update' , 'Partner\PostController@updateMyPosts')->name('partner.mine.posts.update');
    Route::post('partner/{partnerId}/dashboard/post/search', 'Partner\PostController@searchMyPosts')->name('partner.mine.posts.search');
    Route::delete('partner/{partnerId}/dashboard/post/{postId}/delete', 'Partner\PostController@deleteMyPosts')->name('partner.mine.posts.delete');

    // CONNECTED PARTNER BLOG ROUTES
//    Route::get('partner/{partnerId}/dashboard/post/connection/index', 'Partner\CommunityPostController@listPosts')->name('partner.connection.posts.index');
    Route::get('partner/{partnerId}/dashboard/post/connection/view/{postId}', 'Partner\CommunityPostController@showPost')->name('partner.connection.posts.show');
    Route::patch('partner/{partnerId}/dashboard/post/connection/{postId}/update', 'Partner\CommunityPostController@updatePost')->name('partner.connection.posts.update');
    Route::post('partner/{partnerId}/dashboard/post/connection/search', 'Partner\CommunityPostController@searchPosts')->name('partner.connection.posts.search');
    Route::post('partner/{partnerId}/dashboard/post/connection/filter_connections', 'Partner\CommunityPostController@filterConnections')->name('partner.connection.posts.connections.filter');
    Route::post('partner/{partnerId}/dashboard/post/connection/filter_categories', 'Partner\CommunityPostController@filterCategories')->name('partner.connection.posts.categories.filter');

    // ACTIVE CONNECTED PARTNER BLOG ROUTE
//    Route::get('partner/{partnerId}/dashboard/post/active/index', 'Partner\ActiveCommunityPostController@listActivePosts')->name('partner.connection.active.posts.index');
    Route::get('partner/{partnerId}/dashboard/post/active/view/{postId}', 'Partner\ActiveCommunityPostController@showActivePosts')->name('partner.connection.active.posts.show');
    Route::delete('partner/{partnerId}/dashboard/post/active/{postId}/delete', 'Partner\ActiveCommunityPostController@deleteActivePosts')->name('partner.connection.active.posts.delete');
});

Route::get('nochex/boost-benefit/{benefit}', 'BenefitController@boostBenefitNoChex')->name('nochex.boost-benefit');
Route::post('user/show/{user}', 'CallbackController@redirectSuccess')->name('direct.show.user');

// Unauthenticated resource routes
Route::resources([
    'benefits' => 'BenefitController',
    'payment' => 'PaymentController',
    'contact' => 'ContactController'
]);

Route::post('user/store', 'UserController@store')->name('user.store');

// Unauthenticated view routes
Route::view('user-login', 'auth.user-login');
Route::view('nosite', 'errors.nosite');
Route::view('terms_and_conditions', 'frontend.general.terms');


// Unauthenticated GET Routes
Route::get('dashboard', 'OtherController@showDashboard');
Route::get('/register/partner', 'Auth\RegisterController@showPartnerRegistrationForm')->name('partner-registration');
Route::get('register/change', 'Auth\RegisterController@changeCustomerData');
Route::get('/users/{id}/log_in_as', function ($id) {

    $user = App\User::withTrashed()->find($id);

    $user->changelogs()->create([
        'action' => 'Admin logged in as user'
    ]);

    session()->put(['og_user' => auth::user()]);

    \Auth::logout();
    \Auth::loginUsingId($id);

    return redirect('/dashboard');
});

Route::get('/admin/{id}/log_in_as', function ($id) {

    session()->forget('og_user');

    \Auth::logout();
    \Auth::loginUsingId($id);

    return redirect('/dashboard');
});

//Route::post('subscription/updatedate', 'SubscriptionController@updateGocardlessSubDate');

// Unauthenticated POST routes
//Route::post('subscription/change-plan', 'SubscriptionController@updateSubscriptionForStorage')->name('subscription.change-storage-plan');
Route::post('enrol', 'BenefitController@enrol')->name('enrol');
//Route::post('update-benefit-boost', 'BenefitController@updateBenefitBoost')->name('update.benefit.boost');
Route::post('subsequent-benefit-boost/{id}', 'BenefitController@subsequentBoost')->name('subsequent.benefit.boost');
Route::post('cancel-boost/{id}', 'BenefitController@cancelSubsequentBoost')->name('cancel.boost');

Route::post('boost-success', 'BenefitController@boostSuccess')->name('boost.success');
Route::post('remove', 'MailController@remove')->name('remove');


Route::get('accept/{user_id}/{linked_user_id}', function ($user, $linkedUser) {
    $user = User::find($user);
    $linkedUser = User::find($linkedUser);

    $user->trusted()->attach($linkedUser, ['active' => 1]);

    $invites = Invite::where('user_id', $user->id)->where('email', $linkedUser->email);
    $invites->update(["accepted" => 1]);

    return redirect('/dashboard');
});

Route::any('page/{any}', function ($any) {
    $page = Page::where('url', '=', $any);

    if ($page->exists()) {
        $benefit = $page->first()->benefit;
        return view('frontend.benefits.show', compact('page', 'benefit'));
    }

    return view('errors.404');

})->where('any', '.*');

# Temporary pages
Route::view('petition', 'frontend.general.petition');
Route::view('donations', 'frontend.general.donations');

Route::post('invite', 'MailController@invite')->name('invite');

//Route::get('invite/{user}/{type}', function ($user, $type) {
//    $invite = App\Invite::where('plan', $type)->where('user_id', $user)->first();
//
//    if(!$invite){
//        abort(404);
//    }
//
//    $emailRequired = $invite->email;
//
//    $user = App\User::findorfail($user);
//    $type = App\Plan::where('name', '=', $type)->first();
//
//    return view('auth.invite', compact('user', 'type', 'emailRequired'));
//});

Route::get('/invite/{user}/{code}', function ($user, $code) {
    $invite = App\Invite::where('code', $code)->where('user_id', $user)->first();

    if (!$invite) abort(404);

    $user = App\User::findorfail($user);
    $type = App\Plan::where('name', '=', $invite->plan)->first();

    return view('auth.invite', compact('user', 'type', 'code'));
});

Route::get('invite/{invitation}/register/invite/user', function ($invitation) {
    return view('auth.invite-register', compact('invitation'));
});
