<?php

use App\Benefit;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::post('/block/update', function (Request $request) {
//    $data = $request->get('block');
//
//    if (isset($data['id']) && $data['id'] < 9999999999999) {
//        $block = \App\Block::find($data['id']);
//        $block->update($data);
//    } else {
//        $block = \App\Block::create($data);
//    }
//
//    return new JsonResponse($block->id, 200);
//});
//
//
//Route::post('/block/destroy', function (Request $request) {
//   $data = $request->get('block');
//
//   if(isset($data['id'])){
//       $block = \App\Block::find($data['id']);
//
//       if(!is_null($block))
//       {
//           $block->delete();
//       }
//   }
//
//   return new JsonResponse($data, 200);
//});
//
//Route::post('/block/image/upload', function(Request $request){
//    $file = $request->file->store('public/blocks');
//    $file = str_replace('public', 'storage', $file);
//
//    return $file;
//});
//
//Route::post('/page/update', function(Request $request)
//{
//    $page = Page::find($request->get('page_id'));
//    $benefit = Benefit::find($request->get('benefit_id'));
//
//    $page->update([
//        'benefit_id' => $request->get('benefit_id')
//    ]);
//
//    $benefit->update([
//        'page_id' => $request->get('page_id')
//     ]);
//});
