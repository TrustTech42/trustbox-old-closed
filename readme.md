## Trustbox Repository
This repository now runs docker. To clone and install, run the following two commands;

## Commands
```shell script
git clone git@bitbucket.org:TrustTech42/trustbox.git \
&& cd trustbox \
&& git checkout docker \
&& docker-compose up -d --build
```

```shell script
docker exec -it trustbox-app sh -c "rm -rf /var/www/public/storage && composer install" \
&& docker exec -it trustbox-app sh -c "sshpass -p 'bg1w+CyWHysB' scp -P 2020 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null root@194.39.167.102:/home/roottrus/site/.env /var/www/.env" \
&& docker exec -it trustbox-app sh -c "sshpass -p 'bg1w+CyWHysB' scp -r -P 2020 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null root@194.39.167.102:/home/roottrus/site/storage/app/public /var/www/storage/app/public" \
&& docker exec -it trustbox-app sh -c "php artisan storage:link" 
```

## Make the following changes to your env file

```dotenv
DB_HOST=trustbox-db
APP_URL=https://trustbox.test
APP_DOMAIN=trustbox.test
```

